<%@page session="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*,javax.portlet.*,sa.com.mobily.eportal.lines.mgmt.portlet.*" %>
<%@ taglib uri="/WEB-INF/tld/portal.tld" prefix="wps" %>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>        
<%@taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt_rt"%> 
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>        
<portlet:defineObjects/>
<fmt:setBundle  basename="sa.com.mobily.eportal.lines.mgmt.portlet.nl.LinesManagementPortletResource" />
<style>
#account_dashboard.broadband .notification_box{border:none}
#account_dashboard.broadband .data_box .cta_data_consumption{background:none;}
#account_dashboard.broadband .notification_box{padding:0}
#account_dashboard.broadband .data_box .data_consumption_slider_wrap{padding:0}
.style_definer .stacked {margin: 15px 0 0 0;padding: 0;}
#account_dashboard.broadband .notification_box li a {
    width: auto;
    float: left;
    padding: 7px 10px;
    background: #e7d46e;
    background-image: none;
}
#content_main_cont .style_definer .close_alert_msg {
   
	margin: 5px 10px;
}
</style>
<script type="text/javascript" >
    var djConfig  = {
        parseOnLoad: false,
        locale: 'en-us',
        packages: [ {
            name: 'custom',
            location: '/MobilyResources/default/js/custom'
        } ],
        require : [
         'dojo/NodeList-traverse',
            'dojo/ready',
            'dojo/query',
            'dijit/Dialog',
            'dijit/form/Button',
            'dijit/form/CheckBox',
            'dijit/form/DateTextBox',
            'dijit/form/FilteringSelect',
            'dijit/form/Form',
            'dijit/form/HorizontalSlider',
            'dijit/form/Select',
            'dijit/form/RadioButton',
            'dijit/form/TextBox',
            'dijit/form/Textarea',
            'dijit/form/TimeTextBox',
            'dijit/form/ValidationTextBox',
            'dojox/form/BusyButton',
            'dojox/form/CheckedMultiSelect',
            'dojox/grid/EnhancedGrid',
            'dojox/grid/DataGrid',
            'dijit/layout/TabContainer',
            'dijit/layout/ContentPane',
            'dijit/layout/AccordionContainer'
        ],
        async : true
    }

</script>
<script type="text/javascript" src="/MobilyResources/default/js/dojo/dojo/dojo.js" ></script>
<script type="text/javascript">require(['custom/run']);</script>
<script type="text/javascript">
require([
"dijit/registry",
"dojox/widget/Standby",
"custom/grid",
"dojox/validate/web",
"dojox/validate/check",
"dijit/form/TextBox",
"dijit/form/FilteringSelect",
"dojo/ready"
]);
	function <portlet:namespace />clearSelectData(selectWidgetName){
		if(dijit.byId(selectWidgetName).value == '0')
			dijit.byId(selectWidgetName).textbox.value = '';
	};
</script>

<c:if test="${fn:contains(pageContext.request.locale.language,'en')}">
<link rel="stylesheet" media="all" href="/MobilyResources/english/css/enhancedgrid.css">
</c:if>
<c:if test="${fn:contains(pageContext.request.locale.language,'ar')}">
<link rel="stylesheet" media="all" href="/MobilyResources/arabic/css/enhancedgrid.css">
</c:if>

<portlet:actionURL var="addDefaultLine">
<portlet:param name="javax.portlet.action" value="addDefaultLine" />
</portlet:actionURL>


<section id="content_main" class="block clearfix">
	<div class="row">
		<div id="addDefaultLineDiv" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<c:set var="display" value="none" />
			<c:if test="${not empty requestScope.errorMessagesMap}">
				<c:set var="display" value="block" />
			</c:if>
			
			<div id="error_div" style='display:${display}' class="row">
				<c:forEach var="error" items="${requestScope.errorMessagesMap}" varStatus="msgsCounter">
					<div id="errorMsg${msgsCounter.count}" class="alert_msg alert_yield_msg col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<p><fmt:message key='${error.value}' /></p>
						<ul class="prompt_controls">
							<li><a style="cursor:pointer;" onclick="dojo.byId('errorMsg${msgsCounter.count}').setAttribute('style','display:none');" class="cta_prompt transition"><fmt:message key="common.msgs.dialog.button.ok.label"></fmt:message></a></li>
						</ul>
						<a style="cursor:pointer;" onclick="dojo.byId('errorMsg${msgsCounter.count}').setAttribute('style','display:none');" class="close_alert_msg kir"><fmt:message key="common.msgs.dialog.button.close.label"></fmt:message></a>
					</div>
				</c:forEach>
			</div>
			<form class="int_widget style_definer" action="<%=addDefaultLine%>" id="addDefaultLineDivForm" name="addDefaultLineDivForm"  dojoType="dijit.form.Form" method="post" >	
				<script type="dojo/method" event="onSubmit">
				
					if(this.validate()) {
						console.log("here1");
						return true;
					} else {
						console.log("here2");
						return false;
					}			
				</script> 
			<div id="primaryNumberErrorMsg" class="alert_msg alert_yield_msg row" style="display:none;">
				<p class="col-lg-3 col-md-3 col-sm-10 col-xs-10"><fmt:message key="lines.mgmt.error.mobile.number.empty"></fmt:message></p>
				<ul class="prompt_controls floatr" style="width:auto">
					<li><a style="cursor:pointer;" onclick="dojo.byId('primaryNumberErrorMsg').setAttribute('style','display:none');" class="cta_prompt transition"><fmt:message key="common.msgs.dialog.button.ok.label"></fmt:message></a></li>
				</ul>
				<a style="cursor:pointer;" onclick="dojo.byId('primaryNumberErrorMsg').setAttribute('style','display:none');" class="close_alert_msg kir"><fmt:message key="common.msgs.dialog.button.close.label"></fmt:message></a>
			</div>
				<p><fmt:message key="lines.mgmt.label.no.primary.line"><fmt:param value="${requestScope.userName}"></fmt:param></fmt:message></p>
				
				
					<c:forEach items="${requestScope.allSubscriptions}" var="subscriptionListVar">
					
							<div style="margin-top: 20px;display: inline-block;">
								<label for="number_one"> 
									<input name="subscriptionNumberRadio" type="radio" value="${subscriptionListVar.msidsn}" dojoType="dijit.form.RadioButton" />  ${subscriptionListVar.msidsn}
								</label>
							</div>
				
					</c:forEach>			
			<div style="margin-top: 20px;">
				<input type="button" onclick="makePrimaryNumber();" id="saveSettingsBtn"  value='<fmt:message key="lines.mgmt.button.save.settings" />' class="cta_bt gradient transition"/>
            </div>
			</form>
		</div>
	</div>   
</section>

<script>
	function makePrimaryNumber() {
		var result = true;
		var numberSelected = false;
		dojo.setStyle("primaryNumberErrorMsg", "display", "none");
		console.log("length="+document.getElementsByName("subscriptionNumberRadio").length);
		for (var i=0; i<document.getElementsByName("subscriptionNumberRadio").length; i++) {
			if (document.getElementsByName("subscriptionNumberRadio")[i].checked) {
				numberSelected = true;
				break;
			}
		}
		console.log("numberSelected="+numberSelected);
		if(numberSelected) {
			dojo.byId('saveSettingsBtn').disabled=true;
			dojo.byId("saveSettingsBtn").setAttribute("class", "cta_bt_alt gradient floatl transition");
			dojo.byId('addDefaultLineDivForm').action = '<%=addDefaultLine%>';
			dojo.byId('addDefaultLineDivForm').submit();
		} else {
			dojo.setStyle("primaryNumberErrorMsg", "display", "block");
		}

	}
</script>


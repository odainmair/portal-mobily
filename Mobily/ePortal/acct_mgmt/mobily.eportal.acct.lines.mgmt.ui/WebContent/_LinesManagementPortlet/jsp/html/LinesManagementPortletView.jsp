<%@page session="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*,javax.portlet.*,sa.com.mobily.eportal.lines.mgmt.portlet.*" %>
<%@ taglib uri="/WEB-INF/tld/portal.tld" prefix="wps" %>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>        
<%@taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt_rt"%> 
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>        
<portlet:defineObjects/>
<fmt:setBundle  basename="sa.com.mobily.eportal.lines.mgmt.portlet.nl.LinesManagementPortletResource" />
<style>
#account_dashboard.broadband .notification_box{border:none}
#account_dashboard.broadband .data_box .cta_data_consumption{background:none;}
#account_dashboard.broadband .notification_box{padding:0}
#account_dashboard.broadband .data_box .data_consumption_slider_wrap{padding:0}
.style_definer .stacked {margin: 15px 0 0 0;padding: 0;}
#account_dashboard.broadband .notification_box li a {
    width: auto;
    float: left;
    padding: 7px 10px;
    background: #e7d46e;
    background-image: none;
}
#content_main_cont .style_definer .close_alert_msg {
   
	margin: 5px 10px;
}

</style>
<script type="text/javascript" >
    var djConfig  = {
        parseOnLoad: false,
        locale: 'en-us',
        packages: [ {
            name: 'custom',
            location: '/MobilyResources/default/js/custom'
        } ],
        require : [
         'dojo/NodeList-traverse',
            'dojo/ready',
            'dojo/query',
            'dijit/Dialog',
            'dijit/form/Button',
            'dijit/form/CheckBox',
            'dijit/form/DateTextBox',
            'dijit/form/FilteringSelect',
            'dijit/form/Form',
            'dijit/form/HorizontalSlider',
            'dijit/form/Select',
            'dijit/form/RadioButton',
            'dijit/form/TextBox',
            'dijit/form/Textarea',
            'dijit/form/TimeTextBox',
            'dijit/form/ValidationTextBox',
            'dojox/form/BusyButton',
            'dojox/form/CheckedMultiSelect',
            'dojox/grid/EnhancedGrid',
            'dojox/grid/DataGrid',
            'dijit/layout/TabContainer',
            'dijit/layout/ContentPane',
            'dijit/layout/AccordionContainer'
        ],
        async : true
    }

</script>
<script type="text/javascript" src="/MobilyResources/default/js/dojo/dojo/dojo.js" ></script>
<script type="text/javascript">require(['custom/run']);</script>
<script type="text/javascript">
require([
"dijit/registry",
"dojox/widget/Standby",
"custom/grid",
"dojox/validate/web",
"dojox/validate/check",
"dijit/form/TextBox",
"dijit/form/FilteringSelect",
"dojo/ready"
]);
	function <portlet:namespace />clearSelectData(selectWidgetName){
		if(dijit.byId(selectWidgetName).value == '0')
			dijit.byId(selectWidgetName).textbox.value = '';
	};
</script>

<c:if test="${fn:contains(pageContext.request.locale.language,'en')}">
<link rel="stylesheet" media="all" href="/MobilyResources/english/css/enhancedgrid.css">
</c:if>
<c:if test="${fn:contains(pageContext.request.locale.language,'ar')}">
<link rel="stylesheet" media="all" href="/MobilyResources/arabic/css/enhancedgrid.css">
</c:if>

<portlet:actionURL var="addNewLineForNonMobily">
<portlet:param name="javax.portlet.action" value="addNewLineForNonMobily" />
</portlet:actionURL>

<portlet:actionURL var="cancelSubs">
<portlet:param name="javax.portlet.action" value="cancelSubs" />
</portlet:actionURL>



<section id="content_main" class="block clearfix">
	<div class="row">
		
		<c:set var="supportPagesDisplay" value="block" />
		<c:if test="${not empty portletSessionScope.userSubscriptionVO.subscriptionType}">
			<c:set var="supportPagesDisplay" value="none" />
		</c:if>
		<div id="support_pages" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display:${supportPagesDisplay}; ">
			<form method="post" action="<%=cancelSubs%>" id="linesManagementForm" name="linesManagementForm">
				<input type="hidden" id="subscriptionTypeValue" name="SubscriptionType" value="${portletSessionScope.userSubscriptionVO.subscriptionType}" />
				<p style="float:none"><fmt:message key="lines.mgmt.header.label2" /></p>
	            <div id="btnCtrl" style="display: inline-block;">
				<a class="newBtn" id="addMobileLine" onclick="javascript:subscriptionTypeChanged('1');"><span class="mobilyline"></span><fmt:message key="lines.mgmt.button.add.mobile.line" /></a>
				<a class="newBtn" id="addDataSimLine"  onclick="javascript:subscriptionTypeChanged('2');"><span class="simline"></span><fmt:message key="lines.mgmt.button.add.data.sim.line" /></a>
				<a class="newBtn" id="addFibreLine" onclick="javascript:subscriptionTypeChanged('4');"><span class="fibreline"></span><fmt:message key="lines.mgmt.button.add.fiber.line" /></a>
  				<div class="floatr" style="clear: both;margin: 15px;">
					<input type="button" onclick="javascript:showSubscription();" value='<fmt:message key="lines.mgmt.button.next" />' class="cta_bt gradient transition" />
				</div>
				</div>
			</form>
        </div>
         
        <c:set var="gsmDisplay" value="none" />
        <c:set var="dataSimDisplay" value="none" />
        <c:set var="broadbandDisplay" value="none" />
        
		<c:if test="${not empty portletSessionScope.userSubscriptionVO.subscriptionType && portletSessionScope.userSubscriptionVO.subscriptionType eq 1}">
			<c:set var="gsmDisplay" value="block" />
		</c:if>

		<c:if test="${not empty portletSessionScope.userSubscriptionVO.subscriptionType && portletSessionScope.userSubscriptionVO.subscriptionType eq 2}">
			<c:set var="dataSimDisplay" value="block" />
		</c:if>		
		
		<c:if test="${not empty portletSessionScope.userSubscriptionVO.subscriptionType && portletSessionScope.userSubscriptionVO.subscriptionType eq 4}">
			<c:set var="broadbandDisplay" value="block" />
		</c:if>
		
		<div id="mobileDiv" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display:${gsmDisplay}; ">
			<c:set var="display" value="none" />
			<c:if test="${not empty requestScope.errorMessagesMap}">
				<c:set var="display" value="block" />
			</c:if>
			
			<div id="error_div" style='display:${display}' class="row">
				<c:forEach var="error" items="${requestScope.errorMessagesMap}" varStatus="msgsCounter">
					<div id="errorMsg${msgsCounter.count}" class="alert_msg alert_yield_msg col-lg-10 col-md-10 col-sm-10 col-xs-10">
						<p><fmt:message key='${error.value}' /></p>
						<ul class="prompt_controls floatr" style="width:auto">
							<li><a style="cursor:pointer;" onclick="dojo.byId('errorMsg${msgsCounter.count}').setAttribute('style','display:none');" class="cta_prompt transition"><fmt:message key="common.msgs.dialog.button.ok.label"></fmt:message></a></li>
						</ul>
						<a style="cursor:pointer;" onclick="dojo.byId('errorMsg${msgsCounter.count}').setAttribute('style','display:none');" class="close_alert_msg kir"><fmt:message key="common.msgs.dialog.button.close.label"></fmt:message></a>
					</div>
				</c:forEach>
			</div>
			<form class="int_widget style_definer" action="<%=addNewLineForNonMobily%>" id="addLineMobileDivForm" name="addLinePortletForm"  dojoType="dijit.form.Form" method="post" >	

	        <!-- Mobile Start-->
            
            <input type="hidden" id="subscriptionTypeValueGSM" name="SubscriptionType" value="${portletSessionScope.userSubscriptionVO.subscriptionType}" />
            <div class="stacked">
			<h2 class="form_title row" >
				<div class="col-lg-10 col-md-10 col-sm-7 col-xs-7"><fmt:message key="lines.mgmt.button.add.mobile.line" /></div> 
			</h2>					
			</div>     
			<div id="activationCode_server_error_msg" class="alert_msg alert_yield_msg row" style="display:none;">
				<p id="pinCodeErrorMsgText" class="col-lg-10 col-md-10 col-sm-10 col-xs-10"><fmt:message key="lines.mgmt.error.activation.code.server"></fmt:message></p>
				<ul class="prompt_controls floatr" style="width:auto">
					<li><a style="cursor:pointer;" onclick="dojo.byId('activationCode_server_error_msg').setAttribute('style','display:none');" class="cta_prompt transition"><fmt:message key="common.msgs.dialog.button.ok.label"></fmt:message></a></li>
				</ul>
				<a style="cursor:pointer;" onclick="dojo.byId('activationCode_server_error_msg').setAttribute('style','display:none');" class="close_alert_msg kir"><fmt:message key="common.msgs.dialog.button.close.label"></fmt:message></a>
			</div>
			<div id="usernameErrorMsg" class="alert_msg alert_yield_msg row" style="display:none;">
				<p class="col-lg-10 col-md-10 col-sm-10 col-xs-10"><fmt:message key="lines.mgmt.error.mobile.number.empty"></fmt:message></p>
				<ul class="prompt_controls floatr" style="width:auto">
					<li><a style="cursor:pointer;" onclick="dojo.byId('usernameErrorMsg').setAttribute('style','display:none');" class="cta_prompt transition"><fmt:message key="common.msgs.dialog.button.ok.label"></fmt:message></a></li>
				</ul>
				<a style="cursor:pointer;" onclick="dojo.byId('usernameErrorMsg').setAttribute('style','display:none');" class="close_alert_msg kir"><fmt:message key="common.msgs.dialog.button.close.label"></fmt:message></a>
			</div>
			<div id="customerIDErrorMsg" class="alert_msg alert_yield_msg row" style="display:none;">
				<p class="col-lg-10 col-md-10 col-sm-10 col-xs-10"><fmt:message key="lines.mgmt.error.national.id.empty"></fmt:message></p>
				<ul class="prompt_controls floatr" style="width:auto">
					<li><a style="cursor:pointer;" onclick="dojo.byId('customerIDErrorMsg').setAttribute('style','display:none');" class="cta_prompt transition"><fmt:message key="common.msgs.dialog.button.ok.label"></fmt:message></a></li>
				</ul>
				<a style="cursor:pointer;" onclick="dojo.byId('customerIDErrorMsg').setAttribute('style','display:none');" class="close_alert_msg kir"><fmt:message key="common.msgs.dialog.button.close.label"></fmt:message></a>
			</div>
            <div class="row stacked">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<p><fmt_rt:message key="lines.mgmt.label.mobily.phone.number" /></p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 field_items star_fix">
					<input id="mobile_number" name="MobileNumber" type="text" class="text_input" 
					value="${portletSessionScope.userSubscriptionVO.msisdn}" 
					data-dojo-type="dijit.form.ValidationTextBox" maxlength="15"
					data-dojo-props="regExp:'[0-9]{12,15}|^05[0-9]{8,8}', required:true, invalidMessage:'<fmt:message key='lines.mgmt.error.mobile.number.invalid'/>', missingMessage:'<fmt:message key='lines.mgmt.error.mobile.number.empty'/>'"/>
					
				</div>
			</div>
			<div class="row stacked">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<p><fmt_rt:message key="lines.mgmt.label.iqama.number" /></p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 field_items star_fix">
					<input id="iqama_number" name="NationalIdORIqamaNumber" type="text" class="text_input"  
					value="${portletSessionScope.userSubscriptionVO.nationalIdOrIqamaNumber}" data-dojo-type="dijit.form.ValidationTextBox"  maxlength="50"
					data-dojo-props="required:true, missingMessage:'<fmt:message key='lines.mgmt.error.national.id.empty'/>'"/>
			
				</div>
			</div>
			
			<div id="standByDiv" class="basic">
				<div class="row stacked">
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<input type="button" id="verifyNumberBtn" onclick="sendActivationCode();" value='<fmt:message key="lines.mgmt.button.verify.number" />' class="cta_bt gradient transition floatl" />
                        <div id="resendPinId" class="floatl" style="display: none; padding: 3px 15px;">
							<a style="cursor:pointer;" onclick="sendActivationCode();"><fmt_rt:message key="lines.mgmt.label.resend.pin.code" /></a>
						</div>
					</div>
				</div>
			</div>			
			
			<div class="row stacked" id="pinDisplay">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<p><fmt_rt:message key="lines.mgmt.label.enter.sms.pin" />:</p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 field_items star_fix">
					<input id="activation_code" name="ActivationCode" type="text" class="text_input"  
					data-dojo-type="dijit.form.ValidationTextBox" onblur="toggleErrorStyle(dijit.byId('activation_code'));"
  					data-dojo-props="required:false, missingMessage:'<fmt:message key='lines.mgmt.error.activation.code.empty'/>'" 
					maxlength="50" />
				
				</div>
			</div>
			<div class="row stacked">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
    			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<input type="button" id="addGSMLineBtn" onclick="addLineFormAndDisableButton('1');" value='<fmt:message key="lines.mgmt.button.submit" />' class="cta_bt_alt gradient transition" disabled/>
                    &nbsp;
                    <input type="button" id="addGSMLineCancelBtn" onclick="cancelAddLine('addLineMobileDivForm')" value="<fmt:message key="lines.mgmt.button.cancel" />" class="cta_bt gradient transition">
 				</div>
			</div>
            <!-- Mobile END -->
		</form>
		</div>
		
		<div id="dataSimDiv" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display:${dataSimDisplay}; ">
			<c:set var="display" value="none" />
			<c:if test="${not empty requestScope.errorMessagesMap}"><c:set var="display" value="block" /></c:if>
			
			<div id="error_divDataSim" style='display:${display}' class="row">
				<c:forEach var="error" items="${requestScope.errorMessagesMap}" varStatus="msgsCounter">
					<div id="errorMsgDataSim${msgsCounter.count}" class="alert_msg alert_yield_msg col-lg-10 col-md-10 col-sm-10 col-xs-10">
						<p><fmt:message key='${error.value}' /></p>
						<ul class="prompt_controls floatr" style="width:auto">
							<li><a style="cursor:pointer;" onclick="dojo.byId('errorMsgDataSim${msgsCounter.count}').setAttribute('style','display:none');" class="cta_prompt transition"><fmt:message key="common.msgs.dialog.button.ok.label"></fmt:message></a></li>

						</ul>
						<a style="cursor:pointer;" onclick="dojo.byId('errorMsgDataSim${msgsCounter.count}').setAttribute('style','display:none');" class="close_alert_msg kir"><fmt:message key="common.msgs.dialog.button.close.label"></fmt:message></a>
					</div>
				</c:forEach>
			</div>
			<form class="int_widget style_definer" action="<%=addNewLineForNonMobily%>" id="addLineDataSimDivForm" name="addLinePortletForm"  dojoType="dijit.form.Form" method="post" >	
				<script type="dojo/method" event="onSubmit">
					if(this.validate()) {
						return true;
					} else {
						return false;
					}			
				</script> 
            <!-- Data SIM Start-->
            
            <input type="hidden" id="subscriptionTypeValueDataSim" name="SubscriptionType" value="${portletSessionScope.userSubscriptionVO.subscriptionType}" />
            <div class="stacked">
			<h2 class="form_title account_title row" >
				<div class="col-lg-10 col-md-10 col-sm-7 col-xs-7"><fmt:message key="lines.mgmt.button.add.data.sim.line" /></div> 
			</h2>					
			</div>     
            <div class="row stacked">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<p><fmt_rt:message key="lines.mgmt.label.data.sim" /></p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 field_items star_fix">
					<input id="datasim_mobile_number" name="MobileNumber" type="text" class="text_input" 
					value="${portletSessionScope.userSubscriptionVO.msisdn}" onblur="toggleErrorStyle(dijit.byId('datasim_mobile_number'));"
					data-dojo-type="dijit.form.ValidationTextBox" maxlength="15"
					data-dojo-props="regExp:'[0-9]{12,15}', required:true, invalidMessage:'<fmt:message key='error.data.sim.invalid'/>', missingMessage:'<fmt:message key='error.data.sim.empty'/>'"/>
					
				</div>
			</div>
			<div class="row stacked">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<p><fmt_rt:message key="lines.mgmt.label.iqama.number" /></p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 field_items star_fix">
					<input id="datasim_iqama_number" name="NationalIdORIqamaNumber" type="text" class="text_input"  
					value="${portletSessionScope.userSubscriptionVO.nationalIdOrIqamaNumber}" data-dojo-type="dijit.form.ValidationTextBox"  maxlength="50"
					onblur="toggleErrorStyle(dijit.byId('datasim_iqama_number'));"
					data-dojo-props="required:true, missingMessage:'<fmt:message key='lines.mgmt.error.national.id.empty'/>'"/>
					
				</div>
			</div>
			<div class="row stacked">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
   				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<input type="button" id="addDataSimLineBtn" onclick="addLineFormAndDisableButton('2');" value='<fmt:message key="lines.mgmt.button.submit" />' class="cta_bt gradient transition"/>
                   	&nbsp;
                   	<input type="button" id="cancelDataSimLineBtn" onclick="cancelAddLine('addLineDataSimDivForm')" value="<fmt:message key="lines.mgmt.button.cancel" />" class="cta_bt gradient transition">
				</div>
			</div>

            <!-- Data SIM END -->
		</form>
		</div>		
		
		<div id="broadbandDiv" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display:${broadbandDisplay};">
			<c:set var="display" value="none" />
			<c:if test="${not empty requestScope.errorMessagesMap}"><c:set var="display" value="block" /></c:if>
			<div id="error_divBroadband" style='display:${display}' class="row">
				<c:forEach var="error" items="${requestScope.errorMessagesMap}" varStatus="msgsCounter">
					<div id="errorMsgBroad${msgsCounter.count}" class="alert_msg alert_yield_msg col-lg-10 col-md-10 col-sm-10 col-xs-10">
						<p><fmt:message key='${error.value}' /></p>
						<ul class="prompt_controls floatr" style="width:auto">
							<li><a style="cursor:pointer;" onclick="dojo.byId('errorMsgBroad${msgsCounter.count}').setAttribute('style','display:none');" class="cta_prompt transition"><fmt:message key="common.msgs.dialog.button.ok.label"></fmt:message></a></li>
						</ul>
						<a style="cursor:pointer;" onclick="dojo.byId('errorMsgBroad${msgsCounter.count}').setAttribute('style','display:none');" class="close_alert_msg kir"><fmt:message key="common.msgs.dialog.button.close.label"></fmt:message></a>
					</div>
				</c:forEach>
			</div>		
		<form class="int_widget style_definer" method="post" action="<%=addNewLineForNonMobily%>" id="addLineBroadbandDivForm" name="addLinePortletForm" dojoType="dijit.form.Form" >			
			<script type="dojo/method" event="onSubmit">
				if(this.validate()) {
						return true;
				} else {
					return false;
				}			
			</script> 

            <input type="hidden" id="subscriptionTypeValueBroadband" name="SubscriptionType" value="${portletSessionScope.userSubscriptionVO.subscriptionType}" />
            <!-- Broadband Start-->
            <div class="stacked">
				<h2 class="form_title account_title row">
					<div id="broadbandDivTitle" class="col-lg-10 col-md-10 col-sm-7 col-xs-7"><fmt:message key="lines.mgmt.button.add.fiber.line"/></div> 
					<div class="clear_floating"></div>
				</h2>					
			</div>
            <div class="row stacked">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<p><fmt_rt:message key="lines.mgmt.label.service.account.number" /></p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 field_items star_fix">
					<input id="service_account_no" name="ServiceAccountNumber" type="text" class="text_input"  
					value="${portletSessionScope.userSubscriptionVO.serviceAcctNumber}" onblur="toggleErrorStyle(dijit.byId('service_account_no'));"
					data-dojo-type="dijit.form.ValidationTextBox" maxlength="20"
  					data-dojo-props="required:true, missingMessage:'<fmt:message key='lines.mgmt.error.service.account.number.empty'/>'"/>
  					
				</div>
			</div>
			<div class="row stacked">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<p><fmt_rt:message key="lines.mgmt.label.iqama.number" /></p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 field_items star_fix">
					<input id="broadband_iqama_number" name="NationalIdORIqamaNumber" type="text" class="text_input"  
					value="${portletSessionScope.userSubscriptionVO.nationalIdOrIqamaNumber}" onblur="toggleErrorStyle(dijit.byId('broadband_iqama_number'));"
					data-dojo-type="dijit.form.ValidationTextBox" maxlength="50"
  					data-dojo-props="required:true, missingMessage:'<fmt:message key='lines.mgmt.error.national.id.empty'/>'"/>
  					
				</div>
		
			</div>
			<div class="row stacked">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
    			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<input type="button" id="addBroadbandLineBtn" onclick="addLineFormAndDisableButton('4');" value='<fmt:message key="lines.mgmt.button.submit" />' class="cta_bt gradient transition"/>
                    	&nbsp;
                    	<input type="button" id="cancelBroadbandLineBtn" onclick="cancelAddLine('addLineBroadbandDivForm')" value='<fmt:message key="lines.mgmt.button.cancel" />' class="cta_bt gradient transition">
				</div>
			</div>
			</form>
			</div>
			
            <!-- Broadband END -->

		</div>   
</section>


<script type="text/javascript">
  	function subscriptionTypeChanged(selectedSubType) {
  		console.log("selectedSubType="+selectedSubType);
  		dojo.byId("addMobileLine").setAttribute("class", "newBtn");
		dojo.byId("addDataSimLine").setAttribute("class", "newBtn");
		dojo.byId("addFibreLine").setAttribute("class", "newBtn");
  		dojo.byId("subscriptionTypeValue").value = selectedSubType;
  		console.log("subscriptionTypeValue="+dojo.byId("subscriptionTypeValue").value);
  		if(selectedSubType == 1) {
  			dojo.byId("addMobileLine").setAttribute("class", "newBtn active");
  		} else if(selectedSubType == 2) {
  			dojo.byId("addDataSimLine").setAttribute("class", "newBtn active");
  		} else {
  			dojo.byId("addFibreLine").setAttribute("class", "newBtn active");
  		}
  	}

  	function showSubscription() {

		dojo.setStyle(dojo.byId("support_pages"), "display", "none");
 		dojo.setStyle(dojo.byId("mobileDiv"), "display", "none");
 		dojo.setStyle(dojo.byId("dataSimDiv"), "display", "none");
 		dojo.setStyle(dojo.byId("broadbandDiv"), "display", "none");
 		
 		var selectedSubType = dojo.byId("subscriptionTypeValue").value;
 		console.log("selectedSubType="+selectedSubType);
  		if(selectedSubType == 1) {

  			dojo.byId("subscriptionTypeValueGSM").value = selectedSubType;
  			dojo.setStyle(dojo.byId("mobileDiv"), "display", "block");
  			clearGSMFields();
  		} else if(selectedSubType == 2) {
  			dojo.byId("subscriptionTypeValueDataSim").value = selectedSubType;
  			dojo.setStyle(dojo.byId("dataSimDiv"), "display", "block");
			clearDataSimFields();
  		}
  		else {
  			dojo.byId("subscriptionTypeValueBroadband").value = selectedSubType;
  			dojo.setStyle(dojo.byId("broadbandDiv"), "display", "block");
 			clearBroadbandFields();
  		}
  	}
  	
  	function clearBroadbandFields(){
  		dojo.byId("service_account_no").value = "";
  		dojo.byId("broadband_iqama_number").value = "";
  		dojo.setStyle("error_div","display","none");
  		dojo.setStyle("error_divDataSim","display","none");
  		dojo.setStyle("error_divBroadband","display","none");
  	}
  	
  	function clearDataSimFields(){
  		dojo.byId("datasim_mobile_number").value = "";
  		dojo.byId("datasim_iqama_number").value = "";
  		dojo.setStyle("error_divBroadband","display","none");
  		dojo.setStyle("error_divDataSim","display","none");
  		dojo.setStyle("error_div","display","none");
  	}
  	
  	function clearGSMFields(){
  		dojo.byId("mobile_number").value = "";
  		dojo.byId("iqama_number").value = "";
  		dojo.byId("activation_code").value = "";
  		dojo.setStyle("error_divBroadband","display","none");
  		dojo.setStyle("error_divDataSim","display","none");
  		dojo.setStyle("error_div","display","none");
  	}
  	
	function sendActivationCode(){
		var standby = new dojox.widget.Standby({target: "standByDiv", color: "white", zindex: "auto"});
        document.body.appendChild(standby.domNode);
        standby.startup();
        standby.show();
		
		dojo.setStyle(dojo.byId("activationCode_server_error_msg"), "display", "none");
		var mobileNumber = dojo.byId("mobile_number").value;
		var customerID = dojo.byId("iqama_number").value;
		var subTypeID = dojo.byId("subscriptionTypeValue").value;
		if (mobileNumber == null || mobileNumber.length == 0) {
			standby.hide();
   			dojo.setStyle("usernameErrorMsg", "display", "block");
   			dojo.byId("mobile_number").focus();
   		}
   		else if(customerID == null || customerID.length == 0){
   			standby.hide();
   			dojo.setStyle("customerIDErrorMsg", "display", "block");
   			dojo.setStyle("usernameErrorMsg", "display", "none");
   			dojo.byId("iqama_number").focus();
   		}
		else{
			dojo.byId("verifyNumberBtn").setAttribute("class", "cta_bt_alt gradient floatl transition");
			dojo.byId("verifyNumberBtn").disabled=true;
			dojo.setStyle(dojo.byId("resendPinId"), "display", "none");
			
			dojo.setStyle("usernameErrorMsg", "display", "none");
			dojo.setStyle("customerIDErrorMsg", "display", "none");
		   	dojo.xhrPost({
		       url:  '<portlet:resourceURL id="SendActivationPin"></portlet:resourceURL>',
		       content : {'MobileNumber': mobileNumber, 'NationalIdORIqamaNumber':customerID, 'SubscriptionType':subTypeID },
		       load: function(data, ioArgs){
		       		if(data != 'genericError' && data != '-1' && data != 'defaultExisted'){
						dojo.setStyle(dojo.byId("resendPinId"), "display", "block");
						dojo.byId("addGSMLineBtn").setAttribute("class", "cta_bt gradient transition");
						dojo.byId("addGSMLineBtn").disabled=false;
					}
					else if(data=='-1'){
						dojo.byId("verifyNumberBtn").setAttribute("class", "cta_bt gradient floatl transition");
						dojo.byId("verifyNumberBtn").disabled=false;
						dojo.byId('pinCodeErrorMsgText').innerHTML = '<fmt:message key="lines.mgmt.error.subscriptionType.invalid" />';
		    			dojo.setStyle(dojo.byId("activationCode_server_error_msg"), "display", "block");
					}
					else if(data=='defaultExisted'){
						dojo.byId("verifyNumberBtn").setAttribute("class", "cta_bt gradient floatl transition");
						dojo.byId("verifyNumberBtn").disabled=false;
						dojo.byId('pinCodeErrorMsgText').innerHTML = '<fmt:message key="lines.mgmt.error.mobile.number.default.exists" />';
		    			dojo.setStyle(dojo.byId("activationCode_server_error_msg"), "display", "block");
					}
					else{
						dojo.byId("verifyNumberBtn").setAttribute("class", "cta_bt gradient floatl transition");
						dojo.byId("verifyNumberBtn").disabled=false;
						dojo.byId("pinCodeErrorMsgText").innerHTML = '<fmt:message key="lines.mgmt.error.activation.code.server" />';
						//dojo.setStyle(dojo.byId("getPinLink"), "display", "block");
		    			dojo.setStyle(dojo.byId("activationCode_server_error_msg"), "display", "block");
					}
		    		standby.hide();
	       		},
	       		error: function(data,ioArgs){
					dojo.byId("verifyNumberBtn").setAttribute("class", "cta_bt gradient floatl transition");
					dojo.byId("verifyNumberBtn").disabled=false;
	       			dojo.byId("pinCodeErrorMsgText").innerHTML = '<fmt:message key="lines.mgmt.error.activation.code.server" />';
		    		dojo.setStyle(dojo.byId("activationCode_server_error_msg"), "display", "block");
		    		standby.hide();
	       		}
	   		});
   		}
  	}


	function cancelAddLine(formId) {
	
		dojo.byId(formId).action = '<%=cancelSubs%>';
		dojo.byId(formId).submit();
	}

	function addLineFormAndDisableButton(subscriptionType){
	
		console.log("submitAddLineFormAndDisableButton called > subscriptionType::"+subscriptionType);
	
		if(subscriptionType == 1){
		
			if(dijit.byId('addLineMobileDivForm').validate()){
				dojo.byId('addGSMLineBtn').disabled=true;
				dojo.byId("addGSMLineBtn").setAttribute("class", "cta_bt_alt gradient floatl transition");
				dojo.byId('addLineMobileDivForm').action = '<%=addNewLineForNonMobily%>';
				console.log("form="+dojo.byId('addLineMobileDivForm'));
				dojo.byId('addLineMobileDivForm').submit();
				
				
			} else
				return false;
		}else if(subscriptionType == 2){
			if(dijit.byId('addLineDataSimDivForm').validate()){
				dojo.byId('addDataSimLineBtn').disabled=true;
				dojo.byId("addDataSimLineBtn").setAttribute("class", "cta_bt_alt gradient floatl transition");
				dojo.byId('addLineDataSimDivForm').action = '<%=addNewLineForNonMobily%>';
				dojo.byId('addLineDataSimDivForm').submit();
	
			} else
				return false;
		}else{
			if(dijit.byId('addLineBroadbandDivForm').validate()){
				dojo.byId('addBroadbandLineBtn').disabled=true;
				dojo.byId("addBroadbandLineBtn").setAttribute("class", "cta_bt_alt gradient floatl transition");
				dojo.byId('addLineBroadbandDivForm').action = '<%=addNewLineForNonMobily%>';
				dojo.byId('addLineBroadbandDivForm').submit();
			} else
				return false;
		}
	};
</script>

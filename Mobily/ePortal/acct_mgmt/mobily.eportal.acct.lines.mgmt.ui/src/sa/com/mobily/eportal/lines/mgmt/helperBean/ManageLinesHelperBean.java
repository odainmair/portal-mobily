package sa.com.mobily.eportal.lines.mgmt.helperBean;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import javax.portlet.PortletRequest;

import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.cache.util.CacheManagerServiceUtil;
import sa.com.mobily.eportal.cacheinstance.valueobjects.SessionVO;
import sa.com.mobily.eportal.common.service.model.UserSubscription;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.util.puma.PumaUtil;
import sa.com.mobily.eportal.customerinfo.constant.SubscriptionTypeConstant;
import sa.com.mobily.eportal.lines.mgmt.utility.ManageLinesConstants;

public class ManageLinesHelperBean
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(AcctMgmtLoggerConstantsIfc.MANAGE_SUBSCRIPTIONS_SERVICE_LOGGER_NAME);
	private static final String className = ManageLinesHelperBean.class.getName();

	/**
	 * Utility methods that calls cache manager to get session vo, if refresh is
	 * true then cache will be rebuild
	 * 
	 * @param request
	 * @param refresh
	 * @return
	 */
	public SessionVO getSessionVO(PortletRequest request, boolean refresh)
	{
		String method = "getSessionVO";
		SessionVO sessionVO = null;
		executionContext.log(Level.INFO, "refresh value " + refresh, className, method, null);
		try
		{
			if (refresh)
			{
				executionContext.log(Level.INFO, "enter clear cache", className, method, null);
				
				executionContext.log(Level.INFO, "Will refresh cache", className, method, null);
				PumaUtil pumaUtil = new PumaUtil();
				String userName = pumaUtil.getUid();
				executionContext.log(Level.INFO, "Username=" + userName, className, method, null);
				CacheManagerServiceUtil.removeCachedData(request.getPortletSession().getId(), userName);
				executionContext.log(Level.INFO, "Removed cache for the Username=" + userName, className, method, null);
				sessionVO = CacheManagerServiceUtil.getCachedData(request.getPortletSession().getId(), userName);
				executionContext.log(Level.INFO, "sessionVO=" + sessionVO, className, method, null);
			}
			else if (null == request.getAttribute("sessionVO"))
			{
				executionContext.log(Level.INFO, "case1=" + sessionVO, className, method, null);
				PumaUtil pumaUtil = new PumaUtil();
				String userName = pumaUtil.getUid();
				executionContext.log(Level.INFO, "case1 > userName=" + userName, className, method, null);
				sessionVO = CacheManagerServiceUtil.getCachedData(request.getPortletSession().getId(), userName);
				executionContext.log(Level.INFO, "case1 > sessionVO=" + sessionVO, className, method, null);
			}
			else
			{
				executionContext.log(Level.INFO, "case2=" + sessionVO, className, method, null);
				sessionVO = (SessionVO) request.getAttribute("sessionVO");
				executionContext.log(Level.INFO, "case2 > sessionVO=" + sessionVO, className, method, null);
			}
			
			if(sessionVO != null) {
				executionContext.log(Level.INFO, "new session vo data=" + sessionVO.getMsisdn(), className, method, null);
			}
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception while getting SessionVO=" + e.getMessage(), className, method, e);
		}
		return sessionVO;
	}

	
	public Map<String, String> validateUserGSMSubscription(UserSubscription userSubscription)
	{
		Map<String, String> errorMessagesMap = new HashMap<String, String>();
		if(null == userSubscription.getMsisdn() || userSubscription.getMsisdn().length() == 0) {
			
			if(SubscriptionTypeConstant.SUBS_TYPE_CONNECT == userSubscription.getSubscriptionType()) {
				errorMessagesMap.put("msisdnisEmpty", "error.data.sim.empty");
			} else {
				errorMessagesMap.put("msisdnisEmpty", "lines.mgmt.error.mobile.number.empty");
			}
			
			
		} else if(!userSubscription.getMsisdn().matches(ManageLinesConstants.MSISDN_REG_EXP)) {
			
			if(SubscriptionTypeConstant.SUBS_TYPE_CONNECT == userSubscription.getSubscriptionType()) {
				errorMessagesMap.put("msisdnisInvalid", "error.data.sim.invalid");
			} else {
				errorMessagesMap.put("msisdnisInvalid", "lines.mgmt.error.mobile.number.invalid");
			}
		}
		if(null == userSubscription.getNationalIdOrIqamaNumber() || userSubscription.getNationalIdOrIqamaNumber().length() == 0)
			errorMessagesMap.put("iqamaEmpty", "lines.mgmt.error.national.id.empty");
		
		/*		if(SubscriptionTypeConstant.SUBS_TYPE_GSM == userSubscription.getSubscriptionType()){
		if(null == userAccountVO.getResponseActivationCode() || userAccountVO.getResponseActivationCode().length() == 0)
			errorMessagesMap.put("codeEmpty", "lines.mgmt.error.activation.code.empty");
		else if(null == userAccountVO.getActivationCode() || userAccountVO.getActivationCode().length() == 0)
			errorMessagesMap.put("pinCodeInvalid", "lines.mgmt.error.activation.code.invalid");
		else if(!(userAccountVO.getActivationCode().equals(userAccountVO.getResponseActivationCode())))
			errorMessagesMap.put("pinCodeInvalid", "step3.error.activation.code.invalid");
	}*/
		return errorMessagesMap;
	}
	
	public Map<String, String> validateUserServiceSubscription(UserSubscription userSubscription)
	{
		Map<String, String> errorMessagesMap = new HashMap<String, String>();
		if(null == userSubscription.getServiceAcctNumber() || userSubscription.getServiceAcctNumber().length() == 0)
			errorMessagesMap.put("serviceAcctEmpty", "lines.mgmt.error.service.account.number.empty");
		else if(!userSubscription.getServiceAcctNumber().matches(ManageLinesConstants.SERVICE_ACCOUNT_REG_EXP))
			errorMessagesMap.put("msisdnisInvalid", "lines.mgmt.error.service.account.number.invalid");
		if(null == userSubscription.getNationalIdOrIqamaNumber() || userSubscription.getNationalIdOrIqamaNumber().length() == 0)
			errorMessagesMap.put("iqamaEmpty", "lines.mgmt.error.national.id.empty");

		return errorMessagesMap;
	}
}
package sa.com.mobily.eportal.lines.mgmt.portlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.ResourceServingPortlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;

import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.activenumber.service.ManageSubscriptionService;
import sa.com.mobily.eportal.activenumber.service.constants.ErrorCodeConstantsIfc;
import sa.com.mobily.eportal.activenumber.vo.ChangeDefaultSubInputVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.SessionVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.UserSubscriptionsVO;
import sa.com.mobily.eportal.common.beanlocator.BeanLocator;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.application.DataNotFoundException;
import sa.com.mobily.eportal.common.service.model.UserSubscription;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.util.puma.PumaUtil;
import sa.com.mobily.eportal.customerinfo.constant.SubscriptionTypeConstant;
import sa.com.mobily.eportal.lines.mgmt.helperBean.ManageLinesHelperBean;
import sa.com.mobily.eportal.lines.mgmt.utility.ManageLinesConstants;

import com.ibm.portal.navigation.NavigationNode;
import com.ibm.wps.model.ModelUtil;

/**
 * A sample portlet based on GenericPortlet
 */
public class LinesManagementPortlet extends GenericPortlet implements ResourceServingPortlet {

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(AcctMgmtLoggerConstantsIfc.MANAGE_SUBSCRIPTIONS_SERVICE_LOGGER_NAME);
	private ManageSubscriptionService manageSubscriptionService;
	private static final String CLASS_NAME = LinesManagementPortlet.class.getName();
	private ManageLinesHelperBean manageLinesHelperBean;
	 
	/**
	 * @see javax.portlet.Portlet#init()
	 */
	public void init() throws PortletException {
		super.init();
		manageSubscriptionService = (ManageSubscriptionService) BeanLocator.lookup("ejblocal:" + ManageSubscriptionService.class.getName());
		manageLinesHelperBean = new ManageLinesHelperBean();
	}

	/**
	 * Serve up the <code>view</code> mode.
	 * 
	 * @see javax.portlet.GenericPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	public void doView(RenderRequest request, RenderResponse response) throws PortletException, IOException {
		String method = "doView";
		executionContext.log(Level.INFO, "doView Called..........................................", CLASS_NAME, method, null);
		response.setContentType(request.getResponseContentType());
		/*String resourceURL = response.createResourceURL().toString();
		request.setAttribute("resourceURL", resourceURL);*/
		String jspName = null;
		
		String successMsg = (String)request.getPortletSession().getAttribute(ManageLinesConstants.ADD_LINE_SUCCESS_MSG);
		executionContext.log(Level.INFO, "successMsg123::" + successMsg, CLASS_NAME, method, null);
		
		String pageUniqueName = getContentNode(request);
		executionContext.log(Level.INFO, "pageUniqueName::" + pageUniqueName, CLASS_NAME, method, null);
		
		if(FormatterUtility.isNotEmpty(successMsg)) {
			request.setAttribute("Result", "add.sub.success.msg");
			request.setAttribute("resultClass", "alert_succes_msg");
			request.getPortletSession().removeAttribute(ManageLinesConstants.ADD_LINE_SUCCESS_MSG);
			request.getPortletSession().removeAttribute(ManageLinesConstants.ADD_LINE_USER_SUB_VO);
			
			jspName = ManageLinesConstants.SUCCESS_JSP;
			executionContext.log(Level.INFO, "jspName::" + jspName, CLASS_NAME, method, null);
			
			// Refresh Cache Manager Service with removing current entry
			SessionVO sessionVO = manageLinesHelperBean.getSessionVO(request, true);
			executionContext.log(Level.INFO, "sessionVO after refreshing::" + sessionVO, CLASS_NAME, method, null);
			request.setAttribute("sessionVO", sessionVO);

		} else {
			PumaUtil pumaUtil = new PumaUtil();
			
			@SuppressWarnings("unchecked")
			HashMap<String, String> errorMessagesMap = (HashMap<String, String>) request.getPortletSession().getAttribute(ManageLinesConstants.ERROR_MSGS_MAP_ID);
			request.setAttribute(ManageLinesConstants.ERROR_MSGS_MAP_ID, errorMessagesMap);
			request.getPortletSession().removeAttribute(ManageLinesConstants.ERROR_MSGS_MAP_ID);
			
			executionContext.log(Level.INFO, "User Id in doView : " + pumaUtil.getUid(), CLASS_NAME, method, null);
			//executionContext.log(Level.INFO, "Role Id in doView : " + pumaUtil.getRoleId(), CLASS_NAME, method, null);
			
			if("sa.com.mobily.eportal.hidden.page.dashboard.nonMobily".equalsIgnoreCase(pageUniqueName)) {
				
				UserSubscription userSubscription = (UserSubscription)request.getPortletSession().getAttribute(ManageLinesConstants.ADD_LINE_USER_SUB_VO);
				executionContext.log(Level.INFO, "userSubscription::" + userSubscription, CLASS_NAME, method, null);
				request.setAttribute(ManageLinesConstants.ADD_LINE_USER_SUB_VO, userSubscription);
				
				jspName = ManageLinesConstants.ADD_NEW_SUB_JSP;
			} else if("sa.com.mobily.eportal.hidden.page.dashboard.nonPrimary".equalsIgnoreCase(pageUniqueName)) {
				request.setAttribute("userName", pumaUtil.getUid());
				
				List<UserSubscriptionsVO> userSubscriptionsVoList = manageSubscriptionService.getUserRelatedNumberByUsername(pumaUtil.getUid());
				if (CollectionUtils.isNotEmpty(userSubscriptionsVoList))
				{
					executionContext.log(Level.INFO, "Subscription  size : " + userSubscriptionsVoList.size(), CLASS_NAME, method, null);
					request.setAttribute("allSubscriptions", userSubscriptionsVoList);
				}
				
				jspName = ManageLinesConstants.ADD_DEFAULT_LINE_JSP;
			} else {
				jspName = ManageLinesConstants.SUCCESS_JSP;
				request.setAttribute("Result", "common.generic.system.error");
				request.setAttribute("resultClass", "alert_yield_msg");
			}
		}
		
		executionContext.log(Level.INFO, "jspName: " + jspName, CLASS_NAME, method, null);
		
		// Invoke the JSP to render
		PortletRequestDispatcher rd = getPortletContext().getRequestDispatcher(jspName);
		rd.include(request,response);
	}

	@ProcessAction(name = "cancelSubs")
	public void processActionCancelSubs(ActionRequest request, ActionResponse response) throws PortletException, IOException
	{
		request.getPortletSession().removeAttribute(ManageLinesConstants.ADD_LINE_SUCCESS_MSG);
		request.getPortletSession().removeAttribute(ManageLinesConstants.ACTIVATION_CODE_BACKEND);
		request.getPortletSession().removeAttribute(ManageLinesConstants.ADD_LINE_USER_SUB_VO);
	}


	@ProcessAction(name = "addNewLineForNonMobily")
	public void addNewLineForNonMobily(ActionRequest request, ActionResponse response) throws PortletException, java.io.IOException{
		String method = "addNewLineForNonMobily";
		String activationCode = null;
		UserSubscription userSubscription = new UserSubscription();
		userSubscription.setMsisdn(request.getParameter(ManageLinesConstants.FIELD_MOBILE_NUMBER));
		userSubscription.setServiceAcctNumber(request.getParameter(ManageLinesConstants.FIELD_SERVICE_ACCOUNT_NUMBER));
		userSubscription.setNationalIdOrIqamaNumber(request.getParameter(ManageLinesConstants.FIELD_NATIONALID_OR_IQAMA_NUMBER));
		userSubscription.setSubscriptionType(Integer.parseInt(request.getParameter(ManageLinesConstants.FIELD_SUBSCRIPTION_TYPE)));
		//userSubscription.setSubscriptionName(SubscriptionTypeConstant.getSubscriptionName(userSubscription.getSubscriptionType()));
		
		//request.getPortletSession().setAttribute(ManageLinesConstants.HANDLE_TYPE, "nonMobily");
		request.getPortletSession().setAttribute(ManageLinesConstants.ADD_LINE_USER_SUB_VO, userSubscription);
		executionContext.audit(Level.INFO, "SUB VO FOR NON MOBILY " + userSubscription, CLASS_NAME, method);
		Map<String,String> errorMessagesMap = new HashMap<String, String>();
		try{
			
			if(SubscriptionTypeConstant.SUBS_TYPE_GSM == userSubscription.getSubscriptionType() || SubscriptionTypeConstant.SUBS_TYPE_CONNECT == userSubscription.getSubscriptionType())
				errorMessagesMap = manageLinesHelperBean.validateUserGSMSubscription(userSubscription);
			else
				errorMessagesMap = manageLinesHelperBean.validateUserServiceSubscription(userSubscription);
			
			if(SubscriptionTypeConstant.SUBS_TYPE_GSM == userSubscription.getSubscriptionType() ) {
				activationCode = request.getParameter(ManageLinesConstants.FIELD_ACTIVATION_CODE);
				
				if(null == request.getPortletSession().getAttribute(ManageLinesConstants.ACTIVATION_CODE_BACKEND))
					errorMessagesMap.put("codeEmpty", "lines.mgmt.error.activation.code.empty");
				else if(null == activationCode || activationCode.length() == 0)
					errorMessagesMap.put("pinCodeInvalid", "lines.mgmt.error.activation.code.invalid");
				else if(!(activationCode.equals(request.getPortletSession().getAttribute(ManageLinesConstants.ACTIVATION_CODE_BACKEND))))
					errorMessagesMap.put("pinCodeInvalid", "lines.mgmt.error.activation.code.invalid");

			}
			
			if(errorMessagesMap.size()>0)
				request.getPortletSession().setAttribute(ManageLinesConstants.ERROR_MSGS_MAP_ID, errorMessagesMap);
			else
			{
				manageSubscriptionService.handleNonMobilyUserSubscription(request.getRemoteUser(), userSubscription, activationCode);
				
				executionContext.audit(Level.INFO, "User subscription added Successfully................." + userSubscription, CLASS_NAME, method);
				
				request.getPortletSession().setAttribute(ManageLinesConstants.ADD_LINE_SUCCESS_MSG, "add.sub.success.msg");
				request.getPortletSession().removeAttribute(ManageLinesConstants.ADD_LINE_USER_SUB_VO);
				
			}
			
		} catch (DataNotFoundException e) {
			errorMessagesMap.put(ManageLinesConstants.GENERIC_ERROR, "add.sub.error.no.sub.found");
			request.getPortletSession().setAttribute(ManageLinesConstants.ERROR_MSGS_MAP_ID, errorMessagesMap);
			executionContext.log(Level.SEVERE, "Error Occurred " + e.getMessage() , CLASS_NAME, method, e);
			request.setAttribute("exceptionOccurred", "YES");
			
		} catch (BackEndException e) {
			if(ErrorCodeConstantsIfc.SUB_VO_NOT_VALID.equals(e.getErrorCode()))
				errorMessagesMap.put(ManageLinesConstants.GENERIC_ERROR, "add.sub.error.invalid.data");
			else if(ErrorCodeConstantsIfc.DEFAULT_SUBS_EXISTS.equals(e.getErrorCode()))
				errorMessagesMap.put(ManageLinesConstants.GENERIC_ERROR, "add.sub.error.default.existed");
			else if(ErrorCodeConstantsIfc.PIN_CODE_INVALID.equals(e.getErrorCode()))
				errorMessagesMap.put(ManageLinesConstants.GENERIC_ERROR, "lines.mgmt.error.activation.code.invalid");
			else
				errorMessagesMap.put(ManageLinesConstants.GENERIC_ERROR, "common.generic.system.error");
			
			request.getPortletSession().setAttribute(ManageLinesConstants.ERROR_MSGS_MAP_ID, errorMessagesMap);
			
			//request.getPortletSession().setAttribute(ManageLinesConstants.MAIN_CONTENT_FLAG, "");
			executionContext.log(Level.SEVERE, "Error Occurred with code " + e.getErrorCode() + " and message is " + e.getMessage() , CLASS_NAME, method, e);
			request.setAttribute("exceptionOccurred", "YES");
		} catch (Exception e) {
			if(null != e && ErrorCodeConstantsIfc.PIN_CODE_INVALID.equals(e.getMessage()))
				errorMessagesMap.put(ManageLinesConstants.GENERIC_ERROR, "lines.mgmt.error.activation.code.invalid");
			else
				errorMessagesMap.put(ManageLinesConstants.GENERIC_ERROR, "common.generic.system.error");

			request.getPortletSession().setAttribute(ManageLinesConstants.ERROR_MSGS_MAP_ID, errorMessagesMap);
			
			//request.getPortletSession().setAttribute(ManageLinesConstants.MAIN_CONTENT_FLAG, "");
			executionContext.log(Level.SEVERE, "Error Occurred " + e.getMessage() , CLASS_NAME, method, e);
			request.setAttribute("exceptionOccurred", "YES");
		}
		// Refresh Cache Manager Service with removing current entry
		/*SessionVO sessionVO = manageLinesHelperBean.getSessionVO(request, true);
		executionContext.log(Level.INFO, "sessionVO after refreshing::" + sessionVO, CLASS_NAME, method, null);*/
		
		executionContext.audit(Level.INFO, "Lets go to doView11111111111...............", CLASS_NAME, method);
	}
	
	@ProcessAction(name = "addDefaultLine")
	public void addDefaultLine(ActionRequest request, ActionResponse response) throws PortletException, java.io.IOException {
		String method = "addDefaultLine";
		PumaUtil pumaUtil = new PumaUtil();
		String userName = pumaUtil.getUid();
		Map<String,String> errorMessagesMap = new HashMap<String, String>();
		String defaultNumber = request.getParameter("subscriptionNumberRadio");
		//SessionVO sessionVO = null;
		executionContext.log(Level.INFO, "userName is " + userName, CLASS_NAME, method, null);
		executionContext.log(Level.INFO, "ChangeDefaultNumber is " + defaultNumber, CLASS_NAME, method, null);
		
		try {

			//sessionVO = manageLinesHelperBean.getSessionVO(request, false);
			ChangeDefaultSubInputVO changeDefaultSubInputVO = new ChangeDefaultSubInputVO();
			executionContext.log(Level.INFO, "When filling the VO " + defaultNumber, CLASS_NAME, method, null);
			changeDefaultSubInputVO.setSelectedNumber(defaultNumber);
			changeDefaultSubInputVO.setUserAcctId(manageSubscriptionService.getUserInfoByUsername(userName).get(0).getUserAccountId());
			changeDefaultSubInputVO.setUserName(userName);
			
			manageSubscriptionService.changeDefaultNumber(changeDefaultSubInputVO);
			
			request.getPortletSession().setAttribute(ManageLinesConstants.ADD_LINE_SUCCESS_MSG, "add.sub.success.msg");
			
		} catch (BackEndException e) {
			executionContext.log(Level.SEVERE, "addDefaultLine > BackEndException > ErrorCode::"+e.getErrorCode(), CLASS_NAME, method, e);
			//request.getPortletSession().setAttribute(MobilyConstants.ERROR_MSG, e.getErrorCode());
			errorMessagesMap.put(ManageLinesConstants.GENERIC_ERROR, "add.sub.error.default.existed");
			request.getPortletSession().setAttribute(ManageLinesConstants.ERROR_MSGS_MAP_ID, errorMessagesMap);

		} catch(Exception e){
			//request.getPortletSession().setAttribute(MobilyConstants.ERROR_MSG, ErrorCodeConstantsIfc.DEFAULT_SUBS_EXISTS+"1");
			errorMessagesMap.put(ManageLinesConstants.GENERIC_ERROR, "common.generic.system.error");
			request.getPortletSession().setAttribute(ManageLinesConstants.ERROR_MSGS_MAP_ID, errorMessagesMap);
		}
		//sessionVO = manageLinesHelperBean.getSessionVO(request, true);
	}

	@Override
	public void serveResource(ResourceRequest request, ResourceResponse response) throws PortletException, IOException
	{
		String method = "serveResource";
		request.setAttribute("portlet-class", CLASS_NAME);
		request.setAttribute("portlet-method", method);
		response.setCharacterEncoding("UTF-8");
		String resourceID = request.getResourceID();
		
		if (resourceID.equals(ManageLinesConstants.SEND_ACTIVATION_PIN))
		{
			UserSubscription userSubscription = new UserSubscription();
			userSubscription.setNationalIdOrIqamaNumber(request.getParameter(ManageLinesConstants.FIELD_NATIONALID_OR_IQAMA_NUMBER));
			userSubscription.setMsisdn(request.getParameter(ManageLinesConstants.FIELD_MOBILE_NUMBER));
			userSubscription.setSubscriptionType(Integer.parseInt(request.getParameter(ManageLinesConstants.FIELD_SUBSCRIPTION_TYPE)));
			String activationCode = null;
			try {
				executionContext.audit(Level.SEVERE, "User Sub VO " + userSubscription , CLASS_NAME, method);
				
				activationCode = manageSubscriptionService.sendPinCodeForNonMobily(userSubscription, request.getLocale().getLanguage());
				executionContext.audit(Level.SEVERE, "User Sub VO " + userSubscription , CLASS_NAME, method);
				
				request.getPortletSession().setAttribute(ManageLinesConstants.ACTIVATION_CODE_BACKEND, activationCode);
				
				executionContext.audit(Level.INFO, "Activation Code for adding new non mobily subscription " + activationCode, CLASS_NAME, method);
				response.getWriter().write("success");
				
			} catch(DataNotFoundException e){
				response.getWriter().write("-1");
			} catch(BackEndException e) {
				if(ErrorCodeConstantsIfc.DEFAULT_SUBS_EXISTS.equals(e.getErrorCode()))
					response.getWriter().write("defaultExisted");
				else
					response.getWriter().write("genericError");
				executionContext.log(Level.SEVERE, "Backend Exception occurred " + e.getErrorCode(), CLASS_NAME, method, e);
				request.setAttribute("exceptionOccurred", "YES");
			} catch(Exception e) {
				response.getWriter().write("genericError");
				executionContext.log(Level.SEVERE, "Backend Exception occurred " + e.getMessage(), CLASS_NAME, method, e);
				request.setAttribute("exceptionOccurred", "YES");
			}
		} else {
			response.getWriter().write("");
		}
	}
	
	public String getContentNode(RenderRequest request) {

		try {

			HttpServletRequest httpRequest = (HttpServletRequest) request.getAttribute("javax.portlet.request");

			HttpServletResponse httpResponse = (HttpServletResponse) request.getAttribute("javax.portlet.response");

			return getContentNode(httpRequest, httpResponse);

		} catch (Exception e) {
			return null;
		}
	}

	public String getContentNode(HttpServletRequest request,
			HttpServletResponse response) {

		try {

			ModelUtil util = ModelUtil.from(request, response);

			NavigationNode node = (NavigationNode) util.getNavigationSelectionModel().getSelectedNode();

			String uniquename = node.getContentNode().getObjectID().getUniqueName();

			executionContext.audit(Level.INFO, "Requested page unique name[" + uniquename + "]", CLASS_NAME, "getContentNode");

			return (uniquename);

		} catch (Exception e) {
			return null;
		}
	}
	
}

package sa.com.mobily.eportal.acct.mgmt.cards.constants;

public interface ConstantsIfc {
	public static final String REQUEST_QUEUE_NAME = "msisdn.count.request.queue.name";
	public static final String REPLY_QUEUE_NAME = "msisdn.count.reply.queue.name";
	
	public static final String RENDER_MODE_VIEW = "view";
	public static final String ERROR_CODE_EMPTY_REPLY_XML = "9999";
}

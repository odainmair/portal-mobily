/**
 * 
 */
package sa.com.mobily.eportal.acct.mgmt.cards.constants;

/**
 * @author Umer Rasheed
 *
 */
public interface TagIfc {

	
	
	public static final String TAG_CR_REQUEST_MESSAGE = "CR_REQUEST_MESSAGE";
	public static final String TAG_CR_HEADER = "CR_HEADER";
	public static final String TAG_FUNC_ID = "FuncId";
	public static final String TAG_MSG_VERSION = "MsgVersion";
	public static final String TAG_REQUESTOR_CHANNEL_ID = "RequestorChannelId";
	public static final String TAG_REQUESTOR_SR_ID = "RequestorSrId";
	public static final String TAG_REQUESTOR_USER_ID = "RequestorUserId";
	public static final String TAG_REQUESTOR_LANGUAGE = "RequestorLanguage";
	public static final String TAG_INQUIRY_TYPE = "InquiryType";
	
	public static final String TAG_PARAMS = "Params";
	public static final String TAG_PARAM = "Param";
	public static final String TAG_KEY = "Key";
	public static final String TAG_VALUE = "Value";
	
	public static final String INQ_FUNC_ID = "ADV_GENERAL_INQUIRY";
	public static final String INQ_MSG_VERSION = "0000";
	public static final String INQ_REQUESTOR_CHANNEL_ID = "BSL";
	public static final String INQ_REQUESTOR_SR_ID = "414d51204545415";
	public static final String INQ_REQUESTOR_USER_ID = "PF_28920";
	public static final String INQ_REQUESTOR_LANGUAGE_ENGLISH = "E";
	public static final String INQ_REQUESTOR_LANGUAGE_ARABIC = "A";
	public static final String INQ_KEY = "MSISDN";
	
	public static final String INQ_INQUIRY_TYPE = "LINES_ASSOCIATED_WITH_MSISDN";
	
	/***
	<CR_REQUEST_MESSAGE>
		<CR_HEADER>
			<FuncId>ADV_GENERAL_INQUIRY</FuncId>
			<MsgVersion>0000</MsgVersion>
			<RequestorChannelId>BSL</RequestorChannelId>
			<RequestorSrId>414d51204545415</RequestorSrId>
			<RequestorUserId>PF_28920</RequestorUserId>
			<RequestorLanguage>E</RequestorLanguage>
		</CR_HEADER>
		<InquiryType>LINES_ASSOCIATED_WITH_MSISDN</InquiryType>
		<Params>
			<Param>
				<Key>MSISDN</Key>
				<Value>966540511322</Value>
			</Param>
		</Params>
	</CR_REQUEST_MESSAGE>

		 */
	
	
	public static final String TAG_CR_REPLY_MESSAGE = "CR_REPLY_MESSAGE";
	public static final String TAG_ERROR_CODE = "ErrorCode";
	public static final String TAG_ERROR_MESSAGE = "ErrorMessage";
	public static final String TAG_SECTIONS = "Sections";
	public static final String TAG_SECTION = "Section";
	public static final String ATTR_NAME = "Name";
	public static final String ATTR_NAME_VALUE_LINES_ASSOCIATED_WITH_MSISDN = "LINES_ASSOCIATED_WITH_MSISDN";
	public static final String TAG_RESULTS = "Results";
	public static final String TAG_RESULT_SET = "ResultSet";
	public static final String TAG_RESULT = "Result";
	public static final String TAG_MSISDN = "MSISDN";
	public static final String TAG_NO_OF_LINES = "NO_OF_LINES";
	
	/***
	 * 
	 * <CR_REPLY_MESSAGE>
	<CR_HEADER>
		<FuncId>ADV_GENERAL_INQUIRY</FuncId>
		<MsgVersion>0000</MsgVersion>
		<RequestorChannelId>BSL</RequestorChannelId>
		<RequestorSrId>414d51204545415</RequestorSrId>
		<RequestorUserId>PF_28920</RequestorUserId>
		<RequestorLanguage>E</RequestorLanguage>
	</CR_HEADER>
	<ErrorCode>0</ErrorCode>
	<ErrorMessage>SUCCESS</ErrorMessage>
	<Sections>
		<Section Name="LINES_ASSOCIATED_WITH_MSISDN">
			<Results>
				<ResultSet>
					<Result>
						<Key>NO_OF_LINES</Key>
						<Value>1</Value>
					</Result>
				</ResultSet>
			</Results>
		</Section>
	</Sections>
</CR_REPLY_MESSAGE>

	 */
	
	public static final String FUNC_ID_ADV_GENERAL_INQ = "ADV_GENERAL_INQ";
	public static final String INQ_SECURITY_KEY = "123";
	public static final String INQ_REQUESTOR_CHANNEL_ID_EPORTAL = "ePortal";
	public static final String DATE_FORMAT = "yyyyMMddHHmmss";
	public static final String CHANNEL_TRANSACTION_ID = "SR";
	
}

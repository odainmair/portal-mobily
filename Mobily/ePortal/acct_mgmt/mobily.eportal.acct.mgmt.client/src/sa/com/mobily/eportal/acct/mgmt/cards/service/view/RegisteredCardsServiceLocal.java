package sa.com.mobily.eportal.acct.mgmt.cards.service.view;

import sa.com.mobily.eportal.acct.mgmt.cards.valueobjects.RegisteredCardsVO;

public interface RegisteredCardsServiceLocal {

	RegisteredCardsVO getCardNumber(String msisdn);

	

}

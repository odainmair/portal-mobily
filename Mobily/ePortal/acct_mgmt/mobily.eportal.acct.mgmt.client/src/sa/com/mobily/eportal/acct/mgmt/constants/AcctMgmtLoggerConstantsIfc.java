package sa.com.mobily.eportal.acct.mgmt.constants;

public interface AcctMgmtLoggerConstantsIfc {

	public static String BLOCK_LINE_SERVICE_LOGGER_NAME = "acctMgmt/blockline";
	public static String MY_CONTACTS_SERVICE_LOGGER_NAME = "acctMgmt/mycontacts";
	public static String MANAGE_SUBSCRIPTIONS_SERVICE_LOGGER_NAME = "acctMgmt/manageSubscriptions";
	public static String CARDS_SERVICE_LOGGER_NAME = "acctMgmt/cards";
	public static String ACCT_MGMT_CARDS_UI = "acctMgmt/cardsUI";
	public static String ACCT_MGMT_LOGIN_HISTORY = "acctMgmt/loginHistory";
	public static String ACCT_MGMT_AUTHORITY_MGMT = "acctMgmt/authMgmt";
	public static String ACCT_MGMT_PACKAGE_MGMT = "acctMgmt/packageMgmt";
}

package sa.com.mobily.eportal.activenumber.service.view;

import java.util.List;

import javax.validation.constraints.NotNull;

import sa.com.mobily.eportal.activenumber.vo.ChangeDefaultSubInputVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.SessionVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.UserAccountVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.UserSubscriptionsVO;

import sa.com.mobily.eportal.common.service.model.UserSubscription;

public interface ManageSubscriptionServiceRemote
{
	void changeDefaultNumber(ChangeDefaultSubInputVO changeDefaultSubInputVO);

	void addSubscription(long userAcctId, List<sa.com.mobily.eportal.common.service.model.UserSubscription> userSubscriptionList);

	String sendPINCode(long userAcctId, String msisdn, String language);

	List<UserSubscription> getRelatedSubscriptions(String userName, SessionVO sessionVO);

	void addGSMNumber(long userAcctId, String msisdn, String language);
	
	void handleNonMobilyUserSubscription(String username,sa.com.mobily.eportal.common.service.model.UserSubscription userSubscription,String activationCode);
	
	String sendPinCodeForNonMobily(sa.com.mobily.eportal.common.service.model.UserSubscription userSubscription, String locale);
	
	List<UserSubscriptionsVO> getUserRelatedNumberByUsername(String userName);
	
	public void deleteUserSubscription(long subscriptionId);
	
	public void addNonMobilyUserSubscription(@NotNull String username,@NotNull UserSubscription userSubscription);

	public List<UserAccountVO> getUserInfoByUsername(String userName);
	
	public void convertConsumerUserIntoNonMobily(String uid, Long userAccountId);
	
	public void convertConsumerUserIntoNonDefault(String uid, Long userAccountId);

}
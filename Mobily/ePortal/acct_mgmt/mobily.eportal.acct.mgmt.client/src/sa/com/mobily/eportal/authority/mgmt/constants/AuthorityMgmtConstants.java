package sa.com.mobily.eportal.authority.mgmt.constants;

public interface AuthorityMgmtConstants
{

	public static final String CORP_LDAP_GROUP_NAME = "CAPUsers";
	
	public static final String STATUS_ACTIVE = "2";
	
	public static final String CUSTOMER_SEGMENT_BUSINESS = "2";
	
	public static final String REGISTRATION_SOURCE_ID = "1";
	
	public static final String ROLE_ID = "2";
	
	public static final String PREFERRED_LANGUAGE_EN = "en";
	
	public static final String CORPORATE_SUBSCRIPTION_USER_ACTIVATED = "1";
	
	public static final String CORPORATE_SUBSCRIPTION_STATUS_ACTIVE = "1";
	
	public static final String CORPORATE_SUBSCRIPTION_STATUS_INACTIVE = "0";
	
	public static final String CORPORATE_SUBSCRIPTION_SUB_ADMIN = "1";
	
	public static final String GENERIC_SMS_ID_AR = "824975";
	public static final String GENERIC_SMS_ID_EN = "826168";
	
	public static final String SUB_ADMIN_CREATE_EN_SMS_TEXT_1 ="sub.admin.creation.en.sms.text1";
	public static final String SUB_ADMIN_CREATE_EN_SMS_TEXT_2 ="sub.admin.creation.en.sms.text2";
	
	
	public static final String SUB_ADMIN_CREATE_AR_SMS_TEXT_1 ="sub.admin.creation.ar.sms.text1";
	public static final String SUB_ADMIN_CREATE_AR_SMS_TEXT_2 ="sub.admin.creation.ar.sms.text2";
	
	
	public static final String SUB_ADMIN_RESET_PWD_EN_SMS_TEXT_1 ="sub.admin.reset.password.en.sms.text1";
	public static final String SUB_ADMIN_RESET_PWD_EN_SMS_TEXT_2 ="sub.admin.reset.password.en.sms.text2";
	
	
	public static final String SUB_ADMIN_RESET_PWD_AR_SMS_TEXT_1 ="sub.admin.reset.password.ar.sms.text1";
	public static final String SUB_ADMIN_RESET_PWD_AR_SMS_TEXT_2 ="sub.admin.reset.password.ar.sms.text2";
	
	
	
}

package sa.com.mobily.eportal.authority.mgmt.service.view;

import java.util.List;

import sa.com.mobily.eportal.authority.mgmt.vo.CAPUserAccountVO;
import sa.com.mobily.eportal.authority.mgmt.vo.SubAdminVO;
import sa.com.mobily.eportal.capregistration.service.view.CAPRegistrationAsyncServiceRemote;

public interface AuthorityManagementServiceRemote
{

	public boolean createSubAdmin(SubAdminVO subAdminVO,CAPRegistrationAsyncServiceRemote capServiceRegistrationService );
	
	public List<CAPUserAccountVO> getSubAdminsList(String billingAccountNumber);
	
	public boolean disableSubAdminUser(String userAcctId,String billingAccountNumber);
	
	public boolean resetPasswordForSubAdminUser(String userAcctId,String billingAccountNumber,String language);
	
	public boolean checkMobileForDuplicate(String mobileNumber, String billingAccountNumber);
}

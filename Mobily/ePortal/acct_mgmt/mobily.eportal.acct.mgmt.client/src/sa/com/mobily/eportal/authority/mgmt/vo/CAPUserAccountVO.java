package sa.com.mobily.eportal.authority.mgmt.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class CAPUserAccountVO extends BaseVO
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String subscriptionId;
	
	private String userAcctId;
	
	
	private String capMobile;
	
	
	
	private String billingAccountNumber;
	
	private String status;
	
	private String isSubAdminUser;
	
	private String userName;
	
	private String name;
	

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getIsSubAdminUser()
	{
		return isSubAdminUser;
	}

	public void setIsSubAdminUser(String isSubAdminUser)
	{
		this.isSubAdminUser = isSubAdminUser;
	}

	
	
	
	public String getCapMobile() {
		return capMobile;
	}

	public void setCapMobile(String capMobile) {
		this.capMobile = capMobile;
	}

	
	public String getBillingAccountNumber() {
		return billingAccountNumber;
	}

	public void setBillingAccountNumber(String billingAccountNumber) {
		this.billingAccountNumber = billingAccountNumber;
	}

	

	
	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getUserAcctId() {
		return userAcctId;
	}

	public void setUserAcctId(String userAcctId) {
		this.userAcctId = userAcctId;
	}

}

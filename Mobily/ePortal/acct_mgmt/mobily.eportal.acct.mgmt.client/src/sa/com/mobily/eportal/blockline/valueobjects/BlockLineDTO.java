package sa.com.mobily.eportal.blockline.valueobjects;

public class BlockLineDTO
{
	private String errorCode;
	private String errorMessage;
	private String remainingInstallments;
	
	public String getErrorCode()
	{
		return errorCode;
	}
	public void setErrorCode(String errorCode)
	{
		this.errorCode = errorCode;
	}
	public String getErrorMessage()
	{
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}
	public String getRemainingInstallments()
	{
		return remainingInstallments;
	}
	public void setRemainingInstallments(String remainingInstallments)
	{
		this.remainingInstallments = remainingInstallments;
	}
}

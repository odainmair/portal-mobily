package sa.com.mobily.eportal.blockline.valueobjects;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class BlockLineResponseVO extends BaseVO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -208127352371148459L;
	
	
	
	
	private SR_HEADER_REPLY header;
	
	private long lineNumber;
	private String serviceRequestId;
	private String errorCode;
	private String errorMessage;
	private String remainingInstallments;
	
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public long getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(long lineNumber) {
		this.lineNumber = lineNumber;
	}
	public String getServiceRequestId() {
		return serviceRequestId;
	}
	public void setServiceRequestId(String serviceRequestId) {
		this.serviceRequestId = serviceRequestId;
	}
	public SR_HEADER_REPLY getHeader() {
		return header;
	}
	public void setHeader(SR_HEADER_REPLY header) {
		this.header = header;
	}
	public String getRemainingInstallments()
	{
		return remainingInstallments;
	}
	public void setRemainingInstallments(String remainingInstallments)
	{
		this.remainingInstallments = remainingInstallments;
	}
	
}

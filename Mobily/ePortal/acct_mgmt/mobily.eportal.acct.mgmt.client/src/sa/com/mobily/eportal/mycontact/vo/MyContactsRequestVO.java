package sa.com.mobily.eportal.mycontact.vo;

import java.util.List;

import sa.com.mobily.eportal.common.service.vo.FootPrintVO;

/**
 * This class contains the needed parameters for My Contacts Service operations
 * 
 */

public class MyContactsRequestVO extends FootPrintVO {
	
	private static final long serialVersionUID = 1L;
	
	private Long accountId;
	
	private List<Long> contactIds;
	
	private MyContactsVO contact;

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	
	public List<Long> getContactIds() {
		return contactIds;
	}

	public void setContactIds(List<Long> contactIds) {
		this.contactIds = contactIds;
	}

	public MyContactsVO getContact() {
		return contact;
	}

	public void setContact(MyContactsVO contact) {
		this.contact = contact;
	}
}
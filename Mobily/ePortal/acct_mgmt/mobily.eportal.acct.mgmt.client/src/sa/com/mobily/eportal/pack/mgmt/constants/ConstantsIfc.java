package sa.com.mobily.eportal.pack.mgmt.constants;

public interface ConstantsIfc {

	
	public static final String REPLY_QUEUE_PACKAGE_HISTORY = "mq.package.conversion.history.reply.queue";
	public static final String REQUEST_QUEUE_PACKAGE_HISTORY = "mq.package.conversion.history.request.queue";

	public static final String REPLY_QUEUE_PACKAGE_CONVERSION = "mq.package.conversion.reply.queue";
	public static final String REQUEST_QUEUE_PACKAGE_CONVERSION = "mq.package.conversion.request.queue";

	public static final String REQUESTER_USER_ID_ANONYMOUS = "Anonymous";
	
	public static final String FUNC_ID_PKGCONV_HISTORY_INQUIRY = "PKGCONV_HISTORY_INQUIRY";
	public static final String FUNC_ID_PKGCONV_SMOOTH_MIGIRATION = "SMOOTH_MIGIRATION";
	
	public static final int STATUS_ACTIVE = 1;
	public static final String SECURITYKEY = "0000";
	public static final String MSGVERSION_0000 = "0000";
	public static final String MSGVERSION_00001 = "0001";
	public static final String MSGVERSION_1_0 = "1.0";
	public static final String REQUESTER_CHANNEL_ID = "ePortal";
	public static final String REQUESTOR_LANGUAGE = "E";
	public static final String CHARGABLE = "N";
	public static final String Y = "Y";
	public static final String N = "N";
	public static final String CHARGING_MODE = "Payment";
	public static final String CHARGING_AMOUNT = "0";
	public static final String CHANNELTRANSID = "SR";
	public static final String DEST_ID = "OM";
	public static final String DEST_FUNC_ID = "OrchestratorSyncAsync";
	public static final String MSG_TYPE_REQUEST = "REQUEST";
	public static final String CHANGE_TYPE = "ChangeType";
	public static final String PRODUCT_CATEGORY = "ProductCategory";
	public static final String PRODUCT_CATEGORY_VALUE = "1";
	public static final String ACTION_MODIFY = "Modify";
	public static final String ACTION_NO = "NoAction";
	public static final String ACTION_DELETE = "Delete";
	public static final String ACTION_ADD = "Add";
	public static final String ORDER_ID = "ORD_ID";
	
	public static final String PKG_CONV_SUCCESS = "I000000";
	
	/*	public static final String USERCOMMENT = "";
	public static final String NOTIFYBE = "Y";
	public static final String REASONID = "3";
	public static final String FINAL_REPLAY_QUEUE = "";
	public static final String FINAL_REPLAY_QUEUE_MANAGER = "";
	public static final String PARAMETER_NAME1 = "MSISDN";
	public static final String PARAMETER_NAME2 = "SIM_ID";
	public static final String PARAMETER_VALUE2 = "2";
	public static final String ALLOW_DUE_AMT_VALIDATION = "N";


	public static final String SUCCESS_MSG = "SUCCESS_MSG";
	public static final String SUCCESS = "SUCCESS";
	public static final String ERROR_MSG = "ERROR_MSG";
	public static final String ERROR_BLOCK_LINE_NUMBER_BLOCKED = "BL-001";
	//public static final String ERROR_CODE_REMAINING_INSTALLMENTS = "7371";
	public static final String ERROR_CODE_REMAINING_INSTALLMENTS_7371 = "7371";
	public static final String ERROR_CODE_REMAINING_INSTALLMENTS = "1852";*/
}
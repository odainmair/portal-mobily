/**
 * 
 */
package sa.com.mobily.eportal.pack.mgmt.service.view;

import sa.com.mobily.eportal.pack.mgmt.vo.PackageConversionReqVO;
import sa.com.mobily.eportal.pack.mgmt.vo.PackageHistoryRequestVO;
import sa.com.mobily.eportal.pack.mgmt.vo.PackageHistoryVO;

/**
 * @author n.gundluru.mit
 *
 */
public interface PackageMgmtServiceRemote {
	
	public PackageHistoryVO getPackageChangeHistory(PackageHistoryRequestVO requestVO);
	public void doPackageConversion(PackageConversionReqVO requestVO);
}

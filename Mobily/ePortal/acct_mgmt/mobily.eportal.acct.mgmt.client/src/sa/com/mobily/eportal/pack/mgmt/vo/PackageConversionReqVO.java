/**
 * 
 */
package sa.com.mobily.eportal.pack.mgmt.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author n.gundluru.mit
 *
 */
public class PackageConversionReqVO extends BaseVO
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String userId = "";
	private String msisdn = "";
	private String srcPkgId = "";
	private String destPkgId = "";
	private String srcPkgTechName = "";
	private String destPkgTechName = "";
	private String srcPkgPlanType = ""; //Prepaid" or "Postpaid
	private String destPkgPlanType = ""; //Prepaid" or "Postpaid
	private String changeType = "";
	private String orderId = "";
	
	private boolean isTesting = false;

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getMsisdn()
	{
		return msisdn;
	}

	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}

	public String getSrcPkgId()
	{
		return srcPkgId;
	}

	public void setSrcPkgId(String srcPkgId)
	{
		this.srcPkgId = srcPkgId;
	}

	public String getDestPkgId()
	{
		return destPkgId;
	}

	public void setDestPkgId(String destPkgId)
	{
		this.destPkgId = destPkgId;
	}

	public String getSrcPkgTechName()
	{
		return srcPkgTechName;
	}

	public void setSrcPkgTechName(String srcPkgTechName)
	{
		this.srcPkgTechName = srcPkgTechName;
	}

	public String getDestPkgTechName()
	{
		return destPkgTechName;
	}

	public void setDestPkgTechName(String destPkgTechName)
	{
		this.destPkgTechName = destPkgTechName;
	}

	public String getSrcPkgPlanType()
	{
		return srcPkgPlanType;
	}

	public void setSrcPkgPlanType(String srcPkgPlanType)
	{
		this.srcPkgPlanType = srcPkgPlanType;
	}

	public String getDestPkgPlanType()
	{
		return destPkgPlanType;
	}

	public void setDestPkgPlanType(String destPkgPlanType)
	{
		this.destPkgPlanType = destPkgPlanType;
	}

	public String getChangeType()
	{
		return changeType;
	}

	public void setChangeType(String changeType)
	{
		this.changeType = changeType;
	}

	public String getOrderId()
	{
		return orderId;
	}

	public void setOrderId(String orderId)
	{
		this.orderId = orderId;
	}

	public boolean isTesting()
	{
		return isTesting;
	}

	public void setTesting(boolean isTesting)
	{
		this.isTesting = isTesting;
	}
	
	
	
	
	
	
}

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/portal.tld" prefix="wps" %>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<portlet:defineObjects/>
<fmt:setBundle basename="mobily.eportal.acct.mgmt.corp.authority.mngt.nl.AuthorityManagementPortletResource" />


<portlet:actionURL var="addSubAdminURL">
	<portlet:param name="javax.portlet.action" value="addSubAdminAction" />
</portlet:actionURL>

<portlet:actionURL var="disableSubAdminURL">
	<portlet:param name="javax.portlet.action" value="disableSubAdminAction" />
</portlet:actionURL>

<portlet:actionURL var="resetPasswordURL">
	<portlet:param name="javax.portlet.action" value="resetPasswordAction" />
</portlet:actionURL>

<script>require(["dojo/parser", "dijit/form/NumberTextBox"]);</script>
<h2 class="account_title block clearfix">	
	<fmt:message key="auth.mgmt.header" />
</h2>
<c:set var="display" value="none" />
			<c:if test="${not empty requestScope.error}"><c:set var="display" value="inline-block" /></c:if>
		
		<div id="error_div" style='display:${display}' class="row">
            <div class="col-lg-9 col-md-9 col-sm-11 col-xs-11">
				<div id="errorMsg" class="alert_msg alert_yield_msg">
						<p>
							<fmt:message key='${requestScope.error}' />
						</p>
					<ul class="prompt_controls">
						<li><a style="cursor: pointer;"
							onclick="dojo.byId('errorMsg').setAttribute('style','display:none');"
							class="cta_prompt transition"><fmt:message
									key="auth.mgmt.msgs.dialog.button.ok.label"></fmt:message>
						</a>
						</li>
					</ul>
					<a style="cursor: pointer;"
						onclick="dojo.byId('errorMsg').setAttribute('style','display:none');"
						class="close_alert_msg kir"><fmt:message
							key="auth.mgmt.msgs.dialog.button.close.label"></fmt:message>
					</a>
				</div>
                </div>
			<br><br><br>
			</div>

		<c:set var="display1" value="none" />
			<c:if test="${not empty requestScope.success}"><c:set var="display1" value="inline-block" /></c:if>
		
		<div id="success_div" style='display:${display1}' class="row">
            <div class="col-lg-9 col-md-9 col-sm-11 col-xs-11">
				<div id="successMsg" class="alert_msg alert_yield_msg">
						<p>
							<fmt:message key='${requestScope.success}' />
						</p>
					<ul class="prompt_controls">
						<li><a style="cursor: pointer;"
							onclick="dojo.byId('successMsg').setAttribute('style','display:none');"
							class="cta_prompt transition"><fmt:message
									key="auth.mgmt.msgs.dialog.button.ok.label"></fmt:message>
						</a>
						</li>
					</ul>
					<a style="cursor: pointer;"
						onclick="dojo.byId('successMsg').setAttribute('style','display:none');"
						class="close_alert_msg kir"><fmt:message
							key="auth.mgmt.msgs.dialog.button.close.label"></fmt:message>
					</a>
				</div>
                </div>
			<br><br><br>
			</div>
<div style="width:635px;">
<form id="disableResetForm" method="post"  name="disableResetForm" data-dojo-id="disableResetForm"  dojoType="dijit.form.Form">
	<input type="hidden" name="userAccId" id="userAccId" value="">
<!--The boxes UI starts here-->
	<div class="GreyRdBoxes">
   	<c:if test="${not empty requestScope.subAdminsList }">
    	<c:forEach var="SubAdminVO" items="${requestScope.subAdminsList}" varStatus="cntObj" >
    			<div class="Rdbox">
            	<fmt:message key="auth.mgmt.label.name" />: ${SubAdminVO.name}<br>
                <fmt:message key="auth.mgmt.label.mobile.no" />: ${SubAdminVO.capMobile}<br>
                <fmt:message key="auth.mgmt.label.user.name" />: ${SubAdminVO.userName}<br>
                <fmt:message key="auth.mgmt.label.status" />:
                <c:choose>
					<c:when test="${not empty SubAdminVO.status and SubAdminVO.status eq '1'}">
						<fmt:message key="auth.mgmt.label.status.active" />
					</c:when>	
					<c:otherwise>
						<fmt:message key="auth.mgmt.label.status.inactive" />
					</c:otherwise>
				</c:choose>
                <p class="link_group">
                	<a onclick="resetPassword(${SubAdminVO.userAcctId})"><fmt:message key="auth.mgmt.label.reset.password" /></a>
                	<c:if test="${not empty SubAdminVO.status and SubAdminVO.status eq '1'}">
						<span>&nbsp;|&nbsp;</span>
						<a onclick="disableSubAdmin(${SubAdminVO.userAcctId})"><fmt:message key="auth.mgmt.label.delete" /></a>
					</c:if>	
                </p>
                
            </div>
		</c:forEach>
	</c:if>
		<div class="Rdbox add-admin">
            <br>
            	<span><img src="/MobilyResources/default/images/add_plus.png" width="66" height="66" alt="" onclick="showSubAdminForm()"/></span>
                 <p class="link_group"><!-- <a href="">Add Admin</a> --></p>
    	</div>
    	
	</div>
</form>
</div>
<!--The boxes UI ends here-->
<!--The form UI starts here-->
<form id="addSubAdmnForm" method="post" action="<%=addSubAdminURL%>" name="addSubAdmnForm" data-dojo-id="addSubAdmnForm"  dojoType="dijit.form.Form" >
	<script type="dojo/method" event="onSubmit">
		if(this.validate()) {
			return true;
		}else{
			return false;
		}
	</script>
    <div class="form_am" id="addAdminDiv" style="display: none;">
    	<dl class="info_box">
            <dt><fmt:message key="auth.mgmt.label.astrik"/><fmt:message key="auth.mgmt.label.name" /></dt>
            <dd><input type="text" class="text_input" id="name" name="name" value="${requestScope.name}" dojoType="dijit.form.ValidationTextBox" data-dojo-props="regExp:'^[A-Z a-z \u0600-\u06ff]{1,50}$',required:true, missingMessage:'<fmt:message key='auth.mgmt.messages.empty.name' />',invalidMessage:'<fmt:message key='auth.mgmt.messages.messages.invalid.name' />'" maxlength="20" /></dd>
        </dl>
        <dl class="info_box">
            <dt><fmt:message key="auth.mgmt.label.astrik"/><fmt:message key="auth.mgmt.label.mobile.no" /></dt>
            <dd><input type="text" class="text_input" id="mobileNo" name="mobileNo" value="${requestScope.mobileNumber}" dojoType="dijit.form.ValidationTextBox" data-dojo-props="regExp:'(\\+9665|009665|9665|05)\\d{8}', required:true, invalidMessage:'<fmt:message key='auth.mgmt.messages.invalid.mobile'/>', missingMessage:'<fmt:message key='auth.mgmt.messages.empty.mobile'/>'" maxlength="" /></dd>
        </dl>
        <dl class="info_box">
            <dt><fmt:message key="auth.mgmt.label.astrik"/><fmt:message key="auth.mgmt.label.user.name" /></dt>
            <dd><input type="text" class="text_input" id="userName" name="userName" autocomplete="off" value="${requestScope.userName}"  dojoType="dijit.form.ValidationTextBox" data-dojo-props="regExp:'^[0-9A-Za-z][A-Za-z0-9_.]{2,15}$', required:true, missingMessage:'<fmt:message key='auth.mgmt.messages.empty.username' />', invalidMessage:'<fmt:message key='auth.mgmt.messages.invalid.username' />'" maxlength="20" /></dd>
        </dl>
        <dl class="info_box">
            <dt><fmt:message key="auth.mgmt.label.astrik"/><fmt:message key="auth.mgmt.label.password" /></dt>
			<dd><p class="big_size_am italic_am darkGry_am"><fmt:message key="auth.mgmt.label.password.text1" /></p>
				<span class="sm_size_am italic_am ltGry_am"><fmt:message key="auth.mgmt.label.password.text2" /></span></dd>
        </dl>
        
        <div class="save_info_controls align_alt">						
			<ul><li><button class="cta_bt" dojoType="dijit.form.Button" type="submit"><fmt:message key="auth.mgmt.button.save"/></button></li>
			<li><button class="cta_bt" dojoType="dijit.form.Button" type="button" onclick="hideAddSubAdmnForm()"><fmt:message key="auth.mgmt.button.cancel"/></button></li></ul>
		</div>
    </div>
</form>
<!--The form UI ends here-->
</div>

<script>

var msg="${requestScope.addUserFailed}";
console.log("Message ::"+msg);
if(msg=="" || msg==null){
	console.log("msg is null");
}else{
	console.log("msg is not null");
	document.getElementById("addAdminDiv").setAttribute('style','display:');
}

function showSubAdminForm(){
	console.log("display add sub admin form");
	document.getElementById("name").value='';
	document.getElementById("mobileNo").value='';
	document.getElementById("userName").value='';
	document.getElementById("error_div").setAttribute('style','display:none');
	document.getElementById("success_div").setAttribute('style','display:none');
	document.getElementById("addAdminDiv").setAttribute('style','display:');
}

function hideAddSubAdmnForm(userAccId){
	console.log("hide add sub admin form");
	document.getElementById("name").value='';
	document.getElementById("mobileNo").value='';
	document.getElementById("userName").value='';
	document.getElementById("error_div").setAttribute('style','display:none');
	document.getElementById("success_div").setAttribute('style','display:none');
	document.getElementById("addAdminDiv").setAttribute('style','display:none');
}

function disableSubAdmin(userAccId){
	console.log("disableSubAdmin -> userAccId ::"+userAccId);
	document.getElementById("userAccId").value=userAccId;
	document.getElementById("error_div").setAttribute('style','display:none');
	document.getElementById("success_div").setAttribute('style','display:none');
	document.getElementById("addAdminDiv").setAttribute('style','display:none');
	document.getElementById("disableResetForm").action ="<%=disableSubAdminURL%>";
	document.getElementById("disableResetForm").submit();
}

function resetPassword(userAccId){
	console.log("resetPassword -> userAccId ::"+userAccId);
	document.getElementById("userAccId").value=userAccId;
	document.getElementById("error_div").setAttribute('style','display:none');
	document.getElementById("success_div").setAttribute('style','display:none');
	document.getElementById("addAdminDiv").setAttribute('style','display:none');
	document.getElementById("disableResetForm").action ="<%=resetPasswordURL%>";
	document.getElementById("disableResetForm").submit();
}

</script>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="sa.com.mobily.eportal.acct.mgmt.login.history.util.LoginHistoryContstants"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<portlet:defineObjects />

<fmt:setBundle basename="sa.com.mobily.eportal.acct.mgmt.login.history.portlet.nl.LoginHistoryPortletResource" />
					     
<style type="text/css">
	.loginHistorybasic {
		min-height: 800px;
		min-width: 400px;
		position: relative;
		top: 20%;
		z-index: auto;
		left: 0%;
	}
</style>


<script>
	dojo.require("dojo._base.xhr");
	dojo.require("custom.grid");
	dojo.require("dijit.registry");
</script>

<c:if test="${fn:contains(pageContext.request.locale.language,'en')}">
	<link rel="stylesheet" media="all" href="/MobilyResources/english/css/enhancedgrid.css">
</c:if>
<c:if test="${fn:contains(pageContext.request.locale.language,'ar')}">
	<link rel="stylesheet" media="all" href="/MobilyResources/arabic/css/enhancedgrid.css">
</c:if>


<div id="loginHistoryStandByDiv" class="loginHistorybasic">
	<div id="loginHistoryErrorContainer" class="alert_msg alert_yield_msg" style="display: none;">
		<p>
			<span id="loginHistoryErrorMsgContainer"><fmt:message key="login.history.generic.error" /></span>
		</p>
		<ul class="prompt_controls">
			<li><a href="javascript:<portlet:namespace/>HideDiv('loginHistoryErrorContainer');" class="cta_prompt transition">
					<fmt:message key="login.history.ok.label" /> 
				</a>
			</li>
		</ul>
		<a href="javascript:<portlet:namespace/>HideDiv('loginHistoryErrorContainer');" class="close_alert_msg kir">
			<fmt:message key="login.history.close.label" /> 
		</a>
	</div>
	<div id="loginHistoryNoOccasionFound" class="alert_msg alert_yield_msg" style="display: none;">
		<p>
			<span><fmt:message key="login.history.not.found" /></span>
		</p>
		<ul class="prompt_controls">
			<li><a href="javascript:<portlet:namespace/>HideDiv('loginHistoryNoOccasionFound');" class="cta_prompt transition">
					<fmt:message key="login.history.ok.label" /> 
				</a>
			</li>
		</ul>
		<a href="javascript:<portlet:namespace/>HideDiv('loginHistoryNoOccasionFound');" class="close_alert_msg kir">
			<fmt:message key="login.history.close.label" /> 
		</a>
	</div>


	<div id="account_user_content" style="float: left; width: 100%; padding: 0; background: none;">
		<div id="loginHistoryContainer"></div>
	</div>
</div>

<script type="text/javascript">

	require(['dojo/ready', 'custom/grid', 'dijit/registry','dojo/_base/xhr', 'dojox/widget/Standby','dojo/_base/declare','dojo/date/locale',
	         	'dojo/dom-style'], 
				function (ready, Grid, registry, xhr, Standby, declare, locale, style, query) {
                    ready(function () {

						window.loginHistoryStandBy = new Standby({
							target	: "loginHistoryStandByDiv",
							color 	: "white",
							zindex 	: "auto"
						});
						document.body.appendChild(window.loginHistoryStandBy.domNode);
						window.loginHistoryStandBy.startup();
                    	window.loginHistoryStandBy.show();
          				window.grid = new Grid({
                           container : 'loginHistoryContainer',
						   gridId : 'loginHistoryGrid',
                           layout : [
										[
											{'name': '<fmt:message key="login.history.date.lable"/>', 'field': 'loginHistoryDateTime', width: '200px',
												formatter: function(item){
														return dojo.date.locale.format(new Date(item), {datePattern: "MMM dd,yyyy hh:mm:ss aa", selector: "date"});
												}
											},
											{'name': '<fmt:message key="login.history.channel.label"/>', 'field': 'loginChannel', width: '200px', 
															formatter: function(item){
																if(item == '<%=LoginHistoryContstants.LOGIN_CHANNEL_WEBSITE%>')
																	return '<fmt:message key="login.history.channel.EPRTL"/>';
																else if(item == '<%=LoginHistoryContstants.LOGIN_CHANNEL_APP%>')
																	return '<fmt:message key="login.history.channel.APPS"/>';
																else
																	return '';
															}
											},
											{'name': '<fmt:message key="login.history.status.label"/>', 'field': 'loginStatus', width: '200px',
															formatter: function(item){
																if(item == <%=LoginHistoryContstants.LOGIN_STATUS_SUCCESS%>)
																	return '<fmt:message key="login.history.status.0"/>';
																else
																	return '<fmt:message key="login.history.status.1"/>';
															}
											}
										]
									],
                            useXhr : true,
                            xhrUrl : '<portlet:resourceURL/>',
                            gridPagination : true,
							xhrSend : ({'<%=LoginHistoryContstants.ACTION_TYPE%>' : '<%=LoginHistoryContstants.ACTION_TYPE_INITIALIZE_DATA_GRID%>'}),
							xhrLoad : function(response){
				                            if(response == null || response == ''){
				                            	<portlet:namespace/>HideDiv('loginHistoryContainer');
				                            	<portlet:namespace/>ShowDiv('loginHistoryNoOccasionFound');
				                            }
				                            window.grid.settings.data=response;
				                            window.grid.init();
											console.log(window.grid);
				                            window.loginHistoryStandBy.hide();
											dijit.byId('loginHistoryGrid').canSort=false;	
			                          },
                            xhrError : function(error){
                            	<portlet:namespace/>HideDiv('loginHistoryContainer');
				                <portlet:namespace/>ShowDiv('loginHistoryErrorContainer');
	                            window.loginHistoryStandBy.hide();
                            }
                        });
                    });
                }
	);
					
	function <portlet:namespace/>ShowDiv(divId){
		dojo.byId(divId).setAttribute('style','display:');
	}
	
	function <portlet:namespace/>HideDiv(divId){
		dojo.byId(divId).setAttribute('style','display:none');
	}

</script>




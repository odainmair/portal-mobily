package sa.com.mobily.eportal.acct.mgmt.login.history.util;

import java.util.Comparator;

public class MyDateComparator implements Comparator<LoginHistoryViewHelper>{
		 
		//DateFormat format = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss aa");
	
	    @Override
	    public int compare(LoginHistoryViewHelper str1, LoginHistoryViewHelper str2) {
	    	    	
	    	int returnVal = 0;
	        try {
	        
	        	//returnVal = format.parse(str2.getLoginHistoryDateTime()).compareTo(format.parse(str1.getLoginHistoryDateTime()));
	        	returnVal = str2.getLoginHistoryDateTime().compareTo(str1.getLoginHistoryDateTime());
	        	
	        } catch (Exception e) {
			}
	        return returnVal;
	    }
}

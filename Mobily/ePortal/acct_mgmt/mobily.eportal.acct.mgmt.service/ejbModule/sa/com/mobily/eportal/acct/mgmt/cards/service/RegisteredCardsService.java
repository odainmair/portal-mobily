package sa.com.mobily.eportal.acct.mgmt.cards.service;

import static javax.ejb.TransactionAttributeType.NOT_SUPPORTED;

import java.util.logging.Level;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;

import sa.com.mobily.eportal.acct.mgmt.cards.jms.RegisteredCardsJMSRepository;
import sa.com.mobily.eportal.acct.mgmt.cards.service.view.RegisteredCardsServiceLocal;
import sa.com.mobily.eportal.acct.mgmt.cards.service.view.RegisteredCardsServiceRemote;
import sa.com.mobily.eportal.acct.mgmt.cards.valueobjects.RegisteredCardsVO;
import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;


/**
 * Session Bean implementation class RegisteredCardsService
 */
@Stateless
@Local(RegisteredCardsServiceLocal.class)
@Remote(RegisteredCardsServiceRemote.class)
public class RegisteredCardsService implements RegisteredCardsServiceRemote, RegisteredCardsServiceLocal {
	
	private static ExecutionContext executionContext = ExecutionContextFactory
			.getExecutionContext(AcctMgmtLoggerConstantsIfc.CARDS_SERVICE_LOGGER_NAME);
	private static final String CLASS_NAME = RegisteredCardsService.class.getName(); 
    
	/**
	 * @MethodName: getCardNumber
	 * <p>
	 * Returns card number for user by Msisdn
	 * 
	 * @param String msisdn
	 * 
	 * @return RegisteredCardsVO 
	 * 
	 * @author Abu Sharaf
	 */
    @TransactionAttribute(value = NOT_SUPPORTED)
    public RegisteredCardsVO getCardNumber(String msisdn)
    {
    	String methodName="getCardNumber";
    	executionContext.log(Level.INFO," RegisteredCardsService -> getCardNumber Method Called ",CLASS_NAME,methodName,null);
//    	RegisteredCardsVO cardVO=new RegisteredCardsVO();
//    	cardVO.setNoOfLines("10");
//    	cardVO.setErrorCode("0");
    	RegisteredCardsJMSRepository registeredCardsJMSRepository=new RegisteredCardsJMSRepository();
    	RegisteredCardsVO cardVO=registeredCardsJMSRepository.getCardNumber(msisdn);
    	executionContext.log(Level.INFO," RegisteredCardsService -> getCardNumber Method Ended ",CLASS_NAME,methodName,null);
    	return cardVO; 
    }

}

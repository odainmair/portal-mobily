package sa.com.mobily.eportal.acct.mgmt.cards.xml;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import sa.com.mobily.eportal.acct.mgmt.cards.constants.TagIfc;
import sa.com.mobily.eportal.acct.mgmt.cards.jaxb.eai.reply.MOBILYBSLSRREPLY;
import sa.com.mobily.eportal.acct.mgmt.cards.jaxb.eai.request.MOBILYBSLSR;
import sa.com.mobily.eportal.acct.mgmt.cards.jaxb.eai.request.SRHEADER;
import sa.com.mobily.eportal.acct.mgmt.cards.jaxb.request.CRHEADER;
import sa.com.mobily.eportal.acct.mgmt.cards.jaxb.request.CRREQUESTMESSAGE;
import sa.com.mobily.eportal.acct.mgmt.cards.jaxb.request.Param;
import sa.com.mobily.eportal.acct.mgmt.cards.jaxb.request.Params;
import sa.com.mobily.eportal.acct.mgmt.cards.jaxb.response.CRREPLYMESSAGE;
import sa.com.mobily.eportal.acct.mgmt.cards.valueobjects.RegisteredCardsVO;
import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.common.exception.system.XMLParserException;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;

/**
 * 
 * @author r.agarwal.mit
 * 
 */

public class RegisteredCardsXMLHandler {

	private static String clazz = RegisteredCardsXMLHandler.class.getName();

	private static ExecutionContext executionContext = ExecutionContextFactory
			.getExecutionContext(AcctMgmtLoggerConstantsIfc.CARDS_SERVICE_LOGGER_NAME);

	/**
	 * Generates XML request for Supplementary Service Subscription
	 * 
	 * @param requestVo
	 * @return
	 * @throws MobilyCommonException
	 */
	public String generateGetCardNumberRequestXML(
			String  msisdn) {
		String methodName = "generateGetCardNumberRequestXML";
		String xmlRequest = "";
		final ByteOutputStream outputStream = new ByteOutputStream();
		XMLStreamWriter xmlStreamWriter = null;
		try {
			CRREQUESTMESSAGE requestObj = new CRREQUESTMESSAGE();
			
			CRHEADER requestHeader = new CRHEADER();
			requestObj.setCRHEADER(requestHeader);
			
			requestHeader.setFuncId(TagIfc.INQ_FUNC_ID);
			requestHeader.setMsgVersion(TagIfc.INQ_MSG_VERSION);
			requestHeader.setRequestorChannelId(TagIfc.INQ_REQUESTOR_CHANNEL_ID);
			requestHeader.setRequestorSrId(TagIfc.INQ_REQUESTOR_USER_ID);
			requestHeader.setRequestorUserId(TagIfc.INQ_REQUESTOR_USER_ID);
			requestHeader.setRequestorLanguage(TagIfc.INQ_REQUESTOR_LANGUAGE_ENGLISH);
			
			requestObj.setInquiryType(TagIfc.INQ_INQUIRY_TYPE); 
			Param param=new Param();
			param.setKey(TagIfc.TAG_MSISDN);
			param.setValue(msisdn);
			Params params=new Params();
			params.setParam(param);
			
			requestObj.setParams(params);
			final XMLOutputFactory outputFactory = XMLOutputFactory
					.newInstance();

			xmlStreamWriter = outputFactory.createXMLStreamWriter(outputStream,
					"UTF-8");
			JAXBContext jaxbContext = JAXBContext
					.newInstance(CRREQUESTMESSAGE.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.marshal(requestObj, xmlStreamWriter);
			outputStream.flush();

			xmlRequest = new String(outputStream.getBytes(), "UTF-8").trim();

		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "Exception::" + e.getMessage(),
					clazz, methodName, e);
			throw new XMLParserException(e.getMessage(), e);
		} finally {
			if (null != xmlStreamWriter) {
				try {
					xmlStreamWriter.close();
				} catch (XMLStreamException e) {
					executionContext.log(
							Level.SEVERE,
							"Exception while closing xml stream writer::"
									+ e.getMessage(), clazz, methodName, e);
				}
			}
			if (outputStream != null) {
				outputStream.close();
			}
		}

		return xmlRequest;
	}

	/**
	 * 
	 * @param replyXMLMessage
	 * @return replyVo
	 * @throws MobilyCommonException
	 */
	public RegisteredCardsVO parseCardNumberReplyXML(
			String replyXMLMessage) {
		String methodName = "parseCardNumberReplyXML";
		JAXBContext jc;
		RegisteredCardsVO replyVO = new RegisteredCardsVO();

		try {
			jc = JAXBContext.newInstance(CRREPLYMESSAGE.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			XMLInputFactory xmlif = XMLInputFactory.newInstance();
			XMLStreamReader xmlstreamer = xmlif
					.createXMLStreamReader(new StringReader(replyXMLMessage));
			CRREPLYMESSAGE replyObj = (CRREPLYMESSAGE) unmarshaller
					.unmarshal(xmlstreamer);
			if (null != replyObj) {
				replyVO.setErrorCode(replyObj.getErrorCode());
				replyVO.setErrorMessage(replyObj.getErrorMessage());
				
				if(TagIfc.TAG_NO_OF_LINES.equals(replyObj.getSections().getSection().getResults().getResultSet().getResult().getKey()))	
					replyVO.setNoOfLines(replyObj.getSections().getSection().getResults().getResultSet().getResult().getValue());
				
			}
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "Exception::" + e.getMessage(),
					clazz, methodName, e);
		}
		return replyVO;
	}
	
	/**
	 * 
	 * @param msisdn
	 * @return String
	 */
	public String generateNumberOfLinesRequestXML(String  msisdn) {
		String methodName = "generateNumberOfLinesRequestXML";
		String xmlRequest = "";
		final ByteOutputStream outputStream = new ByteOutputStream();
		XMLStreamWriter xmlStreamWriter = null;
		try {
			MOBILYBSLSR requestObj = new MOBILYBSLSR();
			
			Calendar date = Calendar.getInstance();
			SimpleDateFormat objSimpleDateFormat = new SimpleDateFormat(TagIfc.DATE_FORMAT);
			String dateStr = objSimpleDateFormat.format(date.getTime());
			
			SRHEADER requestHeader = new SRHEADER();
			requestHeader.setFuncId(TagIfc.FUNC_ID_ADV_GENERAL_INQ);
			requestHeader.setSecurityKey(TagIfc.INQ_SECURITY_KEY);
			requestHeader.setMsgVersion(TagIfc.INQ_MSG_VERSION);
			requestHeader.setRequestorChannelId(TagIfc.INQ_REQUESTOR_CHANNEL_ID_EPORTAL);
			requestHeader.setServiceRequestId(TagIfc.CHANNEL_TRANSACTION_ID + "_" +dateStr);
			requestHeader.setSrDate(dateStr);
			requestHeader.setRequestorUserId(TagIfc.INQ_REQUESTOR_CHANNEL_ID_EPORTAL);
			requestHeader.setRequestorLanguage(TagIfc.INQ_REQUESTOR_LANGUAGE_ENGLISH);
			
			requestObj.setSRHEADER(requestHeader);
			
			requestObj.setInquiryType(TagIfc.INQ_INQUIRY_TYPE); 
			
			List<sa.com.mobily.eportal.acct.mgmt.cards.jaxb.eai.request.Param> paramList = new ArrayList<sa.com.mobily.eportal.acct.mgmt.cards.jaxb.eai.request.Param>();
			sa.com.mobily.eportal.acct.mgmt.cards.jaxb.eai.request.Param param = new sa.com.mobily.eportal.acct.mgmt.cards.jaxb.eai.request.Param();
			param.setKey(TagIfc.TAG_MSISDN);
			param.setValue(msisdn);
			
			paramList.add(param);
			
			sa.com.mobily.eportal.acct.mgmt.cards.jaxb.eai.request.Params params = new sa.com.mobily.eportal.acct.mgmt.cards.jaxb.eai.request.Params();
			params.setParamList(paramList);
			
			requestObj.setParams(params);
			
			final XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();

			xmlStreamWriter = outputFactory.createXMLStreamWriter(outputStream, "UTF-8");
			JAXBContext jaxbContext = JAXBContext.newInstance(MOBILYBSLSR.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.marshal(requestObj, xmlStreamWriter);
			outputStream.flush();

			xmlRequest = new String(outputStream.getBytes(), "UTF-8").trim();

		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "Exception::" + e.getMessage(), clazz, methodName, e);
			throw new XMLParserException(e.getMessage(), e);
		} finally {
			if (null != xmlStreamWriter) {
				try {
					xmlStreamWriter.close();
				} catch (XMLStreamException e) {
					executionContext.log(
							Level.SEVERE,
							"Exception while closing xml stream writer::"
									+ e.getMessage(), clazz, methodName, e);
				}
			}
			if (outputStream != null) {
				outputStream.close();
			}
		}

		return xmlRequest;
	}

	/**
	 * 
	 * @param replyXMLMessage
	 * @return RegisteredCardsVO
	 */
	public RegisteredCardsVO parseNumberOfLinesReplyXML(String replyXMLMessage) {
		String methodName = "parseNumberOfLinesReplyXML";
		JAXBContext jc;
		RegisteredCardsVO replyVO = new RegisteredCardsVO();

		try {
			jc = JAXBContext.newInstance(MOBILYBSLSRREPLY.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			XMLInputFactory xmlif = XMLInputFactory.newInstance();
			XMLStreamReader xmlstreamer = xmlif.createXMLStreamReader(new StringReader(replyXMLMessage));
			MOBILYBSLSRREPLY replyObj = (MOBILYBSLSRREPLY) unmarshaller.unmarshal(xmlstreamer);
			if (replyObj != null ) {
				if(replyObj.getReturnCode() != null)
					replyVO.setErrorCode(replyObj.getReturnCode().getErrorCode());
				
				replyVO.setErrorMessage(replyObj.getErrorMsg());
				
				replyVO.setNoOfLines(replyObj.getNoOfLines());
				
			}
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "Exception::" + e.getMessage(), clazz, methodName, e);
		}
		return replyVO;
	}
}

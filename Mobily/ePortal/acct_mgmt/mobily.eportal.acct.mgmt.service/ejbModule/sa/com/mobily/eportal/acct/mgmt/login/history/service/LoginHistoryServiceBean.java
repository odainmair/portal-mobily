/**
 * 
 */
package sa.com.mobily.eportal.acct.mgmt.login.history.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.Interceptors;

import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.acct.mgmt.login.history.dao.LoginHistoryDAO;
import sa.com.mobily.eportal.acct.mgmt.login.history.service.view.LoginHistoryServiceLocal;
import sa.com.mobily.eportal.acct.mgmt.login.history.service.view.LoginHistoryServiceRemote;
import sa.com.mobily.eportal.acct.mgmt.login.history.valueobjects.LoginHistoryRequestVO;
import sa.com.mobily.eportal.acct.mgmt.login.history.valueobjects.LoginHistoryVO;
import sa.com.mobily.eportal.common.interceptor.ServiceInterceptor;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

/**
 * @author n.gundluru.mit
 *
 */
@Stateless
@Local(LoginHistoryServiceLocal.class)
@Remote(LoginHistoryServiceRemote.class)
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class LoginHistoryServiceBean implements LoginHistoryServiceLocal, LoginHistoryServiceRemote
{
	private static String className = LoginHistoryServiceBean.class.getName();
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(AcctMgmtLoggerConstantsIfc.ACCT_MGMT_LOGIN_HISTORY);
	private static String KEY_LOGIN_HISTORY_DURATION = "LOGIN_HISTORY_DURATION";

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Interceptors(ServiceInterceptor.class)
	public List<LoginHistoryVO> findLoginHistoryByUserName(LoginHistoryRequestVO requestVoObj)
	{
		String  duration =  (String) ResourceEnvironmentProviderService.getInstance().getPropertyValue(KEY_LOGIN_HISTORY_DURATION);

		executionContext.log(Level.INFO, "Getting login history from DB for UserName =["+requestVoObj.getUserName()+"], loginHistory duration =["+duration+"] days", className, "findLoginHistoryByUserName", null);
		ArrayList<LoginHistoryVO> loginHistoryList = LoginHistoryDAO.getInstance().findLoginHistoryByUserName(requestVoObj.getUserName(), Integer.parseInt(duration));

		executionContext.log(Level.INFO, "Getting login history from DB for UserName =["+requestVoObj.getUserName()+"], loginHistoryList size =["+loginHistoryList.size()+"]", className, "findLoginHistoryByUserName", null);
		return loginHistoryList;
	}

}

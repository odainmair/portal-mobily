package sa.com.mobily.eportal.activenumber.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

//import javax.annotation.Resource;
//import javax.ejb.EJBContext;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.Interceptors;
import javax.naming.NamingException;
//import javax.transaction.UserTransaction;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import sa.com.mobily.eportal.accounts.subscription.vo.UserAccountsSubscriptionsVO;
import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.activenumber.service.constants.ErrorCodeConstantsIfc;
import sa.com.mobily.eportal.activenumber.service.view.ManageSubscriptionServiceLocal;
import sa.com.mobily.eportal.activenumber.service.view.ManageSubscriptionServiceRemote;
import sa.com.mobily.eportal.activenumber.vo.ChangeDefaultSubInputVO;
import sa.com.mobily.eportal.cacheinstance.dao.UserSubscriptionEligibilityDAO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.SessionVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.SubscriptionVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.UserAccountVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.UserSubscriptionsVO;
import sa.com.mobily.eportal.common.beanlocator.BeanLocator;
import sa.com.mobily.eportal.common.constants.PumaProfileConstantsIfc;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.application.DataNotFoundException;
import sa.com.mobily.eportal.common.exception.application.MobilyApplicationException;
import sa.com.mobily.eportal.common.exception.constant.ExceptionConstantIfc;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.interceptor.ServiceInterceptor;
import sa.com.mobily.eportal.common.service.dao.UserAccountDAO;
import sa.com.mobily.eportal.common.service.dao.UserSubscriptionDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.ldapservice.LDAPManagerFactory;
import sa.com.mobily.eportal.common.service.model.UserAccount;
import sa.com.mobily.eportal.common.service.model.UserSubscription;
import sa.com.mobily.eportal.common.service.pincode.activation.dao.PinCodeActivationDAO;
import sa.com.mobily.eportal.common.service.pincode.activation.model.PinCodeActivation;
import sa.com.mobily.eportal.common.service.sms.PinCodeSender;
import sa.com.mobily.eportal.common.service.sms.SMSSender;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.RandomGenerator;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.util.ldap.LDAPManager;
import sa.com.mobily.eportal.customerinfo.constant.CustSegmentConstant;
import sa.com.mobily.eportal.customerinfo.constant.CustomerTypeConstant;
import sa.com.mobily.eportal.customerinfo.constant.LineTypeConstant;
import sa.com.mobily.eportal.customerinfo.constant.RoleIdConstant;
import sa.com.mobily.eportal.customerinfo.constant.SubscriptionTypeConstant;
import sa.com.mobily.eportal.customerinfo.jms.CustomerInfoJMSRepository;
import sa.com.mobily.eportal.customerinfo.vo.ConsumerRelatedNumbersReplyVO;
import sa.com.mobily.eportal.customerinfo.vo.CustomerInfoVO;
import sa.com.mobily.eportal.customerinfo.vo.RelatedAccountVO;
import sa.com.mobily.eportal.mycontact.dao.MyContactsDAO;
import sa.com.mobily.eportal.mycontact.vo.MyContactsVO;
import sa.com.mobily.eportal.registration.constant.RegisterationConstantsIfc;
import sa.com.mobily.eportal.registration.service.view.RegistrationAsyncServiceRemote;
import sa.com.mobily.eportal.registration.vo.FailedOperationInfo;
import sa.com.mobily.eportal.user.account.subscription.dao.UserAccountsSubscriptiondao;
import sa.com.mobily.valueobject.common.PinCodeVO;

@Stateless
@Local(ManageSubscriptionServiceLocal.class)
@Remote(ManageSubscriptionServiceRemote.class)
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class ManageSubscriptionService implements ManageSubscriptionServiceLocal, ManageSubscriptionServiceRemote
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(AcctMgmtLoggerConstantsIfc.MANAGE_SUBSCRIPTIONS_SERVICE_LOGGER_NAME);

	private static final String className = ManageSubscriptionService.class.getName();

	private static final String ACTIVATION_CODE_SMS_ID_AR = "652";

	private static final String ACTIVATION_CODE_SMS_ID_EN = "651";

	//@Resource private EJBContext sessionContext;
	
	
	/**
	 * This method returns the related numbers based on the given user name
	 * 
	 * The passed userName is the name of user
	 * 
	 * It returns the related numbers as subscription list from the database
	 */
	@Override
	@Interceptors(ServiceInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<UserSubscriptionsVO> getUserRelatedNumberByUsername(String userName) {
		
		List<UserSubscriptionsVO> userSubscriptionsVoList = 
				sa.com.mobily.eportal.cacheinstance.dao.UserSubscriptionDAO.getInstance().getUserSubscriptionsByAccountId(userName);		
		
		return userSubscriptionsVoList;
	}
	
	
	/**
	 * This method is called per login, when user tries to access manage
	 * subscription page. It send a request to back end system to get the
	 * related numbers/subscriptions of the user logged in based on user's
	 * default number. This method is called once per login and keeps user's
	 * related numbers in portlet session. This method also takes a difference
	 * between related number list and portal db list to show user an option to
	 * add only the number which are in the difference, also it marks the
	 * numbers which are extra in portal db as inactive.
	 * 
	 * The passed userName is the name of user, second one is sessionID, in case
	 * of portlet client it should not be null, in case of mobile client,
	 * sessionID can be null.
	 * 
	 * It returns the related numbers as subscription list from back end system
	 */
	@Override
	@Interceptors(ServiceInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<UserSubscription> getRelatedSubscriptions(String userName, SessionVO sessionVO)
	{
		String method = "getRelatedSubscriptions";
		List<UserSubscription> userSubList = null;
		executionContext.log(Level.FINER, "Inside getUserRelatedSubscriptions", className, method, null);

		if (sessionVO == null)
		{
			throw new BackEndException(ExceptionConstantIfc.ACTIVE_NUMBER_SERVICE, ErrorCodeConstantsIfc.USER_NOT_LOADED);
		}

		List<SubscriptionVO> dbList = new ArrayList<SubscriptionVO>();
		if (sessionVO.getOtherSubscriptions() != null)
		{
			for (SubscriptionVO vo : sessionVO.getOtherSubscriptions().values())
			{
				dbList.add(vo);
			}
		}

		// add the default one in the list
		SubscriptionVO vo = new SubscriptionVO();
		vo.setMsisdn(sessionVO.getMsisdn());
		vo.setServiceAccountNumber(sessionVO.getServiceAccountNumber());
		vo.setSubscriptionType(sessionVO.getSubscriptionType());
		try
		{
			vo.setCustomerType(Integer.parseInt(sessionVO.getCustomerType()));
		}
		catch (NumberFormatException ne)
		{
			// ignore number format exception
		}
		dbList.add(vo);
		// this msisdn is always the default one
		ConsumerRelatedNumbersReplyVO relatedVO = null;
		CustomerInfoJMSRepository customerInfoJMSRepository = new CustomerInfoJMSRepository();
		if (StringUtils.isNotEmpty(sessionVO.getMsisdn()))
		{
			relatedVO = customerInfoJMSRepository.getRelatedNumbersByMsisdn(sessionVO.getMsisdn());
		}
		else
		{
			relatedVO = customerInfoJMSRepository.getRelatedNumbersByAcctNumber(sessionVO.getServiceAccountNumber());
		}
		executionContext.log(Level.INFO, relatedVO.toString(), className, method, null);
		Set<String> fullNumbersSet = new HashSet<String>();

		// exclude all the existing numbers if found in related list
		if (null != relatedVO && null != relatedVO.getRelatedAccountsListVO() && CollectionUtils.isNotEmpty(relatedVO.getRelatedAccountsListVO().getRelatedAccountVOList()))
		{
			this.removeExtraSubscription(dbList, relatedVO);
			// please note this list also have the default one
			if (CollectionUtils.isNotEmpty(dbList))
			{
				for (SubscriptionVO userSubscription : dbList)
				{
					fullNumbersSet.add(userSubscription.getServiceAccountNumber());
				}
			}
			executionContext.log(Level.INFO, "existingPortalList : " + dbList, className, method, null);
			executionContext.log(Level.INFO, "RelatedList : " + relatedVO, className, method, null);
			userSubList = new ArrayList<UserSubscription>();
			UserSubscription notInPortal = null;

			for (RelatedAccountVO relatedAccountVO : relatedVO.getRelatedAccountsListVO().getRelatedAccountVOList())
			{
				// ignore the existing subscription by adding it to a
				// java.util.Set.
				executionContext.log(Level.INFO, "ACC_NUM : " + relatedAccountVO.getServiceAccountNumber(), className, method, null);

				if (fullNumbersSet.add(relatedAccountVO.getServiceAccountNumber()))
				{
					notInPortal = new UserSubscription();
					notInPortal.setMsisdn(relatedAccountVO.getMSISDN());
					notInPortal.setServiceAcctNumber(relatedAccountVO.getServiceAccountNumber());
					notInPortal.setIsDefault("N");
					notInPortal.setSubscriptionType(relatedAccountVO.getSubscriptionType());
					notInPortal.setLineType(relatedAccountVO.getLineType());
					notInPortal.setCustomerType("" + relatedAccountVO.getCustomerType());
					userSubList.add(notInPortal);
					executionContext.log(Level.INFO, notInPortal.toString(), className, method, null);
				}
			}
		}
		else
		{
			throw new BackEndException(ExceptionConstantIfc.ACTIVE_NUMBER_SERVICE, ErrorCodeConstantsIfc.USER_RELATED_SUBS_NOT_LOADED);
		}
		return userSubList;
	}

	/**
	 * This method is used to add the related number of the user in portal db,
	 * the number is loaded once and is in user's session, so just need to
	 * persist that in db. It supports addition of multiple numbers in once call
	 * 
	 * It will take the id of logged in user, this id is from USER_ACCOUNTS
	 * table. It will take a list of related subscription to be added.
	 */
	@Override
	@Interceptors(ServiceInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void addSubscription(long userAcctId, List<UserSubscription> subList)
	{
		String method = "addSubscription";
		executionContext.log(Level.FINER, "Inside addSubscription", className, method, null);

		if (CollectionUtils.isNotEmpty(subList))
		{
			UserSubscriptionDAO userSubscriptionDAO = UserSubscriptionDAO.getInstance();
			
			MyContactsDAO myContactsDAO = MyContactsDAO.getInstance();
			

			// now here, we will load the all subscriptions of user and then add
			// one more to the list.
			executionContext.log(Level.INFO, "userSubscriptionList size : " + subList.size(), className, method, null);
			CustomerInfoVO replyVO = null;
			
			for (UserSubscription userSubscription : subList)
			{
				boolean isTesting=false;
				
				if (StringUtils.isNotEmpty(userSubscription.getMsisdn()))
				{
					if(isTesting){
						executionContext.log(Level.INFO, "isTesting : " + isTesting, className, method, null);
						replyVO = new CustomerInfoVO();
						replyVO.setSubscriptionType(SubscriptionTypeConstant.SUBS_TYPE_FV);
						replyVO.setCustomerIdNumber("1076120656");
						replyVO.setCustomerType("2");
						replyVO.setPackageId("3434");
						replyVO.setCustomerSegment("Individual");
						replyVO.setPayType("2");
						replyVO.setLineType(LineTypeConstant.SUBS_TYPE_FV);
					}else{
						replyVO = sendCustomerProfileRequest(userSubscription.getMsisdn(), "English");
					}
				}
				else
				{
					replyVO = sendCustomerProfileRequest(userSubscription.getServiceAcctNumber(), "English");
				}
				executionContext.audit(Level.INFO, "replyVO : " + replyVO, className, method);
				
				userSubscription.setUserAcctId(userAcctId);
				
				if (replyVO != null && replyVO.getCreditScoring() != null && replyVO.getCreditScoring().length() > 0)
				{
					int creditScore = 0;
					try
					{
						creditScore = Integer.parseInt(replyVO.getCreditScoring());
					}
					catch (Exception e)
					{
					}
					userSubscription.setCcStatus(creditScore);
				}
				if (replyVO != null && StringUtils.isNotEmpty(replyVO.getPayType().trim()))
				{
					userSubscription.setCustomerType(replyVO.getPayType());
				}
				
				if (replyVO != null) {
					userSubscription.setCustomerSegment(replyVO.getCustomerSegment());
					userSubscription.setLineType(replyVO.getLineType());
					userSubscription.setSubscriptionType(replyVO.getSubscriptionType());
					userSubscription.setSubscriptionName(SubscriptionTypeConstant.getSubscriptionName(replyVO.getSubscriptionType()));
					userSubscription.setNationalIdOrIqamaNumber(replyVO.getCustomerIdNumber());
				}

				userSubscription.setCreatedDate(new Timestamp(new Date().getTime()));
				userSubscription.setPackageId(replyVO.getPackageId());
				userSubscription.setIsDefault("N");
				userSubscription.setIsActive("Y");
				executionContext.audit(Level.INFO, "Before saving to DB : " , className, method);
				userSubscriptionDAO.saveUserSubscripton(userSubscription);
				executionContext.audit(Level.INFO, "After saving to DB : " , className, method);
				
				//Need to Update Status in Contact to preview this row in Login Response
				executionContext.audit(Level.INFO, "Before Updating Contact Table : " , className, method);
				MyContactsVO contact = new MyContactsVO();
				contact.setUserAccountId(userSubscription.getUserAcctId());
				contact.setPhone(userSubscription.getMsisdn());
				myContactsDAO.updateContactAddConfirm(contact);
				executionContext.audit(Level.INFO, "After Updating Contact Table : " , className, method);
				
				
				
				executionContext.audit(Level.INFO, "Subscription Type :: "+userSubscription.getSubscriptionType() , className, method);
				executionContext.audit(Level.INFO, "Customer Type of FV::: "+userSubscription.getCustomerType() , className, method);
				
				if ((SubscriptionTypeConstant.SUBS_TYPE_GSM == userSubscription.getSubscriptionType() || SubscriptionTypeConstant.SUBS_TYPE_FV == userSubscription.getSubscriptionType()) && null != userSubscription.getCustomerType() 
						&& CustomerTypeConstant.PostPaid.equals(userSubscription.getCustomerType())
						|| ("" + CustomerTypeConstant.CUSTOMER_TYPE_PostPaid).equals(userSubscription.getCustomerType()))
				{
					// update poid list in async call
					try
					{
						executionContext.audit(Level.INFO, "Inside POID updation : " , className, method);
						RegistrationAsyncServiceRemote registrationAsyncService = (RegistrationAsyncServiceRemote) BeanLocator.remoteLookup(RegistrationAsyncServiceRemote.class
								.getName());
						registrationAsyncService.updatePOIDListforRegistration(userSubscription.getMsisdn());
					}
					catch (NamingException e)
					{
						executionContext.log(Level.SEVERE, "updatePOIDListforRegistration failed to lookup registrationAsyncService", className, method, e);
					}
				}
			}
			executionContext.log(Level.FINER, "userSubscription added " + subList.size(), className, method, null);
		}
		else
		{
			executionContext.log(Level.FINER, "Empty List", className, method, null);
		}
	}

	/**
	 * This method will change the default number of the user. It check if the
	 * passed number is not default in some other user's account.
	 * 
	 * It accepts the logged in user account db id. It accepts the new selected
	 * number either msisdn / service account number
	 */
	@Override
	@Interceptors(ServiceInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void changeDefaultNumber(ChangeDefaultSubInputVO changeDefaultSubInputVO)
	{
		String method = "changeDefaultNumber";
		// first check if the given number is not a default in some other
		// account's subscription
		UserSubscriptionDAO userSubscriptionDAO = UserSubscriptionDAO.getInstance();
		executionContext.log(Level.INFO, "Seleted number:: " + changeDefaultSubInputVO.getSelectedNumber(),className, method, null);
		if (null != LDAPManagerFactory.getInstance().getLDAPManagerImpl()
				.findMobilyUserByAttribute(PumaProfileConstantsIfc.defaultMsisdn, changeDefaultSubInputVO.getSelectedNumber()))
		{
			executionContext.log(Level.INFO, "The selected number already exist in DB with some other acount: selectedNumber : " + changeDefaultSubInputVO.getSelectedNumber(),
					className, method, null);
			throw new BackEndException(ExceptionConstantIfc.ACTIVE_NUMBER_SERVICE, ErrorCodeConstantsIfc.DEFAULT_SUBS_EXISTS);
		}
		executionContext.log(Level.INFO, "User found in DB : userAcctId " + changeDefaultSubInputVO.getUserAcctId(), className, method, null);
		List<UserSubscription> userSubscriptions = userSubscriptionDAO.findByUserAccountID(changeDefaultSubInputVO.getUserAcctId());
		UserSubscription newDefaultSub = null;
		UserSubscription oldDefaultSub = null;

		if (CollectionUtils.isNotEmpty(userSubscriptions))
		{
			for (UserSubscription userSubscription : userSubscriptions)
			{
				if ((StringUtils.equals(userSubscription.getMsisdn(), changeDefaultSubInputVO.getSelectedNumber()))
						|| (StringUtils.equals(userSubscription.getServiceAcctNumber(), changeDefaultSubInputVO.getSelectedNumber())))
				{
					newDefaultSub = userSubscription;
				}
				else if (userSubscription.getIsDefault().equalsIgnoreCase("Y"))
				{
					oldDefaultSub = userSubscription;
				}
			}
		}
		// here we found the subscription that we will change to default.
		if(oldDefaultSub != null)
			userSubscriptionDAO.updateUserDefaultSubscription(oldDefaultSub.getSubscriptionId(), "N");
		
		userSubscriptionDAO.updateUserDefaultSubscription(newDefaultSub.getSubscriptionId(), "Y");
		
		//Updating RoleId
		UserAccount userAccount = new UserAccount();
		userAccount.setRoleId(RoleIdConstant.ROLE_ID_CONSUMER_CUSTOMER);
		userAccount.setCustSegment(CustSegmentConstant.CUST_SEGMENT_CONSUMER);
		userAccount.setUserAcctId(changeDefaultSubInputVO.getUserAcctId());

		UserAccountDAO.getInstance().updateUserRoledID(userAccount);
		
		executionContext.log(Level.INFO, "Default Number changed in DB", className, method, null);
		executionContext.log(Level.INFO, " New Default Subscription Type ::"+newDefaultSub.getSubscriptionType().intValue(), className, method, null);
		// update the LDAP attribute
		Map<String, Object> attributeMap = new HashMap<String, Object>();
		if (SubscriptionTypeConstant.SUBS_TYPE_GSM == newDefaultSub.getSubscriptionType().intValue() 
				|| SubscriptionTypeConstant.SUBS_TYPE_CONNECT == newDefaultSub.getSubscriptionType().intValue() || SubscriptionTypeConstant.SUBS_TYPE_FV == newDefaultSub.getSubscriptionType().intValue())
			attributeMap.put(PumaProfileConstantsIfc.defaultMsisdn, newDefaultSub.getMsisdn());
		else
			attributeMap.put(PumaProfileConstantsIfc.defaultMsisdn, newDefaultSub.getServiceAcctNumber());

		attributeMap.put(PumaProfileConstantsIfc.defaultSubId, "" + newDefaultSub.getSubscriptionId());
		attributeMap.put(PumaProfileConstantsIfc.defaultSubscriptionType, "" + newDefaultSub.getSubscriptionType());
		attributeMap.put(PumaProfileConstantsIfc.defaultSubSubscriptionType, "" + newDefaultSub.getLineType());
		attributeMap.put(PumaProfileConstantsIfc.serviceAccountNumber, newDefaultSub.getServiceAcctNumber());
		attributeMap.put(PumaProfileConstantsIfc.roleId, "" + RoleIdConstant.ROLE_ID_CONSUMER_CUSTOMER);
		
		if(FormatterUtility.isNotEmpty(newDefaultSub.getNationalIdOrIqamaNumber()))
			attributeMap.put(PumaProfileConstantsIfc.iqama, newDefaultSub.getNationalIdOrIqamaNumber());

		LDAPManagerFactory.getInstance().getLDAPManagerImpl().updateUserAttributesOnLDAPByUserId(changeDefaultSubInputVO.getUserName(), attributeMap);

		executionContext.log(Level.INFO, "Default Number changed in LDAP", className, method, null);
	}

	/**
	 * This method is step 1 and is called during GSM number add process, first
	 * user's GSM number must be validated against his Iqama, this is achieved
	 * by calling Customer profile service, and matching the customer
	 * identification id from customer profile service with the customer
	 * Iqama/Passport saved in USER_ACCOUNT table. After ID validation, it send
	 * a PIN number to user's mobile.
	 * 
	 * The parameter userAcctId is the DB ID of logged in user. msisdn is the
	 * GSM number to be verified language is the user's preferred language.
	 * 
	 * It will return the pin code
	 */
	@Override
	@Interceptors(ServiceInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String sendPINCode(long userAcctId, String msisdn, String language)
	{
		String method = "sendPINCode";
		//CustomerInfoVO replyVO = new CustomerInfoVO();
		executionContext.log(Level.FINER, "userAcctId : " + userAcctId, className, method, null);
		executionContext.log(Level.FINER, "msisdn : " + msisdn, className, method, null);
		executionContext.log(Level.FINER, "language : " + language, className, method, null);
		UserSubscriptionDAO userSubscriptionDAO = UserSubscriptionDAO.getInstance();
		UserAccountDAO userAccountDAO = UserAccountDAO.getInstance();
		// find by PK ID
		List<UserAccount> userList = userAccountDAO.findByUserId((int) userAcctId);
		if (CollectionUtils.isEmpty(userList))
		{
			executionContext.log(Level.WARNING, "The user not found in DB : " + userAcctId, className, method, null);
			throw new BackEndException(ExceptionConstantIfc.ACTIVE_NUMBER_SERVICE, ErrorCodeConstantsIfc.USER_NOT_LOADED);
		}
		//UserAccount userAccount = userList.get(0);
		// now check the number does not exist in DB already.
		List<UserSubscription> results = userSubscriptionDAO.findAllSubscriptionsByServiceAccountNumberOrMsisdn(msisdn);
		if (CollectionUtils.isNotEmpty(results))
		{
			executionContext.log(Level.WARNING, "The number entered already in DB : accountNumber : " + msisdn, className, method, null);
			throw new BackEndException(ExceptionConstantIfc.ACTIVE_NUMBER_SERVICE, ErrorCodeConstantsIfc.SUBS_EXISTS);
		}

		PinCodeVO pinCodeVO = new PinCodeVO();
		pinCodeVO.setLanguage(language);
		pinCodeVO.setMessageIdEn(ACTIVATION_CODE_SMS_ID_EN);
		pinCodeVO.setMessageIdAr(ACTIVATION_CODE_SMS_ID_AR);
		pinCodeVO.setMsisdn(msisdn);
		pinCodeVO.setServiceID(ExceptionConstantIfc.ACTIVE_NUMBER_SERVICE);
		String otp = PinCodeSender.sendPINCode(pinCodeVO);

		return otp;
	}

	/**
	 * This is the second and last step of adding GSM number. It will prepare a
	 * UserSubscripton based on the CustomerInfoVO passed to it, and will store
	 * it in DB.
	 * 
	 * The parameter userAcctId is the DB ID of logged in user. The parameter
	 * msisdn is new mobile number to be added.
	 */
	@Override
	@Interceptors(ServiceInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void addGSMNumber(long userAcctId, String msisdn, String language)
	{
		String method = "addGSMNumber";
		UserAccountDAO userAccountDAO = UserAccountDAO.getInstance();
		UserSubscriptionDAO userSubscriptionDAO = UserSubscriptionDAO.getInstance();
		//UserSubscriptionEligibilityDAO subscriptionEligibilityDAO = UserSubscriptionEligibilityDAO.getInstance();
		// find by PK ID
		List<UserAccount> userList = userAccountDAO.findByUserId((int) userAcctId);
		if (CollectionUtils.isEmpty(userList))
		{
			throw new BackEndException(ExceptionConstantIfc.ACTIVE_NUMBER_SERVICE, ErrorCodeConstantsIfc.USER_NOT_LOADED);
		}

		// Customer Info supports caching so it will get it from cache.
		CustomerInfoVO replyVO = this.sendCustomerProfileRequest(msisdn, language);
		UserSubscription userSubscription = new UserSubscription();
		if (null != replyVO.getCreditScoring() && replyVO.getCreditScoring().length() > 0)
		{
			int creditScore = 0;
			try
			{
				creditScore = Integer.parseInt(replyVO.getCreditScoring());
			}
			catch (Exception e)
			{
				executionContext.log(Level.WARNING, "replyVO.getCreditScoring() was not parsed to Integer, creditScore is " + replyVO.getCreditScoring(), className, method, e);
			}
			userSubscription.setCcStatus(creditScore);
		}

		userSubscription.setCreatedDate(new Timestamp(new Date().getTime()));
		executionContext.log(Level.FINE, "''' Inside addGSMNumber customer type from back end is " + replyVO.getPayType(), className, method, null);
		if (null != replyVO.getPayType() && replyVO.getPayType().length() > 0)
		{
			userSubscription.setCustomerType(replyVO.getPayType() + "");
		}
		userSubscription.setCustomerSegment(replyVO.getCustomerSegment());
		userSubscription.setPackageId(replyVO.getPackageId());
		userSubscription.setSubscriptionType(SubscriptionTypeConstant.SUBS_TYPE_GSM);
		userSubscription.setIsDefault("N");
		userSubscription.setIsActive("Y");
		userSubscription.setMsisdn(replyVO.getMSISDN());
		userSubscription.setUserAcctId(userAcctId);
		userSubscription.setSubscriptionId(null);
		userSubscription.setServiceAcctNumber(replyVO.getAccountNumber());
		userSubscription.setSubscriptionType(SubscriptionTypeConstant.SUBS_TYPE_GSM);
		userSubscription.setSubscriptionName(SubscriptionTypeConstant.getSubscriptionName(SubscriptionTypeConstant.SUBS_TYPE_GSM));
		userSubscription.setLineType(LineTypeConstant.SUBS_TYPE_GSM);
		userSubscription.setNationalIdOrIqamaNumber(replyVO.getCustomerIdNumber());
		
		userSubscriptionDAO.saveUserSubscripton(userSubscription);

	}


	/**
	 * Sends request to customer profile info service
	 * 
	 * @param msisdn
	 * @param language
	 * @return
	 * @throws MobilyCommonException
	 */
	private CustomerInfoVO sendCustomerProfileRequest(String msisdn, String language)
	{
		String method = "sendCustomerProfileRequest";
		CustomerInfoVO replyVO = null;
		CustomerInfoVO requestVO = new CustomerInfoVO();
		requestVO.setSourceId(msisdn);

		if(FormatterUtility.isNotEmpty(msisdn) && "100".equals(msisdn.substring(0, 3))){
			requestVO.setSourceSpecName("BillingAccountNumber");
		} else {
			requestVO.setSourceSpecName("MSISDN");
		}
		
		if (StringUtils.isNotEmpty(language))
		{
			if (language.equalsIgnoreCase("English"))
			{
				requestVO.setPreferredLanguage("En");
			}
			else
			{
				requestVO.setPreferredLanguage("Ar");
			}
		}
		else
		{
			requestVO.setPreferredLanguage("En");
		}
		executionContext.log(Level.FINER, "requestVO is : " + requestVO, className, method, null);
		replyVO = new CustomerInfoJMSRepository().getCustomerProfile(requestVO);
		if (null == replyVO)
		{
			executionContext.log(Level.WARNING, "replyVO from customer profile service is NULL : " + replyVO, className, method, null);
			throw new BackEndException(ExceptionConstantIfc.ACTIVE_NUMBER_SERVICE, ErrorCodeConstantsIfc.MOBILE_IQAMA_MISMATCH);
		}
		executionContext.log(Level.FINER, "replyVO is : " + replyVO, className, method, null);
		if (null != replyVO && null != replyVO.getStatusCode() && "E510000".equals(replyVO.getStatusCode()))
		{
			executionContext.log(Level.WARNING, "replyVO.getStatusCode() : " + replyVO.getStatusCode(), className, method, null);
			throw new BackEndException(ExceptionConstantIfc.ACTIVE_NUMBER_SERVICE, ErrorCodeConstantsIfc.MOBILE_IQAMA_MISMATCH);
		}
		return replyVO;
	}

	/**
	 * Will mark the extra subscription in portal DB as in active
	 * 
	 * @param existingPortalList
	 * @param relatedList
	 * @return
	 * @throws MobilyCommonException
	 */
	private List<SubscriptionVO> removeExtraSubscription(List<SubscriptionVO> existingPortalList, ConsumerRelatedNumbersReplyVO relatedList)
	{
		String method = "removeExtraSubscription";
		executionContext.log(Level.FINER, "Inside removeExtraSubscription", className, method, null);
		List<SubscriptionVO> inPortalNotInCRM = null;

		if (CollectionUtils.isNotEmpty(existingPortalList) && CollectionUtils.isNotEmpty(relatedList.getRelatedAccountsListVO().getRelatedAccountVOList()))
		{
			inPortalNotInCRM = new ArrayList<SubscriptionVO>();
			Set<String> fullNumbersSet = new HashSet<String>();
			for (RelatedAccountVO relatedAccountVO : relatedList.getRelatedAccountsListVO().getRelatedAccountVOList())
			{
				fullNumbersSet.add(relatedAccountVO.getServiceAccountNumber());
				executionContext.log(Level.INFO, "CRM number : " + relatedAccountVO.getServiceAccountNumber(), className, method, null);
			}
			for (SubscriptionVO userSubscription : existingPortalList)
			{
				// avoid duplicate by adding to java.util.Set
				if (fullNumbersSet.add(userSubscription.getServiceAccountNumber()))
				{
					// number is in portal but not in CRM
					inPortalNotInCRM.add(userSubscription);
					executionContext.log(Level.INFO, "Should be deleted : " + userSubscription.getServiceAccountNumber(), className, method, null);
				}
			}
		}
		else
		{
			executionContext.log(Level.WARNING, "Both existingPortalList and relatedList are empty", className, method, null);
		}
		// now delete any subscriptions which are in portal DB but had been
		// removed from CRM
		List<Long> subscriptionIDList = null;
		if (CollectionUtils.isNotEmpty(inPortalNotInCRM))
		{
			subscriptionIDList = new ArrayList<Long>();
			for (SubscriptionVO userSubscription : inPortalNotInCRM)
			{
				subscriptionIDList.add(userSubscription.getSubscriptionID());
			}
			// this step already happening at login time.
			// userSubscriptionDAO.deActiveSubscription(subscriptionIDList);
			existingPortalList.removeAll(inPortalNotInCRM);
		}
		return existingPortalList;
	}

	/**
	 * @author AbuSharaf
	 * Utility method to handle Non mobily user and covert the user to consumer one.
	 * 
	 * @param User Account ID <long>userAcctID</long>
	 * @param User Subscription VO <UserSubscription>userSubscription</UserSubscription>
	 * 
	 * @return void
	 * @throws {@link BackEndException, DataNotFoundException}
	 */
	@Override
	@Interceptors(ServiceInterceptor.class)
	//@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void handleNonMobilyUserSubscription(@NotNull String username,@NotNull UserSubscription userSubscription,String activationCode)
	{
		final String methodName = "handleNonMobilyUserSubscription";
		executionContext.log(Level.INFO, "Begin Add New Sb=ubscription for Non Mobily User", className, methodName);
		if((null == userSubscription.getServiceAcctNumber() || userSubscription.getServiceAcctNumber().length() == 0) && (null == userSubscription.getMsisdn() || 0 == userSubscription.getMsisdn().length()) || (null == userSubscription.getNationalIdOrIqamaNumber() || 0 == userSubscription.getNationalIdOrIqamaNumber().length()))
			throw new BackEndException(ErrorCodeConstantsIfc.SUB_VO_NOT_VALID, null);

		UserAccountDAO userAccountDAO = UserAccountDAO.getInstance();
		UserSubscriptionDAO userSubscriptionDAO = UserSubscriptionDAO.getInstance();
		CustomerInfoVO consumerProfile = null;
		UserAccount userAccount = new UserAccount();
		UserSubscription userSubscriptionVO = new UserSubscription();
		long userAcctId = 0;
		
		executionContext.audit(Level.INFO, "User Subscription type111111 " + userSubscription.getSubscriptionType(), className, methodName);
		
		if(FormatterUtility.isEmpty(userSubscription.getNationalIdOrIqamaNumber()))
			throw new DataNotFoundException();
		
		//	Validating User Account INFO with Customer Profile Reply.
		if (SubscriptionTypeConstant.SUBS_TYPE_GSM == userSubscription.getSubscriptionType().intValue()
				|| SubscriptionTypeConstant.SUBS_TYPE_CONNECT == userSubscription.getSubscriptionType().intValue())
		{
			consumerProfile = new CustomerInfoJMSRepository().getCustProductProfileByMsisdn(userSubscription.getMsisdn());
			
			if(!consumerProfile.getCustomerIdNumber().equals(userSubscription.getNationalIdOrIqamaNumber().trim()))
				throw new DataNotFoundException();
			if(isDefaultMSISDNExisted(userSubscription.getMsisdn()))
				throw new BackEndException(0, ErrorCodeConstantsIfc.DEFAULT_SUBS_EXISTS);
			
			if(SubscriptionTypeConstant.SUBS_TYPE_GSM == userSubscription.getSubscriptionType().intValue()) {
				PinCodeActivation pinCodeActivation = PinCodeActivationDAO.getInstance().getActivationCode(userSubscription.getMsisdn(),"" + ErrorCodeConstantsIfc.REGISTERATION_ACTIVATION_PROJECT_ID);
				if(pinCodeActivation.getActivationCode().equals(activationCode)){
					PinCodeActivationDAO.getInstance().deleteActivationRequest(userSubscription.getMsisdn(),"" +  ErrorCodeConstantsIfc.REGISTERATION_ACTIVATION_PROJECT_ID);
				}
				if(!pinCodeActivation.getActivationCode().equals(activationCode))
					throw new BackEndException(0, ErrorCodeConstantsIfc.PIN_CODE_INVALID);
			}
		}
		else
		{ // for other subscriptions
			executionContext.audit(Level.INFO, "Call  getCustProductProfileByServAcctNumber for service account number::" + userSubscription.getServiceAcctNumber(), className, methodName);
			consumerProfile = new CustomerInfoJMSRepository().getCustProductProfileByServAcctNumber(userSubscription.getServiceAcctNumber());
			if(!consumerProfile.getCustomerIdNumber().equals(userSubscription.getNationalIdOrIqamaNumber().trim()))
				throw new DataNotFoundException();
			if(isDefaultServiceAccountNumberExisted(userSubscription.getServiceAcctNumber()))
				throw new BackEndException(0, ErrorCodeConstantsIfc.DEFAULT_SUBS_EXISTS);
		}
		
		boolean updatedInLDAP = false;
		boolean updatedInDB = false;
		try{
			//userTransaction=sessionContext.getUserTransaction();
			//userTransaction.begin();
			executionContext.audit(Level.INFO, "User Transaction started", className, methodName);
			// Getting User Account ID
			userAcctId = userAccountDAO.findByUserName(username.toUpperCase()).get(0).getUserAcctId();
			executionContext.audit(Level.INFO, "User Acct ID " + userAcctId, className, methodName);
			// updating Non mobily user status
			userAccount.setRoleId(RoleIdConstant.ROLE_ID_CONSUMER_CUSTOMER);
			// userAccount.setIqama(consumerProfile.getCustomerIdNumber());
			userAccount.setCustSegment(consumerProfile.getCustomerSegment());
			userAccount.setUserAcctId(userAcctId);

			userAccountDAO.updateUserRoledID(userAccount);
			executionContext.audit(Level.INFO, "information are updated on DB ", className, methodName);

			// Insert New subscription for non mobily user
			userSubscriptionVO.setUserAcctId(userAcctId);
			userSubscriptionVO.setSubscriptionId(userSubscriptionDAO.getUserSubscriptionIdSeq());
			userSubscriptionVO.setIsDefault(Boolean.TRUE);
			userSubscriptionVO.setMsisdn(consumerProfile.getMSISDN());
			userSubscriptionVO.setServiceAcctNumber(consumerProfile.getAccountNumber());
			userSubscriptionVO.setPackageId(consumerProfile.getPackageId());
			userSubscriptionVO.setCreatedDate(new Timestamp(new Date().getTime()));
			userSubscriptionVO.setIsActive("Y");
			userSubscriptionVO.setCustomerType(consumerProfile.getPayType());
			userSubscriptionVO.setSubscriptionType(consumerProfile.getSubscriptionType());
			userSubscriptionVO.setSubscriptionName(SubscriptionTypeConstant.getSubscriptionName(consumerProfile.getSubscriptionType()));
			userSubscriptionVO.setLineType(consumerProfile.getLineType());
			userSubscriptionVO.setNationalIdOrIqamaNumber(consumerProfile.getCustomerIdNumber());

			userSubscriptionDAO.addUserSubscripton(userSubscriptionVO);
			executionContext.audit(Level.INFO, "new record is added in User Subscription for this user " + username, className, methodName);
			
			updatedInDB = true;

			//	Update LDAP Attribute for non mobily user
			Map<String, Object> attributes = new HashMap<String,Object>();
			attributes.put(PumaProfileConstantsIfc.custSegment, consumerProfile.getCustomerSegment());
			attributes.put(PumaProfileConstantsIfc.defaultSubId, String.valueOf(userSubscriptionVO.getSubscriptionId()));
			attributes.put(PumaProfileConstantsIfc.userAcctReference, String.valueOf(userAcctId));
			attributes.put(PumaProfileConstantsIfc.roleId, RoleIdConstant.ROLE_ID_CONSUMER_CUSTOMER);
			attributes.put(PumaProfileConstantsIfc.iqama, consumerProfile.getCustomerIdNumber());
			attributes.put(PumaProfileConstantsIfc.defaultSubscriptionType, consumerProfile.getSubscriptionType());
			attributes.put(PumaProfileConstantsIfc.defaultSubSubscriptionType, consumerProfile.getLineType());
			attributes.put(PumaProfileConstantsIfc.defaultMsisdn, consumerProfile.getMSISDN());
			attributes.put(PumaProfileConstantsIfc.serviceAccountNumber, consumerProfile.getAccountNumber());

			LDAPManagerFactory.getInstance().getLDAPManagerImpl().updateUserAttributesOnLDAPByUserId(username, attributes);
			executionContext.audit(Level.INFO, "User information on ldap are updated ", className, methodName);

			//	Update LDAP User Group to the ConsumerUsers
			executionContext.log(Level.INFO, "handleNonMobilyUserSubscription > Add user to Group", className, methodName, null);
			LDAPManager.getInstance().updateUserGroup(username, "RegisteredUsers", "ConsumerUsers");
			
			updatedInLDAP = true;
			
			//LDAPManagerFactory.getInstance().getLDAPManagerImpl().updateUserGroup(username, "RegisteredUsers", "ConsumerUsers");
			executionContext.audit(Level.INFO, "User has been removed from LDAP registered users group and added to consumer one ", className, methodName);
			//userTransaction.commit();
			executionContext.audit(Level.INFO, "Transaction has been committed successfully ", className, methodName);
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, e.getMessage(), className, methodName, e);
			try {
				//userTransaction.rollback();
				executionContext.audit(Level.INFO, "Rollback trasacation.......................", className, methodName);
				
				if(updatedInDB) {
					executionContext.audit(Level.INFO, "Rollback trasacation from DB.......................", className, methodName);
					userAccount.setRoleId(RoleIdConstant.ROLE_ID_REGISTERED_USER);
					userAccount.setCustSegment("");
					
					userAccountDAO.updateUserRoledID(userAccount);
					executionContext.audit(Level.INFO, "Role id is rolled back on DB for userAcctId::"+userAcctId, className, methodName);
					
					userSubscriptionDAO.deleteUserSubscriptions(userAcctId);
					
					executionContext.audit(Level.INFO, "User Subscription deleted from DB for userAcctId::"+userAcctId, className, methodName);
				}
				
				if(updatedInLDAP) {
					executionContext.audit(Level.INFO, "Rollback trasacation from LDAP.......................", className, methodName);
					
					Map<String, Object> attributes = new HashMap<String,Object>();
					attributes.put(PumaProfileConstantsIfc.custSegment, null);
					attributes.put(PumaProfileConstantsIfc.defaultSubId, null);
					attributes.put(PumaProfileConstantsIfc.userAcctReference, null);
					attributes.put(PumaProfileConstantsIfc.roleId, RoleIdConstant.ROLE_ID_REGISTERED_USER);
					attributes.put(PumaProfileConstantsIfc.iqama, null);
					attributes.put(PumaProfileConstantsIfc.defaultSubscriptionType, null);
					attributes.put(PumaProfileConstantsIfc.defaultSubSubscriptionType, null);
					attributes.put(PumaProfileConstantsIfc.defaultMsisdn, null);
					attributes.put(PumaProfileConstantsIfc.serviceAccountNumber, null);
					executionContext.audit(Level.INFO, "Attributes " + attributes, className, methodName);
					executionContext.audit(Level.INFO, "Attributes " + attributes.values(), className, methodName);
					LDAPManagerFactory.getInstance().getLDAPManagerImpl().updateUserAttributesOnLDAPByUserId(username, attributes);
					executionContext.audit(Level.INFO, "LDAP Changes have been rollbeacked successfully due to exception", className, methodName);
					//LDAPManagerFactory.getInstance().getLDAPManagerImpl().updateUserGroup(username, "ConsumerUsers", "RegisteredUsers");
					LDAPManager.getInstance().updateUserGroup(username, "ConsumerUsers", "RegisteredUsers");
				}
			} catch (Exception e1) {
			}
			throw new ServiceException(e.getMessage(), e.getCause());
		}

	}
	
	/**
	 * @MethodName: sendPinCodeForNonMobily
	 * <p>
	 * Sends pin code for non mobily user during the process of linkin the number with his account
	 * 
	 * @param UserSubscription userSubscription
	 * @param String locale
	 * 
	 * @return String pinCode
	 * 
	 * @author Abu Sharaf
	 */
	@Override
	@Interceptors(ServiceInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String sendPinCodeForNonMobily(UserSubscription userSubscription, String locale){
		CustomerInfoVO consumerProfile = null;
		consumerProfile = new CustomerInfoJMSRepository().getCustProductProfileByMsisdn(userSubscription.getMsisdn());
		
		if(consumerProfile != null) {
			if(!consumerProfile.getCustomerIdNumber().equals(userSubscription.getNationalIdOrIqamaNumber()))
				throw new DataNotFoundException();
		} else {
			throw new DataNotFoundException();
		}
		
		if(isDefaultMSISDNExisted(userSubscription.getMsisdn()))
			throw new BackEndException(0, ErrorCodeConstantsIfc.DEFAULT_SUBS_EXISTS);

		PinCodeActivation pinCodeActivation = PinCodeActivationDAO.getInstance().getActivationCode(userSubscription.getMsisdn(),"" + ErrorCodeConstantsIfc.REGISTERATION_ACTIVATION_PROJECT_ID);

		String activationCode = null;

		if (pinCodeActivation == null)
		{
			activationCode = RandomGenerator.generateNumericRandom();
			pinCodeActivation = new PinCodeActivation();
			pinCodeActivation.setProjectId("" + ErrorCodeConstantsIfc.REGISTERATION_ACTIVATION_PROJECT_ID);
			pinCodeActivation.setActivationCode(activationCode);
			pinCodeActivation.setMsisdn(userSubscription.getMsisdn());
			pinCodeActivation.setRequestDate(new Timestamp(new Date().getTime()));
			PinCodeActivationDAO.getInstance().addActivationRequest(pinCodeActivation);
		}
		else
			activationCode = pinCodeActivation.getActivationCode();


		String messageId = (locale.equalsIgnoreCase("ar") ? ErrorCodeConstantsIfc.ACTIVATION_CODE_SMS_ID_AR : ErrorCodeConstantsIfc.ACTIVATION_CODE_SMS_ID_EN);
		if (SubscriptionTypeConstant.SUBS_TYPE_GSM == userSubscription.getSubscriptionType())
		{
			SMSSender.sendSMS(userSubscription.getMsisdn(), messageId, new String[] { activationCode });
			pinCodeActivation.setMsisdn(userSubscription.getMsisdn());
		}
		return activationCode;
	}

	/**
	 * @MethodName: isDefaultMSISDNExisted
	 * <p>
	 * Check if the user MSISDN that is used for registering a new one is not used in other account as a default subscription throw LDAP
	 * 
	 * @param String msisdn
	 * 
	 * @return boolean 
	 * 
	 * @author Abu Sharaf
	 */
	private boolean isDefaultMSISDNExisted(@NotNull String msisdn)
	{
		boolean userSubscriptionExist = false;
		if(null != LDAPManagerFactory.getInstance().getLDAPManagerImpl().findMobilyUserByAttribute(PumaProfileConstantsIfc.defaultMsisdn, msisdn))
			userSubscriptionExist = true;
		executionContext.log(Level.INFO, "Manage Subscription Service > isDefaultMSISDNDuplicated > Default MSISDN Exist =[" + userSubscriptionExist + "]", className,
				"isDefaultMSISDNDuplicated", null);
		return userSubscriptionExist;
	}

	/**
	 * @MethodName: isDefaultServiceAccountNumberExisted
	 * <p>
	 * Check if the user Service account number that is used for registering a new one is not used in other account as a default subscription throw LDAP
	 * 
	 * @param String serviceAccountNumber
	 * 
	 * @return boolean 
	 * 
	 * @author Abu Sharaf
	 */
	private boolean isDefaultServiceAccountNumberExisted(@NotNull String serviceAccountNumber)
	{
		boolean userSubscriptionExist = false;
		if(null != LDAPManagerFactory.getInstance().getLDAPManagerImpl().findMobilyUserByAttribute(PumaProfileConstantsIfc.serviceAccountNumber,serviceAccountNumber))
			userSubscriptionExist = true;
		executionContext.log(Level.INFO, "RegistrationService > isDefaultServiceAccountNumberDuplicated > Default Service Account Number Exist =["
				+ userSubscriptionExist + "]", className, "isDefaultServiceAccountNumberDuplicated", null);
		return userSubscriptionExist;
	}

	/**
	 * @MethodName: deleteSubscription
	 * <p>
	 * This method to delete user subscription 
	 * 
	 * @param String subscriptionId 
	 * 
	 * @author Amer Abu Gharbieh
	 */
	@Override
	@Interceptors(ServiceInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void deleteUserSubscription(long subscriptionId){
		
		executionContext.log(Level.INFO, "ManageSubscriptionService > deleteUserSubscription > subscriptionId = " + subscriptionId, className, "deleteUserSubscription", null);
		
		List<UserSubscription> list = UserSubscriptionDAO.getInstance().findByUserSubscriptionId(new Long(subscriptionId).intValue());
		if(list == null || list.size() == 0 ){
			throw new MobilyApplicationException("no subscription found with id : " + subscriptionId);
		}
		
		UserSubscription userSubscription = (UserSubscription) list.get(0);
		
		if(userSubscription.getIsDefault() != null && userSubscription.getIsDefault().equals("Y")){
			throw new MobilyApplicationException("can not delete default subscription");
		}
			
		List<UserAccountsSubscriptionsVO> userAccountSubscriptionlist = UserAccountsSubscriptiondao.getInstance().getUserAccountSubscriptionsByAccountId(userSubscription.getUserAcctId());
		
		executionContext.log(Level.INFO, "ManageSubscriptionService > deleteUserSubscription > userAccountSubscriptionlist Size = " + userAccountSubscriptionlist.size(), className, "deleteUserSubscription", null);
		executionContext.log(Level.INFO, "ManageSubscriptionService > deleteUserSubscription > ROle_Id = " + userAccountSubscriptionlist.get(0).getRoleId(), className, "deleteUserSubscription", null);
		
		if (userAccountSubscriptionlist.size()> 1){
			UserSubscriptionEligibilityDAO.getInstance().deleteEligibilityStatus(subscriptionId);
			UserSubscriptionDAO.getInstance().deleteUserSubscription(subscriptionId);	
		}
		else if (userAccountSubscriptionlist.size()== 1){
			executionContext.log(Level.INFO, "ManageSubscriptionService > deleteUserSubscription > Deletion of Last MSISDN will change Customer to NonMobily ", className, "deleteUserSubscription", null);
			UserSubscriptionEligibilityDAO.getInstance().deleteEligibilityStatus(subscriptionId);
			UserSubscriptionDAO.getInstance().deleteUserSubscription(subscriptionId);
			if (userAccountSubscriptionlist.get(0).getRoleId().equals("4")){
				convertConsumerUserIntoNonMobily (userAccountSubscriptionlist.get(0).getUserName(), userSubscription.getUserAcctId());
			}
		}
		
		
	}
	
	
	
	/**
	 * @author AbuSharaf
	 * Utility method to handle Non mobily user and covert the user to consumer one.
	 * 
	 * @param User Account ID <long>userAcctID</long>
	 * @param User Subscription VO <UserSubscription>userSubscription</UserSubscription>
	 * 
	 * @return void
	 * @throws {@link BackEndException, DataNotFoundException}
	 */
	@Override
	@Interceptors(ServiceInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void addNonMobilyUserSubscription(@NotNull String username,@NotNull UserSubscription userSubscription) {
		
		final String methodName = "addNonMobilyUserSubscription";
		executionContext.log(Level.INFO, "Add new Subscription for Non-Mobily User", className, methodName);

		if (FormatterUtility.isEmpty(userSubscription.getServiceAcctNumber())
					&& FormatterUtility.isEmpty(userSubscription.getMsisdn())){
			executionContext.audit(Level.INFO, "MSISDN or Service Account number is EMPTY.....!", className, methodName);
			throw new BackEndException(ErrorCodeConstantsIfc.SUB_VO_NOT_VALID, null);
		}
		
		UserAccountDAO userAccountDAO = UserAccountDAO.getInstance();
		UserSubscriptionDAO userSubscriptionDAO = UserSubscriptionDAO.getInstance();
		UserAccount userAccount = new UserAccount();
		UserSubscription userSubscriptionVO = new UserSubscription();
		long userAcctId;
		//UserTransaction userTransaction=null;
		CustomerInfoVO consumerProfile = null;

		executionContext.audit(Level.INFO, "User Subscription type " + userSubscription.getSubscriptionType(), className, methodName);
		//Validating User Account INFO with Customer Profile Reply.
			if (SubscriptionTypeConstant.SUBS_TYPE_GSM == userSubscription.getSubscriptionType().intValue()
					|| SubscriptionTypeConstant.SUBS_TYPE_CONNECT == userSubscription.getSubscriptionType().intValue()) {
				consumerProfile = new CustomerInfoJMSRepository().getCustProductProfileByMsisdn(userSubscription.getMsisdn());
				
				if(isDefaultMSISDNExisted(userSubscription.getMsisdn())){
					executionContext.audit(Level.INFO, "MSISDN =["+userSubscription.getMsisdn()+"] is already a DEFAULT of other account.....!", className, methodName);
					throw new BackEndException(0, ErrorCodeConstantsIfc.DEFAULT_SUBS_EXISTS);
				}
			} else { // for other subscriptions
				consumerProfile = new CustomerInfoJMSRepository().getCustProductProfileByServAcctNumber(userSubscription.getServiceAcctNumber());
				
				if(isDefaultServiceAccountNumberExisted(userSubscription.getServiceAcctNumber())){
					executionContext.audit(Level.INFO, "ServiceAccountNumber =["+userSubscription.getMsisdn()+"] is already a DEFAULT of other account.....!", className, methodName);
					throw new BackEndException(0, ErrorCodeConstantsIfc.DEFAULT_SUBS_EXISTS);
				}
			}
		
		try{
			//userTransaction=sessionContext.getUserTransaction();
			//userTransaction.begin();
			executionContext.audit(Level.INFO, "User Transaction started", className, methodName);
			//Getting User Account ID
				userAcctId = userAccountDAO.findByUserName(username.toUpperCase()).get(0).getUserAcctId();
			executionContext.audit(Level.INFO, "User Acct ID " + userAcctId, className, methodName);
			
			//Updating Non mobily user status
				userAccount.setRoleId(RoleIdConstant.ROLE_ID_CONSUMER_CUSTOMER);
				userAccount.setCustSegment(consumerProfile.getCustomerSegment());
				userAccount.setUserAcctId(userAcctId);

			userAccountDAO.updateUserRoledID(userAccount);
			executionContext.audit(Level.INFO, "information are updated on DB for user_accounts table", className, methodName);

			// Insert New subscription for non mobily user
				userSubscriptionVO.setUserAcctId(userAcctId);
				userSubscriptionVO.setSubscriptionId(userSubscriptionDAO.getUserSubscriptionIdSeq());
				userSubscriptionVO.setIsDefault(Boolean.TRUE);
				userSubscriptionVO.setMsisdn(consumerProfile.getMSISDN());
				userSubscriptionVO.setServiceAcctNumber(consumerProfile.getAccountNumber());
				userSubscriptionVO.setPackageId(consumerProfile.getPackageId());
				userSubscriptionVO.setCreatedDate(new Timestamp(new Date().getTime()));
				userSubscriptionVO.setIsActive("Y");
				userSubscriptionVO.setCustomerType(consumerProfile.getPayType());
				userSubscriptionVO.setSubscriptionType(consumerProfile.getSubscriptionType());
				userSubscriptionVO.setSubscriptionName(SubscriptionTypeConstant.getSubscriptionName(consumerProfile.getSubscriptionType()));
				userSubscriptionVO.setLineType(consumerProfile.getLineType());
				userSubscriptionVO.setNationalIdOrIqamaNumber(consumerProfile.getCustomerIdNumber());

			userSubscriptionDAO.addUserSubscripton(userSubscriptionVO);
			executionContext.audit(Level.INFO, "new record is added in User Subscription for this user " + username, className, methodName);

			//Update LDAP Attribute for non mobily user
				Map<String, Object> attributes = new HashMap<String,Object>();
					attributes.put(PumaProfileConstantsIfc.custSegment, consumerProfile.getCustomerSegment());
					attributes.put(PumaProfileConstantsIfc.defaultSubId, String.valueOf(userSubscriptionVO.getSubscriptionId()));
					attributes.put(PumaProfileConstantsIfc.userAcctReference, String.valueOf(userAcctId));
					attributes.put(PumaProfileConstantsIfc.roleId, RoleIdConstant.ROLE_ID_CONSUMER_CUSTOMER);
					attributes.put(PumaProfileConstantsIfc.iqama, consumerProfile.getCustomerIdNumber());
					attributes.put(PumaProfileConstantsIfc.defaultSubscriptionType, consumerProfile.getSubscriptionType());
					attributes.put(PumaProfileConstantsIfc.defaultSubSubscriptionType, consumerProfile.getLineType());
					attributes.put(PumaProfileConstantsIfc.defaultMsisdn, consumerProfile.getMSISDN());
					attributes.put(PumaProfileConstantsIfc.serviceAccountNumber, consumerProfile.getAccountNumber());
	
				LDAPManagerFactory.getInstance().getLDAPManagerImpl().updateUserAttributesOnLDAPByUserId(username, attributes);
				executionContext.audit(Level.INFO, "User information on ldap are updated ", className, methodName);

			//Update LDAP User Group to the ConsumerUsers
				executionContext.log(Level.INFO, "createNewUserOnLDAP Add user to Group", className, methodName, null);
				LDAPManagerFactory.getInstance().getLDAPManagerImpl().updateUserGroup(username, RegisterationConstantsIfc.REGISTERED_USER_GROUP, RegisterationConstantsIfc.CONSUMER_GROUP);
				executionContext.audit(Level.INFO, "User has been removed from LDAP registered users group and added to consumer one ", className, methodName);
				//userTransaction.commit();
				executionContext.audit(Level.INFO, "Transaction has been committed successfully ", className, methodName);
		}catch (Exception e) {
			executionContext.log(Level.SEVERE, "Add new Subscription for Non-Mobily User Exception > "+e.getMessage(), className, methodName, e);
			try {
				//userTransaction.rollback();
				executionContext.audit(Level.INFO, "Transaction has been rollbeacked successfully due to exception", className, methodName);
				Map<String, Object> attributes = new HashMap<String,Object>();
					attributes.put(PumaProfileConstantsIfc.custSegment, null);
					attributes.put(PumaProfileConstantsIfc.defaultSubId, null);
					attributes.put(PumaProfileConstantsIfc.userAcctReference, null);
					attributes.put(PumaProfileConstantsIfc.roleId, RoleIdConstant.ROLE_ID_REGISTERED_USER);
					attributes.put(PumaProfileConstantsIfc.iqama, null);
					attributes.put(PumaProfileConstantsIfc.defaultSubscriptionType, null);
					attributes.put(PumaProfileConstantsIfc.defaultSubSubscriptionType, null);
					attributes.put(PumaProfileConstantsIfc.defaultMsisdn, null);
					attributes.put(PumaProfileConstantsIfc.serviceAccountNumber, null);
				executionContext.audit(Level.INFO, "Attributes " + attributes, className, methodName);
				executionContext.audit(Level.INFO, "Attributes " + attributes.values(), className, methodName);
				LDAPManagerFactory.getInstance().getLDAPManagerImpl().updateUserAttributesOnLDAPByUserId(username, attributes);
				executionContext.audit(Level.INFO, "LDAP Changes have been rollbeacked successfully due to exception", className, methodName);
				LDAPManagerFactory.getInstance().getLDAPManagerImpl().updateUserGroup(username, RegisterationConstantsIfc.CONSUMER_GROUP, RegisterationConstantsIfc.REGISTERED_USER_GROUP);
			} catch (Exception e1) {
				executionContext.log(Level.SEVERE, "Add new Subscription for Non-Mobily User > during rollback > Exception > "+e1.getMessage(), className, methodName, e);
			}
			
			throw new ServiceException(e.getMessage(), e.getCause());
		}
	}

	/**
	 * 
	 */
	@Override
	@Interceptors(ServiceInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<UserAccountVO> getUserInfoByUsername(String userName)
	{
		List<UserAccountVO> userAccountVoList = 
				sa.com.mobily.eportal.cacheinstance.dao.UserAccountDAO.getInstance().getUserAccountVOByUserName(userName);		
		
		return userAccountVoList;
	}
	
	public void convertConsumerUserIntoNonMobily(String uid, Long userAccountId) {
		FailedOperationInfo failedOperation = null;
		String methodName = "convertConsumerUserIntoNonMobily";
		executionContext.audit(Level.INFO, "converting consumer user into non mobily", className, methodName);
//		Update ldap user information defaultSubId, roleId, defaultSubscriptionType, defaultMsisdn, custSegment.
		Map<String, Object> attributes = new HashMap<String, Object>();
		
		attributes.put(PumaProfileConstantsIfc.roleId, String.valueOf(RoleIdConstant.ROLE_ID_REGISTERED_USER));	
		attributes.put(PumaProfileConstantsIfc.defaultMsisdn, null);
		attributes.put(PumaProfileConstantsIfc.custSegment, null);		
		attributes.put(PumaProfileConstantsIfc.defaultSubId, null);	
		attributes.put(PumaProfileConstantsIfc.iqama, null);
		attributes.put(PumaProfileConstantsIfc.serviceAccountNumber, null);
		attributes.put(PumaProfileConstantsIfc.defaultSubscriptionType, "0");
		//attributes.put(PumaProfileConstantsIfc.defaultSubscriptionType, null);
		//attributes.put(PumaProfileConstantsIfc.defaultSubSubscriptionType, null);		
		
		
		try {
		executionContext.audit(Level.INFO, "updating ldap customer information to make it non monbily user", className, methodName);
		LDAPManagerFactory.getInstance().getLDAPManagerImpl().updateUserAttributesOnLDAPByUserId(uid, attributes);
		executionContext.audit(Level.INFO, "ldap information updated. uid: " + uid, className, methodName);
		//Update group  "ConsumerUsers" "RegisteredUsers"
		
		executionContext.audit(Level.INFO, "updating ldap information group to registered users", className, methodName);
		LDAPManagerFactory.getInstance().getLDAPManagerImpl().updateUserGroup(uid, "ConsumerUsers","RegisteredUsers");
		executionContext.audit(Level.INFO, "updated ldap information group to registered users", className, methodName);
		// updating customer segment in user account
		executionContext.audit(Level.INFO, "updating the cust sengment in user account", className, methodName);
		UserAccountDAO.getInstance().updateCustomerSegmentAndRoleId(null,String.valueOf(RoleIdConstant.ROLE_ID_REGISTERED_USER), userAccountId);
		executionContext.audit(Level.INFO, "cust segment updated in user account", className, methodName);
		}
		catch (Exception e) {
			UserAccount userAccount = new UserAccount();
			userAccount.setUserAcctId(userAccountId);
			userAccount.setUserName(uid);
			
			/*failedOperation = populateFailedOperationInfo(userAccount, RegisterationConstantsIfc.CONVERT_CUSTOMER_TO_NON_MOBILY);
			try{
				FailedOperaionDAO.getInstance().saveFailedOperation(failedOperation);
			}
			catch (Exception e1){
				executionContext.log(Level.SEVERE,"Exception while Insert in FailedOperation for DB > error message::"+e1.getMessage(), methodName,className);	
			}*/
			//Here we need to call failed Operation
    		executionContext.audit(Level.SEVERE, "Exception while Convert user to NonMobily > Exception::"+e.getMessage(), className, "convertConsumerUserIntoNonDefault");
    		//returnFlag = false;
		}
	}
	
	
	public void convertConsumerUserIntoNonDefault(String uid, Long userAccountId) {
		FailedOperationInfo failedOperation = null;
		String methodName = "convertConsumerUserIntoNonDefault";
		executionContext.audit(Level.INFO, "converting consumer user into non default", className, methodName);
//		Update ldap user information defaultSubId, roleId, defaultSubscriptionType, defaultMsisdn, custSegment.
		Map<String, Object> attributes = new HashMap<String, Object>();
		attributes.put(PumaProfileConstantsIfc.roleId, String.valueOf(RoleIdConstant.ROLE_ID_REGISTERED_MOBILY_USER));		
		attributes.put(PumaProfileConstantsIfc.defaultMsisdn, null);
		attributes.put(PumaProfileConstantsIfc.custSegment, null);		
		attributes.put(PumaProfileConstantsIfc.defaultSubId, null);	
		attributes.put(PumaProfileConstantsIfc.iqama, null);
		attributes.put(PumaProfileConstantsIfc.serviceAccountNumber, null);
		attributes.put(PumaProfileConstantsIfc.defaultSubscriptionType, "0");
		//attributes.put(PumaProfileConstantsIfc.defaultSubscriptionType, null);
		//attributes.put(PumaProfileConstantsIfc.defaultSubSubscriptionType, null);		
		
	try{
		executionContext.audit(Level.INFO, "updating ldap customer information to make it non deafult MSISDN user", className, methodName);
		LDAPManagerFactory.getInstance().getLDAPManagerImpl().updateUserAttributesOnLDAPByUserId(uid, attributes);
		executionContext.audit(Level.INFO, "ldap information updated. uid: " + uid, className, methodName);
		// updating customer segment in user account
		executionContext.audit(Level.INFO, "updating the cust sengment in user account", className, methodName);
		UserAccountDAO.getInstance().updateCustomerSegmentAndRoleId(null,String.valueOf(RoleIdConstant.ROLE_ID_REGISTERED_MOBILY_USER), userAccountId);
		executionContext.audit(Level.INFO, "cust segment updated in user account", className, methodName);
	}
	catch (Exception e) {
		UserAccount userAccount = new UserAccount();
		userAccount.setUserAcctId(userAccountId);
		userAccount.setUserName(uid);
		/*failedOperation = populateFailedOperationInfo(userAccount, RegisterationConstantsIfc.CONVERT_CUSTOMER_TO_NON_DEFAULT);
		try{
			FailedOperaionDAO.getInstance().saveFailedOperation(failedOperation);
		}
		catch (Exception e1){
			executionContext.log(Level.SEVERE,"Exception while Insert in FailedOperation for DB > error message::"+e1.getMessage(), methodName,className);	
		}*/
		//Here we need to call failed Operation
		executionContext.audit(Level.SEVERE, "Exception while Convert user to NonDefault > Exception::"+e.getMessage(), className, "convertConsumerUserIntoNonDefault");
		//returnFlag = false;
	}
	}
}
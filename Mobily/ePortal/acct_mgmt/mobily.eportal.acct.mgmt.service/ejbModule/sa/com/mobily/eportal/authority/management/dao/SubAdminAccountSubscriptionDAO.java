package sa.com.mobily.eportal.authority.management.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.collections.CollectionUtils;

import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.authority.mgmt.constants.AuthorityMgmtConstants;
import sa.com.mobily.eportal.authority.mgmt.vo.CAPUserAccountVO;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

public class SubAdminAccountSubscriptionDAO extends AbstractDBDAO<CAPUserAccountVO> implements AcctMgmtLoggerConstantsIfc
{

	private ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(ACCT_MGMT_AUTHORITY_MGMT);
	
	private String className = SubAdminAccountSubscriptionDAO.class.getName();
			
	private static SubAdminAccountSubscriptionDAO instance = new SubAdminAccountSubscriptionDAO();

	
	protected SubAdminAccountSubscriptionDAO() {
	     super(DataSources.DEFAULT);
	}
	  
	public static synchronized SubAdminAccountSubscriptionDAO getInstance() {
	    if (instance == null) {
	        instance = new SubAdminAccountSubscriptionDAO();
	    }
	    return instance;
	}
	
    private static final String GET_USER_SUBSCRIPTION_SEQ = 
    		"SELECT CORPORATE_ACCOUNT_SEQ.NEXTVAL AS SUBSCRIPTION_ID FROM DUAL";


    private static final String USER_SUBSCRIPTION_ADD = 
            "INSERT INTO CORPORATE_SUBSCRIPTION " +
            "(SUBSCRIPTION_ID,USER_ACCT_ID,MASTER_BILLING_ACC,MOBILE,USER_ACTIVATED,STATUS,IS_SUB_ADMIN) VALUES " +
            "(?, ?, ?, ?, ?, ?,?)";
   
    private static final String DELETE_CORPORATE_SUBSCRIPTION_AND_USER_ACCT_BY_USERACCID = 
            "BEGIN DELETE FROM CORPORATE_SUBSCRIPTION WHERE USER_ACCT_ID=?; DELETE FROM USER_ACCOUNTS WHERE USER_ACCT_ID=?; END;";

    private static final String GET_SUB_ADMINS_BY_BILLING_ACCT_NUM = 
            "SELECT U.USER_ACCT_ID,USER_NAME,FIRST_NAME,SUBSCRIPTION_ID,C.MOBILE,C.STATUS " +
            "FROM USER_ACCOUNTS U,CORPORATE_SUBSCRIPTION C " +
            "WHERE U.USER_ACCT_ID = C.USER_ACCT_ID AND MASTER_BILLING_ACC = ? AND IS_SUB_ADMIN = ?";    
  
    private static final String DISABLE_SUB_ADMIN_USER =
    		"UPDATE CORPORATE_SUBSCRIPTION SET STATUS = 0 WHERE USER_ACCT_ID =? AND IS_SUB_ADMIN =? AND MASTER_BILLING_ACC = ? ";
    
    private static final String GET_CAP_MOBILE_USER_NAME_BY_USER_ACCT_ID =
    		"SELECT USER_NAME,MOBILE,STATUS,SUBSCRIPTION_ID FROM USER_ACCOUNTS U, CORPORATE_SUBSCRIPTION  C WHERE C.USER_ACCT_ID =? AND U.USER_ACCT_ID = C.USER_ACCT_ID AND IS_SUB_ADMIN =? AND MASTER_BILLING_ACC = ? ";

    private static final String GET_CAP_MOBILE_BY_BAN=
    		"SELECT MOBILE,SUBSCRIPTION_ID FROM CORPORATE_SUBSCRIPTION WHERE MOBILE =?  AND MASTER_BILLING_ACC = ? ";
    
    
    public boolean getCapMobileForDuplicateCheck(String mobileNumber, String billingAccountNumber){
    	
    	executionContext.audit(Level.INFO, "SubAdminAccountSubscriptionDAO >> getCapMobileForDuplicateCheck>> mobileNumber::"+mobileNumber, className, "getCapMobileForDuplicateCheck");
    	executionContext.audit(Level.INFO, "SubAdminAccountSubscriptionDAO >> getCapMobileForDuplicateCheck>> billingAccountNumber::"+billingAccountNumber, className, "getCapMobileForDuplicateCheck");
    	//String capMobile=null;
    	boolean capMobileExists=false;
		List<CAPUserAccountVO> accountVOList = query(GET_CAP_MOBILE_BY_BAN,new Object[] {mobileNumber, billingAccountNumber});
		if (CollectionUtils.isNotEmpty(accountVOList)) {
			capMobileExists=true;
			/*CAPUserAccountVO accountVO = accountVOList.get(0);
			if(accountVO != null){
				
				capMobile =accountVO.getCapMobile();
			}*/
		}
		//executionContext.audit(Level.INFO, "SubAdminAccountSubscriptionDAO >> getCapMobileForDuplicateCheck>> capMobile::"+capMobile, className, "getCapMobileForDuplicateCheck");
		executionContext.audit(Level.INFO, "SubAdminAccountSubscriptionDAO >> getCapMobileForDuplicateCheck>> capMobileExists::"+capMobileExists, className, "getCapMobileForDuplicateCheck");
		return capMobileExists;
    }
    public CAPUserAccountVO getCapMobileUserName(String userAcctId,String billingAccountNumber){
    	
    	executionContext.audit(Level.INFO, "SubAdminAccountSubscriptionDAO >> getCapMobileUserName>> userAcctId::"+userAcctId, className, "getCapMobileUserName");
    	executionContext.audit(Level.INFO, "SubAdminAccountSubscriptionDAO >> getCapMobileUserName>> billingAccountNumber::"+billingAccountNumber, className, "getCapMobileUserName");
    	//String mobileNumber=null;
    	CAPUserAccountVO accountVO =null;
		List<CAPUserAccountVO> accountVOList = query(GET_CAP_MOBILE_USER_NAME_BY_USER_ACCT_ID,new Object[] {userAcctId,AuthorityMgmtConstants.CORPORATE_SUBSCRIPTION_SUB_ADMIN, billingAccountNumber});
		if (CollectionUtils.isNotEmpty(accountVOList)) {
			
			accountVO = accountVOList.get(0);
			/*if(accountVO != null)
				mobileNumber =accountVO.getCapMobile();*/
		}
		executionContext.audit(Level.INFO, "SubAdminAccountSubscriptionDAO >> getCapMobileUserName>> accountVO::"+accountVO, className, "getCapMobileUserName");
		return accountVO;
    	
    }
    public int disableSubAdminUser(String userAcctId,String billingAccountNumber){
    	executionContext.audit(Level.INFO, "SubAdminAccountSubscriptionDAO >> disableSubAdminUser>> userAcctId::"+userAcctId, className, "disableSubAdminUser");
    	executionContext.audit(Level.INFO, "SubAdminAccountSubscriptionDAO >> disableSubAdminUser>> billingAccountNumber::"+billingAccountNumber, className, "disableSubAdminUser");
		return update(DISABLE_SUB_ADMIN_USER, userAcctId , AuthorityMgmtConstants.CORPORATE_SUBSCRIPTION_SUB_ADMIN, billingAccountNumber);

    }
    
    public int addSubscription(CAPUserAccountVO dto) {
    	executionContext.audit(Level.INFO, "SubAdminAccountSubscriptionDAO >> addSubscription>> MSISDN::"+dto.getCapMobile(), className, "addSubscription");
		return update(USER_SUBSCRIPTION_ADD, dto.getSubscriptionId(), dto.getUserAcctId(), dto.getBillingAccountNumber(), 
				dto.getCapMobile(),AuthorityMgmtConstants.CORPORATE_SUBSCRIPTION_USER_ACTIVATED,AuthorityMgmtConstants.CORPORATE_SUBSCRIPTION_STATUS_ACTIVE,AuthorityMgmtConstants.CORPORATE_SUBSCRIPTION_SUB_ADMIN);
    }
    
    public String getUserSubscriptionId() {
    	
    	String subscriptionId=null;
		List<CAPUserAccountVO> accountVOList = query(GET_USER_SUBSCRIPTION_SEQ);
		if (CollectionUtils.isNotEmpty(accountVOList)) {
			
			CAPUserAccountVO accountVO = accountVOList.get(0);
			if(accountVO != null)
				subscriptionId =accountVO.getSubscriptionId();
		}
		executionContext.audit(Level.INFO, "SubAdminAccountSubscriptionDAO >> getUserSubscriptionId>> subscriptionId::"+subscriptionId, className, "getUserSubscriptionId");
		return subscriptionId;
    }
    
    public int deleteCorporateSubscriptionAndUserAccount(Long userAccountId) {
        return update(DELETE_CORPORATE_SUBSCRIPTION_AND_USER_ACCT_BY_USERACCID, new Object[] {userAccountId, userAccountId});
    }
    
	
    public List<CAPUserAccountVO> getSubAdminsList(String billingAccountNumber){
    	
    	executionContext.audit(Level.INFO, "SubAdminAccountSubscriptionDAO >> getSubAdminsList>> billingAccountNumber::"+billingAccountNumber, className, "getSubAdminsList");
    	List<CAPUserAccountVO> accountVOList = query(GET_SUB_ADMINS_BY_BILLING_ACCT_NUM,new Object[] { billingAccountNumber,AuthorityMgmtConstants.CORPORATE_SUBSCRIPTION_SUB_ADMIN});
    	return accountVOList;
    	
    }
    
	@Override
	protected CAPUserAccountVO mapDTO(ResultSet rs) throws SQLException
	{
		CAPUserAccountVO accountVO = new CAPUserAccountVO();

		if (rs.getMetaData().getColumnCount() ==2) {
			accountVO.setCapMobile(rs.getString("MOBILE"));
		}
		if (rs.getMetaData().getColumnCount() ==4) {
			accountVO.setCapMobile(rs.getString("MOBILE"));
			accountVO.setStatus(rs.getString("STATUS"));
			accountVO.setUserName(rs.getString("USER_NAME"));
		}
		else if (rs.getMetaData().getColumnCount() > 4) {
			
			accountVO.setCapMobile(rs.getString("MOBILE"));
			accountVO.setUserAcctId(rs.getString("USER_ACCT_ID"));
			accountVO.setStatus(rs.getString("STATUS"));
			accountVO.setUserName(rs.getString("USER_NAME"));
			accountVO.setName(rs.getString("FIRST_NAME"));
    	}
		
		accountVO.setSubscriptionId(rs.getString("SUBSCRIPTION_ID"));
		return accountVO;
	}
	
}

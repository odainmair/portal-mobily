package sa.com.mobily.eportal.blockline.service;

import static javax.ejb.TransactionAttributeType.NOT_SUPPORTED;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.Interceptors;

import sa.com.mobily.eportal.blockline.jms.BlockLineJMSRepository;
import sa.com.mobily.eportal.blockline.service.view.BlockLineServiceLocal;
import sa.com.mobily.eportal.blockline.service.view.BlockLineServiceRemote;
import sa.com.mobily.eportal.blockline.valueobjects.BlockLineRequestVO;
import sa.com.mobily.eportal.blockline.valueobjects.BlockLineResponseVO;
import sa.com.mobily.eportal.blockline.valueobjects.CheckStatusRequestVO;
import sa.com.mobily.eportal.blockline.valueobjects.CheckStatusResponseVO;
import sa.com.mobily.eportal.common.interceptor.ServiceInterceptor;

/**
 * Session Bean implementation class BlockLineService
 */

@Stateless
@Local(BlockLineServiceLocal.class)
@LocalBean
@Remote(BlockLineServiceRemote.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class BlockLineService implements BlockLineServiceLocal, BlockLineServiceRemote
{

	/**
	 * @MethodName: blockLine
	 * <p>
	 * Perform block line procedure
	 * 
	 * @param BlockLineRequestVO blockLineRequestBO
	 * 
	 * @return BlockLineResponseVO
	 * 
	 * @author Abu Sharaf
	 */
	@Override
	@TransactionAttribute(value = NOT_SUPPORTED)
	@Interceptors(ServiceInterceptor.class)
	public BlockLineResponseVO blockLine(BlockLineRequestVO blockLineRequestBO)
	{
		BlockLineJMSRepository blockLineJMSRepository = new BlockLineJMSRepository();
		return blockLineJMSRepository.blockLine(blockLineRequestBO);
	}

	/**
	 * @MethodName: checkStatus
	 * <p>
	 * Check if the user MSISDN is blocked, or not blocked
	 * 
	 * @param CheckStatusRequestVO checkStatusRequestBO
	 * 
	 * @return CheckStatusResponseVO 
	 * 
	 * @author Abu Sharaf
	 */
	@Override
	@TransactionAttribute(value = NOT_SUPPORTED)
	@Interceptors(ServiceInterceptor.class)
	public CheckStatusResponseVO checkStatus(CheckStatusRequestVO checkStatusRequestBO)
	{
		BlockLineJMSRepository blockLineJMSRepository = new BlockLineJMSRepository();
		return blockLineJMSRepository.checkStatus(checkStatusRequestBO);
	}
}
/**
 * 
 */
package sa.com.mobily.eportal.pack.mgmt.service;

import static javax.ejb.TransactionAttributeType.NOT_SUPPORTED;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.Interceptors;

import sa.com.mobily.eportal.common.interceptor.ServiceInterceptor;
import sa.com.mobily.eportal.pack.mgmt.jms.PackageMgmtJMSRepository;
import sa.com.mobily.eportal.pack.mgmt.service.view.PackageMgmtServiceLocal;
import sa.com.mobily.eportal.pack.mgmt.service.view.PackageMgmtServiceRemote;
import sa.com.mobily.eportal.pack.mgmt.vo.PackageConversionReqVO;
import sa.com.mobily.eportal.pack.mgmt.vo.PackageHistoryRequestVO;
import sa.com.mobily.eportal.pack.mgmt.vo.PackageHistoryVO;

/**
 * @author n.gundluru.mit
 *
 */
@Stateless
@Local(PackageMgmtServiceLocal.class)
@LocalBean
@Remote(PackageMgmtServiceRemote.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PackageMgmtService implements PackageMgmtServiceLocal, PackageMgmtServiceRemote {

	@Override
	@TransactionAttribute(value = NOT_SUPPORTED)
	@Interceptors(ServiceInterceptor.class)
	public PackageHistoryVO getPackageChangeHistory(PackageHistoryRequestVO requestVO) {
		
		return 	new PackageMgmtJMSRepository().getPackageChangeHistory(requestVO);
	}

	@Override
	@TransactionAttribute(value = NOT_SUPPORTED)
	@Interceptors(ServiceInterceptor.class)
	public void doPackageConversion(PackageConversionReqVO requestVO) {
		
		new PackageMgmtJMSRepository().doPackageConversion(requestVO);		
	}

}


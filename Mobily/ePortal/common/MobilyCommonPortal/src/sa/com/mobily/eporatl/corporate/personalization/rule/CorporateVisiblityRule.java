/**
 * 
 */
package sa.com.mobily.eporatl.corporate.personalization.rule;

import java.io.Serializable;

import sa.com.mobily.eportal.cache.util.CacheManagerServiceUtil;
import sa.com.mobily.eportal.cacheinstance.valueobjects.SessionVO;

import com.ibm.websphere.personalization.RequestContext;
import com.ibm.websphere.personalization.applicationObjects.SelfInitializingApplicationObject;

/**
 * @author Mohamed Shalaby
 * 
 */
public class CorporateVisiblityRule implements SelfInitializingApplicationObject, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9121758918553760135L;

//	private boolean hasDia;
//
//	private boolean hasIpvpn;
//
//	private boolean hasEthernet;
	
	private boolean hasIceCube;
	private boolean isPrimaryAP;
	private boolean isSubAdminAP;

	@Override
	public void init(RequestContext requestContext)
	{
		getPersonalizedInfoFromSession(requestContext);
		requestContext.setRequestAttribute("corporateVisibilityProfile", this);

	}

	private void getPersonalizedInfoFromSession(RequestContext requestContext)
	{
		String username = requestContext.getPznRequestObjectInterface().getUserPrincipal();
		String sessionId = requestContext.getSessionInfo();
		sessionId = sessionId.substring(sessionId.indexOf("_sessionId=") + 11, sessionId.indexOf("_sessionId=") + 34);
		SessionVO sessionVO = CacheManagerServiceUtil.getCachedData(sessionId, username);
//		this.setHasDia(sessionVO.isHasDiaSubscribers());
//		this.setHasEthernet(sessionVO.isHasEthernetSubscribers());
//		this.setHasIpvpn(sessionVO.isHasIpVpnSubscribers());
		this.setHasIceCube(sessionVO.isHasIceCubeEligibility());
		
		try{
			
		}catch (Exception e) {

		}
		
	}
	
	public boolean getHasIceCube()
	{
		return hasIceCube;
	}

	public void setHasIceCube(boolean hasIceCube)
	{
		this.hasIceCube = hasIceCube;
	}

	/**
	 * @return the isPrimaryAP
	 */
	public boolean isPrimaryAP()
	{
		return isPrimaryAP;
	}

	/**
	 * @param isPrimaryAP the isPrimaryAP to set
	 */
	public void setPrimaryAP(boolean isPrimaryAP)
	{
		this.isPrimaryAP = isPrimaryAP;
	}

	/**
	 * @return the isSubAdminAP
	 */
	public boolean isSubAdminAP()
	{
		return isSubAdminAP;
	}

	/**
	 * @param isSubAdminAP the isSubAdminAP to set
	 */
	public void setSubAdminAP(boolean isSubAdminAP)
	{
		this.isSubAdminAP = isSubAdminAP;
	}
	

//	public boolean getHasDia()
//	{
//		return hasDia;
//	}
//
//	public void setHasDia(boolean hasDia)
//	{
//		this.hasDia = hasDia;
//	}
//
//	public boolean getHasIpvpn()
//	{
//		return hasIpvpn;
//	}
//
//	public void setHasIpvpn(boolean hasIpvpn)
//	{
//		this.hasIpvpn = hasIpvpn;
//	}
//
//	public boolean getHasEthernet()
//	{
//		return hasEthernet;
//	}
//
//	public void setHasEthernet(boolean hasEthernet)
//	{
//		this.hasEthernet = hasEthernet;
//	}

}
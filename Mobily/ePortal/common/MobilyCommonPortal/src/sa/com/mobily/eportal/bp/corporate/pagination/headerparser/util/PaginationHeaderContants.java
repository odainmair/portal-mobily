package sa.com.mobily.eportal.bp.corporate.pagination.headerparser.util;

public class PaginationHeaderContants {

	public static final String HEADER_RANGE_ATTRIBUTE = "Range";
	public static final String HEADER_CONTENT_RANGE_ATTRIBUTE = "Content-Range";
	public static final String HEADER_ITEMS_ATTRIBUTE_WITH_SPACE = "items ";
	public static final String HEADER_ITEMS_ATTRIBUTE_WITH_EQUAL = "items=";
	public static final String SLASH_SYMBOL = "/";
	public static final String DASH_SYMBOL  = "-";
}

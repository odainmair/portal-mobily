package sa.com.mobily.eportal.bp.corporate.pagination.headerparser.util;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import sa.com.mobily.eportal.common.service.vo.PagingVO;

public class PaginationHeaderParser {
	
	private PaginationHeaderParser(){};
	
	private static PaginationHeaderParser  INSTANCE = new PaginationHeaderParser();
	
	public static PaginationHeaderParser getInstance(){
		return INSTANCE;
	}
	
	
	/**
	 * This method was created to parse the HTTP request range attribute of pagination
	 * @param request Portlet request
	 * @return PageVO
	 * @author Abdalla Khalaf - SBM
	 */
	public PagingVO parseRangeHeader(PortletRequest request/*, Integer totalItems*/){

		String range = request.getProperty(PaginationHeaderContants.HEADER_RANGE_ATTRIBUTE);
		Integer startIndex;
		Integer endIndex;

		if(range == null || range.length() == 0) {
			startIndex = 1;
			endIndex = 10;
		} else {
			String temp = range.replaceAll(PaginationHeaderContants.HEADER_ITEMS_ATTRIBUTE_WITH_EQUAL, "");
			int index = temp.indexOf(PaginationHeaderContants.DASH_SYMBOL);

			startIndex = Integer.parseInt(temp.substring(0, index))+1;
			endIndex = Integer.parseInt(temp.substring(index + 1, temp.length()))+1;
			
			/*if(totalItems != null && endIndex >= totalItems) {
				endIndex = totalItems - 1;
			}*/
			
		}
		PagingVO pagingVO = new PagingVO(startIndex, endIndex, /*totalItems*/0);
		
		return pagingVO;
	}
	
	/**
	 * This method was created to set required attribute "Content-Range" in HTTP response
	 * @param response Portlet response
	 * @author Abdalla Khalaf - SBM
	 */
	public void setContentRangeAttribute(PortletResponse response, PagingVO pagingVO){
		StringBuffer contentRangeValue = new StringBuffer();
		contentRangeValue.append(PaginationHeaderContants.HEADER_ITEMS_ATTRIBUTE_WITH_SPACE);
		contentRangeValue.append(pagingVO.getStartIndex());
		contentRangeValue.append(PaginationHeaderContants.DASH_SYMBOL);
		contentRangeValue.append(pagingVO.getEndIndex());
		contentRangeValue.append(PaginationHeaderContants.SLASH_SYMBOL);
		contentRangeValue.append(pagingVO.getTotalItems());
		response.setProperty(PaginationHeaderContants.HEADER_CONTENT_RANGE_ATTRIBUTE, contentRangeValue.toString());
	}

}

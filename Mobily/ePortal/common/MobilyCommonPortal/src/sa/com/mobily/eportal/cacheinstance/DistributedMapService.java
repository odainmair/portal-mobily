package sa.com.mobily.eportal.cacheinstance;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.prefs.Preferences;

import org.apache.commons.collections.CollectionUtils;

import sa.com.mobily.eportal.cacheinstance.dao.NeqatyUserAccountDAO;
import sa.com.mobily.eportal.cacheinstance.dao.SessionVODAO;
import sa.com.mobily.eportal.cacheinstance.dao.UserAccountDAO;
import sa.com.mobily.eportal.cacheinstance.dao.UserSubscriptionDAO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.NeqatyUserAccountVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.RefactoredSessionVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.SessionVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.SubscriptionVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.UserAccountVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.UserSubscriptionsVO;
import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.dao.UserMyConnectAcctDAO;
import sa.com.mobily.eportal.common.service.model.UserAccount;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.util.puma.PumaUtil;
import sa.com.mobily.eportal.customerinfo.constant.RoleIdConstant;

import com.ibm.portal.portlet.service.spi.PortletServiceProvider;
import com.ibm.websphere.cache.DistributedMap;
import com.ibm.ws.security.auth.DistributedMapFactory;

/**
 * @author Muhammad Irfan Masood
 * 
 *         the service used the DistributedMap caching in WAS to cache the
 *         common used data to the authenticated users. the current impl use the
 *         sessionid-userName as the key in the map. the data is removed using
 *         httpsession listener from the map once the session is destroyed or by
 *         WAS using LRU .
 */

public class DistributedMapService implements DistributedMapJSRIfc, PortletServiceProvider
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonPortalLoggerIfc.CACHING_SERVICE_LOGGER_NAME);

	private static final String className = DistributedMapService.class.getName();

	private final String CACHE_INSTANCE_JNDI = "CACHE_INSTANCE_JNDI";

	private final String FILE_PROPERTIES = "cacheinstance_configuration";

	private Map<String, SessionVO> map = null;

	@SuppressWarnings("unchecked")
	public void init(Preferences aPreferences)
	{
		String method = "init";
		try
		{
			executionContext.log(Level.INFO, "DistributedMapService.init , starting...", className, method, null);
			map = (Map<String, SessionVO>) DistributedMapFactory.getMap(getPropertyValue(CACHE_INSTANCE_JNDI));
			DistributedMap dm= (DistributedMap)DistributedMapFactory.getMap(getPropertyValue(CACHE_INSTANCE_JNDI));
			dm.setTimeToLive(900);
			
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "DistributedMap.init(), CacheInstance was not found.", className, method, e);
		}
	}

	public void removeSessionData(String sessionId, String userName)
	{
		String key = null;
		if (map == null)
			throw new ServiceException("The dyna cache distributed map is null");
		
		try
		{
			SessionVODAO.getInstance().deleteUserSessionVO(sessionId);
		}
		catch (Exception e){}
		key = sessionId + "-" + userName;
		map.remove(key);
	}

	public SessionVO getSessionData(String aSessionId, String userName)
	{
		String method = "getSessionData";
		SessionVO sessionVO = null;
		String key = null;
		if (FormatterUtility.isEmpty(userName) || FormatterUtility.isEmpty(aSessionId))
		{
			throw new ServiceException("Either of Session ID or Use Name is null Session ID = " + aSessionId + " User Name " + userName);
		}
		else
		{
			if (map == null)
			{
				executionContext.log(Level.FINEST, "First time, initialize map " + aSessionId + " userName" + userName, className, method, null);
			}
			key = aSessionId + "-" + userName;
			sessionVO = map.get(key);
			executionContext.log(Level.FINER, "Session ID retrieved succffully for Sesion :> " + aSessionId, className, method, null);
		}
		if (sessionVO == null)
		{
			executionContext.log(Level.FINER, "CacheManager Service ,  SessionVO object for [" + aSessionId + "] Not exist in the map  , start getting session for UserName ["
					+ userName + "]", className, method, null);
			sessionVO = retrieveSessionData(aSessionId, userName);
			setSessionDataInCache(aSessionId, userName, sessionVO);
			return sessionVO;
		}
		else
		{
			return sessionVO;
		}
	}

	/**
	 * @param newNumber
	 *            this number could be the MBA number in case of Corporate Power
	 *            Users, MSISDN in case of CAP & Consumer users.
	 */
	public SessionVO setSessionData(String sessionId, String userName, String newNumber)
	{
		SessionVO sessionVO = this.resetCustomerSessionVO(sessionId, userName, newNumber);
		this.setSessionDataInCache(sessionId, userName, sessionVO);
		return sessionVO;
	}

	private void setSessionDataInCache(String aSessionId, String userName, SessionVO sessionVO)
	{
		String method = "setSessionDataInCache";
		executionContext.log(Level.FINER, "Cache manager Service > setSessionData > SessionID [" + aSessionId + "] , SessionVO = [" + sessionVO + "] ", className, method, null);
		if (map == null)
		{
			executionContext.log(Level.INFO, "Cache manager Service > setSessionData > SessionID [" + aSessionId + "] , Map is Null SessionVO , try to reinitialize it", className,
					method, null);
			init(null);
		}
		if (FormatterUtility.isEmpty(userName) || FormatterUtility.isEmpty(aSessionId))
		{
			executionContext.log(Level.INFO, "Session Id or User Name is empty , do nothing...............", className, method, null);
			throw new ServiceException("Session Id or User Name is empty");
		}
		else
		{
			String key = null;
			key = aSessionId + "-" + userName;
			map.put(key, sessionVO);
		}
	}

	private SessionVO retrieveSessionData(String sessionID, String userName)
	{
		String method = "retrieveSessionData";
		executionContext.log(Level.INFO, "Session Id or User Name is empty , do nothing...............", className, method, null);
		SessionVO sessionVO = null;
		sessionVO = getCustomerSessionVO(sessionID, userName);
		return sessionVO;
	}

	/**
	 * This method will get called only if session VO is null in Dyna Cache
	 * 
	 * @param sessionID
	 * @param portalUserName
	 * @param entityMngr
	 * @return
	 * @throws ServiceException
	 */
	private SessionVO getCustomerSessionVO(String sessionID, String portalUserName)
	{
		String method = "getCustomerSessionVO";
		SessionVO sessionVONew = null;
		RefactoredSessionVO refactoredSessionVO = new RefactoredSessionVO();
		refactoredSessionVO.setSessionId(sessionID);
		refactoredSessionVO.setUserName(portalUserName);
		List<RefactoredSessionVO> refactoredSessionVOs = SessionVODAO.getInstance().getUserSessionVOBySessionIdAndUserName(refactoredSessionVO);
		UserSubscriptionsVO defaultUserSubcription = null;
		List<UserAccountVO> accountVOs = null;
		List<UserSubscriptionsVO> refactoredUserSubscriptionsVOs = null;
		List<UserAccount> userAccountList = null;
		NeqatyUserAccountVO neqatyUserAccountVO = new NeqatyUserAccountVO();
		
		// check if its a corporate user.
		int roleId = new PumaUtil().getRoleId();
		executionContext.log(Level.INFO, "getCustomerSessionVO >> roleId::" + roleId + " for user::" + portalUserName, className, method, null);
		
		// for Neqaty partner
		if(roleId == RoleIdConstant.ROLE_ID_NEQATY_PARTNER)
        {
			neqatyUserAccountVO = NeqatyUserAccountDAO.getInstance().findNeqatyUserByUserName(portalUserName);
            if(neqatyUserAccountVO != null)
                return buildSessionVO(neqatyUserAccountVO);
        }
		
		// for MyConnect
		if(roleId == RoleIdConstant.ROLE_ID_CALL_CENTER)
        {
            userAccountList = UserMyConnectAcctDAO.getInstance().findByUserName(portalUserName);
            if(userAccountList != null && userAccountList.size() > 0)
                return buildSessionVO((UserAccount)userAccountList.get(0));
        }
		// for Non-Mobily 
		if(roleId == RoleIdConstant.ROLE_ID_REGISTERED_USER)
        {
				executionContext.log(Level.INFO, "Non-Mobily User : " + portalUserName, className, method, null);			
				accountVOs = UserAccountDAO.getInstance().getUserAccountVOByUserName(portalUserName);
				if(accountVOs != null && accountVOs.size() > 0){
					executionContext.log(Level.INFO, "Non-Mobily User : accountVOs size > 0", className, method, null);
					return buildSessionVO((UserAccountVO)accountVOs.get(0));
				}
        }
		
		if (CollectionUtils.isNotEmpty(refactoredSessionVOs))
		{

			accountVOs = UserAccountDAO.getInstance().getUserAccountVOByUserName(refactoredSessionVOs.get(0).getUserName());
			
			if (roleId == RoleIdConstant.ROLE_ID_CORPORATE_CUSTOMER)
			{
				return CorporateSessionVOUtil.getExistingSessionVO(accountVOs.get(0), refactoredSessionVOs, portalUserName);
			}

			refactoredUserSubscriptionsVOs = UserSubscriptionDAO.getInstance().getUserSubscriptionsByAccountId(refactoredSessionVO.getUserName());
			if (CollectionUtils.isNotEmpty(refactoredUserSubscriptionsVOs))
			{
				for (UserSubscriptionsVO userSubscriptionVO : refactoredUserSubscriptionsVOs)
				{
					if (userSubscriptionVO.getSubscriptionId() == refactoredSessionVOs.get(0).getSubscriptionId())
					{
						defaultUserSubcription = userSubscriptionVO;
						refactoredUserSubscriptionsVOs.remove(userSubscriptionVO);
						break;
					}
				}
			}
			else
			{
				throw new ServiceException("No subscription found for user " + portalUserName);
			}
			executionContext.log(Level.FINER, "Session retrieved from DB for user : " + portalUserName, className, method, null);
			sessionVONew = this.buildSessionVO(accountVOs.get(0), defaultUserSubcription, refactoredUserSubscriptionsVOs);
			return sessionVONew;
		}
		else
		{
			accountVOs = UserAccountDAO.getInstance().getUserAccountVOByUserName(portalUserName);
			if (CollectionUtils.isNotEmpty(accountVOs))
			{
				// check if its a corporate user.
				if (roleId == RoleIdConstant.ROLE_ID_CORPORATE_CUSTOMER)
				{
					return CorporateSessionVOUtil.getNewSessionVO(accountVOs.get(0), portalUserName, sessionID);
				}

				refactoredUserSubscriptionsVOs = UserSubscriptionDAO.getInstance().getUserSubscriptionsByAccountId(refactoredSessionVO.getUserName());
				if (CollectionUtils.isNotEmpty(refactoredUserSubscriptionsVOs))
				{
					for (UserSubscriptionsVO userSubscriptionVO : refactoredUserSubscriptionsVOs)
					{
						if (userSubscriptionVO.getIsDefault().equalsIgnoreCase("Y"))
						{
							defaultUserSubcription = userSubscriptionVO;
							refactoredUserSubscriptionsVOs.remove(userSubscriptionVO);
							break;
						}
					}
					
					if(defaultUserSubcription != null) {
						refactoredSessionVO.setSubscriptionId(defaultUserSubcription.getSubscriptionId());
						refactoredSessionVO.setCreatedDate(new Timestamp(new Date().getTime()));
						int result = SessionVODAO.getInstance().insertUserSessionVO(refactoredSessionVO);
						if (result <= 0)
							throw new ServiceException("Can not insert record in session table for user " + portalUserName + " and sessionId " + refactoredSessionVO.getSessionId());
					}
					sessionVONew = this.buildSessionVO(accountVOs.get(0), defaultUserSubcription, refactoredUserSubscriptionsVOs);
					executionContext.log(Level.FINEST, "Created session entry in DB for user : " + portalUserName, className, method, null);
					return sessionVONew;
				}
				else
				{
					throw new ServiceException("No subscription found for user " + portalUserName);
				}
			}
			else
			{
				throw new ServiceException("User not found in DB " + portalUserName);
			}
		}
	}

	/*
	 * This method was overloaded to meet the re-factoring
	 */

	private SessionVO buildSessionVO(UserAccountVO userAccount, UserSubscriptionsVO defaultUserSubscription, List<UserSubscriptionsVO> otherUserSubscriptionList)
	{
		String method = "buildSessionVO";
		/*executionContext.log(Level.FINER,
				"userAccount.getUserName() : " + userAccount.getUserName() + " defaultUserSubscription.getSubscriptionId() : " + defaultUserSubscription.getSubscriptionId(),
				className, method, null);*/
		//UserSubscriptionEligibilityDAO eligibilityDAO = UserSubscriptionEligibilityDAO.getInstance();
		executionContext.log(Level.INFO, "userAccount.getUserName() : " + userAccount.getUserName() + " defaultUserSubscription: " + defaultUserSubscription, className, method, null);
		
		SessionVO sessionVONew = new SessionVO();
		sessionVONew.setUserAcctId(userAccount.getUserAccountId());
		sessionVONew.setUserName(userAccount.getUserName());
		sessionVONew.setRoleId(userAccount.getRoleId());
		if(defaultUserSubscription != null) {
			sessionVONew.setIqama(defaultUserSubscription.getIqama());
			sessionVONew.setLastLoginTime(userAccount.getLastLoginTime());
			sessionVONew.setCustomerType(defaultUserSubscription.getCustomerType());
			sessionVONew.setPackageId(defaultUserSubscription.getPackageId());
			sessionVONew.setMsisdn(defaultUserSubscription.getMsidsn());
			sessionVONew.setServiceAccountNumber(defaultUserSubscription.getServiceAccountNumber());
			sessionVONew.setSubscriptionType(defaultUserSubscription.getSubscriptionType());
			sessionVONew.setLineType(defaultUserSubscription.getLineType());
			sessionVONew.setSubscriptionID(defaultUserSubscription.getSubscriptionId());
		}
		// added by r.agarwal.mit for Alsamou -- START
		/*UserSubscriptionEligibilityVO dto = eligibilityDAO.getSubscriptionEligibilitysBySubId(defaultUserSubscription.getSubscriptionId());
		if(dto != null)
			sessionVONew.setVipTier(dto.getStatus());*/
		// added by r.agarwal.mit for Alsamou -- END		
		
		Map<String, SubscriptionVO> otherSubscriptions = null;
		if (CollectionUtils.isNotEmpty(otherUserSubscriptionList))
		{
			otherSubscriptions = new HashMap<String, SubscriptionVO>();
			for (UserSubscriptionsVO otherUserSubribptionVO : otherUserSubscriptionList)
			{
				SubscriptionVO subscriptionVO = new SubscriptionVO();
				executionContext.log(Level.FINER, "userSubscription.getServiceAcctNumber() " + otherUserSubribptionVO.getServiceAccountNumber(), className, method, null);
				executionContext.log(Level.FINER, "userSubscription.getMsisdn() " + otherUserSubribptionVO.getMsidsn(), className, method, null);
				executionContext.log(Level.FINER, "userSubscription.getSubscriptionType() " + otherUserSubribptionVO.getSubscriptionType(), className, method, null);
				subscriptionVO.setSubscriptionType(otherUserSubribptionVO.getSubscriptionType());
				subscriptionVO.setSubscriptionID(otherUserSubribptionVO.getSubscriptionId());
				
				/*dto = eligibilityDAO.getSubscriptionEligibilitysBySubId(otherUserSubribptionVO.getSubscriptionId());
				if(dto != null)
					subscriptionVO.setVipTier(dto.getStatus());*/
				
				try
				{
					subscriptionVO.setCustomerType(Integer.parseInt(otherUserSubribptionVO.getCustomerType()));
				}
				catch (NumberFormatException e)
				{
					// ignore Integer parsing error
				}
				if (FormatterUtility.isNotEmpty(otherUserSubribptionVO.getMsidsn()) && FormatterUtility.isNotEmpty(otherUserSubribptionVO.getServiceAccountNumber()))
				{
					subscriptionVO.setMsisdn(otherUserSubribptionVO.getMsidsn());
					subscriptionVO.setServiceAccountNumber(otherUserSubribptionVO.getServiceAccountNumber());
					otherSubscriptions.put(otherUserSubribptionVO.getMsidsn(), subscriptionVO);
				}
				else if (FormatterUtility.isNotEmpty(otherUserSubribptionVO.getMsidsn()))
				{
					subscriptionVO.setMsisdn(otherUserSubribptionVO.getMsidsn());
					otherSubscriptions.put(otherUserSubribptionVO.getMsidsn(), subscriptionVO);
				}
				else if (FormatterUtility.isNotEmpty(otherUserSubribptionVO.getServiceAccountNumber()))
				{
					subscriptionVO.setServiceAccountNumber(otherUserSubribptionVO.getServiceAccountNumber());
					otherSubscriptions.put(otherUserSubribptionVO.getServiceAccountNumber(), subscriptionVO);
				}
			}
		}
		else
		{
			executionContext.log(Level.INFO, "No subscription found for user : " + userAccount.getUserName(), className, method, null);
		}
		sessionVONew.setOtherSubscriptions(otherSubscriptions);
		return sessionVONew;
	}
	
	// Added for My Connect
	private SessionVO buildSessionVO(UserAccount userAccount)
	{
		String method = "buildSessionVO";
		executionContext.log(Level.INFO,
				"userAccount.getUserName() : " + userAccount.getUserName(),
				className, method, null);
		
		SessionVO sessionVONew = new SessionVO();
		sessionVONew.setUserAcctId(userAccount.getUserAcctId());
		sessionVONew.setUserName(userAccount.getUserName());
		sessionVONew.setContactNumber(userAccount.getContactNumber());
		sessionVONew.setPackageId(userAccount.getPackageId());
		sessionVONew.setPackageType(userAccount.getPackageType());
		sessionVONew.setLastLoginTime(userAccount.getLastLoginTimeAsTimestamp());
			
		return sessionVONew;
	}
	
	// Added for Non-Mobily
		private SessionVO buildSessionVO(UserAccountVO userAccount)
		{
			String method = "buildSessionVO";
			executionContext.log(Level.INFO,
					"userAccount.getUserName() for Non-mobily: " + userAccount.getUserName(),
					className, method, null);
			SessionVO sessionVONew = new SessionVO();
			sessionVONew.setUserAcctId(userAccount.getUserAccountId());
			sessionVONew.setUserName(userAccount.getUserName());
			sessionVONew.setLastLoginTime(userAccount.getLastLoginTime());
				
			return sessionVONew;
		}
	
	// Added for Neqaty Partner
	private SessionVO buildSessionVO(NeqatyUserAccountVO neqatyUserAccountVO)
	{
		String method = "buildSessionVO";
		executionContext.log(Level.INFO, "userAccount.getUserName() : " + neqatyUserAccountVO.getUserName(), className, method, null);
		
		SessionVO sessionVONew = new SessionVO();
		sessionVONew.setUserAcctId(neqatyUserAccountVO.getUserAcctId());
		sessionVONew.setUserName(neqatyUserAccountVO.getUserName());
		sessionVONew.setPartnerEmail(neqatyUserAccountVO.getEmail());
		sessionVONew.setPartnerNameEn(neqatyUserAccountVO.getPartnerNameEn());
		sessionVONew.setPartnerNameAr(neqatyUserAccountVO.getPartnerNameAr());
		sessionVONew.setLastLoginTime(neqatyUserAccountVO.getLastLoginTime());
		sessionVONew.setPartnerActivationStatus(neqatyUserAccountVO.getStatus());
			
		return sessionVONew;
	}


	private SessionVO resetCustomerSessionVO(String sessionID, String portalUserName, String newNumber)
	{
		String method = "resetCustomerSessionVO";
		executionContext.log(Level.FINER, "portalUserName :" + portalUserName + " newNumber : " + newNumber, className, method, null);
		SessionVO sessionVONew = null;
		UserSubscriptionsVO defaultUserSubcriptoin = null;
		RefactoredSessionVO refactoredSessionVO = new RefactoredSessionVO();
		refactoredSessionVO.setSessionId(sessionID);
		refactoredSessionVO.setUserName(portalUserName);
		List<RefactoredSessionVO> refactoredSessionVOs = SessionVODAO.getInstance().getUserSessionVOBySessionIdAndUserName(refactoredSessionVO);
		if (CollectionUtils.isEmpty(refactoredSessionVOs))
		{
			executionContext.log(Level.WARNING, "Dyna cache existing entry not found during session vo reset for user " + portalUserName + " and for session ID " + sessionID,
					className, method, null);
			throw new ServiceException("Dyna cache existing entry not found during session vo reset for user " + portalUserName + " and for session ID " + sessionID);
		}
		else
		{
			List<UserAccountVO> refactoredUserAccountVOs = UserAccountDAO.getInstance().getUserAccountVOByUserName(refactoredSessionVOs.get(0).getUserName());
			if (CollectionUtils.isNotEmpty(refactoredUserAccountVOs))
			{
				// check if its a corporate user.
				int roleId = new PumaUtil().getRoleId();

				if (roleId == RoleIdConstant.ROLE_ID_CORPORATE_CUSTOMER)
				{
					return CorporateSessionVOUtil.resetSessionVO(refactoredUserAccountVOs.get(0), portalUserName, newNumber, sessionID);
				}
				List<UserSubscriptionsVO> refactoredUserSubscriptionsVOs = UserSubscriptionDAO.getInstance().getUserSubscriptionsByAccountId(refactoredSessionVO.getUserName());
				if (refactoredUserSubscriptionsVOs != null && refactoredUserSubscriptionsVOs.size() > 0)
				{
					for (UserSubscriptionsVO refactoredUserSubscriptionVo : refactoredUserSubscriptionsVOs)
					{
						if ((refactoredUserSubscriptionVo.getMsidsn() != null && refactoredUserSubscriptionVo.getMsidsn().equals(newNumber))
								|| (refactoredUserSubscriptionVo.getServiceAccountNumber() != null && refactoredUserSubscriptionVo.getServiceAccountNumber().equals(newNumber)))
						{
							defaultUserSubcriptoin = refactoredUserSubscriptionVo;
							refactoredUserSubscriptionsVOs.remove(refactoredUserSubscriptionVo);
							break;
						}
					}
					sessionVONew = this.buildSessionVO(refactoredUserAccountVOs.get(0), defaultUserSubcriptoin, refactoredUserSubscriptionsVOs);
					refactoredSessionVO = new RefactoredSessionVO();
					refactoredSessionVO.setSessionId(sessionID);
					refactoredSessionVO.setSubscriptionId(defaultUserSubcriptoin.getSubscriptionId());
					SessionVODAO.getInstance().updateUserSessionVO(refactoredSessionVO);
					return sessionVONew;
				}
				else
				{
					executionContext.log(Level.WARNING, "No subscription found during session vo reset for user " + portalUserName, className, method, null);
					throw new ServiceException("No subscription found during session vo reset for user " + portalUserName);
				}
			}
			else
			{
				executionContext.log(Level.WARNING, "User not found in DB " + portalUserName, className, method, null);
				throw new ServiceException("User not found in DB " + portalUserName);
			}
		}
	}

	private String getPropertyValue(String aKey)
	{
		ResourceBundle tempResourceBundle = null;
		String tempValue = null;
		try
		{
			tempResourceBundle = ResourceBundle.getBundle(FILE_PROPERTIES);
			tempValue = tempResourceBundle.getString(aKey);
		}
		catch (Exception e)
		{
			throw new ServiceException("Error when getPropertyValue for key " + aKey, e);
		}
		return tempValue;
	}
}
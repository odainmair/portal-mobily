package sa.com.mobily.eportal.common.beanlocator;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

public class BeanLocator {

	private static Properties properties = new Properties();
	static {
		try {
			properties.put(InitialContext.PROVIDER_URL, ResourceEnvironmentProviderService.getInstance().getPropertyValue("PROVIDER_URL"));
			properties.put(InitialContext.SECURITY_PRINCIPAL, ResourceEnvironmentProviderService.getInstance().getPropertyValue("PROVIDER_USER_NAME"));
			properties.put(InitialContext.SECURITY_CREDENTIALS, ResourceEnvironmentProviderService.getInstance().getPropertyValue("PROVIDER_PASSWORD"));
		} catch (Exception e) {
			IllegalStateException ex = new IllegalStateException("An error occurred when reading from the input stream");
			ex.initCause(e);
			throw ex;
		}
	}

	public static final <T> T lookup(Class<T> clazz, String jndiName) {
		Object bean = lookup(jndiName);
		return clazz.cast(PortableRemoteObject.narrow(bean, clazz));
	}

	public static Object lookup(String jndiName) {
		Context context = null;
		try {
			context = new InitialContext();
			return context.lookup(jndiName);
		} catch (NamingException ex) {
			throw new IllegalStateException("...", ex);
		} finally {
			try {
				context.close();
			} catch (NamingException ex) {
				throw new IllegalStateException("...", ex);
			}
		}
	}
	
	/**
	 * This Method lookups a remote bean, you should cast the returned object to remote interface type
	 * @param jndiName
	 * @return
	 * @throws NamingException
	 */
	public static Object remoteLookup(String jndiName) throws NamingException {
		Context context = null;
		try {
			context = new InitialContext(properties);
			return context.lookup(jndiName);
		} catch (NamingException ex) {
			throw new IllegalStateException("...", ex);
		} finally {
			try {
				context.close();
			} catch (NamingException ex) {
				throw new IllegalStateException("...", ex);
			}
		}
	}
}
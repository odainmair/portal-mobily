package sa.com.mobily.eportal.common.constants;

public final class MobilyConstants {
	//TAM
	public static final String TAM_ADMIN_USER_NAME="TAM_ADMIN_USER_NAME";
	public static final String TAM_ADMIN_USER_PASSWORD="TAM_ADMIN_USER_PASSWORD";
	public static final String TAM_CONFIG_FILE_URL="TAM_CONFIG_FILE_URL";
	public static final String LDAP_SUFFIX="LDAP_SUFFIX";

	//LDAP 
	public static final String LDAP_SERVER="LDAP_SERVER";
	
	public final class SERVICES{
		public static final int GSM = 1;
		public static final int BROADBAND = 2;
		public static final int ELIFE = 3;
		public static final int CONNECT = 4;
		public static final int WIMAX = 5;
	}
	
	public static final String SUCCESS_MSG="SUCCESS_MSG";
	public static final String ERROR_MSG="ERROR_MSG";

	public static final String ERROR_MSG_INVALID_SUBSCRIPTION="ERROR_MSG_INVALID_SUBSCRIPTION"; 
	public static final String ERROR_MSG_BACKEND_FAILURE="ERROR_MSG_BACKEND_FAILURE";
	public static final String ERROR_MSG_GENERIC_ERROR="ERROR_MSG_GENERIC_ERROR";

}
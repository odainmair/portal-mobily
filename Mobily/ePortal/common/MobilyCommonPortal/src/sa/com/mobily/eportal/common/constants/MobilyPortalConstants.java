/**
 * 
 */
package sa.com.mobily.eportal.common.constants;

/**
 * @author Suresh Vathsavai - MIT
 * Date: Nov 29, 2013
 */
public interface MobilyPortalConstants {

	//added here since these common to all portlets
	public static final String PORTLET_VIEW_PHASE = "doView";
	public static final String PORTLET_ACTION_PHASE = "processAction";
	public static final String PORTLET_RESOURCE_PHASE = "serveResource";
	public static final String PORTLET_HEADERS_PHASE = "doHeaders";
	public static final String EC_PORTLET_CLASS = "portlet-class";
	public static final String EC_PORTLET_METHOD = "portlet-method";
	public static final String EC_EXCEPTION_OCCURRED = "exceptionOccurred";
	public static final String EC_EXCEPTION_OCCURRED_YES_FRAGMENT = "YES";
	public static final String EC_EXCEPTION_OCCURRED_NO_FRAGMENT = "NO";
	public static final String EX_EXCEPTION = "EXCEPTION";
	
}

package sa.com.mobily.eportal.common.lookup.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.service.lookup.common.CachingLookupUtil;
import sa.com.mobily.eportal.common.service.lookup.common.CachingLookupVO;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;


public class CachingLookUpCustomTag extends TagSupport {
	/**
	 * 
	 */
	
	private static final long serialVersionUID = -5158786695200760989L;
	private String id = "";
	private String type = "";
	private String locale = null;
	private String name = "";
	private String className = "";
	private String style = "";
	private String lookup_type ="";
	private String insertColon = "false";
	private String code = "";
	private CachingLookupUtil cachingLookupUtil = CachingLookupUtil.getInstance();
	private String readOnly = "";
	private String errorMessage = "<div class = " + "alert_msg alert_yield_msg" + " id= " + "idError " + ">"
									+"<p> an error occurred </p> " + " </div>";
	private String clazz = CachingLookUpCustomTag.class.getName();
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonPortalLoggerIfc.CACHING_SERVICE_LOGGER_NAME);
	@Override
	public int doStartTag() throws JspException {
		String methodName = "doStartTag";
		JspWriter out = pageContext.getOut();
		String tagText ="";
		if(type == "div")
		{
		CachingLookupVO cachingLookupVO = cachingLookupUtil.getCachingLookupVO(lookup_type,code);
		if(cachingLookupVO !=null)
		tagText = "<div id='"+id+"' class='"+className+"' style='"+style+"'>"+locale=="en" ? cachingLookupVO.getEnglishName() : cachingLookupVO.getArabicName()+"</div>";
		else
			tagText = errorMessage;
		}else if(type == "text")
		{
			CachingLookupVO cachingLookupVO = cachingLookupUtil.getCachingLookupVO(lookup_type,code);
			if(cachingLookupVO !=null)
			tagText = "<input type='text' id='"+id+"' class='"+className+"' style='"+style+"' value='"+locale=="en" ? cachingLookupVO.getEnglishName() : cachingLookupVO.getArabicName()+"' "+ readOnly==null || readOnly == "false" ? "" : "readonly"  +" />";
			else
				tagText = errorMessage;
		}else if(type == "combo")
		{
			ArrayList<CachingLookupVO> cachingLookupVOs = cachingLookupUtil.getCachingLookupVOList(lookup_type);
			if(cachingLookupVOs!=null || cachingLookupVOs.size()>0)
			{
			tagText = "<SELECT id='"+id+"' name='"+name+"'  "+readOnly+" style='"+style+"' class="+className+ "  >";
			for(CachingLookupVO cachingLookupVO : cachingLookupVOs)
			{
				tagText +="<option value='"+cachingLookupVO.getShort_name()+"'>"+locale=="en" ? cachingLookupVO.getEnglishName() : cachingLookupVO.getArabicName()+"</option>";
			}
			tagText +="</SELECT>";
			}else
				tagText = errorMessage;
		}else if(type == "")
		{
			CachingLookupVO cachingLookupVO = cachingLookupUtil.getCachingLookupVO(lookup_type,code);
			//System.out.println("locale : "+locale);
			if(cachingLookupVO !=null)
			tagText = locale=="en" ? cachingLookupVO.getEnglishName() : cachingLookupVO.getArabicName();
			if(Boolean.valueOf(insertColon))
				tagText = new StringBuilder(tagText).append(":").toString();
		}
		
		try {			
			executionContext.log(Level.INFO, tagText , clazz, methodName, null);
			out.println(tagText);
		} catch (IOException e) {
			executionContext.log(Level.INFO, "Exception : "+e.getMessage() , clazz, methodName, e.getCause());
			e.printStackTrace();
		}
		return super.doStartTag();
	}
	
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getLookup_type() {
		return lookup_type;
	}

	public void setLookup_type(String lookup_type) {
		this.lookup_type = lookup_type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getReadOnly() {
		return readOnly;
	}

	public void setReadOnly(String readOnly) {
		this.readOnly = readOnly;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public String getClassName() {
		return className;
	}



	public void setClassName(String className) {
		this.className = className;
	}



	public String getInsertColon()
	{
		return insertColon;
	}



	public void setInsertColon(String insertColon)
	{
		this.insertColon = insertColon;
	}

	
}

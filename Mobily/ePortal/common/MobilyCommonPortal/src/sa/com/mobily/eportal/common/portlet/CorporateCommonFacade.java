package sa.com.mobily.eportal.common.portlet;

import java.io.Serializable;

import sa.com.mobily.eportal.cacheinstance.GeneralDistributedMap;
import sa.com.mobily.eportal.cacheinstance.constants.CacheKeyConstantIfc;
import sa.com.mobily.eportal.common.service.facade.MobilyCommonFacade;
import sa.com.mobily.eportal.common.service.vo.CorporateProfileVO;
import sa.com.mobily.eportal.common.service.vo.CustomerProfileReplyVO;

/**
 * 15 March 2014
 * @author Mohamed Ali	
 * 
 */

public class CorporateCommonFacade
{
	private static CorporateCommonFacade corporateCommonFacade;
	private static MobilyCommonFacade facade ;
	
	public CorporateProfileVO getCorporateProfileVO(String rootBillingAccountNumber){
		GeneralDistributedMap map = GeneralDistributedMap.getInstance();
		String cacheKey = CacheKeyConstantIfc.PREFIX_CORP_PROFILE + rootBillingAccountNumber;
		CorporateProfileVO corporateProfileVO = (CorporateProfileVO) map.getCachedData(cacheKey);
		if (corporateProfileVO == null){
			corporateProfileVO = facade.findCorporateProfile(rootBillingAccountNumber);
			map.setCachedData(cacheKey, (Serializable) corporateProfileVO);
		}
		return corporateProfileVO;
	} 
	public CustomerProfileReplyVO getCustomerProfileReplyVO(String rootBillingAccountNumber){
		GeneralDistributedMap map = GeneralDistributedMap.getInstance();
		String cacheKey = CacheKeyConstantIfc.PREFIX_CUSTOMER_PROFILE + rootBillingAccountNumber;
		CustomerProfileReplyVO customerProfileReplyVO = (CustomerProfileReplyVO) map.getCachedData(cacheKey);
		if(customerProfileReplyVO == null){
			customerProfileReplyVO = facade.getCustomerProfileFromSiebel(rootBillingAccountNumber);
			map.setCachedData(cacheKey, (Serializable) customerProfileReplyVO);
		}
		return customerProfileReplyVO;
	}
	public static CorporateCommonFacade getInstance(){
		if(corporateCommonFacade == null){
			corporateCommonFacade = new CorporateCommonFacade();
			facade = new MobilyCommonFacade();
		}
		return corporateCommonFacade;
	}
}

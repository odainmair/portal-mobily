package sa.com.mobily.eportal.common.portlet;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.NullComparator;

import sa.com.mobily.eportal.common.portlet.vo.*;
import sa.com.mobily.eportal.common.service.vo.PagingVO;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

/**
 * This class should be extended by any portlet wishes to use server side paging.
 * <p> 
 * The paging here depends on using dojo JsonRestStore, where it sent on the http request required paging and sorting parameters  
 *  
 * @author Omar Kalaldeh
 */
public class ServerSidePagingPortlet extends GenericPortlet {
	
	public static final String SORT_PARAM = "sortParam";
	public static final String NEW_VALUE_PARAM = "newValueParam";
	public static final String ATTRIBUTE_PARAM = "attributeParam";
	
	/**
	 * Gets <code>PagingVO</code> with all information for currently requested page.
	 * <p>
	 * Building paging bean depends on the "Range" parameter returned from the JsonRestStore dojo from the
	 * calling JSP.
	 * 
	 * @param request <code>ResourceRequest</code> to read HTTP header
	 * @param totalItems the total items for the whole proceed list
	 * @return <code>PagingVO</code> with all information for currently requested page
	 * @throws PortletException
	 * @throws IOException
	 */
	public PagingVO getPagingVO(ResourceRequest request, Integer totalItems) {
		return parseRangeString(request.getProperty("Range"), totalItems);
	}
	
	/**
	 * Parse "Range" parameter returned on HTTP header, this parameter is filled by the JsonRestStore
	 * 
	 * @param range the Range HTTP header parameter
	 * @param totalItems the total items for the whole proceed list
	 * @return <code>PagingVO</code> with all information for currently requested page  
	 */
	private PagingVO parseRangeString(String range, Integer totalItems) {
		Integer startIndex;
		Integer endIndex;

		if(range == null || range.length() == 0) {
			startIndex = 1;
			endIndex = 10;
		} else {
			String temp = range.replaceAll("items=", "");
			int index = temp.indexOf("-");

			startIndex = Integer.parseInt(temp.substring(0, index))+1;
			endIndex = Integer.parseInt(temp.substring(index + 1, temp.length()))+1;
			
			if(totalItems != null && endIndex >= totalItems) {
				endIndex = totalItems ;
			}
			
		}
		PagingVO pagingVO = new PagingVO(startIndex, endIndex, totalItems);
		
		return pagingVO;
	}
	
	/**
	 * Sets "Content-Range" parameter on http response header, so the JsonRestStore can build its pagining
	 *   
	 * @param response the <code>ResourceResponse</code>
	 * @param pagingVO the <code>PagingVO</code> for the currently processed page 
	 */
	public void setPageOnResponseHeader(ResourceResponse response, PagingVO pagingVO) {
		response.setProperty("Content-Range", "items "+pagingVO.getStartIndex()+"-"+pagingVO.getEndIndex()+"/"+pagingVO.getTotalItems());
	}
	
	/**
	 * Sorts list according to http request sort parameter came from dojo data grid.
	 * <p> 
	 * This class have default implementation for <code>getComparator</code> method,
	 * it is strongly recommended to override this method when we are dealing with 
	 * huge size lists.
	 * <p>
	 * Any exception thrown during sorting, original list will be returned with logging the exception
	 * 
	 * @param request the <code>ResourceRequest</code> sent from dojo grid
	 * @param list the <code>List</code> need to be sorted
	 * @see getComparator 
	 */
	@SuppressWarnings("unchecked")
	public <T> void sortList(ResourceRequest request, List<T> list) {
		
		try {
			/* sortParam contains the sort order (ascending or descending) marked by +/- sign 
			 * followed by field name the sorting will be done against.
			 * 
			 * ex: +simNumber this means the list need to be sorted ascending according to sinNumber field on the bean
			 *   
			 */
			String sortParam = request.getParameter(SORT_PARAM);
			String fieldName  = sortParam.substring(1);
			boolean isDescending = sortParam.charAt(0) == '-';
			@SuppressWarnings("rawtypes")
			Comparator comparator = getComparator(fieldName);
			Collections.sort(list, comparator);
			if(! isDescending) {
				Collections.reverse(list);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getItemId(ResourceRequest request, Class clazz) throws JsonSyntaxException, JsonIOException, UnsupportedEncodingException, IOException {
		return request.getParameter("id");
	}
	
	/**
	 * Provides a generic <code>Comparator</code> to be used with sorting.
	 * <P>
	 * This method based on apache <code>BeanComparator</code> which uses reflection, so it could 
	 * have side effects on performance, so it is strongly recommended to override this method when we are
	 * dealing with huge size lists, and provide you own <code>Comparator</code>.     
	 * 
	 * @param fieldName name of the field wants to sort against 
	 * @return a <code>Comparator</code> to be used with sorting list
	 * @see sortList
	 */
	public Comparator<?> getComparator(String fieldName) {
		return new BeanComparator(fieldName, new NullComparator(false));
	}
	
	/**
	 * Sends json response with list details to dojo data grid. This method is used when you have full list and need to be
	 * sub listed.
	 * <P>
	 * Sorting and pagining will be applied on the list, required information to do so will be acquired from the
	 * request sent by dojo grid.
	 * 
	 * @param request the <code>ResourceRequest</code> sent from dojo data grid
	 * @param response the <code>ResourceResponse</code> will be sent to data dojo grid
	 * @param list	the <code>List</code> will be sent to data grid after applying required pagining and sorting
	 * @throws PortletException
	 * @throws java.io.IOException
	 */
	public <T> void sendJsonResponseAfterPaging(ResourceRequest request, ResourceResponse response,  List<T> list) throws PortletException, java.io.IOException {
		PagingVO pagingVO = getPagingVO(request, list.size());
		List<?> subList = list.subList(pagingVO.getStartIndex(), pagingVO.getEndIndex() + 1);
		
		if(request.getParameterMap().containsKey(SORT_PARAM)) {
			sortList(request, subList);
		} 			
		Gson gson = new Gson(); 
		
		String jsonList = gson.toJson(subList);
		setPageOnResponseHeader(response, pagingVO);
		response.getWriter().write(jsonList);
	}
	
	
	public void writeJsonToResponse(ResourceResponse response, Object obj) throws IOException {
		Gson gson = new Gson(); 
		String jsonString = gson.toJson(obj);
		response.getWriter().write(jsonString);
		
	}

	/**
	 * Sends json response with list details to dojo data grid. This method used when you have already paged list, not the full list.
	 * <P>
	 * Sorting will be applied on the list, required information to do so will be acquired from the request sent by dojo grid.
	 * <P>
	 * Although this method will not sub list the original list still it will send on the response, information regarding pagining.    
	 * @param <T>
	 * 
	 * @param request the <code>ResourceRequest</code> sent from dojo data grid
	 * @param response the <code>ResourceResponse</code> will be sent to data dojo grid
	 * @param list	a page <code>List</code> will be sent to data grid after applying required sorting
	 * @throws PortletException
	 * @throws java.io.IOException
	 */
	public <T> void sendJsonResponseForPagedList(ResourceRequest request, ResourceResponse response, List<T> list,Integer totalItems) throws PortletException, java.io.IOException {
		PagingVO pagingVO = getPagingVO(request, totalItems);
		
		if(request.getParameterMap().containsKey(SORT_PARAM)) {
			sortList(request, list);
		} 			
		Gson gson = new Gson(); 
		
		String jsonList = gson.toJson(list);
		setPageOnResponseHeader(response, pagingVO);
		response.getWriter().write(jsonList);

	}
	
	/**
	 * Handles json requests contains update items on server side. 
	 * <p>
	 * The method expect stream of josn contains updated item from the request and 
	 * two parameters: ATTRIBUTE_PARAM and NEW_VALUE_PARAM
	 * 
	 * @param request the <code>ResourceRequest</code>
	 * @param clazz the VO class for the item object
	 * @return <code>UpdatedItem</code> contains the updated object item and updated value and which attribute was updated
	 * @throws JsonSyntaxException
	 * @throws JsonIOException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public Object getNewItem(ResourceRequest request, Class clazz) throws JsonSyntaxException, JsonIOException, UnsupportedEncodingException, IOException {
		Gson gson = new Gson(); 
		return gson.fromJson(request.getReader(), clazz);
	}
	
	/**
	 * Handles json requests contains update items on server side. 
	 * <p>
	 * The method expect stream of josn contains updated item from the request and 
	 * two parameters: ATTRIBUTE_PARAM and NEW_VALUE_PARAM
	 * 
	 * @param request the <code>ResourceRequest</code>
	 * @param clazz the VO class for the item object
	 * @return <code>UpdatedItem</code> contains the updated object item and updated value and which attribute was updated
	 * @throws JsonSyntaxException
	 * @throws JsonIOException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public Object getDeleteItem(ResourceRequest request, Class clazz) throws JsonSyntaxException, JsonIOException, UnsupportedEncodingException, IOException {
		Gson gson = new Gson(); 
		return gson.fromJson(request.getReader(), clazz);
	}

	/**
	 * Handles json requests contains update items on server side. 
	 * <p>
	 * The method expect stream of josn contains updated item from the request and 
	 * two parameters: ATTRIBUTE_PARAM and NEW_VALUE_PARAM
	 * 
	 * @param request the <code>ResourceRequest</code>
	 * @param clazz the VO class for the item object
	 * @return <code>UpdatedItem</code> contains the updated object item and updated value and which attribute was updated
	 * @throws JsonSyntaxException
	 * @throws JsonIOException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public UpdatedItem getUpdatedItem(ResourceRequest request, Class clazz) throws JsonSyntaxException, JsonIOException, UnsupportedEncodingException, IOException {
		String attribute = request.getParameter(ATTRIBUTE_PARAM);
		String newValue = request.getParameter(NEW_VALUE_PARAM);
		return new UpdatedItem(request, newValue, attribute, clazz);
	}
	
	/**
	 * This class contains the updated object item and updated value and which attribute was updated
	 *
	 */
	public class UpdatedItem {
		private String newValue;
		private String attributeName;
		private Object obj;
		public UpdatedItem(ResourceRequest request, String newValue, String attributeName, Class clazz) throws JsonSyntaxException, JsonIOException, UnsupportedEncodingException, IOException {
			this.newValue = newValue;
			this.attributeName = attributeName;
			Gson gson = new Gson(); 
			obj = gson.fromJson(request.getReader(), clazz);
		}
		/**
		 * @return the newValue
		 */
		public String getNewValue() {
			return newValue;
		}
		/**
		 * @return the attributeName
		 */
		public String getAttributeName() {
			return attributeName;
		}

		/**
		 * @return the obj
		 */
		public Object getObj() {
			return obj;
		}
		
	}
	
}

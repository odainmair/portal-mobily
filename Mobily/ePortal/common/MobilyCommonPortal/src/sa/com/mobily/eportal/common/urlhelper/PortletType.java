package sa.com.mobily.eportal.common.urlhelper;
/* @copyright module */
/*                                                                            */
/*  DISCLAIMER OF WARRANTIES:                                                 */
/*  -------------------------                                                 */
/*  The following [enclosed] code is sample code created by IBM Corporation.  */
/*  This sample code is provided to you solely for the purpose of assisting   */
/*  you in the development of your applications.                              */
/*  The code is provided "AS IS", without warranty of any kind. IBM shall     */
/*  not be liable for any damages arising out of your use of the sample code, */
/*  even if they have been advised of the possibility of such damages.        */

/**
 * @author James Barnes
 * This class allows you to target the proper accessor in the various url generation
 * helper classes
 */
public class PortletType {
	
	public static final String LEGACY_PORTLET = "Legacy";
	public static final String STANDARD_PORTLET = "Standard";
	
	public static final PortletType LegacyPortletType = new PortletType(LEGACY_PORTLET);
	public static final PortletType StandardPortletType = new PortletType(STANDARD_PORTLET);
	private String portletType = "";
	
	/**
	 * Used to create a PortletType with the given portletType
	 * @param portletType
	 */
	public PortletType(String portletType) {
		super();
		this.portletType = portletType;
	}

	/**
	 * Used to get the  portlettype for this portlettype
	 * @return String
	 */
	public String getPortletType() {
		return portletType;
	}
	
}

package sa.com.mobily.eportal.common.util.puma;

import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import javax.validation.constraints.NotNull;

import com.ibm.portal.um.Group;
import com.ibm.portal.um.PumaController;
import com.ibm.portal.um.PumaEnvironment;
import com.ibm.portal.um.PumaHome;
import com.ibm.portal.um.PumaLocator;
import com.ibm.portal.um.PumaProfile;
import com.ibm.portal.um.User;
import com.ibm.portal.um.exceptions.PumaException;

import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.constants.PumaProfileConstantsIfc;
import sa.com.mobily.eportal.common.exception.application.DataNotFoundException;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.ldapservice.MobilyUser;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
//import sa.com.mobily.eportal.common.util.ldap.LDAPManager;
import sa.com.mobily.eportal.common.util.ldap.LDAPManagerInterface;


//Updated By Rasha 
public class LDAPService implements LDAPManagerInterface
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonPortalLoggerIfc.PUMA_SERVICE_LOGGER_NAME);

	private static final String className = LDAPService.class.getName();

	private static LDAPService instance;

	private LDAPService()
	{
	}

	public static LDAPService getInstance()
	{
		if (instance == null)
			instance = new LDAPService();
		return instance;
	}

	public MobilyUser getLoginInforamtionFromLdap() throws PumaException
	{
		return getMobilyUser(PumaCachedHome.getInstance().getPumaHome().getProfile().getCurrentUser());
	}

	public boolean isValidUser(String username, String password)
	{
		// boolean nativeImpl = true;
		// if(nativeImpl){
		// return LDAPManager.getInstance().isValidUser(username, password);
		// } else {
		Map<String, String> attributes = new HashMap<String, String>();
		attributes.put(PumaProfileConstantsIfc.uid, username);
		attributes.put(PumaProfileConstantsIfc.password, password);
		MobilyUser user = findMobilyUserByAttributes(attributes);
		return !(user == null);
		// }
	}

	public void createNewUserOnLDAP(@NotNull MobilyUser mobilyUser)
	{
		String method = "createNewUserOnLDAP";
		PumaHome pumaHome = PumaCachedHome.getInstance().getPumaHome();
		executionContext.log(Level.INFO, "createNewUserOnLDAP > mobilyUser [" + mobilyUser + "]", className, method, null);
		
		PrivilegedExceptionAction<Void> privilegedAction = new PrivilegedExceptionAction<Void>()
		{
			@Override
			public Void run()
			{
				PumaController pController = pumaHome.getController();
				HashMap<String, Object> userAttrs = new HashMap<String, Object>();
				userAttrs.put(PumaProfileConstantsIfc.cn, mobilyUser.getCn());
				userAttrs.put(PumaProfileConstantsIfc.sn, mobilyUser.getSn());
				userAttrs.put(PumaProfileConstantsIfc.uid, FormatterUtility.toLowerCase(mobilyUser.getUid()).trim());
				userAttrs.put(PumaProfileConstantsIfc.defaultMsisdn, mobilyUser.getDefaultMsisdn());
				userAttrs.put(PumaProfileConstantsIfc.email, FormatterUtility.toLowerCase(mobilyUser.getEmail()).trim());
				userAttrs.put(PumaProfileConstantsIfc.preferredLanguage, mobilyUser.getPreferredLanguage());
				userAttrs.put(PumaProfileConstantsIfc.serviceAccountNumber, (FormatterUtility.toLowerCase(mobilyUser.getServiceAccountNumber())));
				userAttrs.put(PumaProfileConstantsIfc.defaultSubscriptionType, mobilyUser.getDefaultSubscriptionType());
				userAttrs.put(PumaProfileConstantsIfc.defaultSubSubscriptionType, mobilyUser.getDefaultSubSubscriptionType());
				userAttrs.put(PumaProfileConstantsIfc.defaultSubId, mobilyUser.getDefaultSubId());
				userAttrs.put(PumaProfileConstantsIfc.corpMasterAcctNo, mobilyUser.getCorpMasterAcctNo());
				userAttrs.put(PumaProfileConstantsIfc.corpAPNumber, mobilyUser.getCorpAPNumber());
				userAttrs.put(PumaProfileConstantsIfc.corpAPIDNumber, mobilyUser.getCorpAPIDNumber());
				userAttrs.put(PumaProfileConstantsIfc.commercialRegisterationID, mobilyUser.getCommercialRegisterationID());
				userAttrs.put(PumaProfileConstantsIfc.corpSurveyCompleted, mobilyUser.isCorpSurveyCompleted());
				userAttrs.put(PumaProfileConstantsIfc.password, FormatterUtility.checkNull(mobilyUser.getUserPassword()).trim());
				userAttrs.put(PumaProfileConstantsIfc.givenName, mobilyUser.getGivenName());
				userAttrs.put(PumaProfileConstantsIfc.Status, mobilyUser.getStatus());
				userAttrs.put(PumaProfileConstantsIfc.firstName, mobilyUser.getFirstName());
				userAttrs.put(PumaProfileConstantsIfc.lastName, mobilyUser.getLastName());
				userAttrs.put(PumaProfileConstantsIfc.custSegment, mobilyUser.getCustSegment());
				userAttrs.put(PumaProfileConstantsIfc.roleId, mobilyUser.getRoleId());
				userAttrs.put(PumaProfileConstantsIfc.SecurityQuestion, mobilyUser.getSecurityQuestion());
				userAttrs.put(PumaProfileConstantsIfc.SecurityAnswer, mobilyUser.getSecurityAnswer());
				userAttrs.put(PumaProfileConstantsIfc.birthDate, mobilyUser.getBirthDate());
				userAttrs.put(PumaProfileConstantsIfc.nationality, mobilyUser.getNationality());
				userAttrs.put(PumaProfileConstantsIfc.Gender, mobilyUser.getGender());
				userAttrs.put(PumaProfileConstantsIfc.registerationDate, mobilyUser.getRegisterationDate());
				userAttrs.put(PumaProfileConstantsIfc.iqama, mobilyUser.getIqama());
				userAttrs.put(PumaProfileConstantsIfc.facebookId, FormatterUtility.toLowerCase(mobilyUser.getFacebookId()));
				userAttrs.put(PumaProfileConstantsIfc.registrationSourceId, mobilyUser.getRegistrationSourceId());
				userAttrs.put(PumaProfileConstantsIfc.userAcctReference, mobilyUser.getUserAcctReference());
				userAttrs.put(PumaProfileConstantsIfc.contactNumber, mobilyUser.getContactNumber());
				try
				{
					executionContext.log(Level.INFO, "createNewUserOnLDAP createUser start", className, method, null);
					
					User user = pController.createUser(FormatterUtility.toLowerCase(mobilyUser.getUid()).trim(), null, userAttrs);
					executionContext.log(Level.INFO, "createNewUserOnLDAP user created", className, method, null);
					if (user != null)
					{
						executionContext.log(Level.INFO, "createNewUserOnLDAP Add user to Group", className, method, null);
						List<String> groupList = mobilyUser.getGroupList();
						executionContext.log(Level.INFO, "createNewUserOnLDAP > groupList " + groupList, className, method, null);
						List<Group> ldapGroupList = null;
						if (groupList != null)
						{
							String groupName = "";
							for (int i = 0; i < groupList.size(); i++)
							{
								groupName = (String) groupList.get(i);
								executionContext.log(Level.INFO, "createNewUserOnLDAP > groupName [" + groupName + "]", className, method, null);

								ldapGroupList = (List<Group>) pumaHome.getLocator().findGroupsByAttribute(PumaProfileConstantsIfc.cn, groupName);

								executionContext.log(Level.INFO, "createNewUserOnLDAP > ldapGroupList " + ldapGroupList, className, method, null);
								if (ldapGroupList != null && ldapGroupList.size() > 0)
								{
									// Group is found
									executionContext.log(Level.INFO, "createNewUserOnLDAP > groupName [" + groupName + "] is found in LDAP", className, method, null);

									Group group = (Group) ldapGroupList.get(0);

									List<User> list = new ArrayList<User>();
									list.add(user);
									executionContext.log(Level.INFO, "createNewUserOnLDAP > Add user to group started", className, method, null);
									pController.addToGroup(group, list);
									executionContext.log(Level.INFO, "createNewUserOnLDAP > Add user to group ended", className, method, null);
								}
							}
						}
						else
						{
							executionContext.log(Level.INFO, "createNewUserOnLDAP > No Group sent by the caller....DON'T do anything...", className, method, null);
						}
					}
				}
				catch (Exception e)
				{
					executionContext.log(Level.SEVERE, "Mobily Common portal > Exception during User Creation ->>>>>" + e.getMessage(), className, method, e);
					List<String> attributes = new ArrayList<String>();
					attributes.addAll(userAttrs.keySet());

					for (Iterator<String> iterator = attributes.iterator(); iterator.hasNext();)
					{
						String string = (String) iterator.next();
						executionContext.log(Level.SEVERE, "Attribute " + string + " = " + userAttrs.get(string), className, method, null);
					}
					executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
					ServiceException se = new ServiceException(e.getMessage(), e.getCause());
					throw se;
				}
			
				return null;

			}

		};

		try
		{

			PumaEnvironment pumaEnv = pumaHome.getEnvironment();
			pumaEnv.runUnrestricted(privilegedAction);

		}
		catch (PrivilegedActionException e)
		{
			e.printStackTrace();
			executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
			throw se;
		}

	
		
	}

	/***
	 * Updating user group
	 * 
	 * @param userId
	 * @param oldGroup
	 * @param newGroup
	 */

	public void updateUserGroup(String userId, String oldGroup, String newGroup)
	{
		String method = "updateUserGroup";
		PumaHome pumaHome = PumaCachedHome.getInstance().getPumaHome();
		PumaController pController = pumaHome.getController();
		try
		{
			executionContext.log(Level.INFO, "Update user group start", className, method, null);
			MobilyUser mobilyUser = findMobilyUserByAttribute(PumaProfileConstantsIfc.uid, userId);
			List<String> grouplist = new ArrayList<String>();
			grouplist.add(oldGroup);
			mobilyUser.setGroupList(grouplist);
			User user = findUserByAttribute(PumaProfileConstantsIfc.uid, userId);
			executionContext.log(Level.INFO, "updateUserGroup user created", className, method, null);
			if (mobilyUser != null && user != null)
			{
				executionContext.log(Level.INFO, "updateUserGroup Add user to Group", className, method, null);
				List<String> groupList = mobilyUser.getGroupList();
				executionContext.log(Level.INFO, "updateUserGroup > groupList " + groupList, className, method, null);
				List<Group> ldapGroupList = null;

				List<Group> ldapGroupListNew = (List<Group>) pumaHome.getLocator().findGroupsByAttribute(PumaProfileConstantsIfc.cn, newGroup);

				executionContext.log(Level.INFO, "updateUserGroup > ldapGroupList " + ldapGroupListNew, className, method, null);
				if (ldapGroupListNew != null && ldapGroupListNew.size() > 0)
				{
					// Group is found
					executionContext.log(Level.INFO, "updateUserGroup > groupName [" + newGroup + "] is found in LDAP", className, method, null);

					Group group = (Group) ldapGroupListNew.get(0);

					List<User> list = new ArrayList<User>();
					list.add(user);
					executionContext.log(Level.INFO, "updateUserGroup > Add user to group started", className, method, null);
					pController.addToGroup(group, list);
					executionContext.log(Level.INFO, "updateUserGroup > Add user to group ended", className, method, null);
				}

				if (groupList != null)
				{
					String groupName = "";
					for (int i = 0; i < groupList.size(); i++)
					{
						groupName = (String) groupList.get(i);
						// try to match old group and remove user from this
						// group
						if (groupName.equalsIgnoreCase(oldGroup))
						{
							// old group found

							ldapGroupList = (List<Group>) pumaHome.getLocator().findGroupsByAttribute(PumaProfileConstantsIfc.cn, groupName);
							List<User> userList = new ArrayList<User>();
							userList.add(user);
							if (ldapGroupList != null && ldapGroupList.size() > 0)
							{
								executionContext.log(Level.INFO, "updateUserGroup > removing user form group ..." + groupName, className, method, null);
								pController.removeFromGroup(ldapGroupList.get(0), userList);
							}
							else
							{
								// old group not found
								executionContext.log(Level.INFO, "updateUserGroup > No old Group sent by the caller....DON'T do anything..." + oldGroup, className, method, null);
							}
						}

					}
				}
				else
				{
					executionContext.log(Level.INFO, "updateUserGroup > No old Group sent by the caller....DON'T do anything..." + oldGroup, className, method, null);
				}
			}
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
			throw se;
		}
	}

	/**
	 * Method to delete user from LDAP.
	 * 
	 * @param mobilyUser
	 * @throws ServiceException
	 */
	public void deleteUser(@NotNull MobilyUser mobilyUser)
	{
		String method = "deleteUser";
		executionContext.log(Level.INFO, "delete user from LDAP Started", className, method, null);
		PumaHome pumaHome = PumaCachedHome.getInstance().getPumaHome();
		String userID = mobilyUser.getUid();
		executionContext.log(Level.INFO, "delete user from LDAP userID " + userID, className, method, null);

		PumaController pController = pumaHome.getController();
		try
		{
			User user = findUserByAttribute(PumaProfileConstantsIfc.uid, mobilyUser.getUid());
			if (user != null)
				pController.deleteUser(user);
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
			throw new ServiceException("LDAPService > there was a problem deleting the user", e);
		}
	}

	/**
	 * @param MSISDN
	 * @param attrName
	 * @param attrValue
	 * @throws ServiceException
	 */

	public void updateUserOnLDAPByMSISDN(String MSISDN, String attrName, String attrValue)
	{
		HashMap<String, Object> userAttrs = new HashMap<String, Object>();
		userAttrs.put(attrName, attrValue);
		User user = findUserByAttribute(PumaProfileConstantsIfc.defaultMsisdn, MSISDN);
		if (user != null)
		{
			updateUserOnLDAP(user, userAttrs);
		}
	}

	/**
	 * Update user on LDAP by user id
	 * 
	 * @param userId
	 * @param attrName
	 * @param attrValue
	 * @throws ServiceException
	 */

	public void updateUserOnLDAPByUserId(String userId, String attrName, String attrValue)
	{
		HashMap<String, Object> userAttrs = new HashMap<String, Object>();
		userAttrs.put(attrName, attrValue);
		User user = findUserByAttribute(PumaProfileConstantsIfc.uid, userId);
		if (user != null)
		{
			updateUserOnLDAP(user, userAttrs);
		}
	}

	/**
	 * Update user on LDAP by user id
	 * 
	 * @param userId
	 * @param attrName
	 * @param attrValue
	 * @throws ServiceException
	 */

	public void updateUserAttributesOnLDAPByUserId(String userId, Map<String, Object> attributes)
	{
		User user = findUserByAttribute(PumaProfileConstantsIfc.uid, userId);
		if (user != null)
		{
			updateUserOnLDAP(user, attributes);
		}
	}

	/**
	 * @param MSISDN
	 * @param attrName
	 * @param attrValue
	 * @throws ServiceException
	 */

	private void updateUserOnLDAP(User user, Map<String, Object> attributes)
	{
		String method = "updateUserOnLDAP";
		PumaHome pumaHome = PumaCachedHome.getInstance().getPumaHome();
		PumaController pController = pumaHome.getController();
		try
		{
			pController.setAttributes(user, attributes);
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
			throw se;
		}
	}

	/**
	 * @param userName
	 * @param newPassword
	 * @throws ServiceException
	 */

	public void updatePasswordOnLDAP(String userName, String newPassword)
	{
		updateUserOnLDAPByUserId(userName, PumaProfileConstantsIfc.password, newPassword);
	}

	public MobilyUser findMobilyUserByAttributes(Map<String, String> attributes)
	{
		PumaHome pumaHome = PumaCachedHome.getInstance().getPumaHome();
		PumaLocator pumaLocator = pumaHome.getLocator();

		List<User> users;
		try
		{
			String query = "";

			int index = 0;
			Set<String> attributeNames = attributes.keySet();
			for (String attributeName : attributeNames)
			{
				String attributeValue = attributes.get(attributeName);

				query += String.format("%s = \'%s\'", attributeName, attributeValue);
				if (index < attributeNames.size() - 1)
				{
					query += String.format(" %s ", " and ");
					index++;
				}
			}
			executionContext.severe("findMobilyUserByAttributes  LDAP Query :" + query);
			users = pumaLocator.findUsersByQuery(query);
			executionContext.severe("findMobilyUserByAttributes  users= :" + users);
			if (users != null && users.size() > 0)
				return getMobilyUser(users.get(0));
		}
		catch (Exception e)
		{
			// e.printStackTrace();
			throw new ServiceException(e.getMessage(), e);
		}

		throw new DataNotFoundException();

	}

	private User findUserByAttribute(String attributeName, String attributeValue)
	{

		String method = "findPortalUserByAttribute";
		executionContext.log(Level.INFO, "LDAPService > findPortalUserByAttribute : start:attrName=" + attributeName + ":attrValue=" + attributeValue, className, method, null);
		PumaHome pumaHome = PumaCachedHome.getInstance().getPumaHome();
		executionContext.log(Level.INFO, "LDAPService > findPortalUserByAttribute : pumaHome=" + pumaHome, className, method, null);
		PumaLocator pumaLocator = pumaHome.getLocator();
		executionContext.log(Level.INFO, "LDAPService > findPortalUserByAttribute : pumaLocator=" + pumaLocator, className, method, null);
		try
		{
			List<User> userList = pumaLocator.findUsersByAttribute(attributeName, attributeValue);
			executionContext.log(Level.INFO, "LDAPService > findPortalUserByAttribute : userList=" + userList, className, method, null);
			if (userList != null && !userList.isEmpty() && userList.size() > 0)
			{
				User user = userList.get(0);
				executionContext.log(Level.INFO, "LDAPService > findPortalUserByAttribute : user=" + user, className, method, null);
				return user;
			}
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
			throw se;
		}
		return null;
	}

	/**
	 * @param attributeName
	 * @param attributeValue
	 * @return
	 * @throws ServiceException
	 */
	public MobilyUser findMobilyUserByAttribute(String attributeName, String attributeValue)
	{
		executionContext.log(Level.INFO, "attribute name = " + attributeName + " attributeValue = " + attributeValue, className, "findMobilyUserByAttribute", null);
		return getMobilyUser(findUserByAttribute(attributeName, attributeValue));
	}

	private MobilyUser getMobilyUser(User user)
	{
		if (user == null)
			return null;
		PumaProfile profile = PumaCachedHome.getInstance().getPumaHome().getProfile();
		List<String> requiredAttributes = new ArrayList<String>();
		requiredAttributes.add(PumaProfileConstantsIfc.cn);
		requiredAttributes.add(PumaProfileConstantsIfc.sn);
		requiredAttributes.add(PumaProfileConstantsIfc.uid);
		requiredAttributes.add(PumaProfileConstantsIfc.defaultMsisdn);
		requiredAttributes.add(PumaProfileConstantsIfc.email);
		requiredAttributes.add(PumaProfileConstantsIfc.preferredLanguage);
		requiredAttributes.add(PumaProfileConstantsIfc.serviceAccountNumber);
		requiredAttributes.add(PumaProfileConstantsIfc.defaultSubscriptionType);
		requiredAttributes.add(PumaProfileConstantsIfc.defaultSubId);
		requiredAttributes.add(PumaProfileConstantsIfc.corpMasterAcctNo);
		requiredAttributes.add(PumaProfileConstantsIfc.corpAPNumber);
		requiredAttributes.add(PumaProfileConstantsIfc.corpAPIDNumber);
		requiredAttributes.add(PumaProfileConstantsIfc.commercialRegisterationID);
		requiredAttributes.add(PumaProfileConstantsIfc.corpSurveyCompleted);
		requiredAttributes.add(PumaProfileConstantsIfc.password);
		requiredAttributes.add(PumaProfileConstantsIfc.givenName);
		requiredAttributes.add(PumaProfileConstantsIfc.Status);
		requiredAttributes.add(PumaProfileConstantsIfc.firstName);
		requiredAttributes.add(PumaProfileConstantsIfc.lastName);
		requiredAttributes.add(PumaProfileConstantsIfc.custSegment);
		requiredAttributes.add(PumaProfileConstantsIfc.roleId);
		requiredAttributes.add(PumaProfileConstantsIfc.SecurityQuestion);
		requiredAttributes.add(PumaProfileConstantsIfc.SecurityAnswer);
		requiredAttributes.add(PumaProfileConstantsIfc.birthDate);
		requiredAttributes.add(PumaProfileConstantsIfc.nationality);
		requiredAttributes.add(PumaProfileConstantsIfc.Gender);
		requiredAttributes.add(PumaProfileConstantsIfc.registerationDate);
		requiredAttributes.add(PumaProfileConstantsIfc.iqama);
		requiredAttributes.add(PumaProfileConstantsIfc.facebookId);
		requiredAttributes.add(PumaProfileConstantsIfc.registrationSourceId);
		requiredAttributes.add(PumaProfileConstantsIfc.userAcctReference);
		requiredAttributes.add(PumaProfileConstantsIfc.defaultSubSubscriptionType);
		requiredAttributes.add(PumaProfileConstantsIfc.contactNumber);
		requiredAttributes.add(PumaProfileConstantsIfc.passwordChangedTime);

		Map<String, Object> attributeValues = null;
		try
		{
			attributeValues = profile.getAttributes(user, requiredAttributes);
		}
		catch (Exception e)
		{
			throw new RuntimeException("Couldn't load user attributes from LDAP", e);
		}

		MobilyUser mobilyUser = new MobilyUser();
		try
		{
			mobilyUser.setCn(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.cn)));
			mobilyUser.setSn(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.sn)));
			mobilyUser.setUid(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.uid)));
			mobilyUser.setDefaultMsisdn(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.defaultMsisdn)));
			mobilyUser.setEmail(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.email)));
			mobilyUser.setPreferredLanguage(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.preferredLanguage)));
			mobilyUser.setServiceAccountNumber(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.serviceAccountNumber)));
			mobilyUser.setDefaultSubscriptionType(getIntegerFromMap(attributeValues, PumaProfileConstantsIfc.defaultSubscriptionType));
			mobilyUser.setDefaultSubSubscriptionType(getIntegerFromMap(attributeValues, PumaProfileConstantsIfc.defaultSubSubscriptionType));
			mobilyUser.setDefaultSubId(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.defaultSubId)));
			mobilyUser.setCorpMasterAcctNo(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.corpMasterAcctNo)));
			mobilyUser.setCorpAPNumber(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.corpAPNumber)));
			mobilyUser.setCorpAPIDNumber(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.corpAPIDNumber)));
			mobilyUser.setCommercialRegisterationID(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.commercialRegisterationID)));
			mobilyUser.setCorpSurveyCompleted(getBooleanFromMap(attributeValues, PumaProfileConstantsIfc.corpSurveyCompleted));
			mobilyUser.setGivenName(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.givenName)));
			mobilyUser.setStatus(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.Status)));
			mobilyUser.setFirstName(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.firstName)));
			mobilyUser.setLastName(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.lastName)));
			mobilyUser.setCustSegment(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.custSegment)));
			mobilyUser.setRoleId(getIntegerFromMap(attributeValues, PumaProfileConstantsIfc.roleId));
			mobilyUser.setSecurityQuestion(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.SecurityQuestion)));
			mobilyUser.setSecurityAnswer(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.SecurityAnswer)));
			mobilyUser.setBirthDate(getDateFromMap(attributeValues, PumaProfileConstantsIfc.birthDate));
			mobilyUser.setNationality(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.nationality)));
			mobilyUser.setGender(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.Gender)));
			mobilyUser.setRegisterationDate(getDateFromMap(attributeValues, PumaProfileConstantsIfc.registerationDate));
			mobilyUser.setIqama(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.iqama)));
			mobilyUser.setFacebookId(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.facebookId)));
			mobilyUser.setUserAcctReference(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.userAcctReference)));
			mobilyUser.setUserPassword(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.password)));
			mobilyUser.setContactNumber(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.contactNumber)));
			mobilyUser.setPasswordChangedTime(getDateFromMap(attributeValues, PumaProfileConstantsIfc.passwordChangedTime));
		}
		catch (Exception e)
		{
			executionContext.log(Level.INFO, "getMobilyUser > Exception while populating mobilyUser = " + mobilyUser, className, "getMobilyUser", null);
		}

		return mobilyUser;
	}

	private Date getDateFromMap(Map<String, Object> attributeValues, String attributeName)
	{
		try
		{
			// String value =
			// String.valueOf(attributeValues.get(attributeName));
			// executionContext.log(Level.INFO, "attributeName : "
			// +attributeName+ "value : " + value, className, "getDateFromMap");
			// if (value == null || value.trim().equals("") ||
			// value.trim().equals("null"))
			// return null;
			// return new SimpleDateFormat("yyyyMMddHHmmss").parse(value);
			Date theDate = (Date) attributeValues.get(attributeName);
			// executionContext.log(Level.INFO, "attributeName : "
			// +attributeName+ " theDate : " + theDate, className,
			// "getDateFromMap");
			return theDate;
		}
		catch (Exception e)
		{
			// executionContext.log(Level.SEVERE, e.getMessage(), className,
			// "getDateFromMap",e);
			return null;
		}
	}

	private Integer getIntegerFromMap(Map<String, Object> attributeValues, String attributeName)
	{
		try
		{
			String value = (String.valueOf(attributeValues.get(attributeName)));
			if (value == null || value.trim().equals("") || value.trim().equals("null"))
				return null;
			return Integer.parseInt(value);
		}
		catch (Exception e)
		{
			return null;
		}
	}

	private Boolean getBooleanFromMap(Map<String, Object> attributeValues, String attributeName)
	{
		try
		{
			String value = (String.valueOf(attributeValues.get(attributeName)));
			if (value == null || value.trim().equals("") || value.trim().equals("null"))
				return null;
			return Boolean.valueOf(value);
		}
		catch (Exception e)
		{
			return null;
		}
	}

	public MobilyUser authenticateAndGetUserDetails(String username, String password)
	{
		String method = "authenticateAndGetUserDetails";
		executionContext.log(Level.INFO, "LDAPService > authenticateAndGetUserDetails started...", className, method);

		boolean nativeImpl = true;
		MobilyUser user = null;
		// if(nativeImpl){
		// if(LDAPManager.getInstance().isValidUser(username, password)){
		// user = findMobilyUserByAttribute(PumaProfileConstantsIfc.uid,
		// FormatterUtility.checkNull(username));
		// return user;
		// }
		// } else {
		Map<String, String> attributes = new HashMap<String, String>();
		attributes.put(PumaProfileConstantsIfc.uid, username);
		attributes.put(PumaProfileConstantsIfc.password, password);
		user = findMobilyUserByAttributes(attributes);
		return user;
		// }

		// return null;
	}

	@Override
	public boolean checkMembership(String username, String groupName)
	{
		boolean isMember = false;
		String method = "checkMembership";
		PumaHome pumaHome = PumaCachedHome.getInstance().getPumaHome();

		try
		{
			User user = findUserByAttribute(PumaProfileConstantsIfc.uid, username);
			executionContext.log(Level.INFO, "LDAPService > checkMembership > user found in LDAP::" + user, className, method, null);

			if (user != null)
			{
				PumaLocator pumaLocator = pumaHome.getLocator();
				PumaProfile pumaProfile = pumaHome.getProfile();

				List<Group> groups = pumaLocator.findGroupsByPrincipal(user, false);
				executionContext.audit(Level.INFO, "LDAPService > checkMembership > groups:" + groups, className, method);

				ArrayList<String> GROUP_ATTRS = null;
				GROUP_ATTRS = new ArrayList<String>(1);
				GROUP_ATTRS.add("cn");

				executionContext.audit(Level.INFO, "LDAPService > checkMembership > requiredAttributes:" + GROUP_ATTRS, className, method);
				if (groups != null)
				{
					for (Iterator iter = groups.iterator(); iter.hasNext();)
					{
						Group group = (Group) iter.next();
						executionContext.audit(Level.INFO, "LDAPService > checkMembership > group:" + group, className, method);
						Map attributeMap = pumaProfile.getAttributes(group, GROUP_ATTRS);
						executionContext.audit(Level.INFO, "LDAPService > checkMembership > attributeMap:" + attributeMap, className, method);
						for (Iterator it = attributeMap.values().iterator(); it.hasNext();)
						{
							Object obj = it.next();
							String groupCN = null;
							if (obj instanceof List)
							{
								List test = (ArrayList) obj;
								for (Iterator iterator = test.iterator(); iterator.hasNext();)
								{
									groupCN = (String) iterator.next();
								}
							}
							else if (obj instanceof String)
							{
								groupCN = (String) obj;
							}
							executionContext.audit(Level.INFO, "LDAPService > checkMembership > groupCN:" + groupCN, className, method);

							if (groupCN.equals(groupName))
							{
								isMember = true;
							}
						}
					}
				}

			} // end: f(user != null)

		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "LDAPService > checkMembership > Exception:" + e.getMessage(), className, method);
		}
		executionContext.audit(Level.INFO, "LDAPService > checkMembership > isMember status:" + isMember, className, method);
		return isMember;
	}

	@Override
	public void assignUserToGroup(String username, String groupName)
	{
		String method = "assignUserToGroup";
		executionContext.log(Level.INFO, "LDAPService > assignUserToGroup Add user[" + username + "] to Group[" + groupName + "]", className, method, null);
		PumaHome pumaHome = PumaCachedHome.getInstance().getPumaHome();
		PumaController pController = pumaHome.getController();
		try
		{
			User user = findUserByAttribute(PumaProfileConstantsIfc.uid, username);

			List<Group> ldapGroupList = (List<Group>) pumaHome.getLocator().findGroupsByAttribute(PumaProfileConstantsIfc.cn, groupName);

			executionContext.log(Level.INFO, "LDAPService > assignUserToGroup > ldapGroupList " + ldapGroupList, className, method, null);
			if (ldapGroupList != null && ldapGroupList.size() > 0)
			{
				// Group is found
				executionContext.log(Level.INFO, "LDAPService > assignUserToGroup > groupName [" + groupName + "] is found in LDAP", className, method, null);

				Group group = (Group) ldapGroupList.get(0);

				List<User> list = new ArrayList<User>();
				list.add(user);
				executionContext.log(Level.INFO, "LDAPService > assignUserToGroup > Add user to group started", className, method, null);
				pController.addToGroup(group, list);
				executionContext.log(Level.INFO, "LDAPService > assignUserToGroup > Add user to group ended", className, method, null);
			}

		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "LDAPService > assignUserToGroup > Exception:" + e.getMessage(), className, method);
			throw new ServiceException(e.getMessage(), e);
		}

	}

	public List<MobilyUser> findMobilyUserListByAttributes(Map<String, String> attributes)
	{
		PumaHome pumaHome = PumaCachedHome.getInstance().getPumaHome();
		PumaLocator pumaLocator = pumaHome.getLocator();

		List<User> users;
		try
		{
			String query = "";

			int index = 0;
			Set<String> attributeNames = attributes.keySet();
			for (String attributeName : attributeNames)
			{
				String attributeValue = attributes.get(attributeName);

				query += String.format("%s = \'%s\'", attributeName, attributeValue);
				if (index < attributeNames.size() - 1)
				{
					query += String.format(" %s ", " and ");
					index++;
				}
			}
			executionContext.severe("findMobilyUserByAttributes  LDAP Query :" + query);
			users = pumaLocator.findUsersByQuery(query);
			executionContext.severe("findMobilyUserByAttributes  users= :" + users);
			if (users != null && users.size() > 0)
				return getMobilyUserList(users);
		}
		catch (Exception e)
		{
			// e.printStackTrace();
			throw new ServiceException(e.getMessage(), e);
		}

		throw new DataNotFoundException();

	}

	private List<User> findUsersByAttribute(String attributeName, String attributeValue)
	{

		String method = "findPortalUserByAttribute";
		executionContext.log(Level.INFO, "LDAPService > findPortalUserByAttribute : start:attrName=" + attributeName + ":attrValue=" + attributeValue, className, method, null);
		PumaHome pumaHome = PumaCachedHome.getInstance().getPumaHome();
		executionContext.log(Level.INFO, "LDAPService > findPortalUserByAttribute : pumaHome=" + pumaHome, className, method, null);
		PumaLocator pumaLocator = pumaHome.getLocator();
		executionContext.log(Level.INFO, "LDAPService > findPortalUserByAttribute : pumaLocator=" + pumaLocator, className, method, null);
		try
		{
			List<User> userList = pumaLocator.findUsersByAttribute(attributeName, attributeValue);
			if (null != userList && !userList.isEmpty())
				return userList;
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
			throw se;
		}
		return null;
	}

	/**
	 * @param attributeName
	 * @param attributeValue
	 * @return
	 * @throws ServiceException
	 */
	public List<MobilyUser> findMobilyUserListByAttribute(String attributeName, String attributeValue)
	{
		executionContext.log(Level.INFO, "attribute name = " + attributeName + " attributeValue = " + attributeValue, className, "findMobilyUserByAttribute", null);
		return getMobilyUserList(findUsersByAttribute(attributeName, attributeValue));
	}

	private List<MobilyUser> getMobilyUserList(List<User> userList)
	{
		if (userList == null || userList.size() == 0)
			return null;
		PumaProfile profile = PumaCachedHome.getInstance().getPumaHome().getProfile();
		List<String> requiredAttributes = new ArrayList<String>();
		requiredAttributes.add(PumaProfileConstantsIfc.cn);
		requiredAttributes.add(PumaProfileConstantsIfc.sn);
		requiredAttributes.add(PumaProfileConstantsIfc.uid);
		requiredAttributes.add(PumaProfileConstantsIfc.defaultMsisdn);
		requiredAttributes.add(PumaProfileConstantsIfc.email);
		requiredAttributes.add(PumaProfileConstantsIfc.preferredLanguage);
		requiredAttributes.add(PumaProfileConstantsIfc.serviceAccountNumber);
		requiredAttributes.add(PumaProfileConstantsIfc.defaultSubscriptionType);
		requiredAttributes.add(PumaProfileConstantsIfc.defaultSubId);
		requiredAttributes.add(PumaProfileConstantsIfc.corpMasterAcctNo);
		requiredAttributes.add(PumaProfileConstantsIfc.corpAPNumber);
		requiredAttributes.add(PumaProfileConstantsIfc.corpAPIDNumber);
		requiredAttributes.add(PumaProfileConstantsIfc.commercialRegisterationID);
		requiredAttributes.add(PumaProfileConstantsIfc.corpSurveyCompleted);
		requiredAttributes.add(PumaProfileConstantsIfc.password);
		requiredAttributes.add(PumaProfileConstantsIfc.givenName);
		requiredAttributes.add(PumaProfileConstantsIfc.Status);
		requiredAttributes.add(PumaProfileConstantsIfc.firstName);
		requiredAttributes.add(PumaProfileConstantsIfc.lastName);
		requiredAttributes.add(PumaProfileConstantsIfc.custSegment);
		requiredAttributes.add(PumaProfileConstantsIfc.roleId);
		requiredAttributes.add(PumaProfileConstantsIfc.SecurityQuestion);
		requiredAttributes.add(PumaProfileConstantsIfc.SecurityAnswer);
		requiredAttributes.add(PumaProfileConstantsIfc.birthDate);
		requiredAttributes.add(PumaProfileConstantsIfc.nationality);
		requiredAttributes.add(PumaProfileConstantsIfc.Gender);
		requiredAttributes.add(PumaProfileConstantsIfc.registerationDate);
		requiredAttributes.add(PumaProfileConstantsIfc.iqama);
		requiredAttributes.add(PumaProfileConstantsIfc.facebookId);
		requiredAttributes.add(PumaProfileConstantsIfc.registrationSourceId);
		requiredAttributes.add(PumaProfileConstantsIfc.userAcctReference);
		requiredAttributes.add(PumaProfileConstantsIfc.defaultSubSubscriptionType);
		requiredAttributes.add(PumaProfileConstantsIfc.contactNumber);
		requiredAttributes.add(PumaProfileConstantsIfc.passwordChangedTime);
		Map<String, Object> attributeValues = null;
		MobilyUser mobilyUser = null;
		List<MobilyUser> mobilyUserList = new ArrayList<MobilyUser>();
		for (User user : userList)
		{

			try
			{
				attributeValues = profile.getAttributes(user, requiredAttributes);

			}
			catch (Exception e)
			{
				throw new RuntimeException("Couldn't load user attributes from LDAP", e);
			}
			mobilyUser = new MobilyUser();
			mobilyUser.setCn(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.cn)));
			mobilyUser.setSn(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.sn)));
			mobilyUser.setUid(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.uid)));
			mobilyUser.setDefaultMsisdn(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.defaultMsisdn)));
			mobilyUser.setEmail(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.email)));
			mobilyUser.setPreferredLanguage(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.preferredLanguage)));
			mobilyUser.setServiceAccountNumber(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.serviceAccountNumber)));
			mobilyUser.setDefaultSubscriptionType(getIntegerFromMap(attributeValues, PumaProfileConstantsIfc.defaultSubscriptionType));
			mobilyUser.setDefaultSubSubscriptionType(getIntegerFromMap(attributeValues, PumaProfileConstantsIfc.defaultSubSubscriptionType));
			mobilyUser.setDefaultSubId(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.defaultSubId)));
			mobilyUser.setCorpMasterAcctNo(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.corpMasterAcctNo)));
			mobilyUser.setCorpAPNumber(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.corpAPNumber)));
			mobilyUser.setCorpAPIDNumber(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.corpAPIDNumber)));
			mobilyUser.setCommercialRegisterationID(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.commercialRegisterationID)));
			mobilyUser.setCorpSurveyCompleted(getBooleanFromMap(attributeValues, PumaProfileConstantsIfc.corpSurveyCompleted));
			mobilyUser.setGivenName(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.givenName)));
			mobilyUser.setStatus(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.Status)));
			mobilyUser.setFirstName(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.firstName)));
			mobilyUser.setLastName(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.lastName)));
			mobilyUser.setCustSegment(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.custSegment)));
			mobilyUser.setRoleId(getIntegerFromMap(attributeValues, PumaProfileConstantsIfc.roleId));
			mobilyUser.setSecurityQuestion(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.SecurityQuestion)));
			mobilyUser.setSecurityAnswer(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.SecurityAnswer)));
			mobilyUser.setBirthDate(getDateFromMap(attributeValues, PumaProfileConstantsIfc.birthDate));
			mobilyUser.setNationality(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.nationality)));
			mobilyUser.setGender(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.Gender)));
			mobilyUser.setRegisterationDate(getDateFromMap(attributeValues, PumaProfileConstantsIfc.registerationDate));
			mobilyUser.setIqama(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.iqama)));
			mobilyUser.setFacebookId(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.facebookId)));
			mobilyUser.setUserAcctReference(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.userAcctReference)));
			mobilyUser.setUserPassword(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.password)));
			mobilyUser.setContactNumber(String.valueOf(attributeValues.get(PumaProfileConstantsIfc.contactNumber)));
			mobilyUser.setPasswordChangedTime(getDateFromMap(attributeValues, PumaProfileConstantsIfc.passwordChangedTime));
			mobilyUserList.add(mobilyUser);
		}

		return mobilyUserList;

	}
}
package sa.com.mobily.eportal.common.util.puma;

import java.util.logging.Level;

import javax.naming.Context;
import javax.naming.InitialContext;

import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;

import com.ibm.portal.um.PumaHome;

public class PumaCachedHome
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonPortalLoggerIfc.PUMA_SERVICE_LOGGER_NAME);

	private static final String className = PumaCachedHome.class.getName();

	private static PumaCachedHome instance;

	private PumaHome pumaHome;

	private PumaCachedHome()
	{
		String method = "PumaCachedHome";
		try
		{
			Context ctx = new InitialContext();
			this.pumaHome = (PumaHome) ctx.lookup(PumaHome.JNDI_NAME);
		}
		catch (Exception e)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, e.getMessage(), className, method, e);
			executionContext.log(logEntryVO);
			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
			throw se;
		}
	}

	public PumaHome getPumaHome()
	{
		return pumaHome;
	}

	public static synchronized PumaCachedHome getInstance()
	{
		if (instance == null)
		{
			instance = new PumaCachedHome();
		}
		return instance;
	}
}
//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sa.com.mobily.eportal.core.api.Context;
import sa.com.mobily.eportal.core.api.ErrorCodes;
import sa.com.mobily.eportal.core.api.InternalServerErrorException;
import sa.com.mobily.eportal.core.api.MethodNotSupportedException;
import sa.com.mobily.eportal.core.api.ResourceAlreadyExistsException;
import sa.com.mobily.eportal.core.api.ResourceManager;
import sa.com.mobily.eportal.core.api.ResourceNotFoundException;
import sa.com.mobily.eportal.core.api.ServiceException;
import sa.com.mobily.eportal.core.api.UserAuthorizationException;
import sa.com.mobily.eportal.core.api.ValidationException;
import sa.com.mobily.eportal.core.service.EPortalUtils;
import sa.com.mobily.eportal.core.service.Logger;
import sa.com.mobily.eportal.core.service.ResourceMapper;
import sa.com.mobily.eportal.core.service.ResourceMapper.MappingException;

public class RESTServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(RESTServlet.class);
    private static final String RESOURCE_MANAGER_NAME = "sa.com.mobily.eportal.core.api.resource-manager";
    private static final String RESOURCE_MANAGER_JNDI_NAME = "java:comp/env/ejb/ResourceManager";
    private static final boolean ENABLE_TEST = true;
    private static final String ERROR_MSG_BUNDLE = "nl.error-messages";
    private static final String ERROR_MSG_KEY_PREFIX = "error.message.";

    private ResourceMapper fMapper = new ResourceMapper();
    private Properties resources = new Properties();

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        getResourceManager(config.getServletContext());
        if (ENABLE_TEST) {
            try {
                resources.load(config.getServletContext().getResourceAsStream("/WEB-INF/portlet.properties"));
            } catch (IOException e) {
                LOG.error("Unable to load test.properties", e);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // retrieve the Resource URI from the request
        String uri = getResourceURI(request);
        LOG.info("GET: " + uri);

        if (ENABLE_TEST && uri.startsWith("/_")) {

            try {
                String portlet = uri.substring(2);

                for (String r: resources.getProperty(portlet, "").split(",")) {
                    if (r.length() > 0) {
                        try {
                            request.setAttribute(r, getResourceManager().get(getCtx(request), r));
                        } catch (MethodNotSupportedException e) {
                            request.setAttribute(r, getResourceManager().post(getCtx(request), r, "{}"));
                        }
                    }
                }

                // Invoke the JSP to render
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/jsp/" + portlet + ".jsp");
                rd.include(request, response);

            } catch (ResourceNotFoundException e) {
                handleException(request, response, e, HttpServletResponse.SC_NOT_FOUND);
            } catch (UserAuthorizationException e) {
                handleException(request, response, e, HttpServletResponse.SC_UNAUTHORIZED);
            } catch (MethodNotSupportedException e) {
                handleException(request, response, e, HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            } catch (MappingException e) {
                handleException(request, response, e, HttpServletResponse.SC_BAD_REQUEST);
            } catch (ValidationException e) {
                handleException(request, response, e, HttpServletResponse.SC_BAD_REQUEST);
            } catch (ResourceAlreadyExistsException e) {
                handleException(request, response, e, HttpServletResponse.SC_CONFLICT);
            } catch (InternalServerErrorException e) {
                handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            } catch (RuntimeException e) {
                handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            } catch (ServiceException e) {
                handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }

        } else {

            try {
                // process the request
                Object result = getResourceManager().get(getCtx(request), uri);

                // writing the result to the response
                response.getWriter().write(fMapper.write(result));
                response.setStatus(HttpServletResponse.SC_OK);

            } catch (ResourceNotFoundException e) {
                handleException(request, response, e, HttpServletResponse.SC_NOT_FOUND);
            } catch (UserAuthorizationException e) {
                handleException(request, response, e, HttpServletResponse.SC_UNAUTHORIZED);
            } catch (MethodNotSupportedException e) {
                handleException(request, response, e, HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            } catch (MappingException e) {
                handleException(request, response, e, HttpServletResponse.SC_BAD_REQUEST);
            } catch (ValidationException e) {
                handleException(request, response, e, HttpServletResponse.SC_BAD_REQUEST);
            } catch (ResourceAlreadyExistsException e) {
                handleException(request, response, e, HttpServletResponse.SC_CONFLICT);
            } catch (InternalServerErrorException e) {
                handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            } catch (RuntimeException e) {
                handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            } catch (ServiceException e) {
                handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            // retrieve the Resource URI from the request
            String uri = getResourceURI(request);
            LOG.info("POST: " + uri);

            // retrieve the request body
            String data = getRequestData(request);

            // process the request
            Object result = getResourceManager().post(getCtx(request), uri, data);

            // writing the result to the response
            if (result != null) {
                response.getWriter().write(fMapper.write(result));
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            }

        } catch (ResourceNotFoundException e) {
            handleException(request, response, e, HttpServletResponse.SC_NOT_FOUND);
        } catch (UserAuthorizationException e) {
            handleException(request, response, e, HttpServletResponse.SC_UNAUTHORIZED);
        } catch (MethodNotSupportedException e) {
            handleException(request, response, e, HttpServletResponse.SC_METHOD_NOT_ALLOWED);
        } catch (MappingException e) {
            handleException(request, response, e, HttpServletResponse.SC_BAD_REQUEST);
        } catch (ValidationException e) {
            handleException(request, response, e, HttpServletResponse.SC_BAD_REQUEST);
        } catch (ResourceAlreadyExistsException e) {
            handleException(request, response, e, HttpServletResponse.SC_CONFLICT);
        } catch (InternalServerErrorException e) {
            handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (RuntimeException e) {
            handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (ServiceException e) {
            handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            // retrieve the Resource URI from the request
            String uri = getResourceURI(request);
            LOG.info("PUT: " + uri);

            // retrieve the request body
            String data = getRequestData(request);

            // process the request
            getResourceManager().put(getCtx(request), uri, data);

            // writing the result to the response
            response.setStatus(HttpServletResponse.SC_OK);

        } catch (ResourceNotFoundException e) {
            handleException(request, response, e, HttpServletResponse.SC_NOT_FOUND);
        } catch (UserAuthorizationException e) {
            handleException(request, response, e, HttpServletResponse.SC_UNAUTHORIZED);
        } catch (MethodNotSupportedException e) {
            handleException(request, response, e, HttpServletResponse.SC_METHOD_NOT_ALLOWED);
        } catch (MappingException e) {
            handleException(request, response, e, HttpServletResponse.SC_BAD_REQUEST);
        } catch (ValidationException e) {
            handleException(request, response, e, HttpServletResponse.SC_BAD_REQUEST);
        } catch (ResourceAlreadyExistsException e) {
            handleException(request, response, e, HttpServletResponse.SC_CONFLICT);
        } catch (InternalServerErrorException e) {
            handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (RuntimeException e) {
            handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (ServiceException e) {
            handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            // retrieve the Resource URI from the request
            String uri = getResourceURI(request);
            LOG.info("DELETE: " + uri);

            // process the request
            getResourceManager().delete(getCtx(request), uri);

            // writing the result to the response
            response.setStatus(HttpServletResponse.SC_OK);

        } catch (ResourceNotFoundException e) {
            handleException(request, response, e, HttpServletResponse.SC_NOT_FOUND);
        } catch (UserAuthorizationException e) {
            handleException(request, response, e, HttpServletResponse.SC_UNAUTHORIZED);
        } catch (MethodNotSupportedException e) {
            handleException(request, response, e, HttpServletResponse.SC_METHOD_NOT_ALLOWED);
        } catch (MappingException e) {
            handleException(request, response, e, HttpServletResponse.SC_BAD_REQUEST);
        } catch (ValidationException e) {
            handleException(request, response, e, HttpServletResponse.SC_BAD_REQUEST);
        } catch (ResourceAlreadyExistsException e) {
            handleException(request, response, e, HttpServletResponse.SC_CONFLICT);
        } catch (InternalServerErrorException e) {
            handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (RuntimeException e) {
            handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (ServiceException e) {
            handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    private String getResourceURI(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null || pathInfo.length() == 0) {
            pathInfo = request.getServletPath();
        }
        if (pathInfo == null || pathInfo.length() == 0) {
            pathInfo = "/";
        }
        if ("/index.html".equals(pathInfo)) pathInfo = "/";  // A workaround for Tomcat
        pathInfo = pathInfo.startsWith("/api/") ? pathInfo.substring(4) : pathInfo;

        // to include the parameters in the URI to be resolved later in the resolver
        String queryString = request.getQueryString();

        if (queryString != null) {
            pathInfo += "?" + request.getQueryString();
        }

        return pathInfo;
    }

    private String getRequestData(HttpServletRequest request) throws IOException{

        // Retrieving the request body
        StringBuffer data = new StringBuffer();
        BufferedReader reader = request.getReader();
        char[] buf = new char[4096]; // 4 KB char buffer
        int len;
        while ((len = reader.read(buf, 0, buf.length)) != -1) {
            data.append(buf, 0, len);
        }

        return (data.length() != 0) ? data.toString() : null;
    }

    private Context getCtx(HttpServletRequest request) {
        return new WebContext(request);
    }

    /**
     * A helper method to retrieve the view manager from the application context
     */
    private ResourceManager getResourceManager() throws ServletException {
        return (ResourceManager) getServletContext().getAttribute(RESOURCE_MANAGER_NAME);
    }

    private static ResourceManager getResourceManager(ServletContext ctx) throws ServletException {
        ResourceManager rm = (ResourceManager) ctx.getAttribute(RESOURCE_MANAGER_NAME);
        try {
            if (rm == null) {
                javax.naming.Context context = new InitialContext();
                rm = (ResourceManager) context.lookup(RESOURCE_MANAGER_JNDI_NAME);
                ctx.setAttribute(RESOURCE_MANAGER_NAME, rm);
            }
        } catch (NamingException e) {
            e.printStackTrace();
            throw new ServletException(e);
        }
        return rm;
    }

    /**
     * Resolves the passed exception and returns the appropriate error response to the client.
     * @param request the request object
     * @param response  response object at which the error will be written.
     * @param exception the exception to be resolved.
     * @param httpStatusCode http status code
     */
    private void handleException(HttpServletRequest request, HttpServletResponse response, Exception e, int httpStatusCode) {
        LOG.error(e.getMessage(), e);

        ErrorDetails error = new ErrorDetails();
        error.setErrorCode(ErrorCodes.GENERAL_ERROR);   // default error code, if not assigned
        error.setErrorDetails(e.getMessage());

        Object[] messageParams = null;
        // if e is ServiceException, get the errorCode and messageParams(if any) from it
        if(e instanceof ServiceException) {
            ServiceException serviceEx = (ServiceException) e;
            error.setErrorCode(serviceEx.getErrorCode());
            messageParams = serviceEx.getMessageParams();
        }

        // get the localized error message
        try {
            Context ctx = getCtx(request);
            String errorMessage = getLocalizedErrorMessage(error.getErrorCode(), ctx.getLocale());

            // if there are message substitution parameters
            if(messageParams != null) {
                errorMessage = MessageFormat.format(errorMessage, messageParams);       // substitute the parameters in the message
            }

            error.setErrorMessage(errorMessage);
            response.getWriter().write(new ResourceMapper().write(error));      // write error details to the response
        } catch(Exception ex) {
            LOG.error("Failed to resolve localized exception message.", ex);
        }

        response.setStatus(httpStatusCode);
    }

    private String getLocalizedErrorMessage(int errorCode, Locale locale) {
        String msg = EPortalUtils.getProperty(ERROR_MSG_BUNDLE, locale, ERROR_MSG_KEY_PREFIX + errorCode);

        if(msg == null) {
            LOG.error("Unable to get localized exception message for error code [" + errorCode + "] and locale [" + locale + "]");
            return EPortalUtils.getProperty(ERROR_MSG_BUNDLE, locale, ERROR_MSG_KEY_PREFIX + ErrorCodes.GENERAL_ERROR);
        }

        return msg;
    }
}

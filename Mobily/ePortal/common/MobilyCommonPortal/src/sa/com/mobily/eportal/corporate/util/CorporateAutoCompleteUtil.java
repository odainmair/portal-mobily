package sa.com.mobily.eportal.corporate.util;

import java.io.Serializable;
import java.util.List;

import sa.com.mobily.eportal.cacheinstance.GeneralDistributedMap;
import sa.com.mobily.eportal.cacheinstance.constants.CacheKeyConstantIfc;
import sa.com.mobily.eportal.corporate.dao.AccountHierarchyDAO;
import sa.com.mobily.valueobject.common.AutoCompleteVO;

public class CorporateAutoCompleteUtil{
	
	private CorporateAutoCompleteUtil(){}
	private static CorporateAutoCompleteUtil INSTANCE = new CorporateAutoCompleteUtil();
	
	public static CorporateAutoCompleteUtil getInstance(){
		return INSTANCE;
	}
	
	public List<AutoCompleteVO> getCorporateLinesForAutoComplete(String corporateAccountnumber){
		List<AutoCompleteVO> autoList = null;
		GeneralDistributedMap map = GeneralDistributedMap.getInstance();
		String cacheKey = CacheKeyConstantIfc.PREFIX_CORP_AUTO_COMPLETE + corporateAccountnumber;
		Object list = map.getCachedData(cacheKey);
		if (list == null){
			autoList = AccountHierarchyDAO.getInstance().getCorporateLinesForAutoComplete(corporateAccountnumber);
			map.setCachedData(cacheKey, (Serializable) autoList);
		}else{
			autoList = (List<AutoCompleteVO>) list;
		}
		return autoList;
	}
	
	public List<AutoCompleteVO> getCorporateLinesForAutoCompletePrepaid(String corporateAccountnumber){
		List<AutoCompleteVO> autoList = null;
		GeneralDistributedMap map = GeneralDistributedMap.getInstance();
		String cacheKey = CacheKeyConstantIfc.PREFIX_CORP_AUTO_COMPLETE_PREPAID + corporateAccountnumber;
		Object list = map.getCachedData(cacheKey);
		if (list == null){
			autoList = AccountHierarchyDAO.getInstance().getCorporateLinesForAutoComplete(corporateAccountnumber, AccountHierarchyDAO.LINE_TYPE_PRE_PAID);
			map.setCachedData(cacheKey, (Serializable) autoList);
		}else{
			autoList = (List<AutoCompleteVO>) list;
		}
		return autoList;
	}
	
	public List<AutoCompleteVO> getCorporateLinesForAutoCompletePostpaid(String corporateAccountnumber){
		List<AutoCompleteVO> autoList = null;
		GeneralDistributedMap map = GeneralDistributedMap.getInstance();
		String cacheKey = CacheKeyConstantIfc.PREFIX_CORP_AUTO_COMPLETE_POSTPAID + corporateAccountnumber;
		Object list = map.getCachedData(cacheKey);
		if (list == null){
			autoList = AccountHierarchyDAO.getInstance().getCorporateLinesForAutoComplete(corporateAccountnumber, AccountHierarchyDAO.LINE_TYPE_POST_PAID);
			map.setCachedData(cacheKey, (Serializable) autoList);
		}else{
			autoList = (List<AutoCompleteVO>) list;
		}
		return autoList;
	}
	
}

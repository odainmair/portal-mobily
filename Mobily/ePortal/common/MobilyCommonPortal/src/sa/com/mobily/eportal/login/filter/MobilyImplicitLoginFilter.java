package sa.com.mobily.eportal.login.filter;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sa.com.mobily.eportal.authentication.service.view.UserAuthenticationServiceRemote;
import sa.com.mobily.eportal.authentication.vo.UserAuthenticationInputVO;
import sa.com.mobily.eportal.cache.util.CacheManagerServiceUtil;
import sa.com.mobily.eportal.common.beanlocator.BeanLocator;
import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.service.dao.UserAccountDAO;
import sa.com.mobily.eportal.common.service.util.LoggerHelper;
import sa.com.mobily.eportal.common.util.puma.PumaUtil;
import sa.com.mobily.eportal.customerinfo.constant.RoleIdConstant;

import com.ibm.portal.auth.FilterChainContext;
import com.ibm.portal.auth.ImplicitLoginFilter;
import com.ibm.portal.auth.ImplicitLoginFilterChain;
import com.ibm.portal.auth.exceptions.AuthenticationException;
import com.ibm.portal.auth.exceptions.AuthenticationFailedException;
import com.ibm.portal.auth.exceptions.SessionTimeOutException;
import com.ibm.portal.auth.exceptions.SystemLoginException;
import com.ibm.portal.security.SecurityFilterConfig;
import com.ibm.portal.security.exceptions.SecurityFilterInitException;
import com.ibm.websphere.security.WSSecurityException;

/**
 * After login through external authentication (Webseal in our case) into
 * Websphere Portal, this implicit filter will be invoked before going to any
 * portal page. Here we can put all our post-login logic.
 * 
 * @author Muzammil Mohsin Shaikh
 */

public class MobilyImplicitLoginFilter implements ImplicitLoginFilter
{
	private static final Logger logger = LoggerHelper.getLogger(CommonPortalLoggerIfc.COMMON_Portal_LOGGER_NAME);

	@Override
	public void init(SecurityFilterConfig config) throws SecurityFilterInitException
	{
		try
		{
			logger.log(Level.INFO, "MobilyImplicitLoginFilter > init() updated!");
		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, "MobilyImplicitLoginFilter > init() -> Error initializing Filter: " + e.getMessage());
		}
	}

	/**
	 * Gets called after user gets authenticated into Portal via external
	 * authentication. Here we fetch current users uid and roleId from TDS. Then
	 * we get users telecom profile from cache. Then we fetch the Home page to
	 * redirect user to.
	 */
	@Override
	public void login(HttpServletRequest request, HttpServletResponse response, FilterChainContext filterChainContext, String realm, ImplicitLoginFilterChain chain)
			throws LoginException, WSSecurityException, SessionTimeOutException, AuthenticationFailedException, AuthenticationException, SystemLoginException,
			com.ibm.portal.auth.exceptions.LoginException
	{
		try
		{
			PumaUtil pumaUtil = new PumaUtil();
			request.getSession().setAttribute("uid", pumaUtil.getUid());
			if (pumaUtil.getRoleId() != null)
			{
				logger.log(Level.INFO, "MobilyImplicitLoginFilter > Role ID: " + pumaUtil.getRoleId());
				logger.log(Level.INFO, "MobilyImplicitLoginFilter > Default MSISDN: " + pumaUtil.getDefaultMsisdn());
				if (pumaUtil.getRoleId() == RoleIdConstant.ROLE_ID_CONSUMER_CUSTOMER || pumaUtil.getRoleId() == RoleIdConstant.ROLE_ID_CALL_CENTER
						|| pumaUtil.getRoleId() == RoleIdConstant.ROLE_ID_NEQATY_PARTNER)
					CacheManagerServiceUtil.getCachedData(request.getSession().getId(), pumaUtil.getUid());
				try
				{
					if (pumaUtil.getRoleId() == RoleIdConstant.ROLE_ID_CORPORATE_CUSTOMER)
					{
						Long userAcctID = UserAccountDAO.getInstance().findByUserName(pumaUtil.getUid().toUpperCase()).get(0).getUserAcctId();
						UserAccountDAO.getInstance().updateLastLoginTime(userAcctID);
					}
					else
						UserAccountDAO.getInstance().updateLastLoginTime(Long.parseLong(pumaUtil.getUserAcctReference()));
				}
				catch (Exception e)
				{
					logger.log(Level.SEVERE, "MobilyImplicitLoginFilter > Could not update last time of user=" + pumaUtil.getUid() + " Exception =" + e.getMessage());
				}
			}

			try
			{
				if (pumaUtil.getRoleId() == RoleIdConstant.ROLE_ID_CORPORATE_CUSTOMER)  //PBI25528 uncommented this block
				{
					UserAuthenticationServiceRemote service = (UserAuthenticationServiceRemote) BeanLocator.remoteLookup(UserAuthenticationServiceRemote.class.getName());
					UserAuthenticationInputVO userAuthenticationInputV = new UserAuthenticationInputVO();
					userAuthenticationInputV.setUsername(pumaUtil.getUid());
					service.handleUserAuthentication(userAuthenticationInputV);
				}
				else if (pumaUtil.getRoleId() == RoleIdConstant.ROLE_ID_CONSUMER_CUSTOMER)
				{
					UserAuthenticationServiceRemote service = (UserAuthenticationServiceRemote) BeanLocator.remoteLookup(UserAuthenticationServiceRemote.class.getName());
					service.syncConsumerProfile(pumaUtil.getUid());
					
					logger.log(Level.INFO, "MobilyImplicitLoginFilter > Sync consumer profile completed for user=" + pumaUtil.getUid());
				}
			}
			catch (Exception e)
			{
				// just log it
				logger.log(Level.SEVERE, "MobilyImplicitLoginFilter > Exception in corporate user handleUserAuthentication : " + e.getMessage());
			}
			chain.login(request, response, filterChainContext, realm);
		}

		catch (Exception e)
		{
			logger.log(Level.SEVERE, "MobilyImplicitLoginFilter > login > Exception::" + e.getMessage());
		}

	}

	@Override
	public void destroy()
	{
	}
}
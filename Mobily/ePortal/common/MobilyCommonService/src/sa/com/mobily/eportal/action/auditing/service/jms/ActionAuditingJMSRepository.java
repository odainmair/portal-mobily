/**
 * 
 */
package sa.com.mobily.eportal.action.auditing.service.jms;

import java.util.logging.Level;

import sa.com.mobily.eportal.action.audit.vo.ActionAuditingRequestVO;
import sa.com.mobily.eportal.action.auditing.service.xml.ActionAuditingXMLHandler;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

/**
 * @author m.hassan.dar
 * 
 */
public class ActionAuditingJMSRepository
{
	private static ActionAuditingJMSRepository instance = new ActionAuditingJMSRepository();

	private static final String className = ActionAuditingJMSRepository.class.getName();

	static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext("action-auditing/log");

	protected ActionAuditingJMSRepository()
	{
	}

	public static synchronized ActionAuditingJMSRepository getInstance()
	{
		if (instance == null)
		{
			instance = new ActionAuditingJMSRepository();
		}
		return instance;
	}

	public ActionAuditingRequestVO populateActionAuditing(String xmlRequest)
	{
		
		executionContext.log(Level.INFO, "ActionAuditingJMSRepository > populateActionAuditing -> xmlRequest -> " + xmlRequest, className, "populateActionAuditing");
		
		ActionAuditingXMLHandler xmlHandler = new ActionAuditingXMLHandler();
		return xmlHandler.parseActionAuditingRequestXML(xmlRequest);
	}

	public void auditAction(ActionAuditingRequestVO requestVO)
	{

		String requestXML = "";
		ActionAuditingXMLHandler xmlHandler = new ActionAuditingXMLHandler();

		requestXML = xmlHandler.generateActionAuditingRequestXML(requestVO);

		
		executionContext.log(Level.INFO, "ActionAuditingJMSRepository > populateActionAuditing -> requestXML -> " + requestXML, className, "auditAction");
		
		if (!FormatterUtility.isEmpty(requestXML))
		{
			MQConnectionHandler.getInstance().sendToMQ(requestXML, "EPORTAL.ACTIONAUDITING.REQUEST");
		}

	}

	public static void main(String[] args)
	{
		ActionAuditingRequestVO requestVO = new ActionAuditingRequestVO();
		requestVO.setActionId(new Integer("1"));
		requestVO.setErrorCode("0000");
		requestVO.setStatus(new Integer("1"));

		requestVO.setSan("san");
		requestVO.setUsername("username");

		try
		{
			// System.out.println(generateActionAuditingRequestXML(requestVO));

			ActionAuditingXMLHandler xmlHandler = new ActionAuditingXMLHandler();
			System.out.println(xmlHandler.generateActionAuditingRequestXML(requestVO));
			xmlHandler
					.parseActionAuditingRequestXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?><request><action-id>1</action-id><san>san</san><username>username</username><status>1</status><error-code>0000</error-code></request>");

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}

package sa.com.mobily.eportal.billing.constants;

public interface BalanceInquiryConstants {

	public static final String INVALID_REQUEST ="20131";
	public static final String INVALID_REPLY ="20132";
	
			public static final String MSG_FORMAT_PAYMENT = "Payment_INQ";
			 public static final String MESSAGE_VERSION_VALUE = "0000";
			 public static final String CHANNEL_ID_BSL_VALUE = "BSL";
			 public static final String BALANCE_CHANNEL_FUNC_VALUE = "INQ";
			 public static final String LANG_VALUE = "E";
			 public static final String SECURITY_INFO_VALUE = "secure";
			 public static final String RETURN_CODE_VALUE = "0000";
			 
			
			    public static final String ERROR_NO_RECORDS = "9997";
			    
			    
			    //	Corporate and normal lines types
			 	public static final int LINE_TYPE_CONSUMER = 11;
				public static final int LINE_TYPE_CORPORATE_LINE = 12;
				public static final int LINE_TYPE_CORPORATE_AUTHORIZED_PERSON = 13;
				public static final int LINE_TYPE_CORPORATE_AUTHORIZED_PERSON_CHANGE_IDENTITY = 14;

				
			//  Balance Inquiry
				public static final String BALANCE_INQ_REQUEST_QUEUENAME = "mq.balance.request.queue";
				public static final String BALANCE_INQ_WIMAX_REQUEST_QUEUENAME = "mq.balance.wimax.request.queue";
				public static final String BALANCE_INQ_REPLY_QUEUENAME = "mq.balance.reply.queue";
				
				//Free balance Inquiry
				public static final String FREE_BALANCE_INQ_REQUEST_QUEUENAME = "mq.free.balance.request.queue";
				public static final String FREE_BALANCE_INQ_REPLY_QUEUENAME = "mq.free.balance.reply.queue";
				
				
				public static final String FTTH_RENEWAL_PAYMENT_REQUEST_QUEUENAME = "mq.ftth.renewal.payment.request.queue";
				public static final String FTTH_RENEWAL_PAYMENT_REPLY_QUEUENAME = "mq.ftth.renewal.payment.reply.queue";
				
						
				public static final String ERROR_CODE_EMPTY_REPLY_XML = "9999";	
				
				public static final int SUBS_TYPE_GSM = 1;
				public static final int SUBS_TYPE_3G = 2;
				public static final int SUBS_TYPE_LTE = 3;
				public static final int SUBS_TYPE_WiMAX = 4;
				public static final int SUBS_TYPE_FTTH = 5;
				public static final int SUBS_TYPE_IPTV = 6;
}

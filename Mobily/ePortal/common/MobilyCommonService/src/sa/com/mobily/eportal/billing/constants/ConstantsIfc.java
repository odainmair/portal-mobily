
package sa.com.mobily.eportal.billing.constants;


public interface ConstantsIfc {

	public static final String DATE_FORMAT = "yyyyMMddHHmmss";
	
	public static final int FUNC_ID_CUSTOMER_PRODUCT_INFORMATION = 40200200;
	public static final String CALLER_SERVICE_SUBMIT_PRODUCT_ORDER = "SubmitProductOrder";
	public static final int REQ_MODE_0 = 0;
	public static final String INVALID_REQUEST ="20131";
	public static final String INVALID_REPLY ="20132";
	
	public static final String REQUEST_QUEUE_CUSTOMER_PROFILE = "customer.profile.request.queue";
	public static final String REPLY_QUEUE_CUSTOMER_PROFILE = "customer.profile.reply.queue";
	
	public static final String REQUEST_QUEUE_CONSUMER_PROFILE = "consumer.profile.request.queue";
	public static final String REPLY_QUEUE_CONSUMER_PROFILE = "consumer.profile.reply.queue";
	
	public static final String REQUEST_QUEUE_RELATED_NUMBERS = "related.numbers.request.queue";
	public static final String REPLY_QUEUE_RELATED_NUMBERS = "related.numbers.reply.queue";
	
	public static final String REQUEST_QUEUE_ACCOUNT_POID = "account.poid.request.queue";
	public static final String REPLY_QUEUE_ACCOUNT_POID = "account.poid.reply.queue";
	
	public static final String REQUEST_QUEUE_FAV_NO_ELIGIBILITY = "fav.number.eligibility.request.queue";
	public static final String REPLY_QUEUE_FAV_NO_ELIGIBILITY = "fav.number.eligibility.reply.queue";
	
	public static final String REQUEST_QUEUE_CORPORATE_PROFILE = "corporate.profile.request.queue";
	public static final String REPLY_QUEUE_CORPORATE_PROFILE = "corporate.profile.reply.queue";
	
	public static final String TEMP_DIR = "temp.dir";
	
	public static final String ERROR_CODE_EMPTY_REPLY_XML = "9999";
	
	public static final String STATUS_CODE_CUSTOMER_NOT_FOUND = "E500004";
	public static final String STATUS_CODE_CUSTOMER_NOT_SUBSCRIBED = "E020061";
	
	public static final String FUN_GET_CONSUMER_LINE_INFO = "CustandLineInfoInq";
	public static final String FUN_GET_RELATED_NUMBERS_INQ = "RelatedAccountsInq";
	public static final String FUN_GET_ACCOUNT_POID_INQ = "AccountPOIDInq";
	/*public static final String FUN_GET_LAST_BILLS_INQ = "LastBillsInq";*/
	
	public static final String KEY_TYPE_SA = "SA"; // For Service Account Number
	public static final String KEY_TYPE_MSISDN = "MSISDN"; // For Msisdn
	public static final String KEY_TYPE_CONSUMER_CPE = "CPE"; //For CPE number
	
	public static final String EPORTAL_USER = "ePortalUser";
	public static final String STATUS_CODE_SUCCESS = "I000000";
	
	public static final String FAV_NUMBER_MSG_TYPE = "REQUEST";
	public static final String FAV_NUMBER_FUNC_ID = "44400000";
	public static final String LANG_E = "E";
	public static final String FAV_NUMBER_SC_ID = "Broker";
	public static final String FAV_NUMBER_CUST_ID = "EAI";
	public static final String ACTION_ADD = "Add";
	
	public static final String CORP_PROFILE_FUNC_ID = "CorporateProfileInquiry";
	public static final String CORP_PROFILE_MSG_VERSION = "0002";
	
	public static final String KEY_TYPE_BA = "BA"; // For billing account number
	
}

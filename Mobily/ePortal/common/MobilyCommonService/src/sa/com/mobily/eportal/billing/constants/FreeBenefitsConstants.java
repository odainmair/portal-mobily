package sa.com.mobily.eportal.billing.constants;

public interface FreeBenefitsConstants
{

	public static final String INVALID_REQUEST = "20131";

	public static final String INVALID_REPLY = "20132";

	// Free balance Inquiry
	public static final String FREE_BALANCE_INQ_REQUEST_QUEUENAME = "mq.free.balance.request.queue";

	public static final String FREE_BALANCE_INQ_REPLY_QUEUENAME = "mq.free.balance.reply.queue";

	public static final String ERROR_CODE_EMPTY_REPLY_XML = "9999";
}

package sa.com.mobily.eportal.billing.constants;

public interface TagIfc {
	public static final String TAG_SR_HEADER = "SR_HEADER";
	public static final String TAG_MOBILY_EPORTAL_SR="MOBILY_EPORTAL_SR";
	public static final String TAG_SR_DATE = "SrDate";
	public static final String TAG_FUNC_ID = "FuncId";
	public static final String TAG_SECURITY_KEY = "SecurityKey";
	public static final String TAG_MSG_VERSION = "MsgVersion";
	public static final String TAG_REQUESTOR_CHANNEL_ID = "RequestorChannelId";
	public static final String TAG_REQUESTOR_USER_ID = "RequestorUserId";
	public static final String TAG_REQUESTOR_LANGUAGE = "RequestorLanguage";
	public static final String LINE_NUMBER = "LineNumber";
	public static final String TAG_BILLING_NUMBER = "BillingNumber";
	public static final String TAG_CUST_TYPE = "Customer_Type";
	public static final String CUSTOMER_TYPE = "CustomerType";
	public static final String TAG_MOBILY_EPORTAL_SR_REPLY ="MOBILY_EPORTAL_SR_REPLY";
	public static final String RETURN_CODE = "ReturnCode";
	public static final String TAG_START_IPHONE_REP = "MOBILY_IPHONE_REPLY";
	public static final String TAG_FUNCTIONID = "FunctionId";
	public static final String TAG_HASH_CODE = "HashCode";
	public static final String TAG_ERROR = "ERROR";
	public static final String TAG_ERROR_CODE = "ErrorCode";

	//ePortal
	public static final String TAG_CHARACTERS = "Characters";
	
	//Customer profile
	public static final String TAG_RETRIEVE_CUSTOMER_PRODUCT_CONFIGURATION = "retrieveCustomerProductConfiguration";
	public static final String TAG_MSGRQ_HDR = "MsgRqHdr";
	public static final String TAG_RQUID = "RqUID";
	public static final String TAG_CLIENT_DT = "ClientDt";
	public static final String TAG_CALLER_SERVICE = "CallerService";
	public static final String TAG_BODY = "Body";
	
	public static final String TAG_SOURCE_CRITERIA = "sourceCriteria";
	public static final String TAG_ID = "Id";
	public static final String TAG_SPECIFICATION = "Specification";
	public static final String TAG_NAME = "Name";
	public static final String TAG_REQ_MODE = "RqMode";
	public static final String TAG_LANG_PREF = "LangPref";
	public static final String TAG_PACKAGE_ID = "PackageId";
	public static final String TAG_PRODUCT_ORDER_ITEM = "ProductOrderItem";
	public static final String TAG_PRODUCT = "Product";

}

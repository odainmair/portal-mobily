package sa.com.mobily.eportal.billing.jaxb.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "operation", "msisdn", "MSISDN",
		"voucherNumber" })
@XmlRootElement(name = "EE_EAI_MESSAGE")
public class BillPaymentRequest {

	@XmlElement(name = "EE_EAI_HEADER", required = true)
	protected MessageRequestHeader header;

	@XmlElement(name = "Operation")
	protected String operation;

	@XmlElement(name = "MSISDN")
	protected String msisdn;

	@XmlElement(name = "VoucherNumber")
	protected String voucherNumber;

	public MessageRequestHeader getHeader() {
		return header;
	}

	public void setHeader(MessageRequestHeader header) {
		this.header = header;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

}

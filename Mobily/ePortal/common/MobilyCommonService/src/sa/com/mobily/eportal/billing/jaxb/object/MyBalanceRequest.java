package sa.com.mobily.eportal.billing.jaxb.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "msgRqHdr", "accountNumber",
		"lineNumber", "chargeable", "chargeAmount", "inValue", "isClosed",
		"includeAdditionalRental" })
@XmlRootElement(name = "EE_EAI_MESSAGE")
public class MyBalanceRequest {

	@XmlElement(name = "EE_EAI_HEADER", required = true)
	protected MessageRequestHeader msgRqHdr;

	@XmlElement(name = "AccountNumber")
	protected String accountNumber;

	@XmlElement(name = "LineNumber")
	protected String lineNumber;

	@XmlElement(name = "Chargeable")
	protected String chargeable;

	@XmlElement(name = "ChargeAmount")
	protected String chargeAmount;

	@XmlElement(name = "GetINValue")
	protected String inValue;

	@XmlElement(name = "IsClosed")
	protected String isClosed;

	@XmlElement(name = "IncludeAdditionalRental")
	protected String includeAdditionalRental;

	public MessageRequestHeader getMsgRqHdr() {
		return msgRqHdr;
	}

	public void setMsgRqHdr(MessageRequestHeader value) {
		this.msgRqHdr = value;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getChargeable() {
		return chargeable;
	}

	public void setChargeable(String chargeable) {
		this.chargeable = chargeable;
	}

	public String getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(String chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public String getInValue() {
		return inValue;
	}

	public void setInValue(String inValue) {
		this.inValue = inValue;
	}

	public String getIsClosed() {
		return isClosed;
	}

	public void setIsClosed(String isClosed) {
		this.isClosed = isClosed;
	}

	public String getIncludeAdditionalRental() {
		return includeAdditionalRental;
	}

	public void setIncludeAdditionalRental(String includeAdditionalRental) {
		this.includeAdditionalRental = includeAdditionalRental;
	}
	
	
	

}

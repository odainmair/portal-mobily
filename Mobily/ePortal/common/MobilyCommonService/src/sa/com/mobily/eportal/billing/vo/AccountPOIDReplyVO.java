package sa.com.mobily.eportal.billing.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import sa.com.mobily.eportal.common.service.valueobject.common.ConsumerReplyHeaderVO;



/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}SR_HEADER_REPLY"/>
 *         &lt;element ref="{}AccountPOID"/>
 *         &lt;element ref="{}ParentPOID"/>
 *         &lt;element ref="{}BillableAccountPOID"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headerVO",
    "accountPOID",
    "parentPOID",
    "billableAccountPOID"
})
@XmlRootElement(name = "MOBILY_BSL_SR_REPLY")
public class AccountPOIDReplyVO {

    @XmlElement(name = "SR_HEADER_REPLY", required = true)
    protected ConsumerReplyHeaderVO headerVO;
    @XmlElement(name = "AccountPOID", required = true)
    protected String accountPOID;
    @XmlElement(name = "ParentPOID", required = true)
    protected String parentPOID;
    @XmlElement(name = "BillableAccountPOID", required = true)
    protected String billableAccountPOID;

    public ConsumerReplyHeaderVO getHeaderVO() {
		return headerVO;
	}

	public void setHeaderVO(ConsumerReplyHeaderVO headerVO) {
		this.headerVO = headerVO;
	}

	/**
     * Gets the value of the accountPOID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountPOID() {
        return accountPOID;
    }

    /**
     * Sets the value of the accountPOID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountPOID(String value) {
        this.accountPOID = value;
    }

    /**
     * Gets the value of the parentPOID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentPOID() {
        return parentPOID;
    }

    /**
     * Sets the value of the parentPOID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentPOID(String value) {
        this.parentPOID = value;
    }

    /**
     * Gets the value of the billableAccountPOID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillableAccountPOID() {
        return billableAccountPOID;
    }

    /**
     * Sets the value of the billableAccountPOID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillableAccountPOID(String value) {
        this.billableAccountPOID = value;
    }

}

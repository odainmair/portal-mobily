package sa.com.mobily.eportal.billing.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import sa.com.mobily.eportal.common.service.valueobject.common.ConsumerRequestHeaderVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}SR_HEADER"/>
 *         &lt;element ref="{}MSISDN"/>
 *         &lt;element ref="{}ServiceAccountNumber"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headerVO",
    "msisdn",
    "serviceAccountNumber"
})
@XmlRootElement(name = "MOBILY_BSL_SR")
public class AccountPOIDRequestVO {

    @XmlElement(name = "SR_HEADER", required = true)
    protected ConsumerRequestHeaderVO headerVO;
    @XmlElement(name = "MSISDN", required = true)
    protected String msisdn;
    @XmlElement(name = "ServiceAccountNumber", required = true)
    protected String serviceAccountNumber;

    
    public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

    public ConsumerRequestHeaderVO getHeaderVO() {
		return headerVO;
	}

	public void setHeaderVO(ConsumerRequestHeaderVO headerVO) {
		this.headerVO = headerVO;
	}

	/**
     * Gets the value of the serviceAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceAccountNumber() {
        return serviceAccountNumber;
    }

    /**
     * Sets the value of the serviceAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceAccountNumber(String value) {
        this.serviceAccountNumber = value;
    }

}

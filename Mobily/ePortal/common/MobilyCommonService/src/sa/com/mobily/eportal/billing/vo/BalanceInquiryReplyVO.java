/*
 * Created on Jan 25, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.billing.vo;

import java.util.Date;


import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.BaseVO;


/**
 * @author msayed
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class BalanceInquiryReplyVO extends BaseVO 
{
	private static final long serialVersionUID = 1L;
	private MessageHeaderVO header = new MessageHeaderVO();

	public MessageHeaderVO getHeader() {
		return header;
	}

	public void setHeader(MessageHeaderVO header) {
		this.header = header;
	}

	private String lineNumber;
	private int customerType;

	private String packageID;

	private double balance;

	private Date expirationDate;

	private double billedAmount;

	private double unbilledAmount;

	private double unalocatedAmount;

	private int inValue;

	// for Wimax
	private String accountNumber;
	
	//Roaming & Bill Revamp
	private double outstandingAmount;
	private double amountDue;
	private String packageName;
	private double monthlySubscriptionFee;
	private String lastBillDate;
	private String lastUpdateTime;
	private double additionalRentalFees;
	private double renewalAmount;
	private Date ftthRenewexpDate;
	private String tempFtthRenewexpDate;
	private String errorCode;
	private String errorMessage;
	private String ncraBalance;
	
	
	
	
	public double getAdditionalRentalFees() {
		return additionalRentalFees;
	}

	public void setAdditionalRentalFees(double additionalRentalFees) {
		this.additionalRentalFees = additionalRentalFees;
	}

	public String getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public double getOutstandingAmount() {
		return outstandingAmount;
	}

	public void setOutstandingAmount(double outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}

	public double getAmountDue() {
		return amountDue;
	}

	public void setAmountDue(double amountDue) {
		this.amountDue = amountDue;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public double getMonthlySubscriptionFee() {
		return monthlySubscriptionFee;
	}

	public void setMonthlySubscriptionFee(double monthlySubscriptionFee) {
		this.monthlySubscriptionFee = monthlySubscriptionFee;
	}

	public String getLastBillDate() {
		return lastBillDate;
	}

	public void setLastBillDate(String lastBillDate) {
		this.lastBillDate = lastBillDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	/**
	 * @return true if the customer is prepaid else return false
	 */
	public boolean isPrepaidCustomer() {
		if (customerType == 1)
			return true;
		return false;
	}

	/**
	 * @return Returns the billedAmount.
	 */
	public double getBilledAmount() {
		return billedAmount;
	}

	/**
	 * @param billedAmount
	 *            The billedAmount to set.
	 */
	public void setBilledAmount(double billedAmount) {
		this.billedAmount = billedAmount;
	}

	/**
	 * @return Returns the customerType.
	 */
	public int getCustomerType() {
		return customerType;
	}

	/**
	 * @param customerType
	 *            The customerType to set.
	 */
	public void setCustomerType(int customerType) {
		this.customerType = customerType;
	}

	/**
	 * @return Returns the expirationDate.
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * @param expirationDate
	 *            The expirationDate to set.
	 */
	public void setExpirationDate(String expirationDate) {
		if(expirationDate != null && !expirationDate.equals(""))
			this.expirationDate = MobilyUtility.FormateStringToDate(expirationDate);
		else
			this.expirationDate = null;
		
	}

	/**
	 * @return Returns the lineNumber.
	 */
	public String getLineNumber() {
		return lineNumber;
	}

	/**
	 * @param lineNumber
	 *            The lineNumber to set.
	 */
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}

	/**
	 * @return Returns the packageID.
	 */
	public String getPackageID() {
		return packageID;
	}

	/**
	 * @param packageID
	 *            The packageID to set.
	 */
	public void setPackageID(String packageID) {
		this.packageID = packageID;
	}

	/**
	 * @return Returns the unalocatedAmount.
	 */
	public double getUnalocatedAmount() {
		return unalocatedAmount;
	}

	/**
	 * @param unalocatedAmount
	 *            The unalocatedAmount to set.
	 */
	public void setUnalocatedAmount(double unalocatedAmount) {
		this.unalocatedAmount = unalocatedAmount;
	}

	/**
	 * @return Returns the unbilledAmount.
	 */
	public double getUnbilledAmount() {
		return unbilledAmount;
	}

	/**
	 * @param unbilledAmount
	 *            The unbilledAmount to set.
	 */
	public void setUnbilledAmount(double unbilledAmount) {
		this.unbilledAmount = unbilledAmount;
	}

	/**
	 * @return Returns the header.
	 */
	/*public MessageHeaderVO getHeader() {
		return header;
	}

	/**
	 * @param header
	 *            The header to set.
	 */
	/*public void setHeader(MessageHeaderVO header) {
		this.header = header;
	}

	/**
	 * @return Returns the inValue.
	 */
	public int getInValue() {
		return inValue;
	}

	/**
	 * @param inValue
	 *            The inValue to set.
	 */
	public void setInValue(int inValue) {
		this.inValue = inValue;
	}

	/**
	 * @return Returns the balance.
	 */
	public double getBalance() {
		return balance;
	}

	/**
	 * @param balance
	 *            The balance to set.
	 */
	public void setBalance(double balance) {
		this.balance = balance;
	}
	/**
	 * @return Returns the accountNumber.
	 */
	public String getAccountNumber() {
		return accountNumber;
	}
	/**
	 * @param accountNumber The accountNumber to set.
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the renewalAmount
	 */
	public double getRenewalAmount() {
		return renewalAmount;
	}

	/**
	 * @param renewalAmount the renewalAmount to set
	 */
	public void setRenewalAmount(double renewalAmount) {
		this.renewalAmount = renewalAmount;
	}
	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the ftthRenewexpDate
	 */
	public Date getFtthRenewexpDate() {
		return ftthRenewexpDate;
	}

	/**
	 * @param ftthRenewexpDate the ftthRenewexpDate to set
	 */
	public void setFtthRenewexpDate(String ftthRenewexpDate) {
		this.ftthRenewexpDate = MobilyUtility.FormateStringToDate(ftthRenewexpDate);
	}

	/**
	 * @return the tempFtthRenewexpDate
	 */
	public String getTempFtthRenewexpDate() {
		return tempFtthRenewexpDate;
	}

	/**
	 * @param tempFtthRenewexpDate the tempFtthRenewexpDate to set
	 */
	public void setTempFtthRenewexpDate(String tempFtthRenewexpDate) {
		this.tempFtthRenewexpDate = tempFtthRenewexpDate;
	}
	/**
	 * @return the ncraBalance
	 */
	public String getNcraBalance()
	{
		return ncraBalance;
	}

	/**
	 * @param ncraBalance the ncraBalance to set
	 */
	public void setNcraBalance(String ncraBalance)
	{
		this.ncraBalance = ncraBalance;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "BalanceInquiryReplyVO [header=" + header + ", lineNumber="
				+ lineNumber + ", customerType=" + customerType 
				+ ", packageID=" + packageID + ", balance=" + balance
				+ ", expirationDate=" + expirationDate + ", billedAmount="
				+ billedAmount + ", unbilledAmount=" + unbilledAmount 
				+ ", unalocatedAmount=" + unalocatedAmount
				+ ", inValue=" + inValue + ", accountNumber=" + accountNumber 
				+ ", outstandingAmount=" + outstandingAmount + ", amountDue=" 
				+ amountDue + ", packageName=" + packageName 
				+ ", monthlySubscriptionFee=" + monthlySubscriptionFee + ", lastBillDate=" + lastBillDate 
				+ ", lastUpdateTime=" + lastUpdateTime
				+ ", additionalRentalFees=" + additionalRentalFees + ", renewalAmount=" + renewalAmount 
				+ ", ftthRenewexpDate=" + ftthRenewexpDate + ", tempFtthRenewexpDate="
				+ tempFtthRenewexpDate + ", errorCode=" + errorCode + ", errorMessage=" + errorMessage 
				+ ", ncraBalance=" + ncraBalance + "]";
	}
	
	
	
	
	
}
/*
 * Created on Nov 19, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.billing.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class BalanceInquiryRequestVO extends BaseVO 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MessageHeaderVO header  = new MessageHeaderVO();
	private String userID;
	private String lineNumber;
	private int lineType = 0;
	private int customerType = 0;
	private String locale;
	private boolean ftthRenewInfoRequired = false;


	/**
	 * @return the customerType
	 */
	public int getCustomerType()
	{
		return customerType;
	}
	/**
	 * @param customerType the customerType to set
	 */
	public void setCustomerType(int customerType)
	{
		this.customerType = customerType;
	}
	/**
	 * @return the locale
	 */
	public String getLocale()
	{
		return locale;
	}
	/**
	 * @param locale the locale to set
	 */
	public void setLocale(String locale)
	{
		this.locale = locale;
	}
	/**
	 * @return Returns the lineType.
	 */
	public int getLineType() {
		return lineType;
	}
	/**
	 * @param lineType The lineType to set.
	 */
	public void setLineType(int lineType) {
		this.lineType = lineType;
	}
	/**
	 * @return Returns the header.
	 */
	/*public MessageHeaderVO getHeader() {
		return header;
	}
	/**
	 * @param header The header to set.
	 */
	/*public void setHeader(MessageHeaderVO header) {
		this.header = header;
	}
	/**
	 * @return Returns the lineNumber.
	 */
	public String getLineNumber() {
		return lineNumber;
	}
	/**
	 * @param lineNumber The lineNumber to set.
	 */
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}
	/**
	 * @return Returns the userID.
	 */
	public String getUserID() {
		return userID;
	}
	/**
	 * @param userID The userID to set.
	 */
	public void setUserID(String userID) {
		this.userID = userID;
	}
	/**
	 * @return the ftthRenewInfoRequired
	 */
	public boolean isFtthRenewInfoRequired() {
		return ftthRenewInfoRequired;
	}
	/**
	 * @param ftthRenewInfoRequired the ftthRenewInfoRequired to set
	 */
	public void setFtthRenewInfoRequired(boolean ftthRenewInfoRequired) {
		this.ftthRenewInfoRequired = ftthRenewInfoRequired;
	}
	public MessageHeaderVO getHeader() {
		return header;
	}
	public void setHeader(MessageHeaderVO header) {
		this.header = header;
	}
	
	
	
}

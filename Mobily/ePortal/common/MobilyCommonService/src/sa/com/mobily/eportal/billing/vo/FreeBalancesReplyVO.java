package sa.com.mobily.eportal.billing.vo;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class FreeBalancesReplyVO extends BaseVO {
	
	
	private int freeMinutes = 0;
	private int freeSMS = 0;
	private int freeMMS = 0;
	private String freeGPRS = "";
	private int freeOnNetMinutes = 0;
	private int freeOnNetSMS = 0;
	private int freeOnNetMMS = 0;
	private int freeVoiceSMS = 0;
	private int nationalMinutes = 0;
	private int rgMinutes = 0;
	private String memberMsisdn = "";
	private String memberName = "";
	private int freeWeekendMinutes = 0;
	private int freeWeekendGPRS = 0;
	private int freeCrossNetSMS = 0;
	
	private int fixedOnNet=0; //SR22516 - ePortal Fixed Voice 
	

	private int fixedCrossNet=0;//SR22516 - ePortal Fixed Voice

	//Added from Mobily Common Service App
	private int RGMinutes = 0;
	private String packageType = "";
	private String consumedGPRS = "0";
	private String freeGprsWithoutPromo = "0";
	
	private String freePizzaGPRS = "0";
	private String freePizzaGPRSExpiry = "";
	private int freePizzaSMS;
	private String freePizzaSMSExpiry = "";
	private int freePizzaMO;
	private String freePizzaMOExpiry = "";
	private int freePizzaMT;
	private String freePizzaMTExpiry = "";
	
	private String errorCode = "";
	private String returnCode = "";
	
	private String freeRoamingGPRS = "";
	private int freeRoamingMinutes = 0;
	private int freeRoamingMTMinutes = 0;
	
	public int getFreeCrossNetSMS() {
		return freeCrossNetSMS;
	}

	public void setFreeCrossNetSMS(int freeCrossNetSMS)	{
		if (freeCrossNetSMS < 0)
		{
			freeCrossNetSMS = 0;
		}
		this.freeCrossNetSMS = freeCrossNetSMS;
	}

	public String getMemberMsisdn() {
		return memberMsisdn;
	}

	public void setMemberMsisdn(String memberMsisdn)
	{
		this.memberMsisdn = memberMsisdn;
	}

	public String getMemberName()
	{
		return memberName;
	}

	public void setMemberName(String memberName)
	{
		this.memberName = memberName;
	}

	/**
	 * @return Returns the freeGPRS.
	 */
	public String getFreeGPRS()
	{
		return freeGPRS;
	}

	/**
	 * @param freeGPRS
	 *            The freeGPRS to set.
	 */
	public void setFreeGPRS(String freeGPRS)
	{
		try{
			if (freeGPRS != null && !ConstantIfc.VIEW_BALANCE_GPRS_UNLIMITED.equalsIgnoreCase(freeGPRS) &&  Float.parseFloat(freeGPRS) < 0)
			{
				freeGPRS = ConstantIfc.VIEW_BALANCE_GPRS_UNLIMITED;
			}
		}catch(NumberFormatException ne){
			
		}

		this.freeGPRS = freeGPRS;
	}

	/**
	 * @return Returns the freeMinutes.
	 */
	public int getFreeMinutes()
	{
		return freeMinutes;
	}

	/**
	 * @param freeMinutes
	 *            The freeMinutes to set.
	 */
	public void setFreeMinutes(int freeMinutes)
	{
		if (freeMinutes < 0)
		{
			freeMinutes = 0;
		}

		this.freeMinutes = freeMinutes;
	}

	/**
	 * @return Returns the freeMMS.
	 */
	public int getFreeMMS()
	{
		return freeMMS;
	}

	/**
	 * @param freeMMS
	 *            The freeMMS to set.
	 */
	public void setFreeMMS(int freeMMS)
	{
		if (freeMMS < 0)
		{
			freeMMS = 0;
		}

		this.freeMMS = freeMMS;
	}

	/**
	 * @return Returns the freeOnNetMinutes.
	 */
	public int getFreeOnNetMinutes()
	{
		return freeOnNetMinutes;
	}

	/**
	 * @param freeOnNetMinutes
	 *            The freeOnNetMinutes to set.
	 */
	public void setFreeOnNetMinutes(int freeOnNetMinutes)
	{
		if (freeOnNetMinutes < 0)
		{
			freeOnNetMinutes = 0;
		}

		this.freeOnNetMinutes = freeOnNetMinutes;
	}

	/**
	 * @return Returns the freeOnNetMMS.
	 */
	public int getFreeOnNetMMS()
	{
		return freeOnNetMMS;
	}

	/**
	 * @param freeOnNetMMS
	 *            The freeOnNetMMS to set.
	 */
	public void setFreeOnNetMMS(int freeOnNetMMS)
	{
		if (freeOnNetMMS < 0)
		{
			freeOnNetMMS = 0;
		}

		this.freeOnNetMMS = freeOnNetMMS;
	}

	/**
	 * @return Returns the freeOnNetSMS.
	 */
	public int getFreeOnNetSMS()
	{
		return freeOnNetSMS;
	}

	/**
	 * @param freeOnNetSMS
	 *            The freeOnNetSMS to set.
	 */
	public void setFreeOnNetSMS(int freeOnNetSMS)
	{
		if (freeOnNetSMS < 0)
		{
			freeOnNetSMS = 0;
		}
		this.freeOnNetSMS = freeOnNetSMS;
	}

	/**
	 * @return Returns the freeSMS.
	 */
	public int getFreeSMS()
	{
		return freeSMS;
	}

	/**
	 * @param freeSMS
	 *            The freeSMS to set.
	 */
	public void setFreeSMS(int freeSMS)
	{
		if (freeSMS < 0)
		{
			freeSMS = 0;
		}

		this.freeSMS = freeSMS;
	}

	public int getFreeWeekendMinutes()
	{
		return freeWeekendMinutes;
	}

	public void setFreeWeekendMinutes(int freeWeekendMinutes)
	{
		if (freeWeekendMinutes < 0)
		{
			freeWeekendMinutes = 0;
		}
		this.freeWeekendMinutes = freeWeekendMinutes;
	}

	public int getFreeWeekendGPRS()
	{
		return freeWeekendGPRS;
	}

	public void setFreeWeekendGPRS(int freeWeekendGPRS)
	{
		if (freeWeekendGPRS < 0)
		{
			freeWeekendGPRS = 0;
		}
		this.freeWeekendGPRS = freeWeekendGPRS;
	}

	public int getFreeVoiceSMS()
	{
		return freeVoiceSMS;
	}

	public void setFreeVoiceSMS(int freeVoiceSMS)
	{
		this.freeVoiceSMS = freeVoiceSMS;
	}

	public int getNationalMinutes()
	{
		return nationalMinutes;
	}

	public void setNationalMinutes(int nationalMinutes)
	{
		this.nationalMinutes = nationalMinutes;
	}

	public int getRgMinutes()
	{
		return rgMinutes;
	}

	public void setRgMinutes(int rgMinutes)
	{
		this.rgMinutes = rgMinutes;
	}

	public int getRGMinutes()
	{
		return RGMinutes;
	}

	public void setRGMinutes(int rGMinutes)
	{
		RGMinutes = rGMinutes;
	}

	public String getConsumedGPRS()
	{
		return consumedGPRS;
	}

	public void setConsumedGPRS(String consumedGPRS)
	{
		this.consumedGPRS = consumedGPRS;
	}

	public String getPackageType()
	{
		return packageType;
	}

	public void setPackageType(String packageType)
	{
		this.packageType = packageType;
	}

	public String getFreeGprsWithoutPromo()
	{
		return freeGprsWithoutPromo;
	}

	public void setFreeGprsWithoutPromo(String freeGprsWithoutPromo)
	{
		this.freeGprsWithoutPromo = freeGprsWithoutPromo;
	}

	/**
	 * @return the freePizzaGPRS
	 */
	public String getFreePizzaGPRS()
	{
		return freePizzaGPRS;
	}

	/**
	 * @param freePizzaGPRS the freePizzaGPRS to set
	 */
	public void setFreePizzaGPRS(String freePizzaGPRS)
	{
		this.freePizzaGPRS = freePizzaGPRS;
	}

	/**
	 * @return the freePizzaGPRSExpiry
	 */
	public String getFreePizzaGPRSExpiry()
	{
		return freePizzaGPRSExpiry;
	}

	/**
	 * @param freePizzaGPRSExpiry the freePizzaGPRSExpiry to set
	 */
	public void setFreePizzaGPRSExpiry(String freePizzaGPRSExpiry)
	{
		this.freePizzaGPRSExpiry = freePizzaGPRSExpiry;
	}

	/**
	 * @return the freePizzaSMS
	 */
	public int getFreePizzaSMS()
	{
		return freePizzaSMS;
	}

	/**
	 * @param freePizzaSMS the freePizzaSMS to set
	 */
	public void setFreePizzaSMS(int freePizzaSMS)
	{
		this.freePizzaSMS = freePizzaSMS;
	}

	/**
	 * @return the freePizzaSMSExpiry
	 */
	public String getFreePizzaSMSExpiry()
	{
		return freePizzaSMSExpiry;
	}

	/**
	 * @param freePizzaSMSExpiry the freePizzaSMSExpiry to set
	 */
	public void setFreePizzaSMSExpiry(String freePizzaSMSExpiry)
	{
		this.freePizzaSMSExpiry = freePizzaSMSExpiry;
	}

	/**
	 * @return the freePizzaMO
	 */
	public int getFreePizzaMO()
	{
		return freePizzaMO;
	}

	/**
	 * @param freePizzaMO the freePizzaMO to set
	 */
	public void setFreePizzaMO(int freePizzaMO)
	{
		this.freePizzaMO = freePizzaMO;
	}

	/**
	 * @return the freePizzaMOExpiry
	 */
	public String getFreePizzaMOExpiry()
	{
		return freePizzaMOExpiry;
	}

	/**
	 * @param freePizzaMOExpiry the freePizzaMOExpiry to set
	 */
	public void setFreePizzaMOExpiry(String freePizzaMOExpiry)
	{
		this.freePizzaMOExpiry = freePizzaMOExpiry;
	}

	/**
	 * @return the freePizzaMT
	 */
	public int getFreePizzaMT()
	{
		return freePizzaMT;
	}

	/**
	 * @param freePizzaMT the freePizzaMT to set
	 */
	public void setFreePizzaMT(int freePizzaMT)
	{
		this.freePizzaMT = freePizzaMT;
	}

	/**
	 * @return the freePizzaMTExpiry
	 */
	public String getFreePizzaMTExpiry()
	{
		return freePizzaMTExpiry;
	}

	/**
	 * @param freePizzaMTExpiry the freePizzaMTExpiry to set
	 */
	public void setFreePizzaMTExpiry(String freePizzaMTExpiry)
	{
		this.freePizzaMTExpiry = freePizzaMTExpiry;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode()
	{
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode)
	{
		this.errorCode = errorCode;
	}

	/**
	 * @return the returnCode
	 */
	public String getReturnCode()
	{
		return returnCode;
	}

	/**
	 * @param returnCode the returnCode to set
	 */
	public void setReturnCode(String returnCode)
	{
		this.returnCode = returnCode;
	}

	/**
	 * @return the freeRoamingGPRS
	 */
	public String getFreeRoamingGPRS()
	{
		return freeRoamingGPRS;
	}

	/**
	 * @param freeRoamingGPRS the freeRoamingGPRS to set
	 */
	public void setFreeRoamingGPRS(String freeRoamingGPRS)
	{
		try{
			if (freeRoamingGPRS != null && !ConstantIfc.VIEW_BALANCE_GPRS_UNLIMITED.equalsIgnoreCase(freeRoamingGPRS) &&  Float.parseFloat(freeRoamingGPRS) < 0)
			{
				freeRoamingGPRS = ConstantIfc.VIEW_BALANCE_GPRS_UNLIMITED;
			}
		}catch(NumberFormatException ne){
			
		}
		this.freeRoamingGPRS = freeRoamingGPRS;
	}

	/**
	 * @return the freeRoamingMinutes
	 */
	public int getFreeRoamingMinutes()
	{
		return freeRoamingMinutes;
	}

	/**
	 * @param freeRoamingMinutes the freeRoamingMinutes to set
	 */
	public void setFreeRoamingMinutes(int freeRoamingMinutes)
	{
		if (freeRoamingMinutes < 0)
		{
			freeRoamingMinutes = 0;
		}
		this.freeRoamingMinutes = freeRoamingMinutes;
	}

	/**
	 * @return the freeRoamingMTMinutes
	 */
	public int getFreeRoamingMTMinutes()
	{
		return freeRoamingMTMinutes;
	}

	/**
	 * @param freeRoamingMTMinutes the freeRoamingMTMinutes to set
	 */
	public void setFreeRoamingMTMinutes(int freeRoamingMTMinutes)
	{
		if (freeRoamingMTMinutes < 0)
		{
			freeRoamingMTMinutes = 0;
		}

		this.freeRoamingMTMinutes = freeRoamingMTMinutes;
	}
	//SR22516 ePortal Fixed Voice - Start
	public int getFixedOnNet()
	{
		return fixedOnNet;
	}

	public void setFixedOnNet(int fixedOnNet)
	{
		if(fixedOnNet < 0){
			
			fixedOnNet = 0;
		}
		
		this.fixedOnNet = fixedOnNet;
	}

	public int getFixedCrossNet()
	{
		return fixedCrossNet;
	}

	public void setFixedCrossNet(int fixedCrossNet)
	{
		if(fixedCrossNet < 0){
			
			fixedCrossNet = 0;
		}
		this.fixedCrossNet = fixedCrossNet;
	}
	//SR22516 ePortal Fixed Voice - end
}
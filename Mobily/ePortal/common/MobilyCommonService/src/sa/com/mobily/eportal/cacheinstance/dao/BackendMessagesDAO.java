package sa.com.mobily.eportal.cacheinstance.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import sa.com.mobily.eportal.cacheinstance.valueobjects.BackendMessage;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

public class BackendMessagesDAO extends AbstractDBDAO<BackendMessage>
{

	private static BackendMessagesDAO instance = new BackendMessagesDAO();

	private static final String GET_MESSAGES = "SELECT BEM.ID, BEM.Key, MESSAGE_EN, MESSAGE_AR, BEM.DESCR from BACKEND_MESSAGES BEM";

	protected BackendMessagesDAO()
	{
		super(DataSources.DEFAULT);
	}

	/***
	 * Method for obtaining the singleton instance
	 * 
	 * @return
	 */
	public static BackendMessagesDAO getInstance()
	{
		return instance;
	}

	public List<BackendMessage> getMessages()
	{
		List<BackendMessage> messages = new ArrayList<BackendMessage>();
		messages = query(GET_MESSAGES);
		if (CollectionUtils.isNotEmpty(messages))
		{
			return messages;
		}
		return null;
	}

	@Override
	protected BackendMessage mapDTO(ResultSet rs) throws SQLException
	{
		BackendMessage message = new BackendMessage();
		message.setId(rs.getInt("ID"));
		message.setKey(rs.getString("KEY"));
		message.setDesc(rs.getString("DESCR"));
		message.setMessage_ar(rs.getString("MESSAGE_AR"));
		message.setMessage_en(rs.getString("MESSAGE_EN"));
		return message;
	}

}

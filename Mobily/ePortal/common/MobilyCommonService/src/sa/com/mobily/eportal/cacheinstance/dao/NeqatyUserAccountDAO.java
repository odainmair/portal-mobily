package sa.com.mobily.eportal.cacheinstance.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.cacheinstance.valueobjects.NeqatyUserAccountVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

public class NeqatyUserAccountDAO extends AbstractDBDAO<NeqatyUserAccountVO>{

	
    private static NeqatyUserAccountDAO instance = new NeqatyUserAccountDAO();
    
    private static final String GET_NEQATY_PARTNERS_BY_USERNAME = "SELECT UA.USER_ACCT_ID,SUBSCRIPTION_ID,USER_NAME,EMAIL,PARTNER_NAME_EN,PARTNER_NAME_AR,MSISDN,LAST_LOGIN_TIME,STATUS" +
    		" FROM USER_ACCOUNTS UA,PARTNER_SUBSCRIPTION PS WHERE UA.USER_ACCT_ID = PS.USER_ACCT_ID AND lower(USER_NAME) = ?";
    

    protected NeqatyUserAccountDAO() {
        super(DataSources.DEFAULT);
    }
    
    public static synchronized NeqatyUserAccountDAO getInstance() {
        if (instance == null) {
            instance = new NeqatyUserAccountDAO();
        }
        return instance;
    }
    

    
    public NeqatyUserAccountVO findNeqatyUserByUserName(String userName) {
    	NeqatyUserAccountVO dto = null;
    	List<NeqatyUserAccountVO> userInformationVOList = query(GET_NEQATY_PARTNERS_BY_USERNAME, userName.toLowerCase());
    	if(userInformationVOList != null && userInformationVOList.size() > 0) {
    		dto = userInformationVOList.get(0);
    	} 
        return dto;
    }
    


	@Override
	protected NeqatyUserAccountVO mapDTO(ResultSet rs) throws SQLException {
		NeqatyUserAccountVO dto = new NeqatyUserAccountVO();
		
		dto.setUserAcctId(rs.getLong("USER_ACCT_ID"));
		dto.setSubscriptionId(rs.getLong("SUBSCRIPTION_ID"));
		dto.setUserName(rs.getString("USER_NAME"));
		dto.setEmail(rs.getString("EMAIL"));
		dto.setPartnerNameEn(rs.getString("PARTNER_NAME_EN"));
		dto.setPartnerNameAr(rs.getString("PARTNER_NAME_AR"));
		dto.setMsisdn(rs.getString("MSISDN"));
		dto.setLastLoginTime(rs.getTimestamp("LAST_LOGIN_TIME"));
		dto.setStatus(rs.getString("STATUS"));
		
		return dto;
	}


}

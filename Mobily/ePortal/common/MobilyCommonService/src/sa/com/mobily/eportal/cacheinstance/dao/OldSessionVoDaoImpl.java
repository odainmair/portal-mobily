package sa.com.mobily.eportal.cacheinstance.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import sa.com.mobily.eportal.usermanagement.model.UserSession;

public class OldSessionVoDaoImpl implements OldSessionVoDao
{
	public OldSessionVoDaoImpl()
	{
	}

	@SuppressWarnings("unchecked")
	public UserSession findByUsername(String userName, EntityManager em)
	{
		final String hql = "SELECT sus from UserSession sus WHERE sus.id.username = :username";
		Query query = em.createQuery(hql);
		query.setParameter("username", userName);
		UserSession srUserSessionvoTbl = null;

		List<UserSession> results = query.getResultList();

		if (results != null && results.size() > 0)
		{
			srUserSessionvoTbl = results.get(0);
		}
		return srUserSessionvoTbl;
	}

	public void saveOrUpdate(UserSession instance, EntityManager em)
	{
		em.getTransaction().begin();
		em.persist(instance);
		em.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	public UserSession findBySessionID(String sessionID, EntityManager em)
	{
		final String hql = "SELECT sus from UserSession sus WHERE sus.id.sessionid = :sessionID";
		Query query = em.createQuery(hql);
		query.setParameter("sessionID", sessionID);
		UserSession srUserSessionvoTbl = null;

		List<UserSession> results = query.getResultList();

		if (results != null && results.size() > 0)
		{
			srUserSessionvoTbl = results.get(0);
		}
		return srUserSessionvoTbl;
	}

	@SuppressWarnings("unchecked")
	public UserSession findBySubscriptionID(String subscriptionID, EntityManager em)
	{
		final String hql = "SELECT sus from UserSession sus WHERE sus.subscriptionId = :subID";
		Query query = em.createQuery(hql);
		query.setParameter("subID", subscriptionID);
		UserSession srUserSessionvoTbl = null;

		List<UserSession> results = query.getResultList();

		if (results != null && results.size() > 0)
		{
			srUserSessionvoTbl = results.get(0);
		}
		return srUserSessionvoTbl;
	}
}
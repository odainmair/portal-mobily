package sa.com.mobily.eportal.cacheinstance.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import sa.com.mobily.eportal.usermanagement.model.UserAccount;

public class OldUserAccountDaoImpl implements OldUserAccountsDao
{
	public OldUserAccountDaoImpl()
	{
	}

	@SuppressWarnings("unchecked")
	public List<UserAccount> findByUserAccountId(String userId, EntityManager em)
	{
		final String hql = "SELECT acc from UserAccount acc WHERE acc.userAcctId = ?1 order by acc.userName ASC";
		Query query = em.createQuery(hql);
		query.setParameter(1, userId);
		final List<UserAccount> userAccounts = query.getResultList();
		return userAccounts;
	}

	@SuppressWarnings("unchecked")
	public UserAccount findByUsername(String userName, EntityManager em)
	{
		final String hql = "SELECT acc from UserAccount acc WHERE acc.userName = ?1";
		Query query = em.createQuery(hql);
		query.setParameter(1, userName);
		List<UserAccount> results = query.getResultList();

		UserAccount userAccount = null;
		if (results != null && results.size() > 0)
		{
			userAccount = results.get(0);
		}
		return userAccount;
	}
}
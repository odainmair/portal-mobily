package sa.com.mobily.eportal.cacheinstance.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.cacheinstance.valueobjects.UserSubscriptionEligibilityVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

public class UserSubscriptionEligibilityDAO extends AbstractDBDAO<UserSubscriptionEligibilityVO>{

	private static UserSubscriptionEligibilityDAO INSTANCE = new UserSubscriptionEligibilityDAO();
	
	private static final String US_FIND_USER_ELIGIBILITY_BY_SUBSCRIPTION_ID = "SELECT USE_ID,SUBSCRIPTION_ID,PROJECT_ID,STATUS,UPDATED_DATE FROM USER_SUBSCRIPTIONS_ELIGIBILITY WHERE SUBSCRIPTION_ID = ?";
	
	private static final String USER_SUBSCRIPTION_ELIGIBILITY_ADD = "INSERT INTO USER_SUBSCRIPTIONS_ELIGIBILITY "
			+ "(USE_ID,SUBSCRIPTION_ID,PROJECT_ID,STATUS,UPDATED_DATE) VALUES " + "(USER_SUB_ELIG_SEQ.NEXTVAL, ?, ?, ?, CURRENT_TIMESTAMP)";
	
	private static final String UPDATE_ELIGIBILITY_STATUS = "UPDATE USER_SUBSCRIPTIONS_ELIGIBILITY SET STATUS = ?, UPDATED_DATE = CURRENT_TIMESTAMP WHERE SUBSCRIPTION_ID = ?";
	
	private static final String DELETE_ELIGIBILITY_STATUS = "DELETE FROM USER_SUBSCRIPTIONS_ELIGIBILITY WHERE SUBSCRIPTION_ID = ?";

	
	protected UserSubscriptionEligibilityDAO(){
		super(DataSources.DEFAULT);
	}
	
	public static UserSubscriptionEligibilityDAO getInstance(){
		return INSTANCE;
	}
	
	public UserSubscriptionEligibilityVO getSubscriptionEligibilitysBySubId(long userSubscriptionId){
		List<UserSubscriptionEligibilityVO> eligibilityList = query(US_FIND_USER_ELIGIBILITY_BY_SUBSCRIPTION_ID , new Object[] { userSubscriptionId } );
		UserSubscriptionEligibilityVO dto = null;
		if(eligibilityList != null && eligibilityList.size() >  0)
			dto = eligibilityList.get(0);
		
		return dto;
	}
	
	public int addUserSubscriptionEligibility(UserSubscriptionEligibilityVO dto)
	{
		return update(USER_SUBSCRIPTION_ELIGIBILITY_ADD, dto.getSubscriptionId(),dto.getProjectId(), dto.getStatus());
	}
	
	public int updateEligibilityStatus(long subscriptionId, String status)
	{
		return update(UPDATE_ELIGIBILITY_STATUS, status, subscriptionId);
	}
	
	public int deleteEligibilityStatus(long subscriptionId)
	{
		return update(DELETE_ELIGIBILITY_STATUS, subscriptionId);
	}
	

	@Override
	protected UserSubscriptionEligibilityVO mapDTO(ResultSet rs) throws SQLException{
		UserSubscriptionEligibilityVO dto = new UserSubscriptionEligibilityVO();
		dto.setUseId(rs.getLong("USE_ID"));
		dto.setSubscriptionId(rs.getLong("SUBSCRIPTION_ID"));
		dto.setProjectId(rs.getInt("PROJECT_ID"));
		dto.setStatus(rs.getString("STATUS"));
		dto.setUpdatedTime(rs.getTimestamp("UPDATED_DATE"));
		
		return dto;
	}

}

package sa.com.mobily.eportal.cacheinstance.valueobjects;

import java.util.Date;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class ContactsVO extends BaseVO
{
	private static final long serialVersionUID = 1L;

	private Long id;

	private Long userAccountId;

	private String email;

	private String firstName;

	private String lastName;

	private String phone;

	private Date creationDate;

	public Long getId()
	{
		return this.id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Long getUserAccountId()
	{
		return this.userAccountId;
	}

	public void setUserAccountId(Long userAccountId)
	{
		this.userAccountId = userAccountId;
	}

	public String getEmail()
	{
		return this.email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getFirstName()
	{
		return this.firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return this.lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getPhone()
	{
		return this.phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public Date getCreationDate()
	{
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}
}

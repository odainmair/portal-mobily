package sa.com.mobily.eportal.common.constants;

public interface CommonServiceslLoggerConstantIfcs
{
	public static String GLOBYS_REG_SERVICE_LOGGER_NAME = "MobilyCommonService/GlobysEbillRegisteration";
	public static String EMAIL_SENDER_SERVICE_LOGGER_NAME = "MobilyCommonService/EmailSender";
	public static String RECAPTCHA_SERVICE_LOGGER_NAME = "MobilyCommonService/recaptcha";
	public static String PAYMENT_SERVICE_LOGGER_NAME = "MobilyCommonService/paymentService";
	public static String COMMON_SERVICES_LOGGER_NAME = "MobilyCommonService/commonServices";
	public static String WCM_SERVICES_LOGGER_NAME = "MobilyCommonService/wcmServices";
	public static String CUSTOMER_INFO_SERVICE_LOGGER_NAME = "MobilyCommonService/customerInfo";
	public static String EX_HANDLER_FILTER_SERVICE_LOGGER_NAME = "MobilyCommonService/exceptionHandlerFilter";
	public static String MQ_CONNECTION_HANDLER_SERVICE_LOGGER_NAME = "MobilyCommonService/MQConnectionHandler";
	public static String GOOGLE_RECAPTCHA_V2_LOGGER_NAME = "MobilyCommonService/googleRecaptchaV2";
	
	
	
}

package sa.com.mobily.eportal.common.dao.constants;

public class DAOConstants{
	public static interface Contacts{
		public static final String ID = "ID";
		public static final String FIRST_NAME = "FIRST_NAME";
		public static final String LAST_NAME = "LAST_NAME";
		public static final String PHONE = "PHONE";
		public static final String EMAIL = "EMAIL";
		public static final String CREATION_DATE = "CREATION_DATE";
		public static final String USER_ACCT_ID = "USER_ACCT_ID";
	}
	
	public static interface FailedOperations{
		public static final String ID = "ID";
		public static final String USER_ACCT_ID = "USER_ACCT_ID";
		public static final String CREATED_DATE = "CREATED_DATE";
		public static final String FAILED_OPERATION_ID = "FAILED_OPERATION_ID";
		public static final String ACCOUNT_XML = "ACCOUNT_XML";
		public static final String LAST_UPDATED_DATE = "LAST_UPDATED_DATE";
		public static final String NO_OF_RETRY = "NO_OF_RETRY";
		public static final String PROCESS_STATUS = "PROCESS_STATUS";
	}
	
	public static interface UserAccount{
		public static final String USER_ACCT_ID = "USER_ACCT_ID";
		public static final String USER_NAME = "USER_NAME";
		//public static final String IQAMA = "IQAMA";
		public static final String CREATED_DATE = "CREATED_DATE";
		public static final String ACTIVATION_DATE = "ACTIVATION_DATE";
		public static final String EMAIL = "EMAIL";
		public static final String STATUS = "STATUS";
		public static final String FIRST_NAME = "FIRST_NAME";
		public static final String LAST_NAME = "LAST_NAME";
		public static final String CUST_SEGMENT = "CUST_SEGMENT";
		public static final String BIRTHDATE = "BIRTHDATE";
		public static final String NATIONALITY = "NATIONALITY";
		public static final String GENDER = "GENDER";
		public static final String REGISTRATION_DATE = "REGISTRATION_DATE";
		public static final String LAST_LOGIN_TIME = "LAST_LOGIN_TIME";
		public static final String USERACCTID = "USERACCTID";
		public static final String ACTIVATION_URL = "ACTIVATION_URL";
		public static final String CONTACT_NUMBER = "CONTACT_NUMBER";
		public static final String ROLE_ID = "ROLE_ID";
		public static final String PACKAGE_ID = "PACKAGE_ID";
		public static final String PACKAGE_TYPE = "PACKAGE_TYPE";
	}
	
	public static interface UserRegistrationAudit{
		public static final String  USER_NAME = "USER_NAME"; 
		public static final String  ACCOUNT_NUMBER = "ACCOUNT_NUMBER";        
		public static final String  EMAIL_ID = "EMAIL_ID";              
		public static final String  FACEBOOK_ID = "FACEBOOK_ID";           
		public static final String  TWITTER_ID = "TWITTER_ID";            
		public static final String  CREATED_DATE = "CREATED_DATE";           
		public static final String  REGISTRATION_SOURCE_ID = "REGISTRATION_SOURCE_ID";
		public static final String  ERROR_CODE = "ERROR_CODE";    
		public static final String  ERROR_MESSAGE = "ERROR_MESSAGE";
	}

}

package sa.com.mobily.eportal.common.exception.application;


public class InvalidStatusException extends MobilyApplicationException {

 
	private static final long serialVersionUID = -3419462789848440657L;

	public InvalidStatusException() {
	}

	public InvalidStatusException(String message) {
		super(message);
	}

	public InvalidStatusException(Throwable cause) {
		super(cause);
	}

	public InvalidStatusException(String message, Throwable cause) {
		super(message, cause);
	}
}
package sa.com.mobily.eportal.common.exception.handler;

import java.util.logging.Level;

import javax.portlet.PortletRequest;

import org.apache.commons.lang3.StringUtils;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.DataNotFoundException;
import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.mail.ApacheMailSender;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;
import sa.com.mobily.eportal.common.service.vo.EmailVO;
import sa.com.mobily.eportal.core.api.BackEndServiceError;
import sa.com.mobily.eportal.core.dao.BackEndServiceErrorDAO;

public class EmailNotificationHandler implements ExceptionHandler
{

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	private static String clazz = EmailNotificationHandler.class.getName();

	private EmailVO emailVO;

	public EmailNotificationHandler(EmailVO emailVO)
	{
		this.emailVO = emailVO;
	}

	@Override
	public String handleException(PortletRequest request)
	{
		ApacheMailSender mailSender = new ApacheMailSender();
		mailSender.sendSimpleEmail(emailVO);
		String errorMessage = null;

		String language = request.getLocale().getLanguage();
		int serviceId = ConstantIfc.NO_SERVICE;
		int errorCode = ConstantIfc.GENERAL_ERROR;
		BackEndServiceErrorDAO backEndServiceErrorDAO = BackEndServiceErrorDAO.getInstance();
		BackEndServiceError backEndServiceError = backEndServiceErrorDAO.getBackEndServiceError(serviceId, errorCode);

		if (backEndServiceError == null)
		{
			// give error
		}
		if (StringUtils.isNotEmpty(language) && language.equalsIgnoreCase("ar"))
		{
			errorMessage = backEndServiceError.getErrorMsgAr();
		}
		else
		{
			errorMessage = backEndServiceError.getErrorMsgEn();
		}

		return errorMessage;
	}

	@Override
	public BackEndServiceError getExceptionBackEndError(int serviceId, String errorCode)
	{
		BackEndServiceError backEndServiceError = null;

		if(FormatterUtility.isEmpty(errorCode))
			throw new DataNotFoundException("errorCode IS NULL");

		try
		{
			BackEndServiceErrorDAO backEndServiceErrorDAO = BackEndServiceErrorDAO.getInstance();
			backEndServiceError = backEndServiceErrorDAO.getBackEndServiceError(serviceId, Integer.valueOf(errorCode).intValue());
								
		}
		catch (Exception e)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, e.getMessage(), clazz, "handleException", e);
			executionContext.log(logEntryVO);			
		}
		
		if(null==backEndServiceError)
			throw new DataNotFoundException("BackEndServiceError IS NULL");
		
		// Guarantee that English and Arabic message is submitted because it could be null in DB
		
		if(FormatterUtility.isEmpty(backEndServiceError.getErrorMsgEn()))
				backEndServiceError.setErrorMsgEn("service[" + serviceId + "] error ["+ errorCode + "] has no english error message");
		
		if(FormatterUtility.isEmpty(backEndServiceError.getErrorMsgAr()))
			backEndServiceError.setErrorMsgAr("service[" + serviceId + "] error ["+ errorCode + "] has no arabic error message");
				
		return backEndServiceError;

	}
}
package sa.com.mobily.eportal.common.exception.handler;

import javax.portlet.PortletRequest;

import sa.com.mobily.eportal.core.api.BackEndServiceError;

public interface ExceptionHandler
{
	String handleException(PortletRequest request);
	
	BackEndServiceError getExceptionBackEndError(int serviceId , String errorCode);	
}
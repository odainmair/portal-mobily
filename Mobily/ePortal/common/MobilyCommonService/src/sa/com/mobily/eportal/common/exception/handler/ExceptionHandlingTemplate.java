package sa.com.mobily.eportal.common.exception.handler;

import java.util.ArrayList;

import javax.portlet.PortletRequest;

import org.apache.commons.lang3.exception.ExceptionUtils;

import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.application.BusinessValidationException;
import sa.com.mobily.eportal.common.exception.application.DataNotFoundException;
import sa.com.mobily.eportal.common.exception.system.ConfigurationException;
import sa.com.mobily.eportal.common.exception.system.MiddlewareException;
import sa.com.mobily.eportal.common.exception.system.PersistenceException;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.exception.system.XMLParserException;
import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.util.AppConfig;
import sa.com.mobily.eportal.common.service.vo.EmailVO;
import sa.com.mobily.eportal.core.api.BackEndServiceError;

public class ExceptionHandlingTemplate
{
	public static final String handleException(Throwable exception, PortletRequest request)
	{
		ExceptionHandler handler = null;
		String result = null;
		try
		{
			// avoiding instanceof operator
			throw exception;
		}
		catch (BackEndException bec)
		{
			request.setAttribute("serviceId", bec.getServiceID());
			request.setAttribute("errorCode", bec.getErrorCode());
			handler = new BackEndMessageHandler();
			result = handler.handleException(request);
		}
		catch (BusinessValidationException bve)
		{

		}
		catch (DataNotFoundException dnfe)
		{

		}
		catch (ConfigurationException cfge)
		{
			// Notify Developers

			handler = new EmailNotificationHandler(getEmailVO(ConstantIfc.PORTAL_ADMIN_EMAIL_ADDRESS, cfge));
			result = handler.handleException(request);
		}
		catch (XMLParserException xmlpe)
		{
			// Notify Developers
			handler = new EmailNotificationHandler(getEmailVO(ConstantIfc.PORTAL_DEV_EMAIL_ADDRESS, xmlpe));
			result = handler.handleException(request);
		}
		catch (ServiceException se)
		{
			// Notify Developers
			handler = new EmailNotificationHandler(getEmailVO(ConstantIfc.PORTAL_DEV_EMAIL_ADDRESS, se));
			result = handler.handleException(request);
		}
		catch (MiddlewareException me)
		{
			// Notify MQ team
			handler = new EmailNotificationHandler(getEmailVO(ConstantIfc.PORTAL_MQ_EMAIL_ADDRESS, me));
			result = handler.handleException(request);
		}
		catch (PersistenceException pe)
		{
			// Notify DBA team
			handler = new EmailNotificationHandler(getEmailVO(ConstantIfc.PORTAL_DBA_EMAIL_ADDRESS, pe));
			result = handler.handleException(request);
		}
		catch (RuntimeException re)
		{
			// Notify Developers
			handler = new EmailNotificationHandler(getEmailVO(ConstantIfc.PORTAL_DEV_EMAIL_ADDRESS, re));
			result = handler.handleException(request);
		}
		catch (Throwable t)
		{
			result = t.getMessage();
		}
		return result;
	}

	

	
	public static final BackEndServiceError getExceptionBackEndError(int serviceId, String errorCode)
	{
		ExceptionHandler handler = new BackEndMessageHandler();
		return handler.getExceptionBackEndError(serviceId, errorCode);
	}

	private static EmailVO getEmailVO(String key, Throwable t)
	{
		String recepient = AppConfig.getInstance().get(key);
		String sender = AppConfig.getInstance().get(ConstantIfc.PORTAL_SENDER_EMAIL_ADDRESS);
		String subject = AppConfig.getInstance().get(ConstantIfc.PORTAL_EMAIL_SUBJECT);
		EmailVO emailVO = new EmailVO();
		ArrayList<String> recipients = new ArrayList<String>();
		recipients.add(recepient);
		emailVO.setRecipients(recipients);
		emailVO.setMailBody(ExceptionUtils.getStackTrace(t));
		emailVO.setSender(sender);
		emailVO.setSubject(subject);
		return emailVO;
	}
}
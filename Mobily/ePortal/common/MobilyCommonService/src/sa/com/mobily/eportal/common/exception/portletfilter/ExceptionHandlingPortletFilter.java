package sa.com.mobily.eportal.common.exception.portletfilter;

import java.io.IOException;
import java.util.logging.Level;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.filter.ActionFilter;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.RenderFilter;
import javax.portlet.filter.ResourceFilter;

import org.apache.commons.lang3.StringUtils;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.handler.ExceptionHandlingTemplate;
import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;

public class ExceptionHandlingPortletFilter implements RenderFilter, ActionFilter, ResourceFilter
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.EX_HANDLER_FILTER_SERVICE_LOGGER_NAME);
//	private static final Logger LOGGER = LoggerHelper.getLogger("ExceptionHandlingPortletFilter");

//	private String className = ExceptionHandlingPortletFilter.class.getName();

	@Override
	public void destroy()
	{
//		LOGGER.logp(Level.INFO, className, "init", "Filter Destroyed");
	}

	@Override
	public void init(FilterConfig filterConfig) throws PortletException
	{
//		LOGGER.logp(Level.INFO, className, "init", "Filter Initialized");
	}

	@Override
	public void doFilter(ResourceRequest request, ResourceResponse response, FilterChain chain) throws IOException, PortletException
	{
//		LOGGER.logp(Level.INFO, className, "doFilter-ResourceRequest", "Came inside doFilter of type ResourceRequest");
		String clazz = ExceptionHandlingPortletFilter.class.getName();
		String method = "doFilter-ResourceRequest";
		boolean exceptionOccured = false;
		String portletClass = null;
		String portletMethod = null;
		String exceptionInPortlet = null;
		try
		{
			executionContext.startMethod("ExceptionHandlingPortletFilter", "doFilter(Resource)", null);
//			LOGGER.logp(Level.INFO, className, "doFilter-ResourceRequest", "Before chain.doFilter");
			chain.doFilter(request, response);
//			LOGGER.logp(Level.INFO, className, "doFilter-ResourceRequest", "After chain.doFilter");
		}
		catch (Throwable exception)
		{
//			LOGGER.logp(Level.SEVERE, className, "doFilter-ResourceRequest", "Throwable", exception);
			exceptionOccured = true;
			String errorCode = ExceptionHandlingTemplate.handleException(exception, request);
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, "EXCEPTION " + exception.getMessage(), clazz, method, exception);
			executionContext.log(logEntryVO);
			request.setAttribute(ConstantIfc.ERROR_MSG, errorCode);
		}
		finally
		{
			if (request.getAttribute("portlet-class") != null)
			{
				portletClass = (String) request.getAttribute("portlet-class");
			}
			else
			{
//				LOGGER.logp(Level.INFO, className, "doFilter-ResourceRequest", "portlet-class is  " + request.getAttribute("portlet-class"));
			}
			if (request.getAttribute("portlet-method") != null)
			{
				portletMethod = (String) request.getAttribute("portlet-method");
			}
			else
			{
//				LOGGER.logp(Level.INFO, className, "doFilter-ResourceRequest", "portlet-method is  " + request.getAttribute("portlet-method"));
			}
			if (request.getAttribute("exceptionOccurred") != null)
			{
				exceptionInPortlet = (String) request.getAttribute("exceptionOccurred");
			}
			else
			{
//				LOGGER.logp(Level.INFO, className, "doFilter-ResourceRequest", "exceptionOccurred is  " + request.getAttribute("exceptionOccurred"));
			}

			if (StringUtils.isNotEmpty(exceptionInPortlet) && exceptionInPortlet.equalsIgnoreCase("YES"))
			{
				exceptionOccured = true;
			}

			if (StringUtils.isNotEmpty(portletClass))
			{
				executionContext.endMethod(portletClass, portletMethod, null);
				executionContext.print(portletClass, exceptionOccured);
//				LOGGER.logp(Level.INFO, className, "doFilter-ResourceRequest", "Print with " + portletClass);
			}
			else
			{
				executionContext.endMethod(clazz, method, null);
				executionContext.print(clazz, exceptionOccured);
//				LOGGER.logp(Level.INFO, className, "doFilter-ResourceRequest", "Print with " + clazz);
			}
//			LOGGER.logp(Level.INFO, className, "doFilter-ResourceRequest", "Before clearExecutionContext");
			executionContext.clearExecutionContext();
//			LOGGER.logp(Level.INFO, className, "doFilter-ResourceRequest", "After clearExecutionContext");
		}
	}

	@Override
	public void doFilter(ActionRequest request, ActionResponse response, FilterChain chain) throws IOException, PortletException
	{
//		LOGGER.logp(Level.INFO, className, "doFilter-ActionRequest", "Came for process action");
		String clazz = ExceptionHandlingPortletFilter.class.getName();
		String method = "doFilter-ActionRequest";
		boolean exceptionOccured = false;
		String portletClass = null;
		String portletMethod = null;
		String exceptionInPortlet = null;
		try
		{
			executionContext.startMethod("ExceptionHandlingPortletFilter", "doFilter(Action)", null);
			chain.doFilter(request, response);
		}
		catch (Throwable exception)
		{
			exceptionOccured = true;
			String errorCode = ExceptionHandlingTemplate.handleException(exception, request);
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, "EXCEPTION " + exception.getMessage(), clazz, method, exception);
			executionContext.log(logEntryVO);
			response.setRenderParameter(ConstantIfc.ERROR_MSG, errorCode);
		}
		finally
		{
			portletClass = (String) request.getAttribute("portlet-class");
			portletMethod = (String) request.getAttribute("portlet-method");
			exceptionInPortlet = (String) request.getAttribute("exceptionOccurred");

			if (StringUtils.isNotEmpty(exceptionInPortlet) && exceptionInPortlet.equalsIgnoreCase("YES"))
			{
				exceptionOccured = true;
			}
//			LOGGER.logp(Level.INFO, className, "doFilter-ActionRequest", "Print with prrtlet class = " + portletClass);
			if (StringUtils.isNotEmpty(portletClass))
			{
				executionContext.endMethod(portletClass, portletMethod, null);
				executionContext.print(portletClass, exceptionOccured);
			}
			else
			{
				executionContext.endMethod(clazz, method, null);
				executionContext.print(clazz, exceptionOccured);
			}
			executionContext.clearExecutionContext();
		}
	}

	@Override
	public void doFilter(RenderRequest request, RenderResponse response, FilterChain chain) throws IOException, PortletException
	{
//		LOGGER.logp(Level.INFO, className, "doFilter-RenderRequest", "Came for do view");
		String clazz = ExceptionHandlingPortletFilter.class.getName();
		String method = "doFilter-RenderRequest";
		boolean exceptionOccured = false;
		String portletClass = null;
		String portletMethod = null;
		String exceptionInPortlet = null;
		try
		{
			executionContext.startMethod("ExceptionHandlingPortletFilter", "doFilter(Render)", null);
			chain.doFilter(request, response);
		}
		catch (Throwable exception)
		{
			exceptionOccured = true;
			String errorCode = ExceptionHandlingTemplate.handleException(exception, request);
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, "EXCEPTION " + exception.getMessage(), clazz, method, exception);
			executionContext.log(logEntryVO);
			request.setAttribute(ConstantIfc.ERROR_MSG, errorCode);
		}
		finally
		{
			portletClass = (String) request.getAttribute("portlet-class");
			portletMethod = (String) request.getAttribute("portlet-method");
			exceptionInPortlet = (String) request.getAttribute("exceptionOccurred");

			if (StringUtils.isNotEmpty(exceptionInPortlet) && exceptionInPortlet.equalsIgnoreCase("YES"))
			{
				exceptionOccured = true;
			}

//			LOGGER.logp(Level.INFO, className, "doFilter-RenderRequest", "Print with prrtlet class = " + portletClass);

			if (StringUtils.isNotEmpty(portletClass))
			{
				executionContext.endMethod(portletClass, portletMethod, null);
				executionContext.print(portletClass, exceptionOccured);
			}
			else
			{
				executionContext.endMethod(clazz, method, null);
				executionContext.print(clazz, exceptionOccured);
			}
			executionContext.clearExecutionContext();
		}
	}
}
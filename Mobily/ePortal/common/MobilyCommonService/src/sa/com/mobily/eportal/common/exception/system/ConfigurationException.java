package sa.com.mobily.eportal.common.exception.system;


public class ConfigurationException extends MobilySystemException {

	private static final long serialVersionUID = 9027985265019869098L;

	public ConfigurationException() {
	}

	public ConfigurationException(String message) {
		super(message);
	}

	public ConfigurationException(Throwable cause) {
		super(cause);
	}

	public ConfigurationException(String message, Throwable cause) {
		super(message, cause);
	}
}
package sa.com.mobily.eportal.common.exception.system;


public class MiddlewareException extends MobilySystemException {

	private static final long serialVersionUID = 1939154083158847159L;

	public MiddlewareException() {
	}

	public MiddlewareException(String message) {
		super(message);
	}

	public MiddlewareException(Throwable cause) {
		super(cause);
	}

	public MiddlewareException(String message, Throwable cause) {
		super(message, cause);
	}
}
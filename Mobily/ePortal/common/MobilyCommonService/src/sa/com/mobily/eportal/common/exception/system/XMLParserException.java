package sa.com.mobily.eportal.common.exception.system;

public class XMLParserException  extends MobilySystemException {

	private static final long serialVersionUID = 3508961876555803251L;

	public XMLParserException() {
	}

	public XMLParserException(String message) {
		super(message);
	}

	public XMLParserException(Throwable cause) {
		super(cause);
	}

	public XMLParserException(String message, Throwable cause) {
		super(message, cause);
	}
}
package sa.com.mobily.eportal.common.persistence.util;

import javax.persistence.EntityManagerFactory;

import javax.persistence.Persistence;


public enum EMF  {

	INSTANCE;

	private EntityManagerFactory emfInstance = Persistence.createEntityManagerFactory("CMPU");

	public EntityManagerFactory get() {
		return emfInstance;
	}
}

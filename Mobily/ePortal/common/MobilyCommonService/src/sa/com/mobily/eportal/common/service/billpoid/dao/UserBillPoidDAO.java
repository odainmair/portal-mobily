package sa.com.mobily.eportal.common.service.billpoid.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.common.service.billpoid.model.UserBillPoid;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

public class UserBillPoidDAO extends AbstractDBDAO<UserBillPoid> {

    private static UserBillPoidDAO instance = new UserBillPoidDAO();
    
    private static final String FIND_ALL_BY_MSISDN = "SELECT MSISDN, POID, CREATION_DATE FROM USER_BILL_POID WHERE MSISDN = ? ";
        
    private static final String INSERT_USER_BILL_POID = "INSERT INTO USER_BILL_POID(MSISDN, POID, CREATION_DATE) VALUES(?, ?, SYSDATE)";
    
    private static final String UPDATE_USER_BILL_POID = "UPDATE USER_BILL_POID SET POID=? WHERE MSISDN=? ";
    
    private static final String DELETE_USER_BILL_POID = "DELETE FROM USER_BILL_POID WHERE MSISDN=?";
    
    protected UserBillPoidDAO() {
    	super(DataSources.DEFAULT);
    }

    /**
     * Method for obtaining the singleton instance
     * 
     * @return
     */
    public static synchronized UserBillPoidDAO getInstance() {
    	return instance;
    }
	
    public List<UserBillPoid> findAllByMsisdn(String msisdn) {
        return query(FIND_ALL_BY_MSISDN, msisdn );
    }
    
    public int saveBillPOID(UserBillPoid userBillPoidVO) {
    	return update(INSERT_USER_BILL_POID, userBillPoidVO.getMsisdn(), userBillPoidVO.getPoid());
    }
    
    public int updateBillPOID(UserBillPoid userBillPoidVO) {
    	return update(UPDATE_USER_BILL_POID, userBillPoidVO.getPoid(), userBillPoidVO.getMsisdn());
    }
    
    /***
	 * Delete invoice related data
	 * @param msisdn
	 * @return
	 */
	public int deleteUserBillPoidMisisdn(String msisdn) {
        return update(DELETE_USER_BILL_POID, new Object[] {msisdn});
    }
    

    protected UserBillPoid mapDTO(ResultSet rs) throws SQLException {
    	
    	UserBillPoid userBillPoidVO = new UserBillPoid();
    	userBillPoidVO.setMsisdn(rs.getString("MSISDN"));
    	userBillPoidVO.setPoid(rs.getString("POID"));
    	userBillPoidVO.setCreationDate(rs.getTimestamp("CREATION_DATE"));
		return userBillPoidVO;
    }
}
package sa.com.mobily.eportal.common.service.billpoid.model;

import java.sql.Timestamp;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class UserBillPoid  extends BaseVO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3096847901735279831L;
	private String msisdn;
	private String poid;
	private Timestamp creationDate;
	
	public String getMsisdn()
	{
		return msisdn;
	}
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	public String getPoid()
	{
		return poid;
	}
	public void setPoid(String poid)
	{
		this.poid = poid;
	}
	public Timestamp getCreationDate()
	{
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate)
	{
		this.creationDate = creationDate;
	}
}

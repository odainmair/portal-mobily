/*
 * Created on Feb 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.business;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerStatusDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.CustomerStatusVO;
import sa.com.mobily.eportal.common.service.xml.CustomerStatusXMLHandler;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CustomerStatusBO {

    private static final Logger log = LoggerInterface.log;
    
    public CustomerStatusVO getCustomerStatus(CustomerStatusVO requestVO) throws MobilyCommonException
    {
        CustomerStatusVO  replyObj = null;
        //generate XML request
        try {
            String xmlReply = "";
            CustomerStatusXMLHandler  xmlHandler = new CustomerStatusXMLHandler();
            log.debug("CustomerStatusBO >getCustomerStatus > generate xml message for Customer ["+requestVO.getLineNumber()+"] > ");
            String xmlMessage  = xmlHandler.generateXMLRequest(requestVO);
            
            
            
            //get DAO object
            MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
            CustomerStatusDAO customerStatusDAO = daoFactory.getCustomerStatusDAO();
            xmlReply = customerStatusDAO.getCustomerStatus(xmlMessage);
            log.debug("CustomerStatusBO >getCustomerStatus > parsing the reply message for Customer ["+requestVO.getLineNumber()+"] > ");
            
            //parsing the xml reply
            replyObj = xmlHandler.parsingXMLReplyMessage(xmlReply);
        } catch (MobilyCommonException e) {
            // TODO Auto-generated catch block
            throw e;
        }
      return replyObj;    
    
    }
    public static void main(String[] args) {
    }
}

package sa.com.mobily.eportal.common.service.business;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.PaymentotifyDAO;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.PaymentNotifyReplyVO;
import sa.com.mobily.eportal.common.service.vo.PaymentNotifyRequestVO;
import sa.com.mobily.eportal.common.service.xml.PaymentNotifyXMLHandler;

/**
 * @author n.gundluru.mit
 *
 */
public class PaymentNotificationBO {

    private static final Logger logger = LoggerInterface.log;
    
    /**
     * This method is responsible for sending a payment notification
     * 
     * @param requestVO
     * @return
     */
    public PaymentNotifyReplyVO sendPaymentNotification(PaymentNotifyRequestVO requestVO) {
        
    	PaymentNotifyReplyVO  replyObj = null;
        //generate XML request
        try {
            String xmlReply = "";
            PaymentNotifyXMLHandler  xmlHandler = new PaymentNotifyXMLHandler();
            String xmlMessage  = xmlHandler.generateXMLRequest(requestVO);
           	logger.info("PaymentNotificationBO > sendPaymentNotification > XML Request message =["+xmlMessage+"]");
            
            //get DAO object
            MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
            PaymentotifyDAO paymentNotifyDAO = daoFactory.getPaymentotifyDAO();
            xmlReply = paymentNotifyDAO.processPaymentNotification(xmlMessage);
            logger.debug("PaymentNotificationBO > sendPaymentNotification > XML Reply message =["+xmlReply+"]");
            
            //parsing the xml reply
            	replyObj = xmlHandler.parsingXMLRreply(xmlReply);
            	
        } catch (Exception e) {
        	logger.fatal("PaymentNotificationBO > sendPaymentNotification > Exception =["+e+"]");
        }
      return replyObj;    
    
    }
}
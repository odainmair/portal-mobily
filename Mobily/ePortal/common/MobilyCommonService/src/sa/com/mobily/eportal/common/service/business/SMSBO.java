/*
 * Created on Jan 29, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.business;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.imp.MQSMSDAO;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.sms.vo.SMSVO;


/**
 * @author ysawy
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SMSBO  
{
    public static final Logger log = LoggerInterface.log;
    public void sendSMS( String aXMLRequest ) 
    {
    	
    	    log.debug("Calling sendSMS from Mobily Services Business...");
    	    MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory( DAOFactory.MQ );
    	    MQSMSDAO sms = (MQSMSDAO)daoFactory.getSMSDAO();
    	    sms.sendSMS( aXMLRequest );
   	  
    }
    
    public void sendSMS( SMSVO smsVO) 
    {
    	
    	    log.debug("Calling sendSMS from Mobily Services Business...");
    	    MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory( DAOFactory.MQ );
    	    MQSMSDAO sms = (MQSMSDAO)daoFactory.getSMSDAO();
    	    sms.sendSMS( smsVO );
   	  
    }
}

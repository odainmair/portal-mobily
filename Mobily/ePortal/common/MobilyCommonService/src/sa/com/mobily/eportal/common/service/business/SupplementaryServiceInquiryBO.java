/*
 * Created on Feb 19, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.business;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.PromotionInquiryDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.SupplementaryServiceInquieyDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleInternationalBarringInquiryDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.MobilyPromotionVO;
import sa.com.mobily.eportal.common.service.vo.MyServiceInquiryRequestVO;
import sa.com.mobily.eportal.common.service.vo.MyServiceInquiryVO;
import sa.com.mobily.eportal.common.service.xml.MyServiceXMLHandler;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SupplementaryServiceInquiryBO {
    private static final Logger logger = LoggerInterface.log;
    
    
    public MyServiceInquiryVO getSupplementaryServiceInquiryPage(MyServiceInquiryRequestVO requestVO , boolean getPromotion , boolean getInternationStatus) {
        MyServiceInquiryVO replyObj = new MyServiceInquiryVO();
        try {
            //load supplemenatry service
            replyObj = getSupplementaryServiceInquirty (requestVO);
        } catch (MobilyCommonException e) {
            replyObj.setErrorCode(e.getErrorCode());
        }
        
        if(getPromotion){
            try {
                MobilyPromotionVO obj = getMagicPromotionStatus(requestVO);
                if(obj != null){
                  replyObj.setListOfNotSubscribedpromotion(obj.getListOfNotSubscribedpromotion());
                  replyObj.setListOfSubscribedPromotion(obj.getListOfSubscribedPromotion());
                }
            } catch (MobilyCommonException e) {
                replyObj.setErrorCode(e.getErrorCode());
            }
        }    
          if(getInternationStatus){
              replyObj.setInternationalBarring(getInternationBarringStatus(requestVO.getMsisdn()));
          }  
        
        return replyObj;
    }
    
    /**
     * 
     * @param requestVO
     * @param getPromotion
     * @param getInternationStatus
     * @param wifiStatus
     * @return
     */
	public MyServiceInquiryVO getSupplementaryServiceInquiryPage(MyServiceInquiryRequestVO requestVO, boolean getPromotion,
																	boolean getInternationStatus, boolean wifiStatus) {
		logger.info("SupplementaryServiceInquiryBO >getSupplementaryServiceInquiryPage > called");
		
		MyServiceInquiryVO replyObj = new MyServiceInquiryVO();
		try {
            
			logger.debug("SupplementaryServiceInquiryBO >getSupplementaryServiceInquiryPage > wifiStatus >"+wifiStatus);
			
			// load supplemenatry service
			replyObj = getSupplementaryServiceInquirty(requestVO);

			if (getPromotion) {
				try {
					MobilyPromotionVO obj = getMagicPromotionStatus(requestVO);
					if (obj != null) {
						replyObj.setListOfNotSubscribedpromotion(obj
								.getListOfNotSubscribedpromotion());
						replyObj.setListOfSubscribedPromotion(obj
								.getListOfSubscribedPromotion());
					}
				} catch (MobilyCommonException e) {
					replyObj.setErrorCode(e.getErrorCode());
				}
			}
			if (getInternationStatus) {
				replyObj
						.setInternationalBarring(getInternationBarringStatus(requestVO
								.getMsisdn()));
			}

			if (wifiStatus) {
				replyObj.setWifiServiceType(getWifiRoamingStatus(requestVO));
			}

		} catch (MobilyCommonException e) {
			replyObj.setErrorCode(e.getErrorCode());
		}

		return replyObj;
	}

    
    
    
    /**
     * @param requestVO
     * @return
     * @throws MobilyCommonException
     * Feb 20, 2008
     * SupplementaryServiceInquiryBO.java
     * msayed
     */
    public MyServiceInquiryVO getSupplementaryServiceInquirty(MyServiceInquiryRequestVO requestVO) throws MobilyCommonException{
        MyServiceInquiryVO  replyObj = null;
        //generate XML request
        try {
            String xmlReply = "";
            MyServiceXMLHandler  xmlHandler = new MyServiceXMLHandler();
            String xmlMessage  = xmlHandler.generateSupplementaryServiceInquiryMessage(requestVO);
            
            logger.info("SupplementaryServiceInquiryBO >getSupplementaryServiceInquirty > generate xml message for Customer ["+requestVO.getMsisdn()+"] > "+xmlMessage);
            
            //get DAO object
            MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
            SupplementaryServiceInquieyDAO supplementaryServiceInquieyDAO = daoFactory.getSupplementaryServiceInquieyDAO();
            xmlReply = supplementaryServiceInquieyDAO.getSupplementaryInquiry(xmlMessage);
//            xmlReply="<EE_EAI_MESSAGE><EE_EAI_HEADER><MsgFormat>SUPPLEMENTARY_SERVICES_INQUIRY</MsgFormat><MsgVersion>0000</MsgVersion><RequestorChannelId>ePortal</RequestorChannelId><RequestorChannelFunction>INQ</RequestorChannelFunction><RequestorUserId></RequestorUserId><RequestorLanguage>E</RequestorLanguage><RequestorSecurityInfo>secure</RequestorSecurityInfo><ReturnCode>0</ReturnCode></EE_EAI_HEADER><MSISDN>966540511154</MSISDN><SupplementaryServices><SupplementaryService><ServiceName>RBT</ServiceName><Status>1</Status></SupplementaryService><SupplementaryService><ServiceName>ICS</ServiceName><Status>0</Status></SupplementaryService><SupplementaryService><ServiceName>SMS_Bundle</ServiceName><Status>1</Status><ServiceType>SMS_250</ServiceType></SupplementaryService><SupplementaryService><ServiceName>LBS</ServiceName><Status>0</Status></SupplementaryService><SupplementaryService><ServiceName>CUG</ServiceName><Status>0</Status></SupplementaryService></SupplementaryServices></EE_EAI_MESSAGE>";
            logger.info("SupplementaryServiceInquiryBO >getSupplementaryServiceInquirty > parsing the reply message for Customer ["+requestVO.getMsisdn()+"] > "+xmlReply);
            
            //parsing the xml reply
            replyObj = xmlHandler.ParsingSupplementaryServiceInquiryMQReply(xmlReply);
        } catch (MobilyCommonException e) {
            // TODO Auto-generated catch block
            throw e;
        }
      return replyObj;    

    }
    
    /**
     * @param msisdn
     * @return
     * Feb 20, 2008
     * SupplementaryServiceInquiryBO.java
     * msayed
     */
    public int getInternationBarringStatus(String msisdn){
        logger.debug("SupplementaryServiceInquiryBO > getInternationBarringStatus > for MSISDN = ["+msisdn+"] ");
        int internationStatus = -1;
    
     	OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
	  	OracleInternationalBarringInquiryDAO internationalBarringInquiryDAO =(OracleInternationalBarringInquiryDAO)daoFactory.getInternationalBarringInquiryDAO();
	  	try {
            internationStatus= internationalBarringInquiryDAO.getInternationalbarringStatus(msisdn);
        } catch (MobilyCommonException e) {
            // TODO Auto-generated catch block
            
        }
        logger.debug("SupplementaryServiceInquiryBO > getInternationBarringStatus > for MSISDN = ["+msisdn+"] >  where the status = "+internationStatus);
        return internationStatus;
    
    }
    
    public MobilyPromotionVO getMagicPromotionStatus(MyServiceInquiryRequestVO requestVO) throws MobilyCommonException{
        MobilyPromotionVO  replyObj = null;
        	// generate XML request
        		String xmlReply = "";
        		MyServiceXMLHandler  xmlHandler = new MyServiceXMLHandler();
        		String xmlMessage  = xmlHandler.generateMobilyPromotionXMLRequest(requestVO);

        	logger.debug("SupplementaryServiceInquiryBO >getMagicPromotionStatus > generate xml message for Customer ["+requestVO.getMsisdn()+"] > "+xmlMessage);
            
            // get DAO object
            	MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
            	PromotionInquiryDAO promotionInquiryDAO = daoFactory.getPromotionInquiryDAO();
            	xmlReply = promotionInquiryDAO.managePromotion(xmlMessage);
            // xmlReply="<MOBILY_BSL_SR><SR_HEADER><FuncId>2</FuncId><SecurityKey>qazwsxedc</SecurityKey><ChannelTransId>null</ChannelTransId><MsgVersion>0000</MsgVersion><RequestorChannelId>ePortal</RequestorChannelId><RequestorUserId/><SrDate>20080203091419</SrDate></SR_HEADER><Status>2</Status ><ReturnCodes><ReturnCode>0</ReturnCode ></ReturnCodes><Promotions><AlreadySubscribedList><PromotionId></PromotionId></AlreadySubscribedList><NotSubscribedList><PromotionId>Mobily Magic 1</PromotionId><PromotionId>Mobily Magic 2</PromotionId><PromotionId>Mobily Magic 5</PromotionId></NotSubscribedList></Promotions></MOBILY_BSL_SR>";
            logger.debug("SupplementaryServiceInquiryBO >getMagicPromotionStatus > parsing the reply message for Customer ["+requestVO.getMsisdn()+"] > "+xmlReply);
            
            //parsing the xml reply
            	replyObj = xmlHandler.ParsingMobilyPromotionInquiryReplyXML(xmlReply);

        return replyObj;    
    }
    
    /**
     * Checks the WiFi roaming service subscription status
     * 
     * @param requestVO
     * @return String
     * @throws MobilyCommonException
     */
    public String getWifiRoamingStatus(MyServiceInquiryRequestVO requestVO) throws MobilyCommonException{
    	MyServiceInquiryVO  replyObj = null;
        //generate XML request

    		String xmlReply = "";
            MyServiceXMLHandler  xmlHandler = new MyServiceXMLHandler();
            String xmlMessage  = xmlHandler.generateWifiStatusXMLRequest(requestVO);
            
            logger.debug("SupplementaryServiceInquiryBO >getWifiRoamingStatus > generate xml message for Customer ["+requestVO.getMsisdn()+"] > "+xmlMessage);
            
            MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
            SupplementaryServiceInquieyDAO supplementaryServiceInquieyDAO = daoFactory.getSupplementaryServiceInquieyDAO();
            xmlReply = supplementaryServiceInquieyDAO.getWifiServiceInquiry(xmlMessage);

            logger.debug("SupplementaryServiceInquiryBO >getWifiRoamingStatus > parsing the reply message for Customer ["+requestVO.getMsisdn()+"] > "+xmlReply);
            
            //parsing the xml reply
            replyObj = xmlHandler.ParsingWifiRoamingServiceInquiryMQReply(xmlReply);

        return replyObj.getWifiServiceType();    
    }
}
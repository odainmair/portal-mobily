package sa.com.mobily.eportal.common.service.business;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.ePortalDBDaoIfc;
import sa.com.mobily.eportal.common.service.dao.imp.ePortalDBDao;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.InactiveObject;
import sa.com.mobily.eportal.common.service.vo.SkyUserAccountVO;
import sa.com.mobily.eportal.common.service.vo.UserAccountVO;
import sa.com.mobily.eportal.common.service.vo.UserRelatedNoVO;


/**
 * 
 * @author n.gundluru.mit
 *
 */

public class ePortalDBBO {

	private static final Logger log = LoggerInterface.log;
	
	private List resetPortletRequiredPages = null;
	
	/**
	 * @return
	 * @throws SystemException
	 */
	public Hashtable getArabicProfile() throws SystemException {

		log.debug("PackageConversionBO > getArabicProfile > Called");
		
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();

		return ePortalIfc.getArabicProfile();
	}

	
	
	public boolean isWhiteListedUser(String msisdn) {

		log.debug("ePortalDBBO > isWhiteListedUser > Called");
		boolean status = false;
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();

		try {
			status=  ePortalIfc.isWhiteListUsers(msisdn);
		} catch (MobilyCommonException e) {
			// TODO Auto-generated catch block
		
		}
		return status;
	}

	/**
	 * 
	 * @param objUserAccountVO
	 * @return
	 * @throws SystemException
	 */
	public boolean updateActivatedUserAccount(UserAccountVO objUserAccountVO) throws SystemException {

		log.debug("PackageConversionBO > updateActivatedUserAccount > Called");
		
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();

		return ePortalIfc.updateActivatedUserAccount(objUserAccountVO);
	}

	/**
	 * 
	 * @param msisdn
	 * @return
	 * @throws SystemException
	 */
	public UserAccountVO getUserEportalDBWebProfile(String msisdn) throws SystemException {

		log.debug("PackageConversionBO > getUserEportalDBWebProfile > Called");
		
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();

		return ePortalIfc.getUserAccountWebProfile(msisdn);
	}
	
	/**
	 * 
	 * @param msisdn
	 * @return
	 * @throws SystemException
	 */
	public boolean isWebProfileUpdated(String msisdn) throws SystemException {

		log.debug("PackageConversionBO > isWebProfileUpdated > Called");
		
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();

		return ePortalIfc.isWebProfileUpdated(msisdn);
	}

	/**
	 * 
	 * @return
	 * @throws SystemException
	 */
	public HashMap getChargeServiceAmount() throws SystemException {

		log.debug("PackageConversionBO > getChargeServiceAmount > Called");
		
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();

		return ePortalIfc.getChargeServiceAmount();
	}
	
	/**
	 * @return
	 * @throws SystemException
	 */
	public String fetchPOIDIdforMsisdn(String msisdn, String packType) throws SystemException {

		log.debug("ePortalDBBO > fetchPOIDIdforMsisdn > Called");
		
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();

		return ePortalIfc.fetchPOIDIdforMsisdn(msisdn,packType);
	}
	
	/**
	 * @return
	 * @throws SystemException
	 */
	public String fetchPOIDIdforMsisdnfromDB(String msisdn) throws SystemException {

		log.debug("ePortalDBBO > fetchPOIDIdforMsisdnfromDB > Called");
		
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();

		return ePortalIfc.fetchPOIDIdforMsisdnfromDB(msisdn);
	}
	
	/**
	 * @return
	 * @throws SystemException
	 */
	public void updatePOIDForMsisdninDB(String msisdn, String poid) throws SystemException {

		log.debug("ePortalDBBO > updatePOIDIdforMsisdnfromDB > Called");
		
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();

		ePortalIfc.updatePOIDForMsisdninDB(msisdn, poid);
	}
	/**
	 * @return
	 * @throws SystemException
	 */
	public void insertPOIDForMsisdninDB(String msisdn) throws SystemException {

		log.debug("ePortalDBBO > insertPOIDIdforMsisdnfromDB > Called");
		
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();

		ePortalIfc.updatePOIDList(msisdn);
	}
	
	/**
	 * This method is reposnsbile for reurn the id related to the customer at user_account table
	 * @param input can be msisdn / service account number
	 * */

	public int getUserAccountID(String msisdn) throws Exception {

		log.debug("ePortalDBBO > getUserAccountID > Called");
		
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();

		return ePortalIfc.getUserAccountID(msisdn);
	}
	
	
	/**
	 * This service used to check is the account number need to added by the customer already exist under the same customer id or not
	 * @param
	 * */
	public boolean isUserRelatedNoExist(UserRelatedNoVO relatedVO) throws SystemException {
		log.debug("ePortalDBBO > isUserRelatedNoExist > Called");
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();

		return ePortalIfc.isUserRelatedNoExist(relatedVO);
	}
	
	/**
	 * @return id
	 * @throws SystemException
	 */
	public int getUserAccountIDFromManagedTbl(String msisdn) throws Exception {

		log.debug("ePortalDBBO > getUserAccountIDFromManagedTbl > Called");
		
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		return ePortalIfc.getUserAccountIDFromManagedTbl(msisdn);
	}
	
	/**
	 * @return
	 * @throws SystemException
	 */
	public void updatePOIDListforRegistration(String msisdn, String packType) throws Exception {

		log.debug("ePortalDBBO > updatePOIDListforRegistration > Called");
		
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		ePortalIfc.updatePOIDListforRegistration(msisdn,packType);
	}
	/**
	 * @return
	 * @throws SystemException
	 */
	public int updatePOIDforCCM(String msisdn, String packType) throws Exception {

		log.debug("ePortalDBBO > updatePOIDforCCM > Called");
		
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		return ePortalIfc.updatePOIDforCCM(msisdn,packType);
	}
	
	public String getServiceAccount(String msisdn) throws SystemException{
		log.debug("ePortalDBBO > getServiceAccount > Called");
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		return ePortalIfc.getServiceAccount(msisdn);
 
	}
	
	
	/**
	 * This method is user to insert a username upon creation / deletion from eportal system
	 * so the DR batch can use these information to syncronzie the users between production and DR
	 * @param  username
	 * @param  action (1= create / 2 = delete)
	 * */
	public static void UpdateUserInDR(String username , int action){

		log.debug("ePortalDBBO > UpdateUserInDR > Called");
		
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		ePortalIfc.UpdateUserInDR(username, action);
	}
	
	/**
	 * this is will be a common activation module where it will insert the activation code based on the project id
	 * @param String accountNumber
	 * @param String activation Code
	 * @param int project id [SR_PROJECT_TBL]
	 * @return void
	 * */
	public static void setActivationCode(String accountNumber , String activationCode , int projectId) throws SystemException{
		log.debug("ePortalDBBO > setActivationCode > Called");
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		ePortalIfc.setActivationCode(accountNumber, activationCode, projectId);
	}
	
	/**
	 * this is will be a common activation module where it will reponsible for retrivig the activation code based on MSISDN / Project ID
	 * @param String accountNumber
	 * @param int project id [SR_PROJECT_TBL]
	 * @return String activation code
	 * */
	public static String getActivationCode(String accountNumber ,  int projectId) throws SystemException{
			log.debug("ePortalDBBO > getActivationCode > Called");
			OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
			ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
			return ePortalIfc.getActivationCode(accountNumber, projectId);		
	}

	
	/**
	 * this methed is responsible for adding related number into the customer account
	 * @param UserAccountVO(id , msisdn , username , service account number , line type)
	 * @throws Exception
	 * @throws SQLException
	 */
	public void addingNewRelatedNumber(UserAccountVO userAccount) throws SystemException{
			log.debug("ePortalDBBO > addingNewRelatedNumber ["+userAccount.getMsisdn()+"] > Called");
			OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
			ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
			ePortalIfc.addingNewRelatedNumber(userAccount);		
	}
	
	public Date getLastLoginTime(String msisdn) throws SystemException {
		log.debug("ePortalDBBO > getLastLoginTime for msisdn ["+msisdn+"] > Called");
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		return ePortalIfc.getLastLoginTime(msisdn);
	}
	
	public void updateLastLoginTime(String msisdn) throws SystemException {
		log.debug("ePortalDBBO > updateLastLoginTime for msisdn ["+msisdn+"] > Called");
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		ePortalIfc.updateLastLoginTime(msisdn);		
	}
	
	public SkyUserAccountVO getSkyType( String aMsisdn ) throws SystemException {
		log.debug("ePortalDBBO > getSkyType for msisdn ["+aMsisdn+"] > Called");
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		return ePortalIfc.getSkyType(aMsisdn);				
	}
	
	public Date getLastLoginTimeCorp(String billingAccountNumber,String userName) throws SystemException {
		log.debug("ePortalDBBO > getLastLoginTimeCorp for billingAccountNumber ["+billingAccountNumber+"] > Called");
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		return ePortalIfc.getLastLoginTimeCorp(billingAccountNumber,userName);
	}
	
	public void updateLastLoginTimeCorp(String userName) throws SystemException {
		log.debug("ePortalDBBO > updateLastLoginTimeCorp for userName ["+userName+"] > Called");
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		ePortalIfc.updateLastLoginTimeCorp(userName);		
	}
	
	/**
	 * @date: Mar 8, 2012
	 * @author: s.vathsavai.mit
	 * @description: Responsibel to get the list of portal page unique names from ePortal DB.
	 * @return List
	 * @throws SystemException
	 */
	public List getResetPortletRequiredPages() throws SystemException {
		log.info("ePortalDBBO > getResetPortletRequiredPages : start");
		if(resetPortletRequiredPages == null) {
			OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
			ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
			resetPortletRequiredPages = ePortalIfc.getResetPortletrequiredPages();
		}
		log.info("ePortalDBBO > getResetPortletRequiredPages : end");
		return resetPortletRequiredPages;
	}
	
	public void auditInactiveAccount(InactiveObject inactiveObject) throws SystemException {
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		ePortalIfc.auditInactiveAccount(inactiveObject);		
	}
	
	/**
	 * check if the power user (user created for specifc team like sales , call center empowerment) updated his password after first login or not
	 * 
	 * @param username
	 * @return boolean
	 */
	public boolean isPowerUsr_UpdatedHisPassword(String userName) {
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		return ePortalIfc.isPowerUsr_UpdatedHisPassword(userName);		
	}
	/**
	 * insert a record in the table to know later is the power user customer updated his password or not
	 * 
	 * @param username
	 * @return nothing
	 * 
	 */
	public void updatePowerUsrPassword(String userName ){
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		ePortalIfc.updatePowerUsrPassword(userName);		
	}

	/**
	 * 
	 * @param id
	 * @param msisdn
	 * @return
	 */
	public UserAccountVO getUserAccountInfoFromManageTable(int id, String msisdn)	{
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		
		return ePortalIfc.getUserAccountInfoFromManageTable(id, msisdn);
	}

	/**
	 * 
	 * @param id
	 * @param msisdn
	 * @return
	 */
	public boolean updateLineTypeInUserManagedNo(int id, String msisdn, int lineType) {
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		
		return ePortalIfc.updateLineTypeInUserManagedNo(id, msisdn, lineType);
	}

	/**
	 * 
	 * @param id
	 * @param msisdn
	 * @return
	 */
	public boolean updateLineTypeInUserAccount(String msisdn, int lineType)	{
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ePortalDBDaoIfc ePortalIfc = (ePortalDBDao)daoFactory.getePortalDBDao();
		
		return ePortalIfc.updateLineTypeInUserAccount(msisdn, lineType);
	}

}
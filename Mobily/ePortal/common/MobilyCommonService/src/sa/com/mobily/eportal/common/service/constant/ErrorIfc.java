/*
 * Created on Feb 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.constant;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ErrorIfc {
    public static final String NUMBER_NOT_FOUND_ECEPTION = "1008";
    public static final String EXCEPTION_XML_ERROR = "1006";
    public static final String EXCEPTION_CONNECTION_ERROR = "1001";
    public static final String EXCEPTION_DBCONNECTION_ERROR = "1004";
    public static final String EXCEPTION_MQ_EXCEPTION = "1011";
    
    public static final String MEDIA_CONTACT_NOT_FOUND_ECEPTION = "1020";
    public static final int EXCEPTION_BILL_PDF_FILE_NOT_FOUND      = 1017;
    public static final int EXCEPTION_BILL_NOT_FOUND               = 1016;
    public static final int EXCEPTION_NO_CALLED_TO_LIST_FOUND      = 1015;
    public static final int EXCEPTION_NO_CALL_DETAILS_FOUND        = 1014;
   
    public static final int EXCEPTION_INVALID_INPUT                = 1012;
    public static final int EXCEPTION_NO_SERVICE_ORDERS_LIST_FOUND = 1013;
    public static final int NO_ERROR                       	       = 0000;
    
    public static final int EXCEPTION_INVALID_REQUEST_MESSAGE      = 1002;
    public static final int EXCEPTION_NO_RECORD_FOUND 		       = 1003;
    
    public static final int EXCEPTION_UNKNOW_ERROR 			       = 1005;
   
    public static final int EXCEPTION_SYSTEM_ERROR 		           = 1007;
    public static final int EXCEPTION_NUMBER_NOT_FOUND		       = 1008;    
    public static final int EXCEPTION_MSISDN_NOT_FOUND_ERROR   	   = 1009;
    public static final int EXCEPTION_NO_BILL_LIST_FOUND_ERROR 	   = 1010;
    
    //Loyalty : Neqaty Plus
    public static final String CUSTOMER_NOT_SUBSCRIBED_TO_NEQATY_PLUS = "10329";
	public static final String ROOT_ACCOUNT_NUMBER_NOT_AVAILABLE_IN_LOYALTY = "10325";
	
	//MDM: validate custoemr id
	public static final String MDM_ERROR_CODE_MSISDN_CUST_ID_NOT_MATCH = "E601017";
	public static final String MDM_ERROR_CODE_MSISDN_NOT_EXIST = "E601024";
	public static final String MDM_ERROR_CODE_TIME_OUT_WITH_BACKEND = "E000214";
	public static final String MDM_ERROR_CODE_INVALID_REQUEST = "E000212";
}

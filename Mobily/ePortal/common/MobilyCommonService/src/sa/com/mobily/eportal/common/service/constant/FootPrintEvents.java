package sa.com.mobily.eportal.common.service.constant;

public enum FootPrintEvents{
	MY_CONTACTS_ADD_CONTACT("My Contacts, Add Contact"),
	MY_CONTACTS_UPDATE_CONTACT("My Contacts, Updtae Contact"),
	MY_CONTACTS_DELETE_CONTACTS("My Contacts, Delete Contacts"),
	MY_CONTACTS_GET_ACCOUNT_BY_NAME("My Contacts, Get Account By Name"),
	MY_CONTACTS_GET_ACCOUNT_BY_ID("My Contacts, Get Account By Id"),
	FORGET_ACCOUNT_GET_ACCOUNT_TYPE("Forget Account, Get Account Type"),
	FORGET_ACCOUNT_RESET_PASSWORD("Forget Account, Reset Password"),
	FORGET_ACCOUNT_RECOVER_USER_NAME("Forget Account, Recover User Name"),
	MY_PROFILE_GET_PROFILE_BY_MSISDN("My Profile, Get User Profile By MSISDN"),
	MY_PROFILE_GET_PROFILE_BY_ACCT_NUMBER("My Profile, Get User Profile By Account Number"),
	MY_PROFILE_GET_NON_MOBILY_PROFILE("My Profile, Get Non Mobily User Profile"),
	MY_PROFILE_UPDATE_NON_MOBILY_PROFILE("My Profile, Updtae Non Mobily User Profile"),
	MY_PROFILE_UPDATE_MOBILY_PROFILE("My Profile, Update Mobily User Profile"),
	MY_PROFILE_UPDATE_LOGIN_INFO("My Profile, Update Login Info"),
	MY_PROFILE_RESET_PASSWORD("My Profile, Reset Password"),
	USER_MANAGEMENT_USER_REGISTRATION("User Management, User Registration"),
	CREDIT_CARD_MGMT_GET_CREDIT_CARD_LIST("Credit Card Management, Get credit cards list"),
	CREDIT_CARD_MGMT_CREATE_CARD("Credit Card Management, Create credit card"),
	CREDIT_CARD_MGMT_DELETE_CARD("Credit Card Management, Delete credit card"),
	CREDIT_CARD_MGMT_UPDATE_CARD("Credit Card Management, Updtae credit card"),
	CREDIT_CARD_MGMT_SEND_PIN_CODE("Credit Card Management, Send PIN code"),
	CREDIT_CARD_MGMT_VERIFY_CARD("Credit Card Management, Verify credit card"),
	;
	
	
	private final String text;
	
	FootPrintEvents(String text){
		this.text = text;
	}
	
	@Override
    public String toString() {
        return text;
    }
}

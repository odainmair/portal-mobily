package sa.com.mobily.eportal.common.service.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.common.service.vo.AuthenticateGoActivationVO;

/**
 * This DAO class is responsible for DB operations on CONTACTS table
 * <p> 
 * it contains methods for retrieve, add, delete and update.
 *  
 * @author Yousef Alkhalaileh
 */
public class AuthenticateGoActivationDAO extends AbstractDBDAO<AuthenticateGoActivationVO>{
	
	public static final String FIND_BY_MSISDN_AND_MODULE_ID = "SELECT MODULE_ID, MSISDN, ACTIVATION_CODE, EXPIRATION_DATE, PIN_STATUS FROM EEDBUSR.AUTHENTICATE_GO_ACTIVATION_TBL WHERE MODULE_ID=? AND MSISDN=?";
	
	public static final String FIND_BY_MSISDN = "SELECT MODULE_ID, MSISDN, ACTIVATION_CODE, EXPIRATION_DATE, PIN_STATUS FROM EEDBUSR.AUTHENTICATE_GO_ACTIVATION_TBL WHERE MSISDN=?";
	
	public static final String UPDATE_AUTH_AND_GO = "UPDATE EEDBUSR.AUTHENTICATE_GO_ACTIVATION_TBL SET ACTIVATION_CODE=? WHERE MODULE_ID=? and MSISDN=?";
	
	public static final String INSERT_AUTH_AND_GO = "INSERT INTO EEDBUSR.AUTHENTICATE_GO_ACTIVATION_TBL(MODULE_ID, MSISDN, ACTIVATION_CODE, EXPIRATION_DATE, PIN_STATUS) VALUES(?, ?, ?, ?, ?)";
	
	private static AuthenticateGoActivationDAO instance = new AuthenticateGoActivationDAO();
	
	protected AuthenticateGoActivationDAO() {
		super(DataSources.DEFAULT);
	}
	
	/**
	 * This method returns a singleton instance from the DAO class
	 * 
	 * @return <code>MyContactsDAO</code>
	 * @author Yousef Alkhalaileh
	 */
	public static AuthenticateGoActivationDAO getInstance() {
		return instance;
	}
	
	public AuthenticateGoActivationVO findByMSISDNAndModuleId(int moduleId, String msisdn){
		List<AuthenticateGoActivationVO> list = query(FIND_BY_MSISDN_AND_MODULE_ID, moduleId, msisdn);
		if(list != null && list.size() > 0)
			return list.get(0);
		return null;
	}
	
	public AuthenticateGoActivationVO findByMSISDN(String msisdn){
		List<AuthenticateGoActivationVO> list = query(FIND_BY_MSISDN, msisdn);
		if(list != null && list.size() > 0)
			return list.get(0);
		return null;
	}
	
	public int updateByMSISDNAndModuleId(AuthenticateGoActivationVO authenticateGoActivationVO){
		// commented by MHASSAN
		//return update(FIND_BY_MSISDN_AND_MODULE_ID, authenticateGoActivationVO.getActivationCode(), authenticateGoActivationVO.getModuleId(), authenticateGoActivationVO.getMsisdn());
		// added by MHASSAN 
		// how to send 3 parameters to query accept only 2 parameters
		//return update(FIND_BY_MSISDN_AND_MODULE_ID,authenticateGoActivationVO.getModuleId(), authenticateGoActivationVO.getMsisdn());
		
		return update(UPDATE_AUTH_AND_GO,authenticateGoActivationVO.getActivationCode(),authenticateGoActivationVO.getModuleId(), authenticateGoActivationVO.getMsisdn());
		
	}
	
	public int insertByMSISDNAndModuleId(AuthenticateGoActivationVO authenticateGoActivationVO){
		return update(INSERT_AUTH_AND_GO, authenticateGoActivationVO.getModuleId(), authenticateGoActivationVO.getMsisdn(), authenticateGoActivationVO.getActivationCode(), authenticateGoActivationVO.getExpirationDate(), authenticateGoActivationVO.getPinStatus());
	}
	
	/**
	 * This method returns a list contacts for a specific account id
	 * 
	 * @param request <code>Long id</code> account id
	 * @return <code>MyContactsVO</code> a list of contacts
	 * @author Yousef Alkhalaileh
	 */
	@Override
	protected AuthenticateGoActivationVO mapDTO(ResultSet rs) throws SQLException {
		AuthenticateGoActivationVO authenticateGoActivationVO = new AuthenticateGoActivationVO();
		authenticateGoActivationVO.setModuleId(rs.getInt("MODULE_ID"));
		authenticateGoActivationVO.setMsisdn(rs.getString("MSISDN"));
		authenticateGoActivationVO.setPinStatus(rs.getInt("PIN_STATUS"));
		authenticateGoActivationVO.setActivationCode(rs.getString("ACTIVATION_CODE"));
		authenticateGoActivationVO.setExpirationDate(rs.getDate("EXPIRATION_DATE"));
		return authenticateGoActivationVO;
	}
}

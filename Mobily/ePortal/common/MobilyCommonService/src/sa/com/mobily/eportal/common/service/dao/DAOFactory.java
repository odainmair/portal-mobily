/*
 * Created on Feb 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao;

import sa.com.mobily.eportal.common.service.dao.ifc.AccountFilterServiceDAO;


import sa.com.mobily.eportal.common.service.dao.ifc.AuthenticateAndGoDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.ConnectOTSDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.ContactUSCacheDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerPackageDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerProfileDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerStatusDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerTypeDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.EBillDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.FavoriteNumberInquiryDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.FootPrintTrackingDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.IPTVInqDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.IPhoneDeviceInfoDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.IPhoneServiceDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.InternationalBarringInquiryDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.LoyaltyDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.MCRDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.MCRRelatedNumDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.ManageCreditCardDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.PackageConversionDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.PandaDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.PaymentotifyDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.PhoneBookDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.PromotionInquiryDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.RoyalGuardCommonDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleCreditTransferDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleLookupTableDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleSohoAddOnTableDAO;



/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public abstract class DAOFactory {
    public static final int ORACLE 		= 1;
	public static final int MQ 			= 2;

	
	/**
	 * @param factory
	 * @return
	 * Feb 17, 2008
	 * DAOFactory.java
	 * msayed
	 */
	public static DAOFactory getDAOFactory(int factory){
		switch(factory){
		 case 1:
		 	  return new OracleDAOFactory();
		 case 2:
		 	return new MQDAOFactory();

		 default: 
		 	return null;
		}
      
	}
	
public abstract CustomerTypeDAO getCustomerTypeDAO();
	
	public abstract CustomerStatusDAO getCustomerStatusDAO();
	
	public abstract CustomerProfileDAO getCustomerProfileDAO();
	
	public abstract InternationalBarringInquiryDAO getInternationalBarringInquiryDAO();
	
	public abstract PromotionInquiryDAO getPromotionInquiryDAO();
	
	public abstract FavoriteNumberInquiryDAO getFavoriteNumberInquiryDAO();
	
	public abstract CustomerPackageDAO getCustomerPackageDAO();
	
	public abstract OracleLookupTableDAO getLookupTableDAO();
	
	public abstract ContactUSCacheDAO getContactUSCacheDAO();

	public abstract PhoneBookDAO getPhoneBookDAO();
	
	public abstract AccountFilterServiceDAO getCustomerServiceDetails();
	
	public abstract EBillDAO getMQEBillDAO();
	
	public abstract AuthenticateAndGoDAO getAuthenticateAndGoDAO();
	
	public abstract PandaDAO getMQPandaDAO();
	
	public abstract ManageCreditCardDAO getManageCreditCardDAO();

	public abstract PackageConversionDAO getOraclePackageConversionDAO(); 

	public abstract PackageConversionDAO getMQPackageConversionDAO();
	
	public abstract IPhoneServiceDAO getOracleIPhoneServiceDAO();
	
	public abstract RoyalGuardCommonDAO getMQRoyalGuardCommonDAO();
	
	public abstract FootPrintTrackingDAO getOracleFootPrintTrackingDAO();
	
	public abstract MCRDAO getOracleMCRDAO();
	
	public abstract ConnectOTSDAO getConnectOTSDAO();
	
	public abstract MCRRelatedNumDAO getMCRRelatedNumDAO();
	
	public abstract LoyaltyDAO getLoyaltyDAO();
	
	public abstract OracleSohoAddOnTableDAO getSohoAddOnTableDAO();
	
	public abstract IPhoneDeviceInfoDAO getIPhoneDeviceInfoDAO();
	
	public abstract OracleCreditTransferDAO getCreditTransferDAO();
	
	public abstract IPTVInqDAO getIPTVInqDAO();
	
	public abstract PaymentotifyDAO getPaymentotifyDAO();
	
}
/*
 * Created on Feb 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao;

import sa.com.mobily.eportal.common.service.dao.ifc.AccountFilterServiceDAO;

import sa.com.mobily.eportal.common.service.dao.ifc.AuthenticateAndGoDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.ConnectOTSDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.ContactUSCacheDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.CorporateStrategicTrackDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerPackageDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerProfileDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerStatusDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerTypeDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.EBillDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.FavoriteNumberInquiryDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.FootPrintTrackingDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.IPTVInqDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.IPhoneDeviceInfoDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.IPhoneServiceDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.InternationalBarringInquiryDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.LoyaltyDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.MCRDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.MCRRelatedNumDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.ManageCreditCardDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.PackageConversionDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.PandaDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.PaymentotifyDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.PhoneBookDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.PromotionInquiryDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.RoyalGuardCommonDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.SMSDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.SupplementaryServiceInquieyDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQAccountFilterServiceImpDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQConnectOTSDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQCorporateStrategicTrackDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQCoverageDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQCustomerProfileDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQCustomerStatusDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQCustomerTypeDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQEBillDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQFavoriteNumberInquiryDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQIPTVInqDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQLoyaltyDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQMCRRelatedNumDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQManageCreditCardDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQPackageConversionDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQPandaDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQPaymentNotifyDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQPromotionInquiryDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQRoyalGuardCommonDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQSMSDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQSupplementaryServiceInquieyDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleCreditTransferDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleLookupTableDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleSohoAddOnTableDAO;





/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MQDAOFactory extends DAOFactory {   
 
    public  CustomerTypeDAO getCustomerTypeDAO(){
		return new MQCustomerTypeDAO();
	}

    /* (non-Javadoc)
     * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getCustomerStatusDAO()
     */
    public CustomerStatusDAO getCustomerStatusDAO() {
        // TODO Auto-generated method stub
        return new MQCustomerStatusDAO();
    }

    /* (non-Javadoc)
     * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getCustomerProfileDAO()
     */
    public CustomerProfileDAO getCustomerProfileDAO() {
        // TODO Auto-generated method stub
        return null;
    }
    
    /* (non-Javadoc)
     * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getSupplementaryServiceInquieyDAO()
     */
    public SupplementaryServiceInquieyDAO getSupplementaryServiceInquieyDAO() {
        // TODO Auto-generated method stub
        return new MQSupplementaryServiceInquieyDAO();
    }
    
    /* (non-Javadoc)
     * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getInternationalBarringInquiryDAO()
     */
    public InternationalBarringInquiryDAO getInternationalBarringInquiryDAO() {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getPromotionInquiryDAO()
     */
    public PromotionInquiryDAO getPromotionInquiryDAO() {
        // TODO Auto-generated method stub
        return new MQPromotionInquiryDAO();
    }

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getFavoriteNumberInquiryDAO()
	 */
	public FavoriteNumberInquiryDAO getFavoriteNumberInquiryDAO() {
		// TODO Auto-generated method stub
		return new MQFavoriteNumberInquiryDAO();
	}

//	public  CustomerStatusDAO getCustomerStatusDAO(){
//		return new MQCustomerStatusDAO();
//	}
	public  CustomerPackageDAO getCustomerPackageDAO(){
		return  null;
	}

    /* (non-Javadoc)
     * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getLookupTableDAO()
     */
    public OracleLookupTableDAO getLookupTableDAO() {
        // TODO Auto-generated method stub
        return null;
    }

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getContactUSCacheDAO()
	 */
	public ContactUSCacheDAO getContactUSCacheDAO() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public SMSDAO getSMSDAO()
    {
    	  return new MQSMSDAO();
    }

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getPhoneBookDAO()
	 */
	public PhoneBookDAO getPhoneBookDAO() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public AccountFilterServiceDAO getCustomerServiceDetails() {
		// TODO Auto-generated method stub
		return new MQAccountFilterServiceImpDAO();
	}
	
	public EBillDAO getMQEBillDAO() {
		// TODO Auto-generated method stub
		return new MQEBillDAO();
	}

	@Override
	public AuthenticateAndGoDAO getAuthenticateAndGoDAO() {
		// TODO Auto-generated method stub
		return null;
	}
	
    public PandaDAO getMQPandaDAO() {
        // TODO Auto-generated method stub
        return new MQPandaDAO();
    }

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getOraclePandaDAO()
	 */
	public PandaDAO getOraclePandaDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getManageCreditCardDAO()
	 */
	public ManageCreditCardDAO getManageCreditCardDAO() {
		// TODO Auto-generated method stub
		return new MQManageCreditCardDAO();
	}

	@Override
	public PackageConversionDAO getOraclePackageConversionDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PackageConversionDAO getMQPackageConversionDAO() {
		// TODO Auto-generated method stub
		return new MQPackageConversionDAO();
	}

	 public CorporateStrategicTrackDAO getCorporateStrategicTrackDAO() {
	        // TODO Auto-generated method stub
	        return new MQCorporateStrategicTrackDAO();
	  }

	@Override
	public IPhoneServiceDAO getOracleIPhoneServiceDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RoyalGuardCommonDAO getMQRoyalGuardCommonDAO() {
		// TODO Auto-generated method stub
		return new MQRoyalGuardCommonDAO();
	}

	@Override
	public FootPrintTrackingDAO getOracleFootPrintTrackingDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MCRDAO getOracleMCRDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getFavoriteNumberInquiryDAO()
	 */
	public ConnectOTSDAO getConnectOTSDAO() {
		// TODO Auto-generated method stub
		return new MQConnectOTSDAO();
	}

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getFavoriteNumberInquiryDAO()
	 */
	public MCRRelatedNumDAO getMCRRelatedNumDAO() {
		// TODO Auto-generated method stub
		return new MQMCRRelatedNumDAO();
	}

	/*
	 * 
	 */
	public MQCoverageDAO getMQCoverageDAO() {
		// TODO Auto-generated method stub
		return new MQCoverageDAO();
	}
	
	public CustomerProfileDAO getMQCustomerProfileDAO() {
		// TODO Auto-generated method stub
		return new MQCustomerProfileDAO();
	}

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getLoyaltyDAO()
	 */
	@Override
	public LoyaltyDAO getLoyaltyDAO() {
		// TODO Auto-generated method stub
		return new MQLoyaltyDAO();
	}
	
	public  OracleSohoAddOnTableDAO getSohoAddOnTableDAO(){
		return null;
	}

	@Override
	public IPhoneDeviceInfoDAO getIPhoneDeviceInfoDAO() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public OracleCreditTransferDAO getCreditTransferDAO(){
		return null;
	}
	
	@Override
	public IPTVInqDAO getIPTVInqDAO() {
		// TODO Auto-generated method stub
		return new MQIPTVInqDAO(); 
	}

	@Override
	public PaymentotifyDAO getPaymentotifyDAO() {
		// TODO Auto-generated method stub
		return new MQPaymentNotifyDAO();
	}
}
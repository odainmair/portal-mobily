package sa.com.mobily.eportal.common.service.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.dao.constants.DAOConstants;
import sa.com.mobily.eportal.common.service.model.UserRegistrationAudit;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
/**
 * This DAO class is responsible for DB operations on SR_REGISTRATION_AUDIT_TBL table
 * <p> 
 * it contains methods for retrieve & add 
 *  
 * @author Nagendra B. Gundluru - MIT
 */

public class UserRegistrationAuditDAO extends AbstractDBDAO<UserRegistrationAudit> {

	private static UserRegistrationAuditDAO instance = new UserRegistrationAuditDAO();
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	private static String clazz = UserRegistrationAuditDAO.class.getName();

	private static final String URA_ADD = "INSERT INTO SR_REGISTRATION_AUDIT_TBL (USER_NAME, ACCOUNT_NUMBER, EMAIL_ID, FACEBOOK_ID,"
			+ "TWITTER_ID, CREATED_DATE, REGISTRATION_SOURCE_ID, ERROR_CODE, ERROR_MESSAGE, ELAPSED_TIME) "
			+ "VALUES(?, ?, ?, ?, ?, CURRENT_TIMESTAMP, ?, ?, ?, ?) ";
	
	private static final String URA_QUERY_BY_USERNAME = "SELECT USER_NAME, ACCOUNT_NUMBER, EMAIL_ID, FACEBOOK_ID, " +
			" TWITTER_ID, CREATED_DATE, REGISTRATION_SOURCE_ID, ERROR_CODE, ERROR_MESSAGE" +
			"  FROM SR_REGISTRATION_AUDIT_TBL  WHERE UPPER(USER_NAME) =?";
	
	
	protected UserRegistrationAuditDAO() {
		super(DataSources.DEFAULT);
	}

	/**
	 * This method returns a singleton instance from the DAO class
	 * 
	 * @return <code>UserRegistrationAuditDAO</code>
	 * 
	 * @author Nagendra B. Gundluru - MIT
	 */
	public static synchronized UserRegistrationAuditDAO getInstance() {
		return instance;
	}
	
	
	/**
	 * This method returns a list UserAccount for a specific user name
	 * 
	 * @param request <code>String</code> User name
	 * @return <code>ArrayList<UserAccount></code> a list of UserAccount
	 * 
	 * @author Nagendra B. Gundluru - MIT
	 */
	public List<UserRegistrationAudit> findByUserName(String userName) {
		return query(URA_QUERY_BY_USERNAME, new Object[] { userName.toUpperCase() });
	}
	
	
	
	/**
	 * This method saves UserRegistrationAudit for a specific account in the SR_REGISTRATION_AUDIT_TBL  table
	 * 
	 * @param request <code>UserRegistrationAudit</code> user account to be saved
	 * @return <code>int</code> number of records affected (in case successful execution 1)
	 * 
	 * @author Nagendra B. Gundluru - MIT
	 */
	public int saveUserRegisterAuditInfo(UserRegistrationAudit userRegAudit) {

		 return update(URA_ADD, userRegAudit.getUserName().toLowerCase(), userRegAudit.getAccountNumber(), userRegAudit.getEmail(), 
				 userRegAudit.getFacebookId(), userRegAudit.getTwitterId(), new Integer(userRegAudit.getRegistrationSrcId()), 
				 userRegAudit.getErrorCode(), userRegAudit.getErrorMessage(), userRegAudit.getExecutionTime());
	}

	
	/**
	 * This method returns a UserRegistrationAudit from DB
	 * 
	 * @param request <code>ResultSet</code> from DB
	 * @return <code>UserRegistrationAudit</code> mapped user account
	 * 
	 * @author Nagendra B. Gundluru - MIT
	 */
	@Override
	protected UserRegistrationAudit mapDTO(ResultSet rs) throws SQLException
	{
		UserRegistrationAudit userRegAuditObj = new UserRegistrationAudit();

			userRegAuditObj.setUserName(rs.getString(DAOConstants.UserRegistrationAudit.USER_NAME));
			userRegAuditObj.setAccountNumber(rs.getString(DAOConstants.UserRegistrationAudit.ACCOUNT_NUMBER));
			userRegAuditObj.setEmail(rs.getString(DAOConstants.UserRegistrationAudit.EMAIL_ID));
			userRegAuditObj.setFacebookId(rs.getString(DAOConstants.UserRegistrationAudit.FACEBOOK_ID));
			userRegAuditObj.setTwitterId(rs.getString(DAOConstants.UserRegistrationAudit.TWITTER_ID));
			userRegAuditObj.setCreatedDate(rs.getTimestamp(DAOConstants.UserRegistrationAudit.CREATED_DATE));
			userRegAuditObj.setRegistrationSrcId(rs.getInt(DAOConstants.UserRegistrationAudit.REGISTRATION_SOURCE_ID));
			userRegAuditObj.setErrorCode(rs.getString(DAOConstants.UserRegistrationAudit.ERROR_CODE));
			userRegAuditObj.setErrorMessage(rs.getString(DAOConstants.UserRegistrationAudit.ERROR_MESSAGE));
			

		LogEntryVO logEntryVO = new LogEntryVO(Level.INFO, "userRegAuditObj dto >>>>" + userRegAuditObj.toString(), clazz, "mapDTO", null);
		executionContext.audit(logEntryVO);

		return userRegAuditObj;
	}
}
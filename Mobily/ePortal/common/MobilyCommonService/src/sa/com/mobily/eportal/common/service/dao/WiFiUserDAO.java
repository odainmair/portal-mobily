/**
 * WiFiUserDAO.java 
 * v.ravipati.mit
 * 07-Jul-2015 - 3:20:00 PM
 */
package sa.com.mobily.eportal.common.service.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.common.service.vo.WiFiUserVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

/**
 * @version : 1.0
 * @author  : Venkat Rao Ravipati - MIT
 * @date    : 07-Jul-2015
 * @time    : 3:20:00 PM
 * @extends :
 * @implements :
 */
public class WiFiUserDAO extends AbstractDBDAO<WiFiUserVO>{

	private static WiFiUserDAO instance = new WiFiUserDAO();
	
	protected WiFiUserDAO() {
		super(DataSources.DEFAULT);
	}
	
	public static WiFiUserDAO getInstance() {
		return instance;
	}
	
	
	String FIND_USER_DETAILS_BY_USERNAME = "SELECT USER_NAME,USER_TYPE,MSISDN,CREATED_DATE,LASTLOGINTIME,USER_STATUS,ACTIVATION_DATE,DUMMY_MSISDN FROM WIFI_USER_ACCOUNT WHERE USER_NAME=?";
	
	String FIND_USER_DETAILS_BY_MSISDN = "SELECT USER_NAME,USER_TYPE,MSISDN,CREATED_DATE,LASTLOGINTIME,USER_STATUS,ACTIVATION_DATE,DUMMY_MSISDN FROM WIFI_USER_ACCOUNT WHERE (MSISDN=? OR DUMMY_MSISDN=?)";
	String DELETE_WIFI_USER_BY_MSISDN = "DELETE FROM WIFI_USER_ACCOUNT WHERE (MSISDN=? OR DUMMY_MSISDN=?)";
	
	String DELETE_WIFI_USER_BY_USER_NAME = "DELETE FROM WIFI_USER_ACCOUNT WHERE USER_NAME=?";
	String UPDATE_WIFI_USER = "UPDATE WIFI_USER_ACCOUNT SET MSISDN=? WHERE  MSISDN=?";
	
	public WiFiUserVO findWiFiUserByUserName(String userName){
		List<WiFiUserVO> list = query(FIND_USER_DETAILS_BY_USERNAME, userName);
		return list!=null && list.size() >0 ? list.get(0) : null;
	}
	
	public WiFiUserVO findWiFiUserByMSISDN(String msisdn){
		List<WiFiUserVO> list = query(FIND_USER_DETAILS_BY_MSISDN, msisdn, msisdn);
		return list!=null && list.size() >0 ? list.get(0) : null;
	}
	
	public boolean updateWiFiUser(String newMSISDN, String originalMSISDN ){
		int i= update(UPDATE_WIFI_USER, newMSISDN,originalMSISDN);
		
		return i > 0 ? true : false;
	}
	
	public boolean deleteWiFiUserByUserName(String userName){
		int i= update(DELETE_WIFI_USER_BY_USER_NAME, userName);
		return i > 0 ? true : false;
	}
	
	public boolean deleteWiFiUserByMSISDN(String MSISDN){
		int i= update(DELETE_WIFI_USER_BY_MSISDN, MSISDN, MSISDN);
		return i > 0 ? true : false;
	}
	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.core.dao.AbstractDBDAO#mapDTO(java.sql.ResultSet)
	 */
	@Override
	protected WiFiUserVO mapDTO(ResultSet rs) throws SQLException{
		WiFiUserVO wifiUserVO = new WiFiUserVO();
		wifiUserVO.setUserName(rs.getString("USER_NAME"));
		wifiUserVO.setUserType(rs.getString("USER_TYPE"));
		wifiUserVO.setMSISDN(rs.getString("MSISDN"));
		wifiUserVO.setDummyMSISDN(rs.getString("DUMMY_MSISDN"));
		return wifiUserVO;
	}
}

/*
 * Created on Jun 1, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.ifc;

import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.vo.AuthenticateAndGoVO;

/**
 * @author Suresh V - MIT
 * AuthenticateAndGoDAO
 * Description:
 */
public interface AuthenticateAndGoDAO {


    /**
		date: Jun 1, 2009
		Description: To insert the activation code into Portal DB. This methos will insert the new record into AUTHENTICATE_GO_ACTIVATION_TBL.
	    @param authenticateAndGoVO. Mobily number(in the universial format 9665XXXXXXXX), module id and activation codes are mandatory in the parameter authenticateAndGoVO
	    @return boolean. true if insertion is success else false.
	 */
    public boolean insertActivationCode(AuthenticateAndGoVO authenticateAndGoVO);

    
    /**
		date: Jun 1, 2009
		Description: This method is used to update the activation code
	    @param authenticateAndGoVO. Mobily number(in the universial format 9665XXXXXXXX), module id and activation codes are mandatory in the parameter authenticateAndGoVO
	    @return boolean . true if updated successfully else false.
	 */
	public boolean updateActivationCode(AuthenticateAndGoVO authenticateAndGoVO);
	
	
	/**
		date: Jun 1, 2009
		Description: To delete the Activation code from AUTHENTICATE_GO_ACTIVATION_TBL of portal DB. This method is used for Authenticate and go applications. 
	    @param authenticateAndGoVO. Mobily number(in the universial format 9665XXXXXXXX) and module id are mandatory values in the Parameter authenticateAndGoVO
	    @return boolean . true if deleted successfully else false.
	 */
	public boolean deleteActivationCode(AuthenticateAndGoVO authenticateAndGoVO);
	
	
	/**
		date: Jun 1, 2009
		Description: This method is used to check weather activation code is already exists in DB for the provided MSISD againest module
	    @param authenticateAndGoVO. Mobily number(in the universial format 9665XXXXXXXX) and module id are mandatory in the parameter authenticateAndGoVO
	    @return boolean. true if exist else false.
	 */
	 public boolean isActivationCodeExist(AuthenticateAndGoVO authenticateAndGoVO);
	 
	 
	 /**
	 	 date: Jun 1, 2009
	 	 Description: This method is used to check weather the provided activation code is valid or not.
	     @param authenticateAndGoVO. Mobily number(in the universial format 9665XXXXXXXX), module id and activation code are mandatory values in the Parameter authenticateAndGoVO
	     @return bolean . true if provided activation code is valid else false
	     @throws SystemException
	  */
	 public boolean isValidActivationCode(AuthenticateAndGoVO authenticateAndGoVO);
	 
	 
	 /**
	 	date: Jun 1, 2009
	 	Description: This method is used to insert the record in auditiong table of Authenticate and Go applications.
	    @param authenticateAndGoVO. Service request id, Mobily number, module id and action id are mandatory values in param authenticateAndGoVO.
	    @return booelan. true if inserted successfully else false.
	  */
	 public boolean auditSubscription(AuthenticateAndGoVO authenticateAndGoVO);

	 /**
	  * 
	  * @param authenticateAndGoVO
	  * @return
	  */
	 public int getDifferenceInMinutes(AuthenticateAndGoVO authenticateAndGoVO);
	 
	 /**
	  * 
	  * @param authenticateAndGoVO
	  * @return
	  */
	 public boolean isActivationCodeValidated(AuthenticateAndGoVO authenticateAndGoVO);
	 
	 /**
	  * 
	  * @param authenticateAndGoVO
	  * @return
	  */
	 public boolean updateActivationCodeStatus(AuthenticateAndGoVO authenticateAndGoVO);
}

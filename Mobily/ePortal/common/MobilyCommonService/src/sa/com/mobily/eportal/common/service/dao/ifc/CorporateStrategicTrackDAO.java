/**
 * 
 */
package sa.com.mobily.eportal.common.service.dao.ifc;

import java.util.List;
import java.util.Map;

import sa.com.mobily.eportal.common.service.vo.StrategicTrackVO;

/**
 * @author s.vathsavai.mit
 *
 */
public interface CorporateStrategicTrackDAO {
	
	/**
	 * This method is used to do the corporate inquiry.
	 * @param xmlRequest
	 * @return
	 */
	public String doCorporateInquiry(String xmlRequest);
	
	/**
	 * This method is used to update the alias name
	 * @param xmlRequest
	 * @return 
	 */
	public String updateCircuitAliasName(String xmlRequest);
	
	public StrategicTrackVO doInitialInquiryFromDB(String billingAccountNumber);
	
	public void doCorporateInquiryAsync(String xmlRequest);
	
	public boolean insertCorpRecordInDB(String billingAccountNumber);
	
	public boolean updateCorpRecordInDB(String billingAccountNumber);
	
	public String getMessageFromDB(String billingAccountNumber);
	
	public void updateStrategicTrackRecordInDB(String billingAccountNumber,String xmlMessage);
	
	public boolean updateExpiredStatusInDB(String billingAccountNumber,String expiredStatus);
}

/*
 * Created on Feb 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.ifc;

import java.util.ArrayList;
import java.util.List;

import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.vo.CorporateProfileVO;
import sa.com.mobily.eportal.common.service.vo.CustomerProfileReplyVO;
import sa.com.mobily.eportal.common.service.vo.UserAccountVO;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface CustomerProfileDAO {
    public CustomerProfileReplyVO getSiebelCustomerInoByMsisdn(String msisdn) ;
                                                      
    public ArrayList getSiebelCustomerRelatedMsisdns(String IDDocType , String IDNumber);
    
    public CustomerProfileReplyVO getSiebelCustomerInoByBillingNumber(String billAcNum);
    
    public UserAccountVO getUserAccountWebProfile(String msisdn);
    
    public UserAccountVO getUserAccountWebProfileByName(String userName);
    
    public CustomerProfileReplyVO getSiebelCustomerInoByMSISDN(String msisdn);
    public CustomerProfileReplyVO getCustomerProfileFromSiebel(String billingAccNumber);
    
    public CorporateProfileVO findCorporateProfile(String aBillingNo);
    
    public List getBillingAccountMsisdnList(String aBillingAccount);
    
    public String sendToMQWithReply(String xmlRequestMessage,String requestQueueName,String replyQueueName) throws MobilyCommonException;
}

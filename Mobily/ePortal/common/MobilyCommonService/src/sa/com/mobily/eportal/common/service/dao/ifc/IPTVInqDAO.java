package sa.com.mobily.eportal.common.service.dao.ifc;


/**
 * @author s.vathsavai.mit
 *
 */
public interface IPTVInqDAO {

	/**
	 * @date: Sep 27, 2012
	 * @author: n.gundluru.mit
	 * @description: 
	 * @param requestVO
	 * @return replyVo
	 */
	public String doIPTVInquiry(String strXMLRequest);
	
	
}

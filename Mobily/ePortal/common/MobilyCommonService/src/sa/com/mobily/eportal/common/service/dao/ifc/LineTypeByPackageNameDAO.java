package sa.com.mobily.eportal.common.service.dao.ifc;

import java.util.HashMap;

public interface LineTypeByPackageNameDAO {
	public HashMap<String, String> loadProductLinePerPackageName() ;
	
	public HashMap<String, String> LoadKineTypeByProductDesc()  ;
}

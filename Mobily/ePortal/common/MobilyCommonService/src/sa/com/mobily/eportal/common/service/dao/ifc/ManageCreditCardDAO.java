/*
 * Created on Sept 16, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.ifc;

import java.util.List;

import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.vo.CreditCardPaymentRequestVO;



/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ManageCreditCardDAO {
	public List getRelatedMSISDNList(String aMsisdn);
	public String getThirdGenerationTransId();
	public void auditCreditCardTransactionInDB(CreditCardPaymentRequestVO requestVO);
	public String manageCreditCards(String xmlRequestMessage,String requsterChannelFunction) throws MobilyCommonException;
	public boolean insertTransactionPIN(CreditCardPaymentRequestVO requestVO);
	public boolean isValidRecord(CreditCardPaymentRequestVO requestVO);
	public boolean isValidActivationCode(CreditCardPaymentRequestVO requestVO);
	public void clearInvalidRecord(CreditCardPaymentRequestVO requestVO);
	public String getValidPINCode(CreditCardPaymentRequestVO requestVO);
}

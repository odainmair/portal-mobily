package sa.com.mobily.eportal.common.service.dao.ifc;

import java.util.ArrayList;

import sa.com.mobily.eportal.common.service.vo.PackageTypeVO;

/**
 * 
 * @author n.gundluru.mit
 *
 */

public interface PackageConversionDAO {

	public ArrayList loadPackageTypesFromDB(int customerType);
	public PackageTypeVO getDefaultPackageById(String packageId);
	public PackageTypeVO getDefaultPackageByDefaultId(int defaultPackageId);
	public String handlePackageConversion(String xmlRequestMessage);
	public boolean hasPendingRequest(String msisdn);
}

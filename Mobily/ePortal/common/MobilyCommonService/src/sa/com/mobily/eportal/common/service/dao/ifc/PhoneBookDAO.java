/*
 * Created on Jun 30, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.ifc;

import java.util.ArrayList;

import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.vo.PhoneBookContactVO;
import sa.com.mobily.eportal.common.service.vo.PhoneBookGroupVO;
import sa.com.mobily.eportal.common.service.vo.PhoneBookListVO;

/**
 * @author n.gundluru.mit
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public interface PhoneBookDAO {

	public void createPhoneBookContact(PhoneBookContactVO contactVO)
			throws MobilyCommonException;

	public void createPhoneBookGroup(PhoneBookGroupVO groupVO)
			throws MobilyCommonException;

	public void DeletePhoneBookGroup(int group_id) throws MobilyCommonException;

	public void DeletePhoneBookGroupMemeber(ArrayList listOfContactId,
			int groupId) throws MobilyCommonException;

	public ArrayList listCustomerPhoneBookGroup(String msisdn, int functionId)
			throws MobilyCommonException;

	public ArrayList listCustomerPhoneBookMember(String msisdn, int functionId)
			throws MobilyCommonException;

	public PhoneBookListVO listPhoneBook(String msisdn, int functionId)
			throws MobilyCommonException;

	public PhoneBookGroupVO listCustomerPhoneBookGroupMember(int groupId)
			throws MobilyCommonException;

	public void joinContactToGroup(PhoneBookGroupVO groupVO)
			throws MobilyCommonException;

	public ArrayList getContactMsidnById(ArrayList listOfId)
			throws MobilyCommonException;
	
	

	public int getFunctionId(String functionName) throws MobilyCommonException;

	public boolean isContactMsisdnAlreadyExist(PhoneBookContactVO contactVO)
			throws MobilyCommonException;

	public int createNewDistributionList(PhoneBookGroupVO groupVO)
			throws MobilyCommonException;

	public int createDistributionListEntry(PhoneBookContactVO contactVO,
			int distListId) throws MobilyCommonException;

	public int updateListEntry(PhoneBookContactVO contactVO)
			throws MobilyCommonException;

	public int updateDistributionListEntry(PhoneBookContactVO contactVO)
			throws MobilyCommonException;

	public int deleteDistributinList(PhoneBookGroupVO phoneBookVo)
			throws MobilyCommonException;

	public int deleteDistributionListMember(int contactId) throws MobilyCommonException;

	public ArrayList listDuistributionListMembers(PhoneBookGroupVO groupVO)
			throws MobilyCommonException;

	public boolean isOwnerMsisdnAlreadyExist(PhoneBookGroupVO groupVO)
			throws MobilyCommonException;

	public int createNewBlackList(PhoneBookGroupVO groupVO)
			throws MobilyCommonException;

	public int createBlackListEntry(PhoneBookContactVO contactVO)
			throws MobilyCommonException;

	public ArrayList listBlackListMembers(PhoneBookGroupVO groupVO)
			throws MobilyCommonException;
	
	public int deleteBlackListMember(int contactId) throws MobilyCommonException;
}
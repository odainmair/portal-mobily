/*
 * Created on Feb 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.ifc.CustomerProfileDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.vo.CorporateProfileVO;
import sa.com.mobily.eportal.common.service.vo.CustomerProfileReplyVO;
import sa.com.mobily.eportal.common.service.vo.UserAccountVO;

/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MQCustomerProfileDAO implements CustomerProfileDAO {
    
    private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;
    
    /**
     * Method: sendToMQWithReply
     * @param xmlRequestMessage
     * @return String
     * @throws MobilyCommonException
     *  
     */
    public String sendToMQWithReply(String xmlRequestMessage,String requestQueueName,String replyQueueName) throws MobilyCommonException {
        String replyMessage = "";
		log.debug("MQCustomerProfileDAO > sendToMQWithReply > called...");
		try {

			//requestQueueName = MobilyUtility.getPropertyValue(ConstantIfc.MDM_INQUIRY_REQUEST_QUEUENAME); 
			//replyQueueName   = MobilyUtility.getPropertyValue(ConstantIfc.MDM_INQUIRY_REPLY_QUEUENAME);
			
			log.debug("MQCustomerProfileDAO > sendToMQWithReply > Request Queue Name  ["+ requestQueueName + "]");
			log.debug("MQCustomerProfileDAO > sendToMQWithReply > Reply Queue Name ["+ replyQueueName + "]");
			
			//Get Reply Queue Manager Name
			replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlRequestMessage, requestQueueName,replyQueueName);
			
		} catch (RuntimeException e) {
			log.fatal("MQCustomerProfileDAO > sendToMQWithReply > MQSend Exception "+ e);
			throw new SystemException(e);
		} catch (Exception e) {
			log.fatal("MQCustomerProfileDAO > sendToMQWithReply > Exception " + e);
			throw new SystemException(e);
		}
		return replyMessage;
    }

	public CorporateProfileVO findCorporateProfile(String billingNo) {
		// TODO Auto-generated method stub
		return null;
	}

	public List getBillingAccountMsisdnList(String billingAccount) {
		// TODO Auto-generated method stub
		return null;
	}

	public CustomerProfileReplyVO getCustomerProfileFromSiebel(
			String billingAccNumber) {
		// TODO Auto-generated method stub
		return null;
	}

	public CustomerProfileReplyVO getSiebelCustomerInoByBillingNumber(
			String billAcNum) {
		// TODO Auto-generated method stub
		return null;
	}

	public CustomerProfileReplyVO getSiebelCustomerInoByMSISDN(String msisdn) {
		// TODO Auto-generated method stub
		return null;
	}

	public CustomerProfileReplyVO getSiebelCustomerInoByMsisdn(String msisdn) {
		// TODO Auto-generated method stub
		return null;
	}

	public ArrayList getSiebelCustomerRelatedMsisdns(String IDDocType,
			String IDNumber) {
		// TODO Auto-generated method stub
		return null;
	}

	public UserAccountVO getUserAccountWebProfile(String msisdn) {
		// TODO Auto-generated method stub
		return null;
	}

	public UserAccountVO getUserAccountWebProfileByName(String userName) {
		// TODO Auto-generated method stub
		return null;
	}
	
}

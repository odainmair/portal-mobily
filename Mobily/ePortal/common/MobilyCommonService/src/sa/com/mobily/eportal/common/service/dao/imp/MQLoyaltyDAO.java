/**
 * @date: Dec 28, 2011
 * @author: s.vathsavai.mit
 * @file name: MQLoyaltyDAO.java
 * @description:  
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.util.Date;

import org.apache.log4j.Logger;

import com.mobily.exception.mq.MQSendException;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.LoyaltyDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;

/**
 * @author s.vathsavai.mit
 *
 */
public class MQLoyaltyDAO implements LoyaltyDAO{

	private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;
	
	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.ifc.LoyaltyDAO#doNeqatyPlusCorporateLevelInquiry(java.lang.String)
	 */
	public String doNeqatyPlusCorporateLevelInquiry(String strXMLRequest) {
		 String replyMessage = "";
		 log.info("MQLoyaltyDAO > doNeqatyPlusCorporateLevelInquiry :: strXMLRequest = "+strXMLRequest);
		 String requestQueName = MobilyUtility.getPropertyValue(ConstantIfc.REQUEST_QUEUE_CORPORATE_LEVEL_INQUIRY);
		 String replyQueName = MobilyUtility.getPropertyValue( ConstantIfc.REPLY_QUEUE_CORPORATE_LEVEL_INQUIRY );
		  	 
		 log.debug("MQLoyaltyDAO > doNeqatyPlusCorporateLevelInquiry : Request Queue Name: " + requestQueName );
		 log.debug("MQLoyaltyDAO > doNeqatyPlusCorporateLevelInquiry : Reply Queue Name:   " + replyQueName );
		 
		try {
			 replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(strXMLRequest, requestQueName, replyQueName);
		}catch (RuntimeException e) {
		    log.debug("MQLoyaltyDAO > doNeqatyPlusCorporateLevelInquiry..exception time: "+new Date());
			log.fatal("MQLoyaltyDAO > doNeqatyPlusCorporateLevelInquiry > MQSend Exception > "+ e.getMessage());
			throw new SystemException(e);
		} catch (Exception e) {
		    log.fatal("MQLoyaltyDAO > doNeqatyPlusCorporateLevelInquiry   > MQSend Exception > "+ e.getMessage());
			throw new SystemException(e);
		}
		 log.info("MQLoyaltyDAO > doNeqatyPlusCorporateLevelInquiry  :: end :: replyMessage = "+replyMessage);
		 return replyMessage;
	}

}

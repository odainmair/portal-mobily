package sa.com.mobily.eportal.common.service.dao.imp;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.PandaDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;

import com.mobily.exception.mq.MQSendException;

/**
 * 
 * This class is the MQManagePandaDAO implementation of the ManagePandaDAO interface
 * 
 * @author r.agarwal.mit
 * @since v1.0
 */
public class MQPandaDAO implements PandaDAO {
    
    private static final Logger log = LoggerInterface.log;

    /**
     * Method: sendToMQWithReply
     * @param xmlRequestMessage
     * @return String
     * @throws MobilyCommonException
     *  
     */
    public String sendToMQWithReply(String xmlRequestMessage,String requestType) throws MobilyCommonException {
        String replyMessage = "";
        String requestQueueName = "";
        String replyQueueName = "";
		log.debug("MQPandaDAO > sendToMQWithReply > called...");
		try {
			
			if(ConstantIfc.PANDA_REQUEST_TYPE_INQUIRY.equalsIgnoreCase(requestType)){
				requestQueueName = MobilyUtility.getPropertyValue(ConstantIfc.PANDA_INQUIRY_REQUEST_QUEUENAME); 
				replyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.PANDA_INQUIRY_REPLY_QUEUENAME);
				
			} else if(ConstantIfc.PANDA_REQUEST_TYPE_DISTRIBUTION.equalsIgnoreCase(requestType)){
				requestQueueName = MobilyUtility.getPropertyValue(ConstantIfc.PANDA_DISTRIBUTION_REQUEST_QUEUENAME); 
				replyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.PANDA_DISTRIBUTION_REPLY_QUEUENAME);
			}

			log.debug("MQPandaDAO > sendToMQWithReply > Request Queue Name  ["+ requestQueueName + "]");
			log.debug("MQPandaDAO > sendToMQWithReply > Reply Queue Name ["+ replyQueueName + "]");
			
			//Get Reply Queue Manager Name
			replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlRequestMessage, requestQueueName,replyQueueName);
			
		} catch (RuntimeException e) {
			log.fatal("MQPandaDAO > sendToMQWithReply > MQSend Exception "+ e);
			throw new SystemException(e);
		} catch (Exception e) {
			log.fatal("MQPandaDAO > sendToMQWithReply > Exception " + e);
			throw new SystemException(e);
		}
		return replyMessage;
    }

}

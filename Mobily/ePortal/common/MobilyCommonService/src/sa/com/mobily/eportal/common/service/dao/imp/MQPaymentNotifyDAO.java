package sa.com.mobily.eportal.common.service.dao.imp;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.PaymentotifyDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;

import com.mobily.exception.mq.MQSendException;
/**
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MQPaymentNotifyDAO implements PaymentotifyDAO { 
    private static final Logger logger = LoggerInterface.log;
    
    /**
     * 
     */
    public String processPaymentNotification(String xmlMessage)  throws SystemException {
		String replyMessage = "";
		logger.info("MQPaymentNotifyDAO > processPaymentNotification > Called.................");
		try {
			/*
			 * Get QueuName
			 */
			String strQueueName = MobilyUtility.getPropertyValue(ConstantIfc.CUSTOMER_PAYMENT_NOTIFY_REQUEST_QUEUENAME);
			logger.debug("MQPaymentNotifyDAO > processPaymentNotification > The request Queue Name for Customer Type ["+ strQueueName + "]");
			/*
			 * Get ReplyQueuName
			 */
			String strReplyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.CUSTOMER_PAYMENT_NOTIFY_REPLY_QUEUENAME);
			logger.debug("MQPaymentNotifyDAO > processPaymentNotification > The reply Queue Name for Customer Type ["+ strReplyQueueName + "]");
			/*
			 * Get reply Queu manager Name
			 */
			replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlMessage, strQueueName,strReplyQueueName);
			
			} catch (RuntimeException e) {
				logger.fatal("MQPaymentNotifyDAO > processPaymentNotification > MQSend Exception > "+ e.getMessage());
				throw new SystemException(e);
			} catch (Exception e) {
			    logger.fatal("MQPaymentNotifyDAO > processPaymentNotification > Exception > "+ e.getMessage());
				throw new SystemException(e);
			}
		logger.info("MQPaymentNotifyDAO > processPaymentNotification > Done.................");
		return replyMessage;

    
    }
}
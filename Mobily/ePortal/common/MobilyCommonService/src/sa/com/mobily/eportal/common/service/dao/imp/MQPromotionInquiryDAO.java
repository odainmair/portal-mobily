/*
 * Created on Feb 19, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.ErrorIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.PromotionInquiryDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;

import com.mobily.exception.mq.MQSendException;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MQPromotionInquiryDAO implements PromotionInquiryDAO{
    
    private static final Logger log = LoggerInterface.log;
    
	public String managePromotion(String xmlRequestMessage) throws MobilyCommonException
	{
		String replyMessage = "";
		log.debug("MQMobilyPromotionDAO > managePromotion > called");
		try {
			/*
			 * Get QueuName
			 */
			log.debug("Start get MQ parameter value from ResourceBundle....");
			String strQueueName = MobilyUtility.getPropertyValue(ConstantIfc.MOBILY_PROMOTION_REQUEST_QUEUENAME);
			log.debug("MQMobilyPromotionDAO > managePromotion >Request Queue Name  ["+ strQueueName + "]");
			/*
			 * Get ReplyQueuName
			 */
			String strReplyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.MOBILY_PROMOTION_REPLY_QUEUENAME );
			log.debug("MQMobilyPromotionDAO > managePromotion >Reply Queue Name ["+ strReplyQueueName + "]");
			
				replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlRequestMessage, 
				        														   strQueueName,
																				   strReplyQueueName);
			
			} catch (RuntimeException e) {
//			       AlarmService alarmService = new AlarmService();
//			       alarmService.RaiseAlarm(ConstantIfc.ALARM_MQ_CUSTOMER_TYPE);

				log.fatal("MQMobilyPromotionDAO > managePromotion > MQSend Exception "+ e);
				throw new MobilyCommonException(ErrorIfc.EXCEPTION_MQ_EXCEPTION);
			} catch (Exception e) {
				log.fatal("MQMobilyPromotionDAO > managePromotion >Exception " + e);
				throw new MobilyCommonException(ErrorIfc.EXCEPTION_MQ_EXCEPTION);
			}
		return replyMessage;
	}

	

    public static void main(String[] args) {
    }
}

/*
 * Created on Feb 19, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerPackageDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.PackageDataVO;

/**
 * @author aghareeb
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OracleCustomerPackageDataDAO implements CustomerPackageDAO{

	private static final Logger log = LoggerInterface.log;
    
	//Fetches all packages data
	private static final String FETCH_ALL_PACKAGES_DATA = "select packageid, packagenameen, packagenamear, nationalfvncount, internationalfvncount, familyandfriendscount, default_package_id, moibilyfvncount, mixfvncount, countryfvncount from packagesfvnsettings";

	//Fetches the package data
	private static final String FIND_PACKAGE_DATA_BY_ID_SQL="select packageid, packagenameen, packagenamear, nationalfvncount, internationalfvncount, familyandfriendscount from packagesfvnsettings WHERE packageid=?";
    
    
	/**
	 * Gets all package data and save them in to a list 
	 * @return packageDataList
	 */
	public ArrayList getAllPackagesData() throws SystemException{
		
		log.info("OracleCustomerPackageDataDAO > getAllPackagesData > Start");
		
		ArrayList packageDataList = new ArrayList();
	 
		Connection connection   = null;
	    ResultSet resultSet     = null;
	    PreparedStatement pStatement  = null;
	   	  
	   try{
		   
	      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	      pStatement = connection.prepareStatement(FETCH_ALL_PACKAGES_DATA);
	      resultSet  = pStatement.executeQuery();
	      PackageDataVO packageVO = null;
	     
	      while(resultSet.next()){
		      	packageVO = new PackageDataVO();
		      	packageVO.setPackageId(resultSet.getString("PACKAGEID"));
		      	packageVO.setPackageNameEn(resultSet.getString("PACKAGENAMEEN"));
		      	packageVO.setPackageNameAr(resultSet.getString("PACKAGENAMEAR"));
		      	packageVO.setNationalFVNCount(resultSet.getInt("NATIONALFVNCOUNT"));
		      	packageVO.setInternationalFVNCount(resultSet.getInt("INTERNATIONALFVNCOUNT"));
		      	packageVO.setFamilyAndFriendsCount(resultSet.getInt("FAMILYANDFRIENDSCOUNT"));
		      	packageVO.setDefault_package_id(resultSet.getInt("DEFAULT_PACKAGE_ID"));
		      	packageVO.setMobilyFVNCount(resultSet.getInt("MOIBILYFVNCOUNT"));
		      	packageVO.setMixFVNCount(resultSet.getInt("MIXFVNCOUNT"));
		      	//Added for favorite country staff package
		      		packageVO.setCountryFVNCount(resultSet.getInt("COUNTRYFVNCOUNT"));
		      	
	      		packageDataList.add(packageVO);
	      }
	      
	      log.debug("OracleCustomerPackageDataDAO > getAllPackagesData > packageDataList > Done ["+packageDataList.size()+"]");
	      
	   }catch(SQLException e){
	   	  log.fatal("OracleCustomerPackageDataDAO > getAllPackagesData > SQLException > "+e.getMessage());
	   	  throw new SystemException(e);
	   	
	   }catch(Exception e){
	   	  log.fatal("OracleCustomerPackageDataDAO > getAllPackagesData > Exception > "+e.getMessage());
	   	  throw new SystemException(e);  
	   }
	   finally{
	   	JDBCUtility.closeJDBCResoucrs(connection , pStatement , resultSet);
	   }

	   log.info("OracleCustomerPackageDataDAO > getAllPackagesData > End");

	   return packageDataList;
		
	}
	
	
	/**
	 * Gets specific package data and save it in to value object
	 * @return packageDataVO
	 */
	public PackageDataVO findPackageDataById(String packageID){
    	PackageDataVO packageDataVO = null;
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement statement=null;
		try {
			 
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(FIND_PACKAGE_DATA_BY_ID_SQL);
			statement.setString(1,packageID);
			rs = statement.executeQuery();
			if(rs.next())
			{
				packageDataVO = new PackageDataVO();
				packageDataVO.setPackageId(rs.getString("PACKAGEID"));
				packageDataVO.setPackageNameEn(rs.getString("PACKAGENAMEEN"));
				packageDataVO.setPackageNameAr(rs.getString("PACKAGENAMEAR"));
				packageDataVO.setNationalFVNCount(rs.getInt("NATIONALFVNCOUNT"));
				packageDataVO.setInternationalFVNCount(rs.getInt("INTERNATIONALFVNCOUNT"));
				packageDataVO.setFamilyAndFriendsCount(rs.getInt("FAMILYANDFRIENDSCOUNT"));
			}
		}
		catch(Exception e){
			throw new SystemException("System error while retrieving package id="+packageID,e);
		}
		finally{
			JDBCUtility.closeJDBCResoucrs(connection,statement,rs);
		}
		return packageDataVO;
    }
	
}

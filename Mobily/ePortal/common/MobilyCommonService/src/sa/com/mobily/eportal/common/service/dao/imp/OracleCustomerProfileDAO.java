/*
 * Created on Feb 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerProfileDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.AccountVO;
import sa.com.mobily.eportal.common.service.vo.ContactVO;
import sa.com.mobily.eportal.common.service.vo.CorporateAddressVO;
import sa.com.mobily.eportal.common.service.vo.CorporateAuthorizedPersonAddressVO;
import sa.com.mobily.eportal.common.service.vo.CorporateAuthorizedPersonVO;
import sa.com.mobily.eportal.common.service.vo.CorporateBillingAccountRespVO;
import sa.com.mobily.eportal.common.service.vo.CorporateBillingAccountVO;
import sa.com.mobily.eportal.common.service.vo.CorporateEmployeeVO;
import sa.com.mobily.eportal.common.service.vo.CorporateProfileVO;
import sa.com.mobily.eportal.common.service.vo.CustomerProfileReplyVO;
import sa.com.mobily.eportal.common.service.vo.IdVO;
import sa.com.mobily.eportal.common.service.vo.ServiceLineVO;
import sa.com.mobily.eportal.common.service.vo.UserAccountVO;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OracleCustomerProfileDAO implements CustomerProfileDAO {
    
    private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;
    
	/**
	 * Responsible for get Customer Prrofile by using MSISDN
	 * @param msisdn
	 * @return
	 * Feb 19, 2008
	 * OracleCustomerProfileDAO.java
	 * msayed
	 */
	public CustomerProfileReplyVO getSiebelCustomerInoByMsisdn(String msisdn)  {
	    log.debug("OracleCustomerProfileDAO > getSiebelCustomerInoByMsisdn > called for msisdn = "+msisdn);
		PreparedStatement prepStmt = null;
		CustomerProfileReplyVO replyVO = null;
		ResultSet rset = null;
		AccountVO customer = null;
		Connection connection = null;
		try {
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_SIEBEL));
			StringBuffer sqlStatement = new StringBuffer();
			sqlStatement.append(" SELECT srvx.eecc_msisdn_num msisdn, bil.ou_num billing#, ");
                          sqlStatement.append("        cstx.attrib_07 customercategory, cstx.attrib_41 customertype, ");
                          sqlStatement.append("        PLAN.TYPE PLAN, pack.NAME PACKAGE, srv.cust_stat_cd status, ");
                          sqlStatement.append("        cst.NAME NAME, cstx.attrib_34 iddoctype, cstx.attrib_35 idnumber, ");
                          sqlStatement.append("        conx.x_nationality nationality, addr.addr streetaddress, addr.city city ,  ");
                          sqlStatement.append("        addrx.attrib_01 pobox, conutx.contact_pref_cd contactpreference, ");
                          sqlStatement.append("        con.cell_ph_num mobile, con.home_ph_num telephone, con.fax_ph_num fax, ");
                          sqlStatement.append("        con.email_addr email, con.sex_mf gender, con.birth_dt birthdate, ");
                          sqlStatement.append("        srv.created timecreated, srv.last_upd timeupdated, usr.login createdby, billx.X_EECC_LANGUAGE_PREFERENCE  billinglanguage, ");
                          sqlStatement.append("        cst.hshldhd_occ_cd occupation, srv.row_id sibelid, con.FST_NAME firstName, con.LAST_NAME, con.MID_NAME , con.PER_TITLE title  ");
                          sqlStatement.append("   FROM siebel.s_org_ext_x srvx, ");
                          sqlStatement.append("        siebel.s_org_ext srv, ");
                          sqlStatement.append("        siebel.s_asset pac, ");
                          sqlStatement.append("        siebel.s_org_ext bil, ");
                          sqlStatement.append("        siebel.S_ORG_EXT_X billx , ");
                          sqlStatement.append("        siebel.s_org_ext cst, ");
                          sqlStatement.append("        siebel.s_org_ext_x cstx, ");
                          sqlStatement.append("        siebel.s_contact con, ");
                          sqlStatement.append("        siebel.s_contact_x conx, ");
                          sqlStatement.append("        siebel.s_contact_utx conutx, "); 
                          sqlStatement.append("        siebel.s_addr_per addr, ");
                          sqlStatement.append("        siebel.s_addr_per_x addrx, ");
                          sqlStatement.append("        siebel.s_prod_int PLAN, ");
                          sqlStatement.append("        siebel.s_user usr, ");
                          sqlStatement.append("        siebel.s_prod_int pack ");
                          sqlStatement.append("  WHERE srvx.eecc_msisdn_num = ?  ");
                          sqlStatement.append(" AND srv.row_id = pac.serv_acct_id ");
                          sqlStatement.append("    AND pac.type_cd = 'Service Aggregator' ");
                          sqlStatement.append("    AND pac.status_cd = 'Active' ");
                          sqlStatement.append("    AND srv.cust_stat_cd = 'Active' ");
                          sqlStatement.append("    AND srv.row_id = srvx.par_row_id ");
                          sqlStatement.append("    AND bil.row_id = srv.par_ou_id ");
                          sqlStatement.append("    AND cst.row_id = bil.par_ou_id ");
                          sqlStatement.append("    AND cstx.par_row_id = cst.row_id ");
                          sqlStatement.append("    AND con.row_id = cst.pr_con_id ");
                          sqlStatement.append("    AND conx.par_row_id = con.row_id ");
                          sqlStatement.append("    AND conutx.par_row_id = con.row_id ");
                          sqlStatement.append("    AND PLAN.row_id = srv.x_eecc_pricing_plan_id ");
                          sqlStatement.append("    AND addr.row_id = cst.pr_addr_id ");
                          sqlStatement.append("    AND addrx.par_row_id = addr.row_id ");
                          sqlStatement.append("    AND usr.row_id = srv.created_by ");
                          sqlStatement.append("    AND pac.prod_id = pack.row_id   ");    
                          sqlStatement.append("    AND bil.par_row_id = billx.par_row_id  "); 
                          sqlStatement.append("    order by srv.created desc ");                          
                          prepStmt = connection.prepareStatement(sqlStatement.toString());
            prepStmt.setString(1 ,  msisdn  );
			rset = prepStmt.executeQuery();
			if (rset.next()) 
			{
			    replyVO = new CustomerProfileReplyVO();
				SimpleDateFormat sdF = new SimpleDateFormat();
				sdF.applyPattern("yyyyMMdd");	

				customer = new AccountVO();
				customer.setFullName( rset.getString("NAME"));
				customer.setFirstName(rset.getString("firstName"));
				customer.setMiddleName(rset.getString("MID_NAME"));
				customer.setLastName(rset.getString("LAST_NAME"));
				customer.setTitle(rset.getString("title"));
				customer.setCustomerCategory(rset.getString("customercategory"));
				customer.setCustomerType(rset.getString("customertype"));
				if( rset.getDate("BirthDate") != null )
					customer.setBirthDate( sdF.format( rset.getDate("BirthDate"))  );  
				else   
					customer.setBirthDate("");  
				//customer.setBirthDateFormat(rset.getString("birthdateformat"));
				customer.setBirthDateFormat("Miladi");
				customer.setGender(rset.getString("Gender") );
				customer.setNationality( rset.getString("Nationality")  );  
				customer.setOccupation(rset.getString("occupation"));
				if( rset.getString("billinglanguage")  != null )
					customer.setBillingLanguage( rset.getString("billinglanguage") );
				else
					customer.setBillingLanguage( "" );
				//customer.setBillingLanguage( null);
				
				
				IdVO idVO = new IdVO();
				idVO.setIdDocType( rset.getString("IDDocType")  );  
				idVO.setIdNumber( rset.getString("IDNumber")  );  
				customer.setId(idVO);
				
				ContactVO contact =  new ContactVO();
				contact.setStreetAddress( rset.getString("StreetAddress")  );  
				contact.setPoBox( rset.getString("POBOX")  );  
				contact.setContactPreference( rset.getString("ContactPreference")  );  
				contact.setMobile( rset.getString("Mobile")  );  
				contact.setPhone( rset.getString("Telephone")  );  
				contact.setFax( rset.getString("Fax")  );  
				contact.setEmail( rset.getString("Email")  );
				customer.setContact(contact);
				replyVO.setAccount(customer);
				
				ServiceLineVO serviceLine = new ServiceLineVO();
				serviceLine.setLineNumber(msisdn);
				serviceLine.setBillingNumber( rset.getString("Billing#") );
				serviceLine.setPlan( rset.getString("Plan")  );  
				serviceLine.setPackageName( rset.getString("Package")  );  
				serviceLine.setStatus( rset.getString("Status")  );  
				serviceLine.setCreatedDate( sdF.format( rset.getDate("timecreated"))  );  
				serviceLine.setCreatedBy(rset.getString("CreatedBy"));
				ArrayList list = new ArrayList();
				list.add(serviceLine);
				replyVO.setServiceLines(list);
				  
//				customer.setRowID( rset.getString("sibelid")  );
				sdF = null;

			}
	 } catch (SQLException e) {
			log.fatal("OracleCustomerProfileDAO > getSiebelCustomerInoByMsisdn > SQLException "+e.getMessage());
			throw new SystemException(e);
		} finally {
			try {
				if(connection != null) {
				  connection.close();
				}
				
				if (prepStmt != null)
					prepStmt.close();

				if (rset != null)
					rset.close();
			} catch (SQLException e) {
				log.error("OracleCustomerProfileDAO > getSiebelCustomerInoByMsisdn > SQLException In Closing Connection ", e);
			}
		}
		log.debug("OracleCustomerProfileDAO > getSiebelCustomerInoByMsisdn > done for msisdn = "+msisdn);
		return replyVO;
	}    
	
	
	/**
	 * Responisble for get Customer related Number
	 * @param IDDocType
	 * @param IDNumber
	 * @return
	 * Feb 19, 2008
	 * OracleCustomerProfileDAO.java
	 * msayed
	 */
	public ArrayList getSiebelCustomerRelatedMsisdns(String IDDocType , String IDNumber) {
	    log.debug("OracleCustomerProfileDAO > getSiebelCustomerRelatedMsisdns > called for IDDocType = "+IDDocType);
        PreparedStatement prepStmt = null;
        ResultSet rset = null;
        ArrayList listOfRelatedNumber = new ArrayList();
		Connection connection = null;
		try {
        	connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_SIEBEL));
        	String sqlStatement = "SELECT srvx.eecc_msisdn_num ORIGINAL_MSISDN, bills.ou_num BILLING#, "
        		+" srvrsx.eecc_msisdn_num RELATED_MSISDN, PLAN.TYPE PLAN, "
				+" PACK.NAME PACKAGE, srvrs.cust_stat_cd STATUS, "
				+" cstx.attrib_07 CustomerCategory, cstx.attrib_41 CustomerType "
				+" , usr.login createdby , srv.created timecreated "
				+" FROM siebel.s_org_ext_x srvx, "
				+" siebel.s_org_ext srv, "
				+" siebel.s_org_ext bill, "
				+" siebel.s_org_ext cst, "
				+" siebel.s_org_ext_x cstx, "
				+" siebel.s_org_ext bills, "
				+" siebel.s_org_ext srvrs, "
				+" siebel.s_org_ext_x srvrsx, "
				+" siebel.s_asset pac, "
				+" siebel.s_prod_int PACK, "
				+" siebel.s_prod_int PLAN, "
				+" siebel.s_user usr "                                                    
				+" WHERE cstx.attrib_34 = ? and    cstx.attrib_35=? "
				+" AND srv.row_id = srvx.par_row_id  "
				+" AND bill.row_id = srv.par_ou_id "
				+" AND cst.row_id = bill.par_ou_id "
				+" AND cstx.par_row_id = cst.row_id "
				+" AND bills.par_ou_id = cst.row_id "
				+" AND bills.row_id = srvrs.par_ou_id "
				+" AND usr.row_id = srv.created_by "                                                    
				+" AND srvrs.row_id = srvrsx.par_row_id "
				+" AND pac.serv_acct_id = srvrs.row_id "
				+" AND pac.type_cd='Service Aggregator' "
				+" AND pac.STATUS_CD='Active' "
				+" AND srv.cust_stat_cd = 'Active' "
				+" AND pac.prod_id=pack.row_id "
				+" AND srvrsx.eecc_msisdn_num IS NOT NULL "
			 // +" AND srv.cust_stat_cd = '"+status+"'" 
				+" AND PLAN.row_id = srvrs.x_eecc_pricing_plan_id order by related_msisdn asc ";
        	prepStmt = connection.prepareStatement(sqlStatement);
			prepStmt.setString(1 , IDDocType );
			prepStmt.setString(2 , IDNumber );
			rset = prepStmt.executeQuery();

			SimpleDateFormat sdF = new SimpleDateFormat();
			sdF.applyPattern("yyyyMMdd");           
			String RELATED_MSISDN= "";
			while (rset.next()) 
			{
				String relatedMsisdn = rset.getString("RELATED_MSISDN");
				if( ! relatedMsisdn.equals(RELATED_MSISDN))
				{
				    ServiceLineVO serviceLine = new ServiceLineVO();
					serviceLine.setLineNumber( relatedMsisdn  );
					serviceLine.setBillingNumber( rset.getString("Billing#") );
					serviceLine.setPlan( rset.getString("Plan")  );  
					serviceLine.setPackageName( rset.getString("Package")  );  
					serviceLine.setStatus( rset.getString("Status")  );  
					serviceLine.setCreatedDate( sdF.format( rset.getDate("timecreated"))  );  
					serviceLine.setCreatedBy(rset.getString("CreatedBy"));
					listOfRelatedNumber.add( serviceLine );
					RELATED_MSISDN = relatedMsisdn;
				}
			}
        } catch (SQLException e) {
        	log.fatal("OracleCustomerProfileDAO >getSiebelCustomerRelatedMsisdns >  SQLException ...>"+e.getMessage());
        	throw new SystemException(e);
		}finally {
        	try {
				if(connection != null)
					connection.close();
				if (prepStmt != null)
					prepStmt.close();
				if (rset != null)
					rset.close();
        	} catch (SQLException e) {
        		log.error("Error : ", e);
            }
        }
		log.debug("OracleCustomerProfileDAO > getSiebelCustomerRelatedMsisdns > done for IDDocType = "+IDDocType);
        return listOfRelatedNumber;
	} 

	
	/**
	 * Responsible for get Customer Prrofile by using Billing Number
	 * @param billingNumber
	 * @return
	 * Oct 24, 2008
	 * OracleCustomerProfileDAO.java
	 * Nagendra
	 */
	public CustomerProfileReplyVO getSiebelCustomerInoByBillingNumber(String billingNumber)  {
	    log.debug("OracleCustomerProfileDAO > getSiebelCustomerInoByBillingNumber > called for billing number = "+billingNumber);
	    CallableStatement  cstmt = null;
		CustomerProfileReplyVO replyVO = null;
		ResultSet rset = null;
		AccountVO customer = null;
		Connection connection = null;
			try {
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_SIEBEL));
				//connection = getSiebelLocalConnection();

//				StringBuffer sqlStatement = new StringBuffer("{ CALL SIEBEL.Customerinfo.GetCustomerByAccntNum(? , ?) }");
				StringBuffer sqlStatement = new StringBuffer("{ CALL SIEBEL.Customerinfo.GetCustomer(? , ? , ?) }");
//				connection = DataBaseConnectionHandler.getLocalSiebelConnection();
				
				log.debug("OracleCustomerProfileDAO > getSiebelCustomerInoByBillingNumber > sqlStatement = "+sqlStatement.toString());
				
	            cstmt = connection.prepareCall(sqlStatement.toString());
	            
	            cstmt.setString(1 , billingNumber);
	            cstmt.setString(2,ConstantIfc.CUSTOMER_INFO_ACCOUNT);
	            cstmt.registerOutParameter(3,OracleTypes.CURSOR);
	            
	            cstmt.execute();

	            rset=(ResultSet)cstmt.getObject(3);

	            if (rset.next()) {
				    replyVO = new CustomerProfileReplyVO();
					SimpleDateFormat sdF = new SimpleDateFormat();
					sdF.applyPattern("yyyyMMdd");	
					
					ResultSetMetaData metaData = rset.getMetaData();
		            int rowCount = metaData.getColumnCount();
		            for (int i = 1; i <= rowCount; i++) {
		                if("ACCNT_TYPE".equalsIgnoreCase(metaData.getColumnName(i))) {
		                	replyVO.setAccountType(rset.getString("ACCNT_TYPE"));	
		                } 
		                
		                if("ISMASTERBA".equalsIgnoreCase(metaData.getColumnName(i))) {
		                	replyVO.setIsMasterBilingAccount(rset.getString("ISMASTERBA"));
		                }
		            }

					customer = new AccountVO();
						customer.setFullName( rset.getString("NAME"));
						customer.setFirstName(rset.getString("FIRSTNAME"));
						customer.setMiddleName(rset.getString("MIDNAME"));
						customer.setLastName(rset.getString("LASTNAME"));
						customer.setTitle(rset.getString("TITLE"));
						customer.setCustomerCategory(rset.getString("CUSTOMERCATEGORY"));
						customer.setCustomerType(rset.getString("CUSTOMERTYPE"));
						if( rset.getDate("BIRTHDATE") != null )
						    customer.setBirthDate(rset.getString("BIRTHDATE"));
						else   
							customer.setDateOfBirth("");  
						customer.setBirthDateFormat(rset.getString("DOBFORMAT"));
						customer.setGender(rset.getString("GENDER") );
						customer.setNationality( rset.getString("NATIONALITY")  );  
						customer.setOccupation(rset.getString("OCCUPATION"));
						if( rset.getString("LANG_PREF")  != null )
							customer.setBillingLanguage( rset.getString("LANG_PREF") );
						else
							customer.setBillingLanguage( "" );
					
						customer.setCustomerSegment(rset.getString("customerSegment"));
						log.debug("OracleCustomerProfileDAO > getSiebelCustomerInoByBillingNumber :: customer Segment : "+customer.getCustomerSegment());
						
					IdVO idVO = new IdVO();
						idVO.setIdDocType( rset.getString("IDDOCTYPE")  );  
						idVO.setIdNumber( rset.getString("IDNUMBER")  );  
					customer.setId(idVO);
					
					ContactVO contact =  new ContactVO();
						contact.setStreetAddress( rset.getString("STREETADDRESS")  );  
						contact.setPoBox( rset.getString("POBOX")  );  
						contact.setContactPreference( rset.getString("CONTACTPREFERENCE")  );  
						contact.setMobile( rset.getString("MOBILE")  );  
						contact.setPhone( rset.getString("HOMETELEPHONE")  );  
						contact.setFax( rset.getString("FAX")  );  
						contact.setEmail( rset.getString("EMAIL")  );
						customer.setContact(contact);
					replyVO.setAccount(customer);
					
					ServiceLineVO serviceLine = new ServiceLineVO();
						serviceLine.setLineNumber(rset.getString("MSISDN"));
						serviceLine.setBillingNumber( billingNumber );
						serviceLine.setPlan( rset.getString("PLAN")  );  
						serviceLine.setPackageName( rset.getString("PACKAGE")  );  
						serviceLine.setStatus( rset.getString("STATUS")  );  
						serviceLine.setCreatedDate( sdF.format( rset.getDate("TIMECREATED"))  );  
						serviceLine.setCreatedBy(rset.getString("CREATEDBY"));
						serviceLine.setAccountId(rset.getString("SERVICE_ROW_ID"));
						
					ArrayList list = new ArrayList();
					list.add(serviceLine);
					replyVO.setServiceLines(list);
					  
				}
		 } catch (SQLException e) {
				log.fatal("OracleCustomerProfileDAO > getSiebelCustomerInoByBillingNumber > SQLException "+e);
				if(e.getMessage().indexOf("Account Number "+billingNumber+" not found") == -1)
				    throw new SystemException(e);
		} finally {
		    	JDBCUtility.closeJDBCResoucrs(connection,cstmt,rset);			
		}
		log.debug("OracleCustomerProfileDAO > getSiebelCustomerInoByBillingNumber > done for billing number = "+billingNumber);
		return replyVO;
	}    

	/**
		date: Jul 6, 2009
		Description: This method is used to fetch the Customer profile from siebel based on the passed BillingAccountNumber
	    @param billingAccountNumber
	    @return
	 */
	public CustomerProfileReplyVO getCustomerProfileFromSiebel(String billingAccountNumber)  {
	    log.debug("OracleCustomerProfileDAO > getCustomerProfileFromSiebel > called for billing account number = "+billingAccountNumber);
	    CallableStatement  cstmt = null;
		CustomerProfileReplyVO replyVO = null;
		ResultSet rset = null;
		AccountVO customer = null;
		Connection connection = null;
			try {
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_SIEBEL));
				//connection = getSiebelLocalConnection();

				StringBuffer sqlStatement = new StringBuffer("{ CALL SIEBEL.Customerinfo.GETCORPCUSTPROFILE(? , ?) }");
				
	            cstmt = connection.prepareCall(sqlStatement.toString());
	            
	            cstmt.setString(1 , billingAccountNumber);
	            
	            cstmt.registerOutParameter(2,OracleTypes.CURSOR);
	            log.info("Before execute");
	            cstmt.execute();
	            log.info("After execute the pr");
	            rset=(ResultSet)cstmt.getObject(2);
				
	            if (rset.next()){
				    replyVO = new CustomerProfileReplyVO();
					ResultSetMetaData metaData = rset.getMetaData();
		            int rowCount = metaData.getColumnCount();
		            for (int i = 1; i <= rowCount; i++) {
		            	
		                if("ACCNT_TYPE".equalsIgnoreCase(metaData.getColumnName(i))) {
		                	replyVO.setAccountType(rset.getString("ACCNT_TYPE"));	
		                } 
		                
		                if("ISMASTERBA".equalsIgnoreCase(metaData.getColumnName(i))) {
		                	replyVO.setIsMasterBilingAccount(rset.getString("ISMASTERBA"));
		                }
		            }

					customer = new AccountVO();
						//customer.setFullName( rset.getString("NAME"));
						customer.setFirstName(rset.getString("AUTHORIZEDPERSONFIRSTNAME"));
						customer.setMiddleName(rset.getString("AUTHORIZEDPERSONMIDDLENAME"));
						customer.setLastName(rset.getString("AUTHORIZEDPERSONLASTNAME"));
						//customer.setTitle(rset.getString("TITLE"));
						customer.setCustomerCategory(rset.getString("COMPANYCUSTOMERCATEGORY"));
						customer.setCustomerType(rset.getString("COMPANYCUSTOMERTYPE"));
						if( rset.getDate("AUTHORIZEDPERSONBITHDATE") != null )
						    customer.setBirthDate(rset.getString("AUTHORIZEDPERSONBITHDATE"));
						else   
							customer.setDateOfBirth("");  
						//customer.setBirthDateFormat(rset.getString("DOBFORMAT"));
						customer.setGender(rset.getString("AUTHORIZEDPERSONGENDER") );
						customer.setNationality( rset.getString("AUTHORIZEDPERSONNATIONALITY")  );  
						//customer.setOccupation(rset.getString("OCCUPATION"));
						if( rset.getString("BILLINGLANGREF")  != null )
							customer.setBillingLanguage( rset.getString("BILLINGLANGREF") );
						else
							customer.setBillingLanguage( "" );
					
						customer.setCustomerSegment(rset.getString("customerSegment"));
						customer.setCompanyCountry(rset.getString("COMPANYCOUNTRY"));
						customer.setCompanyCity(rset.getString("COMPANYCITY"));
						customer.setCompanyAddress(rset.getString("COMPANYADDRESS"));
						log.debug("OracleCustomerProfileDAO > getCustomerProfileFromSiebel :: customer Segment : "+customer.getCustomerSegment());
					IdVO idVO = new IdVO();
						idVO.setIdDocType( rset.getString("COMPANYIDDOCTYPE")  );  
						idVO.setIdNumber( rset.getString("COMPANYIDNUMBER")  );  
					customer.setId(idVO);
					
					ContactVO contact =  new ContactVO();
						contact.setStreetAddress( rset.getString("COMPANYADDRESS")  );  
						contact.setPoBox( rset.getString("COMPANYPOBOX"));
						contact.setCompanyRowId(rset.getString("COMPANYROWID")); // Added for static Ip Project
						//contact.setContactPreference( rset.getString("CONTACTPREFERENCE")  );  
						contact.setMobile( rset.getString("AUTHORIZEDPERSONMOBILE")  );  
						contact.setPhone( rset.getString("AUTHORIZEDPERSONTELEPHONE")  );  
						contact.setFax( rset.getString("AUTHORIZEDPERSONFAX")  );  
						contact.setPostalCode(rset.getString("COMPANYPOSTALCODE")  );
						contact.setEmail( rset.getString("AUTHORIZEDPERSONEMAIL")  );
						contact.setBillingCountry(rset.getString("BILLINGCOUNTRY")  );
						
						customer.setContact(contact);
						
						//For Panda
						customer.setAccountManagerName(rset.getString("KAMLOGIN"));
						customer.setAccountManagerEmail(rset.getString("KAMEMAIL"));
						customer.setPandaFlag(rset.getString("PANDAFLG"));
						
						//Added for Eportal Revamp
						customer.setBillingCompanyName(rset.getString("BILLINGCOMPANYNAME"));
						
					replyVO.setAccount(customer);
					
					//added by Suresh V on Jun29 2010
					replyVO.setRootBillingAccountNumber(rset.getString("BILLINGACCOUNTNUMBER"));
					replyVO.setCompanyRowId(rset.getString("COMPANYROWID"));
					
					ServiceLineVO serviceLine = new ServiceLineVO();
						serviceLine.setLineNumber(rset.getString("BILLINGCELLNUMBER"));
						serviceLine.setBillingNumber( billingAccountNumber );
						serviceLine.setAccountId(rset.getString("BILLINGROWID"));
						
						//serviceLine.setPlan( rset.getString("PLAN")  );  
						//serviceLine.setPackageName( rset.getString("PACKAGE")  );  
						//serviceLine.setStatus( rset.getString("STATUS")  );  
						//serviceLine.setCreatedDate( sdF.format( rset.getDate("TIMECREATED"))  );  
						//serviceLine.setCreatedBy(rset.getString("CREATEDBY"));
						
					ArrayList list = new ArrayList();
					list.add(serviceLine);
					replyVO.setServiceLines(list);
					  
				}
		 } catch (SQLException e) {
				log.fatal("OracleCustomerProfileDAO > getCustomerProfileFromSiebel > SQLException "+e.getMessage());
				log.info("OracleCustomerProfileDAO > getCustomerProfileFromSiebel > SQLException "+e.getMessage());
				//if(e.getMessage().indexOf("Account Number "+billingAccountNumber+" not found") == -1)
				    //throw new SystemException(e);
				e.printStackTrace();
		} finally {
		    	JDBCUtility.closeJDBCResoucrs(connection,cstmt,rset);			
		}
		log.info("OracleCustomerProfileDAO > getCustomerProfileFromSiebel > done for billing account number = "+billingAccountNumber);
		return replyVO;
	}   
	
	
	/**
	 * Responsible for get Customer Prrofile by using MSISDN
	 * @param msisdn
	 * @return CustomerProfileReplyVO
	 * Jan 28, 2009
	 * OracleCustomerProfileDAO.java
	 * Nagendra
	 */
	public CustomerProfileReplyVO getSiebelCustomerInoByMSISDN(String msisdn)  {
	    log.debug("OracleCustomerProfileDAO > getSiebelCustomerInoByMSISDN > called for billing number = "+msisdn);
	    CallableStatement  cstmt = null;
		CustomerProfileReplyVO replyVO = null;
		ResultSet rset = null;
		AccountVO customer = null;
		Connection connection = null;
			try {
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_SIEBEL));
				
				//StringBuffer sqlStatement = new StringBuffer("{ CALL SIEBEL.Customerinfo.GetCustomerByMSISDN (?, ?)}");
				
				StringBuffer sqlStatement = new StringBuffer("{ CALL SIEBEL.Customerinfo.GetCustomer(?, ?, ?)}");

				log.debug("OracleCustomerProfileDAO > getSiebelCustomerInoByMSISDN > sqlStatement = "+sqlStatement.toString());

	            cstmt = connection.prepareCall(sqlStatement.toString());
	            
	            cstmt.setString(1 , msisdn);
	            cstmt.setString(2,ConstantIfc.CUSTOMER_INFO_MSISDN);
	            cstmt.registerOutParameter(3,OracleTypes.CURSOR);
	            
	            cstmt.execute();
	            
	            
	            rset=(ResultSet)cstmt.getObject(3);
				
	            if (rset.next()){
				    replyVO = new CustomerProfileReplyVO();
					SimpleDateFormat sdF = new SimpleDateFormat();
					sdF.applyPattern("yyyyMMdd");	
	
					customer = new AccountVO();
						customer.setFullName( rset.getString("NAME"));
						customer.setFirstName(rset.getString("FIRSTNAME"));
						customer.setMiddleName(rset.getString("MIDNAME"));
						customer.setLastName(rset.getString("LASTNAME"));
						customer.setTitle(rset.getString("TITLE"));
						customer.setCustomerCategory(rset.getString("CUSTOMERCATEGORY"));
						customer.setCustomerType(rset.getString("CUSTOMERTYPE"));
						if( rset.getDate("BIRTHDATE") != null )
						    customer.setBirthDate(rset.getString("BIRTHDATE"));
						else   
							customer.setDateOfBirth("");  
						customer.setBirthDateFormat(rset.getString("DOBFORMAT"));
						customer.setGender(rset.getString("GENDER") );
						customer.setNationality( rset.getString("NATIONALITY")  );  
						customer.setOccupation(rset.getString("OCCUPATION"));
						//property added by geetha to fetch the simNumber
						customer.setSimNumber(rset.getString("SIM_NUMBER") );
						//property added by geetha to fetch the package id
						customer.setPackageID(rset.getString("PACKAGE_ID") );
						if( rset.getString("LANG_PREF")  != null )
							customer.setBillingLanguage( rset.getString("LANG_PREF") );
						else
							customer.setBillingLanguage( "" );
					
						customer.setCustomerSegment(rset.getString("customerSegment"));
						log.debug("OracleCustomerProfileDAO > getSiebelCustomerInoByMSISDN :: customer Segment : "+customer.getCustomerSegment());
						
					IdVO idVO = new IdVO();
						idVO.setIdDocType( rset.getString("IDDOCTYPE")  );  
						idVO.setIdNumber( rset.getString("IDNUMBER")  );  
					customer.setId(idVO);
					
					ContactVO contact =  new ContactVO();
						contact.setStreetAddress( rset.getString("STREETADDRESS")  );  
						contact.setPoBox( rset.getString("POBOX")  );  
						contact.setContactPreference( rset.getString("CONTACTPREFERENCE")  );  
						contact.setMobile( rset.getString("MOBILE")  );  
						contact.setPhone( rset.getString("HOMETELEPHONE")  );  
						contact.setFax( rset.getString("FAX")  );  
						contact.setEmail( rset.getString("EMAIL")  );
						customer.setContact(contact);
					replyVO.setAccount(customer);
					
					ServiceLineVO serviceLine = new ServiceLineVO();
						serviceLine.setLineNumber(rset.getString("MSISDN"));
						serviceLine.setBillingNumber( rset.getString("BILLING#"));
						serviceLine.setPlan( rset.getString("PLAN")  );  
						serviceLine.setPackageName( rset.getString("PACKAGE")  );  
						serviceLine.setStatus( rset.getString("STATUS")  );  
						
						serviceLine.setCreatedDate( sdF.format( rset.getDate("TIMECREATED"))  );  
						serviceLine.setCreatedBy(rset.getString("CREATEDBY"));
						serviceLine.setServiceNumber(rset.getString("SERVICE#"));
						serviceLine.setAccountId(rset.getString("SERVICE_ROW_ID"));
						
						
						if( !FormatterUtility.isEmpty(rset.getString("IUCCORPID")) )
							serviceLine.setIUC(true);
						else   
							serviceLine.setIUC(false); 
					ArrayList list = new ArrayList();
					list.add(serviceLine);
					replyVO.setServiceLines(list);
					  
				}
		 } catch (SQLException e) {
				log.fatal("OracleCustomerProfileDAO > getSiebelCustomerInoByMSISDN > SQLException "+e.getMessage());
				if(e.getMessage().indexOf("MSISDN "+msisdn+" not found") == -1)
				    throw new SystemException(e);
		} finally {
		    	JDBCUtility.closeJDBCResoucrs(connection,cstmt,rset);			
		}
		log.debug("OracleCustomerProfileDAO > getSiebelCustomerInoByMSISDN > done for MSISDN number = "+msisdn);
		return replyVO;
	}    
	
	
	/**
	 * Responsible for get user account information for the given MSISDN
	 * @param msisdn
	 * @return UserAccountVO
	 * Nov 26, 2008
	 * OracleCustomerProfileDAO.java
	 * Nagendra
	 */
	public UserAccountVO getUserAccountWebProfile(String msisdn){
		log.debug("OracleCustomerProfileDAO > getUserAccountWebProfile > Called ");
 	    Connection connection = null;
	    PreparedStatement statement = null;
	    ResultSet resultSet = null;
	    UserAccountVO objUserAccountVO = null;
		try
		{
			String sqlQuery = "SELECT FULLNAME, GENDER, AGE, NATIONALITY, EMAIL_ADDRESS, PREFERRED_LANGUAGE FROM USER_ACCOUNT WHERE MSISDN=?";
		    connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1, msisdn);
			resultSet = statement.executeQuery();
		    while(resultSet.next())
		    {
		      	objUserAccountVO = new UserAccountVO();
		      	objUserAccountVO.setFullName(resultSet.getString("FULLNAME"));
		      	objUserAccountVO.setGender(resultSet.getString("GENDER"));
				objUserAccountVO.setAge(resultSet.getString("AGE"));
				objUserAccountVO.setNationality(resultSet.getString("NATIONALITY"));
				objUserAccountVO.setEmailAddress(resultSet.getString("EMAIL_ADDRESS"));
				objUserAccountVO.setPreferredLanguage(resultSet.getString("PREFERRED_LANGUAGE"));
		    }
		} catch (SQLException e) {
			log.fatal("OracleCustomerProfileDAO > getUserAccountWebProfile > SQLException "+e.getMessage());
			throw new SystemException(e);
		}finally{
		    JDBCUtility.closeJDBCResoucrs(connection,statement,resultSet);		
		}
		
		log.debug("OracleCustomerProfileDAO > getUserAccountWebProfile > End ");
		
		return objUserAccountVO;
	}
	
	/**
	 * Responsible for get user account information for the given name
	 * 
	 * @param user name
	 * @return UserAccountVO
	 */
	public UserAccountVO getUserAccountWebProfileByName(String userName){
		log.debug("OracleCustomerProfileDAO > getUserAccountWebProfile > Called ");
 	    Connection connection = null;
	    PreparedStatement statement = null;
	    ResultSet resultSet = null;
	    UserAccountVO objUserAccountVO = null;
		try
		{
		    String sqlQuery = "SELECT FULLNAME, GENDER, AGE, NATIONALITY, EMAIL_ADDRESS, PREFERRED_LANGUAGE, SEC_QUES_ID, SEC_QUES_ANSWER FROM USER_ACCOUNT WHERE USERNAME=?";
		    connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1, userName);
			resultSet = statement.executeQuery();
		    while(resultSet.next())
		    {
		      	objUserAccountVO = new UserAccountVO();
		      	objUserAccountVO.setFullName(resultSet.getString("FULLNAME"));
		      	objUserAccountVO.setGender(resultSet.getString("GENDER"));
				objUserAccountVO.setAge(resultSet.getString("AGE"));
				objUserAccountVO.setNationality(resultSet.getString("NATIONALITY"));
				objUserAccountVO.setEmailAddress(resultSet.getString("EMAIL_ADDRESS"));
				objUserAccountVO.setPreferredLanguage(resultSet.getString("PREFERRED_LANGUAGE"));
				objUserAccountVO.setSecurityQuesId(resultSet.getInt("SEC_QUES_ID"));
				objUserAccountVO.setSecurityQuesAns(resultSet.getString("SEC_QUES_ANSWER"));
		    }
		} catch (SQLException e) {
			log.fatal("OracleCustomerProfileDAO > getUserAccountWebProfile > SQLException "+e.getMessage());
			throw new SystemException(e);
		}finally{
		    JDBCUtility.closeJDBCResoucrs(connection,statement,resultSet);		
		}
		
		log.debug("OracleCustomerProfileDAO > getUserAccountWebProfile > End ");
		
		return objUserAccountVO;
	}
	
	 /**
	  * Date: Sept 17, 2009
	  * Description: This method is used to get the Corporate Profile from Siebel
	  * @author r.agarwal.mit
	  * @param aBillingNo
	  * @return CorporateProfileVO
	  */
	public CorporateProfileVO findCorporateProfile(String aBillingNo) {
		log.debug("OracleCustomerProfileDAO > findCorporateProfile > billing NO: " + aBillingNo);
		
		PreparedStatement cs = null;
		ResultSet rs = null;
		CorporateProfileVO corporateProfileVO = new CorporateProfileVO();
		Connection connection = null;
		try {
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_SIEBEL));

			StringBuffer buf = new StringBuffer(
					"SELECT ORGX5.ATTRIB_52 CompanyName, "
							+ "ORG1.OU_NUM CustomerLevelAccountNumber, "
							+ "ORG5.OU_NUM CompanyAccountNumber, "
							+ "ORGX5.ATTRIB_41 CompanyCustomerCategory, "
							+ "ORGX5.ATTRIB_07 CompanyCustomerType, "
							+ "ORGX5.ATTRIB_34 CompanyIDDocType, "
							+ "ORGX5.ATTRIB_35 CompanyIDNumber, "
							+ "ORGX5.ATTRIB_53 CompanyOfficeLocation, "
							+ "ORGFNX.FIN_METH_CD CompanyType, "
							+ "ORGFNX.INCORP_COUNTRY_CD CompanyCountryOfOrigin, "
							+ "ORGFNX.CURR_ASSET Companycapital, "
							+ "ORGLSX.NUM_EMPL CompanyNoOfEmployees, "
							+ "ORGLSX.NUM_OFFICE CompanyNoOfOffices, "
							+ "corp_user.LOGIN CompanyCreatedBy, "
							+ "ORG5.ROW_ID CompanyRowID, "
							+ "ORG5.CREATED CompanyCreatedDate, "
							+ "ORG5.CUST_STAT_CD CompanyStatus, "
							+ "addr.ADDR CompanyAddress, "
							+ "addr.CITY CompanyCity, "
							+ "addr.COUNTRY CompanyCountry, "
							+ "addr.ZIPCODE CompanyPostalCode, "
							+ "addrx.ATTRIB_01 CompanyPOBOX, "
							+ "CON.FST_NAME AuthorizedPersonFirstName, "
							+ "CON.MID_NAME AuthorizedPersonMiddleName, "
							+ "CON.LAST_NAME AuthorizedPersonLastName, "
							+ "CON.SEX_MF AuthorizedPersonGender, "
							+ "CON.BIRTH_DT AuthorizedPersonBithDate, "
							+ "CONX.X_NATIONALITY AuthorizedPersonNationality, "
							+ "CON.EMAIL_ADDR AuthorizedPersonEmail, "
							+ "CON.FAX_PH_NUM AuthorizedPersonFax, "
							+ "CON.CELL_PH_NUM AuthorizedPersonMobile, "
							+ "CON.WORK_PH_NUM AuthorizedPersonTelephone, "
							+ "CONUTX.CONTACT_PREF_CD AuthorizedPContactPreference, "
							+ "ORG1.NAME BillingCompanyName, "
							+ "ORGX1.ATTRIB_24 BillingCycle, "
							+ "ORGX1.X_EECC_BILL_FREQ BillingFreq, "
							+ "ORGX1.X_EECC_LANGUAGE_PREFERENCE BillingLangRef, "
							+ "ORGX1.X_EECC_MEDIA_TYPE BillingMediaType, "
							+ "ORG1.OU_NUM BillingAccountNumber, "
							+ "ORG1.ROW_ID BillingRowID, "
							+ "addr_per.ADDR BillingAddress, "
							+ "cont.BIRTH_DT BillingBirthdate, "
							+ "cont.CELL_PH_NUM BillingCellNumber, "
							+ "addr_per.CITY BillingCity, "
							+ "addr_per.COUNTRY BillingCountry, "
							+ "cont.EMAIL_ADDR BillingEmail, "
							+ "cont.FAX_PH_NUM BillingFaxNum, "
							+ "cont.FST_NAME BillingFirstName, "
							+ "cont.SEX_MF BillingGender, "
							+ "cont.LAST_NAME BillingLastName, "
							+ "cont.MID_NAME BillingMidName, "
							+ "cont.WORK_PH_NUM BillingWorkNumber, "
							+ "addr_per.ZIPCODE BillingZipCode, "
							+ "bill_user.LOGIN BillCreatedLogin");

			buf
					.append(" FROM SIEBEL.S_ADDR_PER addr, SIEBEL.S_ADDR_PER_X addrx, SIEBEL.S_ADDR_PER addr_per, SIEBEL.S_CONTACT CON, SIEBEL.S_CONTACT cont, SIEBEL.S_CONTACT_UTX CONUTX,"
							+ " SIEBEL.S_CONTACT_X CONX, Siebel.S_ORG_EXT ORG1, SIEBEL.S_ORG_EXT ORG5, SIEBEL.S_ORG_EXT_FNX ORGFNX, SIEBEL.S_ORG_EXT_LSX ORGLSX,"
							+ " SIEBEL.S_ORG_EXT_X ORGX1, SIEBEL.S_ORG_EXT_X ORGX5, SIEBEL.S_USER bill_user, SIEBEL.S_USER corp_user ");

			buf
					.append(" WHERE ORG1.ROW_ID = ORGX1.PAR_ROW_ID AND ORG1.OU_NUM= ? AND cont.ROW_ID = ORG1.PR_CON_ID AND addr_per.ROW_ID=ORG1.PR_ADDR_ID AND bill_user.ROW_ID=ORG1.CREATED_BY"
							+ " AND ORG5.ROW_ID=ORGX5.PAR_ROW_ID AND ORG5.ROW_ID=ORGFNX.PAR_ROW_ID AND ORG5.ROW_ID=ORGLSX.PAR_ROW_ID AND ORG5.ROW_ID= ORG1.MASTER_OU_ID"
							+ " AND corp_user.ROW_ID=ORG5.CREATED_BY AND CON.ROW_ID = ORG5.PR_CON_ID AND CON.ROW_ID=CONX.PAR_ROW_ID AND CON.ROW_ID=CONUTX.PAR_ROW_ID"
							+ " AND addr.ROW_ID=ORG5.PR_ADDR_Id AND addr.ROW_ID=addrx.PAR_ROW_ID");

			cs = connection.prepareStatement(buf.toString());
			cs.setString(1, aBillingNo);
			/* Execute the stored procedure in order to get the OUT parameters */
			rs = cs.executeQuery();

			if (rs.next()) {
				corporateProfileVO.setCompanyName(rs.getString("CompanyName"));
				corporateProfileVO.setCustomerLevelAccountNumber(rs
						.getString("CustomerLevelAccountNumber"));
				corporateProfileVO.setCompanyAccountNumber(rs
						.getString("CompanyAccountNumber"));
				corporateProfileVO.setCompanyCustomerCategory(rs
						.getString("CompanyCustomerCategory"));
				corporateProfileVO.setCompanyCustomerType(rs
						.getString("CompanyCustomerType"));
				corporateProfileVO.setCompanyIDDocType(rs
						.getString("CompanyIDDocType"));
				corporateProfileVO.setCompanyIDNumber(rs
						.getString("CompanyIDNumber"));
				corporateProfileVO.setCompanyOfficeLocation(rs
						.getString("CompanyOfficeLocation"));
				corporateProfileVO.setCompanyType(rs.getString("CompanyType"));
				corporateProfileVO.setCompanyCountryOfOrigin(rs
						.getString("CompanyCountryOfOrigin"));
				corporateProfileVO.setCompanycapital(rs
						.getString("Companycapital"));
				corporateProfileVO.setCompanyNoOfEmployees(rs
						.getString("CompanyNoOfEmployees"));
				corporateProfileVO.setCompanyNoOfOffices(rs
						.getString("CompanyNoOfOffices"));
				corporateProfileVO.setCompanyCreatedBy(rs
						.getString("CompanyCreatedBy"));
				corporateProfileVO
						.setCompanyRowID(rs.getString("CompanyRowID"));
				//corporateProfileVO.setCompanyCreatedDate( cs.getString(52) );
				corporateProfileVO.setCompanyStatus(rs
						.getString("CompanyStatus"));
				///Corporate Address
				CorporateAddressVO corporateAddress = new CorporateAddressVO();
				corporateAddress.setCompanyAddress(rs
						.getString("CompanyAddress"));
				corporateAddress.setCompanyCity(rs.getString("CompanyCity"));
				corporateAddress.setCompanyCountry(rs
						.getString("CompanyCountry"));
				corporateAddress.setCompanyPostalCode(rs
						.getString("CompanyPostalCode"));
				corporateAddress.setCompanyPoBox(rs.getString("CompanyPOBOX"));
				corporateProfileVO.setCorporateAddressVO(corporateAddress);

				/////Authorized Pecson
				CorporateAuthorizedPersonVO corporateAuthorizedPerson = new CorporateAuthorizedPersonVO();
				corporateAuthorizedPerson.setName(FormatterUtility.checkNull(rs
						.getString("AuthorizedPersonFirstName"))
						+ " "
						+ FormatterUtility.checkNull(rs
								.getString("AuthorizedPersonMiddleName"))
						+ " "
						+ FormatterUtility.checkNull(rs
								.getString("AuthorizedPersonLastName")));
				corporateAuthorizedPerson.setGender(rs
						.getString("AuthorizedPersonGender"));
				corporateAuthorizedPerson.setBithDate(rs
						.getString("AuthorizedPersonBithDate"));
				corporateAuthorizedPerson.setNationality(rs
						.getString("AuthorizedPersonNationality"));

				/////Authorized Person Address
				CorporateAuthorizedPersonAddressVO corporateAuthorizedPersonAddress = new CorporateAuthorizedPersonAddressVO();
				corporateAuthorizedPersonAddress.setAuthorizedPersonEmail(rs
						.getString("AuthorizedPersonEmail"));
				corporateAuthorizedPersonAddress.setAuthorizedPersonFax(rs
						.getString("AuthorizedPersonFax"));
				corporateAuthorizedPersonAddress.setAuthorizedPersonMobile(rs
						.getString("AuthorizedPersonMobile"));
				corporateAuthorizedPersonAddress
						.setAuthorizedPersonTelephone(rs
								.getString("AuthorizedPersonTelephone"));
				corporateAuthorizedPersonAddress.setContactPreference(rs
						.getString("AuthorizedPContactPreference"));

				corporateAuthorizedPerson
						.setCorporateAuthorizedPersonAddressVO(corporateAuthorizedPersonAddress);
				corporateProfileVO
						.setCorporateAuthorizedPersonVO(corporateAuthorizedPerson);

				CorporateBillingAccountVO corporateBillingAccountInfo = new CorporateBillingAccountVO();
				corporateBillingAccountInfo.setBillCompanyName(rs
						.getString("BillingCompanyName"));
				corporateBillingAccountInfo.setBillCycle(rs
						.getString("BillingCycle"));
				corporateBillingAccountInfo.setBillFreq(rs
						.getString("BillingFreq"));
				corporateBillingAccountInfo.setBillLangRef(rs
						.getString("BillingLangRef"));
				corporateBillingAccountInfo.setBillMediaType(rs
						.getString("BillingMediaType"));
				corporateBillingAccountInfo.setBillingAccountNumber(rs
						.getString("BillingAccountNumber"));
				corporateBillingAccountInfo.setBillRowID(rs
						.getString("BillingRowID"));

				CorporateBillingAccountRespVO corporateBillingAccountRespInfo = new CorporateBillingAccountRespVO();
				corporateBillingAccountRespInfo.setAddress(rs
						.getString("BillingAddress"));
				corporateBillingAccountRespInfo.setBirthdate(rs
						.getString("BillingBirthdate"));
				corporateBillingAccountRespInfo.setCellNumber(rs
						.getString("BillingCellNumber"));
				corporateBillingAccountRespInfo.setCity(rs
						.getString("BillingCity"));
				corporateBillingAccountRespInfo.setCountry(rs
						.getString("BillingCountry"));
				corporateBillingAccountRespInfo.setEmail(rs
						.getString("BillingEmail"));
				corporateBillingAccountRespInfo.setFaxNum(rs
						.getString("BillingFaxNum"));
				corporateBillingAccountRespInfo.setFirstName(rs
						.getString("BillingFirstName"));
				corporateBillingAccountRespInfo.setGender(rs
						.getString("BillingGender"));
				corporateBillingAccountRespInfo.setLastName(rs
						.getString("BillingLastName"));
				corporateBillingAccountRespInfo.setMidName(rs
						.getString("BillingMidName"));
				corporateBillingAccountRespInfo.setWorkNumber(rs
						.getString("BillingWorkNumber"));
				corporateBillingAccountRespInfo.setZipCode(rs
						.getString("BillingZipCode"));
				corporateBillingAccountRespInfo.setBillCreatedLogin(rs
						.getString("BillCreatedLogin"));
				corporateBillingAccountInfo
						.setCorporateBillingAccountRespVO(corporateBillingAccountRespInfo);
				corporateProfileVO
						.setCorporateBillingAccountVO(corporateBillingAccountInfo);
			}

		} catch (SQLException e) {
			log.debug("OracleCustomerProfileDAO > findCorporateProfile > SQLException > "+e.getMessage());
		   	throw new SystemException(e.getMessage(), e);	
		} catch(Exception e){
	   	  	log.debug("OracleCustomerProfileDAO > findCorporateProfile >  Exception > "+e.getMessage());
	   	  	throw new SystemException(e.getMessage(), e);  
	    } finally {
			JDBCUtility.closeJDBCResoucrs(connection , cs , rs);
		}
		
		return corporateProfileVO;
	}

	/**
	 * This method is used to get the msisdn list from Siebel for the given BillingAccountNumber.
	 */
	public List getBillingAccountMsisdnList(String aBillingAccount) {
		PreparedStatement preaparedStatement = null;
		ResultSet resultSet = null;
		Connection connection = null;
		List msisdnList = new ArrayList();
		CorporateEmployeeVO employeeVo = null;

		try {

			log.debug("OracleCustomerProfileDAO > getBillingAccountMsisdnList > aBillingAccount="+ aBillingAccount);

			StringBuffer sqlStatament = new StringBuffer();
	    	sqlStatament.append("SELECT distinct EECC_MSISDN_NUM msisdn from SIEBEL.S_ORG_EXT_X");
	    	sqlStatament.append(" where EECC_MSISDN_NUM is not null and row_ID in(");
	    	sqlStatament.append(" SELECT ROW_ID FROM SIEBEL.S_ORG_EXT where ACCNT_TYPE_CD='Service'");
	    	sqlStatament.append(" connect by prior row_id= par_ou_id start with OU_NUM = ?)");

	    	connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_SIEBEL));
			
	    	preaparedStatement = connection.prepareStatement(sqlStatament.toString());
	    	preaparedStatement.setString(1, aBillingAccount);
	    	resultSet = preaparedStatement.executeQuery();

	    	while (resultSet.next()) {
				log.debug("OracleCustomerProfileDAO > getBillingAccountMsisdnList > there are some msisdns");
				
				employeeVo = new CorporateEmployeeVO();
				employeeVo.setMsisdn(resultSet.getString("msisdn"));
				//employeeVo.setStatus(rset.getString("ServiceStatus"));
				//employeeVo.setPackageName(rset.getString("PackageName"));
				msisdnList.add(employeeVo);
			}
		} catch (SQLException e) {
			log.debug("OracleCustomerProfileDAO > getBillingAccountMsisdnList > SQLException > "+e.getMessage());
		   	 throw new SystemException(e.getMessage(), e);	
		} catch(Exception e){
	   	  	log.debug("OracleCustomerProfileDAO > getBillingAccountMsisdnList >  Exception > "+e.getMessage());
	   	  	throw new SystemException(e.getMessage(), e);  
	    } finally {
			JDBCUtility.closeJDBCResoucrs(connection , preaparedStatement , resultSet);
		}
		log.debug("OracleCustomerProfileDAO > getBillingAccountMsisdnList  result=" + msisdnList);
		return msisdnList;
	}
	
	/**
	 * 
	 * @param billingAccountNumber
	 * @return
	 */
	public CustomerProfileReplyVO getCustomerProfileFromMDM(String billingAccountNumber)  {
		CustomerProfileReplyVO replyVO = null;
		
		
		
		return replyVO;
	}


	public String sendToMQWithReply(String xmlRequestMessage,String requestQueueName,String replyQueueName)
			throws MobilyCommonException {
		// TODO Auto-generated method stub
		return null;
	}
	
	 public static Connection getSiebelLocalConnection() {
	    	Connection connection = null;
			try
	    	    {
	            Driver d = (Driver)Class.forName( "oracle.jdbc.driver.OracleDriver" ).newInstance();
	            
	            //connection = DriverManager.getConnection( "jdbc:oracle:thin:@10.6.11.14:1521:SIEBTEST", "eportal", "eportal");
	            connection = DriverManager.getConnection( "jdbc:oracle:thin:@10.14.11.209:1521:sblsit","eportal","eportal")  ;
	            System.out.print("DatBaseHandler > getSiebelLocalConnection >connection >" +connection);
	    	    }catch (SQLException e)
	        {
	            // TODO Auto-generated catch block
	        	System.out.print("DatBaseHandler > getSiebelLocalConnection >SQLException >" +e.getMessage());
	        } catch (InstantiationException e) {
	    		// TODO Auto-generated catch block
	        	throw new SystemException(e);
	    	} catch (IllegalAccessException e) {
	    		// TODO Auto-generated catch block
	    		throw new SystemException(e);
	    	} catch (ClassNotFoundException e) {
	    		// TODO Auto-generated catch block
	    		throw new SystemException(e);
	    	}
	        return connection;
	  }
	
	 public static void main(String args[]){
		 CustomerProfileReplyVO replyVO = new OracleCustomerProfileDAO().getCustomerProfileFromSiebel("1000113623871170");//100018729751570
		 System.out.println("accountVO.getId() : "+replyVO.getAccount().getId());
		 System.out.println("accountVO.getId().getIdNumber() : "+replyVO.getAccount().getId().getIdNumber());
		 
		 System.out.println("account type="+replyVO.getAccountType());
		 System.out.println("is master BA="+replyVO.getIsMasterBilingAccount());
		 
		 CustomerProfileReplyVO replyVO1 = new OracleCustomerProfileDAO().getCustomerProfileFromSiebel("100015971233737");//100015971233737
		 System.out.println("account type1="+replyVO1.getAccountType());
		 System.out.println("is master BA1="+replyVO1.getIsMasterBilingAccount());
	 }
}

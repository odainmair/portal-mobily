package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.IPhoneDeviceInfoDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.IPhoneDeviceDetailsVO;

/**
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OracleIPhoneDeviceInfoDAOImpl implements IPhoneDeviceInfoDAO{

	private static final Logger log = LoggerInterface.log;
    
	/**
	 * Gets different device information related to iPhone/MobilyApp
	 *  
	 * @return deviceInfoVoList
	 */
	public ArrayList<IPhoneDeviceDetailsVO> getAllIPhoneDeviceInfo() throws SystemException{
		
		log.info("OracleIPhoneDeviceInfoDAOImpl > getAllIPhoneDeviceInfo > Start");
		
		ArrayList<IPhoneDeviceDetailsVO> deviceInfoVoList = new ArrayList<IPhoneDeviceDetailsVO>();
	 
		Connection connection   = null;
	    ResultSet resultSet     = null;
	    PreparedStatement pStatement  = null;
	   	  
	   try{
		   
	      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	      
	      pStatement = connection.prepareStatement("SELECT DEVICE_ID, DEVICE_NAME FROM SR_MOBILE_DEVICE_TYPE_TBL WHERE DEVICE_ID != '100' ORDER BY DEVICE_ID");
	      resultSet  = pStatement.executeQuery();
	      
	      IPhoneDeviceDetailsVO deviceDetailsVo = null;
	     
	      while(resultSet.next()){
	    	  	deviceDetailsVo = new IPhoneDeviceDetailsVO();
		    	  	deviceDetailsVo.setDeviceId(resultSet.getString("DEVICE_ID"));
		    	  	deviceDetailsVo.setDeviceName(resultSet.getString("DEVICE_NAME"));
		      	
		    	deviceInfoVoList.add(deviceDetailsVo);
	      }
	      
	      log.debug("OracleIPhoneDeviceInfoDAOImpl > getAllIPhoneDeviceInfo > iPhone deviceInfoVoList > Done ["+deviceInfoVoList.size()+"]");
	      
	   }catch(SQLException e){
	   	  log.fatal("OracleIPhoneDeviceInfoDAOImpl > getAllIPhoneDeviceInfo > SQLException > "+e.getMessage());
	   	  throw new SystemException(e);
	   	
	   }catch(Exception e){
	   	  log.fatal("OracleIPhoneDeviceInfoDAOImpl > getAllIPhoneDeviceInfo > Exception > "+e.getMessage());
	   	  throw new SystemException(e);  
	   }
	   finally{
	   	JDBCUtility.closeJDBCResoucrs(connection , pStatement , resultSet);
	   }

	   log.info("OracleIPhoneDeviceInfoDAOImpl > getAllIPhoneDeviceInfo > End");

	   return deviceInfoVoList;
		
	}
}
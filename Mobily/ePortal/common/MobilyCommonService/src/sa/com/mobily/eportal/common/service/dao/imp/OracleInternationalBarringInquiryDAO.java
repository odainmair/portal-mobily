/*
 * Created on Feb 19, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.InternationalBarringInquiryDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OracleInternationalBarringInquiryDAO implements InternationalBarringInquiryDAO{
    private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;
    
    public int getInternationalbarringStatus(String msisdn) throws MobilyCommonException{
		log.debug("OracleInternationalBarringInquiryDAO > getInternationalbarringStatus > called");
	   int internationalbarring = -1;
	   Connection connection 	= null;
	   PreparedStatement pstmt  = null;
	   ResultSet resultSet 		= null;
	   
	   try{
	   	connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_CREDIT));
	   	String sqlStatement = "SELECT  internationalbarred FROM cc_international_status_tbl where msisdn = ?";
	   	pstmt = connection.prepareStatement(sqlStatement);
	   	pstmt.setString(1, msisdn);
	   	resultSet = pstmt.executeQuery();
	    
	   	log.debug("OracleInternationalBarringInquiryDAO > getInternationalbarringStatus > Access Db to retrieve Status");
	   	if(resultSet.next()){
	   		internationalbarring = resultSet.getInt("internationalbarred");
	   		
	   		
	   	}else{
	   		internationalbarring=0;
	   		log.debug("OracleInternationalBarringInquiryDAO > getInternationalbarringStatus > No record > default Status ["+internationalbarring+"]");
	   	}
	   	log.debug("OracleInternationalBarringInquiryDAO > getInternationalbarringStatus > The Status is "+internationalbarring);
	   }catch(Exception e){
	   	  log.fatal("OracleInternationalBarringInquiryDAO > getInternationalbarringStatus > SQLException "+e.getMessage());
	   	  throw new MobilyCommonException(e);
	   	
	   } finally{
	
	   	try{
	   		 connection.close();
	   		 connection = null;
	   		 pstmt.close();
	   		 pstmt=null;
	   		 resultSet.close();
	   		 resultSet=null;
	   	}catch(SQLException e){
	   		log.fatal("OracleInternationalBarringInquiryDAO > getInternationalbarringStatus > CLoseConnection > SQLException "+e.getMessage());
	   		}
	   	
       }
	   return internationalbarring;
	   
		
	}
	
    
    public static void main(String[] args) {
    }
}

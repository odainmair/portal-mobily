package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import org.apache.log4j.Logger;
import oracle.jdbc.OracleTypes;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.LineTypeByPackageNameDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;


public class OracleLineTypeByPackageNameDAO implements LineTypeByPackageNameDAO{
	private static final Logger log = LoggerInterface.log;

	/*
	 * This Method is repsponsible for getting all Line Type support by mobily and it's line type then save it in hashMap to use it later
	 * */
	public HashMap<String, String> loadProductLinePerPackageName()  {
	    log.debug("OracleLineTypeByPackageNameDAO > getLineTypeByPackageName > get All Package");
	    HashMap<String, String> listOfPackagePerLine = new HashMap<String, String>();
	    CallableStatement  cstmt = null;
		ResultSet rset = null;
		Connection connection = null;
			try {
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_SIEBEL));
//				connection = DataBaseConnectionHandler.getLocalSiebelConnection();
				
				StringBuffer sqlStatement = new StringBuffer("{ CALL SIEBEL.PackageInfo.GetAllProductLines(?) }");
				
				log.debug("OracleLineTypeByPackageNameDAO > getLineTypeByPackageName > sqlStatement = "+sqlStatement.toString());
				
	            cstmt = connection.prepareCall(sqlStatement.toString());
	            
	            cstmt.registerOutParameter(1,OracleTypes.CURSOR);
	            cstmt.execute();
	            rset=(ResultSet)cstmt.getObject(1);
				String packagename = "";
				String LineType = "";
	            while(rset.next()){
	            	packagename = rset.getString("PACKAGENAME");
	            	LineType = rset.getString("PRODUCTLINE");
	            	listOfPackagePerLine.put(packagename, LineType);
					  
				}
		 } catch (SQLException e) {
				log.fatal("OracleLineTypeByPackageNameDAO > getLineTypeByPackageName > SQLException "+e.getMessage());
		} finally {
		    	JDBCUtility.closeJDBCResoucrs(connection,cstmt,rset);			
		}
		log.debug("OracleLineTypeByPackageNameDAO > getLineTypeByPackageName > done ");
		return listOfPackagePerLine;
	}    

	
	/* (non-Javadoc)
	 * this method responisble for loading the product desc & line Type from EEDB DB , and we will use it to get the lineType per the product name
	 * @see sa.com.mobily.eportal.common.service.dao.ifc.LineTypeByPackageNameDAO#LoadKineTypeByProductDesc()
	 */
	public HashMap<String, String> LoadKineTypeByProductDesc()  {
	    log.debug("OracleLineTypeByPackageNameDAO > LoadKineTypeByProductDesc > get All Package");
	    HashMap<String, String> listOfPackagePerLine = new HashMap<String, String>();
	    PreparedStatement  cstmt = null;
		ResultSet rset = null;
		Connection connection = null;
			try {
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
//				connection = DataBaseConnectionHandler.getLocalConnection();
				
				StringBuffer sqlStatement = new StringBuffer("SELECT * FROM SR_PRODUCT_LINE_TBL");
				
				log.debug("OracleLineTypeByPackageNameDAO > LoadKineTypeByProductDesc > sqlStatement = "+sqlStatement.toString());
				
	            cstmt = connection.prepareStatement(sqlStatement.toString());
	            cstmt.execute();
	            rset=cstmt.executeQuery();
				String productName = "";
				String productLine = "";
	            while(rset.next()){
	            	productName = rset.getString("PRODUCT_LINE_DESC");
	            	productLine = rset.getString("PRODUCT_LINE_VALUE");
	            	listOfPackagePerLine.put(productName, productLine);
				}
		 } catch (SQLException e) {
				log.fatal("OracleLineTypeByPackageNameDAO > LoadKineTypeByProductDesc > SQLException "+e.getMessage());
		} finally {
		    	JDBCUtility.closeJDBCResoucrs(connection,cstmt,rset);			
		}
		log.debug("OracleLineTypeByPackageNameDAO > LoadKineTypeByProductDesc > done ");
		return listOfPackagePerLine;
	}    
	
}

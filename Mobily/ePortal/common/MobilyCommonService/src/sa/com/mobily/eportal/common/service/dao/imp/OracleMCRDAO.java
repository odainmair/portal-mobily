package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.MCRDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.MCRDetailsVO;

/**
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OracleMCRDAO implements MCRDAO {
    
    private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;
    
	
	/**
	 * Responsible for getting the details from MCR database 
	 * @param mode
	 * @param billAcctNumber
	 * @param eligiblePackages
	 * @return ArrayList
	 */
    public ArrayList getMCRDetails(int mode, String billAcctNumber, String eligiblePackages)  {
	    log.debug("OracleMCRDAO > getMCRDetails > called for mode = "+mode);
	    log.debug("OracleMCRDAO > getMCRDetails > called for billing number = "+billAcctNumber);
	    
	    CallableStatement  cstmt = null;
	    MCRDetailsVO replyVO = null;
		ResultSet rset = null;
		Connection connection = null;
		ArrayList mcrDeatilsVolist = new ArrayList();
			try {
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_MCR));

				StringBuffer sqlStatement = new StringBuffer("{ CALL P_MCR_STATIC_IP_BY_BILLING_NO(?, ?, ?, ?, ?,?) }");

				cstmt = connection.prepareCall(sqlStatement.toString());
				
	            cstmt.setInt(1 , mode);
	            cstmt.setString(2,billAcctNumber);
	            cstmt.setString(3,eligiblePackages);
	            cstmt.setString(4,"");
	            cstmt.setString(5,"");
	            cstmt.registerOutParameter(6,OracleTypes.CURSOR);
            
            cstmt.execute();

            rset=(ResultSet)cstmt.getObject(6);
			
            while (rset.next()){
            	replyVO = new MCRDetailsVO();

					replyVO.setBillAcctNumber(rset.getString("BILL_ACCT_NO"));
					replyVO.setServiceAcctNumber(rset.getString("SRVC_ACCT_NO"));
					replyVO.setMsisdn(rset.getString("MSISDN"));
					replyVO.setSim(rset.getString("SIM"));
					replyVO.setPayType(rset.getString("PAY_TYPE"));
					replyVO.setIpAddress(rset.getString("IP_ADDRESS"));
					replyVO.setIpType(rset.getString("IP_TYPE"));
					replyVO.setPackageId(rset.getString("PACKAGE_ID"));
					replyVO.setApn(rset.getString("APN"));
					replyVO.setWeb1wap1Status(rset.getString("WEB1WAP1"));
					replyVO.setEportalAliasName(rset.getString("EPORTAL_ALIAS_NAME"));
					replyVO.setEportalEmail(rset.getString("EPORTAL_EMAIL"));
					//replyVO.setPackageNameSiebel(rset.getString("PACKAGE_NAME_SIEBEL"));

				mcrDeatilsVolist.add(replyVO);
            }
		 } catch (SQLException e) {
				log.fatal("OracleMCRDAO > getMCRDetails > SQLException "+e.getMessage());
				throw new SystemException(e);
		} finally {
		    	JDBCUtility.closeJDBCResoucrs(connection,cstmt,rset);			
		}
		log.debug("OracleMCRDAO > getMCRDetails > number of items :"+mcrDeatilsVolist.size());
		return mcrDeatilsVolist;
	}    
}
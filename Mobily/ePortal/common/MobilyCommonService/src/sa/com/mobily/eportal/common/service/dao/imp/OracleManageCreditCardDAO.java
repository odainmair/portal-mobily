/*
 * Created on Sept 16, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.ManageCreditCardDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.valueobject.common.ManagedMSISDNDataVO;
import sa.com.mobily.eportal.common.service.vo.CreditCardPaymentRequestVO;



/**
 * @author r.agrawal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OracleManageCreditCardDAO implements ManageCreditCardDAO{
	
	private static final Logger log = LoggerInterface.log;
	
	 /**
	  * Date: Sept 17, 2009
	  * Description: This method is used to get the user managed number from EEDB
	  * @author r.agarwal.mit
	  * @param aMsisdn
	  * @return List
	  */
	public List getRelatedMSISDNList(String aMsisdn) {
		log.debug("OracleManageCreditCardDAO > getRelatedMSISDNList > aMsisdn > "+aMsisdn);
		ResultSet resultSet = null;
		List l_MSISDNList = new ArrayList();
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT t1.MSISDN, t1.USERNAME FROM USER_MANAGED_NO t1 JOIN USER_ACCOUNT t2");
			sqlQuery.append(" ON t1.ID = t2.ID WHERE t2.MSISDN = ?");
			
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1,aMsisdn);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				ManagedMSISDNDataVO managedMsisdnDataVO = new ManagedMSISDNDataVO();
				managedMsisdnDataVO.setMSISDN(resultSet.getString(1));
				managedMsisdnDataVO.setName(resultSet.getString(2));
				
				l_MSISDNList.add(managedMsisdnDataVO);
			}
		} catch (SQLException e) {
			log.fatal("OracleManageCreditCardDAO > getRelatedMSISDNList > SQLException > "+e.getMessage());
		   	throw new SystemException(e.getMessage(), e);	
		} catch(Exception e) {
	   	  	log.fatal("OracleManageCreditCardDAO > getRelatedMSISDNList >  Exception > "+e.getMessage());
	   	  	throw new SystemException(e.getMessage(), e);  
	    } finally {
	    	JDBCUtility.closeJDBCResoucrs(connection , statement , resultSet);
	    }		
		log.debug("OracleManageCreditCardDAO  > getRelatedMSISDNList > returned list size: " + l_MSISDNList.size());
		return l_MSISDNList;
	}

	
	 /**
	  * Date: Sept 17, 2009
	  * Description: This method is used to get the next transaction id from the Sequence created in EEDB
	  * @author r.agarwal.mit
	  * @return String
	  */
	public String getThirdGenerationTransId() {
		
		log.debug("OracleManageCreditCardDAO > getThirdGenerationTransId > called");
		
	    Connection connection = null; 
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		StringBuffer query = null;
		int counter = 0;
		
		try {
		    query = new StringBuffer("SELECT TRANS_ID_SEQ.NEXTVAL TransId from dual");
		    
		    connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		    preparedStatement = connection.prepareStatement(query.toString());
		    resultSet = preparedStatement.executeQuery();
		    
		    if(resultSet.next()) {
		    	counter = resultSet.getInt("TransId");
			}
		} catch (SQLException e) {
			log.fatal("OracleManageCreditCardDAO > getThirdGenerationTransId > SQLException > "+e.getMessage());
		   	throw new SystemException(e.getMessage(), e);	
		} catch(Exception e) {
	   	  	log.fatal("OracleManageCreditCardDAO > getThirdGenerationTransId >  Exception > "+e.getMessage());
	   	  	throw new SystemException(e.getMessage(), e);  
	    } finally {
	    	JDBCUtility.closeJDBCResoucrs(connection , preparedStatement , resultSet);
	    }		
	    log.debug("OracleManageCreditCardDAO > getThirdGenerationTransId > counter > ePortal_"+counter);
		return "ePortal_" + counter;
	}
	
	 /**
	  * Date: Sept 17, 2009
	  * Description: This method is used to audit the credit card payment transaction in EEDB
	  * @author r.agarwal.mit
	  * @param objCreditCardPaymentRequestVO
	  */
	public void auditCreditCardTransactionInDB(CreditCardPaymentRequestVO requestVO) {
		log.debug("OracleManageCreditCardDAO > auditCreditCardTransactionInDB > called ");
		Connection connection   = null;
	    PreparedStatement statement = null;
	    try {
	    	StringBuffer sqlStatament = new StringBuffer();
	    	sqlStatament.append("INSERT INTO SR_CREDITCARD_AUDIT_TBL(TRANS_ID,AMOUNT,MSISDN,REQUESTORCHANNELID,TRANS_DATE)");
	    	sqlStatament.append(" VALUES(?,?,?,?,?)");
			//connection = DataBaseHandler.getConnection();
	    	connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement = connection.prepareStatement(sqlStatament.toString());
	    	statement.setString(1, requestVO.getTransactionID());
	    	statement.setFloat(2, Float.parseFloat(requestVO.getAmount()));
	    	statement.setString(3, requestVO.getMSISDN());
	    	statement.setString(4, requestVO.getHeader().getRequestorChannelId());
	    	statement.setString(5, requestVO.getTransactionDate());
	    	int updatedItem  = statement.executeUpdate();
	    	log.debug("OracleManageCreditCardDAO > auditCreditCardTransactionInDB > updatedItem = "+updatedItem);
	   	} catch (SQLException e) {
			log.debug("OracleManageCreditCardDAO > auditCreditCardTransactionInDB > SQLException > "+e.getMessage());
		   	throw new SystemException(e.getMessage(), e);	
		} catch(Exception e) {
	   	  	log.debug("OracleManageCreditCardDAO > auditCreditCardTransactionInDB >  Exception > "+e.getMessage());
	   	  	throw new SystemException(e.getMessage(), e);  
	    } finally {
	    	JDBCUtility.closeJDBCResoucrs(connection , statement , null);
	    }	
	}	
	

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.creditcardpayment.dao.ifc.CreditCardPaymentDAO#manageCreditCards(java.lang.String)
	 */
	public String manageCreditCards(String xmlRequestMessage,String requesterChannelFunction) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * This method is used to Insert the RECORD
	 * @param CreditCardPaymentRequestVO
	 * @throws SystemException
	 * @return boolean
	 */
	public boolean insertTransactionPIN(CreditCardPaymentRequestVO requestVO) {
		log.debug("OracleManageCreditCardDAO > insertTransactionPIN > Called ");
		Calendar cal =  null;
		Connection connection   = null;
	    PreparedStatement pstm  = null;
       	boolean insertFlag = false;
	   try{
	   	  StringBuffer sqlStatament = new StringBuffer();
	   	  sqlStatament.append("INSERT INTO SR_CC_ACTIVATION_TBL (");
	   	  sqlStatament.append(" BILLING_ACCOUNT_NUMBER, ACTIVATION_KEY,ACTIVATION_TYPE,EXPIRATION_DATE ) ");
	   	  sqlStatament.append(" VALUES ( ? , ? , ? , ?)");

	   	  connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	      pstm = connection.prepareStatement(sqlStatament.toString());
	      pstm.setString(1 , requestVO.getBillAccountNumber());
	      pstm.setString(2 , requestVO.getTransactionPIN());
	      pstm.setString(3 , requestVO.getActivationType());
	      
	      cal =  new GregorianCalendar();
	      int min = cal.get(Calendar.MINUTE)+30;
	      cal.set(Calendar.MINUTE , min);
	      pstm.setTimestamp(4 , new Timestamp(cal.getTime().getTime()));
	      
	      int updatedItem  = pstm.executeUpdate();
	      
	      if(updatedItem > 0)
	    	  insertFlag = true;
	      
	      log.debug("OracleManageCreditCardDAO > insertTransactionPIN > Done : Record Added = "+updatedItem);
	   	} catch (SQLException e) {
	   		log.debug("OracleManageCreditCardDAO > insertTransactionPIN > SQLException > "+e.getMessage());
	   		throw new SystemException(e.getMessage(), e);	
	   	} catch(Exception e) {
	  	  	log.debug("OracleManageCreditCardDAO > insertTransactionPIN >  Exception > "+e.getMessage());
	  	  	throw new SystemException(e.getMessage(), e);  
	   	} finally {
	   		JDBCUtility.closeJDBCResoucrs(connection , pstm , null);
	   	}		
	   	return insertFlag;
	}
	
	
	/**
	 * This method is used to check for the valid Activation Code
	 * @param CreditCardPaymentRequestVO
	 * @return boolean
	 * @throws SystemException
	 */
	public boolean isValidRecord(CreditCardPaymentRequestVO requestVO) {
		   boolean valid = false;
		   log.debug("OracleManageCreditCardDAO > isValidRecord > Called");
		   Connection connection   = null;
		   ResultSet resultset     = null;
		   PreparedStatement pstm  = null;
		   try {
			   	StringBuffer str = new StringBuffer();
			   	str.append(" SELECT * FROM SR_CC_ACTIVATION_TBL" );
				str.append(" WHERE BILLING_ACCOUNT_NUMBER = ?  AND ACTIVATION_TYPE = ?" );
				str.append(" AND sysdate <= EXPIRATION_DATE");
	
				log.debug("OracleManageCreditCardDAO > isValidRecord > where Billing Account Number = ["+requestVO.getBillAccountNumber()+"]");
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
				pstm       = connection.prepareStatement(str.toString());
				pstm.setString(1 , requestVO.getBillAccountNumber());
				pstm.setString(2 , requestVO.getActivationType());
		      
				resultset  = pstm.executeQuery();
				if(resultset.next()) {
			      	valid = true;
			    }
			    log.debug("OracleManageCreditCardDAO > isValidRecord > Done ["+valid+"]");
		   	} catch(SQLException e) {
		   		log.fatal("OracleManageCreditCardDAO > isValidRecord  > SQLException > "+e.getMessage());
		   		throw new SystemException(e.getMessage(),e);
		   	} catch(Exception e) {
		   		log.fatal("OracleManageCreditCardDAO > isValidRecord Exception > "+e.getMessage());
		   		throw new SystemException(e.getMessage(),e);
		   	} finally {
		   		JDBCUtility.closeJDBCResoucrs(connection , pstm , resultset);
		   	}
		   	return valid;
	}	
	
	/**
	 * This method is used to check for the valid Activation Code
	 * @param CreditCardPaymentRequestVO
	 * @return boolean
	 * @throws SystemException
	 */
	public boolean isValidActivationCode(CreditCardPaymentRequestVO requestVO) {
		   boolean valid = false;
		   log.debug("OracleManageCreditCardDAO > isValidActivationCode > Called");
		   Connection connection   = null;
		   ResultSet resultset     = null;
		   PreparedStatement pstm  = null;
		   try {
			   	StringBuffer str = new StringBuffer();
			   	str.append(" SELECT * FROM SR_CC_ACTIVATION_TBL" );
				str.append(" WHERE BILLING_ACCOUNT_NUMBER = ?  AND ACTIVATION_KEY = ? AND ACTIVATION_TYPE = ?" );
				str.append(" AND sysdate <= EXPIRATION_DATE");
	
				log.debug("OracleManageCreditCardDAO > isValidActivationCode > where Billing Account Number = ["+requestVO.getBillAccountNumber()+"] ");
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
				pstm       = connection.prepareStatement(str.toString());
				pstm.setString(1 , requestVO.getBillAccountNumber());
				pstm.setString(2 , requestVO.getTransactionPIN());
				pstm.setString(3 , requestVO.getActivationType());
		      
				resultset  = pstm.executeQuery();
				if(resultset.next()) {
			      	valid = true;
			    }
			    log.debug("OracleManageCreditCardDAO > isValidActivationCode > Done ["+valid+"]");
		   	} catch(SQLException e) {
		   		log.fatal("OracleManageCreditCardDAO > isValidActivationCode  > SQLException > "+e.getMessage());
		   		throw new SystemException(e.getMessage(),e);
		   	} catch(Exception e) {
		   		log.fatal("OracleManageCreditCardDAO > isValidActivationCode Exception > "+e.getMessage());
		   		throw new SystemException(e.getMessage(),e);
		   	} finally {
		   		JDBCUtility.closeJDBCResoucrs(connection , pstm , resultset);
		   	}
		return valid;
	}

	/**
	 * This method is used to clear invalid record
	 * @param msisdn
	 * @throws SystemException
	 */
	public void clearInvalidRecord(CreditCardPaymentRequestVO requestVO) {
		log.debug("OracleManageCreditCardDAO > clearInvalidRecord > Called  Billing Account Number :"+requestVO.getBillAccountNumber());
		Connection connection   = null;
		PreparedStatement pstm  = null;
       	  
	    try{
	    	StringBuffer sqlStatament = new StringBuffer();
		   	sqlStatament.append("DELETE FROM SR_CC_ACTIVATION_TBL WHERE BILLING_ACCOUNT_NUMBER = ? AND ACTIVATION_TYPE = ?");
		   	
		   	connection 	= DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		    pstm 		= connection.prepareStatement(sqlStatament.toString());
		    pstm.setString(1 , requestVO.getBillAccountNumber());
		    pstm.setString(2 , requestVO.getActivationType());
		    int updatedItem  = pstm.executeUpdate();
		    log.debug("OracleManageCreditCardDAO > clearInvalidRecord > Done : Record Cleared = "+updatedItem);
	    } catch(SQLException e){
	    	log.fatal("OracleManageCreditCardDAO > clearInvalidRecord > SQLException > "+e.getMessage());
	    	throw new SystemException(e.getMessage(),e);
	    } catch(Exception e){
	   		log.fatal("OracleManageCreditCardDAO > clearInvalidRecord >  Exception > "+e.getMessage());
	   		throw new SystemException(e.getMessage(),e);
	    } finally{
	   		JDBCUtility.closeJDBCResoucrs(connection , pstm , null);
	    }
	}		
	
	public String getValidPINCode(CreditCardPaymentRequestVO requestVO) {
		   String pinCode = "";
		   log.debug("OracleManageCreditCardDAO > getValidPINCode > Called");
		   Connection connection   = null;
		   ResultSet resultset     = null;
		   PreparedStatement pstm  = null;
		   try {
			   	StringBuffer str = new StringBuffer();
			   	str.append(" SELECT ACTIVATION_KEY FROM SR_CC_ACTIVATION_TBL" );
				str.append(" WHERE BILLING_ACCOUNT_NUMBER = ?  AND ACTIVATION_TYPE = ?" );
				str.append(" AND sysdate <= EXPIRATION_DATE");
	
				log.debug("OracleManageCreditCardDAO > isValidRecord > where Billing Account Number = ["+requestVO.getBillAccountNumber()+"]");
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
				pstm       = connection.prepareStatement(str.toString());
				pstm.setString(1 , requestVO.getBillAccountNumber());
				pstm.setString(2 , requestVO.getActivationType());
		      
				resultset  = pstm.executeQuery();
				if(resultset.next()) {
					pinCode = resultset.getString("ACTIVATION_KEY");
			    }
			    log.debug("OracleManageCreditCardDAO > getValidPINCode > Done ["+pinCode+"]");
		   	} catch(SQLException e) {
		   		log.fatal("OracleManageCreditCardDAO > getValidPINCode  > SQLException > "+e.getMessage());
		   		throw new SystemException(e.getMessage(),e);
		   	} catch(Exception e) {
		   		log.fatal("OracleManageCreditCardDAO > getValidPINCode Exception > "+e.getMessage());
		   		throw new SystemException(e.getMessage(),e);
		   	} finally {
		   		JDBCUtility.closeJDBCResoucrs(connection , pstm , resultset);
		   	}
		   	return pinCode;
	}

}

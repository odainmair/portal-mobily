/*
 * Created on Feb 26, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;

import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.AddOnVO;

/**
 * @author m.abdelmaged
 *
 * This class is used to 
 */
public class OracleSohoAddOnTableDAO  {
	private static final Logger log = LoggerInterface.log;
	private  final int RATE = 0;
	private  final int FLAT_RATE = 1;
	public  ArrayList<AddOnVO> getSohoAddOnRates()
	{
		log.info("OracleSohoAddOnTableDAO >> getSohoAddOnRates   started");
 	    Connection connection = null;
	    PreparedStatement statement = null;
	    ResultSet results = null;

		ArrayList<AddOnVO> itemsList = new ArrayList<AddOnVO>();
		AddOnVO addOnVO = null;

		try
		{
			String sqlQuery = "SELECT ADD_ON_ID, ADD_ON_NAME, MINIMUM_TIER_VALUE, MAXIMUM_TIER_VALUE, " +
							  "RATE_MONTHLY, PAY_TYPE FROM SR_SOHO_ADD_ON_RATES";
					
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			results = statement.executeQuery();				
					
		      while(results.next())
		      {
		    	  addOnVO = new AddOnVO();
		    	  addOnVO.setAddOnId(results.getString(1));
		    	  addOnVO.setAddOnName(results.getString(2));
		    	  addOnVO.setMinTierVal(results.getString(3));
		      	  addOnVO.setMaxTierVal(results.getString(4));
		      	  addOnVO.setRate(results.getFloat(5));
		      	  addOnVO.setPayType(results.getInt(6));
		      	 
		      	  itemsList.add(addOnVO);
		      	  
		      	
		      }
		      
		      log.debug("itemsList Size   ::  " +itemsList.size());

		}
		catch(Exception e)
		{
			throw new SystemException(e);
		}
		finally
		{
			JDBCUtility.closeJDBCResoucrs(connection , statement , results);
		}
		log.info("OracleSohoAddOnTableDAO >> getSohoAddOnRates   ended");
		return itemsList;
	}

}


//This DAO class has common dao functions for ePortal DataBase

package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.ePortalDBDaoIfc;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.valueobject.common.BackEndMessagesVO;
import sa.com.mobily.eportal.common.service.valueobject.common.BackageDataVO;
import sa.com.mobily.eportal.common.service.vo.InactiveObject;
import sa.com.mobily.eportal.common.service.vo.LoyaltyReasonCodeVO;
import sa.com.mobily.eportal.common.service.vo.ManagedWimaxDataVO;
import sa.com.mobily.eportal.common.service.vo.SecondaryEmailVO;
import sa.com.mobily.eportal.common.service.vo.SessionVO;
import sa.com.mobily.eportal.common.service.vo.SkyUserAccountVO;
import sa.com.mobily.eportal.common.service.vo.UserAccountVO;
import sa.com.mobily.eportal.common.service.vo.UserRelatedNoVO;



/**
 * @author m.elbastawisi
 *
 */

public class ePortalDBDao implements ePortalDBDaoIfc{
	private static final Logger log = LoggerInterface.log;
	
	public static final int VIEW_BILL_DETAILS = 1;				// 0000000000000001
	public static final int MANAGE_SERVICES = 2;				// 0000000000000010
	public static final int ADD_OTHER_NUM = 4;

	private Object ArrayList;
	

	
	/**
	 * 
	 */
	public ArrayList getRelatedMSISDNList(String aMsisdn) {
		
		
		ResultSet l_result = null;
		ArrayList l_MSISDNList = new ArrayList();
		Connection connection = null;
		PreparedStatement statement=null;
		try {
			String sqlQuery = "select t1.MSISDN, t1.USERNAME,t1.LINE_TYPE,t1.customertype from USER_MANAGED_NO t1 JOIN user_account t2 on t1.id=t2.id WHERE t2.msisdn=?  and active=1";
	
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			//log.fatal("Ayman test 1");
			statement  = connection.prepareStatement(sqlQuery.toString());
			//log.fatal("Ayman test 2");
			statement.setString(1,aMsisdn);
			log.debug("ePortalDBDao - Before executing the getRelatedMSISDNList query");
			l_result = statement.executeQuery();
			log.debug("ePortalDBDao - After executing the getRelatedMSISDNList query");
			int i = 0;
			while(l_result.next())
			{
				log.debug("ePortalDBDao - getRelatedMSISDNList - related number " + ++i);
				//ManagedMSISDNDataVO temp = new ManagedMSISDNDataVO();
				ManagedWimaxDataVO temp = new ManagedWimaxDataVO();
				temp.setMSISDN(l_result.getString(1));
				temp.setName(l_result.getString(2));
				temp.setLineType(l_result.getInt(3));
				temp.setType(l_result.getString(4));
				
				l_MSISDNList.add(temp);
			}
			log.debug("ePortalDBDao - getRelatedMSISDNList befor return");
		
	}catch(Exception e){
	    throw new SystemException(e);
	}finally{
	    JDBCUtility.closeJDBCResoucrs(connection , statement , null);
	}
		log.debug("ePortalDBDao - getRelatedMSISDNList the returned list size: " + l_MSISDNList.size());
		return l_MSISDNList;
	}

	/**
	 * 
	 * @param aMsisdn
	 * @param aManagedData
	 * @throws Exception
	 * @throws SQLException
	 */
/*	public void insertRelatedMSISDN(String aMsisdn, ManagedWimaxDataVO aManagedData) throws Exception,SQLException{
	
		Connection connection = null;
		PreparedStatement statement=null;
		try {
			String sqlQuery = "INSERT INTO USER_MANAGED_NO(ID,MSISDN,PRIVILEGE,USERNAME) VALUES(?,?,?,?)";
	
			//connection = DataBaseHandler.getConnection();
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setInt(1,getUserAccountID(aMsisdn));
			statement.setString(2,aManagedData.getMSISDN());
			statement.setInt(3,aManagedData.getPrivelege());
			statement.setString(4,aManagedData.getName());
			statement.execute();
		}
		finally{
		    JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
		
			updatePOIDList(aMsisdn);
		
	}*/
	
	
	/**
	 * this methed is responsible for adding related number into the customer account
	 * @param UserAccountVO(id , msisdn , username , service account number , line type)
	 * @throws Exception
	 * @throws SQLException
	 */
	public void addingNewRelatedNumber(UserAccountVO userAccount) throws SystemException{
		Connection connection = null;
		PreparedStatement statement=null;
		
		try {
			
			
			String sqlQuery = "INSERT INTO USER_MANAGED_NO(ID,MSISDN,PRIVILEGE,USERNAME,SERVICE_ACT_NUM , LINE_TYPE) VALUES(?,?,?,?,?,?)";
	
			//connection = DataBaseHandler.getConnection();
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setInt(1,userAccount.getId());
			statement.setString(2,userAccount.getMsisdn());
			statement.setInt(3,7);
			statement.setString(4,userAccount.getUserName());
			statement.setString(5,userAccount.getServiceAccountNumber());
			statement.setInt(6,userAccount.getLineType());
			statement.execute();
		}
		catch(SQLException e){
            log.fatal("SQLException " + e.getMessage());
            throw new SystemException(e);
             
         }
        catch(Exception e){
            log.debug("Exception " + e.getMessage());
           throw new SystemException(e);
            
        }finally{
		    JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
	}
	/**
	 * 
	 * @param aMsisdn
	 * @param aManagedData
	 * @throws Exception
	 * @throws SQLException
	 */
	public void insertNewMSISDNUnderMyAccountForRedesign(String aMsisdn, ManagedWimaxDataVO aManagedData, String packType, String custType) throws Exception{
	
		Connection connection = null;
		PreparedStatement statement=null;
		
		try {
			
			log.debug("ePortalDBDao > insertNewMSISDNUnderMyAccountForRedesign > Service# >"+aManagedData.getServiceActNum());
			
			String sqlQuery = "INSERT INTO USER_MANAGED_NO(ID,MSISDN,PRIVILEGE,USERNAME,SERVICE_ACT_NUM) VALUES(?,?,?,?,?)";
	
			//connection = DataBaseHandler.getConnection();
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setInt(1,getUserAccountID(aMsisdn));
			statement.setString(2,aManagedData.getMSISDN());
			statement.setInt(3,aManagedData.getPrivelege());
			statement.setString(4,aManagedData.getName());
			statement.setString(5,aManagedData.getServiceActNum());
			statement.execute();
		}
		catch(SQLException e){
            log.fatal("SQLException " + e.getMessage());
            throw new SystemException(e);
             
         }
        catch(Exception e){
            log.debug("Exception " + e.getMessage());
           throw new SystemException(e);
            
        }finally{
		    JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
			if(custType=="2"){
				updatePOIDListforRegistration(aManagedData.getMSISDN(),packType);
			}
			
		
	}
	
	/**
	 * 
	 * @param aMsisdn
	 * @return
	 * @throws Exception
	 * @throws SQLException
	 */
	public int getUserAccountID(String aMsisdn)throws Exception {
		int l_ID = -1;
		Connection connection = null;
		PreparedStatement statement=null;
		ResultSet l_result = null;
		try {
			String sqlQuery = "Select id from user_account where msisdn=?";
	
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1,aMsisdn);
			l_result = statement.executeQuery();
			
			if(l_result.next())
				l_ID = l_result.getInt(1);
		}catch(SQLException sqlEx){// if one update fail continue for the rest of the iteratore values
		    log.fatal("MobilyCommonService --> ePortalDBDao --> getUserAccountID :: SQLException "+sqlEx);
		    throw new SystemException(sqlEx);
		}
		catch(Exception e){
			log.fatal("MobilyCommonService --> ePortalDBDao --> getUserAccountID :: Exception "+e);
			throw new SystemException(e);
		}
		finally{
		    JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
		return l_ID;
	}
	
	/**
	 * 
	 * @param aMsisdn
	 * @return
	 * @throws Exception
	 * @throws SQLException
	 */
	public int getUserAccountIDFromManagedTbl(String aMsisdn)throws Exception {
		int l_ID = -1;
		Connection connection = null;
		PreparedStatement statement=null;
		ResultSet l_result = null;
		try {
			String sqlQuery = "Select id from user_managed_no where msisdn=?";
	
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1,aMsisdn);
			l_result = statement.executeQuery();
			
			if(l_result.next())
				l_ID = l_result.getInt(1);
		}catch(SQLException sqlEx){// if one update fail continue for the rest of the iteratore values
		    log.fatal("MobilyCommonService --> ePortalDBDao --> getUserAccountIDFromManagedTbl :: SQLException "+sqlEx);
		    throw new SystemException(sqlEx);
		}
		catch(Exception e){
			log.fatal("MobilyCommonService --> ePortalDBDao --> getUserAccountIDFromManagedTbl :: Exception "+e);
			throw new SystemException(e);
		}
		finally{
		    JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
		return l_ID;
	}

	/**
	 * 
	 * @param aMsisdn
	 * @throws Exception
	 */
	public void updatePOIDList(String aMsisdn) {
		log.debug("in updatePOIDList ---> " + aMsisdn);
		Connection connection = null;
		PreparedStatement statement=null;
			try {
			String billAccountPOID = null;
			billAccountPOID = BillingInfoDAO.getBillingAccountPOID(aMsisdn);
		
		if(billAccountPOID != null) {
			
			
				String sqlQuery = "INSERT INTO user_bill_poid(msisdn, poid) VALUES(?,?)";
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
				statement  = connection.prepareStatement(sqlQuery.toString());
				statement.setString(1,aMsisdn);
				statement.setString(2,billAccountPOID);
				statement.executeUpdate();
			}
			}catch(SQLException sqlEx){// if one update fail continue for the rest of the iteratore values
			    log.fatal("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: SQLException "+sqlEx);
			    throw new SystemException(sqlEx);
			}
			catch(Exception e){
				log.fatal("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: Exception "+e);
				throw new SystemException(e);
			}finally{
			    JDBCUtility.closeJDBCResoucrs(connection , statement , null);
			}
		
	}

	/**
	date: Jun 18, 2009
	Description: This method is used to update the POID for Corporate. This will be invoked while CAP registration.
	@param aMsisdnList
	@author Suresh V.
	@Modified by SUresh V on Dec 28 2009: To check weather customer is already having the record in user_bill_poid table before inserting
	*/
	
	public void updatePOIDList(ArrayList aMsisdnList){
	
		HashMap pOIDHash = null;
		Map msisdnExistanceMap = new HashMap();
		log.info("MobilyCommonService --> ePortalDBDao --> updatePOIDList : start");
		try {
			pOIDHash = BillingInfoDAO.getBillingAccountPOIDList(aMsisdnList);
		}catch (Exception e) {
			log.fatal("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: Exception while fetching billingAccountPOIDList :"+e);	
			throw new SystemException(e);
		}
		if(pOIDHash != null && !pOIDHash.isEmpty())
		{
			Iterator iterator = pOIDHash.keySet().iterator();
				
			Connection connection = null;
			PreparedStatement preparedStatement = null;
			PreparedStatement preparedStatementInsert = null;
			PreparedStatement preparedStatementUpdate = null;
			ResultSet resultSet = null;
			try {
				
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
				String sqlQuerySelect = "SELECT * FROM user_bill_poid WHERE msisdn = ? ";
				String sqlQueryUpdate = "UPDATE user_bill_poid SET POID = ? WHERE msisdn = ?";
				String sqlQueryInsert = "INSERT INTO user_bill_poid(msisdn, poid) VALUES(?,?)";
				
				preparedStatement  = connection.prepareStatement(sqlQuerySelect.toString());
				preparedStatementInsert = connection.prepareStatement(sqlQueryInsert.toString());
				preparedStatementUpdate = connection.prepareStatement(sqlQueryUpdate.toString());
				log.debug("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: after creating the prepared statements.");
				
				while(iterator.hasNext()) {
					String msisdn = iterator.next().toString();
					preparedStatement.setString(1, msisdn);
					resultSet = preparedStatement.executeQuery();
					if(resultSet.next())
						msisdnExistanceMap.put(msisdn, ConstantIfc.MSIDN_EXISTANCE_TRUE);
					else
						msisdnExistanceMap.put(msisdn, ConstantIfc.MSIDN_EXISTANCE_FALSE);
					
					preparedStatement.clearParameters();
				}
				
				if(preparedStatement != null)
					preparedStatement.close();
				
				if(resultSet != null)
					resultSet.close();
				
				log.debug("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: after closing the prepared statements and resultSet.");
				
				if(msisdnExistanceMap != null && !msisdnExistanceMap.isEmpty()) {
					Iterator msisdnExistanceIterator = msisdnExistanceMap.keySet().iterator();
					
					while(msisdnExistanceIterator.hasNext()) {
						String msisdn = msisdnExistanceIterator.next().toString();
						if(ConstantIfc.MSIDN_EXISTANCE_TRUE.equalsIgnoreCase((String)msisdnExistanceMap.get(msisdn))) {
							log.debug("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: poid record already exist in POID table, so updating the record for msisdn ["+msisdn+"]");
							preparedStatementUpdate.setString(1, pOIDHash.get(msisdn).toString());
							preparedStatementUpdate.setString(2,msisdn);
							preparedStatementUpdate.executeUpdate();
						} else if(ConstantIfc.MSIDN_EXISTANCE_FALSE.equalsIgnoreCase((String)msisdnExistanceMap.get(msisdn))) {
							 log.debug("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: poid record not exist in POID table, so inserting new record for msisdn ["+msisdn+"]");
							 preparedStatementInsert.setString(1, msisdn);
							 preparedStatementInsert.setString(2,pOIDHash.get(msisdn).toString());
							 preparedStatementInsert.executeUpdate();
						}
					}
					
				}
				
				/*while( iterator.hasNext()){
					String msisdn = iterator.next().toString();
					preparedStatement.setString(1, msisdn);
					resultSet = preparedStatement.executeQuery();
					log.debug("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: after getting the result set.");
					if(resultSet.next()) {// record exist so update the POID
						 log.debug("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: poid record already exist in POID table, so updating the record for msisdn ["+msisdn+"]");
						 //sqlQuery = "UPDATE user_bill_poid SET POID = ? WHERE msisdn = ?";
						// preparedStatement  = connection.prepareStatement(sqlQuery.toString());
						 preparedStatementUpdate.setString(1, pOIDHash.get(msisdn).toString());
						 preparedStatementUpdate.setString(2,msisdn);
						 preparedStatementUpdate.executeUpdate();
						 log.debug("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: after updating the POID record for msidn ["+msisdn+"]");
					}else { //record not exist, so insert the poid 
						 log.debug("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: poid record not exist in POID table, so inserting new record for msisdn ["+msisdn+"]");
						// sqlQuery = "INSERT INTO user_bill_poid(msisdn, poid) VALUES(?,?)";
						// preparedStatement  = connection.prepareStatement(sqlQuery.toString());
						 preparedStatementInsert.setString(1, msisdn);
						 preparedStatementInsert.setString(2,pOIDHash.get(msisdn).toString());
						 preparedStatementInsert.executeUpdate();
						 log.debug("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: after inserting the POID record for msidn ["+msisdn+"]");
					}
					
//					try{
//					    preparedStatement.executeUpdate();
//					}catch(SQLException sqlEx){// if one update fail continue for the rest of the iteratore values
//					    log.fatal("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: SQLException while executeUpdate"+sqlEx);
//					    throw new SystemException(sqlEx);
//					} 
				}*/
			}catch(SQLException sqlEx){// if one update fail continue for the rest of the iteratore values
			    log.fatal("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: SQLException "+sqlEx);
			    throw new SystemException(sqlEx);
			}
			catch(Exception e){
				log.fatal("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: Exception "+e);
				throw new SystemException(e);
			}finally{
				try{
					if(preparedStatementInsert != null)
						preparedStatementInsert.close();
					if(preparedStatementUpdate != null)
						preparedStatementUpdate.close();
				}catch (Exception e) {
					log.fatal("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: exception while closing the resources");
				}
				JDBCUtility.closeJDBCResoucrs(connection,preparedStatement,resultSet);
			}
			
			log.info("MobilyCommonService --> ePortalDBDao --> updatePOIDList :: end");
		}
	}

	/**
	 * A function to check if the record in recovery table is existing (both msisdn and task)
	 * 
	 * @return boolean
	 * @throws 
	 */
	public boolean isDuplicateInSkyRecovery( String aEmail, String aTask ) {
	    boolean 		  isDuplicate 			= false;
	    Connection        tempConnection        = null; 
		PreparedStatement tempPreparedStatement = null;
		ResultSet         tempResultSet         = null;
		StringBuffer      tempQuery             = null;
		int				  counter               = 0;
				
		try 
		{
		    tempQuery = new StringBuffer("select EMAIL ");
		    tempQuery.append("from SKY_EMAIL_RECOVERY ");
		    tempQuery.append("where STATUS = ? ");
		    tempQuery.append("and EMAIL = ? ");
		    tempQuery.append("and TASK = ? ");
		    
			tempConnection        = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			tempPreparedStatement = tempConnection.prepareStatement(tempQuery.toString());
			tempPreparedStatement.setInt   ( ++counter, ConstantIfc.SKY_EMAIL_SERVICE_FAIL );
			tempPreparedStatement.setString( ++counter, aEmail );
			tempPreparedStatement.setString( ++counter, aTask );
			tempResultSet = tempPreparedStatement.executeQuery();				
			
			if( tempResultSet.next() )
			{
			    isDuplicate = true;
			}
			log.debug("ePortalDAO.isDuplicateInSkyRecovery, " + isDuplicate);
			System.err.println("ePortalDAO.isDuplicateInSkyRecovery, " + isDuplicate);
		}
		catch(Exception e){
			log.fatal(e.toString());
		}finally{
		    JDBCUtility.closeJDBCResoucrs(tempConnection , tempPreparedStatement , null);
		}
		return isDuplicate;
	}

	/**
	 * 
	 * @param aUser
	 * @return
	 */
	public int storeFailedSkyEmailCreation( SecondaryEmailVO aUser ) {
		Connection        tempConnection        = null;
		PreparedStatement tempPreparedStatement = null;
		int               tempErrorCode         = 0;
		int               tempCounter           = 0;
		try	{
			log.debug("ePortalDAO.storeFailedSkyEmailCreation, starting... ");
			System.err.println("ePortalDAO.storeFailedSkyEmailCreation, starting... ");
		    StringBuffer tempSQL = new StringBuffer("insert into");
			tempSQL.append(" SKY_EMAIL_RECOVERY(EMAIL, PASSWORD, GIVEN_NAME, FAMILY_NAME, STATUS, TASK, FAILURE_TIME) ");
			tempSQL.append("values(?, ?, ?, ?, ?, ?, (SELECT CURRENT_TIMESTAMP FROM dual))");
			
			/*Driver d = (Driver)Class.forName( "oracle.jdbc.driver.OracleDriver" ).newInstance();
		    tempConnection = DriverManager.getConnection( "jdbc:oracle:thin:@10.6.11.22:1521:eedb",
		    			     						 	  "eedbusr",
		    										 	  "eedbusr");*/
			tempConnection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			tempPreparedStatement  = tempConnection.prepareStatement( tempSQL.toString());
			tempPreparedStatement.setString( ++tempCounter, aUser.getEmail() );
			tempPreparedStatement.setString( ++tempCounter, aUser.getPassword() );
			tempPreparedStatement.setString( ++tempCounter, aUser.getGivenName() );
			tempPreparedStatement.setString( ++tempCounter, aUser.getFamilyName() );
			tempPreparedStatement.setInt   ( ++tempCounter, ConstantIfc.SKY_EMAIL_SERVICE_FAIL );
			tempPreparedStatement.setString( ++tempCounter, aUser.getService() );
	
			tempErrorCode = tempPreparedStatement.executeUpdate();
			log.debug("ePortalDAO.storeFailedSkyEmailCreation, errorCode is: " + tempErrorCode);
			
		}
		catch(Exception e){
			log.fatal(e.toString());
		}finally{
		    JDBCUtility.closeJDBCResoucrs(tempConnection , tempPreparedStatement , null);
		}
		return tempErrorCode;
	}

	/**
	 * A function to update poid list while registering user account
	 * @param msisdn, package type
	 * @throws 
	 @Modified by SUresh V on Dec 28 2009: To check weather customer is already having the record in user_bill_poid table before inserting. If exist then update the record else insert the record
	 */
	public void updatePOIDListforRegistration(String aMsisdn, String packType) throws Exception{
		log.debug("ePoratlDBDao > updatePOIDListforRegistration > start");
		log.debug("ePoratlDBDao > updatePOIDListforRegistration > MSISDN:"+aMsisdn+":Package:"+packType);
			String billAccountPOID = null;
			Connection connection1 = null;
			PreparedStatement preparedStatement = null;
			ResultSet rsltSet = null;
			ArrayList packageList= new ArrayList();
			connection1 = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			String SQLQuery = "";
			SQLQuery ="SELECT PACKAGE_ID from SR_UPDATE_POID_PACKAGES_TBL";
			preparedStatement  = connection1.prepareStatement(SQLQuery.toString());
			rsltSet = preparedStatement.executeQuery();
			while(rsltSet.next()){
				packageList.add(String.valueOf(rsltSet.getInt("PACKAGE_ID")));				
			}
			boolean packCheck=false;
			if(packageList!=null){
				for(int i=0;i<packageList.size();i++ ) {
					if(packType.equals(packageList.get(i))){
						log.debug("ePoratlDBDao > updatePOIDListforRegistration > package:"+packageList.get(i));
						packCheck=true;
						break;
					}
				}
			}
			log.debug("ePoratlDBDao > updatePOIDListforRegistration > packcheck : "+packCheck);
			if(packCheck){
				log.debug("ePoratlDBDao > updatePOIDListforRegistration > package is Raqi");
			    billAccountPOID = BillingInfoDAO.getBillingAccountPOIDForRaqi(aMsisdn); 
			    log.debug("ePoratlDBDao > updatePOIDListforRegistration > package is Raqi end");
			}else{
				log.debug("ePoratlDBDao > updatePOIDListforRegistration > not Raqi");
				billAccountPOID = BillingInfoDAO.getBillingAccountPOID(aMsisdn);
				log.debug("ePoratlDBDao > updatePOIDListforRegistration > not Raqi End");
			}
			
		if(billAccountPOID != null)
		{
			Connection connection = null;
			
			ResultSet resultSet = null;
			String sqlQuery="";
			String poidExists = "";
			try {
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
				poidExists = fetchPOIDIdforMsisdnfromDB(aMsisdn);
				/*String sqlQuery = "SELECT * FROM user_bill_poid WHERE msisdn = ? "; 
				preparedStatement  = connection.prepareStatement(sqlQuery.toString());
				preparedStatement.setString(1, aMsisdn);
				resultSet = preparedStatement.executeQuery();*/
				//if(resultSet != null && resultSet.next()) {// record exist so update the POID
				if(poidExists!=null&&!"".equals(poidExists)){
					 log.debug("MobilyCommonService --> ePortalDBDao --> updatePOIDListforRegistration :: poid record already exist in table, so updating the record for msisdn ["+aMsisdn+"]");
					 sqlQuery = "UPDATE user_bill_poid SET POID = ? WHERE msisdn = ?";
					 preparedStatement  = connection.prepareStatement(sqlQuery.toString());
					 preparedStatement.setString(1, billAccountPOID);
					 preparedStatement.setString(2,aMsisdn);
				}else { //record not exist, so insert the poid 
					 log.debug("MobilyCommonService --> ePortalDBDao --> updatePOIDListforRegistration :: poid record not exist in table, so inserting new record for msisdn ["+aMsisdn+"]");
					 sqlQuery = "INSERT INTO user_bill_poid(msisdn, poid) VALUES(?,?)";
					 preparedStatement  = connection.prepareStatement(sqlQuery.toString());
					 preparedStatement.setString(1, aMsisdn);
					 preparedStatement.setString(2,billAccountPOID);
				}
				preparedStatement.executeUpdate();
			}catch(SQLException e){
	            log.fatal("SQLException " + e.getMessage());
	            throw new SystemException(e);
	         }
	        catch(Exception e){
	            log.debug("Exception " + e.getMessage());
	           throw new SystemException(e);
	            
	        }finally{
			    JDBCUtility.closeJDBCResoucrs(connection , preparedStatement , resultSet);
			}
		}
	}
	
	
	/**
	 * A function to update poid list for call center empowerment
	 * @param msisdn, package type
	 * @throws 
	 @Modified by Geetha K on Mar 4th 2010: To check weather customer is already having the record in user_bill_poid table before inserting. If exist then update the record else insert the record
	 */
	public int updatePOIDforCCM(String aMsisdn, String packType) throws Exception{
			int status=0;
			String billAccountPOID = null;
			if(packType.equals(ConstantIfc.RAQI_PACKAGE_ID_FOR_REGISTRATION)){
			    billAccountPOID = BillingInfoDAO.getBillingAccountPOIDForRaqi(aMsisdn); 
			}else{
				billAccountPOID = BillingInfoDAO.getBillingAccountPOID(aMsisdn);
			}
		if(billAccountPOID != null)
		{
			Connection connection = null;
			PreparedStatement preparedStatement = null;
			ResultSet resultSet = null;
			String sqlQuery="";
			String poidExists = "";
			try {
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
				poidExists = fetchPOIDIdforMsisdnfromDB(aMsisdn);
				if(poidExists!=null&&!"".equals(poidExists)){
					if(!poidExists.equals(billAccountPOID)){
						log.debug("MobilyCommonService --> ePortalDBDao --> updatePOIDforCCM :: poid record already exist in table, so updating the record for msisdn ["+aMsisdn+"]");
						sqlQuery = "UPDATE user_bill_poid SET POID = ? WHERE msisdn = ?";
						preparedStatement  = connection.prepareStatement(sqlQuery.toString());
						preparedStatement.setString(1, billAccountPOID);
						preparedStatement.setString(2,aMsisdn);
						status = 2;
						preparedStatement.executeUpdate();
					}else{
						log.debug("MobilyCommonService --> ePortalDBDao --> updatePOIDforCCM :: poid record already exist in table, and is same so no update ["+aMsisdn+"]");
						status = 3;
					}
				}else { //record not exist, so insert the poid 
					 log.debug("MobilyCommonService --> ePortalDBDao --> updatePOIDforCCM :: poid record not exist in table, so inserting new record for msisdn ["+aMsisdn+"]");
					 sqlQuery = "INSERT INTO user_bill_poid(msisdn, poid) VALUES(?,?)";
					 preparedStatement  = connection.prepareStatement(sqlQuery.toString());
					 preparedStatement.setString(1, aMsisdn);
					 preparedStatement.setString(2,billAccountPOID);
					 status = 1;
					 preparedStatement.executeUpdate();
				}
				
			}catch(SQLException e){
	            log.fatal("SQLException " + e.getMessage());
	            throw new SystemException(e);
	         }
	        catch(Exception e){
	            log.debug("Exception " + e.getMessage());
	           throw new SystemException(e);
	            
	        }finally{
			    JDBCUtility.closeJDBCResoucrs(connection , preparedStatement , resultSet);
			}
		}
		log.debug(status + "  --.is the status in DAO");
		return status;
	}


	/**
	 * 
	 * @param idArrys
	 * @return
	 * @throws Exception
	 * @throws SQLException
	 */
	public static HashMap getBackEndMessages(int idArrys[]) throws Exception,SQLException  {
		ResultSet l_result = null;
		Connection connection = null;
		PreparedStatement statement=null;
		HashMap rows=new HashMap();
		int previousId=-1;
		int id=-1;
		try {
			String sqlQuery = "select * from  backend_messages  ";
	          if (idArrys!=null && idArrys.length>0){
	          	sqlQuery+=" where id in (";
	          	for (int i=0;i<idArrys.length;i++){
	          		if(i>0)
	          			sqlQuery+=",";
	          		sqlQuery+=""+idArrys[i];
	          	}
	          	sqlQuery+=" )";
	          }
	          	sqlQuery+=" order by 1,2";
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			statement  = connection.prepareStatement(sqlQuery.toString());
			l_result = statement.executeQuery();
			
			HashMap oneGroupMap=new HashMap();
			
			BackEndMessagesVO backEndMessagesVO=null; 
			String key="";
			while(l_result.next())
			{
				id=l_result.getInt(1);
				if (id!=previousId&&previousId!=-1){
				  rows.put(new Integer(previousId),oneGroupMap);
				  oneGroupMap=new HashMap(); 
				}
				key=l_result.getString(2);//key
				backEndMessagesVO=new BackEndMessagesVO();
	            backEndMessagesVO.setKey(key);
	            backEndMessagesVO.setMessageEn(l_result.getString(3));
	            backEndMessagesVO.setMessageAr(l_result.getString(4));
	            backEndMessagesVO.setDescription(l_result.getString(5));
	            oneGroupMap.put(key,backEndMessagesVO);
	            previousId=id;
			}
			rows.put(new Integer(previousId),oneGroupMap);
	
		}catch(SQLException e){
	        log.fatal("SQLException " + e.getMessage());
	        throw new SystemException(e);
	         
	     }
	    catch(Exception e){
	        log.debug("Exception " + e.getMessage());
	       throw new SystemException(e);
	        
	    }finally{
		    JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
		return rows;
	}
	
	/**
	 * 
	 * @param aPackageID
	 * @return
	 * @throws Exception
	 * @throws SQLException
	 */
	public BackageDataVO getPackageData(String aPackageID) throws Exception,SQLException {
		BackageDataVO l_PackageData = null;
		ResultSet l_result = null;
		Connection connection = null;
		PreparedStatement statement=null;
		try {
			String sqlQuery = "select packageid, packagenameen, packagenamear, nationalfvncount, internationalfvncount, familyandfriendscount from packagesfvnsettings WHERE packageid=?";
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1,aPackageID);
			l_result = statement.executeQuery();
			if(l_result.next())
			{
				l_PackageData = new BackageDataVO();
				
				l_PackageData.setPackageId(l_result.getString(1));
				l_PackageData.setPackageNameEn(l_result.getString(2));
				l_PackageData.setPackageNameAr(l_result.getString(3));
				l_PackageData.setNationalFVNCount(l_result.getInt(4));
				l_PackageData.setInternationalFVNCount(l_result.getInt(5));
				l_PackageData.setFamilyAndFriendsCount(l_result.getInt(6));
			}
		}catch(SQLException e){
	        log.fatal("SQLException " + e.getMessage());
	        throw new SystemException(e);
	         
	     }
	    catch(Exception e){
	        log.debug("Exception " + e.getMessage());
	       throw new SystemException(e);
	        
	    }finally{
		    JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
		return l_PackageData;
	}
	
	/**
	 * A function to get all arabic possible values of Customer Profile page
	 * 
	 * @return Hashtable
	 * 
	 * Adde By 'Nagendra'
	 */
	public Hashtable getArabicProfile() {
		
		Connection connection = null;
		PreparedStatement pStmt = null;
		ResultSet rslt = null;
		StringBuffer sqlQuery = null;
	
		Hashtable profileData = null;
		
		log.debug("ePortalDBDAO.getArabicProfile, starting...");
	
		try {
			sqlQuery = new StringBuffer();
					
			sqlQuery.append("SELECT ITEM_DESC_EN, ITEM_DESC_AR FROM SR_LOOKUP_TBL");
				sqlQuery.append(" WHERE ID = ? OR ID = ?");
	
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			pStmt = connection.prepareStatement(sqlQuery.toString());
	
			pStmt.setInt(1, Integer.parseInt(ConstantIfc.LOOKUP_COUNTRIES_ID));
			pStmt.setInt(2, ConstantIfc.LOOKUP_CUSTOMER_PROFILE);
			rslt = pStmt.executeQuery();
	
			profileData = new Hashtable();
			
			while (rslt.next()) {
				profileData.put(rslt.getString("ITEM_DESC_EN"), rslt.getString("ITEM_DESC_AR"));
			}
	
			log.debug("ePortalDBDAO.getArabicProfile, Hashtable size is "+ profileData.size());
			
		} catch (SQLException e) {
			log.fatal("ePortalDBDao > getArabicProfile > SQLException " + e.getMessage());
			throw new SystemException(e);
	
		} catch (Exception e) {
			log.debug("ePortalDBDao > getArabicProfile > Exception " + e.getMessage());
			throw new SystemException(e);
	
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, pStmt, rslt);
		}
		
		return profileData;
	}
	
	/**
	 * 
	 * @param objUserAccountVO
	 * @return
	 * @throws Exception
	 */
	public boolean updateActivatedUserAccount(UserAccountVO objUserAccountVO){
		
		boolean status = false;
		Connection connection = null;
		PreparedStatement pStemt = null;
		StringBuffer sqlQuery = null;
		
		try {
			sqlQuery = new StringBuffer();
				sqlQuery.append("UPDATE USER_ACCOUNT SET FULLNAME = ?, GENDER = ?, AGE = ?, NATIONALITY = ?,");
					sqlQuery.append("EMAIL_ADDRESS = ?, PREFERRED_LANGUAGE = ? WHERE MSISDN = ?");
			
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			pStemt = connection.prepareStatement(sqlQuery.toString());
			
				pStemt.setString(1, objUserAccountVO.getFullName());
				pStemt.setString(2, objUserAccountVO.getGender());
				pStemt.setString(3, objUserAccountVO.getAge());
				pStemt.setString(4, objUserAccountVO.getNationality());
				pStemt.setString(5, objUserAccountVO.getEmailAddress());
				pStemt.setString(6, objUserAccountVO.getPreferredLanguage());
				pStemt.setString(7, objUserAccountVO.getMsisdn());
	
				pStemt.executeUpdate();
				
				status = true;
		} catch (SQLException e) {
			log.fatal("ePortalDBDao > updateActivatedUserAccount > SQLException " + e.getMessage());
			throw new SystemException(e);
	
		} catch (Exception e) {
			log.debug("ePortalDBDao > updateActivatedUserAccount > Exception " + e.getMessage());
			throw new SystemException(e);
	
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, pStemt, null);
		}
		return status;
	}
	
	/**
	 * 
	 * @param msisdn
	 * @return
	 * @throws Exception
	 */
	public UserAccountVO getUserAccountWebProfile(String msisdn) {
		Connection connection = null;
		PreparedStatement pStemt = null;
		ResultSet rslt = null;
		UserAccountVO objUserAccountVO = null;
		StringBuffer sqlQuery = null;
		
		try {
			sqlQuery = new StringBuffer();
				sqlQuery.append("SELECT ID ,USERNAME ,  FULLNAME, GENDER, AGE, NATIONALITY, EMAIL_ADDRESS,USER_ACTIVATED , ");
					sqlQuery.append(" PREFERRED_LANGUAGE ,PACKAGE_ID ,CUSTOMERTYPE ,LINE_TYPE ,SEC_QUES_ID ,SEC_QUES_ANSWER ,CUSTOMER_ID FROM USER_ACCOUNT WHERE MSISDN = ?");
			
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
						
			pStemt = connection.prepareStatement(sqlQuery.toString());
			pStemt.setString(1, msisdn);
			rslt = pStemt.executeQuery();
			while (rslt.next()) {
				objUserAccountVO = new UserAccountVO();
				objUserAccountVO.setId(rslt.getInt("ID"));
				objUserAccountVO.setUserName(rslt.getString("USERNAME"));
				objUserAccountVO.setFullName(rslt.getString("FULLNAME"));
				objUserAccountVO.setGender(rslt.getString("GENDER"));
				objUserAccountVO.setAge(rslt.getString("AGE"));
				objUserAccountVO.setNationality(rslt.getString("NATIONALITY"));
				objUserAccountVO.setEmailAddress(rslt.getString("EMAIL_ADDRESS"));
				objUserAccountVO.setUserActivated(rslt.getString("USER_ACTIVATED"));
				objUserAccountVO.setPreferredLanguage(rslt.getString("PREFERRED_LANGUAGE"));
				
				objUserAccountVO.setPackageId(rslt.getString("PACKAGE_ID"));
				objUserAccountVO.setCustomerType((byte)rslt.getInt("CUSTOMERTYPE"));
				objUserAccountVO.setLineType(rslt.getInt("LINE_TYPE"));
				objUserAccountVO.setSecurityQuesId(rslt.getInt("SEC_QUES_ID"));
				objUserAccountVO.setSecurityQuesAns(rslt.getString("SEC_QUES_ANSWER"));
				objUserAccountVO.setIqama(rslt.getString("CUSTOMER_ID"));
				
				
			}
		} catch (SQLException e) {
			log.fatal("ePortalDBDao > getUserAccountWebProfile > SQLException " + e.getMessage());
			throw new SystemException(e);
	
		} catch (Exception e) {
			log.debug("ePortalDBDao > getUserAccountWebProfile > Exception " + e.getMessage());
			throw new SystemException(e);
	
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, pStemt, rslt);
		}
		return objUserAccountVO;
	}
	
	/**
	 * 
	 * @param msisdn
	 * @return
	 * @throws Exception
	 */
	public boolean isWebProfileUpdated(String msisdn){
		Connection connection = null;
		PreparedStatement pStmt = null;
		ResultSet rslt = null;
		boolean isWebProfileUpdated = false;
		StringBuffer sqlQuery = null;
		
		try {
			sqlQuery = new StringBuffer();
				sqlQuery.append("SELECT FULLNAME, GENDER, AGE, NATIONALITY, EMAIL_ADDRESS,");
					sqlQuery.append("PREFERRED_LANGUAGE FROM USER_ACCOUNT WHERE MSISDN = ?");
			
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			pStmt = connection.prepareStatement(sqlQuery.toString());
			pStmt.setString(1, msisdn);
			rslt = pStmt.executeQuery();
			while (rslt.next()) {
				if (rslt.getString("FULLNAME") != null
						&& rslt.getString("GENDER") != null
						&& rslt.getString("AGE") != null
						&& rslt.getString("NATIONALITY") != null
						&& rslt.getString("EMAIL_ADDRESS") != null
						&& rslt.getString("PREFERRED_LANGUAGE") != null)
					isWebProfileUpdated = true;
			}
		} catch (SQLException e) {
			log.fatal("ePortalDBDao > isWebProfileUpdated > SQLException " + e.getMessage());
			throw new SystemException(e);
	
		} catch (Exception e) {
			log.debug("ePortalDBDao > isWebProfileUpdated > Exception " + e.getMessage());
			throw new SystemException(e);
	
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, pStmt, rslt);
		}
		return isWebProfileUpdated;
	}
	
	/**
	 * 
	 * @return
	 */
	public HashMap getChargeServiceAmount() {
		
	   log.info("ePortalDBDao > getChargeServiceAmount > called");
	   
	   HashMap serviceChargeMap = new HashMap();
	   
	   Connection connection = null;
	   ResultSet  rslt = null;
	   PreparedStatement pStmt = null;
	   try{

		   	StringBuffer sqlQuery = new StringBuffer();
		   	sqlQuery.append("SELECT ID , SUB_ID , AMOUNT FROM SERVICE_CHARGE_AMOUNT_TBL");
		   		sqlQuery.append(" WHERE SUB_ID != ?");
		   	
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	
			pStmt = connection.prepareStatement(sqlQuery.toString());
			pStmt.setString(1, "0");
		   
		   	rslt  = pStmt.executeQuery();
		   
		   	StringBuffer key = null;
		   	
		   	while(rslt.next()){
		   		key = new StringBuffer();
		   		
		   		key.append(rslt.getString("ID"));
		   		key.append(rslt.getString("SUB_ID"));
		   	  
		   		serviceChargeMap.put(key.toString() , rslt.getString("AMOUNT"));
		   	}
	   	
	   }catch (SQLException e) {
			log.fatal("ePortalDBDao > getChargeServiceAmount > SQLException " + e.getMessage());
			throw new SystemException(e);
	
		} catch (Exception e) {
			log.debug("ePortalDBDao > getChargeServiceAmount > Exception " + e.getMessage());
			throw new SystemException(e);
	
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, pStmt, rslt);
		}
			
	   return serviceChargeMap;
	}
	
	public String fetchPOIDIdforMsisdn(String aMsisdn, String packType){
		log.debug("ePortalDBDao >fetchPOIDIdforMsisdn > aMsisdn  -->" + aMsisdn + "pack type" + packType);
		String billAccountPOID = null;
		try{
		if(packType.equals(ConstantIfc.RAQI_PACKAGE_ID_FOR_REGISTRATION)){
			billAccountPOID = BillingInfoDAO.getBillingAccountPOIDForRaqi(aMsisdn); 
		}else{
			billAccountPOID = BillingInfoDAO.getBillingAccountPOID(aMsisdn);
		}
		log.debug("ePortalDBDao >fetchPOIDIdforMsisdn > billAccountPOID  -->" + billAccountPOID);
		}catch (SQLException e) {
			log.fatal("ePortalDBDao > fetchPOIDIdforMsisdn > SQLException " + e.getMessage());
			throw new SystemException(e);
	
		} catch (Exception e) {
			log.debug("ePortalDBDao > fetchPOIDIdforMsisdn > Exception " + e.getMessage());
			throw new SystemException(e);
	
		} 
		return billAccountPOID;
	}
	public String fetchPOIDIdforMsisdnfromDB(String aMsisdn){
		log.debug("ePortalDBDao >fetchPOIDIdforMsisdnfromDB > aMsisdn  -->" + aMsisdn );
		String billAccountPOIDforDB = null;
		Connection        tempConnection = null; 
		PreparedStatement statement = null;
		ResultSet         tempResultSet= null;
		String      tempQuery = null;
		tempQuery = "select poid from user_bill_poid where msisdn=?";
		try{
		tempConnection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		statement  = tempConnection.prepareStatement(tempQuery.toString());
		statement.setString(1,aMsisdn);
		tempResultSet = statement.executeQuery();
		if(tempResultSet.next())
		{
			billAccountPOIDforDB = tempResultSet.getString(1);
		}
		log.debug("ePortalDBDao >fetchPOIDIdforMsisdnfromDB > billAccountPOIDforDB  -->" + billAccountPOIDforDB);
		}catch (SQLException e) {
			log.fatal("ePortalDBDao > fetchPOIDIdforMsisdnfromDB > SQLException " + e.getMessage());
			throw new SystemException(e);
	
		} catch (Exception e) {
			log.debug("ePortalDBDao > fetchPOIDIdforMsisdnfromDB > Exception " + e.getMessage());
			throw new SystemException(e);
	
		} finally {
			JDBCUtility.closeJDBCResoucrs(tempConnection, statement, tempResultSet);
		}
		return billAccountPOIDforDB;
	}
	
	public void updatePOIDForMsisdninDB(String msisdn, String poid){
		log.debug("ePortalDBDao >updatePOIDForMsisdninDB > aMsisdn  -->" + msisdn + "poid" + poid);
		Connection connection = null;
		PreparedStatement pStemt = null;
		StringBuffer sqlQuery = null;
				
				try {
					sqlQuery = new StringBuffer();
					sqlQuery.append("UPDATE user_bill_poid SET poid= ? WHERE MSISDN = ?");
					
					connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
					
					pStemt = connection.prepareStatement(sqlQuery.toString());
					
						pStemt.setString(1, poid);
						pStemt.setString(2, msisdn);
			
						pStemt.executeUpdate();
				}catch (SQLException e) {
					log.fatal("ePortalDBDao > fetchPOIDIdforMsisdnfromDB > SQLException " + e.getMessage());
					throw new SystemException(e);
			
				} catch (Exception e) {
					log.debug("ePortalDBDao > fetchPOIDIdforMsisdnfromDB > Exception " + e.getMessage());
					throw new SystemException(e);
			
				} finally {
					JDBCUtility.closeJDBCResoucrs(connection, pStemt,null);
				}
		
	}
	
	public String getTempActivationData(String msisdn) {
		
		String activationCode=null;
		Connection connection = null;
		PreparedStatement statement=null;
		ResultSet resultSet=null;
		try {
			String sqlQuery = "select activation_code from  TEMP_ACTIVATION where msisdn=? ";
	
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1,msisdn);
			resultSet = statement.executeQuery();				
								
			if (resultSet.next()){
				activationCode = resultSet.getString(1);
			}
		}catch (SQLException e) {
			log.fatal("ePortalDBDao > getTempActivationData > SQLException " + e.getMessage());
			throw new SystemException(e);
	
		} catch (Exception e) {
			log.debug("ePortalDBDao > getTempActivationData > Exception " + e.getMessage());
			throw new SystemException(e);
	
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, statement,null);
		}
		return activationCode;
	}
	
	public void createTempActivationData(String aMsisdn, String aActivationCode) {
		
		Connection connection = null;
		PreparedStatement statement=null;
		try {
			String sqlQuery = "INSERT INTO TEMP_ACTIVATION VALUES(?,?)";
	
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1,aMsisdn);
			statement.setString(2,aActivationCode);
			statement.execute();
		}
		catch (SQLException e) {
			log.fatal("ePortalDBDao > createTempActivationData > SQLException " + e.getMessage());
			throw new SystemException(e);
	
		} catch (Exception e) {
			log.debug("ePortalDBDao > createTempActivationData > Exception " + e.getMessage());
			throw new SystemException(e);
	
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, statement,null);
		}
	}
	
	public void deleteTempActivationData(String aMsisdn) {
		
		Connection connection = null;
		PreparedStatement statement=null;
		try {
			String sqlQuery = "DELETE FROM TEMP_ACTIVATION WHERE msisdn=?";
	
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1,aMsisdn);
			statement.execute();
		}
		catch (SQLException e) {
			log.fatal("ePortalDBDao > deleteTempActivationData > SQLException " + e.getMessage());
			throw new SystemException(e);
	
		} catch (Exception e) {
			log.debug("ePortalDBDao > deleteTempActivationData > Exception " + e.getMessage());
			throw new SystemException(e);
	
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, statement,null);
		}
	}
	
	/**
	 * @return
	 * @throws ContentProviderException
	 */
	public  Hashtable loadLoyaltyReasonCodeFromDB(){
		
		log.info("LotaltyDAO > loadLoyaltyReasonCodeFromDB > Called ");
		Hashtable LoayltyReasonCodeHashTable = new Hashtable();
	   
	   Connection connection   = null;
	   ResultSet resultset     = null;
	   PreparedStatement preparedStatement  = null;
       	  
	   try{
     	  String sqlStatament = "SELECT REASON_CODE , DESC_EN , DESC_AR from SR_LOYALTY_REASONCODE_TBL";
     	  connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
     	  preparedStatement       = connection.prepareStatement(sqlStatament);
	      resultset  = preparedStatement.executeQuery();
	      LoyaltyReasonCodeVO objectVO = null;
	      while(resultset.next()){
		      	objectVO = new LoyaltyReasonCodeVO();
		      	objectVO.setReasonCode(resultset.getInt("REASON_CODE"));
		      	objectVO.setDesc_en(resultset.getString("DESC_EN"));
		      	objectVO.setDesc_ar(resultset.getString("DESC_AR"));
		      	LoayltyReasonCodeHashTable.put(""+objectVO.getReasonCode() , objectVO);
	      }
	      log.debug("LotaltyDAO > loadLoyaltyReasonCodeFromDB > > Done ["+LoayltyReasonCodeHashTable.size()+"]");
	   }catch(SQLException e){
	   		log.fatal("LotaltyDAO > loadLoyaltyReasonCodeFromDB >  > SQLException > "+e.getMessage());
	   		throw new SystemException(e);
	   	
	   }catch(Exception e){
	   		log.fatal("LotaltyDAO > loadLoyaltyReasonCodeFromDB >  Exception > "+e.getMessage());
	   		throw new SystemException(e);  
	   }
	   finally{
		   JDBCUtility.closeJDBCResoucrs(connection , preparedStatement , null);
	  }
	  
	   return LoayltyReasonCodeHashTable;
	}
	public String getServiceAccount(String msisdn) throws SystemException{
		log.debug("ePortalDBDao > getServiceAccount > called");
		String serviceAccount=null;
		Connection connection = null;
		PreparedStatement statement=null;
		ResultSet resultSet=null;
		try {
		String sqlQuery = "select SERVICE_ACT_NUM from user_account where msisdn=? ";

		connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		statement = connection.prepareStatement(sqlQuery.toString());
		statement.setString(1,msisdn);
		resultSet = statement.executeQuery();

		if (resultSet.next()){
		serviceAccount = resultSet.getString("SERVICE_ACT_NUM");
		}
		log.debug("ePortalDBDao > getServiceAccount > from user account : ServiceAccount:"+serviceAccount);
		if(serviceAccount==null){
		if(msisdn.length()>=12){
		if(ConstantIfc.UNIVERSAL_NUMBER_FORMAT.equals(msisdn.substring(0,3))){
		msisdn = ConstantIfc.NUMBER_FORMAT+msisdn.substring(1);
		}
		}
		sqlQuery = "select SERVICE_ACT_NUM from user_managed_no where msisdn=? ";
		statement = connection.prepareStatement(sqlQuery.toString());
		statement.setString(1,msisdn);
		resultSet = statement.executeQuery();
		if (resultSet.next()){
		serviceAccount = resultSet.getString("SERVICE_ACT_NUM");
		}
		log.debug("ePortalDBDao > getServiceAccount > from manage no: ServiceAccount:"+serviceAccount);
		}
		}catch (SQLException e) {
		log.fatal("ePortalDBDao > getServiceAccount > SQLException " + e.getMessage());
		throw new SystemException(e);

		} catch (Exception e) {
		log.debug("ePortalDBDao > getServiceAccount > Exception " + e.getMessage());
		throw new SystemException(e);

		} finally {
		JDBCUtility.closeJDBCResoucrs(connection, statement,null);
		}
		log.debug("ePortalDBDao > getServiceAccount > end");
		return serviceAccount;
		}   
	

	public static void auditMQFailedRequest(String requestQueueName , String replyQueueName , String requestMsg , String replyMsg, String errorCode , String errorMsg){
		Connection connection = null;
		PreparedStatement statement=null;
		try {
			String sqlQuery = "insert into SR_MQ_AUDIT_TBL values (?,?,?,?,?,sysdate,?)";
	
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			//connection = getLocalConnection();
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1,requestQueueName);
			statement.setString(2,replyQueueName);
			if((!FormatterUtility.isEmpty(requestMsg)) && requestMsg.length() > 3990 ) {
				requestMsg = requestMsg.substring(0, 3990);
			}
			statement.setString(3,requestMsg);
			statement.setString(4,errorCode);
			statement.setString(5,errorMsg);
			if((!FormatterUtility.isEmpty(replyMsg)) && replyMsg.length() > 3990 ) {
				replyMsg = replyMsg.substring(0, 3900);
			}
			statement.setString(6,replyMsg);
			
			statement.execute();
		}
		catch (SQLException e) {
			log.fatal("ePortalDBDao > auditMQFailedRequest > SQLException " + e.getMessage());
	
		} catch (Exception e) {
			log.debug("ePortalDBDao > auditMQFailedRequest > Exception " + e.getMessage());
	
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, statement,null);
		}
	}
	
	public  int getGloybasGroupId(){
		 log.info("ePortalDBDao > getGloybasGroupId > called");
		   int seqId = 1;
		   
		   Connection connection = null;
		   ResultSet  rslt = null;
		   PreparedStatement pStmt = null;
		   try{

			   	String sqlQuery ="select GLOYBAS_SEQ.nextval groupId from dual";
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		
				pStmt = connection.prepareStatement(sqlQuery.toString());
			   	rslt  = pStmt.executeQuery();
			   	if(rslt.next()){
			   		seqId = rslt.getInt("groupId");
			   	}
		   	
		   }catch (SQLException e) {
				log.fatal("ePortalDBDao > getGloybasGroupId > SQLException " + e.getMessage());
			} catch (Exception e) {
				log.debug("ePortalDBDao > getGloybasGroupId > Exception " + e.getMessage());
		
			} finally {
				JDBCUtility.closeJDBCResoucrs(connection, pStmt, rslt);
			}
		   return seqId;
	}
	
	
	/**
	 * This method is user to insert a username upon creation / deletion from eportal system
	 * so the DR batch can use these information to syncronzie the users between production and DR
	 * @param  username
	 * @param  action (1= create / 2 = delete)
	 * */
	public void UpdateUserInDR(String username , int action){
		Connection connection = null;
		PreparedStatement statement=null;
		
		try {
			
			log.debug("ePortalDBDao > UpdateUserInDR > USerName >"+username);
			
			String sqlQuery = "INSERT INTO SR_DR_UPDATE_TBL VALUES(?,?,sysdate)";
	
			//connection = DataBaseHandler.getConnection();
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1,username);
			statement.setInt(2,action);
			statement.execute();
		}
		catch(SQLException e){
            log.fatal("ePortalDBDao > UpdateUserInDR > SQLException " + e.getMessage());
         }
        catch(Exception e){
            log.fatal("ePortalDBDao > UpdateUserInDR > Exception " + e.getMessage());
        }finally{
		    JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
	}
	
	/**
	 * this is will be a common activation module where it will insert the activation code based on the project id
	 * @param String accountNumber
	 * @param String activation Code
	 * @param int project id [SR_PROJECT_TBL]
	 * @return void
	 * */
	public void setActivationCode(String accountNumber , String activationCode , int projectId) throws SystemException{
		Connection connection = null;
		PreparedStatement statement=null;
		
		try {
			
			log.debug("ePortalDBDao > setActivationCode > accountNumber >"+accountNumber);
			
			String sqlQuery = "INSERT INTO SR_ACTIVATION_TBL (MSISDN , ACTIVATION_CODE , PROJECT_ID , REQ_DATE) VALUES(?,?,?,sysdate)";
	
			//connection = DataBaseHandler.getConnection();
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1,accountNumber);
			statement.setString(2,activationCode);
			statement.setInt(3,projectId);
			statement.execute();
		}
		catch(SQLException e){
            log.fatal("ePortalDBDao > setActivationCode > SQLException " + e.getMessage());
            throw new SystemException(e);
         }
        catch(Exception e){
            log.fatal("ePortalDBDao > setActivationCode > Exception " + e.getMessage());
            throw new SystemException(e);
        }finally{
		    JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
	}
	
	/**
	 * this is will be a common activation module where it will reponsible for retrivig the activation code based on MSISDN / Project ID
	 * @param String accountNumber
	 * @param int project id [SR_PROJECT_TBL]
	 * @return String activation code
	 * */
	public String getActivationCode(String accountNumber ,  int projectId) throws SystemException{
		String activationCode = "";
		   Connection connection = null;
		   ResultSet  rslt = null;
		   PreparedStatement statement = null;
		   try{

			   	String sqlQuery ="select ACTIVATION_CODE  from SR_ACTIVATION_TBL where MSISDN =? and PROJECT_ID =?";
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		
				statement = connection.prepareStatement(sqlQuery.toString());
				statement.setString(1,accountNumber);
				statement.setInt(2,projectId);
			   	rslt  = statement.executeQuery();
			   	if(rslt.next()){
			   		activationCode = rslt.getString("ACTIVATION_CODE");
			   	}
		   }catch (SQLException e) {
				log.fatal("ePortalDBDao > getActivationCode > SQLException " + e.getMessage());
				 throw new SystemException(e);
			} catch (Exception e) {
				log.debug("ePortalDBDao > getActivationCode > Exception " + e.getMessage());
				 throw new SystemException(e);
		
			} finally {
				JDBCUtility.closeJDBCResoucrs(connection, statement, rslt);
			}
		return activationCode;
	}
	
	/**
	 * @author m.abdelmaged
	 * This method will delete the activation code after activate the desire number based on MSISDN / Project ID
	 * @param String accountNumber
	 * @param int project id [SR_PROJECT_TBL]
	 * @return void
	 * */
	public void deleteActivationCode(String accountNumber ,  int projectId) throws SystemException{
		
		Connection connection = null;
		PreparedStatement statement=null;
		try {
		 	String sqlQuery ="DELETE FROM  SR_ACTIVATION_TBL where MSISDN =? and PROJECT_ID =?";
	
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1,accountNumber);
			statement.setInt(2,projectId);
			statement.execute();
		}
		catch (SQLException e) {
			log.fatal("ePortalDBDao > deleteActivationCode > SQLException " + e.getMessage());
			throw new SystemException(e);
	
		} catch (Exception e) {
			log.debug("ePortalDBDao > deleteActivationCode > Exception " + e.getMessage());
			throw new SystemException(e);
	
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, statement,null);
		}
		
	}
	
	public boolean  isWhiteListUsers(String msisdn) throws MobilyCommonException{
	 	   Connection connection   = null;
		   PreparedStatement pstm  = null;
	       ResultSet resultSet = null;
	       boolean status = false; 
		   try{
		       String sqlStatament = "";
	     	  sqlStatament= "select MSISDN from SR_WHITELIST_USER_TBL  where msisdn = ?";
		      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		      pstm       = connection.prepareStatement(sqlStatament);
		      pstm.setString(1, msisdn);
		      resultSet = pstm.executeQuery();
		      if (resultSet.next()){
		    	  status = true;
			}
		   }catch(SQLException e){
			   log.fatal("ePortalDBDao > isWhiteListUsers  > SQLException > "+e);
			   throw new MobilyCommonException(e);
		   }catch(Exception e){
		   	  log.fatal("ePortalDBDao > isWhiteListUsers  > Exception > "+e);
			   throw new MobilyCommonException(e);
		   }
		   finally{
		   	JDBCUtility.closeJDBCResoucrs(connection , pstm , resultSet);
		   }

		   return status;
	   }
	
	public boolean isUserRelatedNoExist(UserRelatedNoVO relatedVO) throws SystemException{
		Connection connection   = null;
		   PreparedStatement pstm  = null;
	       ResultSet resultSet = null;
	       boolean status = false; 
		   try{
		       String sqlStatament = "";
	     	  sqlStatament= "select * from user_managed_no  where id = ? and msisdn =? ";
		      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		      pstm       = connection.prepareStatement(sqlStatament);
		      pstm.setInt(1, relatedVO.getId());
		      pstm.setString(2, relatedVO.getDestinationNumber());
		      resultSet = pstm.executeQuery();
		      if (resultSet.next()){
		    	  status = true;
			}
		   }catch(SQLException e){
			   log.fatal("ePortalDBDao > isWhiteListUsers  > SQLException > "+e);
			   throw new SystemException(e);
		   }catch(Exception e){
		   	  log.fatal("ePortalDBDao > isWhiteListUsers  > Exception > "+e);
			   throw new SystemException(e);
		   }
		   finally{
		   	JDBCUtility.closeJDBCResoucrs(connection , pstm , resultSet);
		   }

		   return status;
		
	}
	
	public Date getLastLoginTime(String msisdn) throws SystemException {
		
		log.debug("ePortalDBDao > getLastLoginTime  > msisdn::"+msisdn);
		
		ResultSet resultSet = null;
		Date lastLoginDate = null;
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			String sqlQuery = "SELECT LASTLOGINTIME FROM USER_ACCOUNT WHERE MSISDN=?";

			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			statement  = connection.prepareStatement(sqlQuery);
			statement.setString(1,msisdn);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				if(resultSet.getTimestamp("LASTLOGINTIME") != null)
					lastLoginDate = new java.util.Date(resultSet.getTimestamp("LASTLOGINTIME").getTime());
			}
		} catch(SQLException e) {
			log.fatal("ePortalDBDao > getLastLoginTime  > SQLException > "+e);
			throw new SystemException(e);
		} catch(Exception e) {
		   	log.fatal("ePortalDBDao > getLastLoginTime  > Exception > "+e);
			throw new SystemException(e);
		} finally {
		   	JDBCUtility.closeJDBCResoucrs(connection , statement , resultSet);
		}
		return lastLoginDate;
	}
	
	public void updateLastLoginTime(String msisdn) throws SystemException {
		log.debug("ePortalDBDao > updateLastLoginTime  > msisdn::"+msisdn);
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			String sqlQuery = "UPDATE USER_ACCOUNT SET LASTLOGINTIME = sysdate WHERE MSISDN = ?";

			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			statement  = connection.prepareStatement(sqlQuery);
			statement.setString(1,msisdn);
			statement.executeUpdate();
			
		} catch(SQLException e) {
			log.fatal("ePortalDBDao > updateLastLoginTime  > SQLException > "+e);
			throw new SystemException(e);
		} catch(Exception e) {
		   	log.fatal("ePortalDBDao > updateLastLoginTime  > Exception > "+e);
			throw new SystemException(e);
		} finally {
		   	JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
	}	
	
    /**
     * This function is updated to get the expire date of grace. Affects both Sign in and My account filter
     * @param aMsisdn
     * @return
     * @throws SQLException
     * @throws Exception
     */

	
	public SkyUserAccountVO getSkyType( String aMsisdn ) throws SystemException {
		log.debug("ePortalDBDao > getSkyType  > msisdn::"+aMsisdn);
		
	    SkyUserAccountVO skyUserAccountVO = null;
	   	    
		Connection        connection = null;
		PreparedStatement statement  = null;
		ResultSet         resultSet  = null;
		StringBuffer 	  sqlQuery   = null;
		try {
		    sqlQuery = new StringBuffer("select sky_email_status, (sky_action_time+ 30) SCHEDULED_TIME, ");
		    sqlQuery.append("case when sky_type is null ");
		    sqlQuery.append("then -1 ");
		    sqlQuery.append("else sky_type ");
		    sqlQuery.append("end sky_type ");
		    sqlQuery.append("from sky_user_account where msisdn = ? ");

		    connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		    
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1,aMsisdn);
			resultSet = statement.executeQuery();				
			
			if (resultSet.next()) {
				skyUserAccountVO = new SkyUserAccountVO();
				
				skyUserAccountVO.setSkyType         ( resultSet.getInt ( "sky_type" ));
				skyUserAccountVO.setSkyScheduledDate( resultSet.getDate( "SCHEDULED_TIME" ));
				skyUserAccountVO.setSkyEmailStatus  ( resultSet.getInt ( "sky_email_status" ));
				
				log.debug("ePortalDAO.updateSkyType, sKyType: " + skyUserAccountVO.getSkyType());
				log.debug("ePortalDAO.updateSkyType, scheduledTime: " + skyUserAccountVO.getSkyScheduledDate());
				log.debug("ePortalDAO.updateSkyType, emailStatus: " + skyUserAccountVO.getSkyEmailStatus());
			}
		} catch(SQLException e) {
			log.fatal("ePortalDBDao > getSkyType  > SQLException > "+e);
			throw new SystemException(e);
		} catch(Exception e) {
		   	log.fatal("ePortalDBDao > getSkyType  > Exception > "+e);
			throw new SystemException(e);
		} finally {
		   	JDBCUtility.closeJDBCResoucrs(connection , statement , resultSet);
		}
		return skyUserAccountVO;
	}
	
	public Date getLastLoginTimeCorp(String aBillingNo, String aUsername) throws SystemException {
		log.debug("ePortalDBDao > getLastLoginTimeCorp  > billing account number::"+aBillingNo+" and user name::"+aUsername);
		
		ResultSet resultSet = null;
		Date l_LastLoginDate = null;
		Connection connection = null;
		PreparedStatement statement=null;
		try {
			String sqlQuery = "SELECT last_login_time FROM corp_auth_per_acnt where billing_no = ? and username = ?";

			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			statement  = connection.prepareStatement(sqlQuery);
			statement.setString(1,aBillingNo);
			statement.setString(2,aUsername);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				if(resultSet.getTimestamp("last_login_time")!= null)
					l_LastLoginDate = new java.util.Date(resultSet.getTimestamp("last_login_time").getTime());
			}
		} catch(SQLException e) {
			log.fatal("ePortalDBDao > getLastLoginTimeCorp  > SQLException > "+e);
			throw new SystemException(e);
		} catch(Exception e) {
		   	log.fatal("ePortalDBDao > getLastLoginTimeCorp  > Exception > "+e);
			throw new SystemException(e);
		} finally {
		   	JDBCUtility.closeJDBCResoucrs(connection , statement , resultSet);
		}
		return l_LastLoginDate;
	}

	public void updateLastLoginTimeCorp(String aUsername) throws SystemException {
		log.debug("ePortalDBDao > updateLastLoginTimeCorp  > username::"+aUsername);
		Connection connection = null;
		PreparedStatement statement=null;
		try {
			String sqlQuery = "update corp_auth_per_acnt set last_login_time = sysdate where username = ?";
			
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			statement  = connection.prepareStatement(sqlQuery);
			statement.setString(1,aUsername);
			statement.executeUpdate();
		} catch(SQLException e) {
			log.fatal("ePortalDBDao > updateLastLoginTimeCorp  > SQLException > "+e);
			throw new SystemException(e);
		} catch(Exception e) {
		   	log.fatal("ePortalDBDao > updateLastLoginTimeCorp  > Exception > "+e);
			throw new SystemException(e);
		} finally {
		   	JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
	}
	
	
	
	/* (non-Javadoc)
	 * This method is responsible for deleting all the existing record under SR_CUSTOMER_SESSIONVO_TBL for selected customer
	 * where during login process we have to remove the existing data for cleanup purpose
	 * @see sa.com.mobily.eportal.common.service.dao.ifc.ePortalDBDaoIfc#clearSessionVORow(java.lang.String)
	 */
	public void clearSessionVORow(String username){
		
		log.debug("ePortalDBDao > clearSessionVORow  > username::"+username);
		Connection connection = null;
		PreparedStatement statement=null;
		try {
			String sqlQuery = "DELETE FROM SR_CUSTOMER_SESSIONVO_TBL where  username = ?";
			
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery);
			statement.setString(1,username);
			statement.executeUpdate();
		} catch(SQLException e) {
			log.fatal("ePortalDBDao > clearSessionVORow  > SQLException > "+e);
		} catch(Exception e) {
		   	log.fatal("ePortalDBDao > clearSessionVORow  > Exception > "+e);
		} finally {
		   	JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
	}
	/* (non-Javadoc)
	 * this method is reposible for delete the existing related account information for the select customer from SR_CUSTOMER_SESSIONVO_TBL
	 * where when the customer change his related number we have to delete the previous record for the selected one
	 * @param msisdn , which is the original MSISDN for logged in customer
	 * @see sa.com.mobily.eportal.common.service.dao.ifc.ePortalDBDaoIfc#clearRelatedSessionVORow(java.lang.String)
	 */
	public void clearRelatedSessionVORow(String MSISDN){
		log.debug("ePortalDBDao > clearRelatedSessionVORow  > MSISDN::"+MSISDN);
		Connection connection = null;
		PreparedStatement statement=null;
		try {
			String sqlQuery = "DELETE FROM SR_CUSTOMER_SESSIONVO_TBL where  PRIMARY_MSISDN = ?";
			
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery);
			statement.setString(1,MSISDN);
			statement.executeUpdate();
		} catch(SQLException e) {
			log.fatal("ePortalDBDao > clearRelatedSessionVORow  > SQLException > "+e);
		} catch(Exception e) {
		   	log.fatal("ePortalDBDao > clearRelatedSessionVORow  > Exception > "+e);
		} finally {
		   	JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
		
	}
	/* 
	 * this method is responsible for creating a new record under SR_CUSTOMER_SESSIONVO_TBL after login
	 * (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.ifc.ePortalDBDaoIfc#insertNewSessionVORow(sa.com.mobily.eportal.common.service.valueobject.SessionVO)
	 */
	public void insertNewSessionVORow(SessionVO sessionVO){
		
		log.debug("ePortalDBDao > insertNewSessionVORow  > MSISDN::"+sessionVO.getActiveMSISDN() +" , Username ["+sessionVO.getPortalUserName()+"] , LineType ["+sessionVO.getLineType()+"] , Customer Type ["+sessionVO.getCustomerType()+"] , Package ID ["+sessionVO.getPackageID()+"] ,Package name ["+sessionVO.getSiebelPackageName()+"]");
		Connection connection = null;
		PreparedStatement statement=null;
		try {
			String sqlQuery = "INSERT INTO SR_CUSTOMER_SESSIONVO_TBL(USERNAME, MSISDN, PRIMARY_MSISDN , LINE_TYPE, CUSTOMER_TYPE, PACKAGE_ID, PACKAGE_NAME, CREATION_DATE, ISIUC)  values (?,?,?,?,?,?,?,sysdate ,?)";
			
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			statement  = connection.prepareStatement(sqlQuery);
			statement.setString(1,sessionVO.getPortalUserName());
			statement.setString(2,sessionVO.getActiveMSISDN());
			statement.setString(3,"");
			statement.setInt(4,sessionVO.getLineType());
			statement.setInt(5,sessionVO.getCustomerType());
			statement.setString(6,sessionVO.getPackageID());
			statement.setString(7,sessionVO.getSiebelPackageName());
			if(sessionVO.isIUC())
				statement.setInt(8,1);
			else
				statement.setInt(8,0);
			statement.executeUpdate();
			log.debug("ePortalDBDao > insertNewSessionVORow  > record inserted for MSISDN::"+sessionVO.getActiveMSISDN() );
		} catch(SQLException e) {
			log.fatal("ePortalDBDao > insertNewSessionVORow  > SQLException > "+e);
		} catch(Exception e) {
		   	log.fatal("ePortalDBDao > insertNewSessionVORow  > Exception > "+e);
		} finally {
		   	JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
	}
	/* (non-Javadoc)
	 * this method is responsible for creating a new record under SR_CUSTOMER_SESSIONVO_TBL after chnage active number
	 * @see sa.com.mobily.eportal.common.service.dao.ifc.ePortalDBDaoIfc#insertNewRelatedSessionVORow(sa.com.mobily.eportal.common.service.valueobject.SessionVO)
	 */
	public void insertNewRelatedSessionVORow(SessionVO sessionVO){
		
		log.debug("ePortalDBDao > insertNewRelatedSessionVORow  > MSISDN::"+sessionVO.getActiveMSISDN() +" , Username ["+sessionVO.getPortalUserName()+"] , LineType ["+sessionVO.getLineType()+"] , Customer Type ["+sessionVO.getCustomerType()+"] , Package ID ["+sessionVO.getPackageID()+"] ,Package name ["+sessionVO.getSiebelPackageName()+"]");
		Connection connection = null;
		PreparedStatement statement=null;
		try {
			
			String sqlQuery = "INSERT INTO SR_CUSTOMER_SESSIONVO_TBL(USERNAME, MSISDN, PRIMARY_MSISDN , LINE_TYPE, CUSTOMER_TYPE, PACKAGE_ID, PACKAGE_NAME, CREATION_DATE, ISIUC)  values (?,?,?,?,?,?,?,sysdate ,?)";
			
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			//connection = DataBaseConnectionHandler.getLocalInConnection();
			statement  = connection.prepareStatement(sqlQuery);
			statement.setString(1,sessionVO.getPortalUserName());
			statement.setString(2,sessionVO.getActiveMSISDN());
			statement.setString(3,sessionVO.getOriginalMsisdn());
			statement.setInt(4,sessionVO.getLineType());
			statement.setInt(5,sessionVO.getCustomerType());
			statement.setString(6,sessionVO.getPackageID());
			statement.setString(7,sessionVO.getSiebelPackageName());
			if(sessionVO.isIUC())
				statement.setInt(8,1);
			else
				statement.setInt(8,0);
			statement.executeUpdate();
			log.debug("ePortalDBDao > insertNewSessionVORow  > record inserted for MSISDN::"+sessionVO.getActiveMSISDN() );
		} catch(SQLException e) {
			log.fatal("ePortalDBDao > insertNewSessionVORow  > SQLException > "+e);
		} catch(Exception e) {
		   	log.fatal("ePortalDBDao > insertNewSessionVORow  > Exception > "+e);
		} finally {
		   	JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
	}
	
public synchronized SessionVO  retrieveRelatedSessionVORow(String msisdn){
		
		log.debug("ePortalDBDao > retrieve the related account for   > MSISDN::"+msisdn);
		Connection connection = null;
		PreparedStatement statement=null;
		ResultSet resultSet = null;
		SessionVO sessionVO = null;
		try {
			String sqlQuery = "select * from SR_CUSTOMER_SESSIONVO_TBL where primary_MSISDN = ? ";
			
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			//connection = DataBaseConnectionHandler.getLocalInConnection();
			statement  = connection.prepareStatement(sqlQuery);
			statement.setString(1,msisdn);
			resultSet = statement.executeQuery();
			if(resultSet.next()){
				sessionVO = new SessionVO();
				sessionVO.setOriginalMsisdn(msisdn);
				sessionVO.setActiveMSISDN(resultSet.getString("MSISDN"));
				sessionVO.setPortalUserName(resultSet.getString("USERNAME"));
				sessionVO.setOriginalMsisdn(resultSet.getString("PRIMARY_MSISDN"));
				sessionVO.setLineType(resultSet.getInt("LINE_TYPE"));
				sessionVO.setCustomerType(resultSet.getByte("CUSTOMER_TYPE"));
				sessionVO.setPackageID(resultSet.getString("PACKAGE_ID"));
				sessionVO.setSiebelPackageName(resultSet.getString("PACKAGE_NAME"));
				
				if(sessionVO.getLineType() == ConstantIfc.LINE_TYPE_CORPORATE_AUTHORIZED_PERSON){
					sessionVO.setBillingAccountId(sessionVO.getActiveMSISDN());
					sessionVO.setRootBillingAccountNumber(sessionVO.getActiveMSISDN());
				}
				
			}
			log.debug("ePortalDBDao > insertNewSessionVORow  > record inserted for MSISDN::"+sessionVO.getActiveMSISDN() );
		} catch(SQLException e) {
			log.fatal("ePortalDBDao > insertNewSessionVORow  > SQLException > "+e);
		} catch(Exception e) {
		   	log.fatal("ePortalDBDao > insertNewSessionVORow  > Exception > "+e);
		} finally {
		   	JDBCUtility.closeJDBCResoucrs(connection , statement , resultSet);
		}
		log.debug("ePortalDBDao > retrieve the related account for   > MSISDN["+msisdn+"] retrieved successfully " );
		return sessionVO;
	}

public synchronized SessionVO  retrieveOriginalSessionVORow(String msisdn){
	
	log.debug("ePortalDBDao > retrieve the Original account for   > MSISDN::"+msisdn);
	Connection connection = null;
	PreparedStatement statement=null;
	ResultSet resultSet = null;
	SessionVO sessionVO = null;
	try {
		String sqlQuery = "select * from SR_CUSTOMER_SESSIONVO_TBL where MSISDN = ? ";
		
		connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		//connection = DataBaseConnectionHandler.getLocalInConnection();
		statement  = connection.prepareStatement(sqlQuery);
		statement.setString(1,msisdn);
		resultSet = statement.executeQuery();
		if(resultSet.next()){
			sessionVO = new SessionVO();
			sessionVO.setOriginalMsisdn(msisdn);
			sessionVO.setActiveMSISDN(resultSet.getString("MSISDN"));
			sessionVO.setPortalUserName(resultSet.getString("USERNAME"));
			sessionVO.setOriginalMsisdn(msisdn);
			sessionVO.setLineType(resultSet.getInt("LINE_TYPE"));
			sessionVO.setCustomerType(resultSet.getByte("CUSTOMER_TYPE"));
			sessionVO.setPackageID(resultSet.getString("PACKAGE_ID"));
			sessionVO.setSiebelPackageName(resultSet.getString("PACKAGE_NAME"));
		}
		log.debug("ePortalDBDao > insertNewSessionVORow  > record inserted for MSISDN::"+sessionVO.getActiveMSISDN() );
	} catch(SQLException e) {
		log.fatal("ePortalDBDao > insertNewSessionVORow  > SQLException > "+e);
	} catch(Exception e) {
	   	log.fatal("ePortalDBDao > insertNewSessionVORow  > Exception > "+e);
	} finally {
	   	JDBCUtility.closeJDBCResoucrs(connection , statement , resultSet);
	}
	log.debug("ePortalDBDao > retrieve the related account for   > MSISDN["+msisdn+"] retrieved successfully " );
	return sessionVO;
}
/*
 * this method will be responsible for retriving the main customer session vo
 * 
 * **/
public synchronized SessionVO  retrieveSessionVORow(String msisdn){
	
	log.debug("ePortalDBDao > retrieve the related account for   > MSISDN::"+msisdn);
	Connection connection = null;
	PreparedStatement statement=null;
	ResultSet resultSet = null;
	SessionVO sessionVO = null;
	try {
		String sqlQuery = "select * from SR_CUSTOMER_SESSIONVO_TBL where MSISDN = ? ";
		connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		//connection = DataBaseConnectionHandler.getLocalInConnection();
		statement  = connection.prepareStatement(sqlQuery);
		statement.setString(1,msisdn);
		resultSet = statement.executeQuery();
		if(resultSet.next()){
			sessionVO = new SessionVO();
			sessionVO.setOriginalMsisdn(msisdn);
			sessionVO.setPortalUserName(resultSet.getString("USERNAME"));
			sessionVO.setOriginalMsisdn(resultSet.getString("PRIMARY_MSISDN"));
			sessionVO.setLineType(resultSet.getInt("LINE_TYPE"));
			sessionVO.setCustomerType(resultSet.getByte("CUSTOMER_TYPE"));
			sessionVO.setPackageID(resultSet.getString("PACKAGE_ID"));
			sessionVO.setSiebelPackageName(resultSet.getString("PACKAGE_NAME"));
		}
		log.debug("ePortalDBDao > insertNewSessionVORow  > record inserted for MSISDN::"+sessionVO.getActiveMSISDN() );
	} catch(SQLException e) {
		log.fatal("ePortalDBDao > insertNewSessionVORow  > SQLException > "+e);
	} catch(Exception e) {
	   	log.fatal("ePortalDBDao > insertNewSessionVORow  > Exception > "+e);
	} finally {
	   	JDBCUtility.closeJDBCResoucrs(connection , statement , resultSet);
	}
	log.debug("ePortalDBDao > retrieve the related account for   > MSISDN["+msisdn+"] retrieved successfully " );
	return sessionVO;
}
	public static void main(String agrs[]) {
		
		ePortalDBDao dao = new ePortalDBDao();
		SessionVO  vo = new SessionVO();
		vo.setActiveMSISDN("966561110152");
		vo.setOriginalMsisdn("966561110151");
		vo.setPortalUserName("msayed_asm");
		vo.setLineType(12);
		dao.insertNewRelatedSessionVORow(vo);
	}
	 public static Connection getLocalConnection() {
	    	Connection connection = null;
	    	StringBuffer tempQuery = null;
			PreparedStatement preparedStatement = null;
			ResultSet rs = null;
			//Connection connection = null;
			ArrayList list = new ArrayList();
			String accountPOID = null;
	        try
	    	    {
	            Driver d = (Driver)Class.forName( "oracle.jdbc.driver.OracleDriver" ).newInstance();
	            connection = DriverManager.getConnection( "jdbc:oracle:thin:@10.14.11.203:1521:EPDEV",
	                 						 "eedbusr",
	            							 "eedbusr")  ;
	    	}catch (SQLException e)
	        {
	            // TODO Auto-generated catch block
	        	//System.out.print("DatBaseHandler > getLocalConnection >SQLException >" +e.getMessage());
	        } catch (InstantiationException e) {
	    		// TODO Auto-generated catch block
	        	throw new SystemException(e);
	    	} catch (IllegalAccessException e) {
	    		// TODO Auto-generated catch block
	    		throw new SystemException(e);
	    	} catch (ClassNotFoundException e) {
	    		// TODO Auto-generated catch block
	    		throw new SystemException(e);
	    	}
	        return connection;
	    }

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.ifc.ePortalDBDaoIfc#getResetPortletrequiredPages()
	 */
	public List getResetPortletrequiredPages() {
		log.info("ePortalDBDao > getResetPortletrequiredPages :: start" );
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List resetPortletRequiredPages = null;
		try {
			String sqlQuery = "select PAGE_UNIQUE_NAME from RESETPORTLET_REQUIRED_PAGES";
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(sa.com.mobily.eportal.common.service.constant.ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery);
			resultSet = statement.executeQuery();
			resetPortletRequiredPages = new ArrayList();
			if(resultSet.next()) {
				resetPortletRequiredPages.add(resultSet.getString("PAGE_UNIQUE_NAME"));
			}
		} catch(SQLException e) {
			log.fatal("ePortalDBDao > getResetPortletrequiredPages  > SQLException > "+e);
			throw new SystemException(e);
		} catch(Exception e) {
		   	log.fatal("ePortalDBDao > getResetPortletrequiredPages  > Exception > "+e);
		   	throw new SystemException(e);
		} finally {
		   	JDBCUtility.closeJDBCResoucrs(connection , statement , resultSet);
		}
		log.info("ePortalDBDao > getResetPortletrequiredPages :: end" );
		return resetPortletRequiredPages;
	}

	/**
	 * check if the power user (user created for specifc team like sales , call center empowerment) updated his password after first login or not
	 * 
	 * @param username
	 * @return boolean
	 */
	public boolean isPowerUsr_UpdatedHisPassword(String userName){
		boolean isUpdated = false;
		Connection connection = null;
		PreparedStatement statement=null;
		ResultSet l_result = null;
		try {
			String sqlQuery = "SELECT USERNAME FROM SR_POWERUSER_PASS_AUDIT WHERE USERNAME=?";
	
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1,userName);
			l_result = statement.executeQuery();
			
			if(l_result.next()){
				isUpdated = true;
			}
		}catch(SQLException sqlEx){
		    log.fatal("MobilyCommonService --> ePortalDBDao --> isPowerUsr_UpdatedHisPassword :: SQLException "+sqlEx);
		}
		catch(Exception e){
			log.fatal("MobilyCommonService --> ePortalDBDao --> isPowerUsr_UpdatedHisPassword :: Exception "+e);
		}
		finally{
		    JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
		return isUpdated;
	}
	
	
	/**
	 * insert a record in the table to know later is the power user customer updated his password or not
	 * 
	 * @param username
	 * @return
	 * @throws Exception
	 * @throws SQLException
	 */
	public void updatePowerUsrPassword(String userName ){
		Connection connection = null;
		PreparedStatement statement=null;
		try {
			String sqlQuery = "insert into SR_POWERUSER_PASS_AUDIT values (? , sysdate)";
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1,userName);
			statement.execute();
		}
		catch (SQLException e) {
			log.fatal("ePortalDBDao > updatePowerUsrPassword > SQLException " + e.getMessage());
	
		} catch (Exception e) {
			log.debug("ePortalDBDao > updatePowerUsrPassword > Exception " + e.getMessage());
	
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, statement,null);
		}
	}

	/*
	 * this used for auditing the inactive account to do the action later
	 * **/
	public void auditInactiveAccount(InactiveObject inactiveObject) {
		Connection connection = null;
		PreparedStatement statement=null;
		
		try {
			
			
			String sqlQuery = "INSERT INTO USER_INACTIVE_TBL(MSISDN , BACKEND_IQAMA,INACTIVE_TYPE) VALUES(?,?,?)";
	
			//connection = DataBaseHandler.getConnection();
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1,inactiveObject.getMsisdn());
			statement.setString(2,inactiveObject.getIqama());
			statement.setInt(3,inactiveObject.getInactiveType());
			statement.execute();
		}
		catch(SQLException e){
            log.fatal("SQLException " + e.getMessage());
             
         }
        catch(Exception e){
            log.debug("Exception " + e.getMessage());
            
        }finally{
		    JDBCUtility.closeJDBCResoucrs(connection , statement , null);
		}
	}

	/**
	 * 
	 * @param msisdn
	 * @return
	 * @throws Exception
	 */
	public UserAccountVO getUserAccountInfoFromManageTable(int id, String msisdn) {
		Connection connection = null;
		PreparedStatement pStemt = null;
		ResultSet rslt = null;
		UserAccountVO objUserAccountVO = null;
		StringBuffer sqlQuery = null;
		
		try {
			sqlQuery = new StringBuffer();
				sqlQuery.append("SELECT MSISDN ,USERNAME ,PRIVILEGE, SERVICE_ACT_NUM, LINE_TYPE, IQAMA, CUSTOMERTYPE FROM USER_MANAGED_NO WHERE MSISDN = ? AND ID = ?");
			
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
						
			pStemt = connection.prepareStatement(sqlQuery.toString());
			pStemt.setString(1, msisdn);
			rslt = pStemt.executeQuery();
			while (rslt.next()) {
				objUserAccountVO = new UserAccountVO();
				objUserAccountVO.setId(id);
				objUserAccountVO.setMsisdn(msisdn);
				objUserAccountVO.setUserName(rslt.getString("USERNAME"));
				objUserAccountVO.setPrivilege(rslt.getString("PRIVILEGE"));
				objUserAccountVO.setServiceAccountNumber(rslt.getString("SERVICE_ACT_NUM"));
				objUserAccountVO.setLineType(rslt.getInt("LINE_TYPE"));
				if(!FormatterUtility.isEmpty(rslt.getString("CUSTOMERTYPE")))
					objUserAccountVO.setCustomerType((byte)rslt.getInt("CUSTOMERTYPE"));
				objUserAccountVO.setIqama(rslt.getString("IQAMA"));
			}
		} catch (SQLException e) {
			log.fatal("ePortalDBDao > getUserAccountWebProfile > SQLException " + e.getMessage());
			throw new SystemException(e);
	
		} catch (Exception e) {
			log.debug("ePortalDBDao > getUserAccountWebProfile > Exception " + e.getMessage());
			throw new SystemException(e);
	
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, pStemt, rslt);
		}
		return objUserAccountVO;
	}
	
	
	/**
	 * 
	 * @param objUserAccountVO
	 * @return
	 * @throws Exception
	 */
	public boolean updateLineTypeInUserAccount(String msisdn, int lineType){
		
		boolean status = false;
		Connection connection = null;
		PreparedStatement pStemt = null;
		StringBuffer sqlQuery = null;
		
		try {
			sqlQuery = new StringBuffer();
				sqlQuery.append("UPDATE USER_ACCOUNT SET LINE_TYPE = ? WHERE MSISDN = ?");
			
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			pStemt = connection.prepareStatement(sqlQuery.toString());
			
				pStemt.setInt(1, lineType);
				pStemt.setString(2, msisdn);
	
				pStemt.executeUpdate();
				
				status = true;
		} catch (SQLException e) {
			log.fatal("ePortalDBDao > updateLineTypeInUserAccount > SQLException " + e.getMessage());
			throw new SystemException(e);
	
		} catch (Exception e) {
			log.debug("ePortalDBDao > updateLineTypeInUserAccount > Exception " + e.getMessage());
			throw new SystemException(e);
	
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, pStemt, null);
		}
		return status;
	}

	
	/**
	 * 
	 * @param objUserAccountVO
	 * @return
	 * @throws Exception
	 */
	public boolean updateLineTypeInUserManagedNo(int id, String msisdn, int lineType){
		
		boolean status = false;
		Connection connection = null;
		PreparedStatement pStemt = null;
		StringBuffer sqlQuery = null;
		
		try {
			sqlQuery = new StringBuffer();
				sqlQuery.append("UPDATE USER_MANAGED_NO SET LINE_TYPE = ? WHERE MSISDN = ? AND ID = ?");
			
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			pStemt = connection.prepareStatement(sqlQuery.toString());
			
				pStemt.setInt(1, lineType);	
				pStemt.setString(2, msisdn);
				pStemt.setInt(3, id);
				
	
				pStemt.executeUpdate();
				
				status = true;
		} catch (SQLException e) {
			log.fatal("ePortalDBDao > updateLineTypeInUserManagedNo > SQLException " + e.getMessage());
			throw new SystemException(e);
	
		} catch (Exception e) {
			log.debug("ePortalDBDao > updateLineTypeInUserManagedNo > Exception " + e.getMessage());
			throw new SystemException(e);
	
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, pStemt, null);
		}
		return status;
	}

}
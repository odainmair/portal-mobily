package sa.com.mobily.eportal.common.service.exception;

import java.util.Set;

public class BusinessValidationException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private String errorCode = "";
	private String errorMessage;
	private Set<String> violationMessages;

	public BusinessValidationException() {
		super();
	}


	public BusinessValidationException(Set<String> violationMessages) {
		super();
		this.violationMessages = violationMessages;
	}
	public BusinessValidationException(String errorCode, String errorMessage) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

		public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Set<String> getViolationMessages() {
		return violationMessages;
	}

	public void setViolationMessages(Set<String> violationMessages) {
		this.violationMessages = violationMessages;
	}

	public BusinessValidationException(String errorCode, String errorMessage,
			Set<String> violationMessages) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.violationMessages = violationMessages;
	}

	
	
	
	
}

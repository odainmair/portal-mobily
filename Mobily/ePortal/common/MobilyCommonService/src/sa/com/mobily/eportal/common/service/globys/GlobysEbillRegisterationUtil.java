package sa.com.mobily.eportal.common.service.globys;



import java.io.PrintStream;

import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

 
import org.w3c.dom.Document;
 
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.constants.MobilyResEnvKeysConstants;
import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.GlobyusGroupIdSeqDAO;
import sa.com.mobily.eportal.common.service.dao.imp.ePortalDBDao;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.StackTraceUtil;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.vo.GlobysEbillUserVO;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

/**
 * This class is responsible  for  registering user into Globys System.
 * <p> 
 
 *  
 * @author Yousef Alkhalaileh
 */
public class GlobysEbillRegisterationUtil {
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.GLOBYS_REG_SERVICE_LOGGER_NAME);

	private static String clazz = GlobysEbillRegisterationUtil.class.getName();

	public static void registerCAP(GlobysEbillUserVO globysEbillUserVO) {
		try {
			executionContext.fine("registerCAP method with  globysEbillUserVO="+globysEbillUserVO);
			String ebillURL=ResourceEnvironmentProviderService.getInstance().getStringProperty(MobilyResEnvKeysConstants.EBILL_GLOBYUS_URL_USER_ACCESS);
			executionContext.fine("ebillURL"+ebillURL);
			URL url = new URL(ebillURL);
			executionContext.fine("url object="+url);
			// Create connection and set properties
			URLConnection conn = url.openConnection();
			executionContext.fine("conn="+conn);
			// Never use "GET" when calling the API
			((HttpURLConnection) conn).setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);

			// Do not use "plain/text" or "application/x-www-form-urlencoded"
			conn.setRequestProperty("Content-Type", "text/xml");

			// Get the output stream
			PrintWriter pw = new PrintWriter(new PrintStream(conn.getOutputStream()));

			// It is better to use Credential Valut Service to reterive and store
			// System accounts
			StringBuffer request = new StringBuffer("<?xml version=\"1.0\" encoding=\"windows-1252\" ?>");
			request.append("<cv:request xmlns:cv=\"urn:bapi\"><lgn>");
			request.append(ResourceEnvironmentProviderService.getInstance().getStringProperty(MobilyResEnvKeysConstants.EBILL_GLOBYUS_USERNAME) + "</lgn><pwd>"
				+ ResourceEnvironmentProviderService.getInstance().getStringProperty(MobilyResEnvKeysConstants.EBILL_GLOBYUS_PASSWORD));
			request.append("</pwd><userId>" + globysEbillUserVO.getUsername() + "</userId><orgNum>" + globysEbillUserVO.getBillingAcctNumber());
			request.append("</orgNum><userState>" + GlobysEbillUserVO.userState + "</userState>" + "</cv:request>");
			// ToDo: ignore SSL errors outside of production environment
			executionContext.audit(Level.INFO, "Gloybas registerCAP 1:"+request.toString(), clazz, "registerCAP");
			pw.print(request);
			pw.flush();
			pw.close();
 
			// Read the response into a document for further processing
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(conn.getInputStream());

			// Check response status to decide how to proceed
			NodeList nodes = doc.getElementsByTagName("status");
			String status = nodes.item(0).getTextContent();
			if (!status.equals("S")) {
				// Error code path
				String message = doc.getElementsByTagName("error").item(0).getTextContent();
				executionContext.log(Level.SEVERE, "Failed to regiser the user in Globys E-bill .Error: " + message, clazz, "registerCAP", null);
				// System.out.print(
				// "Failed to regiser the user in Globys E-bill .Error: " +
				// message);
			} else 
				executionContext.audit(Level.INFO, "Gloybas SetUserConfig ["+globysEbillUserVO.getBillingAcctNumber()+"] called", clazz, "registerCAP");
				SetUserConfig(globysEbillUserVO);
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "Unexpected error occured during register the user in Globys e-bill. " + e, clazz, "registerCAP", e);
			executionContext.log(Level.SEVERE, StackTraceUtil.getCustomStackTrace(e), clazz, "registerCAP", e);
		}
	}
	
	public static void registerIndividuleLine(GlobysEbillUserVO globysEbillUserVO) {
		try {
			URL url = new URL(ResourceEnvironmentProviderService.getInstance().getStringProperty(MobilyResEnvKeysConstants.EBILL_GLOBYUS_URL_USER_ACCESS));
			// Create connection and set properties
			URLConnection conn = url.openConnection();

			// Never use "GET" when calling the API
			((HttpURLConnection) conn).setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);

			// Do not use "plain/text" or "application/x-www-form-urlencoded"
			conn.setRequestProperty("Content-Type", "text/xml");

			// Get the output stream
			PrintWriter pw = new PrintWriter(new PrintStream(conn.getOutputStream()));

			// It is better to use Credential Valut Service to reterive and store
			// System accounts
			StringBuffer request = new StringBuffer("<?xml version=\"1.0\" encoding=\"windows-1252\" ?>");
			request.append("<cv:request xmlns:cv=\"urn:bapi\"><lgn>");
			request.append(ResourceEnvironmentProviderService.getInstance().getStringProperty(MobilyResEnvKeysConstants.EBILL_GLOBYUS_USERNAME) + "</lgn><pwd>"
				+ ResourceEnvironmentProviderService.getInstance().getStringProperty(MobilyResEnvKeysConstants.EBILL_GLOBYUS_PASSWORD));
			request.append("</pwd><userId>" + globysEbillUserVO.getUsername() + "</userId><orgNum>" + globysEbillUserVO.getBillingAcctNumber());
			request.append("</orgNum><userState>" + ConstantIfc.GLOBYS_INDIVIDULE_USER_STAT + "</userState>" + "</cv:request>");
			// ToDo: ignore SSL errors outside of production environment
			executionContext.audit(Level.INFO, "Gloybas Registration Request 1:"+request.toString(), clazz, "registerIndividuleLine");
			pw.print(request);
			pw.flush();
			pw.close();
 
			// Read the response into a document for further processing
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(conn.getInputStream());

			// Check response status to decide how to proceed
			NodeList nodes = doc.getElementsByTagName("status");
			String status = nodes.item(0).getTextContent();
			if (!status.equals("S")) {
				// Error code path
				
				String message = doc.getElementsByTagName("error").item(0).getTextContent();
				executionContext.log(Level.INFO, "Register Customer  , Service Account ["+globysEbillUserVO.getBillingAcctNumber()+"] Failed > "+message, clazz, "registerIndividuleLine", null);
				executionContext.log(Level.SEVERE, "Failed to regiser the user in Globys E-bill .Error: " + message, clazz, "registerIndividuleLine", null);
				// System.out.print(
				// "Failed to regiser the user in Globys E-bill .Error: " +
				// message);
			} else  
			{
				executionContext.audit(Level.INFO, "Register Customer  , Service Account ["+globysEbillUserVO.getBillingAcctNumber()+"] Succeeded> ", clazz, "registerIndividuleLine");
				executionContext.audit(Level.INFO, "Start Set UserAccountOrg Access , Service Account ["+globysEbillUserVO.getBillingAcctNumber()+"]", clazz, "registerIndividuleLine");
				String orgStatus =SetAccountOrgAccess(globysEbillUserVO);
				if(orgStatus.equals("S")){
					executionContext.audit(Level.INFO, "Start Set UserAccountOrg Access , Service Account ["+globysEbillUserVO.getBillingAcctNumber()+"] Succeeded", clazz, "registerIndividuleLine");
					SetUserConfig(globysEbillUserVO);
				}else{
					
				}
			}
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "Unexpected error occured during register the user in Globys e-bill. " + e, clazz, "registerIndividuleLine", e);
			executionContext.log(Level.SEVERE, StackTraceUtil.getCustomStackTrace(e), clazz, "registerIndividuleLine", e);
		}
	}
	public static void SetUserConfig(GlobysEbillUserVO globysEbillUserVO) {
			try {
				URL url = new URL(ResourceEnvironmentProviderService.getInstance().getStringProperty(MobilyResEnvKeysConstants.EBILL_GLOBYUS_URL_USER_CONFIG));
				// Create connection and set properties
				URLConnection conn = url.openConnection();

				// Never use "GET" when calling the API
				((HttpURLConnection) conn).setRequestMethod("POST");
				conn.setDoOutput(true);
				conn.setDoInput(true);
				conn.setUseCaches(false);

				// Do not use "plain/text" or "application/x-www-form-urlencoded"
				conn.setRequestProperty("Content-Type", "text/xml");

				// Get the output stream
				PrintWriter pw = new PrintWriter(new PrintStream(conn.getOutputStream()));

				// It is better to use Credential Valut Service to reterive and store password
				// System accounts
				StringBuffer request = new StringBuffer("<?xml version=\"1.0\" encoding=\"windows-1252\" ?>");
				request.append("<cv:request xmlns:cv=\"urn:bapi\"><lgn>");
				request.append(ResourceEnvironmentProviderService.getInstance().getStringProperty(MobilyResEnvKeysConstants.EBILL_GLOBYUS_USERNAME) + "</lgn><pwd>"
					+ ResourceEnvironmentProviderService.getInstance().getStringProperty(MobilyResEnvKeysConstants.EBILL_GLOBYUS_PASSWORD)+"</pwd>");
				request.append("<userId>" + globysEbillUserVO.getUsername() + "</userId>");
				if(!FormatterUtility.isEmpty(globysEbillUserVO.getEmail()))
				request.append("<eml>" + globysEbillUserVO.getEmail()+"</eml>");
				if(!FormatterUtility.isEmpty(globysEbillUserVO.getFirstName()))
				request.append("<firstname>" + globysEbillUserVO.getFirstName() + "</firstname>" );
				if(!FormatterUtility.isEmpty(globysEbillUserVO.getLastName()))
				request.append("<lastname>" + globysEbillUserVO.getLastName() + "</lastname>" );
				if(!FormatterUtility.isEmpty(globysEbillUserVO.getPreferedLanguage()))
				 request.append("<language>" + globysEbillUserVO.getPreferedLanguage() + "</language> " );
				request.append("</cv:request>");
				executionContext.audit(Level.INFO, "Start Set SetUserConfig Access , Service Account ["+globysEbillUserVO.getBillingAcctNumber()+"] Called > "+request.toString(), clazz, "SetUserConfig");
				// ToDo: ignore SSL errors outside of production environment language
				pw.print(request);
				pw.flush();
				pw.close();

				// Read the response into a document for further processing
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				DocumentBuilder db = dbf.newDocumentBuilder();
				Document doc = db.parse(conn.getInputStream());

				// Check response status to decide how to proceed
				NodeList nodes = doc.getElementsByTagName("status");
				String status = nodes.item(0).getTextContent();
				if (!status.equals("S")) {
					// Error code path
					String message = doc.getElementsByTagName("error").item(0).getTextContent();
					executionContext.log(Level.SEVERE, "Failed to config the user Service Account ["+globysEbillUserVO.getBillingAcctNumber()+"] , ["+globysEbillUserVO.getUsername()+"]in Globys E-bill .Error: " + message, clazz, "SetUserConfig", null);
					// System.out.print(
					// "Failed to regiser the user in Globys E-bill .Error: " +
					// message);
				}else
				{
					executionContext.log(Level.SEVERE, "config the user Service Account ["+globysEbillUserVO.getBillingAcctNumber()+"] , ["+globysEbillUserVO.getUsername()+"] done ", clazz, "SetUserConfig", null);
				}
			} catch (Exception e) {
				executionContext.log(Level.SEVERE, "Unexpected error occured during configuration the user in Globys e-bill. " + e, clazz, "SetUserConfig", e);
				executionContext.log(Level.SEVERE, StackTraceUtil.getCustomStackTrace(e), clazz, "SetUserConfig", e);
				
			}
		
	}
	

		/**This method will create or update the account access list for one or more users in one or more
		   *organizations.
		   *Notes:
  		   *� If a user ID does not exist it will be created within the specified organization
		   *� The API can and should be called for many organizations at a time.
		**/
		private static String SetAccountOrgAccess(GlobysEbillUserVO globysEbillUserVO) {
			String status ="S";
			try {
				String groupid = ""+GlobyusGroupIdSeqDAO.getInstance().getNextValue();
				
				URL url =new URL(ResourceEnvironmentProviderService.getInstance().getStringProperty(MobilyResEnvKeysConstants.EBILL_GLOBYUS_URL_ACCT_ACCESS));
				// Create connection and set properties
				URLConnection conn = url.openConnection();

				// Never use "GET" when calling the API
				((HttpURLConnection) conn).setRequestMethod("POST");
				conn.setDoOutput(true);
				conn.setDoInput(true);
				conn.setUseCaches(false);

				// Do not use "plain/text" or "application/x-www-form-urlencoded"
				conn.setRequestProperty("Content-Type", "text/xml");

				// Get the output stream
				PrintWriter pw = new PrintWriter(new PrintStream(conn.getOutputStream()));

				// It is better to use Credential Valut Service to reterive and store password
				// System accounts
				StringBuffer request = new StringBuffer("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
				request.append("<cv:request xmlns:cv=\"urn:bapi\"><lgn>");
				request.append(ResourceEnvironmentProviderService.getInstance().getStringProperty(MobilyResEnvKeysConstants.EBILL_GLOBYUS_USERNAME) + "</lgn><pwd>"
					+ ResourceEnvironmentProviderService.getInstance().getStringProperty(MobilyResEnvKeysConstants.EBILL_GLOBYUS_PASSWORD)+"</pwd>");
				
				request.append("<orgList>");
				request.append("<org>");
				request.append("<orgNum>");
				request.append(globysEbillUserVO.getBillingAcctNumber());
				request.append("</orgNum>");
				request.append("<accountGroupList>");
				request.append("<accountGroup>");
					request.append("<accountGroupId>"+groupid+"</accountGroupId>");
					request.append("<accountGroupType>E</accountGroupType>");
					request.append("<accountList>");
						request.append("<account>"+globysEbillUserVO.getBillingAcctNumber()+"</account>");
					request.append("</accountList>");
				request.append("</accountGroup>");
				request.append("</accountGroupList>");
				request.append("<userList><user>");
				request.append("<userId>"+globysEbillUserVO.getUsername()+"</userId>");
				request.append("<userState>explicitAccess</userState>");
				request.append("<accountGroupId>"+groupid+"</accountGroupId>");
				request.append("</user>");
				request.append("</userList>");
				request.append("</org>");
				request.append("</orgList>");
				request.append("</cv:request>");
				// ToDo: ignore SSL errors outside of production environment language
				pw.print(request);
				pw.flush();
				pw.close();

				// Read the response into a document for further processing
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				DocumentBuilder db = dbf.newDocumentBuilder();
				Document doc = db.parse(conn.getInputStream());

				// Check response status to decide how to proceed
				NodeList nodes = doc.getElementsByTagName("status");
				status = nodes.item(0).getTextContent();
				if (!status.equals("S")) {
					// Error code path
					String message = doc.getElementsByTagName("error").item(0).getTextContent();
					executionContext.log(Level.SEVERE, "Failed to SetAccountOrgAccess config the user ["+globysEbillUserVO.getBillingAcctNumber()+"]in Globys E-bill .Error: " + message, clazz, "SetAccountOrgAccess", null);
					// System.out.print(
					// "Failed to regiser the user in Globys E-bill .Error: " +
					// message);
				}
			} catch (Exception e) {
				executionContext.log(Level.SEVERE, "Unexpected error occured during configuration the user in Globys e-bill. " + e, clazz, "SetAccountOrgAccess", e);
				executionContext.log(Level.SEVERE, StackTraceUtil.getCustomStackTrace(e), clazz, "SetAccountOrgAccess", e);
				
			}
	   return status;	
	}
		
		

		public static void main(String args[]){
			GlobysEbillUserVO globysEbillUserVO = new GlobysEbillUserVO();
			globysEbillUserVO.setBillingAcctNumber("10001211112411");
			globysEbillUserVO.setUsername("msayed");
			
			SetAccountOrgAccess(globysEbillUserVO);
		}

}

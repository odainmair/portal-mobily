package sa.com.mobily.eportal.common.service.lookup.common;

import java.util.ArrayList;
import java.util.logging.Level;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.ibm.websphere.cache.DistributedMap;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.lookup.dao.CachingLookupDAO;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

public class CachingLookupUtil
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	CachingLookupDAO cachingLookupDAO = new CachingLookupDAO();

	private static String clazz = CachingLookupUtil.class.getName();

	public static CachingLookupUtil instance = new CachingLookupUtil();

	private static DistributedMap CachinglookupsMap;

	public static CachingLookupUtil getInstance()
	{
		// initDistributedMap();
		return instance;
	}

	private static void initDistributedMap()
	{
		String methodName = "initDistributedMap";
		try
		{
			if (CachinglookupsMap == null)
				CachinglookupsMap = (DistributedMap) new InitialContext().lookup("mobily/catalog/lookup");
			executionContext.log(Level.INFO, "got the cache object ", clazz, methodName, null);
		}
		catch (NamingException e)
		{
			executionContext.log(Level.SEVERE, "Exception : " + e.getMessage(), clazz, methodName, e);
			throw new SystemException(e.getMessage(), e.getCause());
		}
	}

	public CachingLookupVO getCachingLookupVO(String lookup_type, String code)
	{
		String methodName = "getCachingLookupVO";
		// TODO Should be here the implementation of DynaCache
		try
		{
			initDistributedMap();
			CachingLookupVO cachingLookupVO = (CachingLookupVO) CachinglookupsMap.get(lookup_type + "-" + code);
			if (cachingLookupVO == null)
			{
				executionContext.log(Level.INFO, "Not found in the Cache and get from DB ", clazz, methodName, null);
				cachingLookupVO = cachingLookupDAO.getCachingLookupVOFromDB(lookup_type, code);
				CachinglookupsMap.put(lookup_type + "-" + code, cachingLookupVO);
			}
			return cachingLookupVO;
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception " + e.getMessage(), clazz, methodName, e);
			throw new RuntimeException(e);
		}
	}

	public ArrayList<CachingLookupVO> getCachingLookupVOList(String lookup_type)
	{
		String methodName = "getCachingLookupVOList";
		try
		{
			initDistributedMap();
			ArrayList<CachingLookupVO> cachingLookupVOList = (ArrayList<CachingLookupVO>) CachinglookupsMap.get(lookup_type);
			if (cachingLookupVOList == null || cachingLookupVOList.size() < 1)
			{

				executionContext.log(Level.INFO, "Not found in the Cache and get from DB ", clazz, methodName, null);
				cachingLookupVOList = cachingLookupDAO.getCachingLookupVOListFromDB(lookup_type);
				CachinglookupsMap.put(lookup_type, cachingLookupVOList);
			}
			return cachingLookupVOList;
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception " + e.getMessage(), clazz, methodName, e);
			throw new RuntimeException(e);
		}
	}
}
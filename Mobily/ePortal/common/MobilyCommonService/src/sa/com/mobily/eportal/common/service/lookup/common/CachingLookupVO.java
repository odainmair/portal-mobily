package sa.com.mobily.eportal.common.service.lookup.common;

import java.io.Serializable;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class CachingLookupVO extends BaseVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String lookup_type;
	private String code;    
    private String short_name;       
    private String englishName;    
    private String arabicName;
	public String getLookup_type() {
		return lookup_type;
	}
	public void setLookup_type(String lookup_type) {
		this.lookup_type = lookup_type;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getShort_name() {
		return short_name;
	}
	public void setShort_name(String short_name) {
		this.short_name = short_name;
	}
	public String getEnglishName() {
		return englishName;
	}
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	public String getArabicName() {
		return arabicName;
	}
	public void setArabicName(String arabicName) {
		this.arabicName = arabicName;
	}
	public CachingLookupVO(String lookup_type, String code, String short_name,
			String englishName, String arabicName) {
		super();
		this.lookup_type = lookup_type;
		this.code = code;
		this.short_name = short_name;
		this.englishName = englishName;
		this.arabicName = arabicName;
	}
    
}

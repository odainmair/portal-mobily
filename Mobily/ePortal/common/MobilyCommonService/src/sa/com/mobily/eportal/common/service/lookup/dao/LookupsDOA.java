package sa.com.mobily.eportal.common.service.lookup.dao;

import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.List;

import javax.validation.constraints.NotNull;

import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.common.service.lookup.LookupIdConstants;
import sa.com.mobily.eportal.common.service.lookup.vo.Lookup;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;

public class LookupsDOA extends AbstractDBDAO<Lookup>{
	
	private static final String GET_LOOKUPS_BY_ID = "SELECT * FROM SR_LOOKUP_TBL lookup WHERE lookup.id = ? and lookup.sub_Id > 0 order by ITEM_DESC_AR";
	
	private static final String GET_SALES_ORDER_LOOKUP = "SELECT ID, ID as SUB_ID, NAME as ITEM_DESC_EN, NAME_AR, NAME_AR as ITEM_DESC_AR, TYPE as ITEM FROM SR_SALES_ORDER_LOOKUP_TBL order by ITEM_DESC_AR";
	
	//private static final String GET_CITY_LOOKUP = "SELECT ID, SUB_ID, ITEM, ITEM_DESC_AR, ITEM_DESC_EN FROM CITY_LOOKUP_TBL where SUB_ID > 0 order by ITEM_DESC_AR";
	private static final String GET_CITY_LOOKUP = "SELECT ID, SUB_ID, ITEM, ITEM_DESC_AR, ITEM_DESC_EN FROM CITY_LOOKUP_TBL where SUB_ID > 0 and ID = 2 order by SUB_ID";
	
	private static final String GET_SCENARIO_LOOKUP = "SELECT ID, SUB_ID, ITEM, ITEM_DESC_AR, ITEM_DESC_EN FROM sr_lookup_tbl where SUB_ID > 0 and ID = 20 order by SUB_ID";
	
	private static final String GET_EMIRATES_LOOKUP = "select  ID as SUB_ID, NAME_EN as ITEM_DESC_EN, NAME_AR as ITEM_DESC_AR, CODE as ITEM from SR_CMS_EMIRATE order by NAME_AR";
	
	private static final String GET_GOVERNATE_LOOKUP = "select  ID as SUB_ID, NAME_EN as ITEM_DESC_EN, NAME_AR as ITEM_DESC_AR, CODE as ITEM from SR_CMS_GOVERNATE order by NAME_AR";

	private static final String GET_CMS_CITY_LOOKUP = "select  ID as SUB_ID, NAME_EN as ITEM_DESC_EN, NAME_AR as ITEM_DESC_AR, CODE as ITEM from SR_CMS_CITY order by NAME_AR";
	
	private static final String GET_DISTRICT_LOOKUP = "select  ID as SUB_ID, NAME_EN as ITEM_DESC_EN, NAME_AR as ITEM_DESC_AR, CODE as ITEM from SR_CMS_DESTRICT order by NAME_AR";
	
	private static final String GET_REGION_LOOKUP = "select  ID as SUB_ID, NAME_EN as ITEM_DESC_EN, NAME_AR as ITEM_DESC_AR from SR_CMS_REGION order by NAME_AR";
	
	private static LookupsDOA instance = new LookupsDOA();
	
	protected LookupsDOA() {
		super(DataSources.DEFAULT);
	}
	
	/***
	 * Method for obtaining the singleton instance
	 * 
	 * @return
	 */
	public static LookupsDOA getInstance() {
		return instance;
	}
	
	public List<Lookup> getLookupsByID(long lookupId) {
		return query(GET_LOOKUPS_BY_ID, lookupId);
	}
	
	public List<Lookup> getCustomizedLookup(@NotNull String lookupName) {
	    //here add your specific lookup
		if(lookupName.equals(LookupIdConstants.LOOKUP_SALES_ORDER))
			return query(GET_SALES_ORDER_LOOKUP);
		if(lookupName.equals(LookupIdConstants.LOOKUP_CITY))
			return query(GET_CITY_LOOKUP);
		if(lookupName.equals(LookupIdConstants.LOOKUP_SCENARIO))
			return query(GET_SCENARIO_LOOKUP);
		if(lookupName.equals(LookupIdConstants.LOOKUP_EMIRATES))
			return query(GET_EMIRATES_LOOKUP);
		if(lookupName.equals(LookupIdConstants.LOOKUP_GOVERNATE))
			return query(GET_GOVERNATE_LOOKUP);
		if(lookupName.equals(LookupIdConstants.LOOKUP_CITY_CMS))
			return query(GET_CMS_CITY_LOOKUP);
		if(lookupName.equals(LookupIdConstants.LOOKUP_DISTRICT))
			return query(GET_DISTRICT_LOOKUP);
		if(lookupName.equals(LookupIdConstants.LOOKUP_REGION))
			return query(GET_REGION_LOOKUP);
		
		return null;
	}
	
	@Override
	protected Lookup mapDTO(ResultSet rs) throws SQLException {
		Lookup lookup = new Lookup();
		lookup.setId(rs.getLong("ID"));
		if(rs.getMetaData().getColumnName(2).equalsIgnoreCase("SUB_ID"))
			lookup.setSubId(rs.getLong("SUB_ID"));
		
		lookup.setItem(rs.getString("ITEM"));
		lookup.setItemDescriptionInEnglish(rs.getString("ITEM_DESC_EN"));
		lookup.setItemDescriptionInArabic(rs.getString("ITEM_DESC_AR"));
		return lookup;
	}

}

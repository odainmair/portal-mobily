package sa.com.mobily.eportal.common.service.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import sa.com.mobily.eportal.cacheinstance.valueobjects.CorporateSubscriptionVO;
import sa.com.mobily.eportal.common.service.vo.FootPrintVO;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "firstName", "lastName",
		"email", "corpMasterAcctNo", "corpAPNumber", "isCorpSurveyCompleted", "userPassword",
		"givenName", "custSegment", "roleId", "securityQuestion", "securityAnswer", "birthDate",
		"nationality", "status", "gender", "registrationDate", "preferedLanguage", "userAcctId",
		"activationDate", "createdDate", "lastLoginTime", "userName", "userSubscriptions",
		"defaultSubscriptionIndex", "activationURL", "activationCode", "facebookId","twitterId","registrationSourceId",
		"corporateSubscriptionVO","contactNumber","packageId","packageType","redirectionURL"})
@XmlRootElement(name = "UserAccount")
public class UserAccount extends FootPrintVO {

	private static final long serialVersionUID = -577659404084757454L;
	//@NotNull
	@XmlElement(name = "firstName")
	private String firstName;
	
	//@NotNull
	@XmlElement(name = "lastName")
	private String lastName;
	
	//@NotNull
	@XmlElement(name = "email")
	private String email;
	
	@XmlElement(name = "corpMasterAcctNo")
	private String corpMasterAcctNo;
	
	@XmlElement(name = "corpAPNumber")
	private String corpAPNumber;
	
	@XmlElement(name = "isCorpSurveyCompleted")
	private Boolean isCorpSurveyCompleted;
	
	@NotNull
	@XmlElement(name = "userPassword")
	private String userPassword;
	
	@XmlElement(name = "givenName")
	private String givenName;
	
	@XmlElement(name = "custSegment")
	private String custSegment;
	
	@NotNull
	@XmlElement(name = "roleId")
	private Integer roleId;
	
	@XmlElement(name = "securityQuestion")
	private String securityQuestion;
	
	@XmlElement(name = "securityAnswer")
	private String securityAnswer;
	
	@XmlElement(name = "birthDate")
	private Date birthDate;
	
	//@NotNull
	@XmlElement(name = "nationality")
	private String nationality;
	
	@XmlElement(name = "status")
	private Integer status;
	
	@XmlElement(name = "gender")
	private String gender;
	
	@XmlElement(name = "registrationDate")
	private Date registrationDate;
	
	@XmlElement(name = "preferedLanguage")
	private String preferedLanguage;
	
	@XmlElement(name = "userAcctId")
	private Long userAcctId;
	
	@XmlElement(name = "activationDate")
	private Date activationDate;
	
	@XmlElement(name = "createdDate")
	private Date createdDate;

	@XmlElement(name = "lastLoginTime")
	private Date lastLoginTime;

	@XmlElement(name = "userName")
	private String userName;
	
	@XmlElement(name = "UserSubscription")
	private List<UserSubscription> userSubscriptions=new ArrayList<UserSubscription>();
	
	@XmlElement(name = "defaultSubscriptionIndex")
	private int defaultSubscriptionIndex=-1;//index pointing to default subscription in userSubscriptions list
	
	@XmlElement(name = "activationURL")
	private String  activationURL;  // non persistent  property
	
	@XmlElement(name = "activationCode")
	private String activationCode; //non persistent property
	
	@XmlElement(name = "facebookId")
	private String facebookId;
	
	@XmlElement(name = "twitterId")
	private String twitterId;
	
	@XmlElement(name = "registrationSourceId")
	private Integer registrationSourceId;
	
	//Added by r.agarwal.mit for SOUQ corporate Registration
	@XmlElement(name = "CorporateSubscription")
	private CorporateSubscriptionVO corporateSubscriptionVO;
	
	//Added by r.agarwal.mit for OTT and SOUQ to capture mobile number of the Non-Mobily Customers
	@XmlElement(name = "contactNumber")
	private String contactNumber;
	
	
	//Added by r.ravuru.mit for MyConnect project
	
	@XmlElement(name = "packageId")
	private String packageId;
	
	@XmlElement(name = "packageType")
	private String packageType;
	
	// Added by r.agarwal.mit for eSales
	@XmlElement(name = "redirectionURL")
	private String redirectionURL;
	
	
	
	public String getRedirectionURL()
	{
		return redirectionURL;
	}
	public void setRedirectionURL(String redirectionURL)
	{
		this.redirectionURL = redirectionURL;
	}
	public String getPackageId()
	{
		return packageId;
	}
	public void setPackageId(String packageId)
	{
		this.packageId = packageId;
	}
	public String getPackageType()
	{
		return packageType;
	}
	public void setPackageType(String packageType)
	{
		this.packageType = packageType;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCorpMasterAcctNo() {
		return corpMasterAcctNo;
	}
	public void setCorpMasterAcctNo(String corpMasterAcctNo) {
		this.corpMasterAcctNo = corpMasterAcctNo;
	}
	public String getCorpAPNumber() {
		return corpAPNumber;
	}
	public void setCorpAPNumber(String corpAPNumber) {
		this.corpAPNumber = corpAPNumber;
	}
	public Boolean isCorpSurveyCompleted() {
		return isCorpSurveyCompleted;
	}
	public void setCorpSurveyCompleted(Boolean isCorpSurveyCompleted) {
		this.isCorpSurveyCompleted = isCorpSurveyCompleted;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getCustSegment() {
		return custSegment;
	}
	public void setCustSegment(String custSegment) {
		this.custSegment = custSegment;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getSecurityQuestion() {
		return securityQuestion;
	}
	public void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}
	public String getSecurityAnswer() {
		return securityAnswer;
	}
	public void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPreferedLanguage() {
		return preferedLanguage;
	}
	public void setPreferedLanguage(String preferedLanguage) {
		this.preferedLanguage = preferedLanguage;
	}
	public Long getUserAcctId() {
		return userAcctId;
	}
	public void setUserAcctId(Long userAcctId) {
		this.userAcctId = userAcctId;
	}
/*	public String getIqama() {
		return iqama;
	}
	public void setIqama(String iqama) {
		this.iqama = iqama;
	}*/
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public List<UserSubscription> getUserSubscriptions() {
		return userSubscriptions;
	}
	public void setUserSubscriptions(List<UserSubscription> userSubscriptions) {
		this.userSubscriptions = userSubscriptions;
		if(userSubscriptions!=null)
			for(int i=0;i<userSubscriptions.size();i++)
	 			 if((userSubscriptions.get(i).isDefaultSubscription())){
		           this.defaultSubscriptionIndex=i;
		           break;
	 			 }
	}
	public Boolean getIsCorpSurveyCompleted()
	{
		return isCorpSurveyCompleted;
	}
	public void setIsCorpSurveyCompleted(Boolean isCorpSurveyCompleted)
	{
		this.isCorpSurveyCompleted = isCorpSurveyCompleted;
	}
	public int getDefaultSubscriptionIndex()
	{
		return defaultSubscriptionIndex;
	}
	public void setDefaultSubscriptionIndex(int defaultSubscriptionIndex)
	{
		this.defaultSubscriptionIndex = defaultSubscriptionIndex;
	}
	public String getActivationURL()
	{
		return activationURL;
	}
	public void setActivationURL(String activationURL)
	{
		this.activationURL = activationURL;
	}
	public String getActivationCode()
	{
		return activationCode;
	}
	public void setActivationCode(String activationCode)
	{
		this.activationCode = activationCode;
	}
	public String getFacebookId()
	{
		return facebookId;
	}
	public void setFacebookId(String facebookId)
	{
		this.facebookId = facebookId;
	}
	public Date getBirthDate()
	{
		return birthDate;
	}
	public Timestamp getBirthDateAsTimestamp()
	{
		if(birthDate == null)
			return null;
		return new Timestamp(birthDate.getTime());
	}
	public void setBirthDate(Date birthDate)
	{
		this.birthDate = birthDate;
	}
	public Date getRegistrationDate()
	{
		return registrationDate;
	}
	public Timestamp getRegistrationDateAsTimestamp()
	{
		if(registrationDate == null)
			return null;
		return new Timestamp(registrationDate.getTime());
	}
	public void setRegistrationDate(Date registrationDate)
	{
		this.registrationDate = registrationDate;
	}
	public Date getActivationDate()
	{
		return activationDate;
	}
	public Timestamp getActivationDateAsTimestamp()
	{
		if(activationDate == null)
			return null;
		return new Timestamp(activationDate.getTime());
	}
	public void setActivationDate(Date activationDate)
	{
		this.activationDate = activationDate;
	}
	public Date getCreatedDate()
	{
		return createdDate;
	}
	public Timestamp getCreatedDateAsTimestamp()
	{
		if(createdDate == null)
			return null;
		return new Timestamp(createdDate.getTime());
	}
	public void setCreatedDate(Date createdDate)
	{
		this.createdDate = createdDate;
	}
	public Date getLastLoginTime()
	{
		return lastLoginTime;
	}
	public Timestamp getLastLoginTimeAsTimestamp()
	{
		if(lastLoginTime == null)
			return null;
		return new Timestamp(lastLoginTime.getTime());
	}
	public void setLastLoginTime(Date lastLoginTime)
	{
		this.lastLoginTime = lastLoginTime;
	}
	public Integer getRegistrationSourceId()
	{
		return registrationSourceId;
	}
	public void setRegistrationSourceId(Integer registrationSourceId)
	{
		this.registrationSourceId = registrationSourceId;
	}
	public CorporateSubscriptionVO getCorporateSubscriptionVO()
	{
		return corporateSubscriptionVO;
	}
	public void setCorporateSubscriptionVO(CorporateSubscriptionVO corporateSubscriptionVO)
	{
		this.corporateSubscriptionVO = corporateSubscriptionVO;
	}
	public String getContactNumber()
	{
		return contactNumber;
	}
	public void setContactNumber(String contactNumber)
	{
		this.contactNumber = contactNumber;
	}
	public String getTwitterId()
	{
		return twitterId;
	}
	public void setTwitterId(String twitterId)
	{
		this.twitterId = twitterId;
	}
	
	@Override
	public String toString() {
		return 	"firstName=["+firstName+"], lastName=["+lastName+"],email=["+email+"], corpMasterAcctNo=["+corpMasterAcctNo+"], corpAPNumber=["+corpAPNumber+"], " +
				"isCorpSurveyCompleted=["+isCorpSurveyCompleted+"], givenName=["+givenName+"], custSegment=["+custSegment+"], roleId=["+roleId+"], securityQuestion=["+securityQuestion+"], " +
				"birthDate =["+birthDate+"], nationality=["+nationality+"], status=["+status+"], gender=["+gender+"], registrationDate=["+registrationDate+"], preferedLanguage=["+preferedLanguage+"]," +
				" userAcctId =["+userAcctId+"], activationDate=["+activationDate+"], createdDate=["+createdDate+"], lastLoginTime=["+lastLoginTime+"], userName=["+userName+"], " +
				"userSubscriptions =["+userSubscriptions+"], defaultSubscriptionIndex=["+defaultSubscriptionIndex+"], activationURL=["+activationURL+"], activationCode=["+activationCode+"], facebookId=["+facebookId+"], twitterId=["+twitterId+"]," +
				" registrationSourceId =["+registrationSourceId+"], corporateSubscriptionVO =["+corporateSubscriptionVO+"], contactNumber=["+contactNumber+"], packageId=["+packageId+"], packageType=["+packageType+"], redirectionURL =["+redirectionURL+"]";
	}

}
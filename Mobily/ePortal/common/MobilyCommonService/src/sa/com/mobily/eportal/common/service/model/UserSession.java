package sa.com.mobily.eportal.common.service.model;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class UserSession extends BaseVO {
	
	private static final long serialVersionUID = 1L;

	private UserSessionPK id;

	private UserSubscription userSubscription;

    public UserSession() {
    }

	public UserSessionPK getId() {
		return this.id;
	}

	public void setId(UserSessionPK id) {
		this.id = id;
	}
	
	public UserSubscription getUserSubscription() {
		return this.userSubscription;
	}

	public void setUserSubscription(UserSubscription userSubscription) {
		this.userSubscription = userSubscription;
	}
	
}
package sa.com.mobily.eportal.common.service.mq;

import java.util.List;

import javax.enterprise.inject.Alternative;
import javax.jms.JMSException;
import javax.naming.NamingException;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.jms.nativejms.MessageRequester;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.MQHandler;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

import com.mobily.exception.mq.MQSendException;

@Alternative
public class NativeJMS implements MobilyJMS
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.MQ_CONNECTION_HANDLER_SERVICE_LOGGER_NAME);

	private static NativeJMS me;

	private NativeJMS()
	{
	}

	public static NativeJMS getInstance()
	{
		if (me == null)
			me = new NativeJMS();
		return me;
	}

	/**
	 * This Method is responsible for Sending Message to Certain Queue Name and
	 * not waiting for the reply on specific Queue Name in QueueManager Name
	 * 
	 * @param msg
	 * @param queueName
	 * @param replyQueueName
	 * @param replyToQueueManagerName
	 * @return
	 * @throws MQSendException
	 */
	public void sendMessageOnly(String msg, String queueName, String replyQueueName, String replyToQueueManagerName)
	{
		MessageRequester requester = null;
		try
		{
			if (MQHandler.getInstance().getDeploymentType().equals(MQHandler.DEPLOYMENT_TYPE_TESTING))
				return;
			MQAuditVO mqAuditVO = new MQAuditVO();
			mqAuditVO.setMessage(msg);
			mqAuditVO.setRequestQueue(queueName);
			mqAuditVO.setReplyQueue(replyQueueName);

			requester = MessageRequester.newRequestor(mqAuditVO);
			requester.send(mqAuditVO);
		}
		catch (Exception e)
		{
			executionContext.severe("Send to MQ with Reply Generate The following Exception: " + e);
			throw new RuntimeException(e);
		}
		finally
		{
			if (requester != null)
			{
				requester.cleanup();
			}
		}
	}

	public String sendToMQWithReply(String msg, String queueName, String replyQueueName)
	{
		String replyMessage = "";
		executionContext.fine("Start Send to MQWithReply ");
		MessageRequester requester = null;
		try
		{
			if (MQHandler.getInstance().getDeploymentType().equals(MQHandler.DEPLOYMENT_TYPE_TESTING)
					|| MQHandler.getInstance().getDeploymentType().equals(MQHandler.DEPLOYMENT_TYPE_PER_REQUEST))
			{
				List<String> reply = MQHandler.getInstance().getQueues().get(queueName);
				if (reply != null && reply.size() > 0)
					return reply.get((int) (Math.random() * reply.size()));
			}
			MQAuditVO mqAuditVO = new MQAuditVO();
			mqAuditVO.setRequestQueue(queueName);
			mqAuditVO.setReplyQueue(replyQueueName);
			mqAuditVO.setMessage(msg);
			requester = MessageRequester.newRequestor(mqAuditVO);
			replyMessage = requester.sendWithReply(mqAuditVO);
		}
		catch (Exception e)
		{
			executionContext.severe("Send to MQ with Reply Generate The following Exception: " + e);
			throw new RuntimeException(e);
		}
		finally
		{
			if (requester != null)
			{
				requester.cleanup();
			}
		}
		return replyMessage;
	}

	// modified by v.ravipati.mit for SR_MQ_Audit_tbl
	public String sendToMQWithReply(MQAuditVO mqAuditVO)
	{
		String replyMessage = "";
		executionContext.fine("Start Send to MQWithReply ");
		MessageRequester requester = null;
		try
		{
			if (MQHandler.getInstance().getDeploymentType().equals(MQHandler.DEPLOYMENT_TYPE_TESTING)
					|| MQHandler.getInstance().getDeploymentType().equals(MQHandler.DEPLOYMENT_TYPE_PER_REQUEST))
			{
				List<String> reply = MQHandler.getInstance().getQueues().get(mqAuditVO.getRequestQueue());
				if (reply != null && reply.size() > 0)
					return reply.get((int) (Math.random() * reply.size()));
			}
			requester = MessageRequester.newRequestor(mqAuditVO);
			replyMessage =requester.sendWithReplyWithNoAudit(mqAuditVO);
		}
		catch (Exception e)
		{
			executionContext.severe("Send to MQ with Reply Generate The following Exception: " + e);
			throw new RuntimeException(e);
		}
		finally
		{
			if (requester != null)
			{
				requester.cleanup();
			}
		}
		return replyMessage;
	}

	/**
	 * this method is responsible for Sending Message to certain queue without
	 * waiting any reply
	 * 
	 * @param msg
	 * @param queuName
	 * @throws MQSendException
	 * @throws ServiceLocatorException
	 * @throws NamingException
	 * @throws JMSException
	 */
	@Override
	public void sendToMQ(String msg, String queuName)
	{
		MessageRequester requester = null;
		try
		{
			if (MQHandler.getInstance().getDeploymentType().equals(MQHandler.DEPLOYMENT_TYPE_TESTING))
				return;
			MQAuditVO mqAuditVO = new MQAuditVO();
			mqAuditVO.setMessage(msg);
			mqAuditVO.setRequestQueue(queuName);
			requester = MessageRequester.newRequestor(mqAuditVO);

			requester.setTimeToLiveFlag(false);
			requester.send(mqAuditVO);
		}
		catch (Exception e)
		{
			executionContext.severe("Send to MQ Generate The following Exception: " + e);
			throw new RuntimeException(e);
		}
		finally
		{
			if (requester != null)
			{
				requester.cleanup();
			}
		}
	}
	
	@Override
	public void sendToMQWithNoAudit(String msg, String queuName)
	{
		MessageRequester requester = null;
		try
		{
			if (MQHandler.getInstance().getDeploymentType().equals(MQHandler.DEPLOYMENT_TYPE_TESTING))
				return;
			MQAuditVO mqAuditVO = new MQAuditVO();
			mqAuditVO.setMessage(msg);
			mqAuditVO.setRequestQueue(queuName);
			requester = MessageRequester.newRequestor(mqAuditVO);

			requester.setTimeToLiveFlag(false);
			requester.sendwithNoAudit(mqAuditVO);
		}
		catch (Exception e)
		{
			executionContext.severe("Send to MQ Generate The following Exception: " + e);
			throw new RuntimeException(e);
		}
		finally
		{
			if (requester != null)
			{
				requester.cleanup();
			}
		}
	}	
	
	//added by v.ravipati.mit
	//This method is responsible to send the request to back end and Audit the same, so no need of MQ Audit from caller side.
	@Override
	public void sendToMQ(MQAuditVO mqAuditVO )
	{
		MessageRequester requester = null;
		try
		{
			if (MQHandler.getInstance().getDeploymentType().equals(MQHandler.DEPLOYMENT_TYPE_TESTING))
				return;
			
			requester = MessageRequester.newRequestor(mqAuditVO);
			requester.setTimeToLiveFlag(false);
			requester.send(mqAuditVO);
		}
		catch (Exception e)
		{
			executionContext.severe("Send to MQ Generate The following Exception: " + e);
			throw new RuntimeException(e);
		}
		finally
		{
			if (requester != null)
			{
				requester.cleanup();
			}
		}
	}
	

	@Override
	public void respondWithCorrelationID(String msg, String queueName, byte[] correlationID)
	{
		executionContext.fine("Start Send to MQWithReply ");
		MessageRequester requester = null;
		try
		{
			if (MQHandler.getInstance().getDeploymentType().equals(MQHandler.DEPLOYMENT_TYPE_TESTING))
				return;
			MQAuditVO mqAuditVO = new MQAuditVO();
			mqAuditVO.setRequestQueue(queueName);
			mqAuditVO.setMessage(msg);
			mqAuditVO.setCorrelationID(correlationID);
			mqAuditVO.setSendingReply(true);
			requester = MessageRequester.newRequestor(mqAuditVO);
			requester.respondWithCorrelationID(msg, queueName, correlationID);
		}
		catch (Exception e)
		{
			executionContext.severe("Send to MQ with Reply Generate The following Exception: " + e);
			throw new RuntimeException(e);
		}
		finally
		{
			if (requester != null)
			{
				requester.cleanup();
			}
		}
	}
	//added by v.ravipati.mit
	@Override
	public void respondWithCorrelationID(MQAuditVO mqAuditVO)
	{
		executionContext.fine("Start Send to MQWithReply ");
		MessageRequester requester = null;
		try
		{
			if (MQHandler.getInstance().getDeploymentType().equals(MQHandler.DEPLOYMENT_TYPE_TESTING))
				return;
			mqAuditVO.setSendingReply(true);
			requester = MessageRequester.newRequestor(mqAuditVO);
			requester.respondWithCorrelationID(mqAuditVO.getMessage(), mqAuditVO.getRequestQueue(), mqAuditVO.getCorrelationID());
		}
		catch (Exception e)
		{
			executionContext.severe("Send to MQ with Reply Generate The following Exception: " + e);
			throw new RuntimeException(e);
		}
		finally
		{
			if (requester != null)
			{
				requester.cleanup();
			}
		}
	}
}
package sa.com.mobily.eportal.common.service.mq.vo;

import java.util.List;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class AuditingVO extends BaseVO {

	private static final long serialVersionUID = 1L;
	private String tableName;
	private List<AuditingAttributeVO> auditingAttributeVOList = null;
	
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public List<AuditingAttributeVO> getAuditingAttributeVOList()
	{
		return auditingAttributeVOList;
	}
	public void setAuditingAttributeVOList(List<AuditingAttributeVO> auditingAttributeVOList)
	{
		this.auditingAttributeVOList = auditingAttributeVOList;
	}
}

package sa.com.mobily.eportal.common.service.sms;

import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;

import org.apache.commons.lang3.StringUtils;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.service.pincode.activation.dao.PinCodeActivationDAO;
import sa.com.mobily.eportal.common.service.pincode.activation.model.PinCodeActivation;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.RandomGenerator;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.valueobject.common.PinCodeVO;

/**
 * 
 * @author Muhammad Irfan Masood
 * 
 */

public class PinCodeSender
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	private static final String className = PinCodeSender.class.getName();

	private static final String PIN_CODE_SENT_FAILED = "222001";

	private static final String PIN_CODE_VERIFY_FAILED = "222002";
	
	private static final String PIN_CODE_DELETE_FAILED = "222003";
	
	

	public static final String AR = "ar";
	public static final int PROJECT_ID=73;
	public static final String MESSAGEID_EN="824861";
	public static final String MESSAGEID_AR="824862";
	public static final int PINCODE_LENGTH=4;
	public static String sendPINCode(PinCodeVO pinCodeVO)
	{
		String method = "sendPINCode";
		String pinCode = null;
		try
		{
			String messageId = pinCodeVO.getMessageIdEn();
			if (StringUtils.isNotEmpty(pinCodeVO.getLanguage()))
			{
				if (pinCodeVO.getLanguage().equalsIgnoreCase(AR))
				{
					messageId = pinCodeVO.getMessageIdAr();
				}
			}
			//Commented the below code for Security Vulnerablity for Credit Transfer - by Ravi on 23rd Apr 2019
			//PinCodeActivation pinCodeActivation = PinCodeActivationDAO.getInstance().getActivationCode(pinCodeVO.getMsisdn(), "" + pinCodeVO.getServiceID());
			PinCodeActivation pinCodeActivation = null;
			if (pinCodeActivation == null)
			{
				
				pinCode = RandomGenerator.generateNumericRandom(pinCodeVO.getPinLength());
				//pinCode = RandomGenerator.generateNumericRandom();
				pinCodeActivation = new PinCodeActivation();
				pinCodeActivation.setProjectId("" + pinCodeVO.getServiceID());
				pinCodeActivation.setActivationCode(pinCode);
				pinCodeActivation.setMsisdn(pinCodeVO.getMsisdn());
				pinCodeActivation.setRequestDate(new Timestamp(new Date().getTime()));
				//Delete the old code before inserting a new code
				PinCodeActivationDAO.getInstance().deleteActivationRequest(pinCodeVO.getMsisdn(), ""+pinCodeVO.getServiceID());
				PinCodeActivationDAO.getInstance().addActivationRequest(pinCodeActivation);
			}
			else
			{
				pinCode = pinCodeActivation.getActivationCode();
			}
			String[] parametersArray = new String[1];
			parametersArray[0] = pinCode;
			SMSSender.sendSMS(pinCodeVO.getMsisdn(), messageId, parametersArray);
			executionContext.audit(Level.INFO, "SMS Code sent for MSISDN : " + pinCodeVO.getMsisdn() + " is : " + pinCode, className, method);
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
			throw new BackEndException(pinCodeVO.getServiceID(), PIN_CODE_SENT_FAILED);
		}
		return pinCode;
	}

	public static boolean verifyPINCode(PinCodeVO pinCodeVO)
	{
		String method = "verifyPINCode";
		boolean pinCodeFound = false;
		try
		{
			PinCodeActivation pinCodeActivation = PinCodeActivationDAO.getInstance().getActivationCode(pinCodeVO.getMsisdn(), "" + pinCodeVO.getServiceID());
			if (pinCodeActivation == null)
			{
				pinCodeFound = false;
			}
			else
			{
				if (StringUtils.isNotEmpty(pinCodeVO.getExistingPINCode()))
				{
					if (pinCodeVO.getExistingPINCode().equalsIgnoreCase(pinCodeActivation.getActivationCode()))
					{
						pinCodeFound = true;
						executionContext.audit(Level.INFO, "PIN Code Verified MSISDN : " + pinCodeVO.getMsisdn() + " PIN Code : " + pinCodeVO.getExistingPINCode(), className,method);
						if(pinCodeVO.getServiceID()==PROJECT_ID){
							executionContext.audit(Level.INFO, "Deleting PIN code from DB for MSISDN : " + pinCodeVO.getMsisdn() + " Project Id : " + pinCodeVO.getServiceID(), className,method);
							deletePINCode(pinCodeVO);
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
			throw new BackEndException(pinCodeVO.getServiceID(), PIN_CODE_VERIFY_FAILED);
		}
		return pinCodeFound;
	}

	public static void deletePINCode(PinCodeVO pinCodeVO)
	{
		String method = "deletePINCode";
		if(null == pinCodeVO)
		{
			executionContext.audit(Level.SEVERE, "PIN Code Object Can't be NULL  ", className,method);
			return;
		}
		
		if(null == pinCodeVO.getMsisdn())
		{
			executionContext.audit(Level.SEVERE, "PIN Code Verified MSISDN Can't be NULL  ", className,method);
			return;
		}
		
		try
		{
			int result = PinCodeActivationDAO.getInstance().deleteActivationRequest(pinCodeVO.getMsisdn(), String.valueOf(pinCodeVO.getServiceID()));
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
			throw new BackEndException(pinCodeVO.getServiceID(), PIN_CODE_DELETE_FAILED);			
		}
	}
	
	public static String sendOTP(PinCodeVO pinCodeVO)
	{
		String method = "sendOTP";
		executionContext.log(Level.INFO, "PinCodeSender > sendOTP > start for MSISDN:"+pinCodeVO.getMsisdn(), className, method);
		String pinCode = null;
		try
		{
			/*String messageId = pinCodeVO.getMessageIdEn();
			if (StringUtils.isNotEmpty(pinCodeVO.getLanguage()))
			{
				if (pinCodeVO.getLanguage().equalsIgnoreCase(AR))
				{
					messageId = pinCodeVO.getMessageIdAr();
				}
			}*/
			if(pinCodeVO.getServiceID()==0){
				pinCodeVO.setServiceID(PROJECT_ID);
			}
			PinCodeActivation pinCodeActivation = PinCodeActivationDAO.getInstance().getActivationCode(pinCodeVO.getMsisdn(), "" + pinCodeVO.getServiceID());
			if (pinCodeActivation == null)
			{
				pinCode = RandomGenerator.generateNumericRandom(PINCODE_LENGTH);
				pinCodeActivation = new PinCodeActivation();
				pinCodeActivation.setProjectId("" + pinCodeVO.getServiceID());
				pinCodeActivation.setActivationCode(pinCode);
				pinCodeActivation.setMsisdn(pinCodeVO.getMsisdn());
				pinCodeActivation.setRequestDate(new Timestamp(new Date().getTime()));
				PinCodeActivationDAO.getInstance().addActivationRequest(pinCodeActivation);
			}
			else
			{
				pinCode = pinCodeActivation.getActivationCode();
			}
			String[] parametersArray = new String[1];
			parametersArray[0] = pinCode;
			executionContext.audit(Level.INFO, "sendOTP > SMS Code sending for MSISDN in English: " + pinCodeVO.getMsisdn() + " is : " + pinCode, className, method);
			SMSSender.sendSMS(pinCodeVO.getMsisdn(), MESSAGEID_EN, parametersArray);
			executionContext.audit(Level.INFO, "sendOTP > SMS Code sending for MSISDN in Arabic: " + pinCodeVO.getMsisdn() + " is : " + pinCode, className, method);
			SMSSender.sendSMS(pinCodeVO.getMsisdn(), MESSAGEID_AR, parametersArray);
			executionContext.audit(Level.INFO, "sendOTP > SMS Code sent for MSISDN : " + pinCodeVO.getMsisdn() + " is : " + pinCode, className, method);
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "sendOTP > Exception:"+e.getMessage(), className, method, e);
			throw new BackEndException(pinCodeVO.getServiceID(), PIN_CODE_SENT_FAILED);
		}
		return pinCode;
	}

	/**
	 * @return the projectId
	 */
	public static int getProjectId()
	{
		return PROJECT_ID;
	}

	/**
	 * @return the messageidEn
	 */
	public static String getMessageidEn()
	{
		return MESSAGEID_EN;
	}

	/**
	 * @return the messageidAr
	 */
	public static String getMessageidAr()
	{
		return MESSAGEID_AR;
	}

	/**
	 * @return the pincodeLength
	 */
	public static int getPincodeLength()
	{
		return PINCODE_LENGTH;
	}

	
}
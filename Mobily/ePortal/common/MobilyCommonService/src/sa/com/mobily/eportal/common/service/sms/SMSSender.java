/*
 * Created on Feb 19, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.sms;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.business.SMSBO;
import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.sms.vo.SMSVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;

/**
 * @author ysawy
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SMSSender implements ConstantIfc
{
    private static final Logger log = LoggerInterface.log;
    private static SMSBO smsBO = new SMSBO();
    
    public static void sendSMS( String   aMsisdn,
            					String   aId,
            					String[] aParameters) throws SystemException
    {
        String tempRequest = null;
        
        //	Construct request
        tempRequest = aMsisdn + ";" + aId;
        if( aParameters != null && aParameters.length > 0 ) {
        	String dele = ";";
            for( int i=0; i<aParameters.length; i++ ) {
                tempRequest += dele + aParameters[i];
                dele = ",";
            }
        }
        

        smsBO.sendSMS( tempRequest );
    }
    

    public static void sendSMS( String   aMsisdn,String   aId,String[] aParameters,String senderId) throws SystemException{
    	StringBuffer  tempRequest = new StringBuffer();
		
		tempRequest.append(aMsisdn+";" + aId);
		if( aParameters != null && aParameters.length > 0 ) {
			String dele = ";";
			for( int i=0; i<aParameters.length; i++ ) {
				tempRequest.append( dele);
				tempRequest.append( aParameters[i]);
				dele = ",";
			}
			dele = ";";
			if(FormatterUtility.isNotEmpty(senderId)){
				tempRequest.append( dele);
				tempRequest.append( senderId);
			}
		}
		
		smsBO.sendSMS( tempRequest.toString() );
    }
    
    public static void sendSMS(SMSVO smsVO ){
    	
    		//String   aMsisdn,String   aId,String[] aParameters,String senderId) throws SystemException{
    		StringBuffer  tempRequest = new StringBuffer();
		
		tempRequest.append(smsVO.getMsisdn()+";" + smsVO.getSmsIndexId());
		String[] aParameters = smsVO.getParameterArray();
		if( aParameters != null && aParameters.length > 0 ) {
			String dele = ";";
			for( int i=0; i<aParameters.length; i++ ) {
				tempRequest.append( dele);
				tempRequest.append( aParameters[i]);
				dele = ",";
			}
			
			if(FormatterUtility.isNotEmpty(smsVO.getSenderId())){
				tempRequest.append( ";");
				tempRequest.append( smsVO.getSenderId());
				
				if(FormatterUtility.isNotEmpty(smsVO.getAuditRequired())){
					tempRequest.append( ";Y");
					if(FormatterUtility.isNotEmpty(smsVO.getUniqueId())){
						tempRequest.append( ";"+smsVO.getUniqueId());
					}
				}
				
			}
		}
		smsVO.setSmsRequest(tempRequest.toString());
		smsBO.sendSMS( smsVO );
    }

    
    
}
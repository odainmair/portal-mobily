/**
 * SMSVO.java 
 * v.ravipati.mit
 * 26-Nov-2015 - 10:52:07 PM
 */
package sa.com.mobily.eportal.common.service.sms.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @version : 1.0
 * @author  : Venkat Rao Ravipati - MIT
 * @date    : 26-Nov-2015
 * @time    : 10:52:07 PM
 * @extends :
 * @implements :
 */
public class SMSVO extends BaseVO{
	private static final long serialVersionUID = 1L;
	private String msisdn;
	private String smsIndexId;
	private String[] parameterArray;
	private String senderId;
	private String queueName;
	private String auditRequired;
	private String uniqueId;
	private String smsRequest;
	
	public String getMsisdn()
	{
		return msisdn;
	}
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	public String getSmsIndexId()
	{
		return smsIndexId;
	}
	public void setSmsIndexId(String smsIndexId)
	{
		this.smsIndexId = smsIndexId;
	}
	public String[] getParameterArray()
	{
		return parameterArray;
	}
	public void setParameterArray(String[] parameterArray)
	{
		this.parameterArray = parameterArray;
	}
	public String getSenderId()
	{
		return senderId;
	}
	public void setSenderId(String senderId)
	{
		this.senderId = senderId;
	}
	public String getQueueName()
	{
		return queueName;
	}
	public void setQueueName(String queueName)
	{
		this.queueName = queueName;
	}
	public String getAuditRequired()
	{
		return auditRequired;
	}
	public void setAuditRequired(String auditRequired)
	{
		this.auditRequired = auditRequired;
	}
	public String getUniqueId()
	{
		return uniqueId;
	}
	public void setUniqueId(String uniqueId)
	{
		this.uniqueId = uniqueId;
	}
	public String getSmsRequest()
	{
		return smsRequest;
	}
	public void setSmsRequest(String smsRequest)
	{
		this.smsRequest = smsRequest;
	}
	
	
	
}

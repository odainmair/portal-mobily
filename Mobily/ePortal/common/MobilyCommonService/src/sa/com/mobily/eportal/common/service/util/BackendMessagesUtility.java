package sa.com.mobily.eportal.common.service.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.ePortalDBDaoIfc;
import sa.com.mobily.eportal.common.service.dao.imp.ePortalDBDao;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.valueobject.common.BackEndMessagesVO;



/**
 * @author aghareeb
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class BackendMessagesUtility { 
	
	private static final Logger log = LoggerInterface.log;
	private static BackendMessagesUtility myInstance;
	
	private static HashMap messagesMap = new HashMap();	//initialization to avoid null pointer exception
	
	private static int lookupByIdArray[]={1,2,33,9};
	
	public static final int PAYMENT_ALLOCATION_ID = 1;
	public static final int PAYMENT_MODE_ID       = 2;
	public static final int PAYMENT_STATUS_ID     = 33;
	public static final int BILL_MODE_ID          = 9;

	private static String NOT_FOUND_ITEM="";
	private static Date lastRefreshTime ;
	
	
	private BackendMessagesUtility(){
		// load the data once in first request .lazy loading
		try{
			messagesMap = ePortalDBDao.getBackEndMessages(lookupByIdArray);	

	       lastRefreshTime = new Date();
		}catch(Exception e) {
			//failed to load the back end messages table
			myInstance=null;
			log.fatal("BackendMessagesUtility > Exception in the constructor >"+e.getMessage());
		}
	}
	
	 public static BackendMessagesUtility getInstance() {  // it may be synchronized method
        if (myInstance == null)
            myInstance = new BackendMessagesUtility();

        return myInstance;
    }
	 public String getMessage(int id,String key,Locale locale){
	 	try{
	 	if (myInstance==null) return  NOT_FOUND_ITEM;
	 	if (FormatterUtility.isEmpty(key)) return NOT_FOUND_ITEM; 
	 	  String returnMessage=null;
	 	  Object item=messagesMap.get(new Integer(id));
	 	  if(item==null)
	 	  	 return NOT_FOUND_ITEM;
	 	    Object value= ((HashMap)item).get(key);
	 	  if(value==null) return NOT_FOUND_ITEM;
	 	  	BackEndMessagesVO backEndMessagesVO=(BackEndMessagesVO)value;
	 	  	if(locale==null||locale.getLanguage().equals("en"))
	 	  		returnMessage=backEndMessagesVO.getMessageEn();
	 	  	
	 	  	else if (locale.getLanguage().equals("ar"))
	 	  		if(!FormatterUtility.isEmpty(backEndMessagesVO.getMessageAr())) //the default locale is en
	 	  			returnMessage=backEndMessagesVO.getMessageAr();
	 	  if(FormatterUtility.isEmpty(returnMessage)){
	 	  	returnMessage= NOT_FOUND_ITEM;
	 	  	// log the error as fetal .
	 	  }
	 	  	
	 	  return returnMessage;
	 	}catch (Exception e){
	 		log.fatal("BackendMessagesUtility > getMessage > Exception >"+e.getMessage());
	 		
	 	}
	 	return NOT_FOUND_ITEM;
	 }


}

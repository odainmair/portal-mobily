
package sa.com.mobily.eportal.common.service.util;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.valueobject.common.ConsumerRequestHeaderVO;

/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CustomerServiceUtil {
	private static final Logger logger = Logger.getLogger("lib-customerInfoService");
    
	
	public ConsumerRequestHeaderVO populateRequestHeaderVO(String function,String userId) {
		ConsumerRequestHeaderVO headerVO = new ConsumerRequestHeaderVO();
		try {
			
			headerVO.setMsgFormat(function);
			//headerVO.setMsgVersion(ConstantIfc.MSG_VERSION);
			headerVO.setMsgVersion(ConstantIfc.MSG_VERSION1);
			
			headerVO.setRequestorChannelId(ConstantIfc.REQUESTOR_CHANNEL_ID);
			headerVO.setRequestorChannelFunction(function);
			headerVO.setRequestorUserId(userId);
			headerVO.setRequestorLanguage(ConstantIfc.LANG_VALUE);
			headerVO.setRequestorSecurityInfo(ConstantIfc.SECURITY_INFO_VALUE);
			headerVO.setChannelTransId(ConstantIfc.Requestor_ChannelId + "_" + new SimpleDateFormat(ConstantIfc.DATE_FORMAT).format(new Date()));
			
		} catch (Exception e) {
			logger.logp(Level.SEVERE, "CustomerServiceUtil", "populateRequestHeaderVO",
					"Exception while populating request header vo",
					e);
		}
		return headerVO;
	}
}
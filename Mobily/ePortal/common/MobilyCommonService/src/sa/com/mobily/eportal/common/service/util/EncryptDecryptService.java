package sa.com.mobily.eportal.common.service.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.mobily.bsl.encryption.exception.EncryptionFailureException;
import com.mobily.bsl.encryption.string.encryptor.StringEncryptionImplementationFactory;
import com.mobily.bsl.encryption.string.encryptor.StringEncryptionInterface;


/**
 * @author
 *
 */
public class EncryptDecryptService {

	private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;
	
	  public EncryptDecryptService()
	    {
	    }

	    /**
	     * this method is responsible for encrypt the text 
	     * @param input
	     * @return
	     */
	    public static synchronized String encrypt(String input)
	    {
	        String output = null;
	        StringEncryptionInterface interfa = StringEncryptionImplementationFactory.getDefaultImplementation();
	        try
	        {
	            output = interfa.encrypt(input, getKeys());
	        }
	        catch(EncryptionFailureException e)
	        {
	            output = input;
	            log.fatal("EncryptDecryptService >encrypt >  failed > EncryptionFailureException > "+e.getMessage());
	        }
	        return output;
	    }

	    /**
	     * this method is responsible for decryptingString the text 
	     * @param input
	     * @return
	     */
	    public static synchronized String decryptingString(String input)
	    {
	        byte keys[] = getKeys();
	        String de = null;
	        StringEncryptionInterface interfa = StringEncryptionImplementationFactory.getDefaultImplementation();
	        try
	        {
	            de = interfa.decrypt(input, keys);
	        }
	        catch(EncryptionFailureException e)
	        {
	            de = input;
	            log.fatal("EncryptDecryptService >decryptingString >  failed > EncryptionFailureException > "+e.getMessage());
	        }
	        return de;
	    }

	    public static byte[] getKeys()
	    {
	        byte keyByteA[] = (byte[])null;
	        try
	        {
	            keyByteA = get_key(MobilyUtility.getPropertyValue("KEY_FILE_PATH"));
	        }
	        catch(Exception e)
	        {
	        	log.fatal("EncryptDecryptService >getKeys >  failed > Exception > "+e.getMessage());
	        }
	        return keyByteA;
	    }

	    public static byte[] get_key(String fileName)
	        throws FileNotFoundException, IOException
	    {
	        byte got_keys[] = new byte[24];
	        FileInputStream inFile = new FileInputStream(fileName);
	        int m = 0;
	        m = inFile.read(got_keys);
	        inFile.close();
	        return got_keys;
	    }

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String enc = "966561110152";
		String enct = EncryptDecryptService.encrypt(enc);
		String dec = EncryptDecryptService.decryptingString(enct);
		
//		System.out.println(enct);
//		System.out.println("\n\n");
//		System.out.println(dec);
		

	}

}

package sa.com.mobily.eportal.common.service.util;

import java.util.Date;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.FootPrintTrackingDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.FootPrintVO;

/**
 * 
 * @author n.gundluru.mit
 * 
 */

public class FootPrintUtility {

	private static final Logger log = LoggerInterface.log;

	/**
	 * Inserts footprint details
	 * 
	 */
	public static void print(String eventId, String userId, String msisdn,
			String subkey, String comments, String systemError,
			String userError, String errorCode, int reqChnlId,
			String serviceAccount) {
		log.info("FootPrintUtility > Print > EventId[" + eventId
				+ "] , UserId[" + userId + "] , MSISDN[" + msisdn
				+ "], ErrorMessage[" + userError + "]");

		FootPrintVO footPrintVO = new FootPrintVO();

		footPrintVO.setUserId(userId);
		footPrintVO.setKey(msisdn);
		footPrintVO.setSubKey(subkey);
		footPrintVO.setComments(comments);
		footPrintVO.setSystemErrorMessage(systemError);
		footPrintVO.setRequestTimeStamp(new Date());
		footPrintVO.setUserErrorMessage(userError);
		footPrintVO.setErrorCode(errorCode);
		footPrintVO.setEventName(eventId);
		footPrintVO.setRequesterChannelId(reqChnlId);
		log.debug("Service Account :"+serviceAccount);
		if (serviceAccount != null && !"".equals(serviceAccount)
				&& !serviceAccount.equalsIgnoreCase("null")) {
			footPrintVO.setServiceAccount(serviceAccount);
		} else {
			footPrintVO.setServiceAccount(null);
		}
		try {
			OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory
					.getDAOFactory(DAOFactory.ORACLE);
			FootPrintTrackingDAO fptDao = daoFactory
					.getOracleFootPrintTrackingDAO();

			fptDao.addRow(footPrintVO);

		} catch (Exception e) {
			log.fatal("FootPrintUtility > print > Exception > "
					+ e.getMessage());
			throw new SystemException(e);
		}
	}
	
	public static void print(String eventId, String userId, String msisdn,
			String subkey, String comments, String systemError,
			String userError, String errorCode, int reqChnlId) {
		log.info("FootPrintUtility > Print > EventId[" + eventId
				+ "] , UserId[" + userId + "] , MSISDN[" + msisdn
				+ "], ErrorMessage[" + userError + "]");

		FootPrintVO footPrintVO = new FootPrintVO();

		footPrintVO.setUserId(userId);
		footPrintVO.setKey(msisdn);
		footPrintVO.setSubKey(subkey);
		footPrintVO.setComments(comments);
		footPrintVO.setSystemErrorMessage(systemError);
		footPrintVO.setRequestTimeStamp(new Date());
		footPrintVO.setUserErrorMessage(userError);
		footPrintVO.setErrorCode(errorCode);
		footPrintVO.setEventName(eventId);
		footPrintVO.setRequesterChannelId(reqChnlId);
		
		try {
			OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory
					.getDAOFactory(DAOFactory.ORACLE);
			FootPrintTrackingDAO fptDao = daoFactory
					.getOracleFootPrintTrackingDAO();

			fptDao.addRow(footPrintVO);

		} catch (Exception e) {
			log.fatal("FootPrintUtility > print > Exception > "
					+ e.getMessage());
			throw new SystemException(e);
		}
	}
}
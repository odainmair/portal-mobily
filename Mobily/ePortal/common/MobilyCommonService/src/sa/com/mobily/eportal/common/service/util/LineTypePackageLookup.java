package sa.com.mobily.eportal.common.service.util;
import java.util.HashMap;
import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.LineTypeByPackageNameDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;

public class LineTypePackageLookup {
	private static final Logger log = LoggerInterface.log;
    private static HashMap<String,String> LineLookupPerpackage  = null; 
    private static HashMap<String,String> LineTypeLookup  = null;

    
    private static LineTypePackageLookup  utility = null;
	private LineTypePackageLookup(){
	   	
	}
	
	public static LineTypePackageLookup getInstance(){
		if(utility == null)
			utility = new LineTypePackageLookup();
	   
		return utility;
	}
	
	public void cleanup() {
		log.debug("LineTypePackageLookup > cleanup > called");
		LineLookupPerpackage = null;
		LineTypeLookup = null;
	}

	
	public int getLineTypeByProductDesc(String productDesc){
		int lineType =0 ;
		String strProductDesc = productDesc.trim();
		log.debug("LineTypePackageLookup > getLineTypeByPackageName > strProductDesc["+strProductDesc+"]");
		if(LineTypeLookup == null) {
			log.debug("LineLookupPerpackage > getLineTypeByProductDesc > load Data from ePortal DB");
			
	     	OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
	     	LineTypeByPackageNameDAO lineTypeByPackageName = (LineTypeByPackageNameDAO)daoFactory.getLineTypeByPackageNameDAO();
	     	LineTypeLookup =  lineTypeByPackageName.LoadKineTypeByProductDesc();
		}
     	if(LineTypeLookup.containsKey(strProductDesc)) {
     		lineType = Integer.parseInt(LineTypeLookup.get(strProductDesc));
     	}

     	log.debug("LineLookupPerpackage > getLineTypeByProductDesc > llineType >"+lineType);
     	
		return lineType; 
	}

	
	/**
	 *  Input will be package name
	 *  output = Linetype (11 , 15 ,16 ,17 ,....)
	 * @param packgeName
	 * @return
	 */
	public  int getLineTypeByPackageName(String packgeName) {
		int lineType  = 0;
		String lineTypeDesc= packgeName.trim();
		try {
			if(LineLookupPerpackage == null) {
				log.debug("LineLookupPerpackage > getLineTypeByPackageName > load Data from Siebel DB");
				
			 	OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
			 	LineTypeByPackageNameDAO lineTypeByPackageName = (LineTypeByPackageNameDAO)daoFactory.getLineTypeByPackageNameDAO();
			 	LineLookupPerpackage =  lineTypeByPackageName.loadProductLinePerPackageName();
			}
			if(LineLookupPerpackage.containsKey(packgeName)) {
				log.debug("LineTypePackageLookup > getLineTypeByPackageName > Get Item List from Lookup Table Hash["+packgeName+"]");
				lineTypeDesc = (String)LineLookupPerpackage.get(packgeName);
				log.debug("LineTypePackageLookup > getLineTypeByPackageName > lineTypeDesc["+lineTypeDesc+"]");
				lineType = getLineTypeByProductDesc(lineTypeDesc);
			}
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			log.fatal("getLineTypeByPackageName > SystemException > "+e.getMessage());

			
		}
		return lineType;
	}

	
	public static void main(String args[])
	{
//		System.out.println(LineTypePackageLookup.getInstance().getLineTypeByPackageName("Business Raqi IUC Connect"));
//		System.out.println(LineTypePackageLookup.getInstance().getLineTypeByPackageName("Connect Teacher 2G"));
//		System.out.println(LineTypePackageLookup.getInstance().getLineTypeByPackageName("FTTH Package"));
	}
}

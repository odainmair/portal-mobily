package sa.com.mobily.eportal.common.service.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import sa.com.mobily.eportal.common.service.util.MQQueueHandler.QueueHandler;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "deploymentType",
    "queueFiles",
    "queues"
})
@XmlRootElement(name = "MQ_HANDLER")
public class MQHandler {
	@XmlElement(name = "GLOBAL_DEPLOYMENT_TYPE", required = true)
	private String deploymentType;
	@XmlElement(name = "QUEUE_CONFIG_FILE", required = true)
	private ArrayList<String> queueFiles;
	
	private Map<String, ArrayList<String>> queues;
	
	private static MQHandler me;
	public static final String DEPLOYMENT_TYPE_TESTING 		= "TESTING";
	public static final String DEPLOYMENT_TYPE_PER_REQUEST 	= "PER_REQUEST";
	public static final String DEPLOYMENT_TYPE_PRODUCTION 	= "PRODUCTION";
	public static final String MQ_SIMULATOR_CONFIG_FILE 	= "MQHandlerConfig.xml";
	
	private MQHandler() {}
	
	public static MQHandler getInstance(){
		if(me == null){
			try {
				JAXBContext jc = JAXBContext
						.newInstance(MQHandler.class, MQQueueHandler.class);
				Unmarshaller unmarshaller = jc.createUnmarshaller();
				XMLInputFactory xmlif = XMLInputFactory.newInstance();
				ClassLoader cl = Thread.currentThread().getContextClassLoader();
				XMLStreamReader xmler = xmlif.createXMLStreamReader(cl.getResourceAsStream(MQ_SIMULATOR_CONFIG_FILE));
				me = (MQHandler) unmarshaller.unmarshal(xmler);
				
				if(me.getDeploymentType().equals(DEPLOYMENT_TYPE_TESTING) || me.getDeploymentType().equals(DEPLOYMENT_TYPE_PER_REQUEST)){
					MQQueueHandler queueHandler = null;
					for (Iterator<String> moduleQueues = me.getQueueFiles().iterator(); moduleQueues.hasNext();) {
						try {
							xmler = xmlif.createXMLStreamReader(cl.getResourceAsStream(moduleQueues.next()));
							queueHandler = (MQQueueHandler) unmarshaller.unmarshal(xmler);
							for (Iterator<QueueHandler> queueIterator = queueHandler.getQueues().iterator(); queueIterator.hasNext();) {
								QueueHandler queue = (QueueHandler) queueIterator.next();
								if(queue.getDeploymentType().equals(DEPLOYMENT_TYPE_TESTING) || me.getDeploymentType().equals(DEPLOYMENT_TYPE_TESTING)){
									me.getQueues().put(queue.getQueueName(), (ArrayList<String>)queue.getResponses());
								}
							}
						} catch (Exception e) {}
					}
				}
				
			} catch (Exception e) {
//				e.printStackTrace();
				me = new MQHandler();
			}
		}
		return me;
	}


	public String getDeploymentType() {
		if(deploymentType == null)
			deploymentType = DEPLOYMENT_TYPE_PRODUCTION;
		return deploymentType;
	}



	public void setDeploymentType(String deploymentType) {
		this.deploymentType = deploymentType;
	}
	public List<String> getQueueFiles() {
		if(queueFiles == null)
			queueFiles = new ArrayList<String>();
		return queueFiles;
	}

	public void setQueueFiles(ArrayList<String> queueFiles) {
		this.queueFiles = queueFiles;
	}
	
	public Map<String, ArrayList<String>> getQueues() {
		if(queues == null)
			queues = new HashMap<String, ArrayList<String>>();
		return queues;
	}

	public void setQueues(Map<String, ArrayList<String>> queues) {
		this.queues = queues;
	}
	
	
	
	@Override
	public String toString() {
		return "MQHandler [deploymentType=" + deploymentType + ", queueFiles=" + queueFiles + ", queues="
				+ queues + "]";
	}
}

package sa.com.mobily.eportal.common.service.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "queues"
})
@XmlRootElement(name = "MODULE_QUEUES")
public class MQQueueHandler {
	@XmlElement(name = "QUEUE", required = true)
	private List<QueueHandler> queues;
	
	public List<QueueHandler> getQueues() {
		return queues;
	}
	
	public void setQueues(List<QueueHandler> queues) {
		this.queues = queues;
	}
	
	@Override
	public String toString() {
		return "MQQueueHandler [queues=" + queues + "]";
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "queueName", "deploymentType", "responses" })
	public static class QueueHandler {
		
		@XmlElement(name = "RQUEST_QUEUE_NAME", required = true)
		private String queueName;
		@XmlElement(name = "DEPLOYMENT_TYPE", required = true)
		private String deploymentType;
		@XmlElement(name = "REPLY", required = true)
		private List<String> responses;
		public String getQueueName() {
			if(queueName == null)
				queueName = "";
			return queueName;
		}
		public void setQueueName(String queueName) {
			this.queueName = queueName;
		}
		public String getDeploymentType() {
			if(deploymentType == null)
				deploymentType = MQHandler.DEPLOYMENT_TYPE_PRODUCTION;
			return deploymentType;
		}
		public void setDeploymentType(String deploymentType) {
			this.deploymentType = deploymentType;
		}
		public List<String> getResponses() {
			if(responses == null)
				responses = new ArrayList<String>();
			return responses;
		}
		public void setResponses(List<String> responses) {
			this.responses = responses;
		}
		@Override
		public String toString() {
			
			String x = "";
			for (Iterator<String> iterator = responses.iterator(); iterator.hasNext();) {
				String type = (String) iterator.next();
				x += type;
			}
			return "QueueHandler [queueName=" + queueName + ", deploymentType="
					+ deploymentType + ", responses=" + x + "]";
		}
	}
}

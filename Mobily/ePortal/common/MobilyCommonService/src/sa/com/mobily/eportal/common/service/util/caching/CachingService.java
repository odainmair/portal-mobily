package sa.com.mobily.eportal.common.service.util.caching;

import java.io.Serializable;
import java.util.logging.Level;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.validation.constraints.NotNull;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

import com.ibm.websphere.cache.DistributedMap;
import com.ibm.ws.security.auth.DistributedMapFactory;

public class CachingService {
	
	private CachingService(){}
	private static CachingService INSTANCE;
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	private static DistributedMap RESPONSES;
	private static String clazz = CachingService.class.getName();
	
	public synchronized static CachingService getInstance(){
		if(INSTANCE == null)
			INSTANCE = new CachingService();
		
		return INSTANCE;
	}	
	
	private static DistributedMap getResponses(){
		if(RESPONSES == null){
			try	{
				RESPONSES = (DistributedMap) new InitialContext().lookup("cache/eai/services");
//				DistributedMap dm= (DistributedMap)DistributedMapFactory.getMap("cache/eai/services");
				RESPONSES.setTimeToLive(900);
			}
			catch (NamingException e){
				executionContext.log(Level.SEVERE, "Exception " + e.getMessage(), clazz, "getResponses", e);
				throw new RuntimeException(e);
			}
		}
		return RESPONSES;
	}
	
	public Serializable getCachedResponse(@NotNull String key){
		try{
			return (Serializable) getResponses().get(key);
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "Exception " + e.getMessage(), clazz, "getCachedResponse", e);
			throw new RuntimeException(e);
		}
	}
	
	public void putCachedResponse(@NotNull String key, Serializable value){
		try{
			getResponses().put(key, value);
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "Exception " + e.getMessage(), clazz, "putCachedResponse", e);
			throw new RuntimeException(e);
		}
	}
}

package sa.com.mobily.eportal.common.service.util.context;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
//Updated By Rasha 
//import org.apache.openjpa.lib.util.concurrent.ConcurrentHashMap;

import sa.com.mobily.eportal.common.service.util.LoggerHelper;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

public class ExecutionContextFactory
{
	private final static String DEV_MODE = "DEV_MODE";

	private static boolean devMode;
	
	private static String strDevMode;
	
  // private static ExecutionContextMap executionContextMap = null;
   private static Map<String,ExecutionContextLogger> ecLoggerMap=new ConcurrentHashMap<>();
  
   public synchronized static ExecutionContext getExecutionContext(String fileName){
		if(ecLoggerMap.containsKey(fileName))
			return ecLoggerMap.get(fileName);
		ExecutionContextLogger ec= new ExecutionContextLogger(fileName);
		 ecLoggerMap.put(fileName,ec) ;
		 return ec;
	}
	
	public synchronized static ExecutionContext getExecutionContext()
	{
       
		return getExecutionContext("unspecified-logger");
		
//		try
//		{
//			if (strDevMode == null)
//			{
//				String strDevMode = (String) ResourceEnvironmentProviderService.getInstance().getPropertyValue(DEV_MODE);
//				if (StringUtils.isNotEmpty(strDevMode) && strDevMode.equalsIgnoreCase("TRUE"))
//				{
//					devMode = true;
//				}
//			}
//		}
//		catch (Throwable e)
//		{
////			e.printStackTrace();
//		}
//		if (executionContextMap == null)
//		{
//			executionContextMap = new ExecutionContextMap(devMode);
//		}
//		return executionContextMap;
	}
}
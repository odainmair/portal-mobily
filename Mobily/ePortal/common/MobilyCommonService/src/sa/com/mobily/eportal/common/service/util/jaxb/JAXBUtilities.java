package sa.com.mobily.eportal.common.service.util.jaxb;

import java.io.StringReader;


import java.io.StringWriter;
import java.util.logging.Level;

import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

public class JAXBUtilities {
	
	private MarshalPool marshallerPool;
	private MarshalPool unmarshallerPool;
	
	private static String clazz = JAXBUtilities.class.getName();

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	
	private static JAXBUtilities instance;
	
	private JAXBUtilities(){}
	
	public static JAXBUtilities getInstance(){
		if(instance == null){
			instance = new JAXBUtilities();
			instance.marshallerPool = new MarshalPool(new MarshallerFactory());
			instance.unmarshallerPool = new MarshalPool(new UnmarshallerFactory());
		}
		return instance;
	}
	
	public Object unmarshal(Class<?> target, String replyMessage) throws Exception{
		Unmarshaller unmarshaller = null;
		Object obj = null;
		try {
			unmarshaller = (Unmarshaller)unmarshallerPool.borrowObject(target);
			
			obj = unmarshaller.unmarshal(XMLInputFactory.newInstance()
							.createXMLStreamReader(new StringReader(replyMessage)));
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "Exception while unmarshal reply message", clazz, "unmarshal", e);
			throw e;
		} finally {
			try {
				unmarshallerPool.returnObject(target, unmarshaller);
			} catch (Exception e) {
				executionContext.log(Level.SEVERE, "Exception while returning unmarshaller", clazz, "unmarshal", e);
			}
		}
		return obj;
	}
	
	public String marshal(Object obj) throws Exception{
		Marshaller marshaller = null;
		String output = null;
		try {
			marshaller = (Marshaller)marshallerPool.borrowObject(obj.getClass());
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			StringWriter writer = new StringWriter();
			marshaller.marshal(obj,  writer);
			output = writer.toString();
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "Exception while marshal request object", clazz, "marshal", e);
			throw e;
		} finally {
			try {
				marshallerPool.returnObject(obj.getClass(), marshaller);
			} catch (Exception e) {
				executionContext.log(Level.SEVERE, "Exception while returning marshaller", clazz, "marshal", e);
			}
		}
		return output;
		
	}
}

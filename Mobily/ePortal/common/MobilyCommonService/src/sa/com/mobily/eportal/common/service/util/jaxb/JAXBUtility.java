package sa.com.mobily.eportal.common.service.util.jaxb;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;

public class JAXBUtility{
	
	public static String marshal( Object object,Class className){
		String xmlOutput = "";
		try{
		    JAXBContext jContext = JAXBContext.newInstance(className);
		    Marshaller marshallObj = jContext.createMarshaller();
		    marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		    StringWriter writer = new StringWriter();
		    marshallObj.marshal(object, writer);
		    xmlOutput  = writer.toString();
		} catch(Exception e) {
		    e.printStackTrace();
		}
		return xmlOutput;
	}
	
	public static Object unmarshal(Class className,String xmlResponse){
		Object obj = null;
		try{
		    JAXBContext jContext = JAXBContext.newInstance(className);
		    Unmarshaller unmarshallerObj = jContext.createUnmarshaller();
		    obj= unmarshallerObj.unmarshal(XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(xmlResponse)));
		   
		}catch(Exception e){
		    e.printStackTrace();
		}
		return obj;
	}

}

/**
 * 
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author s.vathsavai.mit
 *
 */
public class BillVO extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4143407152292515017L;
	
	private String billNumber = null;
	private String billEndDate = null;
	
	/**
	 * 
	 * @return billNumber
	 */
	public String getBillNumber() {
		return billNumber;
	}
	
	/**
	 * 
	 * @param billNumber
	 */
	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}
	
	/**
	 * 
	 * @return billEndDate
	 */
	public String getBillEndDate() {
		return billEndDate;
	}
	
	/**
	 * 
	 * @param billEndDate
	 */
	public void setBillEndDate(String billEndDate) {
		this.billEndDate = billEndDate;
	}
	
	
}

/*
 * Created on Nov 18, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;



/**
 * @author aghareeb
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class CorporateAuthorizedPersonAddressVO extends BaseVO {

	private String contactPreference;

	private String authorizedPersonMobile;

	private String authorizedPersonEmail;

	private String authorizedPersonTelephone;

	private String authorizedPersonFax;

	/**
	 * @return Returns the authorizedPersonEmail.
	 */
	public String getAuthorizedPersonEmail() {
		return authorizedPersonEmail;
	}

	/**
	 * @param authorizedPersonEmail
	 *            The authorizedPersonEmail to set.
	 */
	public void setAuthorizedPersonEmail(String authorizedPersonEmail) {
		this.authorizedPersonEmail = authorizedPersonEmail;
	}

	/**
	 * @return Returns the authorizedPersonFax.
	 */
	public String getAuthorizedPersonFax() {
		return authorizedPersonFax;
	}

	/**
	 * @param authorizedPersonFax
	 *            The authorizedPersonFax to set.
	 */
	public void setAuthorizedPersonFax(String authorizedPersonFax) {
		this.authorizedPersonFax = authorizedPersonFax;
	}

	/**
	 * @return Returns the authorizedPersonMobile.
	 */
	public String getAuthorizedPersonMobile() {
		return authorizedPersonMobile;
	}

	/**
	 * @param authorizedPersonMobile
	 *            The authorizedPersonMobile to set.
	 */
	public void setAuthorizedPersonMobile(String authorizedPersonMobile) {
		this.authorizedPersonMobile = authorizedPersonMobile;
	}

	/**
	 * @return Returns the authorizedPersonTelephone.
	 */
	public String getAuthorizedPersonTelephone() {
		return authorizedPersonTelephone;
	}

	/**
	 * @param authorizedPersonTelephone
	 *            The authorizedPersonTelephone to set.
	 */
	public void setAuthorizedPersonTelephone(String authorizedPersonTelephone) {
		this.authorizedPersonTelephone = authorizedPersonTelephone;
	}

	/**
	 * @return Returns the contactPreference.
	 */
	public String getContactPreference() {
		return contactPreference;
	}

	/**
	 * @param contactPreference
	 *            The contactPreference to set.
	 */
	public void setContactPreference(String contactPreference) {
		this.contactPreference = contactPreference;
	}
}

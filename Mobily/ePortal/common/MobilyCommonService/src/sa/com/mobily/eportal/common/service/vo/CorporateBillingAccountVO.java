/*******************************************************************************
 * Module: CorporateBillingAccountVO.java Author: a.samir Purpose: Defines the
 * Class CorporateBillingAccountVO
 ******************************************************************************/

package sa.com.mobily.eportal.common.service.vo;



public class CorporateBillingAccountVO extends BaseVO {
    private String billLangRef;

    private String billFreq;

    private String billMediaType;

    private String billCycle;

    private String billCompanyName;

    private CorporateBillingAccountRespVO corporateBillingAccountRespVO;

    private String billingAccountNumber;

    private String billRowID;

	/**
	 * @return Returns the billCompanyName.
	 */
	public String getBillCompanyName() {
		return billCompanyName;
	}

	/**
	 * @param billCompanyName
	 *            The billCompanyName to set.
	 */
	public void setBillCompanyName(String billCompanyName) {
		this.billCompanyName = billCompanyName;
	}

	/**
	 * @return Returns the billCycle.
	 */
	public String getBillCycle() {
		return billCycle;
	}

	/**
	 * @param billCycle
	 *            The billCycle to set.
	 */
	public void setBillCycle(String billCycle) {
		this.billCycle = billCycle;
	}

	/**
	 * @return Returns the billFreq.
	 */
	public String getBillFreq() {
		return billFreq;
	}

	/**
	 * @param billFreq
	 *            The billFreq to set.
	 */
	public void setBillFreq(String billFreq) {
		this.billFreq = billFreq;
	}

	/**
	 * @return Returns the billingAccountNumber.
	 */
	public String getBillingAccountNumber() {
		return billingAccountNumber;
	}

	/**
	 * @param billingAccountNumber
	 *            The billingAccountNumber to set.
	 */
	public void setBillingAccountNumber(String billingAccountNumber) {
		this.billingAccountNumber = billingAccountNumber;
	}

	/**
	 * @return Returns the billLangRef.
	 */
	public String getBillLangRef() {
		return billLangRef;
	}

	/**
	 * @param billLangRef
	 *            The billLangRef to set.
	 */
	public void setBillLangRef(String billLangRef) {
		this.billLangRef = billLangRef;
	}

	/**
	 * @return Returns the billMediaType.
	 */
	public String getBillMediaType() {
		return billMediaType;
	}

	/**
	 * @param billMediaType
	 *            The billMediaType to set.
	 */
	public void setBillMediaType(String billMediaType) {
		this.billMediaType = billMediaType;
	}

	/**
	 * @return Returns the billRowID.
	 */
	public String getBillRowID() {
		return billRowID;
	}

	/**
	 * @param billRowID
	 *            The billRowID to set.
	 */
	public void setBillRowID(String billRowID) {
		this.billRowID = billRowID;
	}

	/**
	 * @return Returns the corporateBillingAccountRespVO.
	 */
	public CorporateBillingAccountRespVO getCorporateBillingAccountRespVO() {
		return corporateBillingAccountRespVO;
	}

	/**
	 * @param corporateBillingAccountRespVO
	 *            The corporateBillingAccountRespVO to set.
	 */
	public void setCorporateBillingAccountRespVO(
			CorporateBillingAccountRespVO corporateBillingAccountRespVO) {
		this.corporateBillingAccountRespVO = corporateBillingAccountRespVO;
	}
}
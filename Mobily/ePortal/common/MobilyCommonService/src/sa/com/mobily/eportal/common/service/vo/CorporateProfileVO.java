/*******************************************************************************
 * Module: CorporateProfileVO.java Author: a.samir Purpose: Defines the Class
 * CorporateProfileVO
 ******************************************************************************/

package sa.com.mobily.eportal.common.service.vo;


public class CorporateProfileVO extends Profile {
	private String companyName;

	private String companyOfficeLocation;

	private String companycapital;

	private String companyCountryOfOrigin;

	private String companyIDDocType;

	private String companyIDDocIssueDate;

	private String companyIDDocIssuePlace;

	private String companyType;

	private String companyNoOfOffices;

	private String companyNoOfEmployees;

	private String companyIDNumber;

	private String companyIDDocExpDate;

	private String companySubType;

	private String companyAccountNumber;

	private String companyCustomerType;

	private String companyAccountStatus;

	private String companyDiscount;

	private String companyCustomerClassification;

	private String companyCustomerCategory;

	private String companyKeyAccountManager;

	private String companyCreatedBy;

	private CorporateAuthorizedPersonVO corporateAuthorizedPersonVO;

	private CorporateAddressVO corporateAddressVO;

	private CorporateBillingAccountVO corporateBillingAccountVO;

	private int corporateNumberOfLines;

	private int corporateCreditLimit;

	private String companyCreatedDate;

	private String CompanyRowID;

	private String CompanyStatus;

	private String customerLevelAccountNumber;

	/**
	 * @return Returns the companyAccountNumber.
	 */
	public String getCompanyAccountNumber() {
		return companyAccountNumber;
	}

	/**
	 * @param companyAccountNumber
	 *            The companyAccountNumber to set.
	 */
	public void setCompanyAccountNumber(String companyAccountNumber) {
		this.companyAccountNumber = companyAccountNumber;
	}

	/**
	 * @return Returns the companyAccountStatus.
	 */
	public String getCompanyAccountStatus() {
		return companyAccountStatus;
	}

	/**
	 * @param companyAccountStatus
	 *            The companyAccountStatus to set.
	 */
	public void setCompanyAccountStatus(String companyAccountStatus) {
		this.companyAccountStatus = companyAccountStatus;
	}

	/**
	 * @return Returns the companycapital.
	 */
	public String getCompanycapital() {
		return companycapital;
	}

	/**
	 * @param companycapital
	 *            The companycapital to set.
	 */
	public void setCompanycapital(String companycapital) {
		this.companycapital = companycapital;
	}

	/**
	 * @return Returns the companyCountryOfOrigin.
	 */
	public String getCompanyCountryOfOrigin() {
		return companyCountryOfOrigin;
	}

	/**
	 * @param companyCountryOfOrigin
	 *            The companyCountryOfOrigin to set.
	 */
	public void setCompanyCountryOfOrigin(String companyCountryOfOrigin) {
		this.companyCountryOfOrigin = companyCountryOfOrigin;
	}

	/**
	 * @return Returns the companyCreatedBy.
	 */
	public String getCompanyCreatedBy() {
		return companyCreatedBy;
	}

	/**
	 * @param companyCreatedBy
	 *            The companyCreatedBy to set.
	 */
	public void setCompanyCreatedBy(String companyCreatedBy) {
		this.companyCreatedBy = companyCreatedBy;
	}

	/**
	 * @return Returns the companyCreatedDate.
	 */
	public String getCompanyCreatedDate() {
		return companyCreatedDate;
	}

	/**
	 * @param companyCreatedDate
	 *            The companyCreatedDate to set.
	 */
	public void setCompanyCreatedDate(String companyCreatedDate) {
		this.companyCreatedDate = companyCreatedDate;
	}

	/**
	 * @return Returns the companyCustomerCategory.
	 */
	public String getCompanyCustomerCategory() {
		return companyCustomerCategory;
	}

	/**
	 * @param companyCustomerCategory
	 *            The companyCustomerCategory to set.
	 */
	public void setCompanyCustomerCategory(String companyCustomerCategory) {
		this.companyCustomerCategory = companyCustomerCategory;
	}

	/**
	 * @return Returns the companyCustomerClassification.
	 */
	public String getCompanyCustomerClassification() {
		return companyCustomerClassification;
	}

	/**
	 * @param companyCustomerClassification
	 *            The companyCustomerClassification to set.
	 */
	public void setCompanyCustomerClassification(
			String companyCustomerClassification) {
		this.companyCustomerClassification = companyCustomerClassification;
	}

	/**
	 * @return Returns the companyCustomerType.
	 */
	public String getCompanyCustomerType() {
		return companyCustomerType;
	}

	/**
	 * @param companyCustomerType
	 *            The companyCustomerType to set.
	 */
	public void setCompanyCustomerType(String companyCustomerType) {
		this.companyCustomerType = companyCustomerType;
	}

	/**
	 * @return Returns the companyDiscount.
	 */
	public String getCompanyDiscount() {
		return companyDiscount;
	}

	/**
	 * @param companyDiscount
	 *            The companyDiscount to set.
	 */
	public void setCompanyDiscount(String companyDiscount) {
		this.companyDiscount = companyDiscount;
	}

	/**
	 * @return Returns the companyIDDocExpDate.
	 */
	public String getCompanyIDDocExpDate() {
		return companyIDDocExpDate;
	}

	/**
	 * @param companyIDDocExpDate
	 *            The companyIDDocExpDate to set.
	 */
	public void setCompanyIDDocExpDate(String companyIDDocExpDate) {
		this.companyIDDocExpDate = companyIDDocExpDate;
	}

	/**
	 * @return Returns the companyIDDocIssueDate.
	 */
	public String getCompanyIDDocIssueDate() {
		return companyIDDocIssueDate;
	}

	/**
	 * @param companyIDDocIssueDate
	 *            The companyIDDocIssueDate to set.
	 */
	public void setCompanyIDDocIssueDate(String companyIDDocIssueDate) {
		this.companyIDDocIssueDate = companyIDDocIssueDate;
	}

	/**
	 * @return Returns the companyIDDocIssuePlace.
	 */
	public String getCompanyIDDocIssuePlace() {
		return companyIDDocIssuePlace;
	}

	/**
	 * @param companyIDDocIssuePlace
	 *            The companyIDDocIssuePlace to set.
	 */
	public void setCompanyIDDocIssuePlace(String companyIDDocIssuePlace) {
		this.companyIDDocIssuePlace = companyIDDocIssuePlace;
	}

	/**
	 * @return Returns the companyIDDocType.
	 */
	public String getCompanyIDDocType() {
		return companyIDDocType;
	}

	/**
	 * @param companyIDDocType
	 *            The companyIDDocType to set.
	 */
	public void setCompanyIDDocType(String companyIDDocType) {
		this.companyIDDocType = companyIDDocType;
	}

	/**
	 * @return Returns the companyIDNumber.
	 */
	public String getCompanyIDNumber() {
		return companyIDNumber;
	}

	/**
	 * @param companyIDNumber
	 *            The companyIDNumber to set.
	 */
	public void setCompanyIDNumber(String companyIDNumber) {
		this.companyIDNumber = companyIDNumber;
	}

	/**
	 * @return Returns the companyKeyAccountManager.
	 */
	public String getCompanyKeyAccountManager() {
		return companyKeyAccountManager;
	}

	/**
	 * @param companyKeyAccountManager
	 *            The companyKeyAccountManager to set.
	 */
	public void setCompanyKeyAccountManager(String companyKeyAccountManager) {
		this.companyKeyAccountManager = companyKeyAccountManager;
	}

	/**
	 * @return Returns the companyName.
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName
	 *            The companyName to set.
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return Returns the companyNoOfEmployees.
	 */
	public String getCompanyNoOfEmployees() {
		return companyNoOfEmployees;
	}

	/**
	 * @param companyNoOfEmployees
	 *            The companyNoOfEmployees to set.
	 */
	public void setCompanyNoOfEmployees(String companyNoOfEmployees) {
		this.companyNoOfEmployees = companyNoOfEmployees;
	}

	/**
	 * @return Returns the companyNoOfOffices.
	 */
	public String getCompanyNoOfOffices() {
		return companyNoOfOffices;
	}

	/**
	 * @param companyNoOfOffices
	 *            The companyNoOfOffices to set.
	 */
	public void setCompanyNoOfOffices(String companyNoOfOffices) {
		this.companyNoOfOffices = companyNoOfOffices;
	}

	/**
	 * @return Returns the companyOfficeLocation.
	 */
	public String getCompanyOfficeLocation() {
		return companyOfficeLocation;
	}

	/**
	 * @param companyOfficeLocation
	 *            The companyOfficeLocation to set.
	 */
	public void setCompanyOfficeLocation(String companyOfficeLocation) {
		this.companyOfficeLocation = companyOfficeLocation;
	}

	/**
	 * @return Returns the companyRowID.
	 */
	public String getCompanyRowID() {
		return CompanyRowID;
	}

	/**
	 * @param companyRowID
	 *            The companyRowID to set.
	 */
	public void setCompanyRowID(String companyRowID) {
		CompanyRowID = companyRowID;
	}

	/**
	 * @return Returns the companyStatus.
	 */
	public String getCompanyStatus() {
		return CompanyStatus;
	}

	/**
	 * @param companyStatus
	 *            The companyStatus to set.
	 */
	public void setCompanyStatus(String companyStatus) {
		CompanyStatus = companyStatus;
	}

	/**
	 * @return Returns the companySubType.
	 */
	public String getCompanySubType() {
		return companySubType;
	}

	/**
	 * @param companySubType
	 *            The companySubType to set.
	 */
	public void setCompanySubType(String companySubType) {
		this.companySubType = companySubType;
	}

	/**
	 * @return Returns the companyType.
	 */
	public String getCompanyType() {
		return companyType;
	}

	/**
	 * @param companyType
	 *            The companyType to set.
	 */
	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	/**
	 * @return Returns the corporateAddressVO.
	 */
	public CorporateAddressVO getCorporateAddressVO() {
		return corporateAddressVO;
	}

	/**
	 * @param corporateAddressVO
	 *            The corporateAddressVO to set.
	 */
	public void setCorporateAddressVO(CorporateAddressVO corporateAddressVO) {
		this.corporateAddressVO = corporateAddressVO;
	}

	/**
	 * @return Returns the corporateAuthorizedPersonVO.
	 */
	public CorporateAuthorizedPersonVO getCorporateAuthorizedPersonVO() {
		return corporateAuthorizedPersonVO;
	}

	/**
	 * @param corporateAuthorizedPersonVO
	 *            The corporateAuthorizedPersonVO to set.
	 */
	public void setCorporateAuthorizedPersonVO(
			CorporateAuthorizedPersonVO corporateAuthorizedPersonVO) {
		this.corporateAuthorizedPersonVO = corporateAuthorizedPersonVO;
	}

	/**
	 * @return Returns the corporateBillingAccountVO.
	 */
	public CorporateBillingAccountVO getCorporateBillingAccountVO() {
		return corporateBillingAccountVO;
	}

	/**
	 * @param corporateBillingAccountVO
	 *            The corporateBillingAccountVO to set.
	 */
	public void setCorporateBillingAccountVO(
			CorporateBillingAccountVO corporateBillingAccountVO) {
		this.corporateBillingAccountVO = corporateBillingAccountVO;
	}

	/**
	 * @return Returns the corporateCreditLimit.
	 */
	public int getCorporateCreditLimit() {
		return corporateCreditLimit;
	}

	/**
	 * @param corporateCreditLimit
	 *            The corporateCreditLimit to set.
	 */
	public void setCorporateCreditLimit(int corporateCreditLimit) {
		this.corporateCreditLimit = corporateCreditLimit;
	}

	/**
	 * @return Returns the corporateNumberOfLines.
	 */
	public int getCorporateNumberOfLines() {
		return corporateNumberOfLines;
	}

	/**
	 * @param corporateNumberOfLines
	 *            The corporateNumberOfLines to set.
	 */
	public void setCorporateNumberOfLines(int corporateNumberOfLines) {
		this.corporateNumberOfLines = corporateNumberOfLines;
	}

	/**
	 * @return Returns the customerLevelAccountNumber.
	 */
	public String getCustomerLevelAccountNumber() {
		return customerLevelAccountNumber;
	}

	/**
	 * @param customerLevelAccountNumber
	 *            The customerLevelAccountNumber to set.
	 */
	public void setCustomerLevelAccountNumber(String customerLevelAccountNumber) {
		this.customerLevelAccountNumber = customerLevelAccountNumber;
	}
}
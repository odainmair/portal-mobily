/*
 * Created on Sep 14, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
//public class CreditCardMessageHeaderVO extends BaseVO {
public class CreditCardMessageHeaderVO 
{
    private String MsgFormat = null;
    private String RequestorChannelId = null;
    private String RequestorChannelFunction = null;
    private String RequestorIpAddress = null;
    private String RequestorUserId = null;
    private String ReturnCode = null;
    
	/**
	 * @return Returns the msgFormat.
	 */
	public String getMsgFormat() {
		return MsgFormat;
	}
	/**
	 * @param msgFormat The msgFormat to set.
	 */
	public void setMsgFormat(String msgFormat) {
		MsgFormat = msgFormat;
	}
	/**
	 * @return Returns the requestorChannelFunction.
	 */
	public String getRequestorChannelFunction() {
		return RequestorChannelFunction;
	}
	/**
	 * @param requestorChannelFunction The requestorChannelFunction to set.
	 */
	public void setRequestorChannelFunction(String requestorChannelFunction) {
		RequestorChannelFunction = requestorChannelFunction;
	}
	/**
	 * @return Returns the requestorChannelId.
	 */
	public String getRequestorChannelId() {
		return RequestorChannelId;
	}
	/**
	 * @param requestorChannelId The requestorChannelId to set.
	 */
	public void setRequestorChannelId(String requestorChannelId) {
		RequestorChannelId = requestorChannelId;
	}
	/**
	 * @return Returns the requestorIpAddress.
	 */
	public String getRequestorIpAddress() {
		return RequestorIpAddress;
	}
	/**
	 * @param requestorIpAddress The requestorIpAddress to set.
	 */
	public void setRequestorIpAddress(String requestorIpAddress) {
		RequestorIpAddress = requestorIpAddress;
	}
	/**
	 * @return Returns the requestorUserId.
	 */
	public String getRequestorUserId() {
		return RequestorUserId;
	}
	/**
	 * @param requestorUserId The requestorUserId to set.
	 */
	public void setRequestorUserId(String requestorUserId) {
		RequestorUserId = requestorUserId;
	}
	/**
	 * @return Returns the returnCode.
	 */
	public String getReturnCode() {
		return ReturnCode;
	}
	/**
	 * @param returnCode The returnCode to set.
	 */
	public void setReturnCode(String returnCode) {
		ReturnCode = returnCode;
	}
}

/*
 * Created on Sep 14, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.List;

/**
 * @author r.agarwal.mit
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */

// public class CreditCardPaymentRequestVO extends BaseVO{
public class CreditCardPaymentRequestVO
{

	private CreditCardMessageHeaderVO header = new CreditCardMessageHeaderVO();

	private String userIP = null;

	private String userUID = null;

	private String TransactionDate = null;

	private String TransactionPIN = null;

	private String Name = null;

	private String PrimeMSISDN = null;

	private String billingAddress;

	private String holderName;

	private String TransactionID = null;

	private String ID = null;

	private String NewTransactionPIN = null;

	private String MSISDN = null;

	private String NewPrimeMSISDN = null;

	private String ExpiryDate = null;

	private String CVV = null;

	private String Amount = null;

	private String AccountNumber = null;

	private String CustomerType = null;

	private String CreditCardNumber = null;

	private String CreditCardType = null;

	private String smsTo = null;

	private String billAccountNumber = null;

	private String language = null;

	private String isCorpAP = "";

	private String channelId = null;

	private String channelFunction = null;

	private String transactionId = null;

	private String transactionDate = null;

	private String customerName = null;

	private String creditCardNumber = null;

	private String creditCardType = null;

	private String expiryDate = null;

	private String customerAddress = null;

	private String amount = null;

	private String paymentType = null;

	private String paymentReason = null;

	private String customerType = null; // Prepaid | Postpaid

	private String accountNo = null;

	private String userID = null;

	private boolean isEncryptionRequired = false;

	private String returnCode = null;

	private String activationType = null;

	private String expiryDateMMYY = "";

	private int lineType;

	private String paymentLevel = "";

	private String serviceName = "";

	private List<Object> lineVOList=null;
	
	private String email = null;
	private boolean includeAllInactive = false;
	
	private boolean skipCreditCardActivation = false;
	private String pinValidation;
	private String serviceAccountNumber;
	
	public String getPinValidation()
	{
		return pinValidation;
	}

	public void setPinValidation(String pinValidation)
	{
		this.pinValidation = pinValidation;
	}

	public String getServiceAccountNumber()
	{
		return serviceAccountNumber;
	}

	public void setServiceAccountNumber(String serviceAccountNumber)
	{
		this.serviceAccountNumber = serviceAccountNumber;
	}

	/**
	 * @return the skipCreditCardActivation
	 */
	public boolean isSkipCreditCardActivation()
	{
		return skipCreditCardActivation;
	}

	/**
	 * @param skipCreditCardActivation the skipCreditCardActivation to set
	 */
	public void setSkipCreditCardActivation(boolean skipCreditCardActivation)
	{
		this.skipCreditCardActivation = skipCreditCardActivation;
	}

	public String getIsCorpAP()
	{
		return isCorpAP;
	}

	public void setIsCorpAP(String isCorpAP)
	{
		this.isCorpAP = isCorpAP;
	}

	public String getLanguage()
	{
		return language;
	}

	public void setLanguage(String language)
	{
		this.language = language;
	}

	public String getBillAccountNumber()
	{
		return billAccountNumber;
	}

	public void setBillAccountNumber(String billAccountNumber)
	{
		this.billAccountNumber = billAccountNumber;
	}

	public String getActivationType()
	{
		return activationType;
	}

	public void setActivationType(String activationType)
	{
		this.activationType = activationType;
	}

	/**
	 * @return Returns the amount.
	 */
	public String getAmount()
	{
		return Amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(String amount)
	{
		Amount = amount;
	}

	/**
	 * @return Returns the cVV.
	 */
	public String getCVV()
	{
		return CVV;
	}

	/**
	 * @param cvv
	 *            The cVV to set.
	 */
	public void setCVV(String cvv)
	{
		CVV = cvv;
	}

	/**
	 * @return Returns the expiryDate.
	 */
	public String getExpiryDate()
	{
		return ExpiryDate;
	}

	/**
	 * @param expiryDate
	 *            The expiryDate to set.
	 */
	public void setExpiryDate(String expiryDate)
	{
		ExpiryDate = expiryDate;
	}

	/**
	 * @return Returns the header.
	 */
	public CreditCardMessageHeaderVO getHeader()
	{
		return header;
	}

	/**
	 * @param header
	 *            The header to set.
	 */
	public void setHeader(CreditCardMessageHeaderVO header)
	{
		this.header = header;
	}

	/**
	 * @return Returns the iD.
	 */
	public String getID()
	{
		return ID;
	}

	/**
	 * @param id
	 *            The iD to set.
	 */
	public void setID(String id)
	{
		ID = id;
	}

	/**
	 * @return Returns the mSISDN.
	 */
	public String getMSISDN()
	{
		return MSISDN;
	}

	/**
	 * @param msisdn
	 *            The mSISDN to set.
	 */
	public void setMSISDN(String msisdn)
	{
		MSISDN = msisdn;
	}

	/**
	 * @return Returns the name.
	 */
	public String getName()
	{
		return Name;
	}

	/**
	 * @param name
	 *            The name to set.
	 */
	public void setName(String name)
	{
		Name = name;
	}

	/**
	 * @return Returns the newPrimeMSISDN.
	 */
	public String getNewPrimeMSISDN()
	{
		return NewPrimeMSISDN;
	}

	/**
	 * @param newPrimeMSISDN
	 *            The newPrimeMSISDN to set.
	 */
	public void setNewPrimeMSISDN(String newPrimeMSISDN)
	{
		NewPrimeMSISDN = newPrimeMSISDN;
	}

	/**
	 * @return Returns the newTransactionPIN.
	 */
	public String getNewTransactionPIN()
	{
		return NewTransactionPIN;
	}

	/**
	 * @param newTransactionPIN
	 *            The newTransactionPIN to set.
	 */
	public void setNewTransactionPIN(String newTransactionPIN)
	{
		NewTransactionPIN = newTransactionPIN;
	}

	/**
	 * @return Returns the primeMSISDN.
	 */
	public String getPrimeMSISDN()
	{
		return PrimeMSISDN;
	}

	/**
	 * @param primeMSISDN
	 *            The primeMSISDN to set.
	 */
	public void setPrimeMSISDN(String primeMSISDN)
	{
		PrimeMSISDN = primeMSISDN;
	}

	/**
	 * @return Returns the transactionDate.
	 */
	public String getTransactionDate()
	{
		return TransactionDate;
	}

	/**
	 * @param transactionDate
	 *            The transactionDate to set.
	 */
	public void setTransactionDate(String transactionDate)
	{
		TransactionDate = transactionDate;
	}

	/**
	 * @return Returns the transactionID.
	 */
	public String getTransactionID()
	{
		return TransactionID;
	}

	/**
	 * @param transactionID
	 *            The transactionID to set.
	 */
	public void setTransactionID(String transactionID)
	{
		TransactionID = transactionID;
	}

	/**
	 * @return Returns the transactionPIN.
	 */
	public String getTransactionPIN()
	{
		return TransactionPIN;
	}

	/**
	 * @param transactionPIN
	 *            The transactionPIN to set.
	 */
	public void setTransactionPIN(String transactionPIN)
	{
		TransactionPIN = transactionPIN;
	}

	/**
	 * @return Returns the billingAddress.
	 */
	public String getBillingAddress()
	{
		return billingAddress;
	}

	/**
	 * @param billingAddress
	 *            The billingAddress to set.
	 */
	public void setBillingAddress(String billingAddress)
	{
		this.billingAddress = billingAddress;
	}

	/**
	 * @return Returns the holderName.
	 */
	public String getHolderName()
	{
		return holderName;
	}

	/**
	 * @param holderName
	 *            The holderName to set.
	 */
	public void setHolderName(String holderName)
	{
		this.holderName = holderName;
	}

	/**
	 * @return Returns the accountNumber.
	 */
	public String getAccountNumber()
	{
		return AccountNumber;
	}

	/**
	 * @param accountNumber
	 *            The accountNumber to set.
	 */
	public void setAccountNumber(String accountNumber)
	{
		AccountNumber = accountNumber;
	}

	/**
	 * @return Returns the userIP.
	 */
	public String getUserIP()
	{
		return userIP;
	}

	/**
	 * @param userIP
	 *            The userIP to set.
	 */
	public void setUserIP(String userIP)
	{
		this.userIP = userIP;
	}

	/**
	 * @return Returns the userUID.
	 */
	public String getUserUID()
	{
		return userUID;
	}

	/**
	 * @param userUID
	 *            The userUID to set.
	 */
	public void setUserUID(String userUID)
	{
		this.userUID = userUID;
	}

	/**
	 * @return Returns the creditCardNumber.
	 */
	public String getCreditCardNumber()
	{
		return CreditCardNumber;
	}

	/**
	 * @param creditCardNumber
	 *            The creditCardNumber to set.
	 */
	public void setCreditCardNumber(String creditCardNumber)
	{
		CreditCardNumber = creditCardNumber;
	}

	/**
	 * @return Returns the creditCardType.
	 */
	public String getCreditCardType()
	{
		return CreditCardType;
	}

	/**
	 * @param creditCardType
	 *            The creditCardType to set.
	 */
	public void setCreditCardType(String creditCardType)
	{
		CreditCardType = creditCardType;
	}

	/**
	 * @return Returns the customerType.
	 */
	public String getCustomerType()
	{
		return CustomerType;
	}

	/**
	 * @param customerType
	 *            The customerType to set.
	 */
	public void setCustomerType(String customerType)
	{
		CustomerType = customerType;
	}

	/**
	 * @return Returns the smsTo.
	 */
	public String getSmsTo()
	{
		return smsTo;
	}

	/**
	 * @param smsTo
	 *            The smsTo to set.
	 */
	public void setSmsTo(String smsTo)
	{
		this.smsTo = smsTo;
	}

	public String getChannelId()
	{
		return channelId;
	}

	public void setChannelId(String channelId)
	{
		this.channelId = channelId;
	}

	public String getChannelFunction()
	{
		return channelFunction;
	}

	public void setChannelFunction(String channelFunction)
	{
		this.channelFunction = channelFunction;
	}

	public String getTransactionId()
	{
		return transactionId;
	}

	public void setTransactionId(String transactionId)
	{
		this.transactionId = transactionId;
	}

	public String getCustomerName()
	{
		return customerName;
	}

	public void setCustomerName(String customerName)
	{
		this.customerName = customerName;
	}

	public String getCustomerAddress()
	{
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress)
	{
		this.customerAddress = customerAddress;
	}

	public String getPaymentType()
	{
		return paymentType;
	}

	public void setPaymentType(String paymentType)
	{
		this.paymentType = paymentType;
	}

	public String getPaymentReason()
	{
		return paymentReason;
	}

	public void setPaymentReason(String paymentReason)
	{
		this.paymentReason = paymentReason;
	}

	public String getAccountNo()
	{
		return accountNo;
	}

	public void setAccountNo(String accountNo)
	{
		this.accountNo = accountNo;
	}

	public String getUserID()
	{
		return userID;
	}

	public void setUserID(String userID)
	{
		this.userID = userID;
	}

	public boolean isEncryptionRequired()
	{
		return isEncryptionRequired;
	}

	public void setEncryptionRequired(boolean isEncryptionRequired)
	{
		this.isEncryptionRequired = isEncryptionRequired;
	}

	public String getReturnCode()
	{
		return returnCode;
	}

	public void setReturnCode(String returnCode)
	{
		this.returnCode = returnCode;
	}

	public String getExpiryDateMMYY()
	{
		return expiryDateMMYY;
	}

	public void setExpiryDateMMYY(String expiryDateMMYY)
	{
		this.expiryDateMMYY = expiryDateMMYY;
	}

	public int getLineType()
	{
		return lineType;
	}

	public void setLineType(int lineType)
	{
		this.lineType = lineType;
	}

	public String getPaymentLevel()
	{
		return paymentLevel;
	}

	public void setPaymentLevel(String paymentLevel)
	{
		this.paymentLevel = paymentLevel;
	}

	public String getServiceName()
	{
		return serviceName;
	}

	public void setServiceName(String serviceName)
	{
		this.serviceName = serviceName;
	}

	public List<Object> getLineVOList()
	{
		return lineVOList;
	}

	public void setLineVOList(List<Object> lineVOList)
	{
		this.lineVOList = lineVOList;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @return the includeAllInactive
	 */
	public boolean isIncludeAllInactive()
	{
		return includeAllInactive;
	}

	/**
	 * @param includeAllInactive the includeAllInactive to set
	 */
	public void setIncludeAllInactive(boolean includeAllInactive)
	{
		this.includeAllInactive = includeAllInactive;
	}
	
	
}

/*
 * Created on Jul 3, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * @author g.krishnamurthy.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CustomerServiceDetReplyVO {
	private String returnCode;
	private String lineNumber;
	private int    customerType ;
	private String packageID;
	private String packageDescription;
	private Date   creationDate;
	private String errorMsg ;
	private int    status ;
	private int reason;
	private String corporatePackage;
	private ArrayList listOfServices =null;
	private String serviceName = "";
	private HashMap serviceMap = null;
	private String accountNumber;
	private String cpeSerialNo;
	private String maxUploadRate;
	private String maxDownloadRate;
	private String duration;
	private String packageName;
	private String customerCategory;
	private String cpeMACAddress;
	
	/**
	 * @return Returns the corporatePackage.
	 */
	public String getCorporatePackage() {
		return corporatePackage;
	}
	/**
	 * @param corporatePackage The corporatePackage to set.
	 */
	public void setCorporatePackage(String corporatePackage) {
		this.corporatePackage = corporatePackage;
	}
	/**
	 * @return Returns the retunCode.
	 */
	public String getReturnCode() {
		return returnCode;
	}
	/**
	 * @return Returns the creationDate.
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	/**
	 * @param creationDate The creationDate to set.
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	/**
	 * @return Returns the customerType.
	 */
	public int getCustomerType() {
		return customerType;
	}
	/**
	 * @param customerType The customerType to set.
	 */
	public void setCustomerType(int customerType) {
		this.customerType = customerType;
	}
	/**
	 * @return Returns the lineNumber.
	 */
	public String getLineNumber() {
		return lineNumber;
	}
	/**
	 * @param lineNumber The lineNumber to set.
	 */
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}
	/**
	 * @return Returns the packageDescription.
	 */
	public String getPackageDescription() {
		return packageDescription;
	}
	/**
	 * @param packageDescription The packageDescription to set.
	 */
	public void setPackageDescription(String packageDescription) {
		this.packageDescription = packageDescription;
	}
	/**
	 * @return Returns the packageID.
	 */
	public String getPackageID() {
		return packageID;
	}
	/**
	 * @param packageID The packageID to set.
	 */
	public void setPackageID(String packageID) {
		this.packageID = packageID;
	}
	/**
	 * @param retunCode The retunCode to set.
	 */
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	
	/**
	 * @return Returns the errorMsg.
	 */
	public String getErrorMsg() {
		return errorMsg;
	}
	/**
	 * @param errorMsg The errorMsg to set.
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	/**
	 * @return Returns the reason.
	 */
	public int getReason() {
		return reason;
	}
	/**
	 * @param reason The reason to set.
	 */
	public void setReason(int reason) {
		this.reason = reason;
	}
	/**
	 * @return Returns the status.
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param status The status to set.
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	/**
	 * @return Returns the listOfServices.
	 */
	public ArrayList getListOfServices() {
		return listOfServices;
	}
	/**
	 * @param listOfServices The listOfServices to set.
	 */
	public void setListOfServices(ArrayList listOfServices) {
		this.listOfServices = listOfServices;
	}
	/**
	 * @return Returns the serviceName.
	 */
	public String getServiceName() {
		return serviceName;
	}
	/**
	 * @param serviceName The serviceName to set.
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	/**
	 * @return Returns the serviceMap.
	 */
	public HashMap getServiceMap() {
		return serviceMap;
	}
	/**
	 * @param serviceMap The serviceMap to set.
	 */
	public void setServiceMap(HashMap serviceMap) {
		this.serviceMap = serviceMap;
	}
	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}
	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	/**
	 * @return the cpeSerialNo
	 */
	public String getCpeSerialNo() {
		return cpeSerialNo;
	}
	/**
	 * @param cpeSerialNo the cpeSerialNo to set
	 */
	public void setCpeSerialNo(String cpeSerialNo) {
		this.cpeSerialNo = cpeSerialNo;
	}
	/**
	 * @return the maxUploadRate
	 */
	public String getMaxUploadRate() {
		return maxUploadRate;
	}
	/**
	 * @param maxUploadRate the maxUploadRate to set
	 */
	public void setMaxUploadRate(String maxUploadRate) {
		this.maxUploadRate = maxUploadRate;
	}
	/**
	 * @return the maxDownloadRate
	 */
	public String getMaxDownloadRate() {
		return maxDownloadRate;
	}
	/**
	 * @param maxDownloadRate the maxDownloadRate to set
	 */
	public void setMaxDownloadRate(String maxDownloadRate) {
		this.maxDownloadRate = maxDownloadRate;
	}
	/**
	 * @return the duration
	 */
	public String getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(String duration) {
		this.duration = duration;
	}
	/**
	 * @return the customerCategory
	 */
	public String getCustomerCategory() {
		return customerCategory;
	}
	/**
	 * @param customerCategory the customerCategory to set
	 */
	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}
	/**
	 * @return the cpeMACAddress
	 */
	public String getCpeMACAddress() {
		return cpeMACAddress;
	}
	/**
	 * @param cpeMACAddress the cpeMACAddress to set
	 */
	public void setCpeMACAddress(String cpeMACAddress) {
		this.cpeMACAddress = cpeMACAddress;
	}
	/**
	 * @return the packageName
	 */
	public String getPackageName() {
		return packageName;
	}
	/**
	 * @param packageName the packageName to set
	 */
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	
}
/**
 * 
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.List;
import java.util.Map;

/**
 * @author s.vathsavai.mit
 * 
 */
public class CutServiceSubAccountVO {

    private String serviceAccNumber = null;
    private List<BillVO> billVOList = null;
    
 // circuit mode i.e Master Backup
    private String packageName = null; 
    private String name = null;
    private String aliasName = null;
    
 // circuit Items map. Key is item name and value is another Hash map, contains attribute name and value pairs.
    private Map<Object, Object> itemsMap = null; 

    private String vpnID = null;

 // status of the item like DIA - Circuit, IPVPN - Circuit or Ethernet - Circuit.
    private String status = null; 

    // below variables declaration reason: in front end we have only cut service
    // sub account vo. So get the values from BillingAccountVO and set this vo
    // to use in UI and email sending

    // to use in circuit details page
    private String networkConfiguration = null;
    // to use for sending email
    private String kamEmail = null;

    public String getKamEmail() {
	return kamEmail;
    }

    public void setKamEmail(String kamEmail) {
	this.kamEmail = kamEmail;
    }

    public String getNetworkConfiguration() {
	return networkConfiguration;
    }

    public void setNetworkConfiguration(String networkConfiguration) {
	this.networkConfiguration = networkConfiguration;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getServiceAccNumber() {
	return serviceAccNumber;
    }

    public void setServiceAccNumber(String serviceAccNumber) {
	this.serviceAccNumber = serviceAccNumber;
    }

    public List<BillVO> getBillVOList() {
	return billVOList;
    }

    public void setBillVOList(List<BillVO> billVOList) {
	this.billVOList = billVOList;
    }

    public String getPackageName() {
	return packageName;
    }

    public void setPackageName(String packageName) {
	this.packageName = packageName;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getAliasName() {
	return aliasName;
    }

    public void setAliasName(String aliasName) {
	this.aliasName = aliasName;
    }

    public String getVpnID() {
	return vpnID;
    }

    public void setVpnID(String vpnID) {
	this.vpnID = vpnID;
    }

    /**
     * @return the itemsMap
     */
    public Map<Object, Object> getItemsMap() {
        return itemsMap;
    }

    /**
     * @param itemsMap the itemsMap to set
     */
    public void setItemsMap(Map<Object, Object> itemsMap) {
        this.itemsMap = itemsMap;
    }

}

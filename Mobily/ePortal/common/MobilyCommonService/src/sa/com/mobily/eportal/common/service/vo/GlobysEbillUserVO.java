package sa.com.mobily.eportal.common.service.vo;

public class GlobysEbillUserVO extends BaseVO {
	private String username;
	private String billingAcctNumber;
	public static final String userState = "adminFullAccess";
	public String firstName;
	public String lastName;
	public String email;
	public String preferedLanguage;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPreferedLanguage() {
		return preferedLanguage;
	}

	public void setPreferedLanguage(String preferedLanguage) {
		this.preferedLanguage = preferedLanguage;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getBillingAcctNumber() {
		return billingAcctNumber;
	}

	public void setBillingAcctNumber(String billingAcctNumber) {
		this.billingAcctNumber = billingAcctNumber;
	}

	public GlobysEbillUserVO() {
		super();
	}

	public GlobysEbillUserVO(String billingAcctNumber, String username) {
		super();
		this.billingAcctNumber = billingAcctNumber;
		this.username = username;
	}

}

/*
 * Created on Jan 25, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;



/**
 * 
 * @author n.gundluru.mit
 *
 */
public class IPhoneReplyVO extends BaseVO {
	
	private String userId;
    private String parentMSISDN;
    private String originalMSISDN;
    private String parentHashCode;
    private String originalHashCode;
	private int customerType;
	private String packageID;
	private int lineType;
	private String isBlockedCustomer = "false";
	private String newsID;
	private String balance;
	String functionId = "";
	String errorCode = "";
	String errorMsg = "";		
	
	private String hashCode = "";
	private String email;
	private String orderNumber = "";
	
    public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getParentMSISDN() {
		return parentMSISDN;
	}

	public void setParentMSISDN(String parentMSISDN) {
		this.parentMSISDN = parentMSISDN;
	}

	public String getOriginalMSISDN() {
		return originalMSISDN;
	}

	public void setOriginalMSISDN(String originalMSISDN) {
		this.originalMSISDN = originalMSISDN;
	}

	public String getParentHashCode() {
		return parentHashCode;
	}

	public void setParentHashCode(String parentHashCode) {
		this.parentHashCode = parentHashCode;
	}

	public String getOriginalHashCode() {
		return originalHashCode;
	}

	public void setOriginalHashCode(String originalHashCode) {
		this.originalHashCode = originalHashCode;
	}
	
	public int getLineType() {
		return lineType;
	}

	public void setLineType(int lineType) {
		this.lineType = lineType;
	}
	
	public String getIsBlockedCustomer() {
		return isBlockedCustomer;
	}

	public void setIsBlockedCustomer(String isBlockedCustomer) {
		this.isBlockedCustomer = isBlockedCustomer;
	}

	/**
	 * @return true if the customer is prepaid else return false
	 */
	public boolean isPrepaidCustomer() {
		if (customerType == 1)
			return true;
		return false;
	}

	/**
	 * @return Returns the customerType.
	 */
	public int getCustomerType() {
		return customerType;
	}

	/**
	 * @param customerType
	 *            The customerType to set.
	 */
	public void setCustomerType(int customerType) {
		this.customerType = customerType;
	}


	/**
	 * @return Returns the packageID.
	 */
	public String getPackageID() {
		return packageID;
	}

	/**
	 * @param packageID
	 *            The packageID to set.
	 */
	public void setPackageID(String packageID) {
		this.packageID = packageID;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getFunctionId() {
		return functionId;
	}

	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}

	public String getNewsID() {
		return newsID;
	}

	public void setNewsID(String newsID) {
		this.newsID = newsID;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	/**
	 * @return the hashCode
	 */
	public String getHashCode() {
		return hashCode;
	}

	/**
	 * @param hashCode the hashCode to set
	 */
	public void setHashCode(String hashCode) {
		this.hashCode = hashCode;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	public String getOrderNumber()
	{
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber)
	{
		this.orderNumber = orderNumber;
	}
}
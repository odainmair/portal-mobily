/*
 * Created on Jan 05, 2012
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;


/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class LOVInfoVO {
	
	private String recTypeCode = "";
	private String recDesc = "";
	private String attribute1 = "";
	
	public String getRecTypeCode() {
		return recTypeCode;
	}
	public void setRecTypeCode(String recTypeCode) {
		this.recTypeCode = recTypeCode;
	}
	public String getRecDesc() {
		return recDesc;
	}
	public void setRecDesc(String recDesc) {
		this.recDesc = recDesc;
	}
	public String getAttribute1() {
		return attribute1;
	}
	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}
	
	

}

/*
 * Created on Jun 16, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.Locale;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class LookupTableVO {

	
	private String item  ;
	private String itemDesc_en;
	private String itemDesc_ar;
	private String id ;
	private int sub_id;
	
	public static void main(String[] args) {
	}
	/**
	 * @return Returns the item.
	 */
	public String getItem() {
		return item;
	}
	/**
	 * @param item The item to set.
	 */
	public void setItem(String item) {
		this.item = item;
	}
	/**
	 * @return Returns the itemDesc_ar.
	 */
	public String getItemDesc_ar() {
		return itemDesc_ar;
	}
	/**
	 * @param itemDesc_ar The itemDesc_ar to set.
	 */
	public void setItemDesc_ar(String itemDesc_ar) {
		this.itemDesc_ar = itemDesc_ar;
	}
	/**
	 * @return Returns the itemDesc_en.
	 */
	public String getItemDesc_en() {
		return itemDesc_en;
	}
	/**
	 * @param itemDesc_en The itemDesc_en to set.
	 */
	public void setItemDesc_en(String itemDesc_en) {
		this.itemDesc_en = itemDesc_en;
	}
	
	public String getItemDescByLocale(Locale locale) {
	  String desc = "";
	  if(locale.getLanguage().equalsIgnoreCase("en")) {
	  	desc = itemDesc_en;
	  }else {
	  	desc = itemDesc_ar;
	  }
	  return desc;
	}
	
	/**
	 * @return Returns the id.
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id The id to set.
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return Returns the sub_id.
	 */
	public int getSub_id() {
		return sub_id;
	}
	/**
	 * @param sub_id The sub_id to set.
	 */
	public void setSub_id(int sub_id) {
		this.sub_id = sub_id;
	}
}

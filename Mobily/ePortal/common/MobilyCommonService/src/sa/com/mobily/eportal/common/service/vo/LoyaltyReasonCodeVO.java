/*
 * Created on Apr 24, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author 
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class LoyaltyReasonCodeVO {

	private int reasonCode ;
	private String desc_en = "";
	private String desc_ar = "";
	
	
	public static void main(String[] args) {
	}
	/**
	 * @return Returns the desc_ar.
	 */
	public String getDesc_ar() {
		return desc_ar;
	}
	/**
	 * @param desc_ar The desc_ar to set.
	 */
	public void setDesc_ar(String desc_ar) {
		this.desc_ar = desc_ar;
	}
	/**
	 * @return Returns the desc_en.
	 */
	public String getDesc_en() {
		return desc_en;
	}
	/**
	 * @param desc_en The desc_en to set.
	 */
	public void setDesc_en(String desc_en) {
		this.desc_en = desc_en;
	}
	/**
	 * @return Returns the reasonCode.
	 */
	public int getReasonCode() {
		return reasonCode;
	}
	/**
	 * @param reasonCode The reasonCode to set.
	 */
	public void setReasonCode(int reasonCode) {
		this.reasonCode = reasonCode;
	}
	
	public String getReasonCodeDesc(String locale){
	String desc = "";
		if(locale.equalsIgnoreCase("en")){
	 	  desc = desc_en;
	 }else{
	 	  desc = desc_ar;
	 }	
	 return desc;
	}
}

/*
 * Created on Feb 20, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;



/**
 * @author aymanh
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ManagedWimaxDataVO extends BaseVO 
{
	private String mSISDN = null;
	private String name   = null;
	private int    privelege;
	private String type = null;
	private String serviceActNum = null;
	private String packageName = "";
	private int lineType = 0;
	
	
    public int getLineType() {
		return lineType;
	}
	public void setLineType(int lineType) {
		this.lineType = lineType;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	/**
     * @return Returns the type.
     */
    public String getType() {
        return type;
    }
    /**
     * @param type The type to set.
     */
    public void setType(String type) {
        this.type = type;
    }
	/**
	 * @return Returns the mSISDN.
	 */
	public String getMSISDN() {
		return mSISDN;
	}
	/**
	 * @param msisdn The mSISDN to set.
	 */
	public void setMSISDN(String msisdn) {
		this.mSISDN = msisdn;
	}
	/**
	 * @return Returns the name.
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return Returns the privelege.
	 */
	public int getPrivelege() {
		return privelege;
	}
	/**
	 * @param privelege The privelege to set.
	 */
	public void setPrivelege(int privelege) {
		this.privelege = privelege;
	}
	public String getServiceActNum() {
		return serviceActNum;
	}
	public void setServiceActNum(String serviceActNum) {
		this.serviceActNum = serviceActNum;
	}
	
}

/*
 * Created on Jun 17, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PandaEmailVO {
	
	private String corporateId = "";
	private String distributedMinutes = "";
	private String availableMinutes = "";
	private String errorCode = "";
	
	

	/**
	 * @return Returns the availableMinutes.
	 */
	public String getAvailableMinutes() {
		return availableMinutes;
	}
	/**
	 * @param availableMinutes The availableMinutes to set.
	 */
	public void setAvailableMinutes(String availableMinutes) {
		this.availableMinutes = availableMinutes;
	}
	/**
	 * @return Returns the corporateId.
	 */
	public String getCorporateId() {
		return corporateId;
	}
	/**
	 * @param corporateId The corporateId to set.
	 */
	public void setCorporateId(String corporateId) {
		this.corporateId = corporateId;
	}
	/**
	 * @return Returns the distributedMinutes.
	 */
	public String getDistributedMinutes() {
		return distributedMinutes;
	}
	/**
	 * @param distributedMinutes The distributedMinutes to set.
	 */
	public void setDistributedMinutes(String distributedMinutes) {
		this.distributedMinutes = distributedMinutes;
	}
	/**
	 * @return Returns the errorCode.
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode The errorCode to set.
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
}

package sa.com.mobily.eportal.common.service.vo;

import java.sql.Timestamp;
import java.util.List;


public class PandaHistoryVO {
	
	private String channelTransId = "";
	private String profileId = "";
	private Timestamp distributionTime;
	private String status = "";
	private String description = "";
	private Timestamp completionTime;
	private List historyDetailVOList;
	private String totalUnconsumedMinutes;
	private String totalDistributedMiutes;
	private String srDate;
	
	private String billingAccountNumber = "";
	private String totalContractMinutes;
	private String errorCode = "";
	private String contractEndDate = "";
	
	private String currentBalMins;
	private String distributedMinutes;
	
	private String corporateId = "";
	
	
	
	public String getCorporateId() {
		return corporateId;
	}
	public void setCorporateId(String corporateId) {
		this.corporateId = corporateId;
	}
	/**
	 * @return Returns the currentBalMins.
	 */
	public String getCurrentBalMins() {
		return currentBalMins;
	}
	/**
	 * @param currentBalMins The currentBalMins to set.
	 */
	public void setCurrentBalMins(String currentBalMins) {
		this.currentBalMins = currentBalMins;
	}
	/**
	 * @return Returns the distributedMinutes.
	 */
	public String getDistributedMinutes() {
		return distributedMinutes;
	}
	/**
	 * @param distributedMinutes The distributedMinutes to set.
	 */
	public void setDistributedMinutes(String distributedMinutes) {
		this.distributedMinutes = distributedMinutes;
	}
	/**
	 * @return Returns the contractEndDate.
	 */
	public String getContractEndDate() {
		return contractEndDate;
	}
	/**
	 * @param contractEndDate The contractEndDate to set.
	 */
	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	/**
	 * @return Returns the errorCode.
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode The errorCode to set.
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return Returns the totalContractMinutes.
	 */
	public String getTotalContractMinutes() {
		return totalContractMinutes;
	}
	/**
	 * @param totalContractMinutes The totalContractMinutes to set.
	 */
	public void setTotalContractMinutes(String totalContractMinutes) {
		this.totalContractMinutes = totalContractMinutes;
	}
	/**
	 * @return Returns the billingAccountNumber.
	 */
	public String getBillingAccountNumber() {
		return billingAccountNumber;
	}
	/**
	 * @param billingAccountNumber The billingAccountNumber to set.
	 */
	public void setBillingAccountNumber(String billingAccountNumber) {
		this.billingAccountNumber = billingAccountNumber;
	}
	/**
	 * @return Returns the srDate.
	 */
	public String getSrDate() {
		return srDate;
	}
	/**
	 * @param srDate The srDate to set.
	 */
	public void setSrDate(String srDate) {
		this.srDate = srDate;
	}
	/**
	 * @return Returns the totalDistributedMiutes.
	 */
	public String getTotalDistributedMiutes() {
		return totalDistributedMiutes;
	}
	/**
	 * @param totalDistributedMiutes The totalDistributedMiutes to set.
	 */
	public void setTotalDistributedMiutes(String totalDistributedMiutes) {
		this.totalDistributedMiutes = totalDistributedMiutes;
	}
	/**
	 * @return Returns the totalUnconsumedMinutes.
	 */
	public String getTotalUnconsumedMinutes() {
		return totalUnconsumedMinutes;
	}
	/**
	 * @param totalUnconsumedMinutes The totalUnconsumedMinutes to set.
	 */
	public void setTotalUnconsumedMinutes(String totalUnconsumedMinutes) {
		this.totalUnconsumedMinutes = totalUnconsumedMinutes;
	}
	/**
	 * @return Returns the historyDetailVOList.
	 */
	public List getHistoryDetailVOList() {
		return historyDetailVOList;
	}
	/**
	 * @param historyDetailVOList The historyDetailVOList to set.
	 */
	public void setHistoryDetailVOList(List historyDetailVOList) {
		this.historyDetailVOList = historyDetailVOList;
	}
	/**
	 * @return Returns the channelTransId.
	 */
	public String getChannelTransId() {
		return channelTransId;
	}
	/**
	 * @param channelTransId The channelTransId to set.
	 */
	public void setChannelTransId(String channelTransId) {
		this.channelTransId = channelTransId;
	}
	/**
	 * @return Returns the completionTime.
	 */
	public Timestamp getCompletionTime() {
		return completionTime;
	}
	/**
	 * @param completionTime The completionTime to set.
	 */
	public void setCompletionTime(Timestamp completionTime) {
		this.completionTime = completionTime;
	}
	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return Returns the distributionTime.
	 */
	public Timestamp getDistributionTime() {
		return distributionTime;
	}
	/**
	 * @param distributionTime The distributionTime to set.
	 */
	public void setDistributionTime(Timestamp distributionTime) {
		this.distributionTime = distributionTime;
	}
	/**
	 * @return Returns the profileId.
	 */
	public String getProfileId() {
		return profileId;
	}
	/**
	 * @param profileId The profileId to set.
	 */
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}

/*
 * Created on Feb 15, 2012
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;


/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PartyIdentificationVO extends BaseVO {
	
	private String companyIDNumber = null;

	public String getCompanyIDNumber() {
		return companyIDNumber;
	}

	public void setCompanyIDNumber(String companyIDNumber) {
		this.companyIDNumber = companyIDNumber;
	}
	
	
}

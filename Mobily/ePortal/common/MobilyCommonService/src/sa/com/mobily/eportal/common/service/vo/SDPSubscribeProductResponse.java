package sa.com.mobily.eportal.common.service.vo;

public class SDPSubscribeProductResponse {

    private String result;
    private String resultDescription;
    
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getResultDescription() {
		return resultDescription;
	}
	public void setResultDescription(String resultDescription) {
		this.resultDescription = resultDescription;
	}
}

/*
 * Created on Feb 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SRMessageHeaderVO extends BaseVO {
    private String returnCode;
	private String msgFormat;
	private String funcID;
	private String msgVersion;
	private String requestorChannelID;
	private String serviceRequestId;
	private Date   srDate;
	private String requestorUserID;
	private String requestorLanguage;
	private String securityKey;
	private String srStatus;
	private Date srRcvDate;
	private String errorCode = "0000";
	private String overwriteOpenOrder;
	private String Chargeable;
	private String ChargeAmount;
	private String chargingMode ;
	
	private String replierChannelId;
	/**
	 * @return Returns the funcID.
	 */
	public String getFuncID() {
		return funcID;
	}
	/**
	 * @param funcID The funcID to set.
	 */
	public void setFuncID(String funcID) {
		this.funcID = funcID;
	}
	/**
	 * @return Returns the msgVersion.
	 */
	public String getMsgVersion() {
		return msgVersion;
	}
	/**
	 * @param msgVersion The msgVersion to set.
	 */
	public void setMsgVersion(String msgVersion) {
		this.msgVersion = msgVersion;
	}
	/**
	 * @return Returns the requestorChannelID.
	 */
	public String getRequestorChannelID() {
		return requestorChannelID;
	}
	/**
	 * @param requestorChannelID The requestorChannelID to set.
	 */
	public void setRequestorChannelID(String requestorChannelID) {
		this.requestorChannelID = requestorChannelID;
	}
	/**
	 * @return Returns the requestorLanguage.
	 */
	public String getRequestorLanguage() {
		return requestorLanguage;
	}
	/**
	 * @param requestorLanguage The requestorLanguage to set.
	 */
	public void setRequestorLanguage(String requestorLanguage) {
		this.requestorLanguage = requestorLanguage;
	}
	/**
	 * @return Returns the requestorUserID.
	 */
	public String getRequestorUserID() {
		return requestorUserID;
	}
	/**
	 * @param requestorUserID The requestorUserID to set.
	 */
	public void setRequestorUserID(String requestorUserID) {
		this.requestorUserID = requestorUserID;
	}
	/**
	 * @return Returns the securityKey.
	 */
	public String getSecurityKey() {
		return securityKey;
	}
	/**
	 * @param securityKey The securityKey to set.
	 */
	public void setSecurityKey(String securityKey) {
		this.securityKey = securityKey;
	}
	/**
	 * @return Returns the srDate.
	 */
	public Date getSrDate() {
		return srDate;
	}
	/**
	 * @param srDate The srDate to set.
	 */
	public void setSrDate(String srDate) {
	    SimpleDateFormat tempSimpleDateFormat = new SimpleDateFormat( ConstantIfc.DATE_FORMAT );
        try
        {
            this.srDate = tempSimpleDateFormat.parse(srDate);
        }
        catch(ParseException tempParseException)
        {
            this.srDate = null;
        }

		
	}
	/**
	 * @return Returns the srRcvDate.
	 */
	public Date getSrRcvDate() {
		return srRcvDate;
	}
	/**
	 * @param srRcvDate The srRcvDate to set.
	 */
	public void setSrRcvDate(String srRcvDate) {
	    SimpleDateFormat tempSimpleDateFormat = new SimpleDateFormat( ConstantIfc.DATE_FORMAT );
        try
        {
            this.srRcvDate = tempSimpleDateFormat.parse(srRcvDate);
        }
        catch(ParseException tempParseException)
        {
            this.srRcvDate = null;
        }

	}
	/**
	 * @return Returns the srStatus.
	 */
	public String getSrStatus() {
		return srStatus;
	}
	/**
	 * @param srStatus The srStatus to set.
	 */
	public void setSrStatus(String srStatus) {
		this.srStatus = srStatus;
	}
	/**
	 * @return Returns the errorCode.
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode The errorCode to set.
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return Returns the returnCode.
	 */
	public String getReturnCode() {
		return returnCode;
	}
	/**
	 * @param returnCode The returnCode to set.
	 */
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	/**
	 * @return Returns the msgFormat.
	 */
	public String getMsgFormat() {
		return msgFormat;
	}
	/**
	 * @param msgFormat The msgFormat to set.
	 */
	public void setMsgFormat(String msgFormat) {
		this.msgFormat = msgFormat;
	}
	/**
	 * @return Returns the chargeable.
	 */
	public String getChargeable() {
		return Chargeable;
	}
	/**
	 * @param chargeable The chargeable to set.
	 */
	public void setChargeable(String chargeable) {
		Chargeable = chargeable;
	}
	/**
	 * @return Returns the chargeAmount.
	 */
	public String getChargeAmount() {
		return ChargeAmount;
	}
	/**
	 * @param chargeAmount The chargeAmount to set.
	 */
	public void setChargeAmount(String chargeAmount) {
		ChargeAmount = chargeAmount;
	}
	/**
	 * @return Returns the overwriteOpenOrder.
	 */
	public String getOverwriteOpenOrder() {
		return overwriteOpenOrder;
	}
	/**
	 * @param overwriteOpenOrder The overwriteOpenOrder to set.
	 */
	public void setOverwriteOpenOrder(String overwriteOpenOrder) {
		this.overwriteOpenOrder = overwriteOpenOrder;
	}
	/**
	 * @return Returns the replierChannelId.
	 */
	public String getReplierChannelId() {
		return replierChannelId;
	}
	/**
	 * @param replierChannelId The replierChannelId to set.
	 */
	public void setReplierChannelId(String replierChannelId) {
		this.replierChannelId = replierChannelId;
	}
	/**
	 * @param srDate The srDate to set.
	 */
	public void setSrDate(Date srDate) {
		this.srDate = srDate;
	}
	/**
	 * @param srRcvDate The srRcvDate to set.
	 */
	public void setSrRcvDate(Date srRcvDate) {
		this.srRcvDate = srRcvDate;
	}
	/**
	 * @return Returns the chargingMode.
	 */
	public String getChargingMode() {
		return chargingMode;
	}
	/**
	 * @param chargingMode The chargingMode to set.
	 */
	public void setChargingMode(String chargingMode) {
		this.chargingMode = chargingMode;
	}
	/**
	 * @return Returns the serviceRequestId.
	 */
	public String getServiceRequestId() {
		return serviceRequestId;
	}
	/**
	 * @param serviceRequestId The serviceRequestId to set.
	 */
	public void setServiceRequestId(String serviceRequestId) {
		this.serviceRequestId = serviceRequestId;
	}
}

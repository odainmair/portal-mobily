/*
 * Created on Jul 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.io.Serializable;

/**
 * @author g.krishnamurthy.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ServiceVO implements Serializable,  Cloneable  {
	
	private String ServiceName;
	private int Status;

	/**
	 * @return Returns the serviceName.
	 */
	public String getServiceName() {
		return ServiceName;
	}
	/**
	 * @param serviceName The serviceName to set.
	 */
	public void setServiceName(String serviceName) {
		ServiceName = serviceName;
	}
	/**
	 * @return Returns the status.
	 */
	public int getStatus() {
		return Status;
	}
	/**
	 * @param status The status to set.
	 */
	public void setStatus(int status) {
		Status = status;
	}
}

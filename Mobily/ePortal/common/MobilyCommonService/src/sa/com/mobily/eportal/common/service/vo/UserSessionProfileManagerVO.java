
package sa.com.mobily.eportal.common.service.vo;


public class UserSessionProfileManagerVO {

	
	String msisdn;
	String customerAccountID;
	String activeMsisdn;
	String serviceLineID;
	String activeServiceLineID;
	String billAccountID;
	String activeBillAccountID;
	/**
	 * @return Returns the activeBillAccountID.
	 */
	public String getActiveBillAccountID() {
		return activeBillAccountID;
	}
	/**
	 * @param activeBillAccountID The activeBillAccountID to set.
	 */
	public void setActiveBillAccountID(String activeBillAccountID) {
		this.activeBillAccountID = activeBillAccountID;
	}
	/**
	 * @return Returns the activeMsisdn.
	 */
	public String getActiveMsisdn() {
		return activeMsisdn;
	}
	/**
	 * @param activeMsisdn The activeMsisdn to set.
	 */
	public void setActiveMsisdn(String activeMsisdn) {
		this.activeMsisdn = activeMsisdn;
	}
	/**
	 * @return Returns the activeServiceLineID.
	 */
	public String getActiveServiceLineID() {
		return activeServiceLineID;
	}
	/**
	 * @param activeServiceLineID The activeServiceLineID to set.
	 */
	public void setActiveServiceLineID(String activeServiceLineID) {
		this.activeServiceLineID = activeServiceLineID;
	}
	/**
	 * @return Returns the billAccountID.
	 */
	public String getBillAccountID() {
		return billAccountID;
	}
	/**
	 * @param billAccountID The billAccountID to set.
	 */
	public void setBillAccountID(String billAccountID) {
		this.billAccountID = billAccountID;
	}
	/**
	 * @return Returns the customerAccountID.
	 */
	public String getCustomerAccountID() {
		return customerAccountID;
	}
	/**
	 * @param customerAccountID The customerAccountID to set.
	 */
	public void setCustomerAccountID(String customerAccountID) {
		this.customerAccountID = customerAccountID;
	}
	/**
	 * @return Returns the msisdn.
	 */
	public String getMsisdn() {
		return msisdn;
	}
	/**
	 * @param msisdn The msisdn to set.
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	/**
	 * @return Returns the serviceLineID.
	 */
	public String getServiceLineID() {
		return serviceLineID;
	}
	/**
	 * @param serviceLineID The serviceLineID to set.
	 */
	public void setServiceLineID(String serviceLineID) {
		this.serviceLineID = serviceLineID;
	}
}

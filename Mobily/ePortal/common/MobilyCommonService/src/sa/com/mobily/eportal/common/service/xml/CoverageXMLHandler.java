/**
 * 
 */
package sa.com.mobily.eportal.common.service.xml;

import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.CoverageVO;
import sa.com.mobily.eportal.common.service.vo.DestinationTypeVO;

import com.mobily.exception.xml.XMLException;

/**
 * @author r.bandi.mit
 *
 */
public class CoverageXMLHandler {
	
	private static final Logger log = LoggerInterface.log;
	/*
	 <MOBILY_BSL_SR>
		<SR_HEADER>
			<MsgFormat>GIS Coverage</MsgFormat>
			<MsgVersion>0</MsgVersion>
			<RequestorChannelId>ePortal</RequestorChannelId>
			<RequestorChannelFunction>GIS COVERAGE</RequestorChannelFunction>
			<RequestorSecurityInfo>Secure</RequestorSecurityInfo>
			<ChannelTransactionId></ChannelTransactionId>
			<SrDate>20091125001911</SrDate>
		</SR_HEADER>
		<Sr_Info>
			<Location>
				<LongitudeX>46.716667</LongitudeX>
				<LatitudeY>24.633333</LatitudeY>
				<Type>2</Type>
			</Location>
			<Layer>1</Layer>
			<Mode>1</Mode>
		</Sr_Info>
	</MOBILY_BSL_SR>
	
	 */
	public String generateRequestXMLMessage(DestinationTypeVO detailsVO) throws MobilyCommonException 
	{
		log.debug("WiamxCoverageXMLHandler > generateRequestXMLMessage > start");
		String xmlRequest = "";
		try {
			Document doc = new DocumentImpl();
			String srDate =MobilyUtility.FormateDate(new Date());
	        Element root = doc.createElement(TagIfc.TAG_WCRG_MOBILY_BSL_SR);

	        //	      Generate Header element
	        Element header = XMLUtility.openParentTag(doc,TagIfc.TAG_WCRG_SR_HEADER);
	        XMLUtility.generateElelemt(doc, header, TagIfc.TAG_WCRGM_MESSAGE_FORAMT,ConstantIfc.MESSAGE_FORMAT_VALUE);
	        XMLUtility.generateElelemt(doc, header, TagIfc.TAG_WCRGM_MESSAGE_VERSION,ConstantIfc.WIMAX_MESSAGE_VERSION_VALUE);
	        
	        
	        XMLUtility.generateElelemt(doc, header, TagIfc.TAG_WCRGM_REQUETOR_CHANNEL_ID,ConstantIfc.CHANNEL_ID_VALUE);
	        XMLUtility.generateElelemt(doc, header,TagIfc.TAG_WCRGM_REQUETOR_CHANNEL_FUNCTION, ConstantIfc.REQUEST_CHANNEL_FUNC_VALUE);
	        XMLUtility.generateElelemt(doc, header,TagIfc.TAG_WCRGM_REQUETOR_SECURITY_INFO, ConstantIfc.SECURITY_VALUE);
	        XMLUtility.generateElelemt(doc, header,TagIfc.TAG_WCRGM_CHANNEL_TRANSACTION_ID,srDate);//value have to check
	        XMLUtility.generateElelemt(doc, header,TagIfc.TAG_WCRGM_SR_DATE,srDate);
	        XMLUtility.closeParentTag(root, header);
	        
	        Element serviceinfo = XMLUtility.openParentTag(doc,TagIfc.TAG_WCRGM_SR_INFO);
	        Element location = XMLUtility.openParentTag(doc,TagIfc.TAG_WCRGM__LOCATION);
	        log.debug("WiamxCoverageXMLHandler > generateRequestXMLMessage > Logitude:"+detailsVO.getLongitude());
	        XMLUtility.generateElelemt(doc, location,TagIfc.TAG_WCRGM_LONGITUDE,detailsVO.getLongitude());
	        log.debug("WiamxCoverageXMLHandler > generateRequestXMLMessage > Latitude:"+detailsVO.getLatitude());
	        XMLUtility.generateElelemt(doc, location,TagIfc.TAG_WCRGM_LATITUDE,detailsVO.getLatitude());
	        log.debug("WiamxCoverageXMLHandler > generateRequestXMLMessage > Type:"+detailsVO.getType());
	        XMLUtility.generateElelemt(doc, location,TagIfc.TAG_WCRGM_TYPE,detailsVO.getType());
	        XMLUtility.closeParentTag(serviceinfo, location);
	        XMLUtility.generateElelemt(doc, serviceinfo,TagIfc.TAG_WCRGM_LAYER,detailsVO.getLayer());
	        XMLUtility.generateElelemt(doc, serviceinfo,TagIfc.TAG_WCRGM_MODE,ConstantIfc.LAYER_MODE);  
	        XMLUtility.closeParentTag(root,serviceinfo);
	       
	        doc.appendChild(root);
			xmlRequest = XMLUtility.serializeRequest(doc);
			log.debug("WiamxCoverageXMLHandler > generateRequestXMLMessage >  XML generated :"+xmlRequest);
	        
	}catch (Exception e) {
		log.fatal("WiamxCoverageXMLHandler > generateRequestXMLMessage > Exception ," + e.getMessage());
		throw new MobilyCommonException(e);
	}
	log.debug("WiamxCoverageXMLHandler > generateRequestXMLMessage > end");
		return xmlRequest;
		
	}	

	
	/**
	 * 
	 * @param replyXMLMessage
	 * @return
	 * @throws MobilyCommonException
	 */
	public CoverageVO parsingReplyXMLMessage(String replyXMLMessage) throws MobilyCommonException{
		log.debug("CoverageXMLHandler > parsingReplyXMLMessage > Start");
		CoverageVO objCoverageVO = null;

		try{
			objCoverageVO = new CoverageVO();
			//objRechargeReplyHeaderVO = new RechargeReplyHeaderVO();
       	    Element objNode = null;
		    String objTagName = null;
		    String objNodeValue = null;
		    Node objRootNode = null;
		    NodeList objParentNodeList = null;
		    NodeList objChildList = null;
		    Element objChildNode = null;
	        String objChildNodeTagName = null;
	        String objChildNodeValue = null;
	        NodeList objSubChildList = null;
		    Element objSubChildNode = null;
	        String objSubChildNodeTagName = null;
	        String objSubChildNodeValue = null;
	        XMLException objXMLException = null;
	        
	        Document objDocument = XMLUtility.parseXMLString(replyXMLMessage);
	        objRootNode = objDocument.getElementsByTagName(TagIfc.TAG_WCRG_MOBILY_BSL_SR_REPLY).item(0);
	        objParentNodeList = objRootNode.getChildNodes(); 
	        
	        for( int i = 0; i < objParentNodeList.getLength(); i++ ){
            	if( (objParentNodeList.item(i)).getNodeType() !=  Node.ELEMENT_NODE)
            		continue;
            	
            	objNode = (Element)objParentNodeList.item(i);
            	objTagName = objNode.getTagName();
            	
//            	Getting the header tag values
            	if(objNode.getFirstChild() != null){    
            		if( objTagName.equalsIgnoreCase(TagIfc.TAG_WCRG_SR_HEADER_REPLY)){
            			
            			objNodeValue = objNode.getFirstChild().getNodeValue();   
            			objChildList = objNode.getChildNodes();
            			
            			for(int j = 0; j < objChildList.getLength(); j++){
            				if((objChildList.item(j)).getNodeType() != Node.ELEMENT_NODE)
            					continue;
            				
            				objChildNode = (Element)objChildList.item(j);
            				objChildNodeTagName = objChildNode.getTagName();
            				objChildNodeValue = "";
            				if(objChildNode.getFirstChild() != null )
						        objChildNodeValue = objChildNode.getFirstChild().getNodeValue();
						    if(objChildNodeTagName.equalsIgnoreCase(TagIfc.TAG_WCRGM_MESSAGE_FORAMT))
						    	objCoverageVO.setMsgFormat(objChildNodeValue);
						    else if(objChildNodeTagName.equalsIgnoreCase(TagIfc.TAG_WCRGM_MESSAGE_VERSION))
						    	objCoverageVO.setMsgVersion(objChildNodeValue);
						    else if(objChildNodeTagName.equalsIgnoreCase(TagIfc.TAG_WCRGM_REQUETOR_CHANNEL_ID))
						    	objCoverageVO.setRequestorChannelId(objChildNodeValue);
						    else if(objChildNodeTagName.equalsIgnoreCase(TagIfc.TAG_WCRGM_REQUETOR_CHANNEL_FUNCTION))
						    	objCoverageVO.setRequestorChannelFunction(objChildNodeValue);
						    else if(objChildNodeTagName.equalsIgnoreCase(TagIfc.TAG_WCRGM_REQUETOR_SECURITY_INFO))
						    	objCoverageVO.setRequestorSecurityInfo(objChildNodeValue);
						    else if(objChildNodeTagName.equalsIgnoreCase(TagIfc.TAG_WCRGM_CHANNEL_TRANSACTION_ID))
						    	objCoverageVO.setChannelTransactionId(objChildNodeValue);
						    else if(objChildNodeTagName.equalsIgnoreCase(TagIfc.TAG_WCRGM_SR_DATE))
						    	objCoverageVO.setSrDate(objChildNodeValue);
						    else if(objChildNodeTagName.equalsIgnoreCase(TagIfc.TAG_WCRG_ERROR_CODE))
						    	objCoverageVO.setErrorCode(objChildNodeValue);
						    else if(objChildNodeTagName.equalsIgnoreCase(TagIfc.TAG_WCRG_ERROR_MESSAGE))
						    	objCoverageVO.setErrorMessage(objChildNodeValue);
						   }
            			
            			//objRechargeReplyVO.setHeader(objRechargeReplyHeaderVO);
	                	
    	             }else if(objTagName.equalsIgnoreCase(TagIfc.TAG_WCRG_COVERAGE)){ //setting "coverage"
		            	objNodeValue = objNode.getFirstChild().getNodeValue();
		            	objCoverageVO.setCoverage(objNodeValue);
    	             }else if(objTagName.equalsIgnoreCase(TagIfc.TAG_WCRG_ODBNUMBER)){ //setting "ODBNUMBER"
		            	objNodeValue = objNode.getFirstChild().getNodeValue();
		            	objCoverageVO.setOdbnumber(objNodeValue);
    	             }else if(objTagName.equalsIgnoreCase(TagIfc.TAG_COVERAGE_RFS)){ //setting "RFS"
		            	objNodeValue = objNode.getFirstChild().getNodeValue();
		            	objCoverageVO.setRfs(objNodeValue);
    	             }
            	}
            	
            }
	        
      } catch (Exception e) {
			log.fatal("WiamxCoverage > CoverageXMLHandler > parsingReplyXMLMessage > Exception ," + e.getMessage());
				throw new MobilyCommonException(e);
      }	
      log.debug("WiamxCoverage > CoverageXMLHandler > parsingReplyXMLMessage > end");
      return objCoverageVO;
	}

}





/*
 * Created on Feb 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.xml;

import java.io.IOException;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.ErrorIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.CustomerStatusVO;
import sa.com.mobily.eportal.common.service.vo.SRMessageHeaderVO;

import com.mobily.exception.xml.XMLException;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CustomerStatusXMLHandler {
    private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;
    
	/**
	 * Generate Customer type inquiry request message
	 * @param requestVO
	 * @return
	 * @throws MobilyCommonException
	 * Feb 18, 2008
	 * CustomerStatusXMLHandler.java
	 * msayed
	 */
	public String generateXMLRequest(CustomerStatusVO requestVO) throws MobilyCommonException{
		 String requestMessage = "";
		 try{
		     log.debug("CustomerStatusXMLHandler > generateXMLRequest > for Customer  ["+requestVO.getLineNumber()+"] Called");
		 	if(requestVO.getLineNumber() == null || requestVO.getLineNumber().equalsIgnoreCase("")){
                log.fatal("CustomerStatusXMLHandler > generate XML Message > Number Not Found....");
                throw new MobilyCommonException(ErrorIfc.NUMBER_NOT_FOUND_ECEPTION);
		 	}
			Document doc= new DocumentImpl();
            Element root = doc.createElement(TagIfc.TAG_MOBILY_BSL_SR);
            Element header = XMLUtility.openParentTag(doc,TagIfc.TAG_SR_HEADER);
            	XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID,ConstantIfc.CUSTSTATUS_FUNC_ID_VALUE);
            	XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SECURITY_KEY,ConstantIfc.SEC_KEY_VALUE);
            	XMLUtility.generateElelemt(doc, header,TagIfc.MSG_VERSION, ConstantIfc.MESSAGE_VERSION_VALUE);
            	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_CHANNEL_ID,ConstantIfc.CHANNEL_ID_VALUE);
            	XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SR_DATE,MobilyUtility.FormateDate(new Date()));
            	XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_USER_ID,requestVO.getRequesterUserId());
            	XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_LANG,ConstantIfc.LANG_VALUE);
            	
            	XMLUtility.generateElelemt(doc, header,TagIfc.TAG_CHARGEABLE,ConstantIfc.CUSTSTATUS_CHARGABLE_VALUE);
            	XMLUtility.generateElelemt(doc, header, TagIfc.TAG_CHARGING_MODE,ConstantIfc.CUSTSTATUS_CHARGING_MODE);
            	XMLUtility.generateElelemt(doc, header, TagIfc.TAG_CHARGE_AMOUNT,ConstantIfc.CUSTSTATUS_CHARGE_AMOUNT);
            XMLUtility.closeParentTag(root, header);
            //body
            XMLUtility.generateElelemt(doc, root, TagIfc.CC_STATUS_ONLY, requestVO.getStatusOnly());
            XMLUtility.generateElelemt(doc, root, TagIfc.LINE_NUMBER, requestVO.getLineNumber());
            doc.appendChild(root);
            requestMessage = XMLUtility.serializeRequest(doc);


		 	} catch (java.io.IOException e) {
	            log.fatal("CustomerStatusXMLHandler > generateXMLRequest >IOException > " + e.getMessage());
	            throw new SystemException(e);
	        } catch (Exception e) {
	            log.fatal("CustomerStatusXMLHandler > generateXMLRequest > Exception > "+ e.getMessage());
	            throw new SystemException(e);
	        }
	        log.debug("CustomerStatusXMLHandler > generateXMLRequest > for Customer  ["+requestVO.getLineNumber()+"]"+requestMessage);
		 return requestMessage;
		}
		
		/**
		 * parsing xml reply message
		 * @return CustomerStatusReplyVO
		 * @throws IOException
		 * @throws XMLException
		 */
		public CustomerStatusVO parsingXMLReplyMessage(String replyMessage)throws MobilyCommonException{
		    CustomerStatusVO replyBean = null;
		    log.debug("CustomerStatusXMLHandler > parsingXMLRreply >  for XML ["+replyMessage+"] Called");
			if(replyMessage == null || replyMessage =="")
				return replyBean;
			try{
				 replyBean = new CustomerStatusVO();
				 Document doc = XMLUtility.parseXMLString(replyMessage);
				 
				 int CustomerType = 0;
				 Node rootNode = doc.getElementsByTagName(TagIfc.TAG_MOBILY_BSL_SR_REPLY).item(0);
				 NodeList nl	 = rootNode.getChildNodes(); 
				 
				 for(int j = 0; j < nl.getLength(); j++){
	                if((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
	                	continue;
	                
	                Element l_Node       = (Element)nl.item(j);
	                String p_szTagName   = l_Node.getTagName();
	                String l_szNodeValue = null;
	                if( l_Node.getFirstChild() != null )
	                	l_szNodeValue 	 =l_Node.getFirstChild().getNodeValue();
	                
	                if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_SR_HEADER_REPLY))
	                {
	                	MessageHeaderHandler headerParser = new MessageHeaderHandler();
	                	SRMessageHeaderVO Header = headerParser.parsingSRXMLHeaderReplyMessage(l_Node);
	                	if(Header.getReturnCode() != null){
	                	    if(Integer.parseInt(Header.getReturnCode()) > 0){
	                	        throw new MobilyCommonException(Header.getReturnCode());    
	                	    }
	                	}
	                }else if(p_szTagName.equalsIgnoreCase(TagIfc.LINE_NUMBER)){
	                	if(l_szNodeValue != null)
	                		replyBean.setLineNumber(l_szNodeValue);
	                }
	                else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_CODE)){
	                	if(l_szNodeValue != null){
	                		replyBean.setErrorCode(l_szNodeValue);
	                		if(Integer.parseInt(l_szNodeValue) > 0){
	                	        throw new MobilyCommonException(l_szNodeValue);    
	                	    }
	                	}
	                }
	                else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_MSG)){
	                	if(l_szNodeValue != null)
	                		replyBean.setErrorMsg(l_szNodeValue);
	                }
	                else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_STATUS)){
	                	if(l_szNodeValue != null)
	                		replyBean.setStatus(Integer.parseInt(l_szNodeValue));
	                }
	                else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_REASON)){
	                	if(l_szNodeValue != null)
	                		replyBean.setReason(Integer.parseInt(l_szNodeValue));
	                }
	 			 }
				}
			catch(Exception e){
			    log.fatal("CustomerStatusXMLHandler > parsingXMLReplyMessage >Exception > " + e.getMessage());
				throw new SystemException(e);
			}		
			
			
			return replyBean;
		}

    public static void main(String[] args) {
        CustomerStatusXMLHandler handler = new CustomerStatusXMLHandler();
        try {
            handler.parsingXMLReplyMessage("<MOBILY_BSL_SR_REPLY><SR_HEADER_REPLY><FuncId>CUST_INQ</FuncId><MsgVersion>0000</MsgVersion><RequestorChannelId>BSL</RequestorChannelId><SrDate>20080819090924</SrDate><SrRcvDate>20080819091035</SrRcvDate><SrStatus>6</SrStatus></SR_HEADER_REPLY><ErrorMsg/><ErrorCode>1000</ErrorCode><LineNumber>966560513502</LineNumber><Status>1</Status><Reason></Reason></MOBILY_BSL_SR_REPLY>");
            //System.out.println("error");
        } catch (MobilyCommonException e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
        }
    }
}

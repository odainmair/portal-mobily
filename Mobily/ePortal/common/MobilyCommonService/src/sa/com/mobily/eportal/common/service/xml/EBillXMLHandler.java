/*
 * Created on Jan 6, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.xml;
import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;



import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;

import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.EBillReplyHeaderVO;
import sa.com.mobily.eportal.common.service.vo.EBillReplyVO;
import sa.com.mobily.eportal.common.service.vo.EBillRequestVO;



/**
 * @author r.bandi.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class EBillXMLHandler implements TagIfc {
	  private static final Logger log = LoggerInterface.log;
	public String generateRequestXMLMessage(EBillRequestVO objEBillRequestVO) throws MobilyCommonException 
	{
		log.debug("EBillXMLHandler > generateRequestXMLMessage > Start1");
		String requestXMLMessage = null;
        Document objDocument = new DocumentImpl();
        
        Element Element_TAG_EI_MOBILY_BSL_SR = objDocument.createElement(TAG_EI_MOBILY_BSL_SR);
        Element Element_TAG_EI_SR_HEADER = objDocument.createElement(TAG_EI_SR_HEADER);
        Element Element_TAG_EI_FuncId = objDocument.createElement(TAG_EI_FuncId);
        log.debug("EBillXMLHandler > generateRequestXMLMessage > Start2");
        Element Element_TAG_EI_SecurityKey = objDocument.createElement(TAG_EI_SecurityKey);
        Element Element_TAG_EI_MsgVersion = objDocument.createElement(TAG_EI_MsgVersion);
        Element Element_TAG_EI_RequestorChannelId = objDocument.createElement(TAG_EI_RequestorChannelId);
        Element Element_TAG_EI_SrDate = objDocument.createElement(TAG_EI_SrDate);
        Element Element_TAG_EI_RequestorUserId = objDocument.createElement(TAG_EI_RequestorUserId);
        Element Element_TAG_EI_RequestorLanguage = objDocument.createElement(TAG_EI_RequestorLanguage);
        Element Element_TAG_EI_LineNumber = objDocument.createElement(TAG_EI_LineNumber);
        //	Write Header Elements data
        log.debug("EBillXMLHandler > generateRequestXMLMessage > Start3");
        Element_TAG_EI_FuncId.appendChild(objDocument.createTextNode(objEBillRequestVO.getHeader().getFuncId()));
        Element_TAG_EI_SecurityKey.appendChild(objDocument.createTextNode(objEBillRequestVO.getHeader().getSecurityKey()));
        Element_TAG_EI_MsgVersion.appendChild(objDocument.createTextNode(objEBillRequestVO.getHeader().getMsgVersion()));
        Element_TAG_EI_RequestorChannelId.appendChild(objDocument.createTextNode(objEBillRequestVO.getHeader().getRequestorChannelId()));
        Element_TAG_EI_SrDate.appendChild(objDocument.createTextNode(objEBillRequestVO.getHeader().getSrDate()));
        Element_TAG_EI_RequestorUserId.appendChild(objDocument.createTextNode(objEBillRequestVO.getHeader().getRequestorUserId()));
        Element_TAG_EI_RequestorLanguage.appendChild(objDocument.createTextNode(objEBillRequestVO.getHeader().getRequestorLanguage()));
        //	Add HEADER sub tags
        log.debug("EBillXMLHandler > generateRequestXMLMessage > Start4");
        Element_TAG_EI_SR_HEADER.appendChild(Element_TAG_EI_FuncId);
        Element_TAG_EI_SR_HEADER.appendChild(Element_TAG_EI_SecurityKey);
        Element_TAG_EI_SR_HEADER.appendChild(Element_TAG_EI_MsgVersion);
        Element_TAG_EI_SR_HEADER.appendChild(Element_TAG_EI_RequestorChannelId);
        Element_TAG_EI_SR_HEADER.appendChild(Element_TAG_EI_SrDate);
        Element_TAG_EI_SR_HEADER.appendChild(Element_TAG_EI_RequestorUserId);
        Element_TAG_EI_SR_HEADER.appendChild(Element_TAG_EI_RequestorLanguage);

        //	Add MESSAGE sub tag
        Element_TAG_EI_MOBILY_BSL_SR.appendChild(Element_TAG_EI_SR_HEADER);

        //	Write Element data
        Element_TAG_EI_LineNumber.appendChild(objDocument.createTextNode(objEBillRequestVO.getLineNumber()));

        //	Add MESSAGE sub tag
        Element_TAG_EI_MOBILY_BSL_SR.appendChild(Element_TAG_EI_LineNumber);

       // 	Add to Document
        objDocument.appendChild(Element_TAG_EI_MOBILY_BSL_SR);
        try {
        	//	Serialize
        	requestXMLMessage = XMLUtility.serializeRequest(objDocument);
        	  log.debug("EBillXMLHandler > generateRequestXMLMessage > XMLRequest: "+requestXMLMessage);
        }catch (Exception e) {
			log.fatal("ebill > EBillXMLHandler > generateRequestXMLMessage > Exception ," + e.getMessage());
			throw new MobilyCommonException(e);
        }
        log.debug("EBillXMLHandler > generateRequestXMLMessage > end");
        return requestXMLMessage;

	}
	public EBillReplyVO parsingReplyXMLMessage(String replyXMLMessage) throws MobilyCommonException{
		log.debug("EBillXMLHandler > parsingReplyXMLMessage > Start");
		EBillReplyVO objEBillReplyVO = new EBillReplyVO();
		EBillReplyHeaderVO objEBillReplyHeaderVO = new EBillReplyHeaderVO();

        Document objDocument = null;
        Element objNode = null;
	    String objTagName = null;
	    String objNodeValue = null;
	    
	    Node objRootNode = null;
	    NodeList objParentNodeList = null;
	    
	    NodeList objChildList = null;
	    Element objChildNode = null;
        String objChildNodeTagName = null;
        String objChildNodeValue = null;
        
        NodeList objSubChildList = null;
	    Element objSubChildNode = null;
        String objSubChildNodeTagName = null;
        String objSubChildNodeValue = null;
        
        //XMLException objXMLException = null;
	    //log.debug( "Parsing XML Reply Message .." );
        log.debug("EBillXMLHandler > parsingReplyXMLMessage > Start > before try");
	    try {
            //	Get the XML object from the Reply String
            objDocument = XMLUtility.parseXMLString(replyXMLMessage);
            log.debug("EBillXMLHandler > parsingReplyXMLMessage > Start> inside try");
        } catch (Exception e) {
			log.fatal("ebill > EBillXMLHandler > parsingReplyXMLMessage > Exception ," + e.getMessage());
			throw new MobilyCommonException(e);
        }

        //log.debug( "Constructing the Credit Cards Numbers Reply Object .." );
		objRootNode = objDocument.getElementsByTagName(TAG_EI_MOBILY_BSL_SR_REPLY).item(0);
		objParentNodeList = objRootNode.getChildNodes(); 

		for( int i = 0; i < objParentNodeList.getLength(); i++ ) {
		    if( (objParentNodeList.item(i)).getNodeType() !=  Node.ELEMENT_NODE)
		        continue;

		    objNode = (Element)objParentNodeList.item(i);
		    objTagName = objNode.getTagName();
		    //	Getting the header tag values
		    if(objNode.getFirstChild() != null){    
		        if( objTagName.equalsIgnoreCase(TAG_EI_SR_HEADER_REPLY)) {
		        	objNodeValue = objNode.getFirstChild().getNodeValue();   
			        objChildList = objNode.getChildNodes();

			        for(int j = 0; j < objChildList.getLength(); j++) {
			            if((objChildList.item(j)).getNodeType() != Node.ELEMENT_NODE)
			                continue;
				    
			            objChildNode = (Element)objChildList.item(j);
			            objChildNodeTagName = objChildNode.getTagName();
			            objChildNodeValue = "";
			            
					    if(objChildNode.getFirstChild() != null )
					        objChildNodeValue = objChildNode.getFirstChild().getNodeValue();

					    if(objChildNodeTagName.equalsIgnoreCase(TAG_EI_FuncId))
					    	objEBillReplyHeaderVO.setFuncId(objChildNodeValue);
					    
					    else if(objChildNodeTagName.equalsIgnoreCase(TAG_EI_MsgVersion))
					    	objEBillReplyHeaderVO.setMsgVersion(objChildNodeValue);
					    
					    else if(objChildNodeTagName.equalsIgnoreCase(TAG_EI_RequestorChannelId))
					    	objEBillReplyHeaderVO.setRequestorChannelId(objChildNodeValue);
					    
					    else if(objChildNodeTagName.equalsIgnoreCase(TAG_EI_SrDate))
					    	objEBillReplyHeaderVO.setSrDate(objChildNodeValue);
					    
					    else if(objChildNodeTagName.equalsIgnoreCase(TAG_EI_SrRcvDate))
					    	objEBillReplyHeaderVO.setSrRcvDate(objChildNodeValue);
					    
					    else if(objChildNodeTagName.equalsIgnoreCase(TAG_EI_SrStatus))
					    	objEBillReplyHeaderVO.setSrStatus(objChildNodeValue);
					    
			        }
			        objEBillReplyVO.setHeader(objEBillReplyHeaderVO);
			        
	            } else if(objTagName.equalsIgnoreCase(TAG_EI_ErrorMsg)) {
	            	objNodeValue = objNode.getFirstChild().getNodeValue();
	            	objEBillReplyVO.setErrorMsg(objNodeValue);
	            
	            } else if(objTagName.equalsIgnoreCase(TAG_EI_ErrorCode)) {
	            	objNodeValue = objNode.getFirstChild().getNodeValue();
	            	objEBillReplyVO.setErrorCode(objNodeValue);
	            
	            } else if(objTagName.equalsIgnoreCase(TAG_EI_LineNumber)) {
	            	objNodeValue = objNode.getFirstChild().getNodeValue();
	            	objEBillReplyVO.setLineNumber(objNodeValue);
	            
	            } else if(objTagName.equalsIgnoreCase(TAG_EI_InvoiceMechanism)) {
	            	objNodeValue = objNode.getFirstChild().getNodeValue();
	            	objEBillReplyVO.setInvoiceMechanism(objNodeValue);

	            } else if(objTagName.equalsIgnoreCase(TAG_EI_Email)) {
	            	objNodeValue = objNode.getFirstChild().getNodeValue();
	            	objEBillReplyVO.setEmail(objNodeValue);
	            
	            }

		    }
		}
		 log.debug("EBillXMLHandler > parsingXMLReplay > end");
		return objEBillReplyVO;
	}

}



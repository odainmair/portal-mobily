/*
 * Created on Feb 23, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.xml;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.ErrorIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.FavoriteNumberInquiryVO;
import sa.com.mobily.eportal.common.service.vo.MessageHeaderVO;


/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FavoriteNumberInquiryXMLHandler {

	
    private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;
    
     /**
      * generate xml request for get balance inquiry
      * @param requestVO
      * @return
      * Feb 19, 2008
      * BlanaceInquiyXMLHandler.java
      * msayed
      * the mansatoryf field is :
      * 	msisdn
      *     requestoruserId
      */
		    /*
		    <EE_EAI_MESSAGE>
			  <EE_EAI_HEADER>
			    <MsgFormat>FAVORITE_NUMBER_INQUIRY</MsgFormat>
			    <MsgOption></MsgOption>
			    <MsgVersion>0001</MsgVersion>
			    <RequestorChannelId>SIEBEL</RequestorChannelId>
			    <RequestorChannelFunction>INQ</RequestorChannelFunction>
			    <RequestorUserId>YALSUHAIBANI</RequestorUserId>
			    <RequestorLanguage>A</RequestorLanguage>
			    <RequestorSecurityInfo>secure</RequestorSecurityInfo>
			    <ReturnCode>0000</ReturnCode>
			  </EE_EAI_HEADER>
			  <MSISDN>966540510249</MSISDN>
			</EE_EAI_MESSAGE>
		
		    */
     public String generateXMLRequest(FavoriteNumberInquiryVO requestVO) {
    	 String requestMessage = "";
 	 try{
 	 	log.debug("FavoriteNumberInquiryXMLHandler > generateXMLRequest > Called for MSISDN = ["+requestVO.getMsisdn()+"]");
         if (requestVO.getMsisdn() == null
                 || requestVO.getMsisdn().equalsIgnoreCase("")) {
             log.fatal("FavoriteNumberInquiryXMLHandler > generate XML Message > Number Not Found....");
             throw new MobilyCommonException(ErrorIfc.NUMBER_NOT_FOUND_ECEPTION);
         }
         
         Document doc = new DocumentImpl();
         Element root = doc.createElement(TagIfc.MAIN_EAI_TAG_NAME);
         Element header = XMLUtility.openParentTag(doc,TagIfc.EAI_HEADER_TAG);
         	XMLUtility.generateElelemt(doc, header, TagIfc.MSG_FORMAT,ConstantIfc.FN_Req_FAVORITE_NUMBER_INQUIRY);
         	XMLUtility.generateElelemt(doc, header, TagIfc.TAG_MSG_OPTION,ConstantIfc.FN_Req_MsgOption);
         	XMLUtility.generateElelemt(doc, header, TagIfc.MSG_VERSION,ConstantIfc.MESSAGE_VERSION_VALUE);
         	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_CHANNEL_ID, ConstantIfc.CHANNEL_ID_VALUE);
         	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_CHANNEL_FUNCTION,ConstantIfc.FN_Req_RequestorChannelFunction);
         	XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_USER_ID,requestVO.getRequestorUserId());
         	XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_LANG,ConstantIfc.LANG_VALUE);
         	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_SECURITY_INFO,ConstantIfc.SECURITY_INFO_VALUE);
         	XMLUtility.generateElelemt(doc, header, TagIfc.RETURN_CODE,ConstantIfc.RETURN_CODE_VALUE);
         XMLUtility.closeParentTag(root, header);
         //body
         XMLUtility.generateElelemt(doc, root, TagIfc.TAG_MSISDN, requestVO.getMsisdn());
         doc.appendChild(root);
         requestMessage = XMLUtility.serializeRequest(doc);

 	 }catch(Exception e){
 	   throw new SystemException(e);    
 	 }
         
 	 log.debug("FavoriteNumberInquiryXMLHandler > generateXMLRequest > done for MSISDN = ["+requestVO.getMsisdn()+"] and message = >"+requestMessage);
 		 return requestMessage;

     
     }
     
     
     /**
      * 
      * @param replyMessage
      * @return
      * @throws MobilyCommonException
      */
     
     /*
	<EE_EAI_MESSAGE>
		<EE_EAI_HEADER>
			<MsgFormat>FAVORITE_NUMBER_INQUIRY</MsgFormat>
			<MsgOption />
			<MsgVersion>0001</MsgVersion>
			<RequestorChannelId>SIEBEL</RequestorChannelId>
			<RequestorChannelFunction>INQ</RequestorChannelFunction>
			<RequestorUserId>YALSUHAIBANI</RequestorUserId>
			<RequestorLanguage>A</RequestorLanguage>
			<RequestorSecurityInfo>secure</RequestorSecurityInfo>
			<ReturnCode>0000</ReturnCode>
			<ErrorCode>0000</ErrorCode>
			<ErrorMsg></ErrorMsg>
		</EE_EAI_HEADER>
		<MSISDN>966560492593</MSISDN>
		<InternationalFavouriteNumber>
			<MSISDN>00914762672082 </MSISDN>
			<FavoriteDesc> International </FavoriteDesc>
			<Type>Voice</Type>
		</InternationalFavouriteNumber>
		<NationalFavouriteNumber>
			<MSISDN>033626589 </MSISDN>
			<FavoriteDesc>Mobily Number</FavoriteDesc>
			<Type>SMS</Type>
		</NationalFavouriteNumber>
		<FavouriteCountry>
			<MSISDN>0020</MSISDN>
			<FavoriteDesc>International Favorite Country 66 Hala</FavoriteDesc>
			<Type>Voice</Type>
		</FavouriteCountry>
		<FavouriteMCC>
			<MSISDN>0543023155</MSISDN>
			<FavoriteDesc>MCC Favorite Number</FavoriteDesc>
			<Type>Voice</Type>
		</FavouriteMCC>
	</EE_EAI_MESSAGE>
      */
     public FavoriteNumberInquiryVO parsingFNInquiryReplyMessage(String replyMessage) throws MobilyCommonException{
     	FavoriteNumberInquiryVO replyVO = null ;
        log.debug("FavoriteNumberInquiryXMLHandler > parsingFNInquiryReplyMessage > message = "+replyMessage);
 		if(replyMessage == null || replyMessage =="")
 			return replyVO;
 		try{
 			 replyVO = new FavoriteNumberInquiryVO();
 			 
 			 Document doc = XMLUtility.parseXMLString(replyMessage);
 			 
 			 int CustomerType = 0;
 			 Node rootNode = doc.getElementsByTagName(TagIfc.MAIN_EAI_TAG_NAME).item(0);
 			 
 			 NodeList nl	 = rootNode.getChildNodes(); 
 			 
 			 for(int j = 0; j < nl.getLength(); j++){
                 if((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
                 	continue;
                 
                 Element l_Node       = (Element)nl.item(j);
                 String p_szTagName   = l_Node.getTagName();
                 String l_szNodeValue = null;
                 
                 if( l_Node.getFirstChild() != null )
                 	l_szNodeValue 	 =l_Node.getFirstChild().getNodeValue();
                 
                 if(p_szTagName.equalsIgnoreCase(TagIfc.EAI_HEADER_TAG))
                 {
                 	MessageHeaderHandler headerParser = new MessageHeaderHandler();
                 	MessageHeaderVO Header = headerParser.parsingXMLHeaderReplyMessage(l_Node);
                 	
                 	if(Integer.parseInt(Header.getReturnCode()) > 0){
                 	  throw new MobilyCommonException(Header.getErrorCode() , Header.getErrorMessage());
                 	}

                 	log.debug("FavoriteNumberInquiryXMLHandler > parsingFNInquiryReplyMessage > Header.getErrorCode() = "+Header.getErrorCode());	
                 	if(Integer.parseInt(Header.getErrorCode()) == 9999){
                 		log.debug("FavoriteNumberInquiryXMLHandler > parsingFNInquiryReplyMessage > Header.getErrorCode() is 9999 ");
                 	  throw new MobilyCommonException(Header.getErrorCode());
                 	}


                 }else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_MSISDN)){
 	                   replyVO.setMsisdn(l_szNodeValue);
                 }
                 else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_INTERNATIONAL_FAVORITE_NUMBER)){ //Fetching the international number list
        			
                 	NodeList intlFavNumList 		= l_Node.getChildNodes();

        			for(int k = 0; k < intlFavNumList.getLength(); k++){
        			    if((intlFavNumList.item(k)).getNodeType() != Node.ELEMENT_NODE)
        			    	continue;
        			    	
        			    Element chl_Node         = (Element)intlFavNumList.item(k);
        			    String chl_szNodeTagName = chl_Node.getTagName();
        			    String chl_szNodeValue   = "";
        			    
        			    if( chl_Node.getFirstChild() != null )
        			    	   chl_szNodeValue   = chl_Node.getFirstChild().getNodeValue();
        			    if(chl_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_MSISDN))
        			    	replyVO.addtoInternational(chl_szNodeValue.trim());
        			    else if(chl_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_FAVORITE_NUMBER_DESCRIPTION))
        			    	replyVO.addtoInternational(chl_szNodeValue.trim());
        			}
                 }else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_NATIONAL_FAVORITE_NUMBER)){ //Fetching the national number list(Mobily, Landline and Other)
        			NodeList natlFavNumList 		= l_Node.getChildNodes();
        			for(int k = 0; k < natlFavNumList.getLength(); k++){
        			    if((natlFavNumList.item(k)).getNodeType() != Node.ELEMENT_NODE)
        			    	continue;

        			    Element chl_Node         = (Element)natlFavNumList.item(k);
        			    String chl_szNodeTagName = chl_Node.getTagName();
        			    String chl_szNodeValue   = "";
        			    
        			    if( chl_Node.getFirstChild() != null )
        			    	   chl_szNodeValue   = chl_Node.getFirstChild().getNodeValue();
        			    if(chl_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_MSISDN))
        			    	replyVO.addtoNational(chl_szNodeValue.trim());
        			    else if(chl_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_FAVORITE_NUMBER_DESCRIPTION))
        			    	replyVO.addtoNational(chl_szNodeValue.trim());
        			}
                 }else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_COUNTRY_FAVORITE_NUMBER)){ //Fetching the country favorite number list
        			NodeList countryFavNumList 		= l_Node.getChildNodes();
        			
        			for(int k = 0; k < countryFavNumList.getLength(); k++){
        			    if((countryFavNumList.item(k)).getNodeType() != Node.ELEMENT_NODE)
        			    	continue;

        			    Element chl_Node         = (Element)countryFavNumList.item(k);
        			    String chl_szNodeTagName = chl_Node.getTagName();
        			    String chl_szNodeValue   = "";
        			    
        			    if( chl_Node.getFirstChild() != null )
        			    	   chl_szNodeValue   = chl_Node.getFirstChild().getNodeValue();
        			    if(chl_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_MSISDN))
        			    	replyVO.addtoCountryFavList(chl_szNodeValue.trim());
        			    else if(chl_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_FAVORITE_NUMBER_DESCRIPTION))
        			    	replyVO.addtoCountryFavList(chl_szNodeValue.trim());
        			}
                 }else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_FAVORITE_MCC)){ //Fetching the MCC favorite number list
        			NodeList mccFavNumList 		= l_Node.getChildNodes();
        			
        			for(int k = 0; k < mccFavNumList.getLength(); k++){
        			    if((mccFavNumList.item(k)).getNodeType() != Node.ELEMENT_NODE)
        			    	continue;

        			    Element chl_Node         = (Element)mccFavNumList.item(k);
        			    String chl_szNodeTagName = chl_Node.getTagName();
        			    String chl_szNodeValue   = "";
        			    
        			    if( chl_Node.getFirstChild() != null )
        			    	   chl_szNodeValue   = chl_Node.getFirstChild().getNodeValue();
        			    if(chl_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_MSISDN))
        			    	replyVO.addtoMCCFavList(chl_szNodeValue.trim());
        			    else if(chl_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_FAVORITE_NUMBER_DESCRIPTION))
        			    	replyVO.addtoMCCFavList(chl_szNodeValue.trim());
        			}
                 }
  			 }
 			}
 		catch(MobilyCommonException e){
 			log.fatal("FavoriteNumberInquiryXMLHandler > parsingFNInquiryReplyMessage >MobilyCommonException>"+e);
 			throw e;
 			}
 		log.debug("FavoriteNumberInquiryXMLHandler > parsingFNInquiryReplyMessage > done");
 		return replyVO;
     }

     
     
     /**
      * 
      * @param replyMessage
      * @return
      * @throws MobilyCommonException
      */
     public FavoriteNumberInquiryVO parsingFNInquiryReplyMessageOld(String replyMessage) throws MobilyCommonException{
     	FavoriteNumberInquiryVO replyVO = null ;
        log.debug("FavoriteNumberInquiryXMLHandler > parsingFNInquiryReplyMessage > message = "+replyMessage);
 		if(replyMessage == null || replyMessage =="")
 			return replyVO;
 		try{
 			 replyVO = new FavoriteNumberInquiryVO();
 			 
 			 Document doc = XMLUtility.parseXMLString(replyMessage);
 			 
 			 int CustomerType = 0;
 			 Node rootNode = doc.getElementsByTagName(TagIfc.MAIN_EAI_TAG_NAME).item(0);
 			 
 			 NodeList nl	 = rootNode.getChildNodes(); 
 			 
 			 for(int j = 0; j < nl.getLength(); j++){
                 if((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
                 	continue;
                 
                 Element l_Node       = (Element)nl.item(j);
                 String p_szTagName   = l_Node.getTagName();
                 String l_szNodeValue = null;
                 if( l_Node.getFirstChild() != null )
                 	l_szNodeValue 	 =l_Node.getFirstChild().getNodeValue();
                 
                 if(p_szTagName.equalsIgnoreCase(TagIfc.EAI_HEADER_TAG))
                 {
                 	MessageHeaderHandler headerParser = new MessageHeaderHandler();
                 	MessageHeaderVO Header = headerParser.parsingXMLHeaderReplyMessage(l_Node);
                 	if(Integer.parseInt(Header.getReturnCode()) > 0){
                 		
                 	  throw new MobilyCommonException(Header.getErrorCode() , Header.getErrorMessage());
                 	}

                 }else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_MSISDN)){
 	                   replyVO.setMsisdn(l_szNodeValue);
                 }
                 else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_INTERNATIONAL_FAVORITE_NUMBER)){
                 	  if(l_szNodeValue != null)
                 	   replyVO.addtoInternational(l_szNodeValue);
                 }
                 else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_NATIONAL_FAVORITE_NUMBER)){
                 	replyVO.addtoNational(l_szNodeValue);
                 }
                 
  			 }
 			}
 		catch(MobilyCommonException e){
 			log.fatal("FavoriteNumberInquiryXMLHandler > parsingFNInquiryReplyMessage >MobilyCommonException>"+e);
 			throw e;
 			}
 		log.debug("FavoriteNumberInquiryXMLHandler > parsingFNInquiryReplyMessage > done");
 		return replyVO;
     }
     
     
     
     public static void main(String[] args) {
		
		try {
			FavoriteNumberInquiryXMLHandler xmlHandler = new FavoriteNumberInquiryXMLHandler();
			FavoriteNumberInquiryVO fvnInqVO = new FavoriteNumberInquiryVO();
			
			fvnInqVO.setRequestorUserId("YALSUHAIBANI");
			fvnInqVO.setMsisdn("966540510249");
			
			String request = xmlHandler.generateXMLRequest(fvnInqVO);
			
			//String response = "<EE_EAI_MESSAGE><EE_EAI_HEADER><MsgFormat>FAVORITE_NUMBER_INQUIRY</MsgFormat><MsgOption /><MsgVersion>0001</MsgVersion><RequestorChannelId>SIEBEL</RequestorChannelId><RequestorChannelFunction>INQ</RequestorChannelFunction><RequestorUserId>YALSUHAIBANI</RequestorUserId><RequestorLanguage>A</RequestorLanguage><RequestorSecurityInfo>secure</RequestorSecurityInfo><ReturnCode>0000</ReturnCode><ErrorCode>0000</ErrorCode><ErrorMsg></ErrorMsg></EE_EAI_HEADER><MSISDN>966560492593</MSISDN><InternationalFavouriteNumber><MSISDN>00914762672081 </MSISDN><FavoriteDesc> International </FavoriteDesc><Type>Voice</Type></InternationalFavouriteNumber><InternationalFavouriteNumber><MSISDN>00914762672082 </MSISDN><FavoriteDesc> International </FavoriteDesc><Type>Voice</Type></InternationalFavouriteNumber><NationalFavouriteNumber><MSISDN>033626589 </MSISDN><FavoriteDesc>Mobily Number</FavoriteDesc><Type>SMS</Type></NationalFavouriteNumber><NationalFavouriteNumber><MSISDN>033626590 </MSISDN><FavoriteDesc>Mobily Number</FavoriteDesc><Type>SMS</Type></NationalFavouriteNumber><NationalFavouriteNumber><MSISDN>033626591 </MSISDN><FavoriteDesc>Mobily Number</FavoriteDesc><Type>SMS</Type></NationalFavouriteNumber></EE_EAI_MESSAGE>";
			String response = "<EE_EAI_MESSAGE><EE_EAI_HEADER><MsgFormat>FAVORITE_NUMBER_INQUIRY</MsgFormat><MsgOption/><MsgVersion>0001</MsgVersion><RequestorChannelId>SIEBEL</RequestorChannelId><RequestorChannelFunction>INQ</RequestorChannelFunction><RequestorUserId>YALSUHAIBANI</RequestorUserId><RequestorLanguage>A</RequestorLanguage><RequestorSecurityInfo>secure</RequestorSecurityInfo><ReturnCode>0000</ReturnCode><ErrorCode>0000</ErrorCode><ErrorMsg></ErrorMsg></EE_EAI_HEADER><MSISDN>966540510249</MSISDN><NationalFavouriteNumber><MSISDN>00966540510248</MSISDN><FavoriteDesc>Mobily Number</FavoriteDesc><Type>SMS</Type></NationalFavouriteNumber><NationalFavouriteNumber><MSISDN>00966540510248</MSISDN><FavoriteDesc>Mobily Number</FavoriteDesc><Type>SMS</Type></NationalFavouriteNumber><NationalFavouriteNumber><MSISDN>00966540510248</MSISDN><FavoriteDesc>Mobily Number</FavoriteDesc><Type>SMS</Type></NationalFavouriteNumber><InternationalFavouriteNumber><MSISDN>0563820424</MSISDN><FavoriteDesc>Mobily Number</FavoriteDesc><Type>Voice</Type></InternationalFavouriteNumber><InternationalFavouriteNumber><MSISDN>0568037054</MSISDN><FavoriteDesc>Mobily Number</FavoriteDesc><Type>Voice</Type></InternationalFavouriteNumber><InternationalFavouriteNumber><MSISDN>0568952114</MSISDN><FavoriteDesc>Mobily Number</FavoriteDesc><Type>Voice</Type></InternationalFavouriteNumber><InternationalFavouriteNumber><MSISDN>0568977990</MSISDN><FavoriteDesc>Mobily Number</FavoriteDesc><Type>Voice</Type></InternationalFavouriteNumber><InternationalFavouriteNumber><MSISDN>0563820424</MSISDN><FavoriteDesc>Mobily Number</FavoriteDesc><Type>Voice</Type></InternationalFavouriteNumber><InternationalFavouriteNumber><MSISDN>0568037054</MSISDN><FavoriteDesc>Mobily Number</FavoriteDesc><Type>Voice</Type></InternationalFavouriteNumber><InternationalFavouriteNumber><MSISDN>0568952114</MSISDN><FavoriteDesc>Mobily Number</FavoriteDesc><Type>Voice</Type></InternationalFavouriteNumber><InternationalFavouriteNumber><MSISDN>0568977990</MSISDN><FavoriteDesc>Mobily Number</FavoriteDesc><Type>Voice</Type></InternationalFavouriteNumber></EE_EAI_MESSAGE>";
			FavoriteNumberInquiryVO respVO = xmlHandler.parsingFNInquiryReplyMessage(response);
			
			//System.out.println(respVO.getInternationalList());
			//System.out.println(respVO.getNationalList());
			
		} catch (MobilyCommonException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		
		
	}
}

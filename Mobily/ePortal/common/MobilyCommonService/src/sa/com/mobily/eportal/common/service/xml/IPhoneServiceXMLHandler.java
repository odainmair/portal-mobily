package sa.com.mobily.eportal.common.service.xml;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.IPhoneReplyVO;
import sa.com.mobily.eportal.common.service.vo.IPhoneRequestVO;


/**
 * 
 * @author n.gundluru.mit
 *
 */

public class IPhoneServiceXMLHandler implements TagIfc {

	private static final Logger log = LoggerInterface.log;
	
	
	 
	/**
	 * Parsing xml request message
	 * 
	 * @return BalanceInquiryReplyVO
	 * @throws MobilyCommonException 
	 */
		/*
			<MOBILY_IPHONE_REQUEST>
				<UserName></UserName>
			    <Password></Password>
			    <MSISDN></MSISDN>
			    <HashCode></HashCode>
			    <FunctionId></FunctionId>
			    <RequestorLanguage></RequestorLanguage>
			    <InvoiceMechanism></InvoiceMechanism>
			    <Email></Email>
			    <NewsId></NewsId>
			    <PromotionId></PromotionId>
			    <Id></Id>
			    <Type></Type>
				<ServiceName></ServiceName>
				<ServiceType></ServiceType>
				<ItemCode>1001</ItemCode>
				<X-Axis></X-Axis>
				<Y-Axis></Y-Axis>
				<ProblemID></ProblemID>
				<RecipientMSISDN></RecipientMSISDN>
				<TransferredAmount>5/10/15/20</TransferredAmount>
				<ConfirmationKey></ConfirmationKey>
				<device>1</device>
				<Range></Range>
				<FRIEND_MSISDN></FRIEND_MSISDN>
				<SR_ID></SR_ID>
				<Partners>
					<Partner>
						<ID></ID>
					</Partner>
				</Partners>	
				<POIs>
					<POI>
						<ID></ID>
					</POI>
				</POIs>	
				<REPLY></REPLY>
				<CHANNEL_ID></CHANNEL_ID>
				<CONTENT_ID></CONTENT_ID>
				<PIN_CODE></PIN_CODE>
				<COMMAND></COMMAND>
				<Category_Id>1</Category_Id>
				<Start_Row></Start_Row>
				<End_Row></End_Row> 
				<Tone_Id>1987</Tone_Id>
				<Contact_Number>966540510521</Contact_Number>
				<Contact_Name>Sayed</Contact_Name>
				<Assigned_Info>
					<Assigned_Rule>
						<Tone_Setting_Id>1987</Tone_Setting_Id>
						<Contact_Number>xxxxxxxxxx</Contact_Number>
					</Assigned_Rule>
					.
					.
					.
				</Assigned_Info>
				<Tone_Personal_Id>1987</Tone_Personal_Id>
				<FullName></FullName>
				<Gender></Gender>
				<PreferredLanguage></PreferredLanguage>
				<Email></Email>
				<Name></Name>
				<PhoneNumber></PhoneNumber>
				<OtherPhoneNumber></OtherPhoneNumber>
				<Customer_Type></Customer_Type>
				<Inquiry_Type></Inquiry_Type>
				<Subject></Subject>
				<Message></Message>
				<VoucherNumber></VoucherNumber>
				<RecipientID></RecipientID>
				<InfoRequired></InfoRequired>
				<SimNumber></SimNumber>
				<IdType></IdType>
				<ExpiryDate></ExpiryDate>
				<AttchedFileName></AttchedFileName>
				<AttchedFile></AttchedFile>
				<ToneCntRequired></ToneCntRequired>
				<isSubscribed></isSubscribed>
				<ServiceId></ServiceId> 
			</MOBILY_IPHONE_REQUEST>       
		 */
	public static IPhoneRequestVO parsingIphoneRequestXML(String requestXML) {
		IPhoneRequestVO requestVo = null;
		
		log.debug("IPhoneServiceXMLHandler > parsingIphoneRequestXML  > Start");
		
		try {
			requestVo = new IPhoneRequestVO();
			 
			 Document doc = XMLUtility.parseXMLString(requestXML);
			 
			 Node rootNode = doc.getElementsByTagName(TagIfc.TAG_START_IPHONE_REQ).item(0);
			 
			 NodeList nl = rootNode.getChildNodes();

	            for (int j = 0; j < nl.getLength(); j++) {
	                if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
	                    continue;

	                Element l_Node = (Element) nl.item(j);
	                String p_szTagName = l_Node.getTagName();
	                String l_szNodeValue = null;
	                if (l_Node.getFirstChild() != null)
	                    l_szNodeValue = l_Node.getFirstChild().getNodeValue();

	                
	                if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_USER_ID)) {
	                	requestVo.setUserId(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_PASSWORD)) {
	                	requestVo.setPassword(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_MSISDN)) {
	                	requestVo.setOriginalMsisdn(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_HASH_CODE)) {
	                	requestVo.setOriginalHashCode(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_FUNCTIONID)) {
	                	requestVo.setFunctionId(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_REQUESTOR_LANG)) {
	                	requestVo.setReqLanguage(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_EI_InvoiceMechanism)) {
	                	requestVo.setInvoiceMechanism(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_EI_Email)) {
	                	requestVo.setEmail(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ACTION)) {
	                	requestVo.setAction(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SERVICE_NAME)) {
	                	requestVo.setServiceName(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SERVICE_TYPE)) {
	                	requestVo.setServiceType(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_X_AXIS)) {
	                	requestVo.setXAxis(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_Y_AXIS)) {
	                	requestVo.setYAxis(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ITEM_CODE)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setItemCode(Integer.parseInt(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_PROBLEM_ID)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setProblemID(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_RECIPIENT_MSISDN)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setRecipientMSISDN(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_TRANSFER_AMOUNT)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setTransferredAmount(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CONFIMATION_KEY)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setConfirmationKey(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_NEWS_ID)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setNewsID(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_PROMOTION_ID)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setPromotionId(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ID)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setId(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_TYPE)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setType(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_DEVICE)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setDevice(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_FRIEND_MSISDN)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setFriendMsisdn(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SR_ID)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setSrId(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_RANGE)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setRange(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_REPLY)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setReply(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CHANNEL_ID)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setChannelId(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CONTENT_ID)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setContentId(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_PIN_CODE)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setPinCode(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CONTENT_COMMAND)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setCommand(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CATEGORY_ID)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setCategoryId(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_START_ROW)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setStartRow(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_END_ROW)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setEndRow(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_TONE_ID)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setToneId(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CONTACT_NUMBER)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setContactNumber(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CONTACT_NAME)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setContactName(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_TONE_SETTING_ID)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setToneSettingId(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_TONE_PERSONAL_ID)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setTonePersonalId(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_PARTNERS)) {
	                	//Loop through Partners 
						NodeList sectionsNl = l_Node.getChildNodes(); // get the sub child
						ArrayList partnerIdList = new ArrayList();
						
						for (int l = 0; sectionsNl != null && l < sectionsNl.getLength(); l++) {
							if ((sectionsNl.item(l)).getNodeType() != Node.ELEMENT_NODE)
								continue;
							
							Element sectionNl = (Element) sectionsNl.item(l);
							p_szTagName = sectionNl.getTagName();
							if(TagIfc.TAG_PARTNER.equalsIgnoreCase(p_szTagName)) {
								NodeList resultsNl = sectionNl.getChildNodes();
								
								for (int k = 0; k < resultsNl.getLength(); k++) {
									if ((resultsNl.item(k)).getNodeType() != Node.ELEMENT_NODE)
										continue;
									
									Element resultSetNl = (Element) resultsNl.item(k);
									p_szTagName = resultSetNl.getTagName();

									if(TagIfc.TAG_ID.equalsIgnoreCase(p_szTagName)) {
										if (resultSetNl.getFirstChild() != null)
											partnerIdList.add(resultSetNl.getFirstChild().getNodeValue());
									} 
								}
							}
						}
                		requestVo.setPartnerIdList(partnerIdList);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_POIS)) {
	                	//Loop through POIs 
						NodeList sectionsNl = l_Node.getChildNodes(); // get the sub child
						ArrayList poiIdList = new ArrayList();
						
						for (int l = 0; sectionsNl != null && l < sectionsNl.getLength(); l++) {
							if ((sectionsNl.item(l)).getNodeType() != Node.ELEMENT_NODE)
								continue;
							
							Element sectionNl = (Element) sectionsNl.item(l);
							p_szTagName = sectionNl.getTagName();
							if(TagIfc.TAG_POI.equalsIgnoreCase(p_szTagName)) {
								NodeList resultsNl = sectionNl.getChildNodes();
								
								for (int k = 0; k < resultsNl.getLength(); k++) {
									if ((resultsNl.item(k)).getNodeType() != Node.ELEMENT_NODE)
										continue;
									
									Element resultSetNl = (Element) resultsNl.item(k);
									p_szTagName = resultSetNl.getTagName();

									if(TagIfc.TAG_ID.equalsIgnoreCase(p_szTagName)) {
										if (resultSetNl.getFirstChild() != null)
											poiIdList.add(resultSetNl.getFirstChild().getNodeValue());
									} 
								}
							}
						}
                		requestVo.setPoiIdList(poiIdList);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ASSIGNED_INFO)) {
						NodeList sectionsNl = l_Node.getChildNodes(); // get the sub child
						ArrayList assignedRuleList = new ArrayList();
						StringBuffer assignedRule = null;
						for (int l = 0; sectionsNl != null && l < sectionsNl.getLength(); l++) {
							if ((sectionsNl.item(l)).getNodeType() != Node.ELEMENT_NODE)
								continue;
							
							Element sectionNl = (Element) sectionsNl.item(l);
							p_szTagName = sectionNl.getTagName();
							if(TagIfc.TAG_ASSIGNED_RULE.equalsIgnoreCase(p_szTagName)) {
								NodeList resultsNl = sectionNl.getChildNodes();
								
								assignedRule = new StringBuffer();
								for (int k = 0; k < resultsNl.getLength(); k++) {
									if ((resultsNl.item(k)).getNodeType() != Node.ELEMENT_NODE)
										continue;
									
									Element resultSetNl = (Element) resultsNl.item(k);
									p_szTagName = resultSetNl.getTagName();

									if(TagIfc.TAG_CONTACT_NUMBER.equalsIgnoreCase(p_szTagName)) {
										if (resultSetNl.getFirstChild() != null){
											assignedRule.append(resultSetNl.getFirstChild().getNodeValue());
											assignedRule.append("~");
										}
									} else if(TagIfc.TAG_TONE_SETTING_ID.equalsIgnoreCase(p_szTagName)) {
										if (resultSetNl.getFirstChild() != null)
											assignedRule.append(resultSetNl.getFirstChild().getNodeValue());
									}
								}
								assignedRuleList.add(assignedRule.toString());
							}
						}
                		requestVo.setAssignedRuleList(assignedRuleList);
	                }else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_FULL_NAME)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setFullName(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_GENDER)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setGender(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_PREFERRED_LANG)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setPreferredLanguage(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_EMAIL)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setEmail(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_NAME)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setName(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_PHONE_NUMBER)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setPhoneNumber(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_OTHER_PHONE_NUMNER)) {
	                		requestVo.setOtherPhoneNumber(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CUST_TYPE)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setCustomerType(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_INQ_TYPE)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setInquiryType(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SUBJECT)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setSubject(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_MESSAGE)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setMessage(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_VOUCHER_NUMBER)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setVoucherNumber(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CTReq_RecipientID)) {
	                	if(l_szNodeValue != null)
	                		requestVo.setRecipientId(l_szNodeValue);
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_INFO_REQUIRED)) {
	                	if(l_szNodeValue != null)
	                		requestVo.setInfoRequired(l_szNodeValue);
	                }  	else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SIM_NUMBER)) {
	                	if(l_szNodeValue != null)
	                		requestVo.setSimNumber(l_szNodeValue);
	                }  	else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_IdType)) {
	                	if(l_szNodeValue != null)
	                		requestVo.setIdType(l_szNodeValue);
	                }  	else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ExpiryDate)) {
	                	if(l_szNodeValue != null)
	                		requestVo.setExpiryDate(l_szNodeValue);
	                }  	else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ATTACHED_FILE_NAME)) {
	                	if(l_szNodeValue != null)
	                		requestVo.setAttchedFileName(l_szNodeValue);
	                }  	else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ATTACHED_FILE)) {
	                	if(l_szNodeValue != null)
	                		requestVo.setAttchedFile(l_szNodeValue);
	                }  	else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_TONE_COUNT_REQ)) {
	                	if(l_szNodeValue != null)
	                		requestVo.setToneCntRequired(l_szNodeValue);
	                }  	else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SERIAL_NO)) {
	                	if(l_szNodeValue != null)
	                		requestVo.setSerialNo(l_szNodeValue);
	                }  	else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SERVICE_ID)) {
	                	if(l_szNodeValue != null)
	                		requestVo.setServiceId(l_szNodeValue);
	                }  	else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SEARCH_BY)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setSearchBy(l_szNodeValue);
	                } 	else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SEARCH_CRITERIA)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setSearchCriteria(l_szNodeValue);
	                }  else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SDP_MGMT_PRODUCT_ID)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setProductId(l_szNodeValue);
	                }  else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SDP_MGMT_CONTENT_TYPE)) {
	                	if(l_szNodeValue != null && !"".equals(l_szNodeValue))
	                		requestVo.setContentType(l_szNodeValue);
	                }  else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_QUANTITY)) {
	                		requestVo.setQuantity(FormatterUtility.checkNull(l_szNodeValue));
	                }  else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_PACKAGE_NAME)) {
	                		requestVo.setPackageName(FormatterUtility.checkNull(l_szNodeValue));
	                }  else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ORDER_NUMBER)) {
	                		requestVo.setOrderNumber(FormatterUtility.checkNull(l_szNodeValue));
	                }
	            }
	    } catch (Exception e) {
            log.fatal("IPhoneServiceXMLHandler > parsingIphoneRequestXML > Exception > "+ e.getMessage());
            throw new SystemException(e);
        }
        
		log.debug("IPhoneServiceXMLHandler > parsingIphoneRequestXML  > End");
		return requestVo;
	}

	
	/**
	 * This Method is responsible for generating XMl Reply for successful login
	 * 
	 * @param reqplyVo
	 * @return
	 */
		/*	<MOBILY_IPHONE_REQUEST>
				<UserId>xxxxxxxx</UserId>
				<Msisdn>xxxxxxxxxx</Msisdn>
				<HashCode>ad4jXuJn50</HashCode>
				<ERROR>
					<ErrorCode>0000</ErrorCode >
				</ERROR>
			</MOBILY_IPHONE_REQUEST>
		
		*/
	public void generateLoggedInXML(Document doc, Element root, IPhoneReplyVO reqplyVo) {

		XMLUtility.generateElelemt(doc, root, TAG_USER_ID,reqplyVo.getUserId());
		XMLUtility.generateElelemt(doc, root, TAG_MSISDN,reqplyVo.getOriginalMSISDN());
		XMLUtility.generateElelemt(doc, root, TAG_HASH_CODE,reqplyVo.getOriginalHashCode());

		//Generate error header element
   		Element errorHeader = XMLUtility.openParentTag(doc,TAG_ERROR);
			XMLUtility.generateElelemt(doc, errorHeader, TAG_ERROR_CODE,reqplyVo.getErrorCode());
   		XMLUtility.closeParentTag(root,errorHeader);
	}

	
	/**
	 * Generates a general error message as mentioned below
	 * 
	 * @param reqplyVo
	 * @return
	 */
	/*
		 <MOBILY_IPHONE_REPLY>
		 	<FunctionId></FunctionId>
			<ERROR>
				<ErrorCode>0000</ErrorCode >
			</ERROR>
		 </MOBILY_IPHONE_REPLY> 
	 */
	public static String generalErrorXML(IPhoneReplyVO reqplyVo) {
		
		String reqplyMessage = "";
		try {

			Document doc= new DocumentImpl();
			
			//create the root tag
			Element root = doc.createElement(TAG_START_IPHONE_REP);
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_FUNCTIONID,reqplyVo.getFunctionId());
				Element errorHeader = XMLUtility.openParentTag(doc,TAG_ERROR);
	        		XMLUtility.generateElelemt(doc, errorHeader, TAG_ERROR_CODE,reqplyVo.getErrorCode());
	        XMLUtility.closeParentTag(root, errorHeader);
			
	        doc.appendChild(root);
			
			reqplyMessage = XMLUtility.serializeRequest(doc);
		} catch (Exception e) {
			log.fatal("IPhoneServiceXMLHandler > generalErrorXML > Exception > " + e.getMessage());
		}

		return reqplyMessage;
	}
	
	/**
	 * This method parses the given iPhone reply xml and returns the error code tag value.
	 * 
	 * @param requestXML
	 * @return replyVo
	 */
	public static IPhoneReplyVO parsingIphoneReplyXML(String requestXML) {
		IPhoneReplyVO replyVo = null;
		log.info("IPhoneServiceXMLHandler > parsingIphoneReplyXML  > Start");
		
		try {
				replyVo = new IPhoneReplyVO();
			 
				Document doc = XMLUtility.parseXMLString(requestXML);
				Node rootNode = doc.getElementsByTagName(TagIfc.TAG_START_IPHONE_REP).item(0);
			 
				NodeList nl = rootNode.getChildNodes();

	            for (int j = 0; j < nl.getLength(); j++) {
	                if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
	                    continue;

	                Element l_Node = (Element) nl.item(j);
	                String p_szTagName = l_Node.getTagName();
	                String l_szNodeValue = null;
	                if (l_Node.getFirstChild() != null)
	                    l_szNodeValue = l_Node.getFirstChild().getNodeValue();

	                
	                if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_MSISDN)) {
	                	replyVo.setOriginalMSISDN(FormatterUtility.checkNull(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR)) {
	                	NodeList sectionsNl = l_Node.getChildNodes(); // get the sub child

	                	for (int l = 0; sectionsNl != null && l < sectionsNl.getLength(); l++) {
							if ((sectionsNl.item(l)).getNodeType() != Node.ELEMENT_NODE)
								continue;
								
							Element sectionNl = (Element) sectionsNl.item(l);
							p_szTagName = sectionNl.getTagName();

							if(TagIfc.TAG_ERROR_CODE.equalsIgnoreCase(p_szTagName)) {
								if (sectionNl.getFirstChild() != null)
									replyVo.setErrorCode(FormatterUtility.checkNull(sectionNl.getFirstChild().getNodeValue()));
							} 
						}
					}
				}
		} catch (Exception e) {
			log.fatal("IPhoneServiceXMLHandler > parsingIphoneReplyXML > Exception > "+ e.getMessage());
	        replyVo.setErrorCode(ConstantIfc.ERROR_CODE_10001);
	    }
	        
		log.debug("IPhoneServiceXMLHandler > parsingIphoneReplyXML  > End");
		
		return replyVo;
	}
	
	public static void main(String args[]){
		IPhoneServiceXMLHandler xmlHandler = new IPhoneServiceXMLHandler();
		String requestXML = "<MOBILY_IPHONE_REQUEST><FunctionId>RANAN_DE_ASSIGN_TONE</FunctionId><Assigned_Info><Assigned_Rule><Contact_Number>966540510569</Contact_Number><Tone_Setting_Id>1987</Tone_Setting_Id></Assigned_Rule></Assigned_Info><HashCode>ad4jXuJn50</HashCode><RequestorLanguage>E</RequestorLanguage></MOBILY_IPHONE_REQUEST>";
		String replyXML = "<MOBILY_IPHONE_REPLY><FunctionId>Logout</FunctionId><ERROR><ErrorCode>100</ErrorCode></ERROR></MOBILY_IPHONE_REPLY>";
		
		IPhoneRequestVO requestVo = xmlHandler.parsingIphoneRequestXML(requestXML);
		
		IPhoneReplyVO replyVo = xmlHandler.parsingIphoneReplyXML(replyXML);
		
		System.out.println(replyVo.getErrorCode());
	}
}
package sa.com.mobily.eportal.common.service.xml;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.MobilyAuthNGoReplyVO;
import sa.com.mobily.eportal.common.service.vo.MobilyAuthNGoRequestVO;

/**
 * 
 * @author n.gundluru.mit
 *
 */
public class MobilyAuthAndGoXMLHandler {
	
	private static final Logger log = LoggerInterface.log;
	
	
	
	/**
	 * Parsing xml request message
	 * 
	 * @return 
	 * @throws MobilyCommonException 
	 */
	 /*
		<MOBILY_AUTH_AND_GO_REQUEST>
			<FunctionId></FunctionId>
		    <SourceMSISDN></SourceMSISDN>
		    <TargetMSISDN></TargetMSISDN>
		    <Amount></Amount>
		    <RecipientID></RecipientID>
		    <RequestorLanguage></RequestorLanguage>
		    <PinCode></PinCode>
		    <ServiceName></ServiceName>
		    <ServiceType></ServiceType>
		    <PlanType></PlanType>
		    <Operation></Operation>
		    <ForwardOption></ForwardOption>
		    <CustomerType></CustomerType>
		    <LineType></LineType>
		    <PackageId></PackageId>
		    <PackageDescription></PackageDescription>
		</MOBILY_AUTH_AND_GO_REQUEST>       
	  */
	
	public static MobilyAuthNGoRequestVO parsingAuthNGoRequestXML(String requestXML) {
		MobilyAuthNGoRequestVO requestVo = null;
		
		log.debug("MobilyAuthAndGoXMLHandler > parsingAuthNGoRequestXML  > Start");
		
		try {
			requestVo = new MobilyAuthNGoRequestVO();
			 
			 Document doc = XMLUtility.parseXMLString(requestXML);
			 
			 Node rootNode = doc.getElementsByTagName(TagIfc.TAG_MOBILY_AUTH_AND_GO_REQUEST).item(0);
			 
			 NodeList nl = rootNode.getChildNodes();

	            for (int j = 0; j < nl.getLength(); j++) {
	                if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
	                    continue;

	                Element l_Node = (Element) nl.item(j);
	                String p_szTagName = l_Node.getTagName();
	                String l_szNodeValue = null;
	                if (l_Node.getFirstChild() != null)
	                    l_szNodeValue = FormatterUtility.checkNull(l_Node.getFirstChild().getNodeValue());

	                if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_FUNCTIONID)) {
	                	requestVo.setFunctionId(FormatterUtility.checkNull(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SOURCE_MSISDN)) {
	                	requestVo.setSourceMSISDN(FormatterUtility.checkNull(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_TARGET_MSISDN)) {
	                	requestVo.setTargetMSISDN(FormatterUtility.checkNull(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_Amount)) {
	                	requestVo.setAmount(FormatterUtility.checkNull(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CTReq_RecipientID)) {
	                	requestVo.setRecipientID(FormatterUtility.checkNull(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_REQUESTOR_LANG)) {
	                	requestVo.setRequestorLanguage(FormatterUtility.checkNull(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_PINCODE)) {
	                	requestVo.setPinCode(FormatterUtility.checkNull(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SERVICE_NAME)) {
	                	requestVo.setServiceName(FormatterUtility.checkNull(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SERVICE_TYPE)) {
	                	requestVo.setServiceType(FormatterUtility.checkNull(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_PLAN_TYPE)) {
	                	requestVo.setPlanType(FormatterUtility.checkNull(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_OPERATION)) {
	                	requestVo.setOperation(FormatterUtility.checkNull(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_FWD_OPTION)) {
	                	requestVo.setForwardOption(FormatterUtility.checkNull(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CUSTOMER_TYPE)) {
	                	if(!"".equals(FormatterUtility.checkNull(l_szNodeValue)))
	                		requestVo.setCustomerType(Integer.parseInt(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_LINE_TYPE)) {
	                	if(!"".equals(FormatterUtility.checkNull(l_szNodeValue)))
	                		requestVo.setLineType(Integer.parseInt(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_PACKAGE_ID)) {
	                	requestVo.setPackageID(FormatterUtility.checkNull(l_szNodeValue));
	                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_PACKAGE_DESCRIPTION)) {
	                	requestVo.setPackageDescription(FormatterUtility.checkNull(l_szNodeValue));
	                } 
	            }
	    } catch (Exception e) {
            log.fatal("MobilyAuthAndGoXMLHandler > parsingAuthNGoRequestXML > Exception > "+ e.getMessage());
            throw new SystemException(e);
        }
        
		log.debug("MobilyAuthAndGoXMLHandler > parsingAuthNGoRequestXML  > End");
		return requestVo;
	}

	/**
	 * Generates reply message
	 * 
	 * @param reqplyVo
	 * @return reqplyMessage
	 */
	/*
		 <MOBILY_AUTH_AND_GO_REPLY>
		 	<FunctionId></FunctionId>
		 	<SourceMSISDN></SourceMSISDN>
		 	<TargetMSISDN></TargetMSISDN>
		 	<Status></Status>
			<ErrorCode></ErrorCode >
			<ErrorMsg></ErrorMsg>
		 </MOBILY_IPHONE_REPLY> 
	 */
	public static String generateAuthNGoReplyXML(MobilyAuthNGoReplyVO reqplyVo) {
		
		String reqplyMessage = "";
		try {

			Document doc= new DocumentImpl();
			
			//create the root tag
			Element root = doc.createElement(TagIfc.TAG_MOBILY_AUTH_AND_GO_REPLY);
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_FUNCTIONID,FormatterUtility.checkNull(reqplyVo.getFunctionId()));
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_SOURCE_MSISDN,FormatterUtility.checkNull(reqplyVo.getSourceMSISDN()));
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_TARGET_MSISDN,FormatterUtility.checkNull(reqplyVo.getTargetMSISDN()));
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_STATUS,FormatterUtility.checkNull(reqplyVo.getStatus()));
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_ERROR_CODE,FormatterUtility.checkNull(reqplyVo.getErrorCode()));
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_ERROR_MSG,new String(FormatterUtility.checkNull(reqplyVo.getErrorMessage()).getBytes("UTF-8")));
	        doc.appendChild(root);
			
			reqplyMessage = XMLUtility.serializeRequest(doc);
		} catch (Exception e) {
			log.fatal("MobilyAuthAndGoXMLHandler > generateAuthNGoReplyXML > Exception > " + e.getMessage());
		}

		return reqplyMessage;
	}
	
	public static void main(String args[]){
		MobilyAuthAndGoXMLHandler xmlHandler = new MobilyAuthAndGoXMLHandler();
		String requestXML = "<MOBILY_AUTH_AND_GO_REQUEST><FunctionId></FunctionId><SourceMSISDN></SourceMSISDN><TargetMSISDN></TargetMSISDN><Amount></Amount><RequestorLanguage></RequestorLanguage><PinCode></PinCode><ServiceName></ServiceName><Operation></Operation><CustomerType></CustomerType><LineType></LineType><PackageId></PackageId><PackageDescription></PackageDescription></MOBILY_AUTH_AND_GO_REQUEST>";
		
		MobilyAuthNGoRequestVO requestVo = xmlHandler.parsingAuthNGoRequestXML(requestXML);
//		System.out.println(generateAuthNGoReplyXML(new MobilyAuthNGoReplyVO()));
//		System.out.println(requestVo.getFunctionId());
//		System.out.println(requestVo.getSourceMSISDN());

	}
}
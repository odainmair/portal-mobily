/*
 * Created on Feb 20, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.xml;

import java.util.ArrayList;

import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.ErrorIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.MessageHeaderVO;
import sa.com.mobily.eportal.common.service.vo.MobilyPromotionVO;
import sa.com.mobily.eportal.common.service.vo.MyServiceInfoVO;
import sa.com.mobily.eportal.common.service.vo.MyServiceInquiryRequestVO;
import sa.com.mobily.eportal.common.service.vo.MyServiceInquiryVO;


/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MyServiceXMLHandler {
    private static final Logger logger = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;
    
    public String generateSupplementaryServiceInquiryMessage(MyServiceInquiryRequestVO requestVO) throws MobilyCommonException{
      	 String requestMessage = "";
    	 try{
    	 	logger.debug("MyServiceXMLHandler > generateXMLRequest > Called for MSISDN = ["+requestVO.getMsisdn()+"]");
            if (requestVO.getMsisdn() == null
                    || requestVO.getMsisdn().equalsIgnoreCase("")) {
                logger.fatal("MyServiceXMLHandler > generate XML Message > Number Not Found....");
                throw new MobilyCommonException(ErrorIfc.NUMBER_NOT_FOUND_ECEPTION);
            }
                
            Document doc = new DocumentImpl();
            Element root = doc.createElement(TagIfc.MAIN_EAI_TAG_NAME);
            Element header = XMLUtility.openParentTag(doc,TagIfc.EAI_HEADER_TAG);
            	XMLUtility.generateElelemt(doc, header, TagIfc.MSG_FORMAT,ConstantIfc.SUPP_SERVICE_MSF_FORMATE);
            	XMLUtility.generateElelemt(doc, header, TagIfc.MSG_VERSION,ConstantIfc.MESSAGE_VERSION_VALUE);
            	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_CHANNEL_ID, ConstantIfc.CHANNEL_ID_VALUE);
            	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_CHANNEL_FUNCTION,ConstantIfc.SUPPLEMENTARY_SERVICE_CHANNEL_FUNC_VALUE);
            	XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_USER_ID,requestVO.getRequestorUserId());
            	XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_LANG,ConstantIfc.LANG_VALUE);
            	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_SECURITY_INFO,ConstantIfc.SECURITY_INFO_VALUE);
            	XMLUtility.generateElelemt(doc, header, TagIfc.RETURN_CODE,ConstantIfc.SUPPLEMENTARY_SERVICE_RETURN_CODE_VALUE);
            XMLUtility.closeParentTag(root, header);
            //body
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_MSISDN, requestVO.getMsisdn());
            doc.appendChild(root);
            
            requestMessage = XMLUtility.serializeRequest(doc);

    	 }catch(Exception e){
    	   //throw new SystemException(e);
    	 	throw new MobilyCommonException(e);
    	 }
            
    	 logger.debug("MyServiceXMLHandler > generateXMLRequest > done for MSISDN = ["+requestVO.getMsisdn()+"] and message = >"+requestMessage);
    		 return requestMessage;
    }

    
    /**
	 * responsible for the following
	 * 1- create xml request for  simple inquiry
	 * 2- create xml request for  fully inquiry based on Func_ID & transaction Type
	 * 
	 * @return
	 * @throws MobilyServicesException
	 */
	public   String generateMobilyPromotionXMLRequest(MyServiceInquiryRequestVO requestVO)throws MobilyCommonException {
		String xmlRequest = "";
		try {
			Document doc = new DocumentImpl();
			Element root = doc.createElement(TagIfc.TAG_MOBILY_BSL_SR);

			//gernerate Header element
			Element header = generateHeaderRequest(doc,requestVO);
			XMLUtility.closeParentTag(root, header);

			//body
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_MSISDN,requestVO.getMsisdn());
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_ACCOUNT_NUMBER, "");
			XMLUtility.generateElelemt(doc, root, TagIfc.MOBILY_PROMOTION_ID, "");
			XMLUtility.generateElelemt(doc, root, TagIfc.MOBILY_PROMOTION_SIM_TYPE, "");
			XMLUtility.generateElelemt(doc, root, TagIfc.MOBILY_PROMOTION_VANITY, "");
			XMLUtility.generateElelemt(doc, root, TagIfc.MOBILY_PROMOTION_PRICING_PLAN, "");
			XMLUtility.generateElelemt(doc, root, TagIfc.CUSTOMER_TYPE, requestVO.getCustomerType());
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_GENDER, "");
			XMLUtility.generateElelemt(doc, root, TagIfc.PACKAGE_ID, requestVO.getPackageId());
			XMLUtility.generateElelemt(doc, root, TagIfc.MOBILY_PROMOTION_ACTIVATION_TIME, "");
			XMLUtility.generateElelemt(doc, root, TagIfc.MOBILY_PROMOTION_ISACKNOWLEDGABLE, ""+ConstantIfc.MOBILY_PROMOTION_ACKNOWLADGE);
			XMLUtility.generateElelemt(doc, root, TagIfc.MOBILY_PROMOTION_EPLY_TO_QUEUE_NAME, "");
			XMLUtility.generateElelemt(doc, root, TagIfc.MOBILY_PROMOTION_EPLY_TO_QUEUE_MANAGER_NAME, "");
			doc.appendChild(root);
			xmlRequest = XMLUtility.serializeRequest(doc);

		} catch (Exception e) {
			throw new MobilyCommonException(e);
		}
		return xmlRequest;
	}
	
	
	/**
	 * responsible for generate xml header tag , this method is generate header tag thats common for all royalty request
	 * @param doc
	 * @param requestVO
	 * @return org.w3c.dom.Element
	 * @throws MobilyServicesException
	 */
	private  Element generateHeaderRequest(Document doc , MyServiceInquiryRequestVO requestVO) throws MobilyCommonException{
		Element header = null;
		try{
			String srdate = MobilyUtility.FormateDate(new Date());
			header = XMLUtility.openParentTag(doc,TagIfc.TAG_SR_HEADER);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID,""+ConstantIfc.MOBILY_PROMOTION_INQUIRY);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SECURITY_KEY,ConstantIfc.SUPPLEMENTARY_SERVICE_SECURITY_KEY);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_CHANNEL_TRANS_ID,"ePortal"+srdate);
			XMLUtility.generateElelemt(doc, header, TagIfc.MSG_VERSION,ConstantIfc.MESSAGE_VERSION_VALUE);
			XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_CHANNEL_ID,ConstantIfc.CHANNEL_ID_VALUE);
			XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_USER_ID,requestVO.getRequestorUserId());
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SR_DATE,srdate);
			XMLUtility.generateElelemt(doc, header, TagIfc.MOBILY_PROMOTION_BACK_END,ConstantIfc.CHANNEL_ID_BSL_VALUE);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_OVERWRITE_OPEN_ORDER,ConstantIfc.PROMOTION_OVERWRITE_VALUE);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_CHARGEABLE,"");
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_CHARGE_AMOUNT,ConstantIfc.PROMOTION_CHARGE_AMOUNT_VALUE);
			
		}catch(Exception e){
			throw new MobilyCommonException(e);
		}
		return header;
	}

    /**
	 * responsible for the following creating xmlrequest for wifi roaming inquiry
	 * @param  requestVO
	 * @return
	 * @throws MobilyServicesException
	 * 
	 * 
	 * <MOBILY_BSL_MESSAGE>
			<BSL_HEADER>
				<FuncId>SUPPLEMENTARY_SERVICE</FuncId>
				<SecurityKey></SecurityKey>
				<MsgVersion>0001</MsgVersion>
				<RequestorChannelId>BSL</RequestorChannelId>
				<SrDate>20091107155539</SrDate>
				<RequestorUserId>BSL</RequestorUserId>
				<RequestorLanguage>A</RequestorLanguage>
				<ReturnCode>0000</ReturnCode>
				<ReturnDesc></ReturnDesc>
			</BSL_HEADER>
			<ChannelTransId>TEST_SR_8620480</ChannelTransId>
			<MSISDN>966540511280</MSISDN>
			<ServiceName>WIFI_Roaming</ServiceName>
		    <UserName>966569950402@mobily.com.sa</UserName>
		</MOBILY_BSL_MESSAGE>
	 */
	public String generateWifiStatusXMLRequest(MyServiceInquiryRequestVO requestVO)throws MobilyCommonException {
		String xmlRequest = "";
		try {
			Document doc = new DocumentImpl();
			Element root = doc.createElement(TagIfc.TAG_MOBILY_BSL_MESSAGE);

			//gernerate Header element
			String srdate = MobilyUtility.FormateDate(new Date());
			Element header = XMLUtility.openParentTag(doc,TagIfc.TAG_MOBILY_BSL_HEADER);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID,""+ConstantIfc.CONTENT_PROVIDER_FUNC_VALUE);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SECURITY_KEY,ConstantIfc.SUPPLEMENTARY_SERVICE_SECURITY_KEY);
				XMLUtility.generateElelemt(doc, header, TagIfc.MSG_VERSION,ConstantIfc.MESSAGE_VERSION_VALUE);
				XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_CHANNEL_ID,ConstantIfc.CHANNEL_ID_VALUE);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SR_DATE,srdate);
				XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_USER_ID,requestVO.getRequestorUserId());
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_LANGUAGE,ConstantIfc.LANG_VALUE);
				XMLUtility.generateElelemt(doc, header, TagIfc.RETURN_CODE,ConstantIfc.RETURN_CODE_VALUE);
				XMLUtility.generateElelemt(doc, header, TagIfc.RETURN_DESC,"");
			XMLUtility.closeParentTag(root, header);

			//body
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_CHANNEL_TRANS_ID,ConstantIfc.CHANNEL_ID_VALUE+srdate);
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_MSISDN,requestVO.getMsisdn());
		    XMLUtility.generateElelemt(doc, root, TagIfc.TAG_SERVICE_NAME, ConstantIfc.SERVICE_VALUE);
			XMLUtility.generateElelemt(doc, root, TagIfc.USER_NAME,requestVO.getMsisdn()+ConstantIfc.USER_NAME_SUFFIX);

			doc.appendChild(root);
			xmlRequest = XMLUtility.serializeRequest(doc);

		} catch (Exception e) {
			throw new MobilyCommonException(e);
		}
		return xmlRequest;
	}

	/**
     * This Method is responsible for parsing the promotion inquiry reply message 
     * @param xmlReply
     * @return
     * @throws MobilyServiceException
     */
    public MobilyPromotionVO ParsingMobilyPromotionInquiryReplyXML(String xmlReply) throws MobilyCommonException {
        MobilyPromotionVO objectVO = null;
        logger
                .debug("MyServiceXMLHandler > ParsingMobilyPromotionInquiryReplyXML > Called");
        if (xmlReply == null || xmlReply == "")
            return objectVO;
        try {
            objectVO = new MobilyPromotionVO();
            Document doc = XMLUtility.parseXMLString(xmlReply.trim());
            Node rootNode = doc.getElementsByTagName(TagIfc.TAG_MOBILY_BSL_SR).item(0);
            NodeList nl = rootNode.getChildNodes();
            for (int j = 0; j < nl.getLength(); j++) {
                if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
                    continue;

                Element l_Node = (Element) nl.item(j);
                String p_szTagName = l_Node.getTagName();
                String l_szNodeValue = null;
                if (l_Node.getFirstChild() != null)
                    l_szNodeValue = l_Node.getFirstChild().getNodeValue();

                if(p_szTagName.equalsIgnoreCase(TagIfc.MOBILY_PROMOTION_RETURN_CODES)){
				    NodeList  contentNode = l_Node.getChildNodes();
					for(int i=0; i< contentNode.getLength(); i++){
						Element s_Node = (Element) contentNode.item(i);
						String s_szNodeTagName = s_Node.getTagName();
						String s_szNodeValue = "";
						
						if (s_Node.getFirstChild() != null)
							s_szNodeValue = s_Node.getFirstChild().getNodeValue();
						
						if (s_szNodeTagName.equalsIgnoreCase(TagIfc.RETURN_CODE)){
						    if(Integer.parseInt(s_szNodeValue) > 0 ){
						        //throw new MobilyCommonException(s_szNodeValue);
						    }
						}
					}
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.MOBILY_PROMOTIONS)) {
				    
				    getPromotionList(objectVO ,l_Node );
                }
            }
        } catch (Exception e) {
            logger.fatal("MyServiceXMLHandler > ParsingMobilyPromotionInquiryReplyXML >Exception , " + e);
            throw new MobilyCommonException(e);
        }
        logger.debug("MyServiceXMLHandler > ParsingMobilyPromotionInquiryReplyXML > Done");
        return objectVO;

    }
    
    
    /**
     * @param replyBean
     * @param node
     */
    private void getPromotionList(MobilyPromotionVO objectVO , Element node) {
    	   NodeList  contentNode = node.getChildNodes();
    		for(int i=0; i< contentNode.getLength(); i++){
    			Element s_Node = (Element) contentNode.item(i);
    			String s_szNodeTagName = s_Node.getTagName();
    			if (s_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_PROMOTION_SUBSCRIBED_LIST)){
    			    objectVO.setListOfSubscribedPromotion(getPromotionIdList(s_Node));
    			}else if (s_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_PROMOTION_NOT_SUBSCRIBED_LIST)){
    			    objectVO.setListOfNotSubscribedpromotion(getPromotionIdList(s_Node));
    			}
    		  }
    }


    /**
     * This Method is responsible retreive the promotion id list from promption list tag
     * @param node
     * @return
     */
    private ArrayList getPromotionIdList(Element node) {

           ArrayList listOfId = null;
    	   NodeList  contentNode = node.getChildNodes();
    	    String l_szNodeValue= "";
    	    String s_szNodeTagName = "";
    	    Element s_Node = null;
    		for(int i=0; i< contentNode.getLength(); i++){
    		    s_Node = (Element) contentNode.item(i);
   			 	s_szNodeTagName = s_Node.getTagName();
    			
                if( s_Node.getFirstChild() != null )
                	l_szNodeValue 	 =s_Node.getFirstChild().getNodeValue();
                
    			if (s_szNodeTagName.equalsIgnoreCase(TagIfc.MOBILY_PROMOTION_ID_INQUIRY)){
    			if(!l_szNodeValue.equals("")){    
    			    if(listOfId == null)
    			        listOfId = new ArrayList();
    			    listOfId.add(l_szNodeValue);
    			   }
    			}
    		  }
    	return listOfId;
        
    }
    
    
    
    /**
	 * This Service is Responsible for Parsing the Reply message returned from supplementary service inquiry service
	 * @param xmlResponse
	 * @return
	 * @throws MobilyServiceException
	 */
	public MyServiceInquiryVO  ParsingSupplementaryServiceInquiryMQReply(String xmlResponse) throws MobilyCommonException{
		logger.debug("MyServicesReplyVO > ParsingSupplementaryServiceInquiryMQReply > Called");
		
		MyServiceInquiryVO obj = new MyServiceInquiryVO(); 

		if (xmlResponse == null || xmlResponse == "")
			return null;
		try {
			obj.reset();			
			Document doc = XMLUtility.parseXMLString(xmlResponse.trim());
			Node rootNode = doc.getElementsByTagName(TagIfc.MAIN_EAI_TAG_NAME).item(0);
			obj.setGprs(0);
			NodeList nl = rootNode.getChildNodes();
			MessageHeaderVO headerVO = null;
			for (int j = 0; j < nl.getLength(); j++) {
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				String l_szNodeValue = null;
				if (l_Node.getFirstChild() != null)
					l_szNodeValue = l_Node.getFirstChild().getNodeValue();

				if (p_szTagName.equalsIgnoreCase(TagIfc.EAI_HEADER_TAG)) {
					headerVO = new MessageHeaderVO();
					headerVO = headerVO.parseXMl(l_Node);
					if(Integer.parseInt(headerVO.getReturnCode())!= 0)
					{
						logger.fatal("MyServicesReplyVO > ParsingSupplementaryServiceInquiryMQReply > MQ ServiceInquiry return Error Code : ["+headerVO.getReturnCode()+"]");
						throw new MobilyCommonException(headerVO.getReturnCode());
					}
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_MSISDN)) {
				    obj.setLineNumber(l_szNodeValue);
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SUPPLEMENTARY_SERVICES)) {

					//get The list of Node Status
					NodeList SuppList    = l_Node.getChildNodes();
					String s_szNodeValue = null;
					String s_sStatusValue = null;
					String s_serviceType  = null;
					String planType		  = null;
					String subscriptionUnit = null;
					String subscriptionType = null;
					
					Element sub_s_Node	  = null;
					
					for(int k =0 ; k < SuppList.getLength(); k++){
						Element supplementaryServiceNode = (Element) SuppList.item(k);
						String S_TagName = supplementaryServiceNode.getTagName();

						NodeList supplementaryServiceItemNodes = supplementaryServiceNode.getChildNodes();
						String innertTagName = "";
						for(int m =0 ; m < supplementaryServiceItemNodes.getLength(); m++) {
							sub_s_Node = (Element)supplementaryServiceItemNodes.item(m);
							innertTagName  = sub_s_Node.getTagName();
							if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_NAME)) {
								s_szNodeValue = sub_s_Node.getFirstChild().getNodeValue();
							}else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_STATUS )) {
								s_sStatusValue = sub_s_Node.getFirstChild().getNodeValue();
							}else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_TYPE  )) {
								if(sub_s_Node.getFirstChild()!= null)
									s_serviceType = sub_s_Node.getFirstChild().getNodeValue();
							}
							else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_PLAN_TYPE)) 
							{
								if(sub_s_Node.getFirstChild()!= null)
								{
									planType = sub_s_Node.getFirstChild().getNodeValue();
									obj.setPlanType(planType);
								}
							}
							else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_SUBSCRIPTION_UNIT )) 
							{
								if(sub_s_Node.getFirstChild()!= null)
								{
									subscriptionUnit = sub_s_Node.getFirstChild().getNodeValue();
									obj.setSubscriptionUnit(subscriptionUnit);
								}
							}
							else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_SUBSCRIPTION_TYPE  )) 
							{
								if(sub_s_Node.getFirstChild()!= null)
								{
									subscriptionType = sub_s_Node.getFirstChild().getNodeValue();
									obj.setSubscriptionType(subscriptionType);
								}
							}
						}
                        
						if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GAMING)){
						    obj.setGamingStatus(Integer.parseInt(s_sStatusValue));
							
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_VEDIO_TELEPHONY)){
						    obj.setVideoTellStatus(Integer.parseInt(s_sStatusValue));
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_VEDIO_STREAMING)){
						    obj.setVideoStrmStatus(Integer.parseInt(s_sStatusValue));
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_RBT)){
						    obj.setRbt(Integer.parseInt(s_sStatusValue));
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_PTT)){
						    obj.setPtt(Integer.parseInt(s_sStatusValue));
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_LBS)){
						    obj.setLbs(Integer.parseInt(s_sStatusValue));
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_SMS_BUNDLE)){
							if(Integer.parseInt(s_sStatusValue) == 0) {
							    obj.setSms(Integer.parseInt(s_sStatusValue));
							}else {
								if(s_serviceType.equals(ConstantIfc.SUPPLEMENTARY_SERVICE_SMS_BUNDLE_250 )) {
								    obj.setSms(ConstantIfc.SUPPLEMENTARY_SERVICE_SMS_BUNDLE_250_VALUE );
								}else if(s_serviceType.equals(ConstantIfc.SUPPLEMENTARY_SERVICE_SMS_BUNDLE_450 )) {
								    obj.setSms(ConstantIfc.SUPPLEMENTARY_SERVICE_SMS_BUNDLE_450_VALUE );
								}
							}
						}
						//else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_NATIONAL_SMS_BUNDLE)){
						else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTARY_SERVICE_TWININGS_NATIONAL)){						
							logger.debug("MyServiceXMLHandler NATIONAL_SMS_BUNDLE> ParsingSupplementaryServiceInquiryMQReply > s_szNodeValue="+s_szNodeValue);
							logger.debug("MyServiceXMLHandler NATIONAL_SMS_BUNDLE> ParsingSupplementaryServiceInquiryMQReply > s_sStatusValue="+s_sStatusValue);
							logger.debug("MyServiceXMLHandler NATIONAL_SMS_BUNDLE> ParsingSupplementaryServiceInquiryMQReply > s_serviceType="+s_serviceType);
							if(Integer.parseInt(s_sStatusValue) == 0) {
								logger.debug("MyServiceXMLHandler NATIONAL_SMS_BUNDLE> ParsingSupplementaryServiceInquiryMQReply > s_sStatusValue="+s_sStatusValue);
							    obj.setNationalsms(Integer.parseInt(s_sStatusValue));
							}else {
								if(s_serviceType.equals(ConstantIfc.SUPPLEMENTARY_SERVICE_SMS_BUNDLE_50 )) {
									obj.setNationalsms(ConstantIfc.SUPPLEMENTARY_SERVICE_SMS_BUNDLE_50_VALUE );
									logger.debug(obj.getNationalsms() + " NATIONAL SMS ");
								}else if(s_serviceType.equals(ConstantIfc.SUPPLEMENTARY_SERVICE_SMS_BUNDLE_150 )) {
								    obj.setNationalsms(ConstantIfc.SUPPLEMENTARY_SERVICE_SMS_BUNDLE_150_VALUE );
								    logger.debug(obj.getNationalsms() + " NATIONAL SMS ");
								}
							}
						}
						else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_TV)){
						    obj.setTv(Integer.parseInt(s_sStatusValue));
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_20MB)){
							if(Integer.parseInt(s_sStatusValue) == 1){
							    obj.setGprs(ConstantIfc.MEGA_BUNDLE_ID_20M);
							    obj.setGprsPackage(ConstantIfc.GPRS_OPERATION_20M);
							}
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_1G )){
							if(Integer.parseInt(s_sStatusValue) == 1){
							    obj.setGprs(ConstantIfc.MEGA_BUNDLE_ID_1G);
							    obj.setGprsPackage(ConstantIfc.GPRS_OPERATION_1G);
							}

						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_5MB )){
							if(Integer.parseInt(s_sStatusValue) == 1){
							    obj.setGprs(ConstantIfc.MEGA_BUNDLE_ID_5M);
							    obj.setGprsPackage(ConstantIfc.GPRS_OPERATION_5M);
							}
							
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_30MB )){
							if(Integer.parseInt(s_sStatusValue) == 1){
							    obj.setGprs(ConstantIfc.MEGA_BUNDLE_ID_30M);
							    obj.setGprsPackage(ConstantIfc.GPRS_OPERATION_30M);
							}
							
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_100MB )){
							if(Integer.parseInt(s_sStatusValue) == 1){
							    obj.setGprs(ConstantIfc.MEGA_BUNDLE_ID_100M);
							    obj.setGprsPackage(ConstantIfc.GPRS_OPERATION_100M);
							}
							
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_5G )){
							if(Integer.parseInt(s_sStatusValue) == 1){
							    obj.setGprs(ConstantIfc.MEGA_BUNDLE_ID_5G);
							    obj.setGprsPackage(ConstantIfc.GPRS_OPERATION_5G);
							}
							
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_UNLIMETED )){
							if(Integer.parseInt(s_sStatusValue) == 1){
							    obj.setGprs(ConstantIfc.MEGA_BUNDLE_ID_UNLIMITED);
							    obj.setGprsPackage(ConstantIfc.GPRS_OPERATION_UNLIMITED);
							}
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_HOME_ZONE )){
						    obj.setHomeZoneStatus(Integer.parseInt(s_sStatusValue));
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_VOICE_SMS )){
						    obj.setVoiceSMSStatus(Integer.parseInt(s_sStatusValue));
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_ICS2 )){
						    obj.setIcs2Status(Integer.parseInt(s_sStatusValue));
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_EVMS )){
						    obj.setEvmsStatus(Integer.parseInt(s_sStatusValue));
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTARY_SERVICE_MMS_BUNDLE)){
							logger.debug("MyServiceXMLHandler > ParsingSupplementaryServiceInquiryMQReply > s_szNodeValue="+s_szNodeValue);
							logger.debug("MyServiceXMLHandler > ParsingSupplementaryServiceInquiryMQReply > s_sStatusValue="+s_sStatusValue);
							logger.debug("MyServiceXMLHandler > ParsingSupplementaryServiceInquiryMQReply > s_serviceType="+s_serviceType);
							if(Integer.parseInt(s_sStatusValue) == 0) {
							    obj.setMms(Integer.parseInt(s_sStatusValue));
							}else {
								if(s_serviceType.equalsIgnoreCase(ConstantIfc.SUPPLEMENTARY_SERVICE_MMS_BUNDLE_30 )) {
								    obj.setMms(ConstantIfc.SUPPLEMENTARY_SERVICE_MMS_BUNDLE_30_VALUE );
								}else if(s_serviceType.equalsIgnoreCase(ConstantIfc.SUPPLEMENTARY_SERVICE_MMS_BUNDLE_90 )) {
								    obj.setMms(ConstantIfc.SUPPLEMENTARY_SERVICE_MMS_BUNDLE_90_VALUE );
								}else if(s_serviceType.equalsIgnoreCase(ConstantIfc.SUPPLEMENTARY_SERVICE_MMS_BUNDLE_150 )) {
								    obj.setMms(ConstantIfc.SUPPLEMENTARY_SERVICE_MMS_BUNDLE_150_VALUE );
								}
							}
						} else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTARY_SERVICE_BMS))	{
							obj.setBmsStatus(Integer.parseInt(s_sStatusValue));
						}
						
						//**** connect roaming ******
						// New Added Change for Supplementary services
						else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_CONNECT_ROAMING  ))
						{
							if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_CONNECT_ROAMING ))
							{
								//obj.setConnectRoamingStatus(Integer.parseInt(s_sStatusValue));
								obj.setConnectRoaming(Integer.parseInt(s_sStatusValue));
							}
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTARY_SERVICE_CONSUMER_VOIP))	{ //CON_VOIP
							obj.setConsumerVOIPStatus(Integer.parseInt(s_sStatusValue));
						}else if(s_szNodeValue.equalsIgnoreCase(ConstantIfc.SUPPLEMENTARY_SERVICE_CORP_CONSUMER_VOIP))	{ //CORP_VOIP
							obj.setCorpConsumerVOIPStatus(Integer.parseInt(s_sStatusValue));
						}
					}					  
				}
			}
			
			//check if the service status not exist then this mean the service inactive
			if(headerVO !=null && Integer.parseInt(headerVO.getReturnCode())== 0){
			    checkForUnavilableStatus(obj);
			}
		}  catch (Exception e) {
			logger.fatal("MyServicesReplyVO > ParsingSupplementaryServiceInquiryRequestXML >Exception , "+ e);
			throw new MobilyCommonException(e);
		}
		logger.debug("MyServicesReplyVO > ParsingSupplementaryServiceInquiryMQReply > Done");
		return obj;
	
	}
	
    /**
	 * This Service is Responsible for Parsing the Reply message returned from wifi roaming service inquiry service
	 * @param xmlResponse
	 * @return
	 * @throws MobilyServiceException
	 */
	public MyServiceInquiryVO  ParsingWifiRoamingServiceInquiryMQReply(String xmlResponse) throws MobilyCommonException{
		logger.debug("MyServicesReplyVO > ParsingWifiRoamingServiceInquiryMQReply > Called");
		
		MyServiceInquiryVO obj = new MyServiceInquiryVO(); 

		if (xmlResponse == null || xmlResponse == "")
			return null;
		try {
			obj.reset();			
			Document doc = XMLUtility.parseXMLString(xmlResponse.trim());
			Node rootNode = doc.getElementsByTagName(TagIfc.TAG_MOBILY_BSL_MESSAGE).item(0);
			obj.setGprs(0);
			NodeList nl = rootNode.getChildNodes();
			MessageHeaderVO headerVO = null;
			for (int j = 0; j < nl.getLength(); j++) {
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				String l_szNodeValue = null;
				if (l_Node.getFirstChild() != null)
					l_szNodeValue = l_Node.getFirstChild().getNodeValue();

				if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_MOBILY_BSL_HEADER)) {
					headerVO = new MessageHeaderVO();
					headerVO = headerVO.parseXMl(l_Node);
					if(Integer.parseInt(headerVO.getReturnCode())!= 0)
					{
						logger.fatal("MyServicesReplyVO > ParsingWifiRoamingServiceInquiryMQReply > MQ ServiceInquiry return Error Code : ["+headerVO.getReturnCode()+"]");
						throw new MobilyCommonException(headerVO.getReturnCode());
					}
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_MSISDN)) {
				    obj.setLineNumber(l_szNodeValue);
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SUPP_SERVICES)) {
					//get The list of Node Status
					NodeList SuppList    = l_Node.getChildNodes();
					String s_szNodeValue = null;
					String s_sStatusValue = null;
					String s_serviceType  = null;
					for(int k =0 ; k < SuppList.getLength(); k++){
						Element supplementaryServiceNode = (Element) SuppList.item(k);
						String S_TagName = supplementaryServiceNode.getTagName();
						NodeList supplementaryServiceItemNodes = supplementaryServiceNode.getChildNodes();
						String innertTagName = "";
						for(int m =0 ; m < supplementaryServiceItemNodes.getLength(); m++) {
							Element sub_s_Node = (Element)supplementaryServiceItemNodes.item(m);
							innertTagName  = sub_s_Node.getTagName();
							
							if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_NAME)) {
								obj.setServiceName(sub_s_Node.getFirstChild().getNodeValue());
							}else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUBSCRIPTION_STATUS )) {
								s_sStatusValue = sub_s_Node.getFirstChild().getNodeValue();
							}else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_TYPE  )) {
								if(sub_s_Node.getFirstChild()!= null)
									obj.setWifiServiceType(sub_s_Node.getFirstChild().getNodeValue());
							}
						}
					}					  
				}
			}
			//check if the service status not exist then this mean the service inactive
			if(headerVO !=null && Integer.parseInt(headerVO.getReturnCode())== 0){
			    checkForUnavilableStatus(obj);
			}
		}  catch (Exception e) {
			logger.fatal("MyServicesReplyVO > ParsingWifiRoamingServiceInquiryMQReply >Exception , "+ e);
			throw new MobilyCommonException(e);
		}
		logger.debug("MyServicesReplyVO > ParsingWifiRoamingServiceInquiryMQReply > WiFi Roaming Type >"+obj.getWifiServiceType());
		logger.info("MyServicesReplyVO > ParsingWifiRoamingServiceInquiryMQReply > Done");
		return obj;
	
	}

	
	
	
	/**
	 * This Method is responsible for convert the unavilable flage to unactivated flag
	 * @param replyVO
	 */
	private void checkForUnavilableStatus(MyServiceInquiryVO obj){
		   if(obj.getGamingStatus() == ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS)
		       obj.setGamingStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
			
			if(obj.getGprs() == ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS)
			    obj.setGprs(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
			
			if(obj.getPtt() == ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS)
			    obj.setPtt(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
			
			if(obj.getTv() == ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS)
			    obj.setTv(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
			
			if(obj.getRbt() == ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS)
			    obj.setRbt(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
			
			if(obj.getLbs() == ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS)
			    obj.setLbs(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
			
			if(obj.getVideoStrmStatus() == ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS)
			    obj.setVideoStrmStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
			
			if(obj.getVideoTellStatus() == ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS)
			    obj.setVideoTellStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
			
			if(obj.getSms() == ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS)
			    obj.setSms(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
			
			if(obj.getNationalsms() == ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS)
			    obj.setNationalsms(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
			
//			if(getSmsStatus() == ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS)
//				  setSmsStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
			
			if(obj.getVoiceSMSStatus() == ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS)
			    obj.setVoiceSMSStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
			
			if(obj.getIcs2Status() == ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS)
			    obj.setIcs2Status(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);

	}
	
	
	//************* New Method For The Connect bundle ************
	public MyServiceInquiryVO  ParsingNewSupplementaryServiceInquiryMQReply(String xmlResponse) throws MobilyCommonException
	{
		logger.debug("MyServicesReplyVO > ParsingNewSupplementaryServiceInquiryMQReply > Called");
		
		MyServiceInquiryVO obj = new MyServiceInquiryVO(); 

		if (xmlResponse == null || xmlResponse == "")
			return null;
		try 
		{
			obj.reset();			
			Document doc = XMLUtility.parseXMLString(xmlResponse.trim());
			Node rootNode = doc.getElementsByTagName(TagIfc.MAIN_EAI_TAG_NAME).item(0);
			obj.setGprs(0);
			NodeList nl = rootNode.getChildNodes();
			MessageHeaderVO headerVO = null;

			for (int j = 0; j < nl.getLength(); j++) 
			{
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				String l_szNodeValue = null;
				if (l_Node.getFirstChild() != null)
					l_szNodeValue = l_Node.getFirstChild().getNodeValue();

				if (p_szTagName.equalsIgnoreCase(TagIfc.EAI_HEADER_TAG)) 
				{
					headerVO = new MessageHeaderVO();
					headerVO = headerVO.parseXMl(l_Node);
					if(Integer.parseInt(headerVO.getReturnCode())!= 0)
					{
						logger.fatal("MyServicesReplyVO > ParsingNewSupplementaryServiceInquiryMQReply > MQ ServiceInquiry return Error Code : ["+headerVO.getReturnCode()+"]");
						throw new MobilyCommonException(headerVO.getReturnCode());
					}
				} 
				else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_MSISDN)) 
				{
				    obj.setLineNumber(l_szNodeValue);
				}
				else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SUPPLEMENTARY_SERVICES)) 
				{
					//get The list of Node Status
					NodeList SuppList    = l_Node.getChildNodes();
					String s_szNodeValue = null;
					String s_sStatusValue = null;
					String s_serviceType  = null;
					//ArrayList SupplementaryServicesArray =  new ArrayList();
					
					for(int k =0 ; k < SuppList.getLength(); k++)
					{
						Element supplementaryServiceNode = (Element) SuppList.item(k);
						String S_TagName = supplementaryServiceNode.getTagName();

						NodeList supplementaryServiceItemNodes = supplementaryServiceNode.getChildNodes();
						String innertTagName = "";
						
						// The Supplementary service inner object
						//SupplementaryServiceChildVO supplementaryServiceChildVO = new SupplementaryServiceChildVO();
						
						for(int m =0 ; m < supplementaryServiceItemNodes.getLength(); m++) 
						{
							Element sub_s_Node = (Element)supplementaryServiceItemNodes.item(m);
							innertTagName  = sub_s_Node.getTagName();
							
							if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_NAME)) 
							{
								if(sub_s_Node.getFirstChild()!= null)
								{
									s_szNodeValue = sub_s_Node.getFirstChild().getNodeValue();
									if(s_szNodeValue != null && !s_szNodeValue.equalsIgnoreCase(""))
									{
										//supplementaryServiceChildVO.setServiceName(s_szNodeValue);
										obj.setServiceName(s_szNodeValue);
									}
								}
							}
							else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_STATUS )) 
							{
								if(sub_s_Node.getFirstChild()!= null)
								{
									s_sStatusValue = sub_s_Node.getFirstChild().getNodeValue();
									if(s_sStatusValue != null && !s_sStatusValue.equalsIgnoreCase(""))
									{
										//supplementaryServiceChildVO.setStatus(Integer.parseInt(s_sStatusValue));
										//obj.setConnectRoamingStatus(Integer.parseInt(s_sStatusValue));
										obj.setConnectRoaming(Integer.parseInt(s_sStatusValue));
									}
								}
							}
//							else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_TYPE  )) 
//							{
//								if(sub_s_Node.getFirstChild()!= null)
//								{
//									s_serviceType = sub_s_Node.getFirstChild().getNodeValue();
//									if(s_serviceType != null && !s_serviceType.equalsIgnoreCase(""))
//									{
//										//supplementaryServiceChildVO.setServiceType(s_serviceType);
//										obj.setSubscriptionType(s_serviceType);
//									}
//								}
//							}
							// New Added Change for Supplementary services
							else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_PLAN_TYPE  )) 
							{
								if(sub_s_Node.getFirstChild()!= null)
								{
									String s_servicePlanType = sub_s_Node.getFirstChild().getNodeValue();
									if(s_servicePlanType != null && !s_servicePlanType.equalsIgnoreCase(""))
									{
										//supplementaryServiceChildVO.setPlanType(s_servicePlanType);
										obj.setPlanType(s_servicePlanType);
									}
								}
							}
							else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_SUBSCRIPTION_UNIT  )) 
							{
								if(sub_s_Node.getFirstChild()!= null)
								{
									String subscriptionUnit = sub_s_Node.getFirstChild().getNodeValue();
									if(subscriptionUnit != null && !subscriptionUnit.equalsIgnoreCase(""))
									{
										//supplementaryServiceChildVO.setSubscriptionUnit(subscriptionUnit);
										obj.setSubscriptionUnit(subscriptionUnit);
									}
								}
							}
							else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_SUBSCRIPTION_TYPE  )) 
							{
								if(sub_s_Node.getFirstChild()!= null)
								{
									String s_serviceSubType = sub_s_Node.getFirstChild().getNodeValue();
									
									if(s_serviceSubType != null && !s_serviceSubType.equalsIgnoreCase(""))
									{
										//supplementaryServiceChildVO.setSubscriptionType(s_serviceSubType);
										obj.setSubscriptionType(s_serviceSubType);
									}
								}
							}
						}
						// put supplementary service object to the Array
						//SupplementaryServicesArray.add(supplementaryServiceChildVO);
					}
					// Add the Array to the parent value object 
					//obj.setListOfSupplementaryService(SupplementaryServicesArray);
					//*****************************************
				}
			}
			
			//check if the service status not exist then this mean the service inactive
			if(headerVO !=null && Integer.parseInt(headerVO.getReturnCode())== 0){
			    checkForUnavilableStatus(obj);
			}
		}  catch (Exception e) {
			logger.fatal("MyServicesReplyVO > ParsingSupplementaryServiceInquiryRequestXML >Exception , "+ e);
			throw new MobilyCommonException(e);
		}
		logger.debug("MyServicesReplyVO > ParsingNewSupplementaryServiceInquiryMQReply > Done");
		return obj;
	
	}
	
	public MobilyPromotionVO ParsingSupplementaryServiceReplyXML(String xmlReply) throws MobilyCommonException 
	{
        MobilyPromotionVO objectVO = null;
        logger.debug("MyServiceXMLHandler > ParsingMobilyPromotionInquiryReplyXML > Called");
        
        if (xmlReply == null || xmlReply == "")
        {
            return objectVO;
        }
        try 
        {
            objectVO = new MobilyPromotionVO();
            Document doc = XMLUtility.parseXMLString(xmlReply.trim());
            Node rootNode = doc.getElementsByTagName(TagIfc.TAG_MOBILY_BSL_SR).item(0);
            NodeList nl = rootNode.getChildNodes();
            
            for (int j = 0; j < nl.getLength(); j++) 
            {
                if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
                {
                    continue;
                }
                
                Element l_Node = (Element) nl.item(j);
                String p_szTagName = l_Node.getTagName();
                String l_szNodeValue = null;
                if (l_Node.getFirstChild() != null)
                {
                    l_szNodeValue = l_Node.getFirstChild().getNodeValue();
                }
                if(p_szTagName.equalsIgnoreCase(TagIfc.MOBILY_PROMOTION_RETURN_CODES))
                {
				    NodeList  contentNode = l_Node.getChildNodes();
					for(int i=0; i< contentNode.getLength(); i++)
					{
						Element s_Node = (Element) contentNode.item(i);
						String s_szNodeTagName = s_Node.getTagName();
						String s_szNodeValue = "";
						
						if (s_Node.getFirstChild() != null)
						{
							s_szNodeValue = s_Node.getFirstChild().getNodeValue();
						}
						if (s_szNodeTagName.equalsIgnoreCase(TagIfc.RETURN_CODE))
						{
						    if(Integer.parseInt(s_szNodeValue) > 0 )
						    {
						        //throw new MobilyCommonException(s_szNodeValue);
						    }
						}
					}
				}
                else if (p_szTagName.equalsIgnoreCase(TagIfc.MOBILY_PROMOTIONS)) 
                {
				    getPromotionList(objectVO ,l_Node );
                }
            }
        }
        catch (Exception e) 
        {
            logger.fatal("MyServiceXMLHandler > ParsingMobilyPromotionInquiryReplyXML >Exception , " + e);
            throw new MobilyCommonException(e);
        }
        logger.debug("MyServiceXMLHandler > ParsingMobilyPromotionInquiryReplyXML > Done");
        return objectVO;

    }
	
    /**
	 * This Service is Responsible for Parsing the Reply message returned from supplementary service inquiry service
	 * 
	 * @param xmlResponse
	 * @return serviceInqVo
	 * @throws MobilyCommonException
	 */
	public MyServiceInquiryVO  parsingSupplementaryServiceInquiryReply(String xmlResponse) throws MobilyCommonException {
		logger.info("MyServiceXMLHandler > parsingSupplementaryServiceInquiryReply > Called");
		
		MyServiceInquiryVO serviceInqVo = new MyServiceInquiryVO(); 
		HashMap serviceInfoMap = new HashMap(); 
		MyServiceInfoVO mySerInfoVo = null;
		
		if (xmlResponse == null || xmlResponse == "")
			return null;

		try {
			Document doc = XMLUtility.parseXMLString(xmlResponse.trim());
			Node rootNode = doc.getElementsByTagName(TagIfc.MAIN_EAI_TAG_NAME).item(0);
			NodeList nl = rootNode.getChildNodes();
			MessageHeaderVO headerVO = null;
			
			for (int j = 0; j < nl.getLength(); j++) {
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				String l_szNodeValue = null;
				if (l_Node.getFirstChild() != null)
					l_szNodeValue = l_Node.getFirstChild().getNodeValue();

				if (p_szTagName.equalsIgnoreCase(TagIfc.EAI_HEADER_TAG)) {
					headerVO = new MessageHeaderVO();
					headerVO = headerVO.parseXMl(l_Node);
					if(Integer.parseInt(headerVO.getReturnCode())!= 0) {
						logger.fatal("MyServiceXMLHandler > parsingSupplementaryServiceInquiryReply > MQ ServiceInquiry return Error Code : ["+headerVO.getReturnCode()+"]");
						throw new MobilyCommonException(headerVO.getReturnCode());
					}
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_MSISDN)) {
				    serviceInqVo.setLineNumber(l_szNodeValue);
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SUPPLEMENTARY_SERVICES)) {

					//Get The list of Node Status
						NodeList SuppList    = l_Node.getChildNodes();
						String serviceName = null;
						String statusValue = null;
						String serviceType  = null;
						String planType		  = null;
						String subscriptionUnit = null;
						String subscriptionType = null;
					
					Element sub_s_Node	  = null;
					
					for(int k =0 ; k < SuppList.getLength(); k++){
						Element supplementaryServiceNode = (Element) SuppList.item(k);
						//String S_TagName = supplementaryServiceNode.getTagName();
						mySerInfoVo = new MyServiceInfoVO();
						
						NodeList supplementaryServiceItemNodes = supplementaryServiceNode.getChildNodes();
						String innertTagName = "";
						for(int m =0 ; m < supplementaryServiceItemNodes.getLength(); m++) {
							sub_s_Node = (Element)supplementaryServiceItemNodes.item(m);
							innertTagName  = sub_s_Node.getTagName();
							
							if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_NAME)) {
								serviceName = sub_s_Node.getFirstChild().getNodeValue();
							} else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_STATUS )) {
								statusValue = sub_s_Node.getFirstChild().getNodeValue();
							} else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_TYPE  )|| innertTagName.equalsIgnoreCase("BBServiceType")) {
								if(sub_s_Node.getFirstChild()!= null)
									serviceType = sub_s_Node.getFirstChild().getNodeValue();
							} else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_PLAN_TYPE) || innertTagName.equalsIgnoreCase("BBPlaneType")) {
								if(sub_s_Node.getFirstChild()!= null) {
									planType = sub_s_Node.getFirstChild().getNodeValue();
								}
							} else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_SUBSCRIPTION_UNIT )) {
								if(sub_s_Node.getFirstChild()!= null) {
									subscriptionUnit = sub_s_Node.getFirstChild().getNodeValue();
								}
							} else if(innertTagName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_SUBSCRIPTION_TYPE )) {
								if(sub_s_Node.getFirstChild()!= null) {
									subscriptionType = sub_s_Node.getFirstChild().getNodeValue();
								}
							}
						}
						
						if(FormatterUtility.isEmpty(statusValue)){
							statusValue = "0";
						}
							
						//Place the values into VO Object and add this to the hashMap
							mySerInfoVo.setServiceName(FormatterUtility.checkNull(serviceName));
							
								if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTARY_SERVICE_TWININGS)){ // SMS Bundle
									if(Integer.parseInt(statusValue) == 0) {
										mySerInfoVo.setStatus(Integer.parseInt(statusValue));
									}else {
										if(serviceType.equals(ConstantIfc.SUPPLEMENTARY_SERVICE_SMS_BUNDLE_250 )) {
											mySerInfoVo.setStatus(ConstantIfc.SUPPLEMENTARY_SERVICE_SMS_BUNDLE_250_VALUE );
										}else if(serviceType.equals(ConstantIfc.SUPPLEMENTARY_SERVICE_SMS_BUNDLE_450 )) {
											mySerInfoVo.setStatus(ConstantIfc.SUPPLEMENTARY_SERVICE_SMS_BUNDLE_450_VALUE);
										}
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTARY_SERVICE_TWININGS_NATIONAL)){
								//else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_NATIONAL_SMS_BUNDLE)){ //National SMS Bundle
									if(Integer.parseInt(statusValue) == 0) {
										mySerInfoVo.setStatus(Integer.parseInt(statusValue));
									}else {
										if(serviceType.equals(ConstantIfc.SUPPLEMENTARY_SERVICE_SMS_BUNDLE_50 )) {
											mySerInfoVo.setStatus(ConstantIfc.SUPPLEMENTARY_SERVICE_SMS_BUNDLE_50_VALUE );
										}else if(serviceType.equals(ConstantIfc.SUPPLEMENTARY_SERVICE_SMS_BUNDLE_150 )) {
											mySerInfoVo.setStatus(ConstantIfc.SUPPLEMENTARY_SERVICE_SMS_BUNDLE_150_VALUE );
										}
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_20MB)){ // GPRS 
									if(Integer.parseInt(statusValue) == 1){
										mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_20M);
										mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_20M);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_1G )){
									if(Integer.parseInt(statusValue) == 1){
									    mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_1G);
									    mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_1G);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_5MB )){
									if(Integer.parseInt(statusValue) == 1){
									    mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_5M);
									    mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_5M);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_30MB )){
									if(Integer.parseInt(statusValue) == 1){
									    mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_30M);
									    mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_30M);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_100MB )){
									if(Integer.parseInt(statusValue) == 1){
									    mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_100M);
									    mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_100M);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_5G )){
									if(Integer.parseInt(statusValue) == 1){
									    mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_5G);
									    mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_5G);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_UNLIMETED )){
									if(Integer.parseInt(statusValue) == 1){
									    mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_UNLIMITED);
									    mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_UNLIMITED);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_1GB_CORP )){ //New bundles -Start
									if(Integer.parseInt(statusValue) == 1){
									    mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_1G_CORP);
									    mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_1G_CORP);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_2GB_CON)){
									if(Integer.parseInt(statusValue) == 1){
									    mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_2G_CON);
									    mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_2G_CON);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_5GB_CORP )){
									if(Integer.parseInt(statusValue) == 1){
									    mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_5G_CORP);
									    mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_5G_CORP);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_250MB_CON )){
									if(Integer.parseInt(statusValue) == 1){
									    mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_250M_CON);
									    mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_250M_CON);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_500MB_CON )){
									if(Integer.parseInt(statusValue) == 1){
									    mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_500M_CON);
									    mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_500M_CON);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRSCON_5GB )){ //GPRSCON_5GB
									if(Integer.parseInt(statusValue) == 1){
									    mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_5G_CON);
									    mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_CON_5G);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRSCON_2GB_PREMIUM )){ //CON_2GB_PREMIUM
									if(Integer.parseInt(statusValue) == 1){
									    mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_CON_2GB_PREMIUM);
									    mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_CON_2GB_PREMIUM);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_1GB_MUBASHER)){ //1GB_MUBASHER
									if(Integer.parseInt(statusValue) == 1){
									    mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_1GB_MUBASHER);
									    mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_1GB_MUBASHER);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_5GB_MUBASHER )){ //5GB_MUBASHER
									if(Integer.parseInt(statusValue) == 1){
									    mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_5GB_MUBASHER);
									    mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_5GB_MUBASHER);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_UNLIMITED_MUBASHER )){ //UNLIMITED_MUBASHER
									if(Integer.parseInt(statusValue) == 1){
									    mySerInfoVo.setStatus(ConstantIfc.MEGA_BUNDLE_ID_UNLIMITED_MUBASHER);
									    mySerInfoVo.setPackageValue(ConstantIfc.GPRS_OPERATION_UNLIMITED_MUBASHER);
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTARY_SERVICE_MMS_BUNDLE)){ //MMS Bundle
									if(Integer.parseInt(statusValue) == 0) {
									    mySerInfoVo.setStatus(Integer.parseInt(statusValue));
									} else {
										if(serviceType.equalsIgnoreCase(ConstantIfc.SUPPLEMENTARY_SERVICE_MMS_BUNDLE_30 )) {
										    mySerInfoVo.setStatus(ConstantIfc.SUPPLEMENTARY_SERVICE_MMS_BUNDLE_30_VALUE );
										}else if(serviceType.equalsIgnoreCase(ConstantIfc.SUPPLEMENTARY_SERVICE_MMS_BUNDLE_90 )) {
										    mySerInfoVo.setStatus(ConstantIfc.SUPPLEMENTARY_SERVICE_MMS_BUNDLE_90_VALUE );
										}else if(serviceType.equalsIgnoreCase(ConstantIfc.SUPPLEMENTARY_SERVICE_MMS_BUNDLE_150 )) {
										    mySerInfoVo.setStatus(ConstantIfc.SUPPLEMENTARY_SERVICE_MMS_BUNDLE_150_VALUE );
										}
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_CONNECT_TOPUP_MOBILE)
											|| serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_CONNECT_TOPUP_LAPTOP)
												|| serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_CONNECT_TOPUP_LTE)){ //Connect Topup Bundle
									
									if(Integer.parseInt(statusValue) == 0) {
									    mySerInfoVo.setStatus(Integer.parseInt(statusValue));
									} else {
										if(serviceType.equalsIgnoreCase(ConstantIfc.CONNECT_TOPUP_500MB )) {
										    mySerInfoVo.setStatus(ConstantIfc.CONNECT_TOPUP_500MB_VALUE );
										}else if(serviceType.equalsIgnoreCase(ConstantIfc.CONNECT_TOPUP_1GB )) {
										    mySerInfoVo.setStatus(ConstantIfc.CONNECT_TOPUP_1GB_VALUE );
										}else if(serviceType.equalsIgnoreCase(ConstantIfc.CONNECT_TOPUP_2GB )) {
										    mySerInfoVo.setStatus(ConstantIfc.CONNECT_TOPUP_2GB_VALUE );
										}else if(serviceType.equalsIgnoreCase(ConstantIfc.CONNECT_TOPUP_5GB )) {
										    mySerInfoVo.setStatus(ConstantIfc.CONNECT_TOPUP_5GB_VALUE );
										}
									}
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								} 
								/*else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTART_SERVICE_CONNECT_ROAMING  )) { // Connect Roaming
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setStatus(Integer.parseInt(statusValue));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTARY_SERVICE_CONSUMER_VOIP))	{ //CON_VOIP
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setStatus(Integer.parseInt(statusValue));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}else if(serviceName.equalsIgnoreCase(ConstantIfc.SUPPLEMENTARY_SERVICE_CORP_CONSUMER_VOIP))	{ //CORP_VOIP
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setStatus(Integer.parseInt(statusValue));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}*/else{
									mySerInfoVo.setServiceType(FormatterUtility.checkNull(serviceType));
									mySerInfoVo.setStatus(Integer.parseInt(statusValue));
									mySerInfoVo.setPlanType(FormatterUtility.checkNull(planType));
									mySerInfoVo.setSubscriptionType(FormatterUtility.checkNull(subscriptionType));
									mySerInfoVo.setSubscriptionUnit(FormatterUtility.checkNull(subscriptionUnit));
								}

						//check if the service status not exist(-1) then this mean the service inactive(0)
							if(headerVO !=null && Integer.parseInt(headerVO.getReturnCode())== 0){
								replaceUnavilableWithDisableStatus(mySerInfoVo);
							}
								
						logger.debug("MyServiceXMLHandler > parsingSupplementaryServiceInquiryReply > Service Name =["+mySerInfoVo.getServiceName()+"] ,Status =["+mySerInfoVo.getStatus()+"], ServiceType =["+mySerInfoVo.getServiceType()
											+"] PlanType =["+mySerInfoVo.getPlanType()+"], SubscriptionUnit =["+mySerInfoVo.getSubscriptionUnit()+"], SubscriptionTye =["+mySerInfoVo.getSubscriptionType()+"]");

						serviceInfoMap.put(FormatterUtility.checkNull(serviceName), mySerInfoVo);
						
						//Clear the previous values
							serviceType = "";
							planType = "";
							subscriptionUnit = "";
							subscriptionType = "";
					}					  
				}
			}
			//set the MAP to serviceInqVo
				serviceInqVo.setServiceInfoMap(serviceInfoMap);
			
			logger.debug("MyServiceXMLHandler > parsingSupplementaryServiceInquiryReply > serviceInfoMap ="+serviceInfoMap.size());
		}  catch (Exception e) {
			logger.fatal("MyServicesReplyVO > parsingSupplementaryServiceInquiryRequestXML > Exception , "+ e);
			throw new MobilyCommonException(e);
		}
		logger.debug("MyServicesReplyVO > parsingSupplementaryServiceInquiryReply > Done");
		return serviceInqVo;
	
	}
	
	
	/**
	 * This Method is responsible for convert the service unavailable status(-1) to disabled state(0)
	 * 
	 * @param serviceInfoVo
	 */
	private void replaceUnavilableWithDisableStatus(MyServiceInfoVO serviceInfoVo){
			String serviceName = FormatterUtility.checkNull(serviceInfoVo.getServiceName());
			int serviceStatus = serviceInfoVo.getStatus();
			
		   if(ConstantIfc.SUPPLEMENTART_SERVICE_GAMING.equalsIgnoreCase(serviceName)  
				   && ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS == serviceStatus){
			   serviceInfoVo.setStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
		   }else if((serviceName.toLowerCase().indexOf(ConstantIfc.SUPPLEMENTART_SERVICE_GPRS_20MB) != -1)   
				   		&& ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS == serviceStatus){
			   serviceInfoVo.setStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
		   }else if(ConstantIfc.SUPPLEMENTART_SERVICE_PTT.equalsIgnoreCase(serviceName)  
				   && ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS == serviceStatus){
			   serviceInfoVo.setStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
		   }else if(ConstantIfc.SUPPLEMENTART_SERVICE_TV.equalsIgnoreCase(serviceName)  
				   && ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS == serviceStatus){
			   serviceInfoVo.setStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
		   }else if(ConstantIfc.SUPPLEMENTART_SERVICE_RBT.equalsIgnoreCase(serviceName)  
				   && ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS == serviceStatus){
			   serviceInfoVo.setStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
		   }else if(ConstantIfc.SUPPLEMENTART_SERVICE_LBS.equalsIgnoreCase(serviceName)  
				   && ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS == serviceStatus){
			   serviceInfoVo.setStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
		   }else if(ConstantIfc.SUPPLEMENTART_SERVICE_VEDIO_STREAMING.equalsIgnoreCase(serviceName)  
				   && ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS == serviceStatus){
			   serviceInfoVo.setStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
		   }else if(ConstantIfc.SUPPLEMENTART_SERVICE_VEDIO_TELEPHONY.equalsIgnoreCase(serviceName)  
				   && ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS == serviceStatus){
			   serviceInfoVo.setStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
		   }else if(ConstantIfc.SUPPLEMENTART_SERVICE_SMS_BUNDLE.equalsIgnoreCase(serviceName)  
				   && ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS == serviceStatus){
			   serviceInfoVo.setStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
		   }else if(ConstantIfc.SUPPLEMENTART_SERVICE_NATIONAL_SMS_BUNDLE.equalsIgnoreCase(serviceName)  
				   && ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS == serviceStatus){
			   serviceInfoVo.setStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
		   }else if(ConstantIfc.SUPPLEMENTART_SERVICE_VOICE_SMS.equalsIgnoreCase(serviceName)  
				   && ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS == serviceStatus){
			   serviceInfoVo.setStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
		   }else if(ConstantIfc.SUPPLEMENTART_SERVICE_ICS2.equalsIgnoreCase(serviceName)  
				   && ConstantIfc.SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS == serviceStatus){
			   serviceInfoVo.setStatus(ConstantIfc.SUPPLEMENTART_SERVICE_DEACTIVE_STATUS);
		   }
	}

	
	/**
	 * Generate XML request for Roaming Call Inquiry
	 * 
	 * @param requestVO
	 * @return

		<siebel-xmlext-query-req>
            <search-spec>
                <node node-type="Binary Operator">AND
                    <node node-type="Binary Operator">AND
                        <node node-type="Binary Operator">=
                            <node node-type="Identifier">Login Id</node>
                            <node node-type="Constant" value-type="TEXT">Eportal</node>
                        </node>
                        <node node-type="Binary Operator">=
                            <node node-type="Identifier">Function Id</node>
                            <node node-type="Constant" value-type="TEXT">1183</node>
                        </node>
                    </node>
                    <node node-type="Binary Operator">=
                        <node node-type="Identifier">MSISDN</node>
                        <node node-type="Constant" value-type="TEXT">966540511206</node>
                    </node>
                </node>
            </search-spec>
		</siebel-xmlext-query-req>
	 */
	public String generateRoamingCallInqXMLRequest(String lineNumber){
		String xmlRequest = "";
		try {
			logger.debug("MyServicesReplyVO > generateRoamingCallInqXMLRequest > called....");
			Document doc = new DocumentImpl();
	
			Text andText = doc.createTextNode(ConstantIfc.ROAMING_CALL_OPERATOR_AND);
			Text andText1 = doc.createTextNode(ConstantIfc.ROAMING_CALL_OPERATOR_AND);
			Text euqlOperator = doc.createTextNode(ConstantIfc.ROAMING_CALL_OPERATOR_EQUAL);
			Text euqlOperator1 = doc.createTextNode(ConstantIfc.ROAMING_CALL_OPERATOR_EQUAL);
			Text euqlOperator2 = doc.createTextNode(ConstantIfc.ROAMING_CALL_OPERATOR_EQUAL);
			Text loginId = doc.createTextNode(ConstantIfc.ROAMING_CALL_LOGIN_ID);
			Text loginIdVal = doc.createTextNode(ConstantIfc.ROAMING_CALL_LOGIN_VAL);
			Text functionId = doc.createTextNode(ConstantIfc.ROAMING_CALL_FUNCTION_ID);
			Text functionIdVal = doc.createTextNode(ConstantIfc.ROAMING_CALL_INQ_FUNC_ID);
			Text msisdn = doc.createTextNode(ConstantIfc.ROAMING_CALL_MSISDN);
			Text msisdnVal = doc.createTextNode(lineNumber);
			
			
			Element root = doc.createElement(TagIfc.TAG_SIEBEL_XML_EXT_QUERY_REQ);
				//gernerate Header element
				Element searchSpecHeader = XMLUtility.openParentTag(doc,TagIfc.TAG_SEARCH_SPEC);
					Element loginFuncMsisdnHeader = XMLUtility.openParentTag(doc,TagIfc.TAG_NODE);
						loginFuncMsisdnHeader.setAttribute(TagIfc.TAG_NODE_TYPE, ConstantIfc.ROAMING_CALL_BINARY_OPERATOR);
						loginFuncMsisdnHeader.appendChild(andText);
						
						Element loginFuncHeader = XMLUtility.openParentTag(doc,TagIfc.TAG_NODE);
							loginFuncHeader.setAttribute(TagIfc.TAG_NODE_TYPE, ConstantIfc.ROAMING_CALL_BINARY_OPERATOR);
							loginFuncHeader.appendChild(andText1);
							
							Element loginHeader = XMLUtility.openParentTag(doc,TagIfc.TAG_NODE);
								loginHeader.setAttribute(TagIfc.TAG_NODE_TYPE, ConstantIfc.ROAMING_CALL_BINARY_OPERATOR);
								loginHeader.appendChild(euqlOperator);
								
								Element loginIdEle = doc.createElement(TagIfc.TAG_NODE);
								loginIdEle.setAttribute(TagIfc.TAG_NODE_TYPE, ConstantIfc.ROAMING_CALL_IDENTIFIER);
								loginIdEle.appendChild(loginId);
								loginHeader.appendChild(loginIdEle);
								
								Element loginValEle = doc.createElement(TagIfc.TAG_NODE);
								loginValEle.setAttribute(TagIfc.TAG_NODE_TYPE, ConstantIfc.ROAMING_CALL_CONSTANT);
								loginValEle.setAttribute(TagIfc.TAG_VALUE_TYPE, ConstantIfc.ROAMING_CALL_TEXT);
								loginValEle.appendChild(loginIdVal);
								loginHeader.appendChild(loginValEle);
						
						loginFuncHeader.appendChild(loginHeader);
						
						Element functionHeader = XMLUtility.openParentTag(doc,TagIfc.TAG_NODE);
							functionHeader.setAttribute(TagIfc.TAG_NODE_TYPE, ConstantIfc.ROAMING_CALL_BINARY_OPERATOR);
							functionHeader.appendChild(euqlOperator1);
							
							Element functionIdEle = doc.createElement(TagIfc.TAG_NODE);
							functionIdEle.setAttribute(TagIfc.TAG_NODE_TYPE, ConstantIfc.ROAMING_CALL_IDENTIFIER);
							functionIdEle.appendChild(functionId);
							functionHeader.appendChild(functionIdEle);
							
							Element fucntionValEle = doc.createElement(TagIfc.TAG_NODE);
							fucntionValEle.setAttribute(TagIfc.TAG_NODE_TYPE, ConstantIfc.ROAMING_CALL_CONSTANT);
							fucntionValEle.setAttribute(TagIfc.TAG_VALUE_TYPE, ConstantIfc.ROAMING_CALL_TEXT);
							fucntionValEle.appendChild(functionIdVal);
							functionHeader.appendChild(fucntionValEle);
					
						loginFuncHeader.appendChild(functionHeader);
					
					loginFuncMsisdnHeader.appendChild(loginFuncHeader);
				
					Element msisdnHeader = XMLUtility.openParentTag(doc,TagIfc.TAG_NODE);
						msisdnHeader.setAttribute(TagIfc.TAG_NODE_TYPE, ConstantIfc.ROAMING_CALL_BINARY_OPERATOR);
						msisdnHeader.appendChild(euqlOperator2);
						
						Element msisdnEle = doc.createElement(TagIfc.TAG_NODE);
						msisdnEle.setAttribute(TagIfc.TAG_NODE_TYPE, ConstantIfc.ROAMING_CALL_IDENTIFIER);
						msisdnEle.appendChild(msisdn);
						msisdnHeader.appendChild(msisdnEle);
						
						Element msisdnValEle = doc.createElement(TagIfc.TAG_NODE);
						msisdnValEle.setAttribute(TagIfc.TAG_NODE_TYPE, ConstantIfc.ROAMING_CALL_CONSTANT);
						msisdnValEle.setAttribute(TagIfc.TAG_VALUE_TYPE, ConstantIfc.ROAMING_CALL_TEXT);
						msisdnValEle.appendChild(msisdnVal);
						msisdnHeader.appendChild(msisdnValEle);
					
					loginFuncMsisdnHeader.appendChild(msisdnHeader);
					
					
				searchSpecHeader.appendChild(loginFuncMsisdnHeader);
			
			root.appendChild(searchSpecHeader);
			
			doc.appendChild(root);
			
				xmlRequest = XMLUtility.serializeRequest(doc);
				logger.debug("MyServicesReplyVO > generateRoamingCallInqXMLRequest > XML generated....");
	
		} catch (Exception e) {
		    	throw new SystemException(e);
		}	
		return xmlRequest;
	}
	
	
	
	/**
	 * Parsing xml reply message for Roaming Call inquiry Service
	 * 
		<siebel-xmlext-query-ret>
		  <row>
		    <value field="Busy Divert Number">value</value>
		    <value field="CAT">value</value>
		    <value field="CAW">value</value>
		    <value field="TS22">value</value>
		    <value field="OCS1">value</value>
		    <value field="TS21">value</value>
		    <value field="MPTY">value</value>
		    <value field="Out of Reach Divert Number">value</value>
		    <value field="SOCFRY">value</value>
		    <value field="OSMCSI">value</value>
		    <value field="HOLD">value</value>
		    <value field="PDPCP">value</value>
		    <value field="DBSG">value</value>
		    <value field="AUTHD">value</value>
		    <value field="CLIP">value</value>
		    <value field="BS26">value</value>
		    <value field="LOC">value</value>
		    <value field="PWD">value</value>
		    <value field="Roaming Calls Barring">value</value>
		    <value field="IN Voice Limit">value</value>
		    <value field="SOCFU">value</value>
		    <value field="OFA">value</value>
		    <value field="SOCLIP">value</value>
		    <value field="All Calls Divert Number">value</value>
		    <value field="TCSI">value</value>
		    <value field="Not Answered Divert Number">value</value>
		    <value field="TS11">value</value>
		    <value field="Outgoing Calls Barring">value</value>
		    <value field="CAMEL">value</value>
		    <value field="Network Access Mode">value</value>
		    <value field="TS62">value</value>
		    <value field="VLRID">value</value>
		    <value field="Incoming Calls Barring">value</value>
		    <value field="SOCFB">value</value>
		    <value field="IMSI">value</value>
		    <value field="SOCFRC">value</value>
		    <value field="BS3G">value</value>
		    <value field="GPRS">value</value>
		    <value field="Status">value</value>
		    <value field="Error Code">value</value>
		    <value field="Error Message">value</value>
		  </row>
	  </siebel-xmlext-query-ret>
	 */
	public MyServiceInquiryVO parsingRoamingCallInqXMLReply(String xmlReply, String serviceName){
		logger.debug("MyServicesReplyVO > parsingRoamingCallInqXMLReply > Called");
		MyServiceInquiryVO  tempInquiryVo = null;
	   	try {
			if (xmlReply == null || xmlReply == "")
					throw new MobilyCommonException();
			Document doc = XMLUtility.parseXMLString(xmlReply);
			
			Node rootNode = doc.getElementsByTagName(TagIfc.TAG_SIEBEL_XML_EXT_QUERY_RET).item(0);
			tempInquiryVo = new MyServiceInquiryVO();
			
			NodeList nl = rootNode.getChildNodes();

			for (int j = 0; j < nl.getLength(); j++) {
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				
				if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ROW)) {
					NodeList headerNode = l_Node.getChildNodes();
					Element nodeElement = null;
					String nodeTagName = "";
					String nodeTageValue = "";
					String fieldValue = "";
					
					for(int k=0; k< headerNode.getLength(); k++) {
						nodeTageValue = "";
						fieldValue = "";
						nodeElement = (Element)headerNode.item(k);
						nodeTagName =nodeElement.getTagName();
						
						if (nodeElement.getFirstChild() != null)
							nodeTageValue = nodeElement.getFirstChild().getNodeValue();

						fieldValue = FormatterUtility.checkNull(nodeElement.getAttribute(ConstantIfc.ATTRIBUTE_FIELD));

						logger.debug("MyServicesReplyVO > parsingRoamingCallInqXMLReply > nodeTagName =["+nodeTagName+"], Field =["+fieldValue+"], nodeTageValue =["+nodeTageValue+"]");
						
						if(fieldValue.equalsIgnoreCase(serviceName)) {
							tempInquiryVo.setRoamingCallStatus(!(FormatterUtility.isEmpty(nodeTageValue))? Integer.parseInt(nodeTageValue):1);
						}else if(fieldValue.equalsIgnoreCase(ConstantIfc.KEY_ERROR_CODE)){
							tempInquiryVo.setErrorCode(nodeTageValue);
						}
					}
				}
			}
		} catch (Exception e) {
			logger.fatal("MyServicesReplyVO > parsingRoamingCallInqXMLReply > MobilyServicesException , "+ e);
			throw new SystemException(e);
		} 
		return tempInquiryVo;
	}

	
	/**
	 * 
	 * @param requestVO
	 * @return
	 * @throws MobilyCommonException
	 */
    public String generateSafeArrivStatusInqMessage(MyServiceInquiryRequestVO requestVO) throws MobilyCommonException{
     String requestMessage = "";
   	 try{
   	 	logger.debug("MyServiceXMLHandler > generateSafeArrivStatusInqMessage > Called for MSISDN = ["+requestVO.getMsisdn()+"]");
           if (requestVO.getMsisdn() == null
                   || requestVO.getMsisdn().equalsIgnoreCase("")) {
               logger.fatal("MyServiceXMLHandler > generateSafeArrivStatusInqMessage > Number Not Found....");
               throw new MobilyCommonException(ErrorIfc.NUMBER_NOT_FOUND_ECEPTION);
           }
               
           Document doc = new DocumentImpl();
           Element root = doc.createElement(TagIfc.TAG_MOBILY_BSL_SR);
           Element header = XMLUtility.openParentTag(doc,TagIfc.TAG_SR_HEADER);
            XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID,"");
           	XMLUtility.generateElelemt(doc, header, TagIfc.MSG_FORMAT,ConstantIfc.MESSAGE_VERSION_VALUE);
           	XMLUtility.generateElelemt(doc, header, TagIfc.MSG_VERSION,ConstantIfc.PANDA_MESSAGE_VERSION);
           	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_CHANNEL_ID, ConstantIfc.CHANNEL_ID_VALUE);
           	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_CHANNEL_FUNCTION,ConstantIfc.SUPPLEMENTARY_SERVICE_CHANNEL_FUNC_VALUE);
           	XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_USER_ID,requestVO.getRequestorUserId());
           	XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_LANG,ConstantIfc.LANG_VALUE);
           	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_SECURITY_INFO,ConstantIfc.SECURITY_INFO_VALUE);
           	XMLUtility.generateElelemt(doc, header, TagIfc.RETURN_CODE,ConstantIfc.SUPPLEMENTARY_SERVICE_RETURN_CODE_VALUE);
           XMLUtility.closeParentTag(root, header);
           //body
           XMLUtility.generateElelemt(doc, root, TagIfc.TAG_MSISDN, requestVO.getMsisdn());
           doc.appendChild(root);
           
           requestMessage = XMLUtility.serializeRequest(doc);

   	 }catch(Exception e){
   	   //throw new SystemException(e);
   	 	throw new MobilyCommonException(e);
   	 }
           
   	 logger.debug("MyServiceXMLHandler > generateSafeArrivStatusInqMessage > done for MSISDN = ["+requestVO.getMsisdn()+"] and message = >"+requestMessage);
   		 return requestMessage;
   }

	/**
	 * 
	 * 
	 * <MOBILY_BSL_SR> Destinationlist
			<SR_HEADER>
				<FuncId>General_MMA_Inquiry</FuncId>
				<MsgFormat>0001</MsgFormat>
				<MsgVersion>0000</MsgVersion>
				<RequestorChannelId>Opertion</RequestorChannelId>
				<RequestorChannelFunction>MMA</RequestorChannelFunction>
				<RequestorUserId>BSL</RequestorUserId>
				<RequestorLanguage>E</RequestorLanguage>
				<RequestorSecurityInfo>secure</RequestorSecurityInfo>
				<ReturnCode>0000</ReturnCode>
			</SR_HEADER>
			<COMMAND>GET_SASSUB_COMPREHENSIVE</COMMAND> 
			<InquiryParam>
				<param name=�MSISDN�>966568362300<param>
			</InquiryParam>
		</MOBILY_BSL_SR>

	 * 
	 * @param requestVO
	 * @return
	 * @throws MobilyCommonException 
	 */
	public String generateSafeArrStatAndRecipInqRequestMessage(MyServiceInquiryRequestVO requestVO) throws MobilyCommonException {
		logger.info("MyServiceXMLHandler > generateSafeArrStatAndRecipInqRequestMessage > Called for MSISDN = ["+requestVO.getMsisdn()+"]");
	    String requestMessage = "";
	    
	   	 try{
	           if (FormatterUtility.isEmpty(requestVO.getMsisdn())) {
	               logger.fatal("MyServiceXMLHandler > generateSafeArrivStatusInqMessage > Number Not Found....");
	               throw new MobilyCommonException(ErrorIfc.NUMBER_NOT_FOUND_ECEPTION);
	           }
	               
	           Document doc = new DocumentImpl();
	           Element root = doc.createElement(TagIfc.TAG_MOBILY_BSL_SR);
	           
	           Element header = XMLUtility.openParentTag(doc,TagIfc.TAG_SR_HEADER);
	            XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID,ConstantIfc.SAFE_ARR_MMA_FUNC_ID_VALUE);
	           	XMLUtility.generateElelemt(doc, header, TagIfc.MSG_FORMAT,ConstantIfc.MESSAGE_VERSION_VALUE);
	           	XMLUtility.generateElelemt(doc, header, TagIfc.MSG_VERSION,ConstantIfc.PANDA_MESSAGE_VERSION);
	           	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_CHANNEL_ID, ConstantIfc.CHANNEL_ID_VALUE);
	           	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_CHANNEL_FUNCTION,ConstantIfc.SUPPLEMENTARY_SERVICE_CHANNEL_FUNC_VALUE);
	           	XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_USER_ID,requestVO.getRequestorUserId());
	           	XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_LANG,ConstantIfc.LANG_VALUE);
	           	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_SECURITY_INFO,ConstantIfc.SECURITY_INFO_VALUE);
	           	XMLUtility.generateElelemt(doc, header, TagIfc.RETURN_CODE,ConstantIfc.CUST_INFO_RETURN_CODE_VALUE);
	           XMLUtility.closeParentTag(root, header);
	           
	           //body
	           XMLUtility.generateElelemt(doc, root, TagIfc.TAG_CONTENT_COMMAND, ConstantIfc.SAFE_ARR_MMA_COMMAND);
	           Element headerInqParam = XMLUtility.openParentTag(doc,TagIfc.TAG_INQ_PARAM);
	           	Element paramElement = doc.createElement(TagIfc.TAG_param);
	           		paramElement.setAttribute(TagIfc.TAG_NAME_SMALL, ConstantIfc.MSISDN);
	           		paramElement.setTextContent(FormatterUtility.checkNull(requestVO.getMsisdn()));
	           	headerInqParam.appendChild(paramElement);
	           	
	           XMLUtility.closeParentTag(root, headerInqParam);
	           	
	           doc.appendChild(root);
	           
	           requestMessage = XMLUtility.serializeRequest(doc);

	   	 }catch(Exception e){
	   		logger.fatal("MyServiceXMLHandler > generateSafeArrivStatusInqMessage > Exception >"+e);
	   	 	throw new MobilyCommonException(e);
	   	 }
		 
	  return requestMessage;
	}

	/**
	 * 
	 * 
	 * <MOBILY_BSL_SR_REPLY>
			<SR_HEADER_REPLY>
				<FuncId>General_MMA_Inquiry</FuncId>
				<MsgFormat>0001</MsgFormat>
				<MsgVersion>0000</MsgVersion>
				<RequestorChannelId>Opertion</RequestorChannelId>
				<RequestorChannelFunction>MMA</RequestorChannelFunction>
				<RequestorUserId>BSL</RequestorUserId>
				<RequestorLanguage>E</RequestorLanguage>
				<RequestorSecurityInfo>secure</RequestorSecurityInfo>
				<ReturnCode>0000</ReturnCode>
			</SR_HEADER_REPLY>
			<Subscriptionstatus>1</Subscriptionstatus>
			<ServiceStatus>1</ServiceStatus>
			<DestinationList>
				<Destination>966541919201</Destination>
				<Destination>966541919202</Destination>
				<Destination>966541919203</Destination>
			</DestinationList>
	   </MOBILY_BSL_SR_REPLY>

	 * 
	 * @param xmlreplyMessage
	 * @return
	 * @throws ConnectionErrorException
	 */
	public static MyServiceInquiryVO parsingSafeArrStatAndRecipMessage(String xmlReply) throws MobilyCommonException {
		logger.info("MyServiceXMLHandler > parsingSafeArrStatAndRecipMessage > called"+xmlReply);
		
		MyServiceInquiryVO  replyVO = null;
		
	   	try {
			if (FormatterUtility.isEmpty(xmlReply))
					throw new MobilyCommonException();
			
			Document doc = XMLUtility.parseXMLString(xmlReply);
			
			Node rootNode = doc.getElementsByTagName(TagIfc.TAG_MOBILY_BSL_SR_REPLY).item(0);
			replyVO = new MyServiceInquiryVO();
			
			NodeList nl = rootNode.getChildNodes();

			for (int j = 0; j < nl.getLength(); j++) {
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				String l_szNodeValue = null;
				
				if (l_Node.getFirstChild() != null)
					l_szNodeValue = l_Node.getFirstChild().getNodeValue();

				if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SR_HEADER_REPLY)) {
					NodeList headerNode = l_Node.getChildNodes();
					Element nodeElement = null;
					String nodeTagName = "";
					String nodeTageValue = "";
					
					for(int k=0; k< headerNode.getLength(); k++) {
						nodeTageValue = "";
						nodeElement = (Element)headerNode.item(k);
						nodeTagName =nodeElement.getTagName();
						if (nodeElement.getFirstChild() != null)
							nodeTageValue = nodeElement.getFirstChild().getNodeValue();
						
						if(nodeTagName.equalsIgnoreCase(TagIfc.TAG_RETURN_CODE)) {
							replyVO.setErrorCode(nodeTageValue);
						}
					}
				}else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_SUBSCRIPTION_STATUS)){
					replyVO.setSuppServiceStatus(l_szNodeValue);
				}else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_SERVICE_STATUS)){
					if(MobilyUtility.isIntNumber(l_szNodeValue))
						replyVO.setServiceEnableStatus(Integer.parseInt(l_szNodeValue));
				}else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_DESTINATION_LIST)){
					NodeList destinationNode = l_Node.getChildNodes();
					Element nodeElement = null;
					String nodeTagName = "";
					String nodeTageValue = "";
					ArrayList msisdnList = new ArrayList();
					
					for(int k=0; k < destinationNode.getLength(); k++) {
						nodeTageValue = "";
						nodeElement = (Element)destinationNode.item(k);
						nodeTagName =nodeElement.getTagName();
						if (nodeElement.getFirstChild() != null)
							nodeTageValue = nodeElement.getFirstChild().getNodeValue();
						
						if(nodeTagName.equalsIgnoreCase(TagIfc.TAG_DESTINATION)) {
							msisdnList.add(FormatterUtility.checkNull(nodeTageValue));
						}
					}

					replyVO.setMsisdnList(msisdnList);
				}
			}
		} catch (Exception e) {
			logger.fatal("SupplementaryServiceXMLHandler > parsingSafeArrStatAndRecipMessage > Exception >"+ e);
			throw new SystemException(e);
		} 
		
		return replyVO;
	}
    
    
	/**
	 * 
	 * @param resp
	 * @return
	 * @throws MobilyCommonException
	 */
	private static int getResponseCode(String resp) throws MobilyCommonException {
		logger.info("MyServiceXMLHandler > getResponseCode > called");
		
		if (resp == null || resp.length() == 0)
			throw new MobilyCommonException();

		int res = -1;
		try {
			StringTokenizer tk = new StringTokenizer(resp, ":");
			if (tk.countTokens() == 2) {
				logger.debug("MyServiceXMLHandler > getResponseCode > FreeBalance: Token 1: [" + tk.nextToken() + "]");
				String temp = tk.nextToken();
				logger.debug("MyServiceXMLHandler > getResponseCode > Token 2: [" + temp + "]");
				String rest = temp.substring(0, temp.indexOf(";"));
				logger.debug("MyServiceXMLHandler > getResponseCode > rest >"+rest);
				res = Integer.parseInt(rest);
			} else if (tk.countTokens() > 2) {
				logger.debug("MyServiceXMLHandler > getResponseCode > Got more than two tokens: " + resp);
				logger.debug("MyServiceXMLHandler > getResponseCode > Token 1: [" + tk.nextToken() + "]");
				String temp = tk.nextToken();
				logger.debug("MyServiceXMLHandler > getResponseCode > Token 2: [" + temp + "]");				
				res = Integer.parseInt(temp);				
			}
		} catch (Exception e) {
			logger.debug("MyServiceXMLHandler > getResponseCode > could not parse repsonse. "+e);
			res = -1;
		}
		
		return res;
	}
	
	
	//**************************
    public static void main(String[] args) {
        //String xx = "<MOBILY_BSL_MESSAGE><BSL_HEADER><FuncId>SUPPLEMENTARY_SERVICE</FuncId><SecurityKey>12qweq342r2fwefw3</SecurityKey><MsgVersion>0001</MsgVersion><RequestorChannelId>ePortal</RequestorChannelId><SrDate>20091120105127</SrDate><RequestorUserId>portaladmin</RequestorUserId><RequestorLanguage>E</RequestorLanguage><ReturnDesc></ReturnDesc><ReturnCode>0</ReturnCode></BSL_HEADER><ChannelTransId>ePortal20091120105127</ChannelTransId><MSISDN>966540510196</MSISDN><ServiceName>WIFI_Roaming</ServiceName><SuppServices><Service><ServiceName>WIFI_ROAMING</ServiceName><ServiceType>1WEEK</ServiceType><PlanType>null</PlanType><SubscriptionStatus>1</SubscriptionStatus><ExpiryDate>20091119125341</ExpiryDate><IsMobily>Y</IsMobily><eMail>KJCX@JKHSDLFLK.COM</eMail></Service></SuppServices></MOBILY_BSL_MESSAGE>";
    	//String xx = "<MOBILY_BSL_MESSAGE><BSL_HEADER><FuncId>SUPPLEMENTARY_SERVICE</FuncId><SecurityKey>12qweq342r2fwefw3</SecurityKey><MsgVersion>0001</MsgVersion><RequestorChannelId>ePortal</RequestorChannelId><SrDate>20091119093148</SrDate><RequestorUserId>testrbt</RequestorUserId><RequestorLanguage>E</RequestorLanguage><ReturnDesc></ReturnDesc><ReturnCode>0</ReturnCode></BSL_HEADER><ChannelTransId>ePortal20091119093148</ChannelTransId><MSISDN>966540510966</MSISDN><ServiceName>WIFI_Roaming</ServiceName><SuppServices><Service></Service></SuppServices></MOBILY_BSL_MESSAGE>";
    	
    	String suppXMLReply="<MOBILY_BSL_SR_REPLY><SR_HEADER_REPLY><FuncId>General_MMA_Inquiry</FuncId><MsgFormat>0001</MsgFormat><MsgVersion>0000</MsgVersion><RequestorChannelId>EAI</RequestorChannelId><RequestorChannelFunction>General_MMA_Inquiry</RequestorChannelFunction><RequestorUserId></RequestorUserId><RequestorLanguage>E</RequestorLanguage><RequestorSecurityInfo></RequestorSecurityInfo><ReturnCode>9999</ReturnCode></SR_HEADER_REPLY></MOBILY_BSL_SR_REPLY>";
    	/*    	String xmlReply = "<siebel-xmlext-query-ret><row><value field=\"Busy Divert Number\">0</value><value field=\"CAT\">1</value><value field=\"Roaming Calls Barring\">1</value><value field=\"Error Code\">0000</value><value field=\"Error Message\">value5</value></row></siebel-xmlext-query-ret>";
*/    	
        MyServiceXMLHandler handler = new MyServiceXMLHandler();
        try {
        	//MyServiceInquiryVO objectVO= handler.ParsingWifiRoamingServiceInquiryMQReply(xx);
/*        	MyServiceInquiryVO serviceInqVo = handler.parsingRoamingCallInqXMLReply(xmlReply, ConstantIfc.KEY_ROAMING_CALL);
        	
        	System.out.println(serviceInqVo.getRoamingCallStatus());
        	System.out.println(serviceInqVo.getErrorCode());
*/
 /*       	MyServiceInquiryVO replyVo = handler.parsingSafeArrStatAndRecipMessage("RESP:0:MSISDN,966540510302:SUBSCRIPTIONSTATUS,0:SERVICESTATUS,1:DESTINATIONLIST,966541919201,966541919202,966541919203;");
        	System.out.println(replyVo.getSuppServiceStatus());
        	System.out.println(replyVo.getServiceEnableStatus());
        	System.out.println(replyVo.getMsisdnList());*/
        	
/*        	MyServiceInquiryRequestVO requestVo = new MyServiceInquiryRequestVO();
        	requestVo.setMsisdn("966540510521");
        	System.out.println(handler.generateSafeArrStatAndRecipInqRequestMessage(requestVo));
*/
        	MyServiceInquiryVO serviceInqVo = handler.parsingSafeArrStatAndRecipMessage(suppXMLReply);
        	
        	System.out.println(serviceInqVo.getSuppServiceStatus());
        	System.out.println(serviceInqVo.getServiceEnableStatus());
        	System.out.println(serviceInqVo.getMsisdnList());
        	System.out.println(serviceInqVo.getErrorCode());
        	
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
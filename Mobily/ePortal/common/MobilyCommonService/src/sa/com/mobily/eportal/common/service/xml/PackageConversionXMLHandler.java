package sa.com.mobily.eportal.common.service.xml;

import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.PackageConversionReplyVO;
import sa.com.mobily.eportal.common.service.vo.PackageConversionRequestVO;


/**
 * @author msayed
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class PackageConversionXMLHandler {

	private static final Logger log = LoggerInterface.log;


	/**
	 * Responsible for generate XMl thats responsible for package conversion
	 * the packageConversionVo should have the following values 
	 * 	   1) requestor user Id
	 *     2)MSISDN
	 *     3)change Type where 1 = convert to aness , 2 = convert to wafeer , 3 = convert to Fallah
	 * @param packageConversionVO
	 * @return
	 */
	public static String generateXMLRequest(PackageConversionRequestVO packageConversionVO) throws MobilyCommonException{
		String xmlRequest = "";
		try {
			log
					.debug("PackageConversionXMLHandler > generateXMLRequest > called....");
			Document doc = new DocumentImpl();

			Element root = doc.createElement(TagIfc.TAG_MOBILY_BSL_SR);

			//gernerate Header element
			Element header = XMLUtility.openParentTag(doc,TagIfc.TAG_SR_HEADER);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID,ConstantIfc.PACKAGE_CONVERSION_FUNC_ID_VALUE);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SECURITY_KEY ,ConstantIfc.SECURITY_KEY_VALUE);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_MSG_VERSION,ConstantIfc.MSG_VER_VALUE);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_CHANNEL_ID,ConstantIfc.REQUESTOR_CHANNEL_ID_VALUE);
				String srdate = FormatterUtility.FormateDate(new Date());
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SR_DATE,srdate);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_USER_ID,packageConversionVO.getRequestorUserId());
				XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_LANG,ConstantIfc.REQUESTOR_LANG_VALUE);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_OVER_WRITE_OPEN_ORDER_TAG,"");
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_CHARGEABLE,"");
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_CHARGING_MODE,"");
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_CHARGE_AMOUNT,"");
				
			XMLUtility.closeParentTag(root, header);
			//body
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_MSISDN,packageConversionVO.getMsisdn());
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_CHANGE_TYPE, ""+packageConversionVO.getChangeType());
			doc.appendChild(root);
			xmlRequest = XMLUtility.serializeRequest(doc);

		} catch (java.io.IOException e) {
            log.fatal("PackageConversionXMLHandler > generateXMLRequest >IOException > " + e.getMessage());
            throw new SystemException(e);
        } catch (Exception e) {
            log.fatal("PackageConversionXMLHandler > generateXMLRequest > Exception > "+ e.getMessage());
            throw new SystemException(e);
        }
        
		return xmlRequest;
	}

	
	/**
	 * Paring the resply message from Backend 
	 * check status code & error code where status code = 4 mean accepted and status code = 5 mean rejected
	 * @param xmlReply
	 * @return
	 * @throws MobilyServicesException
	 */
	public static PackageConversionReplyVO parsingXMLReply(String xmlReply) throws MobilyCommonException {
		
		PackageConversionReplyVO replyVO = null;
		try {
			log	.debug("PackageConversionXMLHandler > parsingXMLReply > called....");
			if (xmlReply == null || xmlReply == "")
				throw new MobilyCommonException();;

			replyVO = new PackageConversionReplyVO();
			Document doc = XMLUtility.parseXMLString(xmlReply);

			Node rootNode = doc.getElementsByTagName(TagIfc.TAG_MOBILY_BSL_SR_REPLY).item(0);

			NodeList nl = rootNode.getChildNodes();

			for (int j = 0; j < nl.getLength(); j++) {
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				String l_szNodeValue = null;
				if (l_Node.getFirstChild() != null)
					l_szNodeValue = l_Node.getFirstChild().getNodeValue();

				if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SR_BK_HEADER_REPLY)) {
					int status = getStatusCodeFromxml(l_Node);
					replyVO.setStatus(status);
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_CODE)) {
					replyVO.setErrorCode(l_szNodeValue);
					
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_MSG)) {
					replyVO.setErrorMessage(l_szNodeValue);
					
				} 				
			}
		} catch(Exception e){
		    log.fatal("PackageConversionXMLHandler > parsingXMLReply >Exception > " + e.getMessage());
			throw new SystemException(e);
		}
		
		return replyVO;
	}
	
	/**
	 * responsible for extract status code from the xml String
	 * 
	 * @param l_Node
	 * @return
	 * @throws MobilyServicesException
	 */
	private static int getStatusCodeFromxml(Element l_Node) {
		
		NodeList commonNode = l_Node.getChildNodes();
		int status = 0;

		for (int j = 0; j < commonNode.getLength(); j++) {
			if ((commonNode.item(j)).getNodeType() != Node.ELEMENT_NODE)
				continue;

			Element c_Node = (Element) commonNode.item(j);
			String p_szTagName = c_Node.getTagName();
			String l_szNodeValue = null;
			if (c_Node.getFirstChild() != null)
				l_szNodeValue = c_Node.getFirstChild().getNodeValue();

			if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SR_STATUS)) {
				if (l_szNodeValue != null)
					status = Integer.parseInt(l_szNodeValue);
			}
		}// end for

		return status;
	}
}

/*
 * Created on June 06, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.xml;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.PandaHistoryVO;
import sa.com.mobily.eportal.common.service.vo.PandaReportVO;
import sa.com.mobily.eportal.common.service.vo.ProfileContactVO;
import sa.com.mobily.eportal.common.service.vo.ProfileVO;





/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PandaXMLHandler {
	private static final Logger log = LoggerInterface.log;
	
    /**
     * 
     * Method: generateXMLRequestForDistribution
     * @param requestVO
     * @return String
     * @throws MobilyCommonException
     * @throws MobilyCommonException 
     * 	
     * <MOBILY_BSL_SR>
     * 	<SR_HEADER>
     * 		<FuncId>DISTRIBUTE_MINUTES</FuncId>
     * 		<RequestorChannelId>ePortal</RequestorChannelId>
     * 		<SecurityKey></SecurityKey>
     * 		<MsgVersion>0000</MsgVersion>
     * 		<RequestorUserId>Razzak</RequestorUserId>
     * 		<SrDate>20090414153159</SrDate>
     * 		<Chargeable>N</Chargeable>
     * 		<ChargingMode>Payment</ChargingMode>
     * 		<DealerId>111111</DealerId>
     * 		<ShopId>111111</ShopId>
     * 		<AgentId>Razzak</AgentId>
     * 		<RequestorLanguage></RequestorLanguage>
     * 	</SR_HEADER>
     * 	<ChannelTransId>SR_PANDA_117373</ChannelTransId>
     * <ReplyQueueMgr>EEAX041I</ReplyQueueMgr>
     * <ReplyQueue>EPORTAL.FUNC.PANDA.DISTRIBUTION.ASYNC.REPLY</ReplyQueue>
     * 	<SrInfo>
     * 		<RootAccountNumber>234234234</RootAccountNumber>
     * 
     * 		<DistributionSchema>
     * 			<MSISDN>966569999099</MSISDN>
     * 			<CustomerType>1| 2</CustomerType>
     * 			<PackageId>1142</PackageId>
     * 			<Minutes>500</Minutes>
     * 		</DistributionSchema>
     * 
     * 		<DistributionSchema>
     * 			<MSISDN>966569999098</MSISDN>
     * 			<CustomerType>1| 2</CustomerType>
     * 			<PackageId>1142</PackageId>
     * 			<Minutes>1000</Minutes>
     * 		</DistributionSchema>
     * 
     * 		<DistributionSchema>
     * 			<MSISDN>966569999097</MSISDN>
     * 			<CustomerType>1| 2</CustomerType>
     * 			<PackageId>1142</PackageId>
     * 			<Minutes>200</Minutes>
     * 		</DistributionSchema>
     * 
     * 	</SrInfo>
     * </MOBILY_BSL_SR>
     */    
    public String generateXMLRequestForDistribution(ProfileVO profileVO,PandaHistoryVO pandaHistoryVO) throws MobilyCommonException{
		String xmlRequest = "";
		try {
			log.info("PandaXMLHandler > generateXMLRequestForDistribution > called");
			Document doc = new DocumentImpl();

			Element root = doc.createElement(TagIfc.TAG_MOBILY_BSL_SR);
	      	        
			//gernerate Header element
			Element header = XMLUtility.openParentTag(doc, TagIfc.TAG_SR_HEADER);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID, ConstantIfc.PANDA_DISTRIBUTION_FUNC_VALUE);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_CHANNEL_ID, ConstantIfc.PANDA_REQUESTOR_CHANNEL);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SECURITY_KEY, "");
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_MSG_VERSION, ConstantIfc.PANDA_MESSAGE_VERSION);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_USER_ID, "");
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SR_DATE, pandaHistoryVO.getSrDate());
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_CHARGEABLE, ConstantIfc.PANDA_CHARGEABLE_NO);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_CHARGING_MODE, ConstantIfc.PANDA_MODE_PAYMENT);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_DEALER_ID, ""); 
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SHOP_ID, "");
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_AGENT_ID, "");
			    XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_LANGUAGE, "");
			XMLUtility.closeParentTag(root, header);
			
			//body
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_CHANNEL_TRANS_ID, pandaHistoryVO.getChannelTransId());
			
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_REPLY_QUEUE_MANAGER, MobilyUtility.getMQConfigPropertyValue(ConstantIfc.MQ_QUEUE_MANAGER_NAME));  // Queue Manager Name
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_REPLY_QUEUE, MobilyUtility.getPropertyValue(ConstantIfc.PANDA_DISTRIBUTION_ASYNC_REPLY_QUEUENAME)); //Async Queue Name
			
			if(profileVO.getProfileContactVOList() != null){
				Element msisdnListElement = XMLUtility.openParentTag(doc, TagIfc.TAG_SR_INFO);
				XMLUtility.generateElelemt(doc, msisdnListElement, TagIfc.TAG_ROOT_ACCOUNT_NUMBER, profileVO.getCorporateId());
		    	for(int i = 0; i < profileVO.getProfileContactVOList().size(); i++ ){
		    		Element msisdnElement = XMLUtility.openParentTag(doc, TagIfc.TAG_DISTRIBUTION_SCHEMA);
		    		XMLUtility.generateElelemt(doc, msisdnElement, TagIfc.TAG_MSISDN, ((ProfileContactVO)profileVO.getProfileContactVOList().get(i)).getMsisdn());
		    		XMLUtility.generateElelemt(doc, msisdnElement, TagIfc.CUSTOMER_TYPE, ((ProfileContactVO)profileVO.getProfileContactVOList().get(i)).getCustomerType());
		    		XMLUtility.generateElelemt(doc, msisdnElement, TagIfc.TAG_PACKAGE_ID, ((ProfileContactVO)profileVO.getProfileContactVOList().get(i)).getPackageName());
		    		XMLUtility.generateElelemt(doc, msisdnElement, TagIfc.TAG_DISTRIBUTION_MINUTES, ((ProfileContactVO)profileVO.getProfileContactVOList().get(i)).getAllocatedMinutes());
		    		XMLUtility.closeParentTag(msisdnListElement, msisdnElement);
		    	}			    		
		    	XMLUtility.closeParentTag(root, msisdnListElement);
			}
			
			doc.appendChild(root);
			xmlRequest = XMLUtility.serializeRequest(doc);
			log.info("PandaXMLHandler > generateXMLRequestForDistribution > XML generated");
		} catch (IOException e) {
			log.fatal("PandaXMLHandler > generateXMLRequestForDistribution > Exception >" + e.getMessage());
			throw new SystemException(e);
		}		
		return xmlRequest;		
	}
    
 
 
    /**
     * 
     * Method: parseDistributionXMLReply
     * @param xmlReply
     * @return PandaReplyVO
     *	
     *	<MOBILY_BSL_SR_REPLY>
     *		<SR_HEADER_REPLY>
     *			<FuncId>PROVISION_MINUTES</FuncId>
     *			<SecurityKey></SecurityKey>
     *			<MsgVersion>0000</MsgVersion>
     *			<RequestorChannelId>SIEBEL</RequestorChannelId>
     *			<ReplierChannelId></ReplierChannelId>
     *			<SrDate></SrDate>
     *			<SrRcvDate>20090406171036</SrRcvDate>
     *			<SrStatus>4|3</SrStatus>
     *		</SR_HEADER_REPLY>
     *	
     *		<ChannelTransId>20586169</ChannelTransId>
     *		<ServiceRequestId>SR_1204404741</ServiceRequestId>
     *	
     *		<ErrorCode>0</ErrorCode>
     *		<ErrorMsg></ErrorMsg>
     *	</MOBILY_BSL_SR_REPLY>
     * @throws MobilyCommonException
     *
     */
    public PandaHistoryVO parseDistributionXMLReply(String xmlReply,PandaHistoryVO pandaHistoryVO) throws MobilyCommonException {
		log.info("PandaXMLHandler > parseDistributionXMLReply > xmlReply >"+xmlReply);
		
		try {
			
			if (xmlReply == null || xmlReply == "") {
				throw new MobilyCommonException();
			}

			Document doc = XMLUtility.parseXMLString(xmlReply);
			
			Node rootNode = doc.getElementsByTagName(TagIfc.TAG_MOBILY_BSL_SR_REPLY).item(0);

			NodeList nl = rootNode.getChildNodes();

			for (int j = 0; j < nl.getLength(); j++) {
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				String l_szNodeValue = null;
				if (l_Node.getFirstChild() != null){
					l_szNodeValue = l_Node.getFirstChild().getNodeValue();
				}					
				if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SR_HEADER_REPLY)) {
				    NodeList headerNode = l_Node.getChildNodes();
					Element nodeElement = null;
					String nodeTagName = "";
					String nodeTageValue = "";
					
					for(int k = 0; k < headerNode.getLength(); k++) {
						nodeTageValue = "";
						nodeElement = (Element)headerNode.item(k);
						nodeTagName = nodeElement.getTagName();
						if (nodeElement.getFirstChild() != null)
							nodeTageValue = nodeElement.getFirstChild().getNodeValue();
					
						if(nodeTagName.equalsIgnoreCase(TagIfc.TAG_SR_STATUS)) {
							if(nodeTageValue.equalsIgnoreCase(ConstantIfc.PANDA_SYNC_STATUS_SUCCESS)){
								pandaHistoryVO.setStatus(ConstantIfc.PANDA_SUCCESS_MSG);
							} else {
								pandaHistoryVO.setStatus(ConstantIfc.PANDA_FAILURE_MSG);
							}
						}
					}
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CHANNEL_TRANS_ID)) {
					pandaHistoryVO.setChannelTransId(l_szNodeValue);
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_CODE)) {
				    if(l_szNodeValue != null && Integer.parseInt(l_szNodeValue) >  0){
				    	pandaHistoryVO.setErrorCode(l_szNodeValue);
				    }

				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_MSG)) {
					pandaHistoryVO.setDescription(l_szNodeValue);
				}  
			}
						
		}  catch (DOMException e) {
			log.fatal("PandaXMLHandler > parseDistributionXMLReply > Exception > " + e);
			throw new SystemException(e);
		}
		return pandaHistoryVO;
	}

    /**
     * 
     * Method: generateXMLRequestForAccInquiry
     * @param profileVO
     * @param srDate
     * @return String
     * @throws MobilyCommonException 
     *
     * <MOBILY_BSL_SR>
	 * 		<SR_HEADER>
	 *			<FuncId>PANDA_ACC_BAL_INQ</FuncId> 
	 *			<RequestorChannelId>ePortal</RequestorChannelId> 
	 *			<SecurityKey /> 
	 *			<MsgVersion>0000</MsgVersion> 
	 *			<RequestorUserId>Razzak</RequestorUserId> 
	 *			<SrDate>20090414153159</SrDate> 
	 *			<RequestorLanguage/> 
	 *		</SR_HEADER>
	 *		<RootAccountNumber /> 
	 * 	</MOBILY_BSL_SR>
     */
    public String generateXMLRequestForAccInquiry(String corporateId,String srDate) throws MobilyCommonException {
		String xmlRequest = "";
		try {
			log.info("PandaXMLHandler > generateXMLRequestForAccInquiry > called");
			Document doc = new DocumentImpl();
			Element root = doc.createElement(TagIfc.TAG_MOBILY_BSL_SR);
	        	        
			//gernerate Header element
			Element header = XMLUtility.openParentTag(doc, TagIfc.TAG_SR_HEADER);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID, ConstantIfc.PANDA_ACC_BAL_INQ_FUNC_VALUE);				
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_CHANNEL_ID, ConstantIfc.PANDA_REQUESTOR_CHANNEL);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SECURITY_KEY, "");
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_MSG_VERSION, ConstantIfc.PANDA_MESSAGE_VERSION);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_USER_ID, "");
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SR_DATE, srDate);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_LANGUAGE, "");
			XMLUtility.closeParentTag(root, header);
			
			//body
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_ROOT_ACCOUNT_NUMBER, corporateId);
			
			doc.appendChild(root);
			xmlRequest = XMLUtility.serializeRequest(doc);
			log.info("PandaXMLHandler > generateXMLRequestForAccInquiry > XML generated");
		} catch (IOException e) {
			log.fatal("PandaXMLHandler > generateXMLRequestForAccInquiry > Exception >" + e.getMessage());
			throw new SystemException(e);
		}		
		return xmlRequest;		
	}
    
    /**
     * 
     * Method: parseInquiryXMLReply
     * @param xmlReply
     * @return PandaReplyVO
     *
     * <MOBILY_BSL_SR_REPLY>
     * 		<SR_HEADER_REPLY>
     *			<FuncId>PANDA_ACC_BAL_INQ</FuncId>
     *			<SecurityKey></SecurityKey>
     *			<MsgVersion>0000</MsgVersion>
     *			<RequestorChannelId>SIEBEL</RequestorChannelId>
     *			<ReplierChannelId>EAI</ReplierChannelId>
     *			<SrDate>20090414153159</SrDate>
     *			<SrRcvDate>20090406171036</SrRcvDate>
     *		</SR_HEADER_REPLY>
     *		<ErrorCode>0</ErrorCode> 
     *		<ErrorMsg>Success</ErrorMsg>
     *		<BalanceMinutes>20586169</BalanceMinutes >
     *		<RootAccountNumber>2453450586169</RootAccountNumber>
     *		<TierId>1|2|3</ TierId>
     *		<ContractId>xxxx</ContractId>
     *		<ContractStartDate>20090414153159</ ContractStartDate>
     *      <ContractEndDate>20090414153159</ ContractEndDate>
     *      <TotalContractMinutes>10000000</ TotalContractMinutes>
     * </MOBILY_BSL_SR_REPLY>
     * @throws MobilyCommonException
     * 
     */
    public PandaHistoryVO parseAccInquiryXMLReply(String xmlReply,PandaHistoryVO pandaHistoryVO) throws MobilyCommonException {
		//log.info("PandaXMLHandler > parseAccInquiryXMLReply > xmlReply >"+xmlReply);

		try {
			
			if (xmlReply == null || xmlReply == "") {
				throw new MobilyCommonException();
			}

			Document doc = XMLUtility.parseXMLString(xmlReply);
			
			Node rootNode = doc.getElementsByTagName(TagIfc.TAG_MOBILY_BSL_SR_REPLY).item(0);

			NodeList nl = rootNode.getChildNodes();

			for (int j = 0; j < nl.getLength(); j++) {
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				String l_szNodeValue = null;
				if (l_Node.getFirstChild() != null){
					l_szNodeValue = l_Node.getFirstChild().getNodeValue();
				}					
				if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SR_HEADER_REPLY)) {
						//
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_CODE)) {
				    if(l_szNodeValue != null && Integer.parseInt(l_szNodeValue) >  0) {
				    	throw new MobilyCommonException(l_szNodeValue);
				    }
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_MSG)) {
					if(l_szNodeValue != null && l_szNodeValue.length()>0){
						pandaHistoryVO.setDescription(l_szNodeValue);
					}
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_BALANCE_MINUTES)) {
					if(l_szNodeValue != null && l_szNodeValue.length()>0){
						pandaHistoryVO.setTotalUnconsumedMinutes(l_szNodeValue);
					}
				} else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_CONTRACT_END_DATE)){
					if(l_szNodeValue != null && l_szNodeValue.length()>0) {
						pandaHistoryVO.setContractEndDate(l_szNodeValue);
					}
				} else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_TOTAL_MINUTES)){
					if(l_szNodeValue != null && l_szNodeValue.length()>0) {
						pandaHistoryVO.setTotalContractMinutes(l_szNodeValue);
					}
				}
			}
						
		}  catch (DOMException e) {
			log.fatal("PandaXMLHandler > parseAccInquiryXMLReply > Exception " + e);
			throw new SystemException(e);
		}
		return pandaHistoryVO;
	}
    
    /**
     * 
     * Method: generateXMLRequestForMSISDNInquiry
     * @param pandaReportVO
     * @return String
     * @throws MobilyCommonException 
     *
     * <MOBILY_BSL_SR>
	 * 		<SR_HEADER>
	 *			<FuncId>PANDA_MSISDN_BAL_INQ</FuncId> 
	 *			<RequestorChannelId>ePortal</RequestorChannelId> 
	 *			<SecurityKey /> 
	 *			<MsgVersion>0000</MsgVersion> 
	 *			<RequestorUserId>Razzak</RequestorUserId> 
	 *			<SrDate>20090414153159</SrDate> 
	 *			<RequestorLanguage/> 
	 *		</SR_HEADER>
	 *		<MSISDN>966540511719</MSISDN>
	 *	    <CustomerType>1|2</CustomerType>
	 *	    <FromDt>20090501000000</FromDt>
	 *	    <ToDt>20090601000000</ToDt>
	 * 	</MOBILY_BSL_SR>
     */
    public String generateXMLRequestForMSISDNInquiry(PandaReportVO pandaReportVO) throws MobilyCommonException {
		String xmlRequest = "";
		try {
			log.info("PandaXMLHandler > generateXMLRequestForMSISDNInquiry > called");
			Document doc = new DocumentImpl();
			Element root = doc.createElement(TagIfc.TAG_MOBILY_BSL_SR);
	        	        
			//gernerate Header element
			Element header = XMLUtility.openParentTag(doc, TagIfc.TAG_SR_HEADER);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID, ConstantIfc.PANDA_MSISDN_BAL_INQ_FUNC_VALUE);				
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_CHANNEL_ID, ConstantIfc.PANDA_REQUESTOR_CHANNEL);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SECURITY_KEY, "");
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_MSG_VERSION, ConstantIfc.PANDA_MESSAGE_VERSION);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_USER_ID, "");
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SR_DATE, pandaReportVO.getSrDate());
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_LANGUAGE, "");
			XMLUtility.closeParentTag(root, header);
			
			//body
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_MSISDN, pandaReportVO.getMsisdn());
			XMLUtility.generateElelemt(doc, root, TagIfc.CUSTOMER_TYPE, pandaReportVO.getCustomerType());
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_FROM_DATE, pandaReportVO.getFromDate());
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_TO_DATE, pandaReportVO.getToDate());
			
			doc.appendChild(root);
			xmlRequest = XMLUtility.serializeRequest(doc);
			log.info("PandaXMLHandler > generateXMLRequestForMSISDNInquiry > XML generated");
		} catch (IOException e) {
			log.fatal("PandaXMLHandler > generateXMLRequestForMSISDNInquiry > Exception >" + e.getMessage());
			throw new SystemException(e);
		}		
		return xmlRequest;		
	}
    
    /**
     * 
     * Method: parseMSISDNInquiryXMLReply
     * @param xmlReply
     * @return pandaReportVO
     *
     * <MOBILY_BSL_SR_REPLY>
     * 		<SR_HEADER_REPLY>
     *			<FuncId>PANDA_MSISDN_BAL_INQ</FuncId>
     *			<SecurityKey></SecurityKey>
     *			<MsgVersion>0000</MsgVersion>
     *			<RequestorChannelId>SIEBEL</RequestorChannelId>
     *			<ReplierChannelId>EAI</ReplierChannelId>
     *			<SrDate>20090414153159</SrDate>
     *			<SrRcvDate>20090406171036</SrRcvDate>
     *		</SR_HEADER_REPLY>
     *		<MSISDN>966540511719</MSISDN>
     *		<DistributedMints>20586169</DistributedMints>
     *		<CurrentBalMints>2058</CurrentBalMints>
     *		<ErrorCode>0</ErrorCode> 
     *		<ErrorMsg>Success</ErrorMsg>
     * </MOBILY_BSL_SR_REPLY>
     * @throws MobilyCommonException
     * 
     */
    public PandaReportVO parseMSISDNInquiryXMLReply(String xmlReply,PandaReportVO pandaReportVO) throws MobilyCommonException {
		log.info("PandaXMLHandler > parseMSISDNInquiryXMLReply > xmlReply >"+xmlReply);
		
		try {
			
			if (xmlReply == null || xmlReply == "") {
				throw new MobilyCommonException();
			}

			Document doc = XMLUtility.parseXMLString(xmlReply);
			
			Node rootNode = doc.getElementsByTagName(TagIfc.TAG_MOBILY_BSL_SR_REPLY).item(0);

			NodeList nl = rootNode.getChildNodes();

			for (int j = 0; j < nl.getLength(); j++) {
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				String l_szNodeValue = null;
				if (l_Node.getFirstChild() != null){
					l_szNodeValue = l_Node.getFirstChild().getNodeValue();
				}					
				if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SR_HEADER_REPLY)) {
						//
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_CODE)) {
				    if(l_szNodeValue != null && Integer.parseInt(l_szNodeValue) >  0) {
				    	throw new MobilyCommonException(l_szNodeValue);
				    }
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_MSG)) {
					if(l_szNodeValue != null && l_szNodeValue.length()>0){
						pandaReportVO.setErrorMsg(l_szNodeValue);
					}
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_MSISDN)) {
					if(l_szNodeValue != null && l_szNodeValue.length()>0){
					}
				} else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_DISTRIBUTED_MINUTES)){
					if(l_szNodeValue != null && l_szNodeValue.length()>0) {
						pandaReportVO.setTotalAllocatedMinutes(l_szNodeValue);
					}
				} else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_CURRENT_BALANCE_MINUTES)){
					if(l_szNodeValue != null && l_szNodeValue.length()>0) {
						pandaReportVO.setTotalRemainingMinutes(l_szNodeValue);
					}
				}
			}
			
		}  catch (DOMException e) {
			log.fatal("PandaXMLHandler > parseMSISDNInquiryXMLReply > Exception " + e);
			throw new SystemException(e);
		}
		return pandaReportVO;
	}

}

package sa.com.mobily.eportal.common.util.errorCodeUtil;

import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.dao.AppErrorCodeInfoDAO;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.valueobject.common.AppErrorCodeInfoVO;

public class ErrorCodeUtils
{

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	
	private static final String className = ErrorCodeUtils.class.getName();
	
	public AppErrorCodeInfoVO getErrorInfoByErrorCode(String errorCode){
		executionContext.audit(Level.INFO,"ErrorCodeUtils >> getErrorInfoByErrorCode >errorCode::"+errorCode,"getErrorInfoByErrorCode",className);
		return AppErrorCodeInfoDAO.getInstance().getErrorInfoByErrorCode(errorCode);
	}

}

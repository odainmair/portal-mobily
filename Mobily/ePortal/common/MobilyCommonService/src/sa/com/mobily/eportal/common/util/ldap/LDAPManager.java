package sa.com.mobily.eportal.common.util.ldap;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.HashMap;
import java.util.logging.Level;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;
import javax.validation.constraints.NotNull;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.constants.PumaProfileConstantsIfc;
import sa.com.mobily.eportal.common.exception.application.MobilyApplicationException;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.ldapservice.LDAPManagerFactory;
import sa.com.mobily.eportal.common.service.ldapservice.MobilyUser;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.vo.LDAPConfigVO;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;


//Updated By Rasha 
public class LDAPManager 
//implements LDAPManagerInterface
{

//	private static String userDN = null;
//
//	private  Properties props = null;
//
	private static DirContext adminContext = null;
//
//	private   String LDAP_USER_NAME = "LDAP_USER_NAME";
//
//	private   String LDAP_PASSWORD = "LDAP_PASSWORD";
//
//	private  final String LDAP_IP_ADDRESS = "LDAP_IP_ADDRESS";
//
//	private  final String LDAP_PORT = "LDAP_PORT";
//
//	private   String LDAP_USERS_BASE = "LDAP_USERS_BASE";
//
//	private   String LDAP_GROUPS_BASE = "LDAP_GROUPS_BASE";
	
	private static LDAPManager instance;
	private static LDAPManager instanceWifi;

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext("MobilyCommonService/commonServicesLDAP");

	private static final String className = LDAPManager.class.getName();

	public static LDAPManager getInstance()
	{
		if (instance == null || adminContext == null)
			instance = new LDAPManager();
		return instance;
	}
	
	public static LDAPManager getInstance(String ldapUsersBase,String ldapGroupsBase)
	{
		if (instance == null)
			instance = new LDAPManager(ldapUsersBase,ldapGroupsBase);
		return instance;
	}

	/**
	 * Default Constructor (Gets the Path for the LDAP properties file).
	 * 
	 * @throws MobilyApplicationException
	 */
	private LDAPManager()
	{
//		super();
//		init();

	}
	
	private LDAPManager(String ldapUsersBase,String ldapGroupsBase)
	{
//		super();
//		LDAP_USERS_BASE=ldapUsersBase;
//		LDAP_GROUPS_BASE=ldapGroupsBase;
//		init();

	}

	/**
	 * This method gets the properties from the LDAP properties file (LDAP IP,
	 * Port, Domain Name and other properties).
	 * 
	 * @throws MobilyApplicationException
	 */
	private void init()
	{
//		String method = "init";
//		if (props == null)
//		{
//			executionContext.log(Level.INFO, "Properties is NULL, Initialize......................", className, method);
//			try{
//				props = new Properties();
//				props.put(LDAP_USER_NAME, ResourceEnvironmentProviderService.getInstance().getPropertyValue(LDAP_USER_NAME));
//				props.put(LDAP_PASSWORD, ResourceEnvironmentProviderService.getInstance().getPropertyValue(LDAP_PASSWORD));
//				props.put(LDAP_IP_ADDRESS, ResourceEnvironmentProviderService.getInstance().getPropertyValue(LDAP_IP_ADDRESS));
//				props.put(LDAP_PORT, ResourceEnvironmentProviderService.getInstance().getPropertyValue(LDAP_PORT));
//				props.put(LDAP_USERS_BASE, ResourceEnvironmentProviderService.getInstance().getPropertyValue(LDAP_USERS_BASE)); 
//				props.put(LDAP_GROUPS_BASE, ResourceEnvironmentProviderService.getInstance().getPropertyValue(LDAP_GROUPS_BASE));
//			}
//			catch (Exception e1)
//			{
//				props = null;
//				executionContext.log(Level.SEVERE, "Exception During init() >"+e1, className, method, e1);
//				throw new MobilyApplicationException("BACKEND_ERROR_UNABLE_TO_LOAD_LDAP_CONFIGURATION");
//			}
//		}
//		
//		if (adminContext == null)
//		{
//			executionContext.log(Level.INFO, "adminContext is NULL, Initialize......................", className, method);			
//			try
//			{
//				adminContext = getInitialContext(props.getProperty(LDAP_USER_NAME), props.getProperty(LDAP_PASSWORD));
//			}
//			catch (NamingException e)
//			{
//				executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
//				throw new MobilyApplicationException("BACKEND_ERROR_UNABLE_TO_LOAD_LDAP_ADMIN_CONTEXT");
//			}
//		}
//		executionContext.log(Level.INFO, "adminContext after=" + adminContext, className, method);
	}

	/**
	 * This Method returns an Initial Context based on user name and password.
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws NamingException
	 */
	private DirContext getInitialContext(String username, String password) throws NamingException
	{
//		String providerUrl = props.getProperty(LDAP_IP_ADDRESS) + ":" + props.getProperty(LDAP_PORT);
//		executionContext.log(Level.INFO, "providerUrl::" + providerUrl, className, "getInitialContext");
//		Hashtable<String, String> env = new Hashtable<String, String>();
//		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
//		env.put(LdapContext.CONTROL_FACTORIES, "com.sun.jndi.ldap.ControlFactory");
//		env.put(Context.STATE_FACTORIES, "PersonStateFactory");
//		env.put(Context.OBJECT_FACTORIES, "PersonObjectFactory");
//		env.put(Context.SECURITY_AUTHENTICATION, "simple");
//		env.put(Context.REFERRAL, "follow");
//		env.put(Context.PROVIDER_URL, "ldap://" + providerUrl);
//		env.put(Context.SECURITY_PRINCIPAL, username);
//		env.put(Context.SECURITY_CREDENTIALS, password);
//		DirContext context = new InitialDirContext(env);
//		return context;
		return null;
	}
//
//	/**
//	 * This method checks the validity of a user.
//	 * 
//	 * @param username
//	 * @param password
//	 * @return
//	 */
//	public boolean isValidUser(String username, String password)
//	{
//		String method = "isValidUser";
//		executionContext.log(Level.INFO, "isValidUser > username =["+username+"]", className, method, null);
//		boolean isValidUserFlag = false;
//		DirContext adminCtx = null;
//		try
//		{
//			adminCtx = getInitialContext(getUserDN(username), password);
//			
//			isValidUserFlag = true;
//		}
//		catch (Exception e)
//		{
//			executionContext.log(Level.SEVERE, "Exception During isValidUser >"+e.getMessage(), className, method, e);
//			isValidUserFlag = false;
//		} finally {
//			if(adminCtx != null) {
//				try
//				{
//					adminCtx.close();
//					adminCtx = null;
//				}
//				catch (Exception e)
//				{
//					executionContext.log(Level.SEVERE, "LDAPManager > isValidUser > Exception while closing adminContext:" + e.getMessage(), className, method);
//				}
//			}
//		}
//		
//		return isValidUserFlag;
//	}
//
//	private void putAttribute(Attributes container, String attributeName, Object attributeValue)
//	{
//		if (attributeValue != null && !attributeValue.toString().trim().equals(""))
//			container.put(new BasicAttribute(attributeName, String.valueOf(attributeValue)));
//	}
//
	/**
	 * This method adds a user to the LDAP
	 * 
	 * @param username
	 * @param firstName
	 * @param lastName
	 * @param password
	 * @throws MobilyApplicationException
	 */
	public void createNewUserOnLDAP(@NotNull MobilyUser mobilyUser)
	{
		String method = "createNewUserOnLDAP";
		executionContext.log(Level.INFO, "createNewUserOnLDAP > mobilyUser ["+mobilyUser+"]", className, method, null);
		executionContext.log(Level.INFO, "createNewUserOnLDAP > username =["+FormatterUtility.checkNull(mobilyUser.getUid()).trim().toLowerCase()+"]", className, "createNewUserOnLDAP", null);
		try{
			LDAPManagerFactory.getInstance().getLDAPManagerImpl().createNewUserOnLDAP(mobilyUser);
		}catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Mobily Common Service > Exception during User Creation ->>>>>"+e.getMessage(), className, "createNewUserOnLDAP", e);
			throw new MobilyApplicationException(e.getMessage(), e.getCause());
		}
		
		
		
		
		
		
//		// Create acontainer set of attributes
//		Attributes container = new BasicAttributes();
//		Attribute att;
//
//		// Create the objectclass to add
//		att = new BasicAttribute("objectClass");
//		att.add("top");
//		att.add("person");
//		att.add("organizationalPerson");
//		att.add("inetOrgPerson");
//		container.put(att); 
//
//		putAttribute(container, PumaProfileConstantsIfc.cn, mobilyUser.getCn());
//		putAttribute(container, PumaProfileConstantsIfc.sn, mobilyUser.getSn());
//		putAttribute(container, PumaProfileConstantsIfc.uid, FormatterUtility.checkNull(mobilyUser.getUid()).trim().toLowerCase());
//		putAttribute(container, PumaProfileConstantsIfc.defaultMsisdn, mobilyUser.getDefaultMsisdn());
//		putAttribute(container, PumaProfileConstantsIfc.email, FormatterUtility.checkNull(mobilyUser.getEmail()).trim().toLowerCase());
//		putAttribute(container, PumaProfileConstantsIfc.preferredLanguage, mobilyUser.getPreferredLanguage());
//		putAttribute(container, PumaProfileConstantsIfc.serviceAccountNumber, (FormatterUtility.toLowerCase(mobilyUser.getServiceAccountNumber())));
//		putAttribute(container, PumaProfileConstantsIfc.defaultSubscriptionType, mobilyUser.getDefaultSubscriptionType());
//		putAttribute(container, PumaProfileConstantsIfc.defaultSubId, mobilyUser.getDefaultSubId());
//		putAttribute(container, PumaProfileConstantsIfc.defaultSubSubscriptionType, mobilyUser.getDefaultSubSubscriptionType());
//		putAttribute(container, PumaProfileConstantsIfc.corpMasterAcctNo, mobilyUser.getCorpMasterAcctNo());
//		putAttribute(container, PumaProfileConstantsIfc.corpAPNumber, mobilyUser.getCorpAPNumber());
//		putAttribute(container, PumaProfileConstantsIfc.corpAPIDNumber, mobilyUser.getCorpAPIDNumber());
//		putAttribute(container, PumaProfileConstantsIfc.commercialRegisterationID, mobilyUser.getCommercialRegisterationID());
//		putAttribute(container, PumaProfileConstantsIfc.corpSurveyCompleted, mobilyUser.isCorpSurveyCompleted());
//		putAttribute(container, "userPassword", FormatterUtility.checkNull(mobilyUser.getUserPassword()).trim());
//		putAttribute(container, PumaProfileConstantsIfc.givenName, mobilyUser.getGivenName());
//		putAttribute(container, PumaProfileConstantsIfc.Status, mobilyUser.getStatus());
//		putAttribute(container, PumaProfileConstantsIfc.firstName, mobilyUser.getFirstName());
//		putAttribute(container, PumaProfileConstantsIfc.lastName, mobilyUser.getLastName());
//		putAttribute(container, PumaProfileConstantsIfc.custSegment, mobilyUser.getCustSegment());
//		putAttribute(container, PumaProfileConstantsIfc.roleId, mobilyUser.getRoleId());
//		putAttribute(container, PumaProfileConstantsIfc.SecurityQuestion, mobilyUser.getSecurityQuestion());
//		putAttribute(container, PumaProfileConstantsIfc.SecurityAnswer, mobilyUser.getSecurityAnswer());
//		// putAttribute(container, PumaProfileConstantsIfc.birthDate,
//		// mobilyUser.getBirthDate());
//		putAttribute(container, PumaProfileConstantsIfc.nationality, mobilyUser.getNationality());
//		putAttribute(container, PumaProfileConstantsIfc.Gender, mobilyUser.getGender());
//		// putAttribute(container, PumaProfileConstantsIfc.registerationDate,
//		// mobilyUser.getRegisterationDate());
//		putAttribute(container, PumaProfileConstantsIfc.iqama, mobilyUser.getIqama());
//		putAttribute(container, PumaProfileConstantsIfc.facebookId, FormatterUtility.toLowerCase(mobilyUser.getFacebookId()));
//		putAttribute(container, PumaProfileConstantsIfc.twitterId, FormatterUtility.toLowerCase(mobilyUser.getTwitterId()));
//		putAttribute(container, PumaProfileConstantsIfc.userAcctReference, mobilyUser.getUserAcctReference());
//		putAttribute(container, PumaProfileConstantsIfc.registrationSourceId, mobilyUser.getRegistrationSourceId());
//		putAttribute(container, PumaProfileConstantsIfc.contactNumber, mobilyUser.getContactNumber());
//
//		// Create the entry
//		try
//		{
//			//adminContext = getInitialContext(props.getProperty(LDAP_USER_NAME), props.getProperty(LDAP_PASSWORD));
//			init();
//			adminContext.createSubcontext(getUserDN(FormatterUtility.checkNull(mobilyUser.getUid()).trim().toLowerCase()), container);
//			if (mobilyUser.getGroupList() != null && mobilyUser.getGroupList().size() > 0)
//			{
//				for (Iterator<String> iterator = mobilyUser.getGroupList().iterator(); iterator.hasNext();)
//				{
//					assignUser(FormatterUtility.checkNull(mobilyUser.getUid()).trim().toLowerCase(), iterator.next());
//				}
//			}
//		}
//		catch (Exception e)
//		{
//			executionContext.log(Level.SEVERE, "Mobily Common Service > Exception during User Creation ->>>>>"+e.getMessage(), className, "createNewUserOnLDAP", e);
//			
//			if(adminContext != null) {
//				try
//				{
//					adminContext.close();
//					adminContext = null;
//				}
//				catch (Exception e1)
//				{
//					executionContext.log(Level.SEVERE, "LDAPManager > createNewUserOnLDAP > Exception while closing adminContext:" + e1.getMessage(), className, method);
//				}
//			}
//			
//			throw new MobilyApplicationException(e.getMessage(), e.getCause());
//			
//		}
	}
 
	/**
	 * Use this method to delete user from LDAP
	 * 
	 * @param username
	 * @throws MobilyApplicationException
	 */
	public void deleteUser(@NotNull MobilyUser mobilyUser)
	{	
		
		String method = "deleteUser";
		executionContext.log(Level.INFO, "deleteUser > username =["+mobilyUser.getUid()+"]", className, "deleteUser", null);
		
		try
		{
			LDAPManagerFactory.getInstance().getLDAPManagerImpl().deleteUser(mobilyUser);
			/*init();
			//adminContext = getInitialContext(props.getProperty(LDAP_USER_NAME), props.getProperty(LDAP_PASSWORD));
			adminContext.unbind(getUserDN(mobilyUser.getUid()));*/
			
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception during deleteUser > "+e, className, "deleteUser", e);
			if(adminContext != null) {
				try
				{
					adminContext.close();
				}
				catch (Exception e1)
				{
					executionContext.log(Level.SEVERE, "LDAPManager > deleteUser > Exception while closing adminContext:" + e1.getMessage(), className, method);
				}
			}
			
			throw new RuntimeException(e);
		}
	}
 
//	/**
//	 * Use this method to change user password in LDAP
//	 * 
//	 * @param oldAuthentication
//	 * @param newAuthentication
//	 * @throws MobilyApplicationException
//	 */
//	public void updateUserOnLDAPByMSISDN(String MSISDN, String attrName, String attrValue)
//	{
//		// login to ldap using user account
//		MobilyUser user = findMobilyUserByAttribute(PumaProfileConstantsIfc.defaultMsisdn, MSISDN);
//
//		if (user != null)
//		{
//			updateUserOnLDAPByUserId(user.getUid(), attrName, attrValue);
//		}
//	}
//
//	/**
//	 * Update user on LDAP by user id
//	 * 
//	 * @param userId
//	 * @param attrName
//	 * @param attrValue
//	 * @throws ServiceException
//	 */
//
//	public void updateUserOnLDAPByUserId(String userId, String attrName, String attrValue)
//	{
//		//DirContext adminContext = null;
//		String method = "updateUserOnLDAPByUserId";
//		try
//		{
//			init();
//			// login to ldap using user account
//			//adminContext = getInitialContext(props.getProperty(LDAP_USER_NAME), props.getProperty(LDAP_PASSWORD));
//			// set password is an ldap modification operation
//			ModificationItem[] mods = new ModificationItem[1];
//			// Replace the "userpassword" attribute with a new value
//			mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(attrName, attrValue));
//			// Perform the update
//			adminContext.modifyAttributes(getUserDN(userId), mods);
//
//		}
//		catch (Exception e)
//		{
//			executionContext.log(Level.SEVERE, "Exception during updateUserOnLDAPByUserId > "+e.getMessage(), className, method, e);
//			if(adminContext != null) {
//				try
//				{
//					adminContext.close();
//					adminContext = null;
//				}
//				catch (Exception e1)
//				{
//					executionContext.log(Level.SEVERE, "LDAPManager > updateUserOnLDAPByUserId > Exception while closing adminContext:" + e1.getMessage(), className, method);
//				}
//			}
//
//			throw new MobilyApplicationException("BACKEND_ERROR_UNABLE_TO_UPDATE_ATTRIBUTE_LDAP_ID");
//		}
//	}
//
//	/**
//	 * Use this method to change user password in LDAP
//	 * 
//	 * @param oldAuthentication
//	 * @param newAuthentication
//	 * @throws MobilyApplicationException
//	 */
//	public void updatePasswordOnLDAP(String userName, String newPassword)
//	{
//		updateUserOnLDAPByUserId(userName, "userPassword", newPassword);
//	}
//
//	/**
//	 * Searches ldap directory server based on contextName and filter values.
//	 * 
//	 * @param String
//	 *            - [contextName] context name to start search within.
//	 * @param String
//	 *            - [filter] filtering value.
//	 * @param String
//	 *            [] - [attributeIds] String array of attributes ids to retirve.
//	 *            If null will return all attributes.
//	 * @param int - @see <code>javax.naming.directory.SearchControls</code>
//	 *        constants. Deafult is ONELEVEL_SCOPE
//	 * 
//	 * @return List of <code>java.util.Map</code> that contains attributes
//	 *         values.
//	 * @throws ParseException
//	 * 
//	 * @throws IntegratorLDAPException
//	 */
//	public MobilyUser findMobilyUserByAttributes(Map<String, String> attributes)
//	{
//		String method = "findMobilyUserByAttributes";
//		executionContext.audit(Level.INFO, "LDAPManager > findMobilyUserByAttributes > called", className, method);
//		MobilyUser user = null;
//		//DirContext adminContext = null;
//		try
//		{
//			SearchControls controls = new SearchControls();
//			controls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
//			String[] attributeIds = { PumaProfileConstantsIfc.cn, PumaProfileConstantsIfc.sn, PumaProfileConstantsIfc.uid, PumaProfileConstantsIfc.defaultMsisdn,
//					PumaProfileConstantsIfc.email, PumaProfileConstantsIfc.preferredLanguage, PumaProfileConstantsIfc.serviceAccountNumber,
//					PumaProfileConstantsIfc.defaultSubscriptionType, PumaProfileConstantsIfc.defaultSubId, PumaProfileConstantsIfc.corpMasterAcctNo,
//					PumaProfileConstantsIfc.corpAPNumber, PumaProfileConstantsIfc.corpAPIDNumber, PumaProfileConstantsIfc.commercialRegisterationID,
//					PumaProfileConstantsIfc.corpSurveyCompleted, "userPassword", PumaProfileConstantsIfc.givenName, PumaProfileConstantsIfc.Status,
//					PumaProfileConstantsIfc.firstName, PumaProfileConstantsIfc.lastName, PumaProfileConstantsIfc.custSegment, PumaProfileConstantsIfc.roleId,
//					PumaProfileConstantsIfc.SecurityQuestion, PumaProfileConstantsIfc.SecurityAnswer, PumaProfileConstantsIfc.birthDate, PumaProfileConstantsIfc.nationality,
//					PumaProfileConstantsIfc.Gender, PumaProfileConstantsIfc.registerationDate, PumaProfileConstantsIfc.iqama, PumaProfileConstantsIfc.facebookId,
//					PumaProfileConstantsIfc.registrationSourceId, PumaProfileConstantsIfc.defaultSubSubscriptionType, PumaProfileConstantsIfc.userAcctReference,
//					PumaProfileConstantsIfc.contactNumber, PumaProfileConstantsIfc.twitterId };
//			// TODO: UR for password
//			controls.setReturningAttributes(attributeIds);
//
//			String filter = "(&";
//
//			for (Iterator<String> iterator = attributes.keySet().iterator(); iterator.hasNext();)
//			{
//				String attributeName = (String) iterator.next();
//				filter += "(" + attributeName + "=" + attributes.get(attributeName) + ")";
//			}
//
//			filter += ")";
//			
//			executionContext.audit(Level.INFO, "LDAPManager > findMobilyUserByAttributes > Before LDAP Search", className, method);
//			
//			long _start_time = (new Date()).getTime();
//			
//			init();
//			
//			executionContext.audit(Level.INFO, "LDAPManager > findMobilyUserByAttributes > LDAP UserName["+props.getProperty(LDAP_USER_NAME)+"]", className, method);
//			
//			NamingEnumeration<SearchResult> names = adminContext.search(props.getProperty(LDAP_USERS_BASE), filter, controls);
//			
//			long _end_time = (new Date()).getTime();
//			long elapsed_time = _end_time - _start_time;
//			
//			executionContext.audit(Level.INFO, "Query time is ["+elapsed_time+"] ms searching for ["+_start_time+"]", className, method);
//			
//			executionContext.audit(Level.INFO, "LDAPManager > findMobilyUserByAttributes > After LDAP Search", className, method);
//			
//			while (names.hasMore())
//			{
//				user = getMobilyUser(((SearchResult) names.next()).getAttributes());
//				break;
//			}
//			if(names != null)
//				names.close();
//			
//			executionContext.audit(Level.INFO, "LDAPManager > findMobilyUserByAttributes > user data is ={"+user+"]", className, method);
//			
//		}
//		catch (Exception e)
//		{
//			executionContext.log(Level.SEVERE, "LDAPManager > findMobilyUserByAttributes > Exception >"+e, className, method, e);
//			if(adminContext != null) {
//				try
//				{
//					adminContext.close();
//					adminContext = null;
//				}
//				catch (Exception e1)
//				{
//					executionContext.log(Level.SEVERE, "LDAPManager > findMobilyUserByAttributes > Exception while closing adminContext:" + e1.getMessage(), className, method);
//				}
//			}
//			throw new MobilyApplicationException("BACKEND_ERROR_UNABLE_TO_SEARCH_IN_LDAP_ID");
//		}
//		return user;
//	}
//
//	/**
//	 * @param attributeName
//	 * @param attributeValue
//	 * @return
//	 * @throws ServiceException
//	 */
//	public MobilyUser findMobilyUserByAttribute(String attributeName, String attributeValue)
//	{
//		Map<String, String> attributes = new HashMap<String, String>();
//		attributes.put(attributeName, attributeValue);
//		return findMobilyUserByAttributes(attributes);
//	}
//	
//	public List<MobilyUser> findMobilyUserListByAttribute(String attributeName, String attributeValue)
//	{
//		Map<String, String> attributes = new HashMap<String, String>();
//		attributes.put(attributeName, attributeValue);
//		return findMobilyUserListByAttributes(attributes);
//	}
//	
//
//	/**
//	 * Update user on LDAP by user id
//	 * 
//	 * @param userId
//	 * @param attrName
//	 * @param attrValue
//	 * @throws ServiceException
//	 */
//
//	public void updateUserAttributesOnLDAPByUserId(String userId, Map<String, Object> attributes)
//	{
//		String method = "updateUserAttributesOnLDAPByUserId";
//
//		for (String key : attributes.keySet())
//		{
//			executionContext.log(Level.INFO, "modifying attribute: " + key + ", value :" + String.valueOf(attributes.get(key)), className, method, null);
//			updateUserOnLDAPByUserId(userId, key, String.valueOf(attributes.get(key)));
//		}
//	}
//
	/***
	 * Updating user group
	 * 
	 * @param userId
	 * @param oldGroup
	 * @param newGroup
	 */

	public void updateUserGroup(String userId, String oldGroup, String newGroup)
	{
		String method = "updateUserGroup";
		//DirContext adminContext = null;
		executionContext.log(Level.INFO, "remove user from group", className, method, null);
		try
		{
			
			LDAPManagerFactory.getInstance().getLDAPManagerImpl().updateUserGroup(userId,oldGroup, newGroup);
		/*	//adminContext = getInitialContext(props.getProperty(LDAP_USER_NAME), props.getProperty(LDAP_PASSWORD));
			removeGroup(userId, oldGroup);
			executionContext.log(Level.INFO, "assign user to group", className, method, null);
			assignUser(userId, newGroup);
			*/
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception During updateUserGroup >"+e, className, "updateUserGroup", e);
		}
	}
//
//	/**
//	 * This method returns the Distinguish name for a specific user id
//	 * 
//	 * @param username
//	 * @return
//	 */
//	private String getUserDN(String userName)
//	{
//		userDN = new StringBuffer().append("uid=").append(userName).append(",").append(props.getProperty(LDAP_USERS_BASE)).toString();
//		return userDN;
//	}
//
//	/**
//	 * This method returns the Distinguish name for a specific user id
//	 * 
//	 * @param username
//	 * @return
//	 */
//	private String getGroupDN(String groupName)
//	{
//		userDN = new StringBuffer().append("cn=").append(groupName).append(",").append(props.getProperty(LDAP_GROUPS_BASE)).toString();
//		return userDN;
//	}
//
//	/**
//	 * Use this method to assign user to group in LDAP
//	 * 
//	 * @param username
//	 * @param groupName
//	 * @throws MobilyApplicationException
//	 */
//	private void removeGroup(String username, String groupName) throws MobilyApplicationException
//	{
//		String method = "removeGroup";
//		//DirContext adminContext = null;
//		try
//		{
//			//adminContext = getInitialContext(props.getProperty(LDAP_USER_NAME), props.getProperty(LDAP_PASSWORD));
//			// System.out.println("Method removeGroup called.  Username:" +
//			// username + ", groupName:" + groupName );
//			init();
//			ModificationItem[] mods = new ModificationItem[1];
//			mods[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("uniquemember", getUserDN(username)));
//
//			// System.out.println("Removing user dn "+getUserDN(username) +
//			// " from group dn " + getGroupDN(groupName));
//			adminContext.modifyAttributes(getGroupDN(groupName), mods);
//			// System.out.println("Removed succesfully");
//			
//		}
//		catch (Exception e)
//		{
//			executionContext.log(Level.SEVERE, "Exception During removeGroup >"+e, className, method, e);
//			if(adminContext != null) {
//				try
//				{
//					adminContext.close();
//				}
//				catch (Exception e1)
//				{
//					executionContext.log(Level.SEVERE, "LDAPManager > removeGroup > Exception while closing adminContext:" + e1.getMessage(), className, method);
//				}
//			}
//			throw new MobilyApplicationException("BACKEND_ERROR_UNABLE_TO_REMOVE_USER_TO_GROUP_ID");
//		}
//	}
//
//	/**
//	 * Use this method to assign user to group in LDAP
//	 * 
//	 * @param username
//	 * @param groupName
//	 * @throws MobilyApplicationException
//	 */
//	private void assignUser(String username, String groupName) throws MobilyApplicationException
//	{
//		String method = "assignUser";
//		//DirContext adminContext = null;
//		try
//		{
//			//adminContext = getInitialContext(props.getProperty(LDAP_USER_NAME), props.getProperty(LDAP_PASSWORD));
//			// System.out.println("Method called: assignUser. username: " + username + ", groupname: " + groupName);
//			init();
//			
//			ModificationItem[] mods = new ModificationItem[1]; 
//			mods[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE, new BasicAttribute("uniquemember", getUserDN(username)));
//				//mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("uniquemember", getUserDN(username)));
//				
//			// System.out.println("Assigning user : " + getUserDN(username) + " to group " + getGroupDN(groupName));
//			adminContext.modifyAttributes(getGroupDN(groupName), mods);
//			// System.out.println("Assigned user to group successfully");
//	
//		}
//		catch (NamingException e)
//		{
//			executionContext.log(Level.SEVERE, "Exception During assignUser ->>>>>"+e, className, method, e);
//			if(adminContext != null) {
//				try
//				{
//					adminContext.close();
//				}
//				catch (Exception e1)
//				{
//					executionContext.log(Level.SEVERE, "LDAPManager > assignUser > Exception while closing adminContext:" + e1.getMessage(), className, method);
//				}
//			}
//			throw new MobilyApplicationException(e.getMessage(), e.getCause());
//			//throw new MobilyApplicationException("BACKEND_ERROR_UNABLE_TO_ASSIGN_USER_TO_GROUP_ID");
//		}
//	}
//
//	private MobilyUser getMobilyUser(Attributes attributeValues)
//	{
//		MobilyUser mobilyUser = new MobilyUser();
//		try {
//			mobilyUser.setCn(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.cn));
//			mobilyUser.setSn(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.sn));
//			mobilyUser.setUid(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.uid));
//			mobilyUser.setUserPassword(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.password));
//			mobilyUser.setDefaultMsisdn(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.defaultMsisdn));
//			mobilyUser.setEmail(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.email));
//			mobilyUser.setPreferredLanguage(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.preferredLanguage));
//			mobilyUser.setServiceAccountNumber(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.serviceAccountNumber));
//			if (attributeValues.get(PumaProfileConstantsIfc.defaultSubscriptionType) != null)
//				mobilyUser.setDefaultSubscriptionType(getIntegerFromSearchResult(attributeValues, PumaProfileConstantsIfc.defaultSubscriptionType));
//			if (attributeValues.get(PumaProfileConstantsIfc.defaultSubSubscriptionType) != null)
//				mobilyUser.setDefaultSubSubscriptionType(getIntegerFromSearchResult(attributeValues, PumaProfileConstantsIfc.defaultSubSubscriptionType));
//			mobilyUser.setDefaultSubId(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.defaultSubId));
//			mobilyUser.setCorpMasterAcctNo(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.corpMasterAcctNo));
//			mobilyUser.setCorpAPNumber(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.corpAPNumber));
//			mobilyUser.setCorpAPIDNumber(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.corpAPIDNumber));
//			mobilyUser.setCommercialRegisterationID(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.commercialRegisterationID));
//			mobilyUser.setCorpSurveyCompleted(Boolean.parseBoolean(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.corpSurveyCompleted)));
//			mobilyUser.setGivenName(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.givenName));
//			mobilyUser.setStatus(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.Status));
//			mobilyUser.setFirstName(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.firstName));
//			mobilyUser.setLastName(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.lastName));
//			mobilyUser.setCustSegment(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.custSegment));
//			if (attributeValues.get(PumaProfileConstantsIfc.roleId) != null)
//				mobilyUser.setRoleId(getIntegerFromSearchResult(attributeValues, PumaProfileConstantsIfc.roleId));
//			mobilyUser.setSecurityQuestion(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.SecurityQuestion));
//			mobilyUser.setSecurityAnswer(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.SecurityAnswer));
//			mobilyUser.setBirthDate(getDateFromSearchResult(attributeValues, PumaProfileConstantsIfc.birthDate));
//			mobilyUser.setNationality(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.nationality));
//			mobilyUser.setGender(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.Gender));
//			mobilyUser.setUserAcctReference(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.userAcctReference));
//			mobilyUser.setRegisterationDate(getDateFromSearchResult(attributeValues, PumaProfileConstantsIfc.registerationDate));
//			mobilyUser.setIqama(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.iqama));
//			mobilyUser.setFacebookId(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.facebookId));
//			mobilyUser.setTwitterId(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.twitterId));
//			if (attributeValues.get(PumaProfileConstantsIfc.registrationSourceId) != null)
//				mobilyUser.setRegistrationSourceId(getIntegerFromSearchResult(attributeValues, PumaProfileConstantsIfc.registrationSourceId));
//			if (attributeValues.get(PumaProfileConstantsIfc.contactNumber) != null)
//				mobilyUser.setContactNumber(getAttributeFromSearchResult(attributeValues, PumaProfileConstantsIfc.contactNumber));
//		} catch(Exception e) {
//			executionContext.log(Level.SEVERE, "Exception while populating mobilyUser ->>>>>"+e.getMessage(), className, "getMobilyUser", e);
//		}
//
//		return mobilyUser;
//	}
//
//	private String getAttributeFromSearchResult(Attributes attributeValues, String attributeName)
//	{
//		try
//		{
//			if (attributeValues.get(attributeName) == null || attributeValues.get(attributeName).size() == 0 || attributeValues.get(attributeName).get(0) == null
//					|| ((String) attributeValues.get(attributeName).get(0)).trim().equals(""))
//				return null;
//			return (String) attributeValues.get(attributeName).get(0);
//		}
//		catch (Exception e)
//		{
//			executionContext.log(Level.SEVERE, "Exception During getAttributeFromSearchResult >"+e.getMessage(), className, "getAttributeFromSearchResult", e);
//			return null;
//		}
//	}
//
//	private Integer getIntegerFromSearchResult(Attributes attributeValues, String attributeName)
//	{
//		try
//		{
//			String value = getAttributeFromSearchResult(attributeValues, attributeName);
//			if (value == null || value.trim().equals(""))
//				return null;
//			return Integer.parseInt(value);
//		}
//		catch (Exception e)
//		{
//			executionContext.log(Level.SEVERE, "Exception During getIntegerFromSearchResult >"+e.getMessage(), className, "getIntegerFromSearchResult", e);
//			return null;
//		}
//	}
//
//	private Date getDateFromSearchResult(Attributes attributeValues, String attributeName)
//	{
//		try
//		{
//			String value = getAttributeFromSearchResult(attributeValues, attributeName);
//			if (value == null || value.trim().equals(""))
//				return null;
//			return new SimpleDateFormat("yyyyMMddHHmmss").parse(value);
//		}
//		catch (Exception e)
//		{
//			executionContext.log(Level.SEVERE, "Exception During getDateFromSearchResult >"+e.getMessage(), className, "getDateFromSearchResult", e);
//			return null;
//		}
//	}
//	
//	public static LDAPManager getInstance(LDAPConfigVO ldapConfigVO){
//		if (instanceWifi == null)
//			instanceWifi = new LDAPManager(ldapConfigVO);
//		return instanceWifi;
//	}
//	
//	private LDAPManager(LDAPConfigVO ldapConfig)
//	{
//		super();
//		LDAP_USERS_BASE=ldapConfig.getLdapUserBase();
//		LDAP_GROUPS_BASE=ldapConfig.getLdapGroupBase();
//		LDAP_USER_NAME=ldapConfig.getLdapUserName();
//		LDAP_PASSWORD=ldapConfig.getLdapPassword();
//		init();
//
//	}
//	
//	public MobilyUser authenticateAndGetUserDetails(String username, String password){
//		String method = "authenticateAndGetUserDetails";
//		executionContext.log(Level.INFO, "LDAPManager > authenticateAndGetUserDetails started...", className, method);
//		
//		if(isValidUser(username, FormatterUtility.checkNull(password))){
//			executionContext.log(Level.INFO, "LDAPManager > authenticateAndGetUserDetails > userName =["+username+"] credentials are valid.", className, method);
//			//Get the Details from LDAP
//			MobilyUser mobilyuserObj = findMobilyUserByAttribute(PumaProfileConstantsIfc.uid, FormatterUtility.checkNull(username));
//			
//			return mobilyuserObj;
//		}else{
//			executionContext.log(Level.INFO, "LDAPManager > authenticateAndGetUserDetails > userName =["+username+"] credentials are not valid.", className, method);
//		}
//		
//		return null;
//	}
//
//    private String getGroupCN(String groupDN) {
//    	
//        int start = groupDN.indexOf("=");
//        int end = groupDN.indexOf(",");
//
//        if (end == -1) {
//            end = groupDN.length();
//        }
//
//        return groupDN.substring(start+1, end);
//    }
//    
//    /**
//     * Get the list of groups of a user
//     * @param username
//     * @return
//     */
//    public List<String> getGroups(String username) {
//    	String method = "getGroups";
//		executionContext.log(Level.INFO, "LDAPManager > getGroups called for username="+username, className, method);
//    	List<String> groups = new LinkedList<String>();
//    	//DirContext adminContext = null;
//        try {
//        	init();
//        	
//	        // Set up criteria to search on
//	        String filter = new StringBuffer()
//	            .append("(&")
//	            //.append("(objectClass=user)")
//	            .append("(uniqueMember=")
//	            .append(getUserDN(username))
//	            .append(")")
//	            .append(")")
//	            .toString();
//	
//	        // Set up search constraints
//	        SearchControls cons = new SearchControls();
//	        cons.setSearchScope(SearchControls.ONELEVEL_SCOPE);
//	        long _start_time = (new Date()).getTime();
//	        
//	        //adminContext = getInitialContext(props.getProperty(LDAP_USER_NAME), props.getProperty(LDAP_PASSWORD));
//	        NamingEnumeration<SearchResult> results = adminContext.search(props.getProperty(LDAP_GROUPS_BASE), filter, cons);
//	        executionContext.log(Level.INFO, "LDAPManager > getGroups > results="+results, className, method);
//	        
//	        long _end_time = (new Date()).getTime();
//	        long elapsed_time = _end_time - _start_time;
//	        executionContext.audit(Level.INFO, "Query time is ["+elapsed_time+"] ms searching groups for username ["+username+"]", className, method);
//	        
//	        SearchResult result;
//	        String groupName;
//	        while(results.hasMore()) {
//	        	executionContext.log(Level.INFO, "LDAPManager > getGroups > Inside the while loop", className, method);
//	        	result = results.next();
//	        	groupName = result != null ? getGroupCN(result.getName()) : "";
//	            executionContext.log(Level.INFO, "LDAPManager > getGroups > groupName="+groupName, className, method);
//	            if(FormatterUtility.isNotEmpty(groupName))
//	            	groups.add(groupName);
//	        }
//		}
//		catch (Exception e)
//		{
//			executionContext.log(Level.SEVERE, "LDAPManager > getGroups > Exception:" + e.getMessage(), className, method);
//			if(adminContext != null) {
//				try
//				{
//					adminContext.close();
//					adminContext = null;
//				}
//				catch (Exception e1)
//				{
//					executionContext.log(Level.SEVERE, "LDAPManager > assignUserToGroup > Exception while closing adminContext:" + e1.getMessage(), className, method);
//				}
//			}
//		}
//        return groups;
//    }
//	
//	@Override
//	public boolean checkMembership(String username, String groupName) {
//		executionContext.log(Level.INFO, "LDAPManager > checkMembership > called for user : " + username + " to group " + groupName, className, "checkMembership");
//		boolean isAMember = false;
//		List<String> ldapGroupsList = getGroups(username);
//		executionContext.log(Level.INFO, "LDAPManager > checkMembership > ldapGroupsList::" + ldapGroupsList, className, "checkMembership");
//		if(ldapGroupsList != null && ldapGroupsList.size() > 0) {
//			if(ldapGroupsList.contains(groupName)) {
//				isAMember = true;
//			}
//		}
//
//		return isAMember;
//	}
//	
//	@Override
//	public void assignUserToGroup(String username, String groupName) {
//		String method = "assignUserToGroup";
//		//DirContext adminContext = null;
//		try
//		{
//			//adminContext = getInitialContext(props.getProperty(LDAP_USER_NAME), props.getProperty(LDAP_PASSWORD));
//			init();
//			ModificationItem[] mods = new ModificationItem[1]; 
//			mods[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE, new BasicAttribute("uniquemember", getUserDN(username)));
//			
//			executionContext.log(Level.INFO, "LDAPManager > assignUserToGroup > Assigning user : " + getUserDN(username) + " to group " + getGroupDN(groupName), className, method);
//			adminContext.modifyAttributes(getGroupDN(groupName), mods);
//			executionContext.log(Level.INFO, "LDAPManager > assignUserToGroup > Assigned successfully user : " + getUserDN(username) + " to group " + getGroupDN(groupName), className, method);
//			
//		}
//		catch (Exception e)
//		{
//			executionContext.log(Level.SEVERE, "LDAPManager > assignUserToGroup > Exception:" + e.getMessage(), className, method);
//			if(adminContext != null) {
//				try
//				{
//					adminContext.close();
//					adminContext = null;
//				}
//				catch (Exception e1)
//				{
//					executionContext.log(Level.SEVERE, "LDAPManager > assignUserToGroup > Exception while closing adminContext:" + e1.getMessage(), className, method);
//				}
//			}
//			throw new ServiceException(e.getMessage(),e);
//		}
//	}
//	
//	@Override
//    public List<MobilyUser> findMobilyUserListByAttributes(Map<String, String> attributes)
//    {
//		String method = "findMobilyUserListByAttributes";
//	  	executionContext.audit(Level.INFO, "LDAPManager > findMobilyUserListByAttributes > called", className, method);
//	   
//	  	List<MobilyUser>  mobilyUserList = new ArrayList<MobilyUser>();
//	  	//DirContext adminContext = null;
//	  	try
//	  	{
//	     	SearchControls controls = new SearchControls();
//	    	controls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
//	    	String[] attributeIds = { PumaProfileConstantsIfc.cn, PumaProfileConstantsIfc.sn, PumaProfileConstantsIfc.uid, PumaProfileConstantsIfc.defaultMsisdn,
//	               	PumaProfileConstantsIfc.email, PumaProfileConstantsIfc.preferredLanguage, PumaProfileConstantsIfc.serviceAccountNumber,
//	             	PumaProfileConstantsIfc.defaultSubscriptionType, PumaProfileConstantsIfc.defaultSubId, PumaProfileConstantsIfc.corpMasterAcctNo,
//	              	PumaProfileConstantsIfc.corpAPNumber, PumaProfileConstantsIfc.corpAPIDNumber, PumaProfileConstantsIfc.commercialRegisterationID,
//	              	PumaProfileConstantsIfc.corpSurveyCompleted, "userPassword", PumaProfileConstantsIfc.givenName, PumaProfileConstantsIfc.Status,
//	              	PumaProfileConstantsIfc.firstName, PumaProfileConstantsIfc.lastName, PumaProfileConstantsIfc.custSegment, PumaProfileConstantsIfc.roleId,
//	               	PumaProfileConstantsIfc.SecurityQuestion, PumaProfileConstantsIfc.SecurityAnswer, PumaProfileConstantsIfc.birthDate, PumaProfileConstantsIfc.nationality,
//	               	PumaProfileConstantsIfc.Gender, PumaProfileConstantsIfc.registerationDate, PumaProfileConstantsIfc.iqama, PumaProfileConstantsIfc.facebookId,
//	            	PumaProfileConstantsIfc.registrationSourceId, PumaProfileConstantsIfc.defaultSubSubscriptionType, PumaProfileConstantsIfc.userAcctReference,
//	            	PumaProfileConstantsIfc.contactNumber, PumaProfileConstantsIfc.twitterId };
//	               	// TODO: UR for password
//           	controls.setReturningAttributes(attributeIds);
//
//           	String filter = "(&";
//
//           	for (Iterator<String> iterator = attributes.keySet().iterator(); iterator.hasNext();)
//           	{
//            	String attributeName = (String) iterator.next();
//            	filter += "(" + attributeName + "=" + attributes.get(attributeName) + ")";
//           	}
//
//           	filter += ")";
//           
//           	executionContext.audit(Level.INFO, "LDAPManager > findMobilyUserListByAttributes > Before LDAP Search", className, method);
//           
//           	long _start_time = (new Date()).getTime();
//           
//           	init();
//           
//           	executionContext.audit(Level.INFO, "LDAPManager > findMobilyUserListByAttributes > LDAP UserName["+props.getProperty(LDAP_USER_NAME)+"]", className, method);
//           
//           	NamingEnumeration<SearchResult> names = adminContext.search(props.getProperty(LDAP_USERS_BASE), filter, controls);
//           
//           	long _end_time = (new Date()).getTime();
//           	long elapsed_time = _end_time - _start_time;
//           
//           	executionContext.audit(Level.INFO, "Query time is ["+elapsed_time+"] ms searching for ["+_start_time+"]", className, method);
//           
//           	executionContext.audit(Level.INFO, "LDAPManager > findMobilyUserListByAttributes > After LDAP Search", className, method);
//           
//           	while (names.hasMore())
//           	{
//           		MobilyUser user = null;
//                user = getMobilyUser(((SearchResult) names.next()).getAttributes());
//                mobilyUserList.add(user);
//           	}
//           	if(names != null)
//           		names.close();
//           	executionContext.audit(Level.INFO, "LDAPManager > findMobilyUserListByAttributes > userList data is ={"+mobilyUserList+"}", className, method);
//	               
//		}
//		catch (Exception e)
//		{
//			executionContext.log(Level.SEVERE, "LDAPManager > findMobilyUserListByAttributes > Exception >"+e, className, method, e);
//			if(adminContext != null) {
//                try
//                {
//                	adminContext.close();
//                	adminContext = null;
//                }
//                catch (Exception e1)
//                {
//                	executionContext.log(Level.SEVERE, "LDAPManager > findMobilyUserListByAttributes > Exception while closing adminContext:" + e1.getMessage(), className, method);
//                }
//			}
//			throw new MobilyApplicationException("BACKEND_ERROR_UNABLE_TO_SEARCH_IN_LDAP_ID");
//		}
//	  	return mobilyUserList;
//    }
//
}
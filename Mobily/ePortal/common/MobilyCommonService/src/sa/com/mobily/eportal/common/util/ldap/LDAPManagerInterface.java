package sa.com.mobily.eportal.common.util.ldap;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import sa.com.mobily.eportal.common.exception.application.MobilyApplicationException;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.ldapservice.MobilyUser;


public interface LDAPManagerInterface
{
	
	
	/**
	 * This method checks the validity of a user.
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean isValidUser(String username, String password);
	
	/**
	 * This method adds a user to the LDAP
	 * 
	 * @param username
	 * @param firstName
	 * @param lastName
	 * @param password
	 * @throws MobilyApplicationException
	 */
	public void createNewUserOnLDAP(@NotNull MobilyUser mobilyUser);
	/**
	 * Use this method to delete user from LDAP
	 * 
	 * @param username
	 * @throws MobilyApplicationException
	 */
	public void deleteUser(@NotNull MobilyUser mobilyUser);

	/**
	 * Use this method to change user password in LDAP
	 * 
	 * @param oldAuthentication
	 * @param newAuthentication
	 * @throws MobilyApplicationException
	 */
	public void updateUserOnLDAPByMSISDN(String MSISDN, String attrName, String attrValue);

	/**
	 * Update user on LDAP by user id
	 * 
	 * @param userId
	 * @param attrName
	 * @param attrValue
	 * @throws ServiceException
	 */

	public void updateUserOnLDAPByUserId(String userId, String attrName, String attrValue);
	/**
	 * Use this method to change user password in LDAP
	 * 
	 * @param oldAuthentication
	 * @param newAuthentication
	 * @throws MobilyApplicationException
	 */
	public void updatePasswordOnLDAP(String userName, String newPassword);

	/**
	 * Searches ldap directory server based on contextName and filter values.
	 * 
	 * @param String
	 *            - [contextName] context name to start search within.
	 * @param String
	 *            - [filter] filtering value.
	 * @param String
	 *            [] - [attributeIds] String array of attributes ids to retirve.
	 *            If null will return all attributes.
	 * @param int - @see <code>javax.naming.directory.SearchControls</code>
	 *        constants. Deafult is ONELEVEL_SCOPE
	 * 
	 * @return List of <code>java.util.Map</code> that contains attributes
	 *         values.
	 * @throws ParseException
	 * 
	 * @throws IntegratorLDAPException
	 */
	public MobilyUser findMobilyUserByAttributes(Map<String, String> attributes);

	/**
	 * @param attributeName
	 * @param attributeValue
	 * @return
	 * @throws ServiceException
	 */
	public MobilyUser findMobilyUserByAttribute(String attributeName, String attributeValue);
	
	/**
	 * @param attributeName
	 * @param attributeValue
	 * @return
	 * @throws ServiceException
	 */
	
	public List<MobilyUser> findMobilyUserListByAttributes(Map<String, String> attributes);

	/**
	 * @param attributeName
	 * @param attributeValue
	 * @return
	 * @throws ServiceException
	 */
	
	public List<MobilyUser> findMobilyUserListByAttribute(String attributeName, String attributeValue);
	
	/**
	 * Update user on LDAP by user id
	 * 
	 * @param userId
	 * @param attrName
	 * @param attrValue
	 * @throws ServiceException
	 */
	public void updateUserAttributesOnLDAPByUserId(String userId, Map<String, Object> attributes);
	
	
	/***
	 * Updating user group
	 * @param userId
	 * @param oldGroup
	 * @param newGroup
	 */

		public void updateUserGroup(String userId, String oldGroup, String newGroup);
		
	/**
	 * @param username
	 * @param password
	 * @return
	 * @throws ServiceException
	 */
	public MobilyUser authenticateAndGetUserDetails(String username, String password);
	
	/**
	 * 
	 * @param username
	 * @param groupName
	 * @return
	 */
	public boolean checkMembership(String username, String groupName);

	
	/**
	 * 
	 * @param username
	 * @param groupName
	 */
	public void assignUserToGroup(String username, String groupName);

}

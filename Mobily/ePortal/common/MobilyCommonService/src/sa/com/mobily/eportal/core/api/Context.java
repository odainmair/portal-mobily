//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

import java.util.Locale;

/**
 * This interface must be implemented by the caller of the ResourceManager
 * to provide the necessary user-related information.
 * @see sa.com.mobily.eportal.core.api.ResourceManager
 */
public interface Context {

    /***
     * Returns the current logged in user, or null in case of anonymous user
     * @return the current logged in user, or null in case of anonymous user
     */
    public User getCurrentUser();

    /***
     * returns the current locale of the user
     * @return the current locale of the user
     */
    public Locale getLocale();

    /***
     * returns the current locale of the user
     * @return the current locale of the user
     */
    public ChannelType getChannel();


    public enum ChannelType {
        web, mobile
    }
}

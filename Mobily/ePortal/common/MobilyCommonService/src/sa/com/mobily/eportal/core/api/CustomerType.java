//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

/**
 * This is an enumeration for the list of possible values of
 * the Customer Type that can be used by the Value Objects of the module.
 */
public enum CustomerType {
    MobilyCustomer, NonMobilyCustomer, 
}

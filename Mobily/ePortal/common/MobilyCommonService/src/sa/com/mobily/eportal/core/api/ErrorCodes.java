//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

/**
 * Lists the available Mobily ePortal error codes 
 * @author Hossam Omar
 *
 */
public interface ErrorCodes {

    int GENERAL_ERROR = 100;
    int VALIDATION_EXCEPTION = 101;
    int USER_NOT_AUTHORIZED = 102;
    int RESOURCE_NOT_FOUND = 103;
    int METHOD_NOT_SUPPORTED = 104;
    int INTERNAL_SERVER_ERROR = 105;
    int RESOURCE_ALREADY_EXISTS = 106;
    
    int INVALID_PIN_CODE = 107;
    int MAIL_SEND_FAILED = 108;
    int MISSING_REQUIRED_FIELDS = 109;
    
    int USER_ALREADY_EXISTS = 111;
    int USER_NOT_FOUND = 112;
    int INVALID_SHUKRAN_ACCOUNTNUMBER = 120;
}

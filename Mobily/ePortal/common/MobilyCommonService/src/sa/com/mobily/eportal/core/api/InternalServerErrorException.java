//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

/**
 * This exception is thrown by the REST APIs to provide the error code
 * defined by ErrorCodes.INTERNAL_SERVER_ERROR.
 *
 * @see ErrorCodes
 */
public class InternalServerErrorException extends ServiceException {

    private static final long serialVersionUID = 1L;

    public InternalServerErrorException(String message) {
        super(message, ErrorCodes.INTERNAL_SERVER_ERROR);
    }
    
    public InternalServerErrorException(String message, int errorCode) {
        super(message, errorCode);
    }
}

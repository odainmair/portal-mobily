//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

/**
 * Enumerates the available user line types
 * @author Hossam Omar
 *
 */
public enum LineType {

    CONSUMER(11), 
    CORPORATE_LINE(12), 
    CORPORATE_AUTHORIZED_PERSON(13), 
    CORPORATE_AUTHORIZED_PERSON_CHANGE_IDENTITY(14), 
    WIMAX(15), 
    CONNECT(16), 
    FTTH(17), 
    NON_MOBILY(18), 
    ECOMMERCE_B2B_CUSTOMER(19), 
    ECOMMERCE_B2C_CUSTOMER(20), 
    CAP_POWERUSER(21), 
    SALES_ORDER_USER(22), 
    IPTV(23), 
    DOCTOR_ON_DEMAND(24), 
    FTTH_WO(25), 
    FTTH_MANAGEMENT(26), 
    CONNECT_LTE(27);
    
    private int value;
    private LineType(int value) {
	this.value = value;
    }
    public int getValue() {
	return value;
    }
}

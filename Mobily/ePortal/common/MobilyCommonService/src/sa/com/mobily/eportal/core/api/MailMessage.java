package sa.com.mobily.eportal.core.api;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;

/**
 * This class represents a value object used for sending a mail with
 * its associated attachments.
 */
public class MailMessage implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private String from;
    private List<String> to;
    private List<String> cc;
    private String subject;
    private String body;
    private String contentType;
    private List<File> attachements;
    
    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }
    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }
    /**
     * @return the to
     */
    public List<String> getTo() {
        return to;
    }
    /**
     * @param to the to to set
     */
    public void setTo(List<String> to) {
        this.to = to;
    }
    
    /***
     * Adds an email to the To list
     * @param email a single email address
     */
    public void addTo(String email) {
	if(to == null) {
	    to = new Vector<String>();
	}
	
	to.add(email);
    }
    
    /***
     * Adds multiple email addresses(separated by a separator) to the TO addresses
     * @param emails multiple email addresses separated by separator
     * @param separator the separator between the mail addresses
     */
    public void addTo(String emails, String separator) {
	if(to == null) {
	    to = new Vector<String>();
	}
	
	addMultipleEmails(emails, separator, to);
    }
    
    /**
     * @return the cc
     */
    public List<String> getCc() {
        return cc;
    }
    /**
     * @param cc the cc to set
     */
    public void setCc(List<String> cc) {
        this.cc = cc;
    }
    
    /***
     * Adds an email to the CC list
     * @param email a single email address
     */
    public void addCc(String email) {
	if(cc == null) {
	    cc = new Vector<String>();
	}
	
	cc.add(email);
    }
    
    /***
     * Adds multiple email addresses(separated by a separator) to the CC addresses
     * @param emails multiple email addresses separated by separator
     * @param separator the separator between the mail addresses
     */
    public void addCc(String emails, String separator) {
	if(cc == null) {
	    cc = new Vector<String>();
	}
	
	addMultipleEmails(emails, separator, cc);
    }
    
    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }
    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }
    /**
     * @return the body
     */
    public String getBody() {
        return body;
    }
    /**
     * @param body the body to set
     */
    public void setBody(String body) {
        this.body = body;
    }
    
    public String getContentType() {
        return contentType;
    }
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
    public List<File> getAttachements() {
        return attachements;
    }
    public void setAttachements(List<File> attachements) {
        this.attachements = attachements;
    }
    public void addAttachement(File attachment) {
	if(attachements == null) {
	    attachements = new Vector<File>();
	}
	
	attachements.add(attachment);
    }
    /***
     * Extracts multiple emails from a separated string and adds them to a target list of emails
     * @param emails string contains multiple emails separated by a separator
     * @param separator the separator between the emails
     * @param targetList the target list to add the extracted emails to
     */
    private void addMultipleEmails(String emails, String separator, List<String> targetList) {
	String[] lstEmails = emails.split(separator);
	for (String address : lstEmails) {
	    targetList.add(address);
        }
    }
}

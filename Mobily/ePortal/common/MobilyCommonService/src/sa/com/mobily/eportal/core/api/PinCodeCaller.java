//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

/**
 * This is an enumeration for the list of possible values of
 * the Pin Code Caller that can be used by the Value Objects of the module.
 */
public enum PinCodeCaller {
    Souq, PriorityPass, FlyWithNeqatyEtihad, FlyWithNeqatyRj, FlyWithNeqatyGulfAir, Neqatona, RahatiAuthGo, KhalihaAlaiaAuthGo, Cobone, NeqatyAuthGo, FlyWithNeqatyFlynas,NeqatyShukran 
}

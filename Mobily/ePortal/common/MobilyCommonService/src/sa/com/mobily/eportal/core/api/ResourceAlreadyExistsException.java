//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

/**
 * This exception is thrown by the REST APIs to provide the error code
 * defined by RESOURCE_ALREADY_EXISTS.
 *
 * @see ErrorCodes
 */
public class ResourceAlreadyExistsException extends ServiceException {

    private static final long serialVersionUID = 1L;

    public ResourceAlreadyExistsException(String message) {
        super(message, ErrorCodes.RESOURCE_ALREADY_EXISTS);
    }
    
    public ResourceAlreadyExistsException(String message, int errorCode) {
        super(message, errorCode);
    }
}

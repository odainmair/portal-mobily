//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

/**
 * This exception is thrown by the REST APIs to provide the error code
 * defined by RESOURCE_NOT_FOUND.
 *
 * @see ErrorCodes
 */
public class ResourceNotFoundException extends ServiceException {

    private static final long serialVersionUID = 1L;

    public ResourceNotFoundException(String message) {
        super(message, ErrorCodes.RESOURCE_NOT_FOUND);
    }
    
    public ResourceNotFoundException(String message, int errorCode) {
        super(message, errorCode);
    }
}

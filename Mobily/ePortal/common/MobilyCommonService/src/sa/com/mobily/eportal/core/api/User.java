//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

/**
 * A interface to provide read-only methods of the user information.
 * The Context interface returns an instance that implements this interface.
 * @see Context
 */
public interface User {
    
	/**
	 * Retrieves the user Id
	 * @return the user Id
	 */
    public String getUserId();

	/**
	 * Retrieves the service account number
	 * @return the service account number
	 */
    public String getServiceAccountNumber();
    
	/**
	 * Retrieves the user's default MSISDN
	 * @return the user's default MSISDN
	 */
    public String getDefaultMSISDN();
    
	/**
	 * Retrieves the currently selected line type
	 * @return the currently selected line type
	 */
    public abstract int getLineType();
    
	/**
	 * Retrieves the currently selected subscription type
	 * @return the currently selected subscription type
	 */
    public int getSubscriptionType();
    
	/**
	 * Retrieves the package Id
	 * @return the package Id
	 */
    public String getPackageId();
    
	/**
	 * Retrieves the customer type
	 * @return the customer type
	 */
    public String getCustomerType();
    
	/**
	 * Retrieves the Iqama of the user
	 * @return the Iqama of the user
	 */
    public String getIqama();
}

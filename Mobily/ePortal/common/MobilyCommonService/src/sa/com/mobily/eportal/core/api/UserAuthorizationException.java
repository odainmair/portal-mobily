//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

/**
 * This exception is thrown by the REST APIs to provide the error code
 * defined by ErrorCodes.USER_NOT_AUTHORIZED.
 * 
 * @see sa.com.mobily.eportal.core.api.ErrorCodes
 */
public class UserAuthorizationException extends ServiceException {

    private static final long serialVersionUID = 1L;

    public UserAuthorizationException(String message) {
        super(message, ErrorCodes.USER_NOT_AUTHORIZED);
    }
    
    public UserAuthorizationException(String message, int errorCode) {
        super(message, errorCode);
    }
}

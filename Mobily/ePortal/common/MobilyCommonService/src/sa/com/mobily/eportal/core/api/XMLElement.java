package sa.com.mobily.eportal.core.api;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;


/**
 * A class that contains a set of XML-handling utility methods
 * All methods of this class are static
 */
public class XMLElement {

	/**
	 * Parses the XML file contained in the given input stream and reads
	 * the main Document node which is returned from this method
	 * 
	 * @param in the input stream to read the XML data from
	 * @return the Document object which represents the root Node of the XML
	 * @throws Exception if an error occurs during parsing of the xml
	 */
	public static XMLElement parse(String in) throws Exception {
		// Get DOM Parser Factory
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		// Turn on validation, and turn off namespaces
		factory.setValidating(false);
		// factory.setNamespaceAware(true);
		// factory.setXIncludeAware(true);

		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(new InputSource(new StringReader(in)));
		
		return new XMLElement(doc.getDocumentElement());
	}
	
	private Element element;
	
	private XMLElement(Element element) {
		this.element = element;
	}
	
    public XMLElement getRoot() {
        return new XMLElement(element.getOwnerDocument().getDocumentElement());       
    }
    
    /**
     */
    public XMLElement getParent(String type) {
        Element root = element.getOwnerDocument().getDocumentElement();
        Element parent = (Element) element.getParentNode();
        while (parent != root && !type.equals(parent.getTagName())) {
            parent = (Element) parent.getParentNode();
        }
        return (type.equals(parent.getTagName())) ? new XMLElement(parent) : null;       
    }
    
    /**
     * Reads the child node which has the given name from the given parent node This method assumes that there is a
     * single child node with the given name under the given parent node, otherwise the first child found is returned<br/>
     * <br/>
     * It also accepts specifying a nested child in XPath style like <b>parent/nested/child</b> <br/>
     * if the child element or any of the elements in the path is not found null value is returned
     * 
     * @param name
     *            the name of the child node to read <br/>
     *            can be multiple node names separated by <b>/</b> to get a nested child node
     * @return the child node
     */
    public XMLElement getChild(String name) {
        if (name == null) return null;
        Element child = element;
        for (String nodeName : name.split("/")) {
            NodeList nodeList = child.getElementsByTagName(nodeName);
            if(nodeList.getLength() > 0) {
        	child = (Element)nodeList.item(0);
            }
            else {	// element not found
        	return null;
            }
        }
        
        return new XMLElement(child);
    }
    
    /**
     * Returns the list of child nodes that have the given name from
     * the given parent node
     * 
     * @param node the parent node to read the child nodes from
     * @return the list of child nodes
     */
    public List<XMLElement> getChildren() {
        return getChildren("*");
    }
    
    /**
     * Returns the list of child nodes that have the given name from
     * the given parent node
     * 
     * @param node the parent node to read the child nodes from
     * @param name the name of the child nodes to read
     * @return the list of child nodes
     */
    public List<XMLElement> getChildren(String name) {
        List<XMLElement> childNodes = new ArrayList<XMLElement>(); 
        if (name == null) return childNodes;
        
        NodeList nodeList = element.getElementsByTagName(name);
        int count = nodeList.getLength();
        for (int i = 0; i < count; i++) {
            Element e = (Element) nodeList.item(i);
            if (e.getParentNode() == element) {
                childNodes.add(new XMLElement(e));
            }
        }
        return childNodes;
    }

    public List<XMLElement> find(String query) throws XPathException {
    	List<XMLElement> childNodes = new ArrayList<XMLElement>();
    	
    	XPath xPath = XPathFactory.newInstance().newXPath();
    	
    	NodeList nodeList = (NodeList) xPath.evaluate(query, element, XPathConstants.NODESET);
    	int count = nodeList.getLength();
    	for (int i = 0; i < count; i++) {
    		childNodes.add(new XMLElement((Element) nodeList.item(i)));
    	}
    	return childNodes;
    }

    public XMLElement findElement(String query) throws XPathException {
    	XPath xPath = XPathFactory.newInstance().newXPath();
    	
    	Node result = (Node) xPath.evaluate(query, element, XPathConstants.NODE);
    	return (result != null)? new XMLElement((Element) result) : null;
    }
    
    public XMLElement findElement(String type, String id) {
        if (id == null) return null;
        try {
            return findElement("//" + type + "[@id='" + id + "']");
        } catch (XPathException e) { 
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns the value of the xml attribute with the given name that
     * is contained in the given node
     * 
     * @param node the node to read the attribute from
     * @param attrName the name of the attribute whose value is to be retrieved
     * @return the value corresponding to the given attribute
     */
    public String getAttribute(String attrName) {
    	String value = element.getAttribute(attrName);
    	return (value != null && value.length() == 0) ? null : value;
    }
    
    /**
     * Reads the text value contained in the given child node<br/>
     * <br/>
     * It also accepts specifying a nested child in XPath style like <b>parent/nested/child</b> <br/>
     * if the child element or any of the elements in the path is not found null value is returned
     * 
     * @param node the parent node to read the child node from
     * @param childName the name of the child node whose text value is to be retrieved
     * <br/>
     * can be multiple element names separated by <b>/</b> to get a nested child element
     * @return the text value of the child node
     */
    public String getChildValue(String childName) {
    	XMLElement childNode = getChild(childName);
    	return (childNode != null)? childNode.element.getTextContent() : null;
    }
    
    /***
     * Returns the Text contents of the current element
     * @return the Text contents of the current element
     */
    public String getValue() {
	return (element != null)? element.getTextContent() : null;
    }
    
    public String getType() {
        return element.getTagName();
    }
    
    public void assertType(String type) {
        String myType = getType();
        if (!myType.equals(type)) {
            throw new IllegalArgumentException("Invalid argument type: " + myType + ", " + type + " was expected.");
        }
    }
    
    public void assertType(String[] types) {
        String myType = getType();
        for (String type: types) {
            if (myType.equals(type)) return;
        }
        throw new IllegalArgumentException("Invalid argument type: " + myType + 
                ", one of:" + java.util.Arrays.deepToString(types) + " was expected.");
    }
}

package sa.com.mobily.eportal.core.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import sa.com.mobily.eportal.core.api.BackEndServiceError;
import sa.com.mobily.eportal.core.service.DataSources;

public class BackEndServiceErrorDAO extends AbstractDBDAO<BackEndServiceError> {

	private static BackEndServiceErrorDAO instance = new BackEndServiceErrorDAO();

	private static final String GET = "select * from (SELECT SERVICEID, ERRORCODE, ERRORMSGEN, ERRORMSGAR"
			+ " FROM BACKEND_SERVICE_ERROR"
			+ " WHERE SERVICEID = ? AND ERRORCODE = ?"
			+" union SELECT SERVICEID, ERRORCODE, ERRORMSGEN, ERRORMSGAR  FROM BACKEND_SERVICE_ERROR WHERE SERVICEID = -1 AND ERRORCODE = -1 "
			+" ) order by serviceID desc";
	protected BackEndServiceErrorDAO() {
		super(DataSources.DEFAULT);
	}

	/***
	 * Method for obtaining the singleton instance
	 * 
	 * @return
	 */
	public static BackEndServiceErrorDAO getInstance() {
		return instance;
	}

	@Override
	protected BackEndServiceError mapDTO(ResultSet rs) throws SQLException {
		BackEndServiceError backEndServiceError = new BackEndServiceError();
		backEndServiceError.setServiceId(rs.getLong("SERVICEID"));
		backEndServiceError.setErrorCode(rs.getLong("ERRORCODE"));
		backEndServiceError.setErrorMsgEn(rs.getString("ERRORMSGEN"));
		backEndServiceError.setErrorMsgAr(rs.getString("ERRORMSGAR"));
		return backEndServiceError;
	}

	public BackEndServiceError getBackEndServiceError(long serviceId,
			long errorCode) {
		List<BackEndServiceError> lstBackEndServiceError = query(GET,
				serviceId, errorCode);
		if (CollectionUtils.isNotEmpty(lstBackEndServiceError)) {
			return lstBackEndServiceError.get(0);
		}
		return null;
	}

	public static void main(String[] args) {
		try {
			// BackEndServiceErrorDAO dao =
			// BackEndServiceErrorDAO.getInstance();
			// BackEndServiceError obj = dao.getBackEndServiceError(12, 1);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
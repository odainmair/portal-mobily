//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.dao;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import sa.com.mobily.eportal.core.api.ErrorCodes;
import sa.com.mobily.eportal.core.api.ServiceException;
import sa.com.mobily.eportal.core.api.XMLElement;
import sa.com.mobily.eportal.core.service.Logger;

/**
 * Provides common methods that are applicable to all Xml Mappers
 * @author Hossam Omar
 *
 */
public class BaseXmlMapper {
    private static final Logger log = Logger.getLogger(BaseXmlMapper.class);

    /***
     * Returns the value of the tag with the name tagName, or throws ServiceException if the tag is missing/empty<br/>
     * @param xml the XmlElement to get the tag value from
     * @param tagName the name of the tag.<br/>
     * can also be an xPath like expression to indicate a nested element like parent/child/<b>tagName</b>
     * @return the text value inside the tag
     * @throws ServiceException if the specified tag is empty or couldn't be found
     */
    public String getRequiredTagValue(XMLElement xml, String tagName) throws ServiceException {
        String strTagValue = xml.getChildValue(tagName);
        if (strTagValue == null || strTagValue == "") {
            log.error("Tag [" + tagName + "] is " + (strTagValue == null? "missing" : "empty") + ".");
            throw new ServiceException("Invalid JMS reply structure");
        }
        
        return strTagValue;
    }

    /***
     * Returns the tag with the name tagName as an XMLElement, or throws ServiceException if the tag is missing<br/>
     * @param xml the XmlElement to get the tag from
     * @param tagName the name of the tag.<br/>
     * can also be an xPath like expression to indicate a nested element like parent/child/<b>tagName</b>
     * @return the tag as an XMLElement
     * @throws ServiceException if the specified tag couldn't be found
     */
    public XMLElement getRequiredTag(XMLElement xml, String tagName) throws ServiceException {
        XMLElement xmlTag = xml.getChild(tagName);
        if (xmlTag == null) {
            log.error("Tag [" + tagName + "] is missing.");
            throw new ServiceException("Invalid JMS reply structure");
        }
        
        return xmlTag;
    }

    /***
     * Parses the reply to the passed replyType class, or throws exception if the reply is invalid
     * @param reply the reply to be parsed
     * @throws ServiceException in case the request is empty or cannot be parsed into the passed replyType
     * @return parsed reply of type replyType
     * @throws ServiceException
     */
    public Object parseReply(String reply, Class<?> replyType) throws ServiceException {
        if(reply == null || reply.equals("")) {
            log.error("no reply received.");
            // TODO: throw proper error code
            throw new ServiceException("Invalid JMS Reply Structure");
        }
        
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(replyType);
            XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
            
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(new StringReader(reply));
            Object bslReply = unmarshaller.unmarshal(xmlStreamReader);
            
            return bslReply;
            
        } catch (JAXBException e) {
            // TODO throw proper error code
            log.error("Invalid Reply Structure", e);
            throw new ServiceException("Invalid JMS Reply Structure", e);
        } catch (XMLStreamException e) {
         // TODO throw proper error code
            log.error("Invalid Reply Structure", e);
            throw new ServiceException("Invalid JMS Reply Structure", e);
        }
    }

}

//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.core.api.LookupItem;
import sa.com.mobily.eportal.core.service.DataSources;

/**
 * Provides access to the lookup items stored in database
 * 
 * @author Hossm Omar
 */
public class LookupDBDAO extends AbstractDBDAO<LookupItem>
{

	/***
	 * Singleton instance of the class
	 */
	private static LookupDBDAO instance;

	// exclude 0 and negative SUB_IDs as they're reserved holding metadata about
	// the lookup
	private static final String QUERY_ALL = "SELECT SUB_ID, ITEM_DESC_EN, ITEM_DESC_AR" + " FROM SR_LOOKUP_TBL" + " WHERE ID = ?" + " AND SUB_ID > 0" + " ORDER BY SUB_ID";

	protected LookupDBDAO()
	{
		super(DataSources.DEFAULT);
	}

	/***
	 * Method for obtaining the singleton instance
	 * 
	 * @return
	 */
	public static synchronized LookupDBDAO getInstance()
	{
		if (instance == null)
		{
			instance = new LookupDBDAO();
		}
		return instance;
	}

	/***
	 * Returns the lookup items for the given lookupItemType
	 * 
	 * @param dbLookupItemType
	 *            the DB lookup item type
	 * @return all the lookup items for the given lookupItemType
	 * @see DBLookupItemType
	 */
	public List<LookupItem> getLookupItems(DBLookupItemType dbLookupItemType)
	{
		return super.query(QUERY_ALL, dbLookupItemType.getID());
	}

	@Override
	protected LookupItem mapDTO(ResultSet rs) throws SQLException
	{
		LookupItem item = new LookupItem();
		item.setId(rs.getString("SUB_ID"));
		item.setName(rs.getString("ITEM_DESC_EN"));
		item.setNameAr(rs.getString("ITEM_DESC_AR"));
		return item;
	}

	public static void main(String[] args)
	{
		LookupDBDAO lookupDBDAO = new LookupDBDAO();
		List<LookupItem> items = lookupDBDAO.getLookupItems(DBLookupItemType.IPTV_A_LA_CARTE);

		for (LookupItem lookupItem : items)
		{
			// System.out.println(lookupItem);
		}
	}
}
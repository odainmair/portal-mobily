//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.dao;


/**
 * Enumerates the available project IDs
 * @author Hossam Omar
 *
 */
public enum ProjectID {
    OTS(1),
    GIFTS_BUNDLE(2),
    CONNECT_OTS(3),
    CUSTOMER_REGISTRATION(4),
    APP_AWARDS_VOTING(5),
    LOYALTY_ETIHAD(6),
    LOYALTY_ROYALPLUS(7),
    ACTIVE_NUMBER(8),
    SOUQ(9),
    CONNECT_ALJ(10),
    LTE_OTS(11),
    LTE_CONNECT_4G_OTS(12),
    BUSINESS_OTS_IUC(13),
    BUSINESS_OTS_CUC(14),
    LOYALTY_GULFAIR(15),
    ITUNES_APP(16),
    LOYALTY_COBONE(17),
    WIFI(18),
    HAJJ_WIFI_2012(19),
    FACEBOOK_WAJID_INTEGRATION(20),
    CAP_REGISTRATION(21),
    EMAIL_BROADCAST_NEWSLETTER(22),
    PRIORITY_PASS(40),
    NEQATONA(41),
    LOYALTY_FLYNAS(64),
    NeqatyShukran(90);
    
    private int value;
    private ProjectID(int value) {
	this.value = value;
    }
    
    public int getValue() {
	return value;
    }
}

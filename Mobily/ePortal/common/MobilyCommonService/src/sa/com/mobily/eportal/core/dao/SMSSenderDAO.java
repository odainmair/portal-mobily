//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.dao;

import javax.naming.NamingException;

import sa.com.mobily.eportal.core.api.ErrorCodes;
import sa.com.mobily.eportal.core.api.ServiceException;
import sa.com.mobily.eportal.core.service.EPortalUtils;
import sa.com.mobily.eportal.core.service.JMSConnectionManager;
import sa.com.mobily.eportal.core.service.Logger;
import sa.com.mobily.eportal.core.service.Queues;

/**
 * Provides operations for sending SMS messages
 * @author Hossam Omar
 *
 */
public class SMSSenderDAO {
    
    private static final Logger log = Logger.getLogger(SMSSenderDAO.class);
    /**
     * The separator between the message parts
     */
    private static final String PARAM_SEPARATOR = ",";
    /**
     * The separator between the message parameters
     */
    private static final String MSG_PART_SEPARATOR = ";";
    /***
     * Singleton instance of the class
     */
    private static SMSSenderDAO instance = new SMSSenderDAO();

    protected SMSSenderDAO() {
    }

    /***
     * Method for obtaining the singleton instance
     * @return
     */
    public static SMSSenderDAO getInstance() {
	return instance;
    }
    
    /***
     * Sends an SMS to the passed MSISDN
     * @param MSISDN	the number to which this message will be sent
     * @param messageID	the message id
     * @param parameters (optional) any parameters that needs to be substituted in the message
     * @throws ServiceException 
     */
    public void sendSMS(String MSISDN, String messageID, String...parameters) throws ServiceException {
	StringBuffer request = new StringBuffer("");
	// append "msisdn;msgID"
	request.append(MSISDN).append(MSG_PART_SEPARATOR).append(messageID);
	
	// if there are parameters
	if(parameters != null && parameters.length > 0) {
	    request.append(MSG_PART_SEPARATOR);
	    request.append(EPortalUtils.combineWith(parameters, PARAM_SEPARATOR));
	}
	
	log.debug(request.toString());
	
	try {
	    JMSConnectionManager jmsConnectionManager = JMSConnectionManager.getInstance();
	    jmsConnectionManager.sendMessage(request.toString(), Queues.SMS_REQUEST, Queues.SMS_REPLY);

	} catch (NamingException e) {
	    throw new ServiceException("Failed to send SMS, JMS lookup failure.", e);
	} catch (Exception e) {
	    throw new ServiceException("Failed to send SMS.", e);
	}
    }
}

//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.service;

import java.util.List;
import java.util.Map;

import sa.com.mobily.eportal.core.api.Context;
import sa.com.mobily.eportal.core.api.LookupItem;
import sa.com.mobily.eportal.core.api.ServiceException;
import sa.com.mobily.eportal.core.dao.DBLookupItemType;
import sa.com.mobily.eportal.core.dao.FavoriteCountryDAO;
import sa.com.mobily.eportal.core.dao.LookupDBDAO;

/**
 * The implementation class that provides the set of backend services
 * needed by the Country resource.
 */
class CountryHandler {

    /**
     * Retrieves the list of Country resources
     *
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param params the list of name/value pair parameters associated with the query
     * @return the list of of resources
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
     * @see LookupItem
     */
    List<LookupItem> find(Context ctx, Map<String, String> params) throws ServiceException {
        if(params != null){
            String param = params.get("fav");
            if(param != null && "true".equals(param)){
                return FavoriteCountryDAO.getInstance().getFavoriteCountries(ctx.getLocale().getLanguage());
            }
        }
        return LookupDBDAO.getInstance().getLookupItems(DBLookupItemType.COUNTRY);
    }
}

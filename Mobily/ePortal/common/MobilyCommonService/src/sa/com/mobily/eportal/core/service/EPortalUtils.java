package sa.com.mobily.eportal.core.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import sa.com.mobily.eportal.core.api.ErrorCodes;

/***
 * Provides access to the ePortal system configurations and provides common utility methods
 * @author Hossam Omar
 *
 */
public class EPortalUtils {
    
    /***
     * Enumeration that holds all the available ePortal property names
     * @author Hossam Omar
     *
     */
    public enum EPortalProperty {
    	ALLOWED_UPLOAD_EXTENSIONS("allowed-upload-extensions")
    	, MAIL_SMTP_HOST("mail-smtp-host")
    	, JMS_TIMEOUT("jms.timeout");
	
	private EPortalProperty(final String value) {
	    this.value = value;
	}
	
	@Override
	public String toString() {
	    return value;
	}
	
	private final String value;
    }
    
    private static final String EPORTAL_CONFIG_PROPERTIES = "config.eportal-config";
    private static final String DEFAULT_VALUES_SEPARATOR = ",";
    private static final Logger log = Logger.getLogger(EPortalUtils.class);
    private static ResourceBundle ePortalProperties = null;
    
    static {
	try {
	    ePortalProperties = ResourceBundle.getBundle(EPORTAL_CONFIG_PROPERTIES);
        } catch (MissingResourceException e) {
          //TODO: throw mobily exception instead
            log.error("Unable to load " + EPORTAL_CONFIG_PROPERTIES, e);
            throw new RuntimeException("Unable to load " + EPORTAL_CONFIG_PROPERTIES, e);	//TODO: throw mobily exception instead
        }
    }
    
    /***
     * Gets a configured property from the default ePortal properties
     * 
     * @param property
     *            the property to be obtained
     * @return property value, or null if property is not found
     */
    public static String getProperty(EPortalProperty property) {
	return ePortalProperties.getString(property.value);
    }
    
    /***
     * gets a property value from a specific bundleName
     * @param bundleName the bundle base name
     * @param propertyName the property name
     * @return the property value or null if property/bundle not found
     */
    public static String getProperty(String bundleName, String propertyName) {
	return getProperty(bundleName, null, propertyName);
    }
    
    /***
     * gets a property value from a specific bundleName
     * @param bundleName the bundle base name
     * @param propertyName the property name
     * @param classLoader the class loader to be used for loading the bundle properties
     * @return the property value or null if property/bundle not found
     */
	public static String getProperty(String bundleName, String propertyName, ClassLoader classLoader) {
		return getProperty(bundleName, Locale.getDefault(), propertyName, classLoader);
	}
    
    /***
     * gets a property value from a specific bundleName
     * @param bundleName the bundle base name
     * @param locale a specific locale to get the resource bundle for. if null, default locale will be used.
     * @param propertyName the property name
     * @return the property value or null if property/bundle not found
     */
    public static String getProperty(String bundleName, Locale locale, String propertyName) {
    	return getProperty(bundleName, locale, propertyName, null);
    }
    
    /***
     * gets a property value from a specific bundleName
     * @param bundleName the bundle base name
     * @param locale a specific locale to get the resource bundle for. if null, default locale will be used.
     * @param propertyName the property name
     * @param classLoader the class loader to be used for loading the bundle properties
     * @return the property value or null if property/bundle not found
     */
	public static String getProperty(String bundleName, Locale locale, String propertyName, ClassLoader classLoader)
	{
		try {
			if (locale == null) {
				locale = Locale.getDefault();
			}

			ResourceBundle bundle = classLoader == null ? ResourceBundle.getBundle(bundleName, locale) : ResourceBundle.getBundle(bundleName, locale, classLoader);
			return bundle.getString(propertyName);
		} catch (MissingResourceException e) {
			log.error("Unable to get property " + propertyName + " from bundle " + bundleName, "getProperty", null);
		}

		return null;
	}
    
    /***
     * Gets all the values of a multi-valued property from the default ePortal properties
     * 
     * @param property
     *            name of the multi-valued property
     * @return values of the property, or null if property is not found
     */
    public static String[] getPropertyValues(EPortalProperty property) {
	String values = ePortalProperties.getString(property.value);
	if(values != null) {
	    return values.split(DEFAULT_VALUES_SEPARATOR);
	}
	
	// Property not found
	return null;
    }
    
    /***
     * Gets all the values of a multi-valued property from the a specific resource bundle
     * 
     * @param property
     *            name of the multi-valued property
     * @return values of the property, or null if property/bundle is not found
     */
    public static String[] getPropertyValues(String bundleName, String propertyName) {
	String values = getProperty(bundleName, propertyName);
	if(values != null) {
	    return values.split(DEFAULT_VALUES_SEPARATOR);
	}
	
	// Property not found
	return null;
    }
    
    /***
     * Combines a list of values into a string
     * @param values the values to be combined
     * @param separator the separator between the values
     * @return string containing the values in the array separated by the combiner
     */
    public static String combineWith(Object[] values, String separator) {
	if(values == null || values.length == 0) {
	    return "";
	}
	
	StringBuffer sbValues = new StringBuffer(values[0].toString());
	
	for (int i = 1; i < values.length; i++) {
	    sbValues.append(separator);
	    sbValues.append(values[i]);
        }
	
	return sbValues.toString();
    }
    
    public static String formatDate(Date date, String format) {
	SimpleDateFormat dateFormat = new SimpleDateFormat(format);
	return dateFormat.format(date);
    }
    
	public static String getUniversalMsisdn(String MSISDN) {
		if (MSISDN != null && MSISDN.startsWith("00")) {
			MSISDN = MSISDN.substring(2);
		}
		
		if (MSISDN == null || MSISDN.trim().equals("") || MSISDN.startsWith(EPortalConstants.PREFIX_CONNECT_ACCOUNT)||MSISDN.startsWith("966")) {
			return MSISDN;
		} else {
			return EPortalConstants.PREFIX_UNIVERSAL + getLast8digits(MSISDN);
		}
	}
    	
	private static String getLast8digits(String MSISDN) {
		if (MSISDN == null || MSISDN.trim().equals("")) {
			return MSISDN;
		}

		MSISDN = MSISDN.trim();
		if (MSISDN.length() > 8)
			return MSISDN.substring(MSISDN.length() - 8);
		else
			return MSISDN;
	}
    
    public static String getUserFriendlyBalance(String strBalance) {
    	log.info("getUserFriendlyBalance : start :strBalance = "+strBalance);
    	if (strBalance == null || strBalance.trim().equals("")) {
    		if(strBalance.indexOf(".") != -1) {
    			if(Double.parseDouble(strBalance.substring(strBalance.indexOf(".")+1)) <= 0){
    				strBalance = strBalance.substring(0,strBalance.indexOf("."));
    			} else if(strBalance.substring(strBalance.indexOf(".")+1).length() > 2) {
    				strBalance = strBalance.substring(0,strBalance.indexOf(".")+3);
    			}
    		}
    	}else {
    		strBalance = "";
    	}
    	
    	log.info("getUserFriendlyBalance : end :strBalance = "+strBalance);
    	return strBalance;
    }
    
    public static int parseErrorCode(String errorCode) {
        try {
            return Integer.parseInt(errorCode);
        } catch (NumberFormatException e) {
            log.error("Unable to parse error code [" + errorCode + "]");
        }
        
        return ErrorCodes.GENERAL_ERROR;
    }
}
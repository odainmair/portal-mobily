package sa.com.mobily.eportal.core.service;

import java.util.Date;

import javax.jms.Queue;

import sa.com.mobily.eportal.common.service.mq.dao.MQAuditDAO;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MQUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.core.api.ServiceException;
import sa.com.mobily.eportal.core.service.EPortalUtils;
import sa.com.mobily.eportal.core.service.EPortalUtils.EPortalProperty;
import sa.com.mobily.eportal.core.service.Logger;
import sa.com.mobily.eportal.core.service.Queues;
import sa.com.mobily.eportal.core.service.ServiceLocator;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

public class FutureResponse<T>
{

	private static final Logger log = Logger.getLogger(FutureResponse.class);

	private ExecutionContext ec = ExecutionContextFactory.getExecutionContext();

	private final int DEFAULT_TIMEOUT = Integer.valueOf(EPortalUtils.getProperty(EPortalProperty.JMS_TIMEOUT)).intValue();

	private ResponseHandler<T> responseHandler;

	private Queues requestQ;

	private Queues replyQ;

	private String correlationId;

	private String request;

	private String response;

	private T responseObj;

	private boolean isResponseHandled = false;

	FutureResponse(Queues requestQ, Queues replyQ, String request, String correlationId, ResponseHandler<T> responseHandler)
	{
		this.responseHandler = responseHandler;
		this.requestQ = requestQ;
		this.replyQ = replyQ;
		this.request = request;
		this.correlationId = correlationId;
	}

	public T getResponse() throws ServiceException
	{
		return getResponse(DEFAULT_TIMEOUT);
	}

	public T getResponse(int timeout) throws ServiceException
	{
		int waitingtime = -1;
		
		// if the response has already been handled
		if (isResponseHandled)
		{
			return responseObj;
		}

		try
		{
			JMSConnectionManager connectionManager = JMSConnectionManager.getInstance();
			ec.fine("FR> Receiving msg with correlation id [" + correlationId + "], timeout [" + DEFAULT_TIMEOUT + "]");

			long timeBeforeSending = new Date().getTime() / 1000;
			
			response = connectionManager.receiveMessage(replyQ, correlationId, timeout);
			
			waitingtime = (int) (new Date().getTime() / 1000 - timeBeforeSending);
			
			ec.fine("FR> Message received successfully [" + response + "]");
			
			ec.severe("FR >  getResponse > waitingtime =["+waitingtime+"]");
		}
		catch (Exception ex)
		{
			ec.fine("FR> Failed to receive future response" + ex.getMessage());
			// ex.printStackTrace(System.out);
			throw new ServiceException("Failed to receive future message", ex);
		}
		finally
		{
			
        	String isAuditRequired = ResourceEnvironmentProviderService.getInstance().getStringProperty(ConstantsIfc.MQ_AUDIT_REQUIRED);
        	ec.severe("FR >  getResponse > isAuditRequired =["+isAuditRequired+"]");

            if(FormatterUtility.isEmpty(isAuditRequired)
            		|| ConstantsIfc.TRUE.equalsIgnoreCase(FormatterUtility.checkNull(isAuditRequired))){

				// The audit method needs to be in the finally method because its
				// not executing once the exception occurs
				audit(waitingtime);
				ec.severe("FR >  getResponse > MQ request Saved Successfuly");
				
            }else{
            	ec.severe("FR >  getResponse > MQ request Audit Not Required....");
            }
		}

		responseObj = responseHandler.handleResponse(response);
		ec.fine("FR> Response Handled");
		isResponseHandled = true;
		return responseObj;
	}

	private void audit(Integer waitingTime )
	{
		try
		{
			String requestQName = JMSConnectionManager.getValidQueueName(((Queue) ServiceLocator.lookupQueue(requestQ)).getQueueName());
			String replyQName = JMSConnectionManager.getValidQueueName(((Queue) ServiceLocator.lookupQueue(replyQ)).getQueueName());

			MQAuditVO mqAuditVO = new MQAuditVO();

				mqAuditVO.setRequestQueue(requestQName);
				mqAuditVO.setReplyQueue(replyQName);
				mqAuditVO.setMessage(request);
				mqAuditVO.setReply(response);
				mqAuditVO.setWaitingTime(waitingTime);

			MQUtility.saveMQAudit(mqAuditVO);
			
			//MQUtility.saveMQAudit(requestQName, replyQName, request, response);

			// clear request (no longer needed)
			request = null;
		}
		catch (Exception ex)
		{
			log.error("An error happens while auditing MQ future response", "audit", ex);
		}
	}
}

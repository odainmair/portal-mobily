//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.service;

/**
 * An exception class thrown when the URL mapping is incorrect, such as when
 * a parameter in the URL patter cannot be found.
 */
public class InvalidParametersException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InvalidParametersException(String message) {
        super(message);
    }
    
    public InvalidParametersException(String message, Throwable cause) {
        super(message, cause);
    }
}

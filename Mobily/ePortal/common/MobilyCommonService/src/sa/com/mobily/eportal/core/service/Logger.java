//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.service;

import java.io.Serializable;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

/***
 * The default logger class used across the application
 * @author Hossam Omar
 * @author Anas Mosaad
 *
 */
public class Logger implements Serializable {

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//    private final java.util.logging.Logger log;
    private final String className;
    
    public static final ExecutionContext getExecutionContext() {
    	return ExecutionContextFactory.getExecutionContext();
    }

    private Logger(String className) {
//        this.log = java.util.logging.Logger.getLogger(className);
        this.className = className;
    }

    public static Logger getLogger(@SuppressWarnings("rawtypes") Class cls) {
        return new Logger(cls.getName());
    }
    public static Logger getLogger(String className) {
        return new Logger(className);
    }
    
    private static final String getCallerMethodName() {
    	StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
//    	System.out.println("method name is: " + stackTraceElements[2].getMethodName());
//    	System.out.println("method name is: " + stackTraceElements[3].getMethodName());
//    	System.out.println("method name is: " + stackTraceElements[4].getMethodName());
//    	System.out.println("method name is: " + stackTraceElements[5].getMethodName());
    	return stackTraceElements[4].getMethodName();
    }

    /**
     * Log information message in INFO level if the level is enabled.
     * @param message - String object to be logged
     */
    public void info(String message, String methodName) {
//        log.info(message);
    	
    	//TODO remove this the logger issue is resolved ...
    	System.out.println("[Mobily Portal] " + methodName + ": " + message);
        getExecutionContext().audit(Level.INFO, message, className, methodName);
    }
    
    /**
     * 
     * @deprecated use Logger.info(String message, String methodName) instead
     */
    @Deprecated 
    public void info(String message) {
    	info(message, getCallerMethodName());
    }
    
    /**
     * Log warning message in WARNING level if the level is enabled.
     * @param message - String object to be logged
     */
    public void warn(String message, String methodName) {
//        log.warning(message);
        getExecutionContext().log(Level.WARNING, message, className, methodName, null);
    }

    /**
     * 
     * @deprecated use Logger.warn(String message, String methodName) instead
     */
    @Deprecated
    public void warn(String message) {
    	warn(message, getCallerMethodName());
    }

    /**
     * Log error message in SEVERE level if the level is enabled.
     * @param message - String object to be logged
     */
    public void error(String message, String methodName) {
//        log.severe(message);
        getExecutionContext().log(Level.SEVERE, message, className, methodName, null);
    }

    /**
     * Log exception and an euivalent message in FATAL level if the level is enabled.
     *
     * @param message - String object to be logged
     * @param thrown - the Throwable object to be logged
     */
    public void error(String message, String methodName, Throwable thrown) {
//        log.log(Level.SEVERE, message, thrown);
        getExecutionContext().log(Level.SEVERE, message, className, methodName, thrown);
    }

    /**
     * 
     * @deprecated use Logger.error(String message, String methodName, Throwable thrown) instead
     */
    @Deprecated
    public void error(String message, Throwable thrown) {
    	error(message, getCallerMethodName(), thrown);
    }

    /**
     * 
     * @deprecated use Logger.error(String message, String methodName) instead
     */
    @Deprecated
    public void error(String message) {
    	error(message, getCallerMethodName(), null);
    }

    /**
     * Log debug message in FINEST level if the level is enabled.
     * @param message - String object to be logged
     */
    public void debug(String message, String methodName) {
//        log.finest(message);
        getExecutionContext().audit(Level.FINER, message, className, methodName);
    }

    /**
     * 
     * @deprecated use Logger.debug(String message, String methodName) instead
     */
    @Deprecated
    public void debug(String message) {
    	debug(message, getCallerMethodName());
    }

    /**
     * Log method entry in FINEST level if the level is enabled.
     * @param methodName - String
     * @param className the class name
     */
    public void entering(String methodName) {
//    	getExecutionContext().audit(Level.FINEST, "ENTRY", className, methodName);
    	getExecutionContext().startMethod(className, methodName, null);
    }

    /**
     * Log method entry in FINEST level if the level is enabled.
     * @param methodName - String
     */
    public void exiting(String methodName) {
//    	getExecutionContext().audit(Level.FINEST, "RETURN", className, methodName);
    	getExecutionContext().endMethod(className, methodName, null);
    }
    
    /** 
     * Logs a message indicating throwing an exception. A log record with log
    * level <code>Level.FINER</code>, log message "THROW", and the current
    * source class name and source method name and <code>Throwable</code>
    * object is submitted for logging.
    * 
    * @param methodName
    *            the source method name
    * @param thrown
    *            the <code>Throwable</code> object
    */
    public void throwing(String methodName, Throwable thrown) {
//    	log.throwing(className, sourceMethod, thrown);
    	getExecutionContext().log(Level.FINER, "THROW", className, methodName, thrown);
    }
    

    public void flush(String fileName, boolean exception) {
    	getExecutionContext().print(fileName, exception);
    	getExecutionContext().clearExecutionContext();
    }
    public void flush(String fileName) {
    	flush(fileName, false);
    }
    
    
    public static void main(String[] args)
	{
		getLogger(Logger.class).info("me");
	}
}

//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.service;

import java.util.Locale;

import sa.com.mobily.eportal.common.service.sms.PinCodeSender;
import sa.com.mobily.eportal.core.api.Context;
import sa.com.mobily.eportal.core.api.PinCode;
import sa.com.mobily.eportal.core.api.PinCodeCaller;
import sa.com.mobily.eportal.core.api.ServiceException;
import sa.com.mobily.eportal.core.dao.PinCodeDAO;
import sa.com.mobily.eportal.core.utils.ValidationUtils;
import sa.com.mobily.valueobject.common.PinCodeVO;

/**
 * The implementation class that provides the set of backend services
 * needed by the Pin Code resource.
 */
class PinCodeHandler {
    private static final Logger log = Logger.getLogger(PinCodeHandler.class);
    
    // SMS Message IDs
    public static final String SMS_ID_ACTIVATION_CODE_EN = "2413";
    public static final String SMS_ID_ACTIVATION_CODE_AR = "2414";
    public static final String SMS_ID_SOUQ_EN ="11121";
    public static final String SMS_ID_SOUQ_AR ="11120";
    public static final String SMS_ID_FLYNAS_EN ="824758";
    public static final String SMS_ID_FLYNAS_AR ="824759";
    
    /***
     * The number of digits in the pin code
     */
    private static final int PIN_CODE_DIGITS = 8;

    /***
     * Generates a new pin code for the current user and the specified projectId<br/>
     * The new PinCode is saved in the DB
     * @param ctx	the context object
     * @param projectId	the project id for which this pin code is generated to
     * @return the newly generated pinCode
     * @throws InvalidParametersException if the projectId is missing/invalid
     */
    PinCode create(Context ctx, PinCode resource) throws ServiceException {
    	/*
        log.entering("create");
	
        User currentUser = ctx.getCurrentUser();
        
        validatePinCodeCaller(resource.getPinCodeCaller());
        
        // generate the pin code
        resource.setCode(String.valueOf(Math.random()).substring(2, 2 + PIN_CODE_DIGITS));
        
        // get the messageID to be used for this activation code SMS
        String messageID = getMessageID(resource.getPinCodeCaller(), ctx.getLocale());
        
        // send the pin code in an SMS to the user
        SMSSenderDAO.getInstance().sendSMS(currentUser.getDefaultMSISDN(), messageID);
        
        // save the pinCode
        PinCodeDAO.getInstance().insertOrUpdatePinCode(currentUser.getDefaultMSISDN(), resource);
        
        log.exiting("create");
        return resource;
        */
    	
    	String messageID = getMessageID(resource.getPinCodeCaller(), ctx.getLocale());
        log.entering("create");
        PinCodeVO pinCode = new PinCodeVO();
        pinCode.setLanguage(ctx.getLocale().getLanguage());
    	log.debug("resource.getPinCodeCaller() ... " + resource.getPinCodeCaller());
        if (resource.getPinCodeCaller() == PinCodeCaller.NeqatyAuthGo) {
        	log.debug("MSISDN resource.getCode() ... " + resource.getCode());
        	pinCode.setMsisdn(resource.getCode());
        } else {
        	log.debug("MSISDN ctx.getCurrentUser().getDefaultMSISDN() ... " + ctx.getCurrentUser().getDefaultMSISDN());
        	pinCode.setMsisdn(ctx.getCurrentUser().getDefaultMSISDN());
        }
    	pinCode.setServiceID(PinCodeDAO.getProjectId(resource.getPinCodeCaller()));
    	if ( ctx.getLocale().getLanguage().equalsIgnoreCase("ar") )
    	{
    		pinCode.setMessageIdAr(messageID);
    	}
    	else
    	{
    		pinCode.setMessageIdEn(messageID);
    	}
    	
        PinCodeSender.sendPINCode(pinCode);
        log.exiting("create");
        return resource;
    }

    /**
     * Returns the SMS messageID for a give pinCodeCaller
     * @param pinCodeCaller the pin code caller
     * @param locale the message locale
     * @throws ServiceException if no messageID is mapped for the passed pinCodeCaller
     */
    private String getMessageID(PinCodeCaller pinCodeCaller, Locale locale) throws ServiceException {
	boolean isArabic = "ar".equals(locale.getLanguage());
	
		if (PinCodeCaller.Souq == pinCodeCaller) {
		    return isArabic ? SMS_ID_SOUQ_AR : SMS_ID_SOUQ_EN;
		} else if (PinCodeCaller.FlyWithNeqatyFlynas == pinCodeCaller) {
		    return isArabic ? SMS_ID_FLYNAS_AR : SMS_ID_FLYNAS_EN;
		} else {
		    return isArabic ? SMS_ID_ACTIVATION_CODE_AR : SMS_ID_ACTIVATION_CODE_EN;
		}
    }
    
    /***
     * Validates the pinCodeCaller is one of the Auth&Go permitted pin code callers
     * @param pinCodeCaller the pin code caller
     * @throws ServiceException if pinCodeCaller is null, or not a valid Auth&Go pin code caller
     */
    private void validatePinCodeCaller(PinCodeCaller pinCodeCaller) throws ServiceException {
	if(pinCodeCaller == null) {
	    throw new ServiceException("Missing PinCodeCaller.");
	}
	
	// if pinCodeCaller is one of the Auth&Go pin code callers	
	if(ValidationUtils.equalsAny(pinCodeCaller, PinCodeCaller.RahatiAuthGo, PinCodeCaller.KhalihaAlaiaAuthGo)) {
	    log.error("PinCodeCaller [" + pinCodeCaller + "] is not a valid pin code caller");
	    throw new ServiceException("PinCodeCaller [" + pinCodeCaller + "] is not a valid pin code caller");
	}
    }
}

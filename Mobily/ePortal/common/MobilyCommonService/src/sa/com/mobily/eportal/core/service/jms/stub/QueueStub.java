package sa.com.mobily.eportal.core.service.jms.stub;

import java.util.List;

class QueueStub {
    private String stubFileName;
    private String queueName;
    private List<String> requestFilters;
    private String response;

    public String getStubFileName() {
        return stubFileName;
    }

    public void setStubFileName(String stubFileName) {
        this.stubFileName = stubFileName;
    }

    public String getQueueName() {
	return queueName;
    }

    public void setQueueName(String queueName) {
	this.queueName = queueName;
    }

    public List<String> getRequestFilters() {
	return requestFilters;
    }

    public void setRequestFilters(List<String> requestFilters) {
	this.requestFilters = requestFilters;
    }

    public String getResponse() {
	return response;
    }

    public void setResponse(String response) {
	this.response = response;
    }

    @Override
    public String toString() {
	return "QueueStub [stubFileName=" + stubFileName + ", queueName="
	        + queueName + ", requestFilters=" + requestFilters + "]";
    }
}

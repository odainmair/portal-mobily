package sa.com.mobily.eportal.corporate.model;

import java.sql.Date;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class AccountHierarchy extends BaseVO
{
	private static final long serialVersionUID = 7416128222982602862L;

	private String ID;

	private String PARENT_ID;

	private String CUSTOMER_ID;

	private String ACCOUNT_NO;

	private String ACCOUNT_TYPE;

	private String ACCOUNT_STATUS;

	private String NAME;

	private String COMPANY_NAME;

	private String IS_MBA;

	private String ALIAS_NAME;

	private String EMAIL;

	private String MSISDN;

	private String SIM;

	private String VPNID;

	private String PACKAGE_NAME;

	private String PACKAGE_ID;

	private String LINE_OF_BUSINESS;

	private String PRODUCT_FAMILY;

	private String COMM_PLAN_TYPE;

	private String PRODUCT_TYPE;

	private String CUSTOMER_SEGMENT;

	private String ICE_CUBE_ELIGIBILITY;

	private int NO_OF_ACTIVE_SIMS;

	private Date LAST_UPDATE_DATE;
	
	//Added by Ravi for Virtual IBAN project on 15-feb-2017 -start
	private String DUE_AMOUNT;
	
	private String AMT_TO_BE_PAID;
	
	private String V_IBAN;
	
	private String PACKAGE_NAME_E;
	
	private String PACKAGE_NAME_A;
	
	private String CREDIT_BALANCE;
	//Added new field for date
	private Date AMOUNT_UPDATED_DATE;
	
	// JK - 41088
	private String REMAINING_MONTHS;
	
	private String MODE_PACKAGE_NAME_EN;
	
	private String MODE_PACKAGE_NAME_AR;
	
	//Added for B2B Drop2 project
	private int USER_ID;
	private String USER_NAME_AR;
	private String USER_NAME_EN;
	private String USER_DOC_TYPE;
	private String USER_DOC_ID;
	private Date ID_EXPIRY_DATE;
	private String FINGER_PRINT_STATUS;
	private Date FINGER_PRINT_DATE;
	private String IMSI;
	private String PIN;
	private String PIN2;
	private String PUK;
	private String PUK2;
	
	public Date getAMOUNT_UPDATED_DATE()
	{
		return AMOUNT_UPDATED_DATE;
	}

	public void setAMOUNT_UPDATED_DATE(Date aMOUNT_UPDATED_DATE)
	{
		AMOUNT_UPDATED_DATE = aMOUNT_UPDATED_DATE;
	}

	public String getCREDIT_BALANCE()
	{
		return CREDIT_BALANCE;
	}

	public void setCREDIT_BALANCE(String cREDIT_BALANCE)
	{
		CREDIT_BALANCE = cREDIT_BALANCE;
	}

	public String getPACKAGE_NAME_E()
	{
		return PACKAGE_NAME_E;
	}

	public void setPACKAGE_NAME_E(String pACKAGE_NAME_E)
	{
		PACKAGE_NAME_E = pACKAGE_NAME_E;
	}

	public String getPACKAGE_NAME_A()
	{
		return PACKAGE_NAME_A;
	}

	public void setPACKAGE_NAME_A(String pACKAGE_NAME_A)
	{
		PACKAGE_NAME_A = pACKAGE_NAME_A;
	}

	

	public String getDUE_AMOUNT()
	{
		return DUE_AMOUNT;
	}

	public void setDUE_AMOUNT(String dUE_AMOUNT)
	{
		DUE_AMOUNT = dUE_AMOUNT;
	}

	public String getAMT_TO_BE_PAID()
	{
		return AMT_TO_BE_PAID;
	}

	public void setAMT_TO_BE_PAID(String aMT_TO_BE_PAID)
	{
		AMT_TO_BE_PAID = aMT_TO_BE_PAID;
	}

	public String getV_IBAN()
	{
		return V_IBAN;
	}

	public void setV_IBAN(String v_IBAN)
	{
		V_IBAN = v_IBAN;
	}

	//Added by Ravi for Virtual IBAN project on 15-feb-2017 -end
	public String getID()
	{
		return ID;
	}

	public void setID(String iD)
	{
		ID = iD;
	}

	public String getPARENT_ID()
	{
		return PARENT_ID;
	}

	public void setPARENT_ID(String pARENT_ID)
	{
		PARENT_ID = pARENT_ID;
	}

	public String getCUSTOMER_ID()
	{
		return CUSTOMER_ID;
	}

	public void setCUSTOMER_ID(String cUSTOMER_ID)
	{
		CUSTOMER_ID = cUSTOMER_ID;
	}

	public String getACCOUNT_NO()
	{
		return ACCOUNT_NO;
	}

	public void setACCOUNT_NO(String aCCOUNT_NO)
	{
		ACCOUNT_NO = aCCOUNT_NO;
	}

	public String getACCOUNT_TYPE()
	{
		return ACCOUNT_TYPE;
	}

	public void setACCOUNT_TYPE(String aCCOUNT_TYPE)
	{
		ACCOUNT_TYPE = aCCOUNT_TYPE;
	}

	public String getACCOUNT_STATUS()
	{
		return ACCOUNT_STATUS;
	}

	public void setACCOUNT_STATUS(String aCCOUNT_STATUS)
	{
		ACCOUNT_STATUS = aCCOUNT_STATUS;
	}

	public String getNAME()
	{
		return NAME;
	}

	public void setNAME(String nAME)
	{
		NAME = nAME;
	}

	public String getCOMPANY_NAME()
	{
		return COMPANY_NAME;
	}

	public void setCOMPANY_NAME(String cOMPANY_NAME)
	{
		COMPANY_NAME = cOMPANY_NAME;
	}

	public String getIS_MBA()
	{
		return IS_MBA;
	}

	public void setIS_MBA(String iS_MBA)
	{
		IS_MBA = iS_MBA;
	}

	public String getALIAS_NAME()
	{
		return ALIAS_NAME;
	}

	public void setALIAS_NAME(String aLIAS_NAME)
	{
		ALIAS_NAME = aLIAS_NAME;
	}

	public String getEMAIL()
	{
		return EMAIL;
	}

	public void setEMAIL(String eMAIL)
	{
		EMAIL = eMAIL;
	}

	public String getMSISDN()
	{
		return MSISDN;
	}

	public void setMSISDN(String mSISDN)
	{
		MSISDN = mSISDN;
	}

	public String getSIM()
	{
		return SIM;
	}

	public void setSIM(String sIM)
	{
		SIM = sIM;
	}

	public String getVPNID()
	{
		return VPNID;
	}

	public void setVPNID(String vPNID)
	{
		VPNID = vPNID;
	}

	public String getPACKAGE_NAME()
	{
		return PACKAGE_NAME;
	}

	public void setPACKAGE_NAME(String pACKAGE_NAME)
	{
		PACKAGE_NAME = pACKAGE_NAME;
	}

	public String getPACKAGE_ID()
	{
		return PACKAGE_ID;
	}

	public void setPACKAGE_ID(String pACKAGE_ID)
	{
		PACKAGE_ID = pACKAGE_ID;
	}

	public String getLINE_OF_BUSINESS()
	{
		return LINE_OF_BUSINESS;
	}

	public void setLINE_OF_BUSINESS(String lINE_OF_BUSINESS)
	{
		LINE_OF_BUSINESS = lINE_OF_BUSINESS;
	}

	public String getPRODUCT_FAMILY()
	{
		return PRODUCT_FAMILY;
	}

	public void setPRODUCT_FAMILY(String pRODUCT_FAMILY)
	{
		PRODUCT_FAMILY = pRODUCT_FAMILY;
	}

	public String getCOMM_PLAN_TYPE()
	{
		return COMM_PLAN_TYPE;
	}

	public void setCOMM_PLAN_TYPE(String cOMM_PLAN_TYPE)
	{
		COMM_PLAN_TYPE = cOMM_PLAN_TYPE;
	}

	public String getPRODUCT_TYPE()
	{
		return PRODUCT_TYPE;
	}

	public void setPRODUCT_TYPE(String pRODUCT_TYPE)
	{
		PRODUCT_TYPE = pRODUCT_TYPE;
	}

	public String getCUSTOMER_SEGMENT()
	{
		return CUSTOMER_SEGMENT;
	}

	public void setCUSTOMER_SEGMENT(String cUSTOMER_SEGMENT)
	{
		CUSTOMER_SEGMENT = cUSTOMER_SEGMENT;
	}

	public String getICE_CUBE_ELIGIBILITY()
	{
		return ICE_CUBE_ELIGIBILITY;
	}

	public void setICE_CUBE_ELIGIBILITY(String iCE_CUBE_ELIGIBILITY)
	{
		ICE_CUBE_ELIGIBILITY = iCE_CUBE_ELIGIBILITY;
	}

	public int getNO_OF_ACTIVE_SIMS()
	{
		return NO_OF_ACTIVE_SIMS;
	}

	public void setNO_OF_ACTIVE_SIMS(int nO_OF_ACTIVE_SIMS)
	{
		NO_OF_ACTIVE_SIMS = nO_OF_ACTIVE_SIMS;
	}

	public Date getLAST_UPDATE_DATE()
	{
		return LAST_UPDATE_DATE;
	}

	public void setLAST_UPDATE_DATE(Date lAST_UPDATE_DATE)
	{
		LAST_UPDATE_DATE = lAST_UPDATE_DATE;
	}

	public String getMODE_PACKAGE_NAME_EN()
	{
		return MODE_PACKAGE_NAME_EN;
	}

	public void setMODE_PACKAGE_NAME_EN(String mODE_PACKAGE_NAME_EN)
	{
		MODE_PACKAGE_NAME_EN = mODE_PACKAGE_NAME_EN;
	}

	public String getMODE_PACKAGE_NAME_AR()
	{
		return MODE_PACKAGE_NAME_AR;
	}

	public void setMODE_PACKAGE_NAME_AR(String mODE_PACKAGE_NAME_AR)
	{
		MODE_PACKAGE_NAME_AR = mODE_PACKAGE_NAME_AR;
	}

	public String getREMAINING_MONTHS()
	{
		return REMAINING_MONTHS;
	}

	public void setREMAINING_MONTHS(String rEMAINING_MONTHS)
	{
		REMAINING_MONTHS = rEMAINING_MONTHS;
	}

	//Added for B2B Drop2 project
	public int getUSER_ID()
	{
		return USER_ID;
	}

	public void setUSER_ID(int uSER_ID)
	{
		USER_ID = uSER_ID;
	}

	public String getUSER_NAME_AR()
	{
		return USER_NAME_AR;
	}

	public void setUSER_NAME_AR(String uSER_NAME_AR)
	{
		USER_NAME_AR = uSER_NAME_AR;
	}

	public String getUSER_NAME_EN()
	{
		return USER_NAME_EN;
	}

	public void setUSER_NAME_EN(String uSER_NAME_EN)
	{
		USER_NAME_EN = uSER_NAME_EN;
	}

	public String getUSER_DOC_TYPE()
	{
		return USER_DOC_TYPE;
	}

	public void setUSER_DOC_TYPE(String uSER_DOC_TYPE)
	{
		USER_DOC_TYPE = uSER_DOC_TYPE;
	}

	public String getUSER_DOC_ID()
	{
		return USER_DOC_ID;
	}

	public void setUSER_DOC_ID(String uSER_DOC_ID)
	{
		USER_DOC_ID = uSER_DOC_ID;
	}

	public Date getID_EXPIRY_DATE()
	{
		return ID_EXPIRY_DATE;
	}

	public void setID_EXPIRY_DATE(Date iD_EXPIRY_DATE)
	{
		ID_EXPIRY_DATE = iD_EXPIRY_DATE;
	}

	public String getFINGER_PRINT_STATUS()
	{
		return FINGER_PRINT_STATUS;
	}

	public void setFINGER_PRINT_STATUS(String fINGER_PRINT_STATUS)
	{
		FINGER_PRINT_STATUS = fINGER_PRINT_STATUS;
	}

	public Date getFINGER_PRINT_DATE()
	{
		return FINGER_PRINT_DATE;
	}

	public void setFINGER_PRINT_DATE(Date fINGER_PRINT_DATE)
	{
		FINGER_PRINT_DATE = fINGER_PRINT_DATE;
	}

	public String getIMSI()
	{
		return IMSI;
	}

	public void setIMSI(String iMSI)
	{
		IMSI = iMSI;
	}

	public String getPIN()
	{
		return PIN;
	}

	public void setPIN(String pIN)
	{
		PIN = pIN;
	}

	public String getPIN2()
	{
		return PIN2;
	}

	public void setPIN2(String pIN2)
	{
		PIN2 = pIN2;
	}

	public String getPUK()
	{
		return PUK;
	}

	public void setPUK(String pUK)
	{
		PUK = pUK;
	}

	public String getPUK2()
	{
		return PUK2;
	}

	public void setPUK2(String pUK2)
	{
		PUK2 = pUK2;
	}
	
	
}
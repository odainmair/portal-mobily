package sa.com.mobily.eportal.corporate.model;

import java.sql.Timestamp;
import java.util.Date;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class AccountHierarchyApprovals extends BaseVO{
	
	private static final long serialVersionUID = 1L;
	private int ID;
	private String ACCOUNT_HIERARCHY_ID;
	private Timestamp USER_DATA_DATE;
	private Timestamp SIM_DATA_DATE;
	private String EPORTAL_USER_NAME;
	private String NEW_USER_NAME;
	private int NEW_USER_DOC_TYPE;
	private String NEW_USER_DOC_ID;
	private Date NEW_ID_EXPORY_DATE;
	private String SIM_SAWP_APPROVED;
	
	public int getID()
	{
		return ID;
	}
	public void setID(int iD)
	{
		ID = iD;
	}
	public String getACCOUNT_HIERARCHY_ID()
	{
		return ACCOUNT_HIERARCHY_ID;
	}
	public void setACCOUNT_HIERARCHY_ID(String aCCOUNT_HIERARCHY_ID)
	{
		ACCOUNT_HIERARCHY_ID = aCCOUNT_HIERARCHY_ID;
	}
	public Timestamp getUSER_DATA_DATE()
	{
		return USER_DATA_DATE;
	}
	public void setUSER_DATA_DATE(Timestamp uSER_DATA_DATE)
	{
		USER_DATA_DATE = uSER_DATA_DATE;
	}
	public Timestamp getSIM_DATA_DATE()
	{
		return SIM_DATA_DATE;
	}
	public void setSIM_DATA_DATE(Timestamp sIM_DATA_DATE)
	{
		SIM_DATA_DATE = sIM_DATA_DATE;
	}
	public String getEPORTAL_USER_NAME()
	{
		return EPORTAL_USER_NAME;
	}
	public void setEPORTAL_USER_NAME(String ePORTAL_USER_NAME)
	{
		EPORTAL_USER_NAME = ePORTAL_USER_NAME;
	}
	public String getNEW_USER_NAME()
	{
		return NEW_USER_NAME;
	}
	public void setNEW_USER_NAME(String nEW_USER_NAME)
	{
		NEW_USER_NAME = nEW_USER_NAME;
	}
	public int getNEW_USER_DOC_TYPE()
	{
		return NEW_USER_DOC_TYPE;
	}
	public void setNEW_USER_DOC_TYPE(int nEW_USER_DOC_TYPE)
	{
		NEW_USER_DOC_TYPE = nEW_USER_DOC_TYPE;
	}
	public String getNEW_USER_DOC_ID()
	{
		return NEW_USER_DOC_ID;
	}
	public void setNEW_USER_DOC_ID(String nEW_USER_DOC_ID)
	{
		NEW_USER_DOC_ID = nEW_USER_DOC_ID;
	}
	public Date getNEW_ID_EXPORY_DATE()
	{
		return NEW_ID_EXPORY_DATE;
	}
	public void setNEW_ID_EXPORY_DATE(Date nEW_ID_EXPORY_DATE)
	{
		NEW_ID_EXPORY_DATE = nEW_ID_EXPORY_DATE;
	}
	public String getSIM_SAWP_APPROVED()
	{
		return SIM_SAWP_APPROVED;
	}
	public void setSIM_SAWP_APPROVED(String sIM_SAWP_APPROVED)
	{
		SIM_SAWP_APPROVED = sIM_SAWP_APPROVED;
	}
}

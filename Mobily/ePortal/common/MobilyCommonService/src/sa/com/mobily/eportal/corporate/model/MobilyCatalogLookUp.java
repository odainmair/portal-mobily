package sa.com.mobily.eportal.corporate.model;

import java.sql.Date;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class MobilyCatalogLookUp extends BaseVO
{
	private static final long serialVersionUID = -4777330515693105523L;

	private String TYPE;

	private String CODE;

	private String SHORT_NAME;

	private String ENGLISH_NAME;

	private String ARABIC_NAME;

	private int PARENT_ID;

	private Date UPDATE_DATE;

	public String getTYPE()
	{
		return TYPE;
	}

	public void setTYPE(String tYPE)
	{
		TYPE = tYPE;
	}

	public String getCODE()
	{
		return CODE;
	}

	public void setCODE(String cODE)
	{
		CODE = cODE;
	}

	public String getSHORT_NAME()
	{
		return SHORT_NAME;
	}

	public void setSHORT_NAME(String sHORT_NAME)
	{
		SHORT_NAME = sHORT_NAME;
	}

	public String getENGLISH_NAME()
	{
		return ENGLISH_NAME;
	}

	public void setENGLISH_NAME(String eNGLISH_NAME)
	{
		ENGLISH_NAME = eNGLISH_NAME;
	}

	public String getARABIC_NAME()
	{
		return ARABIC_NAME;
	}

	public void setARABIC_NAME(String aRABIC_NAME)
	{
		ARABIC_NAME = aRABIC_NAME;
	}

	public int getPARENT_ID()
	{
		return PARENT_ID;
	}

	public void setPARENT_ID(int pARENT_ID)
	{
		PARENT_ID = pARENT_ID;
	}

	public Date getUPDATE_DATE()
	{
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(Date uPDATE_DATE)
	{
		UPDATE_DATE = uPDATE_DATE;
	}
}
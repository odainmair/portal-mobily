/**
 * 
 */
package sa.com.mobily.eportal.corporate.model;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author Mohamed Shalaby
 *
 */
public class PersonalizationVo extends BaseVO
{
	/**
	 * The default serial version UID
	 */
	private static final long serialVersionUID = 1L;
	
	// Quantify the number of Ethernet sub-accounts under specific Corporate Account
	private int ethernet;
	
	// Quantify the number of DIA sub-accounts under specific Corporate Account
	private int dia;
	
	// Quantify the number of IPVPN sub-accounts under specific Corporate Account
	private int ipVpn;
	
	private String iceCubeEligible;


	public String getIceCubeEligible()
	{
		return iceCubeEligible;
	}

	public void setIceCubeEligible(String iceCubeEligible)
	{
		this.iceCubeEligible = iceCubeEligible;
	}

	public int getEthernet()
	{
		return ethernet;
	}

	public void setEthernet(int ethernet)
	{
		this.ethernet = ethernet;
	}

	public int getDia()
	{
		return dia;
	}

	public void setDia(int dia)
	{
		this.dia = dia;
	}

	public int getIpVpn()
	{
		return ipVpn;
	}

	public void setIpVpn(int ipVpn)
	{
		this.ipVpn = ipVpn;
	}

}

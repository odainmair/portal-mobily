package sa.com.mobily.eportal.customerAccDetails.constants;

public interface CustomerAccDetailsConstants
{
	public static final String FUNC_ID="WS_GATEWAY";
	public static final String MSG_VERSION="1.0";
	public static final String NS1="NS1";
	public static final String REQ_QUEUE_CUSTOMER_ACC_DETAILS = "customer.acc.details.request.queue";
	public static final String REP_QUEUE_CUSTOMER_ACC_DETAILS = "customer.acc.details.reply.queue";
	public static final String REQUESTOR_CHANNEL_ID="ePortal";
	public static final String WS_NAME="RelatedAccountInquiry";
	
	public static final String REQUESTOR_SECURITY_INFO="Secure";
	/*public static final String ="";
	public static final String ="";
	public static final String ="";
	public static final String ="";
	public static final String ="";
	public static final String ="";
	public static final String ="";
	public static final String ="";
	public static final String ="";
	public static final String ="";
	public static final String ="";
	public static final String ="";*/
}

package sa.com.mobily.eportal.customerAccDetails.jms;

import java.util.logging.Level;

import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MQUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.customerAccDetails.constants.CustomerAccDetailsConstants;
import sa.com.mobily.eportal.customerAccDetails.vo.CustomerAccDetailsRequestVO;
import sa.com.mobily.eportal.customerAccDetails.vo.CustomerAccDetailsVO;
import sa.com.mobily.eportal.customerAccDetails.xml.CustomerAccDetailsXMLHandler;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.customerinfo.jms.CustomerInfoJMSRepository;
import sa.com.mobily.eportal.customerinfo.util.AppConfig;

public class CustomerAccDetailsJMSRepository implements CustomerAccDetailsConstants
{
	public static final String CUSTOMER_ACC_DETAILS_LOGGER_NAME ="MobilyCommonService/customerAccDetails";
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CUSTOMER_ACC_DETAILS_LOGGER_NAME);
	private static final String className = CustomerInfoJMSRepository.class.getName();
	
	// to get the account details for Online SIM
	public CustomerAccDetailsVO getCustomerDetails(CustomerAccDetailsRequestVO requestVO)
	{
		String method = "getCustomerDetails";
		executionContext.audit(Level.INFO, "start for id number[" + requestVO.getIdNumber() + "]", className, method);

		CustomerAccDetailsVO replyVO = new CustomerAccDetailsVO();
		boolean isTesting = false;
		String strXMLReply = "";
		MQAuditVO mqAuditVO = new MQAuditVO();
		
		String requestQueueName = AppConfig.getInstance().get(REQ_QUEUE_CUSTOMER_ACC_DETAILS);
		String replyQueueName = AppConfig.getInstance().get(REP_QUEUE_CUSTOMER_ACC_DETAILS);
		
		mqAuditVO.setRequestQueue(requestQueueName);
		mqAuditVO.setReplyQueue(replyQueueName);
		mqAuditVO.setMsisdn("");
		mqAuditVO.setUserName("Anonymous");
		mqAuditVO.setFunctionName(FUNC_ID);
		executionContext.audit(Level.INFO, "requestQueueName[" + requestQueueName + "]", className, method);
		executionContext.audit(Level.INFO, "replyQueueName[" + replyQueueName + "]", className, method);

		// Populate Header
		
		
		String strXMLRequest = new CustomerAccDetailsXMLHandler().generateCustomerAccDetailsXML(requestVO);
		System.out.println(strXMLRequest);
		executionContext.audit(Level.INFO, "strXMLRequest: " + strXMLRequest, className, method);
		mqAuditVO.setMessage(strXMLRequest);
		executionContext.audit(Level.INFO, "isTesting: " + isTesting, className, method);
		try {
			if(isTesting){
				strXMLReply ="<MOBILY_BSL_SR_REPLY><SR_HEADER_REPLY><FuncId>WS_GATEWAY</FuncId><MsgVersion>1.0</MsgVersion><RequestorChannelId>ePortal</RequestorChannelId><RequestorChannelFunction>WS_GATEWAY</RequestorChannelFunction>"+        
						    "<ChannelTransactionId>SR_20190301100640</ChannelTransactionId><SrDate>20190301100640</SrDate><ErrorCode>0000</ErrorCode><ErrorMsg>Success</ErrorMsg></SR_HEADER_REPLY><WSGatewayResponse><ns:GetRelatedAccounts_Output xmlns:ns=\"http://siebel.com/CustomUI\"><ns:Error_spcCode/><ns:Error_spcMessage/>"+
							"<ListOfEemblCustomerLightIo xmlns=\"http://www.siebel.com/xml/EEMBL%20Customer%20Light%20IO\"><Account><BorderNumber/><ExceptionFlag>0</ExceptionFlag>"+       
							"<FingerprintStatus>N</FingerprintStatus><AccountNumber>1001231968385561</AccountNumber><EECCBlackList>N</EECCBlackList><EECCCustomerCategory>Individual</EECCCustomerCategory><EECCCustomerType>Consumer</EECCCustomerType>"+                    
							"<EECCEALanguagePreference>English</EECCEALanguagePreference><EECCIDDocExpDate>04/30/2019 00:00:00</EECCIDDocExpDate><EECCIDDocExpHDate>24/08/1440</EECCIDDocExpHDate><EECCIDDocIssueDate>02/01/2019 00:00:00</EECCIDDocIssueDate><EECCIDDocIssueHDate>25/05/1440</EECCIDDocIssueHDate>"+                    
							"<EECCIDDocIssuePlace/><EECCIDDocType>GCC ID</EECCIDDocType><EECCIDNumber>201922021242</EECCIDNumber><EEMBLAlelmLastSyncStatus/><AlelmOnlineOverallStatus>Not Applicable</AlelmOnlineOverallStatus><UpdatedByAlElm>N</UpdatedByAlElm><HouseholdHeadOccupation>Professional</HouseholdHeadOccupation>"+                    
							"<IntegrationId>5022251288</IntegrationId><Location>GCC ID201922021242</Location><Name>QA REGRESSION</Name><PrimaryAddressId>1-M8GM</PrimaryAddressId><PrimaryContactId>1-2YKBZJTQ</PrimaryContactId><PrimaryOrganization>IT Test Organization, IT Test Organization</PrimaryOrganization>"+                    
							"<ListOfCutServiceSubAccounts><CutServiceSubAccounts><Created>02/22/2019 10:26:41</Created><BARStatus/><AccountNumber>1001231968385807</AccountNumber><AccountStatus>Active</AccountStatus><EmailAddress>qareg@ymail.com</EmailAddress><EECCBlackList>N</EECCBlackList><EECCCompanyName2/>"+                            
							"<EECCCustomerAccountName>QA REGRESSION</EECCCustomerAccountName><EECCCustomerCategory>Individual</EECCCustomerCategory><EECCCustomerType>Consumer</EECCCustomerType><EECCFullName>QA REGRESSION</EECCFullName><EECCIDDocType2>GCC ID</EECCIDDocType2><EECCIDNumber>201922021242</EECCIDNumber>"+                            
							"<EECCMSISDN/><EECCMSISDNVanityStatus/><EECCPricingPlan>Postpaid Plan</EECCPricingPlan><RegisteredDate>02/22/2019</RegisteredDate><EEMBLAlelmLastSyncStatus/><EEMBLCPEDeviceName>Bayanat - HU_FO_R_HG8245</EEMBLCPEDeviceName><EEMBLCPEDigitalCertificateNumber>QAELIFE2017122376</EEMBLCPEDigitalCertificateNumber>"+                            
							"<EEMBLCPEExpiryDate>04/08/2019</EEMBLCPEExpiryDate><EEMBLCPENumber>QAELIFE2017122376</EEMBLCPENumber><EEMBLCommercialID/><EEMBLCustomerSegment/><EEMBLRootProductLine>FTTH</EEMBLRootProductLine><RootProductCategory>FTTH</RootProductCategory><EEMBLPackageId>3434</EEMBLPackageId><EEMBLProductName>eLife Connect Postpaid</EEMBLProductName>"+                            
							"<EEMBLPromotionName/><PackageName>eLife Connect Postpaid</PackageName><EEMBLUpdatedByAlElm>N</EEMBLUpdatedByAlElm><Location>1-2YKBZK0F</Location><MasterAccountId>1-2YKBZJTL</MasterAccountId><MasterAccountNumber>1001231968385561</MasterAccountNumber><ParentAccountId>1-2YKBZK00</ParentAccountId><ParentAccountNumber>1001231968385792</ParentAccountNumber>"+
							"<PartyName>QA REGRESSION</PartyName><PartyTypeCode>Organization</PartyTypeCode><PartyUId2>1-2YKBZK0F</PartyUId2><PrimaryAddressId>1-M8GM</PrimaryAddressId><PrimaryContactId>1-2YKBZJTQ</PrimaryContactId><Type>Consumer</Type></CutServiceSubAccounts><CutServiceSubAccounts><Created>02/22/2019 10:37:54</Created><BARStatus/><AccountNumber>1001231968522657</AccountNumber>"+                            
							"<AccountStatus>Active</AccountStatus><EmailAddress>qareg@ymail.com</EmailAddress><EECCBlackList>N</EECCBlackList><EECCCompanyName2/><EECCCustomerAccountName>QA REGRESSION</EECCCustomerAccountName><EECCCustomerCategory>Individual</EECCCustomerCategory><EECCCustomerType>Consumer</EECCCustomerType><EECCFullName>QA REGRESSION</EECCFullName>"+                            
							"<EECCIDDocType2>GCC ID</EECCIDDocType2><EECCIDNumber>201922021242</EECCIDNumber><EECCMSISDN/><EECCMSISDNVanityStatus/><EECCPricingPlan>Prepaid Plan</EECCPricingPlan><RegisteredDate>02/22/2019</RegisteredDate>"+                            
							"<EEMBLAlelmLastSyncStatus/><EEMBLCPEDeviceName>Bayanat - HU_FO_R_HG8245</EEMBLCPEDeviceName><EEMBLCPEDigitalCertificateNumber>QAELIFE2017122377</EEMBLCPEDigitalCertificateNumber><EEMBLCPEExpiryDate>05/22/2019</EEMBLCPEExpiryDate><EEMBLCPENumber>QAELIFE2017122377</EEMBLCPENumber><EEMBLCommercialID/><EEMBLCustomerSegment/><EEMBLRootProductLine>FTTH</EEMBLRootProductLine>"+                            
							"<RootProductCategory>FTTH</RootProductCategory><EEMBLPackageId>6000</EEMBLPackageId><EEMBLProductName>eLife Connect Prepaid</EEMBLProductName><EEMBLPromotionName/><PackageName>eLife Connect Prepaid</PackageName><EEMBLUpdatedByAlElm>N</EEMBLUpdatedByAlElm><Location>1-2YKC2HLT</Location><MasterAccountId>1-2YKBZJTL</MasterAccountId>"+                            
							"<MasterAccountNumber>1001231968385561</MasterAccountNumber><ParentAccountId>1-2YKC2HLE</ParentAccountId><ParentAccountNumber>1001231968522642</ParentAccountNumber><PartyName>QA REGRESSION</PartyName><PartyTypeCode>Organization</PartyTypeCode><PartyUId2>1-2YKC2HLT</PartyUId2><PrimaryAddressId>1-M8GM</PrimaryAddressId><PrimaryContactId>1-2YKBZJTQ</PrimaryContactId>"+                            
							"<Type>Consumer</Type></CutServiceSubAccounts><CutServiceSubAccounts><Created>12/10/2018 17:38:22</Created><BARStatus/><AccountNumber>1001231247800956</AccountNumber><AccountStatus>Active</AccountStatus><EmailAddress>mobilytest@mobily.com.sa</EmailAddress><EECCBlackList>N</EECCBlackList><EECCCompanyName2/><EECCCustomerAccountName>SIMULATORFIRST SIMULATORFATHER SIMULATORFAMILY</EECCCustomerAccountName>"+                            
							"<EECCCustomerCategory>Individual</EECCCustomerCategory><EECCCustomerType>Consumer</EECCCustomerType><EECCFullName>SIMULATORFIRST SIMULATORFATHER SIMULATORFAMILY</EECCFullName><EECCIDDocType2>GCC ID</EECCIDDocType2><EECCIDNumber>2018010102</EECCIDNumber><EECCMSISDN>966831022410050</EECCMSISDN><EECCMSISDNVanityStatus>None</EECCMSISDNVanityStatus><EECCPricingPlan>Prepaid Plan</EECCPricingPlan><RegisteredDate>12/10/2018</RegisteredDate><EEMBLAlelmLastSyncStatus/><EEMBLCPEDeviceName/>"+                            
							"<EEMBLCPEDigitalCertificateNumber/><EEMBLCPEExpiryDate/><EEMBLCPENumber/><EEMBLCommercialID/><EEMBLCustomerSegment/><EEMBLRootProductLine>Connect</EEMBLRootProductLine><RootProductCategory>Connect</RootProductCategory><EEMBLPackageId>2080</EEMBLPackageId><EEMBLProductName>Connect 100GB 3M_SR 335</EEMBLProductName><EEMBLPromotionName/><PackageName>Connect 100GB 3M_SR 335</PackageName><EEMBLUpdatedByAlElm>N</EEMBLUpdatedByAlElm>"+                            
							"<Location>1-2Y8EYX6K</Location><MasterAccountId>1-2Y6COKGR</MasterAccountId><MasterAccountNumber>1001231123026331</MasterAccountNumber><ParentAccountId>1-2Y8EYX67</ParentAccountId><ParentAccountNumber>1001231247800943</ParentAccountNumber><PartyName>SIMULATORFIRST SIMULATORFATHER SIMULATORFAMILY</PartyName><PartyTypeCode>Organization</PartyTypeCode><PartyUId2>1-2Y8EYX6K</PartyUId2><PrimaryAddressId>1-2Y6COKGU</PrimaryAddressId>"+                            
							"<PrimaryContactId>1-2Y6COKGW</PrimaryContactId><Type>Consumer</Type></CutServiceSubAccounts><CutServiceSubAccounts><Created>03/18/2018 21:34:49</Created><BARStatus>1</BARStatus><AccountNumber>1001227067637476</AccountNumber><AccountStatus>Active</AccountStatus><EmailAddress>a.a.odhah@gmail.com</EmailAddress><EECCBlackList>N</EECCBlackList><EECCCompanyName2/><EECCCustomerAccountName>????????? ??????? ????</EECCCustomerAccountName>"+                            
							"<EECCCustomerCategory>Individual</EECCCustomerCategory><EECCCustomerType>Consumer</EECCCustomerType><EECCFullName>????????? ??????? ????</EECCFullName><EECCIDDocType2>IQAMA</EECCIDDocType2><EECCIDNumber>2448246484</EECCIDNumber><EECCMSISDN>966560632554</EECCMSISDN><EECCMSISDNVanityStatus>None</EECCMSISDNVanityStatus><EECCPricingPlan>Postpaid Plan</EECCPricingPlan><RegisteredDate>03/19/2018</RegisteredDate><EEMBLAlelmLastSyncStatus/>"+
							"<EEMBLCPEDeviceName/><EEMBLCPEDigitalCertificateNumber/><EEMBLCPEExpiryDate/><EEMBLCPENumber/><EEMBLCommercialID/><EEMBLCustomerSegment>MID</EEMBLCustomerSegment><EEMBLRootProductLine>Post-Paid</EEMBLRootProductLine><RootProductCategory>Voice</RootProductCategory><EEMBLPackageId>2052</EEMBLPackageId><EEMBLProductName>200 Plus</EEMBLProductName>"+                            
							"<EEMBLPromotionName/><PackageName>200 Plus</PackageName><EEMBLUpdatedByAlElm>N</EEMBLUpdatedByAlElm><Location>1-2WBA7IEC</Location><MasterAccountId>1-2WB9ZPR0</MasterAccountId><MasterAccountNumber>1001227067273756</MasterAccountNumber><ParentAccountId>1-2WBA7IDX</ParentAccountId>"+
							"<ParentAccountNumber>1001227067637461</ParentAccountNumber><PartyName>????????? ??????? ????</PartyName><PartyTypeCode>Organization</PartyTypeCode><PartyUId2>1-2WBA7IEC</PartyUId2><PrimaryAddressId>1-A8RW2JQ</PrimaryAddressId>"+
							"<PrimaryContactId>1-2WB9ZPR4</PrimaryContactId><Type>Consumer</Type></CutServiceSubAccounts></ListOfCutServiceSubAccounts><ListOfAddress><Address><Id>1-2YKBZJTO</Id><IsPrimaryMVG>N</IsPrimaryMVG><StreetAddress>N/A</StreetAddress><Country>Saudi Arabia</Country><City>Riyadh</City><EECCPOBox/><EECCPostalCode>00000</EECCPostalCode>"+                        
							"</Address><Address><Id>1-M8GM</Id><IsPrimaryMVG>Y</IsPrimaryMVG><StreetAddress>PA19022722562252704634</StreetAddress><Country>Saudi Arabia</Country><City>??????</City><EECCPOBox>15455</EECCPOBox><EECCPostalCode>12345</EECCPostalCode></Address>"+                    
							"</ListOfAddress><ListOfContact><Contact><Id>1-2YKBZJTQ</Id><IsPrimaryMVG>Y</IsPrimaryMVG><MM>Mrs.</MM><MF>F</MF><FirstName>QA</FirstName><MiddleName/><LastName>REGRESSION</LastName><EECCNationality>United Arab Emirates</EECCNationality><EECCDOBFormat>MELADI</EECCDOBFormat><EECCBirthDateHijri>26/07/1410</EECCBirthDateHijri>"+                            
							"<BirthDate>02/22/1990</BirthDate><ContactPreference>Email</ContactPreference><CellularPhone>+966547106282</CellularPhone><WorkPhone2/></Contact></ListOfContact></Account></ListOfEemblCustomerLightIo></ns:GetRelatedAccounts_Output></WSGatewayResponse></MOBILY_BSL_SR_REPLY>";
			}else{
				 strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
			}
			
			executionContext.audit(Level.INFO, "strXMLReply: " + strXMLReply, className, method);
			mqAuditVO.setReply(strXMLReply);
			if (!FormatterUtility.isEmpty(strXMLReply)) {
				
				replyVO = new CustomerAccDetailsXMLHandler().parseCustomerAccDetailsXML(strXMLReply);
				executionContext.audit(Level.INFO, "CustomerAccDetailsJMSRepository >> getCustomerDetails>>MQ Audit Setting error code & error messages: " + strXMLReply, className, method);
				if(replyVO != null && FormatterUtility.isNotEmpty(replyVO.getErrorSpecCode())){
					
					mqAuditVO.setErrorMessage(FormatterUtility.checkNull(replyVO.getErrorSpecMessage()));
					mqAuditVO.setServiceError(FormatterUtility.checkNull(replyVO.getErrorSpecCode()));
				}
			} else {
				mqAuditVO.setErrorMessage(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
				mqAuditVO.setServiceError(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
			}
		} catch(ServiceException e) {
			executionContext.log(Level.SEVERE, "ServiceException while parsing xml reply::" + e.getMessage(), className, method, e);
			throw e;
		} catch(Exception e) {
			executionContext.log(Level.SEVERE, "Exception in MQ Call::" + e.getMessage(), className, method, e);
			mqAuditVO.setErrorMessage(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
			mqAuditVO.setServiceError(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
		} finally {
			MQUtility.saveMQAudit(mqAuditVO);
		}
		return replyVO;
	}
	
	/*public static void main(String[] args){
		CustomerAccDetailsRequestVO requestVO = new CustomerAccDetailsRequestVO();
		requestVO.setIdNumber("12345");
		requestVO.setIdType("GCCID");
		String idType="GCCID";
		String idValue="12345";
		System.out.println("123");
		CustomerAccDetailsReplyVO replyVO = getCustomerDetails(requestVO);
	}*/
}

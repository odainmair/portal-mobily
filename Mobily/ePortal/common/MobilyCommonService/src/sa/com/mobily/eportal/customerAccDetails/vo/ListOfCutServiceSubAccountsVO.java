/**
 * 
 */
package sa.com.mobily.eportal.customerAccDetails.vo;

import java.util.List;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author 80057930
 *
 */
public class ListOfCutServiceSubAccountsVO extends BaseVO
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<CutServiceSubAccountsVO> cutServiceSubAccounts;
	
	/**
	 * @return the cutServiceSubAccounts
	 */
	public List<CutServiceSubAccountsVO> getCutServiceSubAccounts()
	{
		return cutServiceSubAccounts;
	}
	/**
	 * @param cutServiceSubAccounts the cutServiceSubAccounts to set
	 */
	public void setCutServiceSubAccounts(List<CutServiceSubAccountsVO> cutServiceSubAccounts)
	{
		this.cutServiceSubAccounts = cutServiceSubAccounts;
	}
	
	
}


package sa.com.mobily.eportal.customerinfo.constant;


public interface ConstantsIfc {

	public static final String DATE_FORMAT = "yyyyMMddHHmmss";
	
	public static final int FUNC_ID_CUSTOMER_PRODUCT_INFORMATION = 40200200;
	public static final int FUNC_ID_CUSTOMER_PRODUCT_INFORMATION_ACCT_NUMBER_INQUIRY = 40200500;
	public static final String CALLER_SERVICE_SUBMIT_PRODUCT_ORDER = "SubmitProductOrder";
	public static final int REQ_MODE_0 = 0;
	public static final String INVALID_REQUEST ="20131";
	public static final String INVALID_REPLY ="20132";
	
	public static final String REQUEST_QUEUE_CUSTOMER_PROFILE = "customer.profile.request.queue";
	public static final String REPLY_QUEUE_CUSTOMER_PROFILE = "customer.profile.reply.queue";
	
	public static final String REQUEST_QUEUE_CONSUMER_PROFILE = "consumer.profile.request.queue";
	public static final String REPLY_QUEUE_CONSUMER_PROFILE = "consumer.profile.reply.queue";
	
	public static final String REQUEST_QUEUE_RELATED_NUMBERS = "related.numbers.request.queue";
	public static final String REPLY_QUEUE_RELATED_NUMBERS = "related.numbers.reply.queue";
	
	public static final String REQUEST_QUEUE_ACCOUNT_POID = "account.poid.request.queue";
	public static final String REPLY_QUEUE_ACCOUNT_POID = "account.poid.reply.queue";
	
	public static final String REQUEST_QUEUE_FAV_NO_ELIGIBILITY = "fav.number.eligibility.request.queue";
	public static final String REPLY_QUEUE_FAV_NO_ELIGIBILITY = "fav.number.eligibility.reply.queue";
	
	public static final String REQUEST_QUEUE_CORPORATE_PROFILE = "corporate.profile.request.queue";
	public static final String REPLY_QUEUE_CORPORATE_PROFILE = "corporate.profile.reply.queue";
	
	public static final String TEMP_DIR = "temp.dir";
	
	public static final String ERROR_CODE_EMPTY_REPLY_XML = "9999";
	
	public static final String STATUS_CODE_CUSTOMER_NOT_FOUND = "E500004";
	public static final String STATUS_CODE_CUSTOMER_NOT_SUBSCRIBED = "E020061";
	
	public static final String FUN_GET_CONSUMER_LINE_INFO = "CustandLineInfoInq";
	public static final String FUN_GET_RELATED_NUMBERS_INQ = "RelatedAccountsInq";
	public static final String FUN_GET_ACCOUNT_POID_INQ = "AccountPOIDInq";
	/*public static final String FUN_GET_LAST_BILLS_INQ = "LastBillsInq";*/
	
	public static final String KEY_TYPE_SA = "SA"; // For Service Account Number
	public static final String KEY_TYPE_MSISDN = "MSISDN"; // For Msisdn
	public static final String KEY_TYPE_CONSUMER_CPE = "CPE"; //For CPE number
	
	public static final String EPORTAL_USER = "ePortalUser";
	public static final String STATUS_CODE_SUCCESS = "I000000";
	
	public static final String FAV_NUMBER_MSG_TYPE = "REQUEST";
	public static final String FAV_NUMBER_FUNC_ID = "44400000";
	public static final String LANG_E = "E";
	public static final String FAV_NUMBER_SC_ID = "Broker";
	public static final String FAV_NUMBER_CUST_ID = "EAI";
	public static final String ACTION_ADD = "Add";
	
	public static final String CORP_PROFILE_FUNC_ID = "CorporateProfileInquiry";
	public static final String CORP_PROFILE_MSG_VERSION = "0002";
	public static final String CORP_PROFILE_MSG_VERSION_0001 = "0001";
	public static final String CORP_PROFILE_MSG_OPERATION_DOCID = "DOCID";
	public static final String CORP_PROFILE_MSG_OPERATION_PROFILE = "PROFILE";
	
	public static final String KEY_TYPE_BA = "BA"; // For billing account number
	
	//ID valication prompt
		public static final String ID_VALIDATE_MESSAGE_FORMAT_VALUE = "ValidationPromptInquiry";
		public static final String QUEUE_REQUEST_ID_VALIDATION_PROMPT_INQUIRY = "mq.validation.prompt.inq.request.queue";
		public static final String QUEUE_REPLY_ID_VALIDATION_PROMPT_INQUIRY = "mq.validation.prompt.inq.reply.queue";

		public static final String ID_VALIDATION_PROMPT_Y = "Y";
		public static final String ID_VALIDATION_PROMPT_N = "N";
		public static final String MSG_VER_0 = "0";
		public static final String REQUESTOR_CHANNEL_ePortal = "ePortal";
		public static final String SECURITY_INFO_VALUE = "secure";
		
		
	public static final String MQ_AUDIT_REQUIRED = "mq.audit.required";
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	
	//LP
	public static final String AllContractsWithProduct = "AllContractsWithProduct";
	public static final String PartyInquiryLevel = "PartyInquiryLevel";
	public static final String ContractInquiryLevel = "ContractInquiryLevel";

	//Customer Profile enhancement constants - Added by Ravi on 14th Feb 2018
	public static final String ACTIVE_ACCOUNT_STATUS = "2";
	public static final String INACTIVE_ACCOUNT_STATUS ="1";
	
	public static final String CORPORATE_PACKAGE_NO="No";
	public static final String CORPORATE_PACKAGE_YES="Yes";
	
	public static final String PRODUCT_TYPE_GSM="1";
	public static final String PRODUCT_TYPE_WIMAX="2";
	public static final String PRODUCT_TYPE_FTTH="3";
	public static final String PRODUCT_TYPE_CONNECT="4";
	public static final String PRODUCT_TYPE_LTE="5";
	
	public static final String MQ_AUDIT_TYPE = "mq.audit.type";
	public static final String MQ_AUDIT_TYPE_NO_AUDIT = "0";
	public static final String MQ_AUDIT_TYPE_DB = "1";
	public static final String MQ_AUDIT_TYPE_MDB = "2";
	public static final String InquiryLevel = "InquiryLevel";
	public static final String Status = "Status";
	public static final String Active = "Active";
	public static final String RCPC_CONTACTNUBER_FUNC_ID="40202000";
}

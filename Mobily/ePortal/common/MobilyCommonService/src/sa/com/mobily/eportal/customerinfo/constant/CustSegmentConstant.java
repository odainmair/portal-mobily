package sa.com.mobily.eportal.customerinfo.constant;

public interface CustSegmentConstant {

	public static final String CUST_SEGMENT_CONSUMER = "1";
	public static final String CUST_SEGMENT_BUSINESS = "2";
	
	
}

/**
 * 
 */
package sa.com.mobily.eportal.customerinfo.constant;

/**
 * @author Umer Rasheed UmerRasheed@hotmail.com
 *
 */
public interface CustomerErrorsIfc {

	public static final String SUCCESS_CODE = "I000000";
	
	//GET CUSTOMER PRODUCT INFORMATION: ERROR CODES
	public static final String ERROR_CINFO_CUSTOMER_NOT_FOUND_SIEBEL = "E500004";
	public static final String ERROR_CINFO_CUSTOMER_NOT_SCBSCRIBED = "E020061";
	public static final String ERROR_CINFO_MISSING_INFO = "E200021";
	public static final String ERROR_CINFO_INVALID_MSISDN_FORMAT = "E020043";
	public static final String ERROR_CINFO_NO_FAV_NUMBER_PRESENT = "E950014";
	public static final String ERROR_CINFO_CUSTOMER_NOT_FOUND_MCR = "E510000";
	
	//ePortal error codes
	public static final String ERROR_INVALID_CUSTOMER_ID = "EP2000";
	
}
package sa.com.mobily.eportal.customerinfo.constant;

import java.util.ArrayList;
import java.util.List;

public abstract class LineTypeConstant {
	
	public static final String GSM = "GSM";
	public static final String three3G = "3G";//connect 3g
	public static final String LTE = "LTE"; // 4G	
	public static final String WiMAX = "WiMAX";
	public static final String FTTH = "FTTH";
	public static final String IPTV = "IPTV";
	public static final String FV = "Fixed voice"; //SR22516 - ePortal Fixed Voice
	
	public static final int SUBS_TYPE_GSM = 1;
	public static final int SUBS_TYPE_3G = 2;
	public static final int SUBS_TYPE_LTE = 3;
	public static final int SUBS_TYPE_WiMAX = 4;
	public static final int SUBS_TYPE_FTTH = 5;
	public static final int SUBS_TYPE_IPTV = 6;
	public static final int SUBS_TYPE_FV = 7;//SR22516 - ePortal Fixed Voice
	
	public static final int getLineType(String lineTypeText) {
		int to_return = 0;
         if(GSM.equalsIgnoreCase(lineTypeText))
          to_return =SUBS_TYPE_GSM;
          else
		 if (three3G.equalsIgnoreCase(lineTypeText)) {
			to_return = SUBS_TYPE_3G;
		} else if (LTE.equalsIgnoreCase(lineTypeText)) {
			to_return = SUBS_TYPE_LTE;
		}  
		 else if (FTTH.equalsIgnoreCase(lineTypeText)) {
			to_return = SUBS_TYPE_FTTH;
		}  else if (IPTV.equalsIgnoreCase(lineTypeText)) {
			to_return = SUBS_TYPE_IPTV;
		}
		else if(WiMAX.equalsIgnoreCase(lineTypeText)){
			to_return=SUBS_TYPE_WiMAX;
		}else if(FV.equalsIgnoreCase(lineTypeText)){//SR22516 - ePortal Fixed Voice
			to_return=SUBS_TYPE_FV;
		}
         
		return to_return;
	}

	public static final List<String> getLineTypes() {
		List<String> subList = new ArrayList<String>();
		subList.add(GSM);
		subList.add(three3G);
		subList.add(LTE);
		subList.add(WiMAX);
		subList.add(FTTH);
		subList.add(IPTV);
		subList.add(FV);//SR22516 - ePortal Fixed Voice
		
		return subList;
	}

	public static final String getLineTypeName(int lineType) {
		String to_return = "";
		switch (lineType) {
			case SUBS_TYPE_GSM:
				to_return = GSM;
				break;
			case SUBS_TYPE_3G:
				to_return = three3G;
				break;
			case SUBS_TYPE_LTE:
				to_return = LTE;
				break;			
			case SUBS_TYPE_WiMAX:
				to_return = WiMAX;
				break;
			case SUBS_TYPE_FTTH:
				to_return = FTTH;
				break;
			case SUBS_TYPE_IPTV:
				to_return = IPTV;
				break;			
			case SUBS_TYPE_FV: //SR22516 - ePortal Fixed Voice
				to_return = FV;
				break;	
			default:
				break;
		}
		return to_return;
	}
}

package sa.com.mobily.eportal.customerinfo.constant;

public abstract class RelatedNumberConstant
{
	public static final String PrePaid = "Pre-Paid";

	public static final String PostPaid = "Post-Paid";

	public static final String WiMax = "WiMax";

	public static final String Connect = "Connect";

	public static final String FTTH = "FTTH";

	public static final String LTE = "LTE";
	
	public static final String eLifePostPaidBundle = "eLife Postpaid Bundle";
	
	public static final String IPTVBundle = "IPTV Bundle";
	
	public static final String ELIFE_FIXED_VOICE = "Fixed Voice";
}
package sa.com.mobily.eportal.customerinfo.constant;


public abstract class VIPTierConstant
{
	public static final String GOLD = "GOLD";
	public static final String PLATINUM = "PLATINUM";
	public static final String TITANIUM = "TITANIUM";
}
package sa.com.mobily.eportal.customerinfo.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.collections.CollectionUtils;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.customerinfo.vo.UserProfileVO;

public class UserProfileDAO extends AbstractDBDAO<UserProfileVO> implements ConstantsIfc
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.CUSTOMER_INFO_SERVICE_LOGGER_NAME);
	private static String className = UserProfileDAO.class.getName();

	private static String GET_USER_PROFILE = "SELECT SAN,PRODUCT_TYPE,CUSTOMERTYPE,ACCOUNT_NUMBER,PACKAGE_ID,CREATED_DATE,UPDATED_DATE,ID_TYPE,ID_NUMBER,ACCOUNT_STATUS,PRIM_CONTACT_MOBILE,PACKAGE_CATEGORY,CUSTOMER_CATEGORY,CORPORATE_PACKAGE,PLAN_CATEGORY,PACKAGE_SERVICE_TYPE,EMAIL,CUST_PROFILE_VO FROM EEDBUSR.LP_USER_PROFILE_TBL WHERE ACCOUNT_NUMBER =? AND ACCOUNT_STATUS = ?";
	
	private static String SAVE_USER_PROFILE_DATA = "INSERT INTO EEDBUSR.LP_USER_PROFILE_TBL(SAN, PRODUCT_TYPE, CUSTOMERTYPE, ACCOUNT_NUMBER, PACKAGE_ID, CREATED_DATE,UPDATED_DATE, ID_TYPE, ID_NUMBER,PRIM_CONTACT_MOBILE,PACKAGE_CATEGORY,CUSTOMER_CATEGORY,CORPORATE_PACKAGE,PLAN_CATEGORY,PACKAGE_SERVICE_TYPE,EMAIL,CUST_PROFILE_VO) VALUES (?, ? ,?, ?, ?, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP , ?, ?,?,?,?,?,?,?,?,?)";
	
	private static String DISABLE_USER_STATUS = "UPDATE EEDBUSR.LP_USER_PROFILE_TBL SET  UPDATED_DATE=CURRENT_TIMESTAMP,ACCOUNT_STATUS =? WHERE SAN = ?";
	
	private static String UPDATE_USER_PROFILE="UPDATE EEDBUSR.LP_USER_PROFILE_TBL SET PRODUCT_TYPE =?, CUSTOMERTYPE=?, PACKAGE_ID=?, UPDATED_DATE=CURRENT_TIMESTAMP, ID_TYPE=?, ID_NUMBER=?, ACCOUNT_STATUS=?,PACKAGE_SERVICE_TYPE=?,EMAIL=?,CUST_PROFILE_VO=? WHERE SAN = ?";
	
	private static String UPDATE_ONLY_USER_PROFILE_BY_SAN="UPDATE EEDBUSR.LP_USER_PROFILE_TBL SET CUST_PROFILE_VO=? WHERE SAN = ?";
	private static String UPDATE_CONTACT_NUMBER="UPDATE EEDBUSR.LP_USER_PROFILE_TBL SET CONTACT_NUMBER=?,CONTACT_UPDATED_DATE=CURRENT_TIMESTAMP WHERE SAN = ?";
	private static String GET_CONTACT_DETAILS = "SELECT SAN,PRODUCT_TYPE,CUSTOMERTYPE,ACCOUNT_NUMBER,PACKAGE_ID,CREATED_DATE,UPDATED_DATE,ID_TYPE,ID_NUMBER,ACCOUNT_STATUS,PRIM_CONTACT_MOBILE,PACKAGE_CATEGORY,CUSTOMER_CATEGORY,CORPORATE_PACKAGE,PLAN_CATEGORY,PACKAGE_SERVICE_TYPE,EMAIL,CUST_PROFILE_VO,CONTACT_NUMBER,CONTACT_UPDATED_DATE FROM EEDBUSR.LP_USER_PROFILE_TBL WHERE ACCOUNT_NUMBER =? AND ACCOUNT_STATUS = ?";
	private UserProfileDAO() {
		super(DataSources.DEFAULT);
	}
	
	private static class UserProfileDAOHolder {
		private static final UserProfileDAO userProfileDAOInstance = new UserProfileDAO();
	}
	
	public static UserProfileDAO getInstance() {
		return UserProfileDAOHolder.userProfileDAOInstance;
	}
	
	public UserProfileVO getUserProfile(String lineNumber) {
		
		executionContext.audit(Level.INFO, "UserProfileDAO > getUserProfile > lineNumber:: " + lineNumber, className, "getUserProfile");
		UserProfileVO userProfileVO = null;
		List<UserProfileVO> userProfileVOList =  query(GET_USER_PROFILE,lineNumber,ACTIVE_ACCOUNT_STATUS);
		executionContext.audit(Level.INFO, "UserProfileDAO > getUserProfile > userProfileVOList:: " + userProfileVOList, className, "getUserProfile");
		if(CollectionUtils.isNotEmpty(userProfileVOList)){
			userProfileVO =  userProfileVOList.get(0);
		}
		return userProfileVO;
	}
	public boolean saveUserProfileData(UserProfileVO userProfileVO) {
		executionContext.audit(Level.INFO, "UserProfileDAO > saveUserProfileData > Account Number:: " + userProfileVO.getAccountNumber(), className, "saveUserProfileData");
		//executionContext.audit(Level.INFO, "UserProfileDAO > saveUserProfileData > Customer Info:: " + userProfileVO.getCustomeProfileData(), className, "saveUserProfileData");
		int count = update(SAVE_USER_PROFILE_DATA, userProfileVO.getSan(),userProfileVO.getProductType(),userProfileVO.getCustomerType(),userProfileVO.getAccountNumber(),userProfileVO.getPackageId(),userProfileVO.getIdType(),userProfileVO.getIdNumber(),userProfileVO.getPrimeContactNumber(),userProfileVO.getPackageCategory(),userProfileVO.getCustomerCategory(),userProfileVO.getCorporatePackage(),userProfileVO.getPlanCategory(),userProfileVO.getPackageServiceType(),userProfileVO.getEmail(),userProfileVO.getCustomeProfileData());
		executionContext.audit(Level.INFO, "UserProfileDAO > saveUserProfileData > row inserted:: " + count, className, "saveUserProfileData");
		return count > 0 ? true : false;
	}

	public boolean updateUserProfile(UserProfileVO userProfileVO) {
		executionContext.audit(Level.INFO, "UserProfileDAO > updateUserProfile > Account Number:: " + userProfileVO.getAccountNumber(), className, "updateUserProfile");
		int count = update(UPDATE_USER_PROFILE,userProfileVO.getProductType(),userProfileVO.getCustomerType(), userProfileVO.getPackageId(), userProfileVO.getIdType(),userProfileVO.getIdNumber(),userProfileVO.getAccountStatus(),userProfileVO.getPackageServiceType(),userProfileVO.getEmail(), userProfileVO.getCustomeProfileData(),userProfileVO.getSan());
		executionContext.audit(Level.INFO, "UserProfileDAO > updateUserProfile > row inserted:: " + count, className, "updateUserProfile");
		return count > 0 ? true : false;
	}
	public boolean disableUserStatus(String san){
		executionContext.audit(Level.INFO, "UserProfileDAO > disableUserStatus > Account Number:: " + san, className, "disableUserStatus");
		int count = update(DISABLE_USER_STATUS,INACTIVE_ACCOUNT_STATUS, san);
		return count > 0 ? true : false;
	}

	public boolean updateUserProfileBySAN(UserProfileVO userProfileVO) {
		executionContext.audit(Level.INFO, "UserProfileDAO > updateUserProfileBySAN > Account Number:: " + userProfileVO.getAccountNumber(), className, "updateUserProfileBySAN");
		int count = update(UPDATE_ONLY_USER_PROFILE_BY_SAN, userProfileVO.getCustomeProfileData(),userProfileVO.getSan());
		executionContext.audit(Level.INFO, "UserProfileDAO > updateUserProfileBySAN >  updated the customer profile for SAN successfully> Count:: " + count, className, "updateUserProfileBySAN");
		return count > 0 ? true : false;
	}
	
	public UserProfileVO getContactDetails(String lineNumber) {
		executionContext.audit(Level.INFO, "UserProfileDAO > getContactDetails > lineNumber:: " + lineNumber, className, "getUserProfile");
		UserProfileVO userProfileVO = null;
		List<UserProfileVO> userProfileVOList =  query(GET_CONTACT_DETAILS,lineNumber,ACTIVE_ACCOUNT_STATUS);
		executionContext.audit(Level.INFO, "UserProfileDAO > getContactDetails > userProfileVOList:: " + userProfileVOList, className, "getUserProfile");
		if(CollectionUtils.isNotEmpty(userProfileVOList)){
			userProfileVO =  userProfileVOList.get(0);
		}
		return userProfileVO;
	}

	public boolean updateContactNumber(UserProfileVO userProfileVO) {
		executionContext.audit(Level.INFO, "UserProfileDAO > updateContactNumber > Account Number:: " + userProfileVO.getAccountNumber(), className, "updateUserProfile");
		int count = update(UPDATE_CONTACT_NUMBER,userProfileVO.getContactNumber(),userProfileVO.getSan());
		executionContext.audit(Level.INFO, "UserProfileDAO > updateContactNumber > row inserted:: " + count, className, "updateContactNumber");
		return count > 0 ? true : false;
	}
	
	@Override
	protected UserProfileVO mapDTO(ResultSet rs) throws SQLException {
		
		UserProfileVO userProfileVO = new UserProfileVO();
		userProfileVO.setSan(rs.getString("SAN"));
		userProfileVO.setProductType(rs.getString("PRODUCT_TYPE"));
		userProfileVO.setCustomerType(rs.getString("CUSTOMERTYPE"));
		userProfileVO.setAccountNumber(rs.getString("ACCOUNT_NUMBER"));
		userProfileVO.setPackageId(rs.getString("PACKAGE_ID"));
		userProfileVO.setCreatedDate(rs.getString("CREATED_DATE"));
		userProfileVO.setCreatedTimeStamp(rs.getTimestamp("UPDATED_DATE"));
		userProfileVO.setUpdatedDate(rs.getString("UPDATED_DATE"));
		userProfileVO.setIdType(rs.getString("ID_TYPE"));
		userProfileVO.setIdNumber(rs.getString("ID_NUMBER"));
		userProfileVO.setAccountStatus(rs.getString("ACCOUNT_STATUS"));
		userProfileVO.setPrimeContactNumber(rs.getString("PRIM_CONTACT_MOBILE"));
		userProfileVO.setPackageCategory(rs.getString("PACKAGE_CATEGORY"));
		userProfileVO.setCustomerCategory(rs.getString("CUSTOMER_CATEGORY"));
		userProfileVO.setCorporatePackage(rs.getString("CORPORATE_PACKAGE"));
		userProfileVO.setPlanCategory(rs.getString("PLAN_CATEGORY"));
		userProfileVO.setPackageServiceType(rs.getString("PACKAGE_SERVICE_TYPE"));
		userProfileVO.setEmail(rs.getString("EMAIL"));
		userProfileVO.setCustomeProfileData(rs.getBytes("CUST_PROFILE_VO"));
		if(rs.getMetaData().getColumnCount() > 19){
			userProfileVO.setContactNumber(rs.getString("CONTACT_NUMBER"));
			userProfileVO.setContactUpdateDate(rs.getTimestamp("CONTACT_UPDATED_DATE"));
		}
		return userProfileVO;
	}
}

//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.04.03 at 02:26:45 PM AST 
//


package sa.com.mobily.eportal.customerinfo.jaxb.object;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MsgRqHdr">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FuncId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="RqMode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="RqUID" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="LangPref" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ClientDt" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                   &lt;element name="CallerService" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Body">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="sourceCriteria">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="Specification">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "msgRqHdr",
    "body"
})
@XmlRootElement(name = "retrieveCustomerProductConfiguration")
public class RetrieveCustomerProductConfiguration {

    @XmlElement(name = "MsgRqHdr", required = true)
    protected RetrieveCustomerProductConfiguration.MsgRqHdr msgRqHdr;
    @XmlElement(name = "Body", required = true)
    protected RetrieveCustomerProductConfiguration.Body body;

    /**
     * Gets the value of the msgRqHdr property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveCustomerProductConfiguration.MsgRqHdr }
     *     
     */
    public RetrieveCustomerProductConfiguration.MsgRqHdr getMsgRqHdr() {
        return msgRqHdr;
    }

    /**
     * Sets the value of the msgRqHdr property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveCustomerProductConfiguration.MsgRqHdr }
     *     
     */
    public void setMsgRqHdr(RetrieveCustomerProductConfiguration.MsgRqHdr value) {
        this.msgRqHdr = value;
    }

    /**
     * Gets the value of the body property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveCustomerProductConfiguration.Body }
     *     
     */
    public RetrieveCustomerProductConfiguration.Body getBody() {
        return body;
    }

    /**
     * Sets the value of the body property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveCustomerProductConfiguration.Body }
     *     
     */
    public void setBody(RetrieveCustomerProductConfiguration.Body value) {
        this.body = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="sourceCriteria">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="Specification">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sourceCriteria",
        "targetCriteria"
    })
    public static class Body {
    	
    	@XmlElement(name = "targetCriteria", required = true)
    	protected targetCriteria targetCriteria;
    	
        @XmlElement(required = true)
        protected RetrieveCustomerProductConfiguration.Body.SourceCriteria sourceCriteria;

        /**
         * Gets the value of the sourceCriteria property.
         * 
         * @return
         *     possible object is
         *     {@link RetrieveCustomerProductConfiguration.Body.SourceCriteria }
         *     
         */
        public RetrieveCustomerProductConfiguration.Body.SourceCriteria getSourceCriteria() {
            return sourceCriteria;
        }

        /**
         * Sets the value of the sourceCriteria property.
         * 
         * @param value
         *     allowed object is
         *     {@link RetrieveCustomerProductConfiguration.Body.SourceCriteria }
         *     
         */
        public void setSourceCriteria(RetrieveCustomerProductConfiguration.Body.SourceCriteria value) {
            this.sourceCriteria = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="Specification">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "id",
            "specification"
        })
        public static class SourceCriteria {

            @XmlElement(name = "Id", required = true)
            protected String id;
            @XmlElement(name = "Specification", required = true)
            protected RetrieveCustomerProductConfiguration.Body.SourceCriteria.Specification specification;

            /**
             * Gets the value of the id property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public String getId() {
                return id;
            }

            /**
             * Sets the value of the id property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setId(String value) {
                this.id = value;
            }

            /**
             * Gets the value of the specification property.
             * 
             * @return
             *     possible object is
             *     {@link RetrieveCustomerProductConfiguration.Body.SourceCriteria.Specification }
             *     
             */
            public RetrieveCustomerProductConfiguration.Body.SourceCriteria.Specification getSpecification() {
                return specification;
            }

            /**
             * Sets the value of the specification property.
             * 
             * @param value
             *     allowed object is
             *     {@link RetrieveCustomerProductConfiguration.Body.SourceCriteria.Specification }
             *     
             */
            public void setSpecification(RetrieveCustomerProductConfiguration.Body.SourceCriteria.Specification value) {
                this.specification = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "name"
            })
            public static class Specification {

                @XmlElement(name = "Name", required = true)
                protected String name;

                /**
                 * Gets the value of the name property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getName() {
                    return name;
                }

                /**
                 * Sets the value of the name property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setName(String value) {
                    this.name = value;
                }

            }

        }


		public targetCriteria getTargetCriteria()
		{
			return targetCriteria;
		}

		public void setTargetCriteria(targetCriteria targetCriteria)
		{
			this.targetCriteria = targetCriteria;
		}

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FuncId" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="RqMode" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="RqUID" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="LangPref" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ClientDt" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *         &lt;element name="CallerService" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "funcId",
        "rqMode",
        "rqUID",
        "langPref",
        "clientDt",
        "callerService",
        "sCId",
        "usrId"
    })
    public static class MsgRqHdr {

        @XmlElement(name = "FuncId")
        protected int funcId;
        @XmlElement(name = "RqMode")
        protected int rqMode;
        @XmlElement(name = "RqUID", required = true)
        protected BigDecimal rqUID;
        @XmlElement(name = "LangPref", required = true)
        protected String langPref;
        @XmlElement(name = "ClientDt", required = true)
        @XmlSchemaType(name = "dateTime")
        protected String clientDt;
        @XmlElement(name = "CallerService", required = true)
        protected String callerService;
        @XmlElement(name = "SCId", required = true)
        protected String sCId;
        @XmlElement(name = "UsrId", required = true)
        protected String usrId;

        /**
         * Gets the value of the funcId property.
         * 
         */
        public int getFuncId() {
            return funcId;
        }

        /**
         * Sets the value of the funcId property.
         * 
         */
        public void setFuncId(int value) {
            this.funcId = value;
        }

        /**
         * Gets the value of the rqMode property.
         * 
         */
        public int getRqMode() {
            return rqMode;
        }

        /**
         * Sets the value of the rqMode property.
         * 
         */
        public void setRqMode(int value) {
            this.rqMode = value;
        }

        /**
         * Gets the value of the rqUID property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getRqUID() {
            return rqUID;
        }

        /**
         * Sets the value of the rqUID property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setRqUID(BigDecimal value) {
            this.rqUID = value;
        }

        /**
         * Gets the value of the langPref property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLangPref() {
            return langPref;
        }

        /**
         * Sets the value of the langPref property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLangPref(String value) {
            this.langPref = value;
        }

        /**
         * Gets the value of the clientDt property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClientDt() {
            return clientDt;
        }

        /**
         * Sets the value of the clientDt property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClientDt(String value) {
            this.clientDt = value;
        }

        /**
         * Gets the value of the callerService property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCallerService() {
            return callerService;
        }

        /**
         * Sets the value of the callerService property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCallerService(String value) {
            this.callerService = value;
        }

		public String getsCId()
		{
			return sCId;
		}

		public void setsCId(String sCId)
		{
			this.sCId = sCId;
		}

		public String getUsrId()
		{
			return usrId;
		}

		public void setUsrId(String usrId)
		{
			this.usrId = usrId;
		}

    }

}

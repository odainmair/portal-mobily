package sa.com.mobily.eportal.customerinfo.jaxb.object;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	"name",	
    "characteristicValue"
})

@XmlRootElement(name = "targetCriteria")
public class targetCriteria{
	  @XmlElement(name = "CharacteristicValue", required = true)
	    protected List<CharacteristicValue> characteristicValue;
	  
	  @XmlElement(name = "Name", required = true)
	    protected String name;

	public List<CharacteristicValue> getCharacteristicValue()
	{
		return characteristicValue;
	}

	public void setCharacteristicValue(List<CharacteristicValue> characteristicValue)
	{
		this.characteristicValue = characteristicValue;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

}

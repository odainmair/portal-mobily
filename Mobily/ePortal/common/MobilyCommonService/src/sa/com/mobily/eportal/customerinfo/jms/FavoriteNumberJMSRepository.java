package sa.com.mobily.eportal.customerinfo.jms;

import java.util.logging.Level;


import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.constant.ExceptionConstantIfc;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MQUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.customerinfo.util.AppConfig;
import sa.com.mobily.eportal.customerinfo.vo.FavoriteNumberVO;
import sa.com.mobily.eportal.customerinfo.xml.FavoriteNumberXMLHandler;

public class FavoriteNumberJMSRepository
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	private static final String className = FavoriteNumberJMSRepository.class.getName();

	
	public FavoriteNumberVO getFavoriteNumberEligibility(String packageId)
	{
		String method = "getFavoriteNumberEligibility";
		executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getFavoriteNumberEligibility : start for packageId: " + packageId, className, method);

		FavoriteNumberVO favoriteNumberVO = null;
		MQAuditVO mqAuditVO = new MQAuditVO();
		
		String requestQueueName = AppConfig.getInstance().get(ConstantsIfc.REQUEST_QUEUE_FAV_NO_ELIGIBILITY);
		String replyQueueName = AppConfig.getInstance().get(ConstantsIfc.REPLY_QUEUE_FAV_NO_ELIGIBILITY);
		
		executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getFavoriteNumberEligibility : requestQueueName[" + requestQueueName + "]", className, method);
		executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getFavoriteNumberEligibility : replyQueueName[" + replyQueueName + "]", className, method);

		mqAuditVO.setRequestQueue(requestQueueName);
		mqAuditVO.setReplyQueue(replyQueueName);
		mqAuditVO.setMsisdn(packageId);
		mqAuditVO.setFunctionName(ConstantsIfc.FAV_NUMBER_FUNC_ID);
		String strXMLRequest = new FavoriteNumberXMLHandler().generateFavoriteNumberXML(packageId);
		executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getFavoriteNumberEligibility : strXMLRequest: " + strXMLRequest, className, method);
		mqAuditVO.setMessage(strXMLRequest);
		//String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(strXMLRequest, requestQueueName, replyQueueName);
		String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
		executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getFavoriteNumberEligibility : strXMLReply: " + strXMLReply, className, method);
		mqAuditVO.setReply(strXMLReply);
		if (!FormatterUtility.isEmpty(strXMLReply))
		{
			favoriteNumberVO = new FavoriteNumberXMLHandler().parseFavoriteNumberXML(strXMLReply);
			mqAuditVO.setErrorMessage(favoriteNumberVO.getStatusCode());
			mqAuditVO.setServiceError(favoriteNumberVO.getStatusCode());
			MQUtility.saveMQAudit(mqAuditVO);
			if (!ConstantsIfc.STATUS_CODE_SUCCESS.equalsIgnoreCase(favoriteNumberVO.getStatusCode()))
			{
				throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, favoriteNumberVO.getStatusCode());
			}

		}
		else{
			mqAuditVO.setErrorMessage(String.valueOf(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE));
			mqAuditVO.setServiceError(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
			MQUtility.saveMQAudit(mqAuditVO);
			throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
		}
		return favoriteNumberVO;
	}

}

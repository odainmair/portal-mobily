package sa.com.mobily.eportal.customerinfo.util;

import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.SerializationUtils;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.valueobject.common.ConsumerReplyHeaderVO;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.customerinfo.dao.ConsumerProfileDAO;
import sa.com.mobily.eportal.customerinfo.jms.CustomerInfoJMSRepository;
import sa.com.mobily.eportal.customerinfo.vo.ConsumerFinalReplyVO;
import sa.com.mobily.eportal.customerinfo.vo.ConsumerHeaderVO;
import sa.com.mobily.eportal.customerinfo.vo.ConsumerProfileReplyVO;
import sa.com.mobily.eportal.customerinfo.vo.ConsumerProfileRequestVO;
import sa.com.mobily.eportal.customerinfo.vo.ConsumerProfileVO;
import sa.com.mobily.eportal.customerinfo.vo.ConsumerVO;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

public class ConsumerProfileUtil
{
	

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.CUSTOMER_INFO_SERVICE_LOGGER_NAME);
	
	private static String className = ConsumerProfileUtil.class.getName();
	
	public ConsumerProfileReplyVO getConsumerProfile(String msisdn){
    	executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > msisdn:: "+msisdn, className, "getConsumerProfile");
    	ConsumerProfileReplyVO consumerInfo = null;
    	ConsumerFinalReplyVO consumerReply = null;
    	try{
			if(FormatterUtility.isNotEmpty(msisdn)) {
				ConsumerProfileVO consumerProfile  = ConsumerProfileDAO.getInstance().getConsumerProfile(msisdn);
				boolean status = false;
				if(consumerProfile !=null ){
					// Data available in DB
					executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > data exists in db", className, "getConsumerProfile");
					executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > ::"+consumerProfile.getAccountNumber(), className, "getConsumerProfile");
					if(isConsumerProfileIsExpired(consumerProfile.getCreatedTimeStamp())){ // Data available but expired
					
						executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > data is expired", className, "getConsumerProfile");
						consumerInfo = getConsumerInfo(msisdn); //get data from back end
						executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > Fetching data from MOBILY.INQ.CUST.LINE.INFO.REQ inquiry "+consumerInfo, className, "getConsumerProfile");
						
						if (consumerInfo != null && consumerInfo.getHeaderVO() != null && consumerInfo.getHeaderVO().getErrorCode() != null) {	// check errorCode from backend			
							executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerInfo > errorCode: "+consumerInfo.getHeaderVO().getErrorCode(), className, "getConsumerInfo");
							if (consumerInfo.getHeaderVO().getErrorCode().equals("0000")){ // On Success only update DB
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerInfo > success reply from backend: ", className, "getConsumerInfo");
								updateConsumerProfile(consumerInfo);
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > Updated the customer profile in db successfully", className, "getConsumerProfile");								
							}else {
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > Reply not success from EAI, do not update db, return same reply from backend ", className, "getConsumerProfile");
							}							
						} else {
							//Data is available in DB but expired and from back-end timeout/failure scenario, need to send the same data from DB
							consumerReply = (ConsumerFinalReplyVO) SerializationUtils.deserialize(consumerProfile.getCustomeProfileData());		

							if(consumerReply != null) {															
								consumerInfo = getfinalDesObj(consumerReply,consumerInfo); //
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile >>> after deserializing ServiceAccountNo:"+consumerInfo.getServiceAccountNo(), className, "getConsumerProfile");	
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile >>> after deserializing MSISDN :"+consumerInfo.getMsisdn(), className, "getConsumerProfile");
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile >>> after deserializing PUK1: "+consumerInfo.getPuk1(), className, "getConsumerProfile");
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile >>> after deserializing PUK2: "+consumerInfo.getPuk2(), className, "getConsumerProfile");
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile >>> after deserializing IdNumber: "+consumerInfo.getIdNumber(), className, "getConsumerProfile");
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile >>> after deserializing PackageId: "+consumerInfo.getPackageID(), className, "getConsumerProfile");
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile >>> after deserializing PackageName: "+consumerInfo.getPackageName(), className, "getConsumerProfile");
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile >>> after deserializing BillingAccountNumber: "+consumerInfo.getBillingAccountNumber(), className, "getConsumerProfile");
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile >>> after deserializing City: "+consumerInfo.getCity(), className, "getConsumerProfile");
							
							}						
							executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > No Reply from EAI, return same data from DB "+consumerInfo, className, "getConsumerProfile");
						}

					}else{
						executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > ConsumerProfile is not expired, returning the customerinfo from DB", className, "getConsumerProfile");
						if(consumerProfile.getCustomeProfileData()!=null){
							executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > ConsumerProfile is not null in db", className, "getConsumerProfile");							
							
							consumerReply = (ConsumerFinalReplyVO) SerializationUtils.deserialize(consumerProfile.getCustomeProfileData());
							executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > after consumerReply: "+consumerReply, className, "getConsumerProfile");
							if(consumerReply != null) {																
									consumerInfo = getfinalDesObj(consumerReply,consumerInfo);
									if(consumerInfo != null){
										executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > after deserializing ServiceAccountNo: "+consumerInfo.getServiceAccountNo(), className, "getConsumerProfile");	
										executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > after deserializing MSISDN : "+consumerInfo.getMsisdn(), className, "getConsumerProfile");
										executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > after deserializing PUK1: "+consumerInfo.getPuk1(), className, "getConsumerProfile");
										executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > after deserializing PUK2: "+consumerInfo.getPuk2(), className, "getConsumerProfile");
										executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > after deserializing IDNumber: "+consumerInfo.getIdNumber(), className, "getConsumerProfile");
										executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > after deserializing PackageId: "+consumerInfo.getPackageID(), className, "getConsumerProfile");
										executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > after deserializing PackageName: "+consumerInfo.getPackageName(), className, "getConsumerProfile");
										executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > after deserializing BillingAccountNumber: "+consumerInfo.getBillingAccountNumber(), className, "getConsumerProfile");
										executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > after deserializing City: "+consumerInfo.getCity(), className, "getConsumerProfile");
										
									}
								}
							
							executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > consumerInfo "+consumerInfo, className, "getConsumerProfile");
							executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > Returning the customer profile from DB", className, "getConsumerProfile");
						}else{
							executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > ConsumerProfile is null in db", className, "getConsumerProfile");
							consumerInfo = getConsumerInfo(msisdn);
							executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > Fetched the consumer profie from MOBILY.INQ.CUST.LINE.INFO.REQ inquiry "+consumerInfo, className, "getConsumerProfile");
							
							if (consumerInfo != null && consumerInfo.getHeaderVO() != null && consumerInfo.getHeaderVO().getErrorCode() != null) {				
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerInfo > errorCode:: "+consumerInfo.getHeaderVO().getErrorCode(), className, "getConsumerInfo");
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerInfo > consumerInfo:: "+consumerInfo, className, "getConsumerInfo");
								if (consumerInfo.getHeaderVO().getErrorCode().equals("0000")){
									executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerInfo > success reply from backend:: ", className, "getConsumerInfo");

									consumerReply = getFinalSerObj(consumerInfo);
									
									if(consumerReply != null){
										consumerProfile.setCustomeProfileData(SerializationUtils.serialize(consumerReply));
										status = ConsumerProfileDAO.getInstance().updateConsumerProfileBySAN(consumerProfile);
									}else{
										executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > Iuuse occured while getting serialized obj"+status, className, "getConsumerProfile");
									}
									executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > Update the ConsumerProfile profile for the SAN in db > status::"+status, className, "getConsumerProfile");									
								}else {
									executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > Reply not success from EAI, do not update db, return same reply from backend ", className, "getConsumerProfile");
								}							
							}
							
						}
					}
				}else{// if Consumer profile doesn't exists in DB
					executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > ConsumerProfile is not exists in db (NULL)", className, "getConsumerProfile");
					consumerInfo = getConsumerInfo(msisdn);
					if(consumerInfo!=null){
						executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > Got the profile EAI "+consumerInfo, className, "getConsumerProfile");
						if (consumerInfo != null && consumerInfo.getHeaderVO() != null && consumerInfo.getHeaderVO().getErrorCode() != null) {				
							executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerInfo > errorCode:: "+consumerInfo.getHeaderVO().getErrorCode(), className, "getConsumerInfo");
							if (consumerInfo.getHeaderVO().getErrorCode().equals("0000")){
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerInfo > success reply from backend--- ", className, "getConsumerInfo");
								status = insertConsumerProfile(consumerInfo);
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > Got the profile from CustomerInfo Inquiry & Insert the record in db successfully -> status:: "+status, className, "getConsumerProfile");
							}else {
								executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > Reply not success from EAI, not insert into db, return same reply from backend ", className, "getConsumerProfile");
							}							
						}
						
					}else{
						executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > conumserProfile is  not available from MOBILY.INQ.CUST.LINE.INFO.REQ CustomerInfo Inquiry", className, "getConsumerProfile");
					}
				}
			}else{ // If MSISDN is empty
				executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerProfile > MSISDN is empty", className, "getConsumerProfile");
			}
		}catch(Exception e){
			executionContext.log(Level.SEVERE, "ConsumerProfileUtil > getConsumerProfile > Exception while fetching the customer information ::"+e.getMessage(), className, "getConsumerProfile", e);
		}
		return consumerInfo;
	}
		 	 	 
	 private ConsumerProfileReplyVO getConsumerInfo(String msisdn)
	 {
		 	executionContext.audit(Level.INFO, "ConsumerProfileUtil > getConsumerInfo > msisdn:: "+msisdn, className, "getConsumerInfo");
			ConsumerProfileRequestVO requestVO = new ConsumerProfileRequestVO();
			requestVO.setKeyType(ConstantsIfc.KEY_TYPE_MSISDN);
			requestVO.setKeyValue(msisdn);
			CustomerInfoJMSRepository customerInfo = new CustomerInfoJMSRepository();			
			return customerInfo.getConsumerProfile(requestVO);								
   }
	 
	 private ConsumerProfileReplyVO getfinalDesObj(ConsumerFinalReplyVO consumerReply, ConsumerProfileReplyVO consumerInfo){
		 executionContext.audit(Level.INFO, "ConsumerProfileUtil > getfinalDeserializedObj > Start ", className, "getfinalDesObj");
		 	try
			{
				ConsumerReplyHeaderVO consumerReplyHeader = new  ConsumerReplyHeaderVO();								
				BeanUtils.copyProperties(consumerReplyHeader, consumerReply.getHeaderVO());
				ConsumerVO tempConsumerVO = new ConsumerVO();
				BeanUtils.copyProperties(tempConsumerVO, consumerReply);	
				consumerInfo = new ConsumerProfileReplyVO();
				BeanUtils.copyProperties(consumerInfo,tempConsumerVO);
				consumerInfo.setHeaderVO(consumerReplyHeader);
				executionContext.audit(Level.INFO, "ConsumerProfileUtil > getfinalDesObj > End "+consumerInfo, className, "getfinalDesObj");
			}
			catch (IllegalAccessException e)
			{
				 executionContext.log(Level.SEVERE, "ConsumerProfileUtil > getfinalDesObj > Exception while getting final Deserilized Obj::"+e.getMessage(), className, "getfinalDesObj", e);
			}
			catch (InvocationTargetException e)
			{
				 executionContext.log(Level.SEVERE, "ConsumerProfileUtil > getfinalDesObj > Exception while getting final Deserilized Obj::"+e.getMessage(), className, "getfinalDesObj", e);
			}
			return consumerInfo;
	 }
	 
	 private ConsumerFinalReplyVO getFinalSerObj(ConsumerProfileReplyVO consumerInfo){
		 executionContext.audit(Level.INFO, "ConsumerProfileUtil > getFinalSerObj > Start ", className, "getFinalSerObj");
		 ConsumerFinalReplyVO cfinal = null;
		 	try
			{
				ConsumerHeaderVO ch = new ConsumerHeaderVO();				
				BeanUtils.copyProperties(ch, consumerInfo.getHeaderVO());
				ConsumerVO tempConsumerVO = new ConsumerVO();
				BeanUtils.copyProperties(tempConsumerVO, consumerInfo);	
				cfinal = new ConsumerFinalReplyVO();
				BeanUtils.copyProperties(cfinal,tempConsumerVO);
				cfinal.setHeaderVO(ch);
				executionContext.audit(Level.INFO, "ConsumerProfileUtil > getFinalSerObj > cfinal "+cfinal, className, "getFinalSerObj");
			}
			catch (IllegalAccessException e)
			{
				executionContext.log(Level.SEVERE, "ConsumerProfileUtil > getFinalSerObj > Exception while getting final serilized Obj::"+e.getMessage(), className, "getFinalSerObj", e);
			}
			catch (InvocationTargetException e)
			{
				executionContext.log(Level.SEVERE, "ConsumerProfileUtil > getFinalSerObj > Exception while getting final serilized Obj::"+e.getMessage(), className, "getFinalSerObj", e);			
			}
		 	return cfinal;
		 
	 }
	 public void updateConsumerProfile(ConsumerProfileReplyVO consumerInfoVO){
		 boolean updateStatus=false;
		 ConsumerFinalReplyVO cfinal = null;
		 executionContext.audit(Level.INFO, "ConsumerProfileUtil > updateConsumerProfile > msisdn:: "+consumerInfoVO.getMsisdn(), className, "updateConsumerProfile");
		 executionContext.audit(Level.INFO, "ConsumerProfileUtil > updateConsumerProfile > SAN from Customer Info:: "+consumerInfoVO.getServiceAccountNo(), className, "updateConsumerProfile");
		 try{
			 ConsumerProfileVO consumerProfile  = ConsumerProfileDAO.getInstance().getConsumerProfile(consumerInfoVO.getMsisdn());
			 boolean status =false;
			 if(consumerProfile!=null){
				 executionContext.audit(Level.INFO, "ConsumerProfileUtil > updateConsumerProfile > SAN from Consumer Profile from DB :: "+consumerProfile.getSan(), className, "updateConsumerProfile");
				 if(consumerProfile.getSan().equals(consumerInfoVO.getServiceAccountNo())){
					 executionContext.audit(Level.INFO, "ConsumerProfileUtil > updateConsumerProfile >   both SANs are same", className, "updateConsumerProfile");
					 //userProfile.setAccountNumber(customerInfoVO.getMSISDN());
					 consumerProfile.setAccountStatus(ConstantsIfc.ACTIVE_ACCOUNT_STATUS);
					 //userProfile.setSan(customerInfoVO.getAccountNumber());
					 try{
						
						 cfinal = getFinalSerObj(consumerInfoVO);
							if(cfinal != null){						
								executionContext.log(Level.SEVERE, "ConsumerProfileUtil > updateConsumerProfile >- after serializing the customer info VO ::", className, "updateConsumerProfile");
								 consumerProfile.setCustomeProfileData(SerializationUtils.serialize(cfinal));
								 status = ConsumerProfileDAO.getInstance().updateConsumerProfile(consumerProfile);
							} else {
								 executionContext.log(Level.SEVERE, "ConsumerProfileUtil > updateConsumerProfile > Issue occurred while serializing the customer info VO ::", className, "updateConsumerProfile");
							}
					 }catch(Exception e){
						 executionContext.log(Level.SEVERE, "ConsumerProfileUtil > updateConsumerProfile > Exception while serializing the customer info VO ::"+e.getMessage(), className, "updateConsumerProfile", e);
					 }
					 
					 executionContext.audit(Level.INFO, "ConsumerProfileUtil > updateConsumerProfile > updated the record successfully -> status:: "+status, className, "updateConsumerProfile");
				 }else{
					 executionContext.audit(Level.INFO, "ConsumerProfileUtil > updateConsumerProfile >   both SANs are not same", className, "updateConsumerProfile");
					 updateStatus = ConsumerProfileDAO.getInstance().disableConsumerStatus(consumerProfile.getSan());
					 executionContext.audit(Level.INFO, "ConsumerProfileUtil > updateConsumerProfile > Update the status to Inactive for SAN::"+consumerProfile.getSan()+" >> and updateStatus is :: "+updateStatus, className, "updateConsumerProfile");
					 status = insertConsumerProfile(consumerInfoVO);
					 executionContext.audit(Level.INFO, "ConsumerProfileUtil > updateConsumerProfile > Insert the record successfully -> status::"+status, className, "updateConsumerProfile");
				 }
			 }
		 }
		 catch(Exception e){
			 executionContext.log(Level.SEVERE, "ConsumerProfileUtil > updateConsumerProfile > Exception while fetching the customer information from DB ::"+e.getMessage(), className, "updateConsumerProfile", e);
		 }
	 }
	 
	 public boolean  insertConsumerProfile(ConsumerProfileReplyVO consumerInfoVO){
		 boolean status =false;
		 ConsumerFinalReplyVO cfinal = null;
		 String method = "insertConsumerProfile";
		 executionContext.audit(Level.INFO, "ConsumerProfileUtil > insertConsumerProfile > customerInfoVO ::"+consumerInfoVO, className, method);
		 executionContext.audit(Level.INFO, "ConsumerProfileUtil > insertConsumerProfile > MSISDN ::"+consumerInfoVO.getMsisdn(), className, method);
		 executionContext.audit(Level.INFO, "ConsumerProfileUtil > insertConsumerProfile > getServiceAccountNumber ::"+consumerInfoVO.getServiceAccountNo(), className, method);
		 executionContext.audit(Level.INFO, "ConsumerProfileUtil > insertConsumerProfile > getParentAccountNumber ::"+consumerInfoVO.getParentAccountNumber(), className, method);
		 try{
			ConsumerProfileVO consumerProfile  =  new ConsumerProfileVO();
			consumerProfile.setAccountNumber(consumerInfoVO.getMsisdn());
			consumerProfile.setAccountStatus(ConstantsIfc.ACTIVE_ACCOUNT_STATUS);			
			consumerProfile.setSan(consumerInfoVO.getServiceAccountNo());			
			try{
				executionContext.audit(Level.INFO, "ConsumerProfileUtil > insertConsumerProfile > Before serializing the consumer info", className, method);

				cfinal = getFinalSerObj(consumerInfoVO);
				
				executionContext.audit(Level.INFO, "ConsumerProfileUtil > insertConsumerProfile > after serializing --consumerInfoVO "+consumerInfoVO, className, method);
				if(cfinal != null){
					consumerProfile.setCustomeProfileData(SerializationUtils.serialize(cfinal));
					status = ConsumerProfileDAO.getInstance().saveConsumerProfileData(consumerProfile);
					executionContext.audit(Level.INFO, "ConsumerProfileUtil > insertConsumerProfile > after serializing --status "+status, className, method);
				}else {
					executionContext.log(Level.INFO, "ConsumerProfileUtil > insertConsumerProfile > issue occured while serializing the consumer :", className, method);	
				}
			}catch(Exception e){
				executionContext.log(Level.SEVERE, "ConsumerProfileUtil > insertConsumerProfile > Exception while serializing the consumer info VO ::"+e.getMessage(), className, method, e);
			}
			
			executionContext.audit(Level.INFO, "ConsumerProfileUtil > insertConsumerProfile > Insert the record successfully ", className, method);
		 }catch(Exception e){
			 executionContext.log(Level.SEVERE, "ConsumerProfileUtil > insertConsumerProfile > Exception while inserting the consumer Profile in DB "+e.getMessage(), className, method, e); 
		 }
		 executionContext.audit(Level.INFO, "ConsumerProfileUtil > insertConsumerProfile > status::"+status, className, method);
		 return status;
	 }

	 public  boolean isConsumerProfileIsExpired(Timestamp passExpDate){
    	Timestamp currTimeStamp = new Timestamp(new  java.util.Date().getTime());
    	Calendar cal = GregorianCalendar.getInstance();
    	executionContext.audit(Level.INFO, "ConsumerProfileUtil > isConsumerProfileIsExpired > after passExpDate:: "+passExpDate, className, "isConsumerProfileIsExpired");
    	executionContext.audit(Level.INFO, "ConsumerProfileUtil > isConsumerProfileIsExpired > currTimeStamp:: "+currTimeStamp, className, "isConsumerProfileIsExpired");
    	try {
    		if(passExpDate!=null){
        		cal.setTimeInMillis(passExpDate.getTime());
        		String expDays = (String)ResourceEnvironmentProviderService.getInstance().getPropertyValue("USER_PROFILE_EXP_DAYS");
        		if(FormatterUtility.isNumber(expDays)){
        			cal.add(Calendar.DATE, Integer.parseInt(expDays));
        		}
        		else{
        			cal.add(Calendar.DATE, 365);
        		}
        		passExpDate =  new Timestamp(cal.getTimeInMillis());
        		executionContext.audit(Level.INFO, "ConsumerProfileUtil > isConsumerProfileIsExpired > passExpDate:: "+passExpDate, className, "isConsumerProfileIsExpired");
        		if(passExpDate.before(currTimeStamp)){
            		return true;
            	}
            	
        	}
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "ConsumerProfileUtil>> isConsumerProfileIsExpired >>Exception while checking the date is expired: " + e.getMessage(), className, "isConsumerProfileIsExpired", e);
		}
		return false;    	
	 }
}

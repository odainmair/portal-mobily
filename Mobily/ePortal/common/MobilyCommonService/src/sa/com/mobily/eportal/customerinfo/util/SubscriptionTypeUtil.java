package sa.com.mobily.eportal.customerinfo.util;

import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.customerinfo.constant.SubscriptionTypeConstant;

/**
 * 
 * @author n.gundluru.mit
 *
 */
public class SubscriptionTypeUtil {
	
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	private static String clazz = SubscriptionTypeUtil.class.getName();

	
	public static final String GSM			 = "GSM";
	public static final String three3G		 = "3G";
	public static final String CONNECT		 = "Connect";
	public static final String LTE			 = "LTE";	
	public static final String WiMAX		 = "WiMAX";
	public static final String FTTH			 = "FTTH";
	public static final String IPTV			 = "IPTV";

	public static final int SUBS_TYPE_GSM	 = 1;
	public static final int SUBS_TYPE_3G	 = 2;
	public static final int SUBS_TYPE_LTE	 = 3;
	public static final int SUBS_TYPE_WiMAX	 = 4;
	public static final int SUBS_TYPE_FTTH	 = 5;
	public static final int SUBS_TYPE_IPTV	 = 6;

	
	/**
	 * 
	 * @param subscriptionType
	 * @param subSubscriptionType
	 * @return
	 */
	@SuppressWarnings("unused")
	public static String getSubSubScriptionName(int subscriptionType, int subSubscriptionType){
		
		String subSubScriptionName = "";
		
		executionContext.log(Level.INFO, "Subscription Type = ["+subscriptionType+"] and SubSubscription Type = ["+subSubscriptionType+"]", clazz, "getSubSubScriptionName", null);

			if(SubscriptionTypeConstant.SUBS_TYPE_GSM == subscriptionType){ // GSM customer
				if(SUBS_TYPE_GSM == subSubscriptionType){
					subSubScriptionName = GSM;
				}
			} else if(SubscriptionTypeConstant.SUBS_TYPE_CONNECT == subscriptionType){ // Connect Customer
				if(SUBS_TYPE_3G == subSubscriptionType){
					subSubScriptionName = CONNECT;
				}else if(SUBS_TYPE_LTE == subSubscriptionType){
					subSubScriptionName = LTE;
				}
			} else if(SubscriptionTypeConstant.SUBS_TYPE_WiMAX == subscriptionType){ // WiMax Customer
				if(SUBS_TYPE_WiMAX == subSubscriptionType){
					subSubScriptionName = WiMAX;
				}
			} else if(SubscriptionTypeConstant.SUBS_TYPE_FTTH == subscriptionType){ // FTTH(eLife) Customer
				if(SUBS_TYPE_FTTH == subSubscriptionType){
					subSubScriptionName = FTTH;
				}else if(SUBS_TYPE_IPTV == subSubscriptionType){
					subSubScriptionName = IPTV;
				}
			} 
			
		executionContext.log(Level.INFO, "subSubScriptionName =["+subSubScriptionName+"]", clazz, "getSubSubScriptionName", null);
		
		return subSubScriptionName;
	}
	
	
	
	/**
	 * 
	 * @param subscriptionType
	 * @param subSubscriptionType
	 * @return
	 */
	@SuppressWarnings("unused")
	public static int getSubSubScriptionType(String subscriptionName, String subSubscriptionName){
		
		int subSubScriptionType = -1;
		
		executionContext.log(Level.INFO, "Subscription Name = ["+subscriptionName+"] and SubSubscription Name = ["+subSubscriptionName+"]", clazz, "getSubSubScriptionType", null);
			
			if(SubscriptionTypeConstant.GSM.equalsIgnoreCase(subscriptionName)){ // GSM customer
				if(GSM.equalsIgnoreCase(subSubscriptionName)){
					subSubScriptionType = SUBS_TYPE_GSM;
				}
			} else if(SubscriptionTypeConstant.Connect.equalsIgnoreCase(subscriptionName)){ // Connect Customer
				if(three3G.equalsIgnoreCase(subSubscriptionName) 
						|| CONNECT.equalsIgnoreCase(subSubscriptionName)){
					subSubScriptionType = SUBS_TYPE_3G;
				}else if(LTE.equalsIgnoreCase(subSubscriptionName)){
					subSubScriptionType = SUBS_TYPE_LTE;
				}
			} else if(SubscriptionTypeConstant.WiMAX.equalsIgnoreCase(subscriptionName)){ // WiMax Customer
				if(WiMAX.equalsIgnoreCase(subSubscriptionName)){
					subSubScriptionType = SUBS_TYPE_WiMAX;
				}
			} else if(SubscriptionTypeConstant.FTTH.equalsIgnoreCase(subscriptionName)){ // FTTH(eLife) Customer
				if(FTTH.equalsIgnoreCase(subSubscriptionName)){
					subSubScriptionType = SUBS_TYPE_FTTH;
				}else if(IPTV.equalsIgnoreCase(subSubscriptionName)){
					subSubScriptionType = SUBS_TYPE_IPTV;
				}
			} 

		executionContext.log(Level.INFO, "subSubScriptionType =["+subSubScriptionType+"]", clazz, "getSubSubScriptionType", null);

		return subSubScriptionType;
	}
	
}

package sa.com.mobily.eportal.customerinfo.util;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;

import org.apache.commons.lang.SerializationUtils;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.application.DataNotFoundException;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.customerinfo.constant.SubscriptionTypeConstant;
import sa.com.mobily.eportal.customerinfo.dao.UserProfileDAO;
import sa.com.mobily.eportal.customerinfo.jms.CustomerInfoJMSRepository;
import sa.com.mobily.eportal.customerinfo.vo.CustomerInfoVO;
import sa.com.mobily.eportal.customerinfo.vo.UserProfileVO;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

public class UserProfileUtil
{
	

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.CUSTOMER_INFO_SERVICE_LOGGER_NAME);
	
	private static String className = UserProfileUtil.class.getName();
	
	public CustomerInfoVO getUserProfile(String msisdn){
    	executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > msisdn:: "+msisdn, className, "getUserProfile");
    	CustomerInfoVO customerInfo = null;
    	try{
			if(FormatterUtility.isNotEmpty(msisdn)) {
				UserProfileVO userProfile  = UserProfileDAO.getInstance().getUserProfile(msisdn);
				boolean status = false;
				if(userProfile !=null ){
					executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > userProfile is exists in db", className, "getUserProfile");
					executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > SAN::"+userProfile.getAccountNumber(), className, "getUserProfile");
					if(isUserProfileIsExpired(userProfile.getCreatedTimeStamp())){
						executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > userProfile is expired", className, "getUserProfile");
						customerInfo = getCustInfo(msisdn);
						executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > Fetching the customer profie from RCPC inquiry", className, "getUserProfile");
						if(customerInfo!=null){
							updateUserProfile(customerInfo);
						}
						executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > Updated the customer profile in db successfully", className, "getUserProfile");
					}else{
						executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > userProfile is not expired and hence returning the customerinfo from db", className, "getUserProfile");
						if(userProfile.getCustomeProfileData()!=null){
							executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > userProfile is not null in db", className, "getUserProfile");
							customerInfo = (CustomerInfoVO) SerializationUtils.deserialize(userProfile.getCustomeProfileData());
							
							if(!isUserProfileValid(customerInfo)){
								executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > userProfile is not valid in db", className, "getUserProfile");
								customerInfo = getCustInfo(msisdn);
								executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > Fetched the customer profie from RCPC inquiry", className, "getUserProfile");
								userProfile.setCustomeProfileData(SerializationUtils.serialize(customerInfo));
								status = UserProfileDAO.getInstance().updateUserProfileBySAN(userProfile);
								executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > Update the user profile for the SAN in db > status::"+status, className, "getUserProfile");
								
							} else{
								executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > Returning the customer profile from DB", className, "getUserProfile");
							}
							
						}else{
							executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > userProfile is null in db", className, "getUserProfile");
							customerInfo = getCustInfo(msisdn);
							executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > Fetched the customer profie from RCPC inquiry", className, "getUserProfile");
							userProfile.setCustomeProfileData(SerializationUtils.serialize(customerInfo));
							status = UserProfileDAO.getInstance().updateUserProfileBySAN(userProfile);
							executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > Update the user profile for the SAN in db > status::"+status, className, "getUserProfile");
						}
					}
				}else{// if user profile doesn't exists in DB
					executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > userProfile is not exists in db (NULL)", className, "getUserProfile");
					customerInfo = getCustInfo(msisdn);
					if(customerInfo!=null){
						status = insertUserProfile(customerInfo);
						executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > Got the profile from CustomerInfo Inquiry & Insert the record in db successfully -> status:: "+status, className, "getUserProfile");
					}else{
						executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > userProfile is  not available from RCPC CustomerInfo Inquiry", className, "getUserProfile");
					}
				}
			}else{ // If MSISDN is empty
				executionContext.audit(Level.INFO, "UserProfileUtil > getUserProfile > MSISDN is empty", className, "getUserProfile");
			}
		}catch(DataNotFoundException e){
			executionContext.log(Level.SEVERE, "UserProfileUtil > getUserProfile > DataNotFoundException while fetching the customer information > error message ::"+e.getMessage(), className, "getUserProfile", e);
			throw e;
		}catch(BackEndException e){
			executionContext.log(Level.SEVERE, "UserProfileUtil > getUserProfile > BackEndException while fetching the customer information > error code ::"+e.getErrorCode(), className, "getUserProfile", e);
			throw e;
		}catch(Exception e){
			executionContext.log(Level.SEVERE, "UserProfileUtil > getUserProfile > Exception while fetching the customer information ::"+e.getMessage(), className, "getUserProfile", e);
		}
		return customerInfo;
	}
		 	 
	 private CustomerInfoVO getCustInfo(String msisdn){
		 executionContext.audit(Level.INFO, "UserProfileUtil > retriveCustInfo > msisdn:: "+msisdn, className, "retriveCustInfo");
		 	CustomerInfoVO customerInfoVO = new CustomerInfoVO();
			customerInfoVO.setSourceId(msisdn);
			customerInfoVO.setSourceSpecName(CustomerInfoVO.SOURCE_SPECIFICATION_NAME_MSISDN);
			customerInfoVO.setPreferredLanguage("En");
			CustomerInfoJMSRepository customerInfo = new CustomerInfoJMSRepository();
			return customerInfo.getCustomerProfile(customerInfoVO);
	 }
	 private CustomerInfoVO getCustContactDetails(String msisdn){
		 executionContext.audit(Level.INFO, "UserProfileUtil > getCustContactDetails > msisdn:: "+msisdn, className, "retriveCustInfo");
		 	CustomerInfoVO customerInfoVO = new CustomerInfoVO();
			customerInfoVO.setSourceId(msisdn);
			customerInfoVO.setSourceSpecName(CustomerInfoVO.SOURCE_SPECIFICATION_NAME_MSISDN);
			customerInfoVO.setPreferredLanguage("En");
			CustomerInfoJMSRepository customerInfo = new CustomerInfoJMSRepository();
			return customerInfo.getCustContactDetails(customerInfoVO);
	 }
	 
	 public void updateUserProfile(CustomerInfoVO customerInfoVO){
		 boolean updateStatus=false;
		 executionContext.audit(Level.INFO, "UserProfileUtil > updateUserProfile > msisdn:: "+customerInfoVO.getMSISDN(), className, "updateUserProfile");
		 executionContext.audit(Level.INFO, "UserProfileUtil > updateUserProfile > SAN from Customer Info:: "+customerInfoVO.getAccountNumber(), className, "updateUserProfile");
		 try{
			 UserProfileVO userProfile  = UserProfileDAO.getInstance().getUserProfile(customerInfoVO.getMSISDN());
			 boolean status =false;
			 if(userProfile!=null){
				 executionContext.audit(Level.INFO, "UserProfileUtil > updateUserProfile > SAN from User Profile from DB :: "+userProfile.getSan(), className, "updateUserProfile");
				 if(userProfile.getSan().equals(customerInfoVO.getAccountNumber())){
					 executionContext.audit(Level.INFO, "UserProfileUtil > updateUserProfile >   both SANs are same", className, "updateUserProfile");
					 //userProfile.setAccountNumber(customerInfoVO.getMSISDN());
					 userProfile.setCustomerType(customerInfoVO.getPayType());
					 userProfile.setIdNumber(customerInfoVO.getCustomerIdNumber().trim());
					 userProfile.setIdType(customerInfoVO.getCustomerIdType());
					 userProfile.setPackageId(customerInfoVO.getPackageId());
					 userProfile.setEmail(customerInfoVO.getEmailAddress());
					 userProfile.setPrimeContactNumber(customerInfoVO.getTelephoneNumber());
					 userProfile.setPackageCategory(customerInfoVO.getPackageCategory());
					 if(FormatterUtility.isNotEmpty(customerInfoVO.getCorporatePackage())) {
						 if("N".equalsIgnoreCase(customerInfoVO.getCorporatePackage())){
							 userProfile.setCorporatePackage(ConstantsIfc.CORPORATE_PACKAGE_NO);
						 }else{
							 userProfile.setCorporatePackage(ConstantsIfc.CORPORATE_PACKAGE_YES);
						 }
					 }
					 userProfile.setAccountStatus(ConstantsIfc.ACTIVE_ACCOUNT_STATUS);
					 userProfile.setCustomerCategory(customerInfoVO.getCustomerType());
					 userProfile.setPlanCategory(customerInfoVO.getPlanCategory());
					 userProfile.setPackageServiceType(customerInfoVO.getPackageServiceType());
					 //userProfile.setSan(customerInfoVO.getAccountNumber());
					 userProfile.setProductType(getProductType(customerInfoVO.getPackageCategory()));
					 try{
						 userProfile.setCustomeProfileData(SerializationUtils.serialize(customerInfoVO));
					 }catch(Exception e){
						 executionContext.log(Level.SEVERE, "UserProfileUtil > updateUserProfile > Exception while serializing the customer info VO ::"+e.getMessage(), className, "updateUserProfile", e);
					 }
					 status = UserProfileDAO.getInstance().updateUserProfile(userProfile);
					 executionContext.audit(Level.INFO, "UserProfileUtil > updateUserProfile > updated the record successfully -> status:: "+status, className, "updateUserProfile");
				 }else{
					 executionContext.audit(Level.INFO, "UserProfileUtil > updateUserProfile >   both SANs are not same", className, "updateUserProfile");
					 updateStatus = UserProfileDAO.getInstance().disableUserStatus(userProfile.getSan());
					 executionContext.audit(Level.INFO, "UserProfileUtil > updateUserProfile > Update the status to Inactive for SAN::"+userProfile.getSan()+" >> and updateStatus is :: "+updateStatus, className, "updateUserProfile");
					 status = insertUserProfile(customerInfoVO);
					 executionContext.audit(Level.INFO, "UserProfileUtil > updateUserProfile > Insert the record successfully -> status::"+status, className, "updateUserProfile");
				 }
			 }
		 }
		 catch(Exception e){
			 executionContext.log(Level.SEVERE, "UserProfileUtil > updateUserProfile > Exception while fetching the customer information from DB ::"+e.getMessage(), className, "updateUserProfile", e);
		 }
	 }
	 
	 public boolean  insertUserProfile(CustomerInfoVO customerInfoVO){
		 boolean status =false;
		 executionContext.audit(Level.INFO, "UserProfileUtil > insertUserProfile > customerInfoVO ::"+customerInfoVO, className, "insertUserProfile");
		 executionContext.audit(Level.INFO, "UserProfileUtil > insertUserProfile > MSISDN ::"+customerInfoVO.getMSISDN(), className, "insertUserProfile");
		 try{
			UserProfileVO userProfile  =  new UserProfileVO();
			userProfile.setAccountNumber(customerInfoVO.getMSISDN().trim());
			userProfile.setCustomerType(customerInfoVO.getPayType());
			userProfile.setIdNumber(customerInfoVO.getCustomerIdNumber().trim());
			userProfile.setIdType(customerInfoVO.getCustomerIdType());
			userProfile.setPackageId(customerInfoVO.getPackageId());
			userProfile.setEmail(customerInfoVO.getEmailAddress());
			userProfile.setPrimeContactNumber(customerInfoVO.getTelephoneNumber());
			userProfile.setPackageCategory(customerInfoVO.getPackageCategory());
			if(FormatterUtility.isNotEmpty(customerInfoVO.getCorporatePackage())) {
				if("N".equalsIgnoreCase(customerInfoVO.getCorporatePackage())){
					userProfile.setCorporatePackage(ConstantsIfc.CORPORATE_PACKAGE_NO);
				}else{
					userProfile.setCorporatePackage(ConstantsIfc.CORPORATE_PACKAGE_YES);
				}
			}
			userProfile.setAccountStatus(ConstantsIfc.ACTIVE_ACCOUNT_STATUS);
			userProfile.setCustomerCategory(customerInfoVO.getCustomerType());
			userProfile.setPlanCategory(customerInfoVO.getPlanCategory());
			userProfile.setPackageServiceType(customerInfoVO.getPackageServiceType());
			userProfile.setSan(customerInfoVO.getAccountNumber().trim());
			userProfile.setProductType(getProductType(customerInfoVO.getPackageCategory()));
			try{
				executionContext.audit(Level.INFO, "UserProfileUtil > insertUserProfile > Before serializing the customer info", className, "insertUserProfile");
				userProfile.setCustomeProfileData(SerializationUtils.serialize(customerInfoVO));
			}catch(Exception e){
				executionContext.log(Level.SEVERE, "UserProfileUtil > insertUserProfile > Exception while serializing the customer info VO ::"+e.getMessage(), className, "insertUserProfile", e);
			}
			status = UserProfileDAO.getInstance().saveUserProfileData(userProfile);
			executionContext.audit(Level.INFO, "UserProfileUtil > insertUserProfile > Insert the record successfully ", className, "insertUserProfile");
		 }catch(Exception e){
			 executionContext.log(Level.SEVERE, "UserProfileUtil > insertUserProfile > Exception while inserting the Customer Profile in DB "+e.getMessage(), className, "insertUserProfile", e); 
		 }
		 executionContext.audit(Level.INFO, "UserProfileUtil > insertUserProfile > status::"+status, className, "insertUserProfile");
		 return status;
	 }

	 public  boolean isUserProfileIsExpired(Timestamp passExpDate){
    	Timestamp currTimeStamp = new Timestamp(new  java.util.Date().getTime());
    	Calendar cal = GregorianCalendar.getInstance();
    	executionContext.audit(Level.INFO, "UserProfileUtil > isUserProfileIsExpired > after passExpDate:: "+passExpDate, className, "isUserProfileIsExpired");
    	executionContext.audit(Level.INFO, "UserProfileUtil > isUserProfileIsExpired > currTimeStamp:: "+currTimeStamp, className, "isUserProfileIsExpired");
    	try {
    		if(passExpDate!=null){
        		cal.setTimeInMillis(passExpDate.getTime());
        		String expDays = (String)ResourceEnvironmentProviderService.getInstance().getPropertyValue("USER_PROFILE_EXP_DAYS");
        		if(FormatterUtility.isNumber(expDays)){
        			cal.add(Calendar.DATE, Integer.parseInt(expDays));
        		}
        		else{
        			cal.add(Calendar.DATE, 365);
        		}
        		passExpDate =  new Timestamp(cal.getTimeInMillis());
        		executionContext.audit(Level.INFO, "UserProfileUtil > isUserProfileIsExpired > passExpDate:: "+passExpDate, className, "isUserProfileIsExpired");
        		if(passExpDate.before(currTimeStamp)){
            		return true;
            	}
            	
        	}
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "UserProfileUtil>> isUserProfileIsExpired >>Exception while checking the date is expired: " + e.getMessage(), className, "isUserProfileIsExpired", e);
		}
		return false;
	 }


	 public String getProductType(String packageCategory){
		 String productType="-1";
		 executionContext.audit(Level.INFO, "UserProfileUtil > getProductType > Package Category:: "+packageCategory, className, "getProductType");
		 if(SubscriptionTypeConstant.GSM.equalsIgnoreCase(packageCategory)){
			 productType =ConstantsIfc.PRODUCT_TYPE_GSM;
		 }
		 else if(SubscriptionTypeConstant.Connect.equalsIgnoreCase(packageCategory)){
			 productType =ConstantsIfc.PRODUCT_TYPE_CONNECT;
		 }
		 else if(SubscriptionTypeConstant.WiMAX.equalsIgnoreCase(packageCategory)){
			 productType =ConstantsIfc.PRODUCT_TYPE_WIMAX;
		 }
		 else if(SubscriptionTypeConstant.FTTH.equalsIgnoreCase(packageCategory)){
			 productType =ConstantsIfc.PRODUCT_TYPE_FTTH;
		 }
		 else if(SubscriptionTypeConstant.LTE.equalsIgnoreCase(packageCategory)){
			 productType =ConstantsIfc.PRODUCT_TYPE_LTE;
		 }
		 executionContext.audit(Level.INFO, "UserProfileUtil > getProductType > productType:: "+productType, className, "getProductType");
		 return productType;
	 }
	 
	 public  boolean isUserProfileValid(CustomerInfoVO customerInfo){
	    	boolean isProfileValid = true;
	    	try {
	    		if(customerInfo != null){
	    			if(FormatterUtility.isEmpty(customerInfo.getPackageServiceType())){
	    				isProfileValid = false;
	    			}
	    		}
			} catch (Exception e) {
				executionContext.log(Level.SEVERE, "UserProfileUtil > isUserProfileValid > Exception >>" + e.getMessage(), className, "isUserProfileValid", e);
				isProfileValid = false;
			}
	    	
	    	executionContext.audit(Level.INFO, "UserProfileUtil > isUserProfileValid > isProfileValid =["+isProfileValid+"]", className, "getProductType");
	    	
			return isProfileValid;
		 }
	 public CustomerInfoVO getContactDetails(String msisdn){
	    	executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > msisdn:: "+msisdn, className, "getUserProfile");
	    	CustomerInfoVO customerInfo = null;
	    	try{
				if(FormatterUtility.isNotEmpty(msisdn)) {
					UserProfileVO userProfile  = UserProfileDAO.getInstance().getContactDetails(msisdn);
					boolean status = false;
					if(userProfile !=null ){
						executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > userProfile is exists in db", className, "getContactDetails");
						executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > userProfile"+userProfile, className, "getContactDetails");
						executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > SAN::"+userProfile.getAccountNumber(), className, "getContactDetails");
						if(FormatterUtility.isEmpty(userProfile.getContactNumber()) || null==userProfile.getContactUpdateDate() || isUserProfileIsExpired(userProfile.getContactUpdateDate())){
							executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > userProfile is expired or ContactNumber not exist", className, "getContactDetails");
							customerInfo = getCustContactDetails(msisdn);
							executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > Fetching the customer profie from new RCPC inquiry", className, "getContactDetails");
							if(customerInfo!=null){
								executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > customerInfo not null from new RCPC inquiry"+customerInfo, className, "getContactDetails");
								userProfile.setContactNumber(FormatterUtility.checkNull(customerInfo.getTelephoneNumber()));
								customerInfo.setContactNumber(FormatterUtility.checkNull(customerInfo.getTelephoneNumber()));
								UserProfileDAO.getInstance().updateContactNumber(userProfile);
							}
							executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > Updated the customer profile in db successfully", className, "getContactDetails");
						}else{
							executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > userProfile is not expired and hence returning the customerinfo from db", className, "getContactDetails");
							if(userProfile.getCustomeProfileData()!=null){
								executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > userProfile is not null in db", className, "getContactDetails");
								customerInfo = (CustomerInfoVO) SerializationUtils.deserialize(userProfile.getCustomeProfileData());
								customerInfo.setContactNumber(FormatterUtility.checkNull(userProfile.getContactNumber()));
								executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > Contactnumber updated to customerInfo", className, "getContactDetails");
								if(!isUserProfileValid(customerInfo)){
									executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > userProfile is not valid in db", className, "getContactDetails");
									customerInfo = getCustInfo(msisdn);
									executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > Fetched the customer profie from RCPC inquiry", className, "getContactDetails");
									userProfile.setCustomeProfileData(SerializationUtils.serialize(customerInfo));
									status = UserProfileDAO.getInstance().updateUserProfileBySAN(userProfile);
									executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > Update the user profile for the SAN in db > status::"+status, className, "getContactDetails");
									customerInfo = getCustContactDetails(msisdn);
									executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > Fetching the customer profie from New RCPC inquiry", className, "getContactDetails");
									if(customerInfo!=null){
										userProfile.setContactNumber(FormatterUtility.checkNull(customerInfo.getTelephoneNumber()));
										customerInfo.setContactNumber(FormatterUtility.checkNull(customerInfo.getTelephoneNumber()));
										UserProfileDAO.getInstance().updateContactNumber(userProfile);
										executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > updated contact number", className, "getContactDetails");
									}
								} else{
									executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > Returning the customer profile from DB", className, "getContactDetails");
								}
								
							}else{
								executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > userProfile is null in db", className, "getContactDetails");
								customerInfo = getCustInfo(msisdn);
								executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > in else > Fetched the customer profie from RCPC inquiry", className, "getContactDetails");
								if(customerInfo!=null){
									customerInfo.setContactNumber(FormatterUtility.checkNull(customerInfo.getTelephoneNumber()));
									userProfile.setCustomeProfileData(SerializationUtils.serialize(customerInfo));
									status = UserProfileDAO.getInstance().updateUserProfileBySAN(userProfile);
									
								}
								executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > in else > Update the user profile for the SAN in db > status::"+status, className, "getContactDetails");
								customerInfo = getCustContactDetails(msisdn);
								executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > in else > Fetching the customer profie from New RCPC inquiry", className, "getContactDetails");
								if(customerInfo!=null){
									userProfile.setContactNumber(FormatterUtility.checkNull(customerInfo.getTelephoneNumber()));
									UserProfileDAO.getInstance().updateContactNumber(userProfile);
									executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > in else > updated contact number", className, "getContactDetails");
								}
							}
						}
					}/*else{// if user profile doesn't exists in DB
						executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > userProfile is not exists in db (NULL)", className, "getUserProfile");
						customerInfo = getCustInfo(msisdn);
						if(customerInfo!=null){
							status = insertUserProfile(customerInfo);
							executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > Got the profile from CustomerInfo Inquiry & Insert the record in db successfully -> status:: "+status, className, "getUserProfile");
						}else{
							executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > userProfile is  not available from RCPC CustomerInfo Inquiry", className, "getUserProfile");
						}
					}*/
				}else{ // If MSISDN is empty
					executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > MSISDN is empty", className, "getContactDetails");
				}
			}catch(DataNotFoundException e){
				executionContext.log(Level.SEVERE, "UserProfileUtil > getContactDetails > DataNotFoundException while fetching the customer information > error message ::"+e.getMessage(), className, "getContactDetails", e);
				throw e;
			}catch(BackEndException e){
				executionContext.log(Level.SEVERE, "UserProfileUtil > getContactDetails > BackEndException while fetching the customer information > error code ::"+e.getErrorCode(), className, "getContactDetails", e);
				throw e;
			}catch(Exception e){
				executionContext.log(Level.SEVERE, "UserProfileUtil > getContactDetails > Exception while fetching the customer information ::"+e.getMessage(), className, "getContactDetails", e);
			}
	    	executionContext.audit(Level.INFO, "UserProfileUtil > getContactDetails > end > customerInfo::"+customerInfo, className, "getContactDetails");
			return customerInfo;
		}
}

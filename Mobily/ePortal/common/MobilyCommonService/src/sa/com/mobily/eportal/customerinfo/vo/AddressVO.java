package sa.com.mobily.eportal.customerinfo.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import sa.com.mobily.eportal.common.service.vo.BaseVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}IsPrimary"/>
 *         &lt;element ref="{}CompanyAddress"/>
 *         &lt;element ref="{}CompanyCity"/>
 *         &lt;element ref="{}CompanyCountry"/>
 *         &lt;element ref="{}CompanyPostalCode"/>
 *         &lt;element ref="{}CompanyPOBox"/>
 *         &lt;element ref="{}BillingZIPCode"/>
 *         &lt;element ref="{}BillingAddress"/>
 *         &lt;element ref="{}BillingCity"/>
 *         &lt;element ref="{}BillingCountry"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "isPrimary",
    "companyAddress",
    "companyCity",
    "companyCountry",
    "companyPostalCode",
    "companyPOBox",
    "billingZIPCode",
    "billingAddress",
    "billingCity",
    "billingCountry"
})
@XmlRootElement(name = "Address")
public class AddressVO extends BaseVO{

    @XmlElement(name = "IsPrimary", required = true)
    protected String isPrimary;
    @XmlElement(name = "CompanyAddress", required = true)
    protected String companyAddress;
    @XmlElement(name = "CompanyCity", required = true)
    protected String companyCity;
    @XmlElement(name = "CompanyCountry", required = true)
    protected String companyCountry;
    @XmlElement(name = "CompanyPostalCode", required = true)
    protected String companyPostalCode;
    @XmlElement(name = "CompanyPOBox", required = true)
    protected String companyPOBox;
    @XmlElement(name = "BillingZIPCode", required = true)
    protected String billingZIPCode;
    @XmlElement(name = "BillingAddress", required = true)
    protected String billingAddress;
    @XmlElement(name = "BillingCity", required = true)
    protected String billingCity;
    @XmlElement(name = "BillingCountry", required = true)
    protected String billingCountry;

    /**
     * Gets the value of the isPrimary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrimary() {
        return isPrimary;
    }

    /**
     * Sets the value of the isPrimary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrimary(String value) {
        this.isPrimary = value;
    }

    /**
     * Gets the value of the companyAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyAddress() {
        return companyAddress;
    }

    /**
     * Sets the value of the companyAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyAddress(String value) {
        this.companyAddress = value;
    }

    /**
     * Gets the value of the companyCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyCity() {
        return companyCity;
    }

    /**
     * Sets the value of the companyCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyCity(String value) {
        this.companyCity = value;
    }

    /**
     * Gets the value of the companyCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyCountry() {
        return companyCountry;
    }

    /**
     * Sets the value of the companyCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyCountry(String value) {
        this.companyCountry = value;
    }

    /**
     * Gets the value of the companyPostalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyPostalCode() {
        return companyPostalCode;
    }

    /**
     * Sets the value of the companyPostalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyPostalCode(String value) {
        this.companyPostalCode = value;
    }

    /**
     * Gets the value of the companyPOBox property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyPOBox() {
        return companyPOBox;
    }

    /**
     * Sets the value of the companyPOBox property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyPOBox(String value) {
        this.companyPOBox = value;
    }

    /**
     * Gets the value of the billingZIPCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingZIPCode() {
        return billingZIPCode;
    }

    /**
     * Sets the value of the billingZIPCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingZIPCode(String value) {
        this.billingZIPCode = value;
    }

    /**
     * Gets the value of the billingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingAddress() {
        return billingAddress;
    }

    /**
     * Sets the value of the billingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingAddress(String value) {
        this.billingAddress = value;
    }

    /**
     * Gets the value of the billingCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingCity() {
        return billingCity;
    }

    /**
     * Sets the value of the billingCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingCity(String value) {
        this.billingCity = value;
    }

    /**
     * Gets the value of the billingCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingCountry() {
        return billingCountry;
    }

    /**
     * Sets the value of the billingCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingCountry(String value) {
        this.billingCountry = value;
    }

}

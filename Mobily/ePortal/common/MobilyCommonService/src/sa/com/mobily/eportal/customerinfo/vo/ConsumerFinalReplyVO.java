package sa.com.mobily.eportal.customerinfo.vo;
import sa.com.mobily.eportal.common.service.vo.BaseVO;
public class ConsumerFinalReplyVO  extends BaseVO{

	
		private static final long serialVersionUID = 1L;
		private ConsumerHeaderVO headerVO;
	    private String msisdn;
	    private String serviceAccountNo;
	    private String billingAccountNumber;
	    private String parentAccountNumber;
	    private String genderChangeCounter;
	    private String customerCategory;
	    private String customerType;
	    private String plan;
	    private String status;
	    private String packageName;
	    private String packageID;
	    private String name;
	    private String idDocType;
	    private String idNumber;
	    private String idExpirydate;
	    private String nationality;
	    private String streetAddress;
	    private String city;
	    private String poBox;
	    private String contactPreference;
	    private String zipCode;
	    private String mobile;
	    private String homeTelephone;
	    private String workTelphone;
	    private String fax;
	    private String email;
	    private String gender;
	    private String dobFormat;
	    private String birthDate;
	    private String timeCreated;
	    private String mbAccountID;
	    private String parentAccountID;
	    private String firstName;
	    private String midName;
	    private String lastName;
	    private String title;
	    private String langPref;
	    private String customerSegment;
	    private String simNumber;
	    private String imsi;
	    private String puk1;
	    private String puk2;
	    private String servMobile;
	    private String cpeSerialNumber;
	    private String digitalCertificateNumber;
	    private String wimaxExpDate;
	    private String wimaxDuration;
	    private String vanityType;
	    private String productLine;
	    private String fingerPrintFlag;//Y|N
	    private String idExpiryFlag;//Y|N
	    private String payType; //1: Prepaid | 2: Postpaid 
	    private String packageDataType;//GSM, LTE, etc
	    private String packageCategory; //control, Voice (Control) or Control Plus, etc..
	    protected String pin1;
	    protected String pin2;
	    protected String puk;


	    public  ConsumerHeaderVO getHeaderVO() {
			return headerVO;
	    }
	    
		public void setHeaderVO(ConsumerHeaderVO headerVO) {			
			this.headerVO = headerVO;
		}

		public String getMsisdn() {			
			return msisdn;
		}

		public void setMsisdn(String msisdn) {			
			this.msisdn = msisdn;
		}
		public String getServiceAccountNo()
		{
			return serviceAccountNo;
		}

		public void setServiceAccountNo(String serviceAccountNo)
		{
			this.serviceAccountNo = serviceAccountNo;
		}
		
		public String getBillingAccountNumber() {
			return billingAccountNumber;
		}

		public void setBillingAccountNumber(String billingAccountNumber) {
			this.billingAccountNumber = billingAccountNumber;
		}

		public String getParentAccountNumber() {
			return parentAccountNumber;
		}

		public void setParentAccountNumber(String parentAccountNumber) {
			this.parentAccountNumber = parentAccountNumber;
		}

		public String getGenderChangeCounter() {
			return genderChangeCounter;
		}

		public void setGenderChangeCounter(String genderChangeCounter) {
			this.genderChangeCounter = genderChangeCounter;
		}

		public String getCustomerCategory() {
			return customerCategory;
		}

		public void setCustomerCategory(String customerCategory) {
			this.customerCategory = customerCategory;
		}

		public String getCustomerType() {
			return customerType;
		}

		public void setCustomerType(String customerType) {
			this.customerType = customerType;
		}

		public String getPlan() {
			return plan;
		}

		public void setPlan(String plan) {
			this.plan = plan;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getPackageID() {
			return packageID;
		}

		public void setPackageID(String packageID) {
			this.packageID = packageID;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getIdDocType() {
			return idDocType;
		}

		public void setIdDocType(String idDocType) {
			this.idDocType = idDocType;
		}

		public String getIdNumber() {
			return idNumber;
		}

		public void setIdNumber(String idNumber) {
			this.idNumber = idNumber;
		}

		public String getIdExpirydate() {
			return idExpirydate;
		}

		public void setIdExpirydate(String idExpirydate) {
			this.idExpirydate = idExpirydate;
		}

		public String getNationality() {
			return nationality;
		}

		public void setNationality(String nationality) {
			this.nationality = nationality;
		}

		public String getStreetAddress() {
			return streetAddress;
		}

		public void setStreetAddress(String streetAddress) {
			this.streetAddress = streetAddress;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getPoBox() {
			return poBox;
		}

		public void setPoBox(String poBox) {
			this.poBox = poBox;
		}

		public String getContactPreference() {
			return contactPreference;
		}

		public void setContactPreference(String contactPreference) {
			this.contactPreference = contactPreference;
		}

		public String getZipCode() {
			return zipCode;
		}

		public void setZipCode(String zipCode) {
			this.zipCode = zipCode;
		}

		public String getMobile() {
			return mobile;
		}

		public void setMobile(String mobile) {
			this.mobile = mobile;
		}

		public String getHomeTelephone() {
			return homeTelephone;
		}

		public void setHomeTelephone(String homeTelephone) {
			this.homeTelephone = homeTelephone;
		}

		public String getWorkTelphone() {
			return workTelphone;
		}

		public void setWorkTelphone(String workTelphone) {
			this.workTelphone = workTelphone;
		}

		public String getFax() {
			return fax;
		}

		public void setFax(String fax) {
			this.fax = fax;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getGender() {
			return gender;
		}

		public void setGender(String gender) {
			this.gender = gender;
		}

		public String getDobFormat() {
			return dobFormat;
		}

		public void setDobFormat(String dobFormat) {
			this.dobFormat = dobFormat;
		}

		public String getBirthDate() {
			return birthDate;
		}

		public void setBirthDate(String birthDate) {
			this.birthDate = birthDate;
		}

		public String getTimeCreated() {
			return timeCreated;
		}

		public void setTimeCreated(String timeCreated) {
			this.timeCreated = timeCreated;
		}

		public String getMbAccountID() {
			return mbAccountID;
		}

		public void setMbAccountID(String mbAccountID) {
			this.mbAccountID = mbAccountID;
		}

		public String getParentAccountID() {
			return parentAccountID;
		}

		public void setParentAccountID(String parentAccountID) {
			this.parentAccountID = parentAccountID;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getMidName() {
			return midName;
		}

		public void setMidName(String midName) {
			this.midName = midName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getLangPref() {
			return langPref;
		}

		public void setLangPref(String langPref) {
			this.langPref = langPref;
		}

		public String getCustomerSegment() {
			return customerSegment;
		}

		public void setCustomerSegment(String customerSegment) {
			this.customerSegment = customerSegment;
		}

		public String getSimNumber() {
			return simNumber;
		}

		public void setSimNumber(String simNumber) {
			this.simNumber = simNumber;
		}

		public String getImsi() {
			return imsi;
		}

		public void setImsi(String imsi) {
			this.imsi = imsi;
		}

		public String getPuk1() {
			return puk1;
		}

		public void setPuk1(String puk1) {
			this.puk1 = puk1;
		}

		public String getPuk2() {
			return puk2;
		}

		public void setPuk2(String puk2) {
			this.puk2 = puk2;
		}

		public String getServMobile() {
			return servMobile;
		}

		public void setServMobile(String servMobile) {
			this.servMobile = servMobile;
		}

		public String getCpeSerialNumber() {
			return cpeSerialNumber;
		}

		public void setCpeSerialNumber(String cpeSerialNumber) {
			this.cpeSerialNumber = cpeSerialNumber;
		}

		public String getDigitalCertificateNumber() {
			return digitalCertificateNumber;
		}

		public void setDigitalCertificateNumber(String digitalCertificateNumber) {
			this.digitalCertificateNumber = digitalCertificateNumber;
		}

		public String getWimaxExpDate() {
			return wimaxExpDate;
		}

		public void setWimaxExpDate(String wimaxExpDate) {
			this.wimaxExpDate = wimaxExpDate;
		}

		public String getWimaxDuration() {
			return wimaxDuration;
		}

		public void setWimaxDuration(String wimaxDuration) {
			this.wimaxDuration = wimaxDuration;
		}

		public String getVanityType() {
			return vanityType;
		}

		public void setVanityType(String vanityType) {
			this.vanityType = vanityType;
		}

		public String getProductLine() {
			return productLine;
		}

		public void setProductLine(String productLine) {
			this.productLine = productLine;
		}

		public String getPackageName() {
			return packageName;
		}

		public void setPackageName(String packageName) {
			this.packageName = packageName;
		}
		
		
		public String getFingerPrintFlag()
		{
			return fingerPrintFlag;
		}

		public void setFingerPrintFlag(String fingerPrintFlag)
		{
			this.fingerPrintFlag = fingerPrintFlag;
		}

		public String getIdExpiryFlag()
		{
			return idExpiryFlag;
		}

		public void setIdExpiryFlag(String idExpiryFlag)
		{
			this.idExpiryFlag = idExpiryFlag;
		}

		public String getPayType()
		{
			return payType;
		}

		public void setPayType(String payType)
		{
			this.payType = payType;
		}

		public String getPackageDataType()
		{
			return packageDataType;
		}

		public void setPackageDataType(String packageDataType)
		{
			this.packageDataType = packageDataType;
		}

		public String getPackageCategory()
		{
			return packageCategory;
		}

		public void setPackageCategory(String packageCategory)
		{
			this.packageCategory = packageCategory;
		}

		public String getPin1()
		{
			return pin1;
		}

		public void setPin1(String pin1)
		{
			this.pin1 = pin1;
		}

		public String getPin2()
		{
			return pin2;
		}

		public void setPin2(String pin2)
		{
			this.pin2 = pin2;
		}

		public String getPuk()
		{
			return puk;
		}

		public void setPuk(String puk)
		{
			this.puk = puk;
		}
		
	}

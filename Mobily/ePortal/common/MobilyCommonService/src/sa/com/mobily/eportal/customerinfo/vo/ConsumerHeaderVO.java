package sa.com.mobily.eportal.customerinfo.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class ConsumerHeaderVO extends BaseVO{

   
	private static final long serialVersionUID = 1L;

	private String msgFormat;
    
    private String msgVersion;
    
    private String requestorChannelId;
    
    private String requestorChannelFunction;
    
    private String requestorUserId;
    
    private String requestorLanguage;
    
    private String requestorSecurityInfo;
    
    private String channelTransId;
    
    private String status;
    
    private String errorCode;
    
    private String errorMessage;

	public String getMsgFormat()
	{
		System.out.println("Return msgFormat : " + msgFormat);
		return msgFormat;
	}

	public void setMsgFormat(String msgFormat)
	{
		System.out.println("msgFormat : " + msgFormat);
		this.msgFormat = msgFormat;
	}

	public String getMsgVersion()
	{
		System.out.println("Return msgVersion : " + msgVersion);
		return msgVersion;
	}

	public void setMsgVersion(String msgVersion)
	{
		System.out.println("msgVersion : " + msgVersion);
		this.msgVersion = msgVersion;
	}

	public String getRequestorChannelId()
	{
		return requestorChannelId;
	}

	public void setRequestorChannelId(String requestorChannelId)
	{
		this.requestorChannelId = requestorChannelId;
	}

	public String getRequestorChannelFunction()
	{
		return requestorChannelFunction;
	}

	public void setRequestorChannelFunction(String requestorChannelFunction)
	{
		this.requestorChannelFunction = requestorChannelFunction;
	}

	public String getRequestorUserId()
	{
		return requestorUserId;
	}

	public void setRequestorUserId(String requestorUserId)
	{
		this.requestorUserId = requestorUserId;
	}

	public String getRequestorLanguage()
	{
		return requestorLanguage;
	}

	public void setRequestorLanguage(String requestorLanguage)
	{
		this.requestorLanguage = requestorLanguage;
	}

	public String getRequestorSecurityInfo()
	{
		return requestorSecurityInfo;
	}

	public void setRequestorSecurityInfo(String requestorSecurityInfo)
	{
		this.requestorSecurityInfo = requestorSecurityInfo;
	}

	public String getChannelTransId()
	{
		return channelTransId;
	}

	public void setChannelTransId(String channelTransId)
	{
		this.channelTransId = channelTransId;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getErrorCode()
	{
		return errorCode;
	}

	public void setErrorCode(String errorCode)
	{
		this.errorCode = errorCode;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}


}

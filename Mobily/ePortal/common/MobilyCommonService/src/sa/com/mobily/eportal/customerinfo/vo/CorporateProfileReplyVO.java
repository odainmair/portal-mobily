package sa.com.mobily.eportal.customerinfo.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}SR_HEADER"/>
 *         &lt;element ref="{}CompanyName"/>
 *         &lt;element ref="{}CustomerAccountNumber"/>
 *         &lt;element ref="{}CustomerCreatedLogin"/>
 *         &lt;element ref="{}Customercategory"/>
 *         &lt;element ref="{}Customertype"/>
 *         &lt;element ref="{}IDDocType"/>
 *         &lt;element ref="{}Companyidnumber"/>
 *         &lt;element ref="{}OfficeLocation"/>
 *         &lt;element ref="{}CompanyType"/>
 *         &lt;element ref="{}CompanyCountryofOrigin"/>
 *         &lt;element ref="{}CompanyCapital"/>
 *         &lt;element ref="{}CompanyROWID"/>
 *         &lt;element ref="{}CompanyCreatedDate"/>
 *         &lt;element ref="{}CompanyStatus"/>
 *         &lt;element ref="{}BillingCompanyName"/>
 *         &lt;element ref="{}BillingLangRef"/>
 *         &lt;element ref="{}BillingAccountNumber"/>
 *         &lt;element ref="{}BillingROWID"/>
 *         &lt;element ref="{}BillCreatedLogin"/>
 *         &lt;element ref="{}KAMLogin"/>
 *         &lt;element ref="{}ContactList"/>
 *         &lt;element ref="{}AddressList"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headerVO",
    "companyName",
    "customerAccountNumber",
    "customerCreatedLogin",
    "customercategory",
    "customertype",
    "idDocType",
    "companyidnumber",
    "officeLocation",
    "companyType",
    "companyCountryofOrigin",
    "companyCapital",
    "companyROWID",
    "companyCreatedDate",
    "companyStatus",
    "billingCompanyName",
    "billingLangRef",
    "billingAccountNumber",
    "billingROWID",
    "billCreatedLogin",
    "kamLogin",
    "contactListVO",
    "addressListVO"
})
@XmlRootElement(name = "MOBILY_SR_MESSAGE")
public class CorporateProfileReplyVO {

    @XmlElement(name = "SR_HEADER", required = true)
    protected HeaderVO headerVO;
    @XmlElement(name = "CompanyName", required = true)
    protected String companyName;
    @XmlElement(name = "CustomerAccountNumber", required = true)
    protected String customerAccountNumber;
    @XmlElement(name = "CustomerCreatedLogin", required = true)
    protected String customerCreatedLogin;
    @XmlElement(name = "Customercategory", required = true)
    protected String customercategory;
    @XmlElement(name = "Customertype", required = true)
    protected String customertype;
    @XmlElement(name = "IDDocType", required = true)
    protected String idDocType;
    @XmlElement(name = "Companyidnumber", required = true)
    protected String companyidnumber;
    @XmlElement(name = "OfficeLocation", required = true)
    protected String officeLocation;
    @XmlElement(name = "CompanyType", required = true)
    protected String companyType;
    @XmlElement(name = "CompanyCountryofOrigin", required = true)
    protected String companyCountryofOrigin;
    @XmlElement(name = "CompanyCapital", required = true)
    protected String companyCapital;
    @XmlElement(name = "CompanyROWID", required = true)
    protected String companyROWID;
    @XmlElement(name = "CompanyCreatedDate", required = true)
    protected String companyCreatedDate;
    @XmlElement(name = "CompanyStatus", required = true)
    protected String companyStatus;
    @XmlElement(name = "BillingCompanyName", required = true)
    protected String billingCompanyName;
    @XmlElement(name = "BillingLangRef", required = true)
    protected String billingLangRef;
    @XmlElement(name = "BillingAccountNumber", required = true)
    protected String billingAccountNumber;
    @XmlElement(name = "BillingROWID", required = true)
    protected String billingROWID;
    @XmlElement(name = "BillCreatedLogin", required = true)
    protected String billCreatedLogin;
    @XmlElement(name = "KAMLogin", required = true)
    protected String kamLogin;
    @XmlElement(name = "ContactList", required = true)
    protected ContactListVO contactListVO;
    @XmlElement(name = "AddressList", required = true)
    protected AddressListVO addressListVO;



    public HeaderVO getHeaderVO() {
		return headerVO;
	}

	public void setHeaderVO(HeaderVO headerVO) {
		this.headerVO = headerVO;
	}

	/**
     * Gets the value of the companyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Sets the value of the companyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyName(String value) {
        this.companyName = value;
    }

    /**
     * Gets the value of the customerAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerAccountNumber() {
        return customerAccountNumber;
    }

    /**
     * Sets the value of the customerAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerAccountNumber(String value) {
        this.customerAccountNumber = value;
    }

    /**
     * Gets the value of the customerCreatedLogin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerCreatedLogin() {
        return customerCreatedLogin;
    }

    /**
     * Sets the value of the customerCreatedLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerCreatedLogin(String value) {
        this.customerCreatedLogin = value;
    }

    /**
     * Gets the value of the customercategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomercategory() {
        return customercategory;
    }

    /**
     * Sets the value of the customercategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomercategory(String value) {
        this.customercategory = value;
    }

    /**
     * Gets the value of the customertype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomertype() {
        return customertype;
    }

    /**
     * Sets the value of the customertype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomertype(String value) {
        this.customertype = value;
    }

    /**
     * Gets the value of the idDocType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDDocType() {
        return idDocType;
    }

    /**
     * Sets the value of the idDocType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDDocType(String value) {
        this.idDocType = value;
    }

    /**
     * Gets the value of the companyidnumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyidnumber() {
        return companyidnumber;
    }

    /**
     * Sets the value of the companyidnumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyidnumber(String value) {
        this.companyidnumber = value;
    }

    /**
     * Gets the value of the officeLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficeLocation() {
        return officeLocation;
    }

    /**
     * Sets the value of the officeLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficeLocation(String value) {
        this.officeLocation = value;
    }

    /**
     * Gets the value of the companyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyType() {
        return companyType;
    }

    /**
     * Sets the value of the companyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyType(String value) {
        this.companyType = value;
    }

    /**
     * Gets the value of the companyCountryofOrigin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyCountryofOrigin() {
        return companyCountryofOrigin;
    }

    /**
     * Sets the value of the companyCountryofOrigin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyCountryofOrigin(String value) {
        this.companyCountryofOrigin = value;
    }

    /**
     * Gets the value of the companyCapital property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyCapital() {
        return companyCapital;
    }

    /**
     * Sets the value of the companyCapital property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyCapital(String value) {
        this.companyCapital = value;
    }

    /**
     * Gets the value of the companyROWID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyROWID() {
        return companyROWID;
    }

    /**
     * Sets the value of the companyROWID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyROWID(String value) {
        this.companyROWID = value;
    }

    /**
     * Gets the value of the companyCreatedDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyCreatedDate() {
        return companyCreatedDate;
    }

    /**
     * Sets the value of the companyCreatedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyCreatedDate(String value) {
        this.companyCreatedDate = value;
    }

    /**
     * Gets the value of the companyStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyStatus() {
        return companyStatus;
    }

    /**
     * Sets the value of the companyStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyStatus(String value) {
        this.companyStatus = value;
    }

    /**
     * Gets the value of the billingCompanyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingCompanyName() {
        return billingCompanyName;
    }

    /**
     * Sets the value of the billingCompanyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingCompanyName(String value) {
        this.billingCompanyName = value;
    }

    /**
     * Gets the value of the billingLangRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingLangRef() {
        return billingLangRef;
    }

    /**
     * Sets the value of the billingLangRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingLangRef(String value) {
        this.billingLangRef = value;
    }

    /**
     * Gets the value of the billingAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingAccountNumber() {
        return billingAccountNumber;
    }

    /**
     * Sets the value of the billingAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingAccountNumber(String value) {
        this.billingAccountNumber = value;
    }

    /**
     * Gets the value of the billingROWID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingROWID() {
        return billingROWID;
    }

    /**
     * Sets the value of the billingROWID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingROWID(String value) {
        this.billingROWID = value;
    }

    /**
     * Gets the value of the billCreatedLogin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillCreatedLogin() {
        return billCreatedLogin;
    }

    /**
     * Sets the value of the billCreatedLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillCreatedLogin(String value) {
        this.billCreatedLogin = value;
    }

    /**
     * Gets the value of the kamLogin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKAMLogin() {
        return kamLogin;
    }

    /**
     * Sets the value of the kamLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKAMLogin(String value) {
        this.kamLogin = value;
    }

    /**
     * Gets the value of the contactListVO property.
     * 
     * @return
     *     possible object is
     *     {@link ContactListVO }
     *     
     */
    public ContactListVO getContactListVO() {
        return contactListVO;
    }

    /**
     * Sets the value of the contactListVO property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactListVO }
     *     
     */
    public void setContactListVO(ContactListVO value) {
        this.contactListVO = value;
    }

    /**
     * Gets the value of the addressList property.
     * 
     * @return
     *     possible object is
     *     {@link AddressList }
     *     
     */
    public AddressListVO getAddressListVO() {
        return addressListVO;
    }

    /**
     * Sets the value of the addressList property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressList }
     *     
     */
    public void setAddressListVO(AddressListVO value) {
        this.addressListVO = value;
    }

}

package sa.com.mobily.eportal.customerinfo.vo;

import java.util.Map;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.vo.BaseVO;
import sa.com.mobily.eportal.customerinfo.constant.CustSegmentConstant;
import sa.com.mobily.eportal.customerinfo.constant.LineTypeConstant;
import sa.com.mobily.eportal.customerinfo.constant.SubscriptionTypeConstant;

/**
 * @author r.agarwal.mit 
 * Mar 26, 2013
 * Description: This is an Value object which contains Customer information. And this can be used for both request and reply.
 */

public class CustomerInfoVO  extends BaseVO{

	//private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.CUSTOMER_INFO_SERVICE_LOGGER_NAME);
	private static final String className = CustomerInfoVO.class.getName();
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Request
	private String sourceId = null;// customer Mobile number(for consumer) or Billing Account Number(for WiMax/FTTH/IPTV)
	private String sourceSpecName = null;// "MSISDN"(for consumer) or "BillingAccountNumber"(for WiMax/FTTH/IPTV) 
	private int funcId=-1; //default is -1 for  Get Customer Information 
	
	//Reply
	private String AccountNumber = null;
	private String CPESerialNumber = null;
	private String statusCode = null;
	private String customerCategory = null;//P{Personal},O{Organization}
	private String customerType = null; //Consumer,Business
	private String packageCategory = null; //GSM or Connect
	private String customerIdType = null;
	private String customerIdNumber = null;
	private String customerGender = null;
	private String isCorporateKA = null;
	private String customerBSLId = null;
	private String MSISDN = null;
	private String packageId = null;
	private String isIUC = null;
	private String packageName = null;
	private String billingId = null;
	private String packageDescription = null;
	private String serviceId = null;
	private String serviceStatus = null;
	private String deviceSerialNumber = null;
	private String CPENumber = null;
	private String iSMSIM = null;
	private String CPEMACAddress = null;
	private String payType = null; //1 or 2 : 1 for prepaid and 2 for postpaid
	private String CPEDomain = null;
	private String CPEPassword = null;
	private String CPEUsername = null;
	private String packageServiceType = null; // GSM/Connect/LTE/WiMAX/FTTH/IPTV
	private String planCategory = null;
	private String customerActualSegment = null;
	private String isNewControl = null;
	
	private String corporatePackage = null;
	private String isMix = null;
	private String isControlPlus = null;
	private String isIPhone = null;
	private String packageBandwidth = null;
	private String masterBillingAccount = null;
	private String preferredLanguage = null;
	private String packageNetworkType = null;
	private String VIPCareTier = null;
	private String VIPCareStatus = null;
	private String PackageType = null;
	private String deviceType = null;
	private String multiSimMasterLineFlag = null;
	private String VIPSocialFlag = null;
	private String VIPGold = null;
	private String VIPHighSender = null;
	private String VIPPlatinumFlag = null;
	private String multiSIMFlag = null;
	private String MNPDonor = null;
	private String IMSI = null;
	private String corporateContactId = null;
	private String networkStatus = null;
	private String billPreference = null;
	private String barringStatusChangeDate = null;
	private String customerValueSegment = null;
	private String HUCCCheckTime = null;
	private String creditLimit = null;
	private String creditLimitChangeTime = null;
	private String usageLimit = null;
	private String creditScoring = null;
	private String StatusChangeDate = null;
	private String staticIPAddress = null;
	private String VPNId = null;
	private String circuitId = null;
	private String eliteFlag = null;
	private String segment = null;
	private String creditLimitAmount = null;
	private String CPEDCN = null;
	private String masterBillingAccountIndicator = null;
	private String vanityType = null;
	private String srviceBillingIndicator = null;//S : Service account, B : Billing account
	private String billCycle = null;
	private String barringReason = null;
	private String barringStatus = null;
	private String TEJWALYFlag = null;
	private String TEJWALYFlagUpdateDate = null;
	private String contractLanguage = null;
	private String contractId = null;
	private String lineNumber = null;
	private String duration = null;
	private String dealName = null;
	private String productLine = null;// to differentiate FTTH and IPTV
	
	//E-portal Application constants
	
	private int subscriptionType = -1;
	private int lineType = -1;
	private String  customerSegment;  //1  for consumer , 2 for business 
	
	private Map<String, String> otherCharacters = null;

	
	public static final String SOURCE_SPECIFICATION_NAME_MSISDN = "MSISDN";
	public static final String SOURCE_SPECIFICATION_NAME_BILLING_ACCOUNT_NUMBER = "BillingAccountNumber";
	public static final String SOURCE_SPECIFICATION_NAME_CPE = "CPE";
	
	public static final String PRODUCT_LINE_IPTV_BUNDLE = "IPTV Bundle";
	public static final String PACKAGE_SERVICE_TYPE_IPTV = "IPTV";
	public static final String PACKAGE_CATEGORY_CONNECT = "Connect";
	public static final String PACKAGE_CATEGORY_FV = "Fixed voice"; //SR22516 - ePortal Fixed Voice
	
	
	private String telephoneNumber = null;
	private String emailAddress = null;
	private String usrId;
	private String sCId;
	private String contactNumber = null;
	private String givenName = null;
	private String middleName = null;
	private String thirdName = null;
	private String familyName = null;
	private String occupation = null;
	
	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return AccountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		AccountNumber = accountNumber;
	}

	/**
	 * @return the cPESerialNumber
	 */
	public String getCPESerialNumber() {
		return CPESerialNumber;
	}

	/**
	 * @param serialNumber the cPESerialNumber to set
	 */
	public void setCPESerialNumber(String serialNumber) {
		CPESerialNumber = serialNumber;
	}

	/**
	 * @return the sourceId
	 */
	public String getSourceId() {
		return sourceId;
	}

	/**
	 * @param sourceId the sourceId to set
	 */
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	/**
	 * @return the sourceSpecName
	 */
	public String getSourceSpecName() {
		return sourceSpecName;
	}

	/**
	 * @param sourceSpecName the sourceSpecName to set
	 */
	public void setSourceSpecName(String sourceSpecName) {
		this.sourceSpecName = sourceSpecName;
	}

	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the customerCategory
	 */
	public String getCustomerCategory() {
		return customerCategory;
	}

	/**
	 * @param customerCategory the customerCategory to set
	 */
	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}

	/**
	 * @return the customerType
	 */
	public String getCustomerType() {
		return customerType;
	}

	/**
	 * @param customerType the customerType to set
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
		if(customerType!=null)
			if(customerType.equalsIgnoreCase("Consumer"))
				this.customerSegment=CustSegmentConstant.CUST_SEGMENT_CONSUMER;
			else if(customerType.equalsIgnoreCase("Business"))
				this.customerSegment=CustSegmentConstant.CUST_SEGMENT_BUSINESS;
	}

	/**
	 * @return the customerIdType
	 */
	public String getCustomerIdType() {
		return customerIdType;
	}

	/**
	 * @param customerIdType the customerIdType to set
	 */
	public void setCustomerIdType(String customerIdType) {
		this.customerIdType = customerIdType;
	}

	/**
	 * @return the customerIdNumber
	 */
	public String getCustomerIdNumber() {
		return customerIdNumber;
	}

	/**
	 * @param customerIdNumber the customerIdNumber to set
	 */
	public void setCustomerIdNumber(String customerIdNumber) {
		this.customerIdNumber = customerIdNumber;
	}

	/**
	 * @return the customerGender
	 */
	public String getCustomerGender() {
		return customerGender;
	}

	/**
	 * @param customerGender the customerGender to set
	 */
	public void setCustomerGender(String customerGender) {
		this.customerGender = customerGender;
	}

	/**
	 * @return the isCorporateKA
	 */
	public String getIsCorporateKA() {
		return isCorporateKA;
	}

	/**
	 * @param isCorporateKA the isCorporateKA to set
	 */
	public void setIsCorporateKA(String isCorporateKA) {
		this.isCorporateKA = isCorporateKA;
	}

	/**
	 * @return the customerBSLId
	 */
	public String getCustomerBSLId() {
		return customerBSLId;
	}

	/**
	 * @param customerBSLId the customerBSLId to set
	 */
	public void setCustomerBSLId(String customerBSLId) {
		this.customerBSLId = customerBSLId;
	}

	/**
	 * @return the mSISDN
	 */
	public String getMSISDN() {
		return MSISDN;
	}

	/**
	 * @param msisdn the mSISDN to set
	 */
	public void setMSISDN(String msisdn) {
		MSISDN = msisdn;
	}

	/**
	 * @return the packageId
	 */
	public String getPackageId() {
		return packageId;
	}

	/**
	 * @param packageId the packageId to set
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	/**
	 * @return the isIUC
	 */
	public String getIsIUC() {
		return isIUC;
	}

	/**
	 * @param isIUC the isIUC to set
	 */
	public void setIsIUC(String isIUC) {
		this.isIUC = isIUC;
	}

	/**
	 * @return the packageName
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * @param packageName the packageName to set
	 */
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	/**
	 * @return the billingId
	 */
	public String getBillingId() {
		return billingId;
	}

	/**
	 * @param billingId the billingId to set
	 */
	public void setBillingId(String billingId) {
		this.billingId = billingId;
	}

	/**
	 * @return the packageDescription
	 */
	public String getPackageDescription() {
		return packageDescription;
	}

	/**
	 * @param packageDescription the packageDescription to set
	 */
	public void setPackageDescription(String packageDescription) {
		this.packageDescription = packageDescription;
	}

	/**
	 * @return the serviceId
	 */
	public String getServiceId() {
		return serviceId;
	}

	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	/**
	 * @return the serviceStatus
	 */
	public String getServiceStatus() {
		return serviceStatus;
	}

	/**
	 * @param serviceStatus the serviceStatus to set
	 */
	public void setServiceStatus(String serviceStatus) {
		this.serviceStatus = serviceStatus;
	}

	/**
	 * @return the deviceSerialNumber
	 */
	public String getDeviceSerialNumber() {
		return deviceSerialNumber;
	}

	/**
	 * @param deviceSerialNumber the deviceSerialNumber to set
	 */
	public void setDeviceSerialNumber(String deviceSerialNumber) {
		this.deviceSerialNumber = deviceSerialNumber;
	}

	/**
	 * @return the cPENumber
	 */
	public String getCPENumber() {
		return CPENumber;
	}

	/**
	 * @param number the cPENumber to set
	 */
	public void setCPENumber(String number) {
		CPENumber = number;
	}

	/**
	 * @return the iSMSIM
	 */
	public String getISMSIM() {
		return iSMSIM;
	}

	/**
	 * @param ismsim the iSMSIM to set
	 */
	public void setISMSIM(String ismsim) {
		iSMSIM = ismsim;
	}

	/**
	 * @return the cPEMACAddress
	 */
	public String getCPEMACAddress() {
		return CPEMACAddress;
	}

	/**
	 * @param address the cPEMACAddress to set
	 */
	public void setCPEMACAddress(String address) {
		CPEMACAddress = address;
	}

	/**
	 * @return the payType
	 */
	public String getPayType() {
		return payType;
	}

	/**
	 * @param payType the payType to set
	 */
	public void setPayType(String payType) {
		this.payType = payType;
	}

	/**
	 * @return the cPEDomain
	 */
	public String getCPEDomain() {
		return CPEDomain;
	}

	/**
	 * @param domain the cPEDomain to set
	 */
	public void setCPEDomain(String domain) {
		CPEDomain = domain;
	}

	/**
	 * @return the cPEPassword
	 */
	public String getCPEPassword() {
		return CPEPassword;
	}

	/**
	 * @param password the cPEPassword to set
	 */
	public void setCPEPassword(String password) {
		CPEPassword = password;
	}

	/**
	 * @return the cPEUsername
	 */
	public String getCPEUsername() {
		return CPEUsername;
	}

	/**
	 * @param username the cPEUsername to set
	 */
	public void setCPEUsername(String username) {
		CPEUsername = username;
	}

	/**
	 * @return the packageServiceType
	 */
	public String getPackageServiceType() {
		return packageServiceType;
	}

	/**
	 * @param packageServiceType the packageServiceType to set
	 */
	public void setPackageServiceType(String packageServiceType) {// GSM/Connect/LTE/WiMAX/FTTH/IPTV
		//executionContext.audit(Level.INFO, "Before mapping backend with packageServiceType =["+packageServiceType +"]", className, "setPackageServiceType");
		
		//if(FormatterUtility.isEmpty(this.packageServiceType)){
			if(FormatterUtility.isEmpty(packageServiceType)){// Set Subscription & Line Type based on the Account Number Type
				//executionContext.audit(Level.INFO, "MSISDN =["+this.MSISDN +"] Service account number =["+this.AccountNumber+"]", className, "setPackageServiceType");
				String number = (FormatterUtility.isNotEmpty(this.MSISDN))?this.MSISDN:this.AccountNumber;
				//executionContext.audit(Level.INFO, "Number =["+number+"]", className, "setPackageServiceType");
				
				if(number.startsWith(ConstantIfc.PREFIX_FIXED_VOICE_ACCOUNT) && number.length() == 12){//SR22516 - ePortal Fixed Voice
					this.subscriptionType = SubscriptionTypeConstant.getSubscriptionType(SubscriptionTypeConstant.FV);  //Fixed Voice
					this.lineType = LineTypeConstant.getLineType(SubscriptionTypeConstant.FV); //Fixed Voice
					this.packageServiceType = SubscriptionTypeConstant.FV;
				} else if(number.startsWith(ConstantIfc.PREFIX_GSM_ACCOUNT) && number.length() == 12){
					this.subscriptionType = SubscriptionTypeConstant.getSubscriptionType(SubscriptionTypeConstant.GSM); // GSM
					this.lineType = LineTypeConstant.getLineType(SubscriptionTypeConstant.GSM); // GSM
					this.packageServiceType = SubscriptionTypeConstant.GSM;
				} else if(number.startsWith(ConstantIfc.PREFIX_CONNECT_ACCOUNT) || (number.startsWith(ConstantIfc.PREFIX_GSM_ACCOUNT) && number.length() > 12)){
					this.subscriptionType = SubscriptionTypeConstant.getSubscriptionType(SubscriptionTypeConstant.Connect); // Connect
					this.lineType = LineTypeConstant.getLineType(LineTypeConstant.LTE); // 3G
					this.packageServiceType = SubscriptionTypeConstant.LTE;
				} else if(number.startsWith(ConstantIfc.PREFIX_FTTH_ACCOUNT)){
					this.subscriptionType = SubscriptionTypeConstant.getSubscriptionType(SubscriptionTypeConstant.FTTH); // FTTH
					this.lineType = LineTypeConstant.getLineType(SubscriptionTypeConstant.FTTH); // FTTH
					this.packageServiceType = SubscriptionTypeConstant.FTTH;
				}
				
				
			}else{// Set Subscription & Line Type
				if(FormatterUtility.isEmpty(this.packageCategory)){
					if(SubscriptionTypeConstant.GSM.equalsIgnoreCase(packageServiceType)) {
						this.subscriptionType = SubscriptionTypeConstant.getSubscriptionType(packageServiceType); // GSM
						this.lineType = LineTypeConstant.getLineType(packageServiceType); // GSM
					} else if(SubscriptionTypeConstant.Connect.equalsIgnoreCase(packageServiceType)) {
						this.subscriptionType = SubscriptionTypeConstant.getSubscriptionType(packageServiceType); // Connect
						this.lineType = LineTypeConstant.getLineType(LineTypeConstant.three3G); // 3G
					} else if(LineTypeConstant.LTE.equalsIgnoreCase(packageServiceType)) {
						this.subscriptionType = SubscriptionTypeConstant.getSubscriptionType(SubscriptionTypeConstant.Connect); // Connect
						this.lineType = LineTypeConstant.getLineType(packageServiceType); // LTE
					} else if(SubscriptionTypeConstant.WiMAX.equalsIgnoreCase(packageServiceType)) {
						this.subscriptionType = SubscriptionTypeConstant.getSubscriptionType(packageServiceType); // WiMAX
						this.lineType = LineTypeConstant.getLineType(packageServiceType); // WiMAX
					} else if(SubscriptionTypeConstant.FTTH.equalsIgnoreCase(packageServiceType)) {
						this.subscriptionType = SubscriptionTypeConstant.getSubscriptionType(packageServiceType); // FTTH
						this.lineType = LineTypeConstant.getLineType(packageServiceType); // FTTH
					} else if(LineTypeConstant.IPTV.equalsIgnoreCase(packageServiceType)) {
						this.subscriptionType = SubscriptionTypeConstant.getSubscriptionType(SubscriptionTypeConstant.FTTH); // FTTH
						this.lineType = LineTypeConstant.getLineType(packageServiceType); // IPTV
					} else if(SubscriptionTypeConstant.FV.equalsIgnoreCase(packageServiceType)) {//SR22516 - ePortal Fixed Voice
						this.subscriptionType = SubscriptionTypeConstant.getSubscriptionType(SubscriptionTypeConstant.FV); // Fixed Voice
						this.lineType = LineTypeConstant.getLineType(packageServiceType); // IPTV
					}
				}
				
				if(FormatterUtility.isEmpty(this.packageCategory))
					this.packageServiceType = packageServiceType;
			}
		//}

		//executionContext.audit(Level.INFO, "after mapping backend with packageServiceType"+packageServiceType +" subscriptionType="+subscriptionType+" lineType="+lineType, className, "setPackageServiceType");
	}

	/**
	 * @return the planCategory
	 */
	public String getPlanCategory() {
		return planCategory;
	}

	/**
	 * @param planCategory the planCategory to set
	 */
	public void setPlanCategory(String planCategory) {
		this.planCategory = planCategory;
	}

	/**
	 * @return the customerActualSegment
	 */
	public String getCustomerActualSegment() {
		return customerActualSegment;
	}

	/**
	 * @param customerActualSegment the customerActualSegment to set
	 */
	public void setCustomerActualSegment(String customerActualSegment) {
		this.customerActualSegment = customerActualSegment;
	}

	/**
	 * @return the isNewControl
	 */
	public String getIsNewControl() {
		return isNewControl;
	}

	/**
	 * @param isNewControl the isNewControl to set
	 */
	public void setIsNewControl(String isNewControl) {
		this.isNewControl = isNewControl;
	}

	/**
	 * @return the packageCategory
	 */
	public String getPackageCategory() {
		return packageCategory;
	}

	/**
	 * @param packageCategory the packageCategory to set
	 */
	public void setPackageCategory(String packageCategory) {
		if(PACKAGE_CATEGORY_CONNECT.equalsIgnoreCase(packageCategory))
			this.setPackageServiceType(PACKAGE_CATEGORY_CONNECT);
		else if(PACKAGE_CATEGORY_FV.equalsIgnoreCase(packageCategory))
			this.setPackageServiceType(PACKAGE_CATEGORY_FV);
		
		this.packageCategory = packageCategory;
	}

	/**
	 * @return the corporatePackage
	 */
	public String getCorporatePackage() {
		return corporatePackage;
	}

	/**
	 * @param corporatePackage the corporatePackage to set
	 */
	public void setCorporatePackage(String corporatePackage) {
		this.corporatePackage = corporatePackage;
	}

	/**
	 * @return the isMix
	 */
	public String getIsMix() {
		return isMix;
	}

	/**
	 * @param isMix the isMix to set
	 */
	public void setIsMix(String isMix) {
		this.isMix = isMix;
	}

	/**
	 * @return the isControlPlus
	 */
	public String getIsControlPlus() {
		return isControlPlus;
	}

	/**
	 * @param isControlPlus the isControlPlus to set
	 */
	public void setIsControlPlus(String isControlPlus) {
		this.isControlPlus = isControlPlus;
	}

	/**
	 * @return the isIPhone
	 */
	public String getIsIPhone() {
		return isIPhone;
	}

	/**
	 * @param isIPhone the isIPhone to set
	 */
	public void setIsIPhone(String isIPhone) {
		this.isIPhone = isIPhone;
	}

	/**
	 * @return the packageBandwidth
	 */
	public String getPackageBandwidth() {
		return packageBandwidth;
	}

	/**
	 * @param packageBandwidth the packageBandwidth to set
	 */
	public void setPackageBandwidth(String packageBandwidth) {
		this.packageBandwidth = packageBandwidth;
	}

	/**
	 * @return the masterBillingAccount
	 */
	public String getMasterBillingAccount() {
		return masterBillingAccount;
	}

	/**
	 * @param masterBillingAccount the masterBillingAccount to set
	 */
	public void setMasterBillingAccount(String masterBillingAccount) {
		this.masterBillingAccount = masterBillingAccount;
	}

	/**
	 * @return the preferredLanguage
	 */
	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	/**
	 * @param preferredLanguage the preferredLanguage to set
	 */
	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	/**
	 * @return the packageNetworkType
	 */
	public String getPackageNetworkType() {
		return packageNetworkType;
	}

	/**
	 * @param packageNetworkType the packageNetworkType to set
	 */
	public void setPackageNetworkType(String packageNetworkType) {
		this.packageNetworkType = packageNetworkType;
	}

	/**
	 * @return the vIPCareTier
	 */
	public String getVIPCareTier() {
		return VIPCareTier;
	}

	/**
	 * @param careTier the vIPCareTier to set
	 */
	public void setVIPCareTier(String careTier) {
		VIPCareTier = careTier;
	}

	/**
	 * @return the vIPCareStatus
	 */
	public String getVIPCareStatus() {
		return VIPCareStatus;
	}

	/**
	 * @param careStatus the vIPCareStatus to set
	 */
	public void setVIPCareStatus(String careStatus) {
		VIPCareStatus = careStatus;
	}

	/**
	 * @return the packageType
	 */
	public String getPackageType() {
		return PackageType;
	}

	/**
	 * @param packageType the packageType to set
	 */
	public void setPackageType(String packageType) {
		PackageType = packageType;
	}

	/**
	 * @return the deviceType
	 */
	public String getDeviceType() {
		return deviceType;
	}

	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	/**
	 * @return the multiSimMasterLineFlag
	 */
	public String getMultiSimMasterLineFlag() {
		return multiSimMasterLineFlag;
	}

	/**
	 * @param multiSimMasterLineFlag the multiSimMasterLineFlag to set
	 */
	public void setMultiSimMasterLineFlag(String multiSimMasterLineFlag) {
		this.multiSimMasterLineFlag = multiSimMasterLineFlag;
	}

	/**
	 * @return the vIPSocialFlag
	 */
	public String getVIPSocialFlag() {
		return VIPSocialFlag;
	}

	/**
	 * @param socialFlag the vIPSocialFlag to set
	 */
	public void setVIPSocialFlag(String socialFlag) {
		VIPSocialFlag = socialFlag;
	}

	/**
	 * @return the vIPGold
	 */
	public String getVIPGold() {
		return VIPGold;
	}

	/**
	 * @param gold the vIPGold to set
	 */
	public void setVIPGold(String gold) {
		VIPGold = gold;
	}

	/**
	 * @return the vIPHighSender
	 */
	public String getVIPHighSender() {
		return VIPHighSender;
	}

	/**
	 * @param highSender the vIPHighSender to set
	 */
	public void setVIPHighSender(String highSender) {
		VIPHighSender = highSender;
	}

	/**
	 * @return the vIPPlatinumFlag
	 */
	public String getVIPPlatinumFlag() {
		return VIPPlatinumFlag;
	}

	/**
	 * @param platinumFlag the vIPPlatinumFlag to set
	 */
	public void setVIPPlatinumFlag(String platinumFlag) {
		VIPPlatinumFlag = platinumFlag;
	}

	/**
	 * @return the multiSIMFlag
	 */
	public String getMultiSIMFlag() {
		return multiSIMFlag;
	}

	/**
	 * @param multiSIMFlag the multiSIMFlag to set
	 */
	public void setMultiSIMFlag(String multiSIMFlag) {
		this.multiSIMFlag = multiSIMFlag;
	}

	/**
	 * @return the mNPDonor
	 */
	public String getMNPDonor() {
		return MNPDonor;
	}

	/**
	 * @param donor the mNPDonor to set
	 */
	public void setMNPDonor(String donor) {
		MNPDonor = donor;
	}

	/**
	 * @return the iMSI
	 */
	public String getIMSI() {
		return IMSI;
	}

	/**
	 * @param imsi the iMSI to set
	 */
	public void setIMSI(String imsi) {
		IMSI = imsi;
	}

	/**
	 * @return the corporateContactId
	 */
	public String getCorporateContactId() {
		return corporateContactId;
	}

	/**
	 * @param corporateContactId the corporateContactId to set
	 */
	public void setCorporateContactId(String corporateContactId) {
		this.corporateContactId = corporateContactId;
	}

	/**
	 * @return the networkStatus
	 */
	public String getNetworkStatus() {
		return networkStatus;
	}

	/**
	 * @param networkStatus the networkStatus to set
	 */
	public void setNetworkStatus(String networkStatus) {
		this.networkStatus = networkStatus;
	}

	/**
	 * @return the billPreference
	 */
	public String getBillPreference() {
		return billPreference;
	}

	/**
	 * @param billPreference the billPreference to set
	 */
	public void setBillPreference(String billPreference) {
		this.billPreference = billPreference;
	}

	/**
	 * @return the barringStatusChangeDate
	 */
	public String getBarringStatusChangeDate() {
		return barringStatusChangeDate;
	}

	/**
	 * @param barringStatusChangeDate the barringStatusChangeDate to set
	 */
	public void setBarringStatusChangeDate(String barringStatusChangeDate) {
		this.barringStatusChangeDate = barringStatusChangeDate;
	}

	/**
	 * @return the customerValueSegment
	 */
	public String getCustomerValueSegment() {
		return customerValueSegment;
	}

	/**
	 * @param customerValueSegment the customerValueSegment to set
	 */
	public void setCustomerValueSegment(String customerValueSegment) {
		this.customerValueSegment = customerValueSegment;
	}

	/**
	 * @return the hUCCCheckTime
	 */
	public String getHUCCCheckTime() {
		return HUCCCheckTime;
	}

	/**
	 * @param checkTime the hUCCCheckTime to set
	 */
	public void setHUCCCheckTime(String checkTime) {
		HUCCCheckTime = checkTime;
	}

	/**
	 * @return the creditLimit
	 */
	public String getCreditLimit() {
		return creditLimit;
	}

	/**
	 * @param creditLimit the creditLimit to set
	 */
	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}

	/**
	 * @return the creditLimitChangeTime
	 */
	public String getCreditLimitChangeTime() {
		return creditLimitChangeTime;
	}

	/**
	 * @param creditLimitChangeTime the creditLimitChangeTime to set
	 */
	public void setCreditLimitChangeTime(String creditLimitChangeTime) {
		this.creditLimitChangeTime = creditLimitChangeTime;
	}

	/**
	 * @return the usageLimit
	 */
	public String getUsageLimit() {
		return usageLimit;
	}

	/**
	 * @param usageLimit the usageLimit to set
	 */
	public void setUsageLimit(String usageLimit) {
		this.usageLimit = usageLimit;
	}

	/**
	 * @return the creditScoring
	 */
	public String getCreditScoring() {
		return creditScoring;
	}

	/**
	 * @param creditScoring the creditScoring to set
	 */
	public void setCreditScoring(String creditScoring) {
		this.creditScoring = creditScoring;
	}

	/**
	 * @return the statusChangeDate
	 */
	public String getStatusChangeDate() {
		return StatusChangeDate;
	}

	/**
	 * @param statusChangeDate the statusChangeDate to set
	 */
	public void setStatusChangeDate(String statusChangeDate) {
		StatusChangeDate = statusChangeDate;
	}

	/**
	 * @return the staticIPAddress
	 */
	public String getStaticIPAddress() {
		return staticIPAddress;
	}

	/**
	 * @param staticIPAddress the staticIPAddress to set
	 */
	public void setStaticIPAddress(String staticIPAddress) {
		this.staticIPAddress = staticIPAddress;
	}

	/**
	 * @return the vPNId
	 */
	public String getVPNId() {
		return VPNId;
	}

	/**
	 * @param id the vPNId to set
	 */
	public void setVPNId(String id) {
		VPNId = id;
	}

	/**
	 * @return the circuitId
	 */
	public String getCircuitId() {
		return circuitId;
	}

	/**
	 * @param circuitId the circuitId to set
	 */
	public void setCircuitId(String circuitId) {
		this.circuitId = circuitId;
	}

	/**
	 * @return the eliteFlag
	 */
	public String getEliteFlag() {
		return eliteFlag;
	}

	/**
	 * @param eliteFlag the eliteFlag to set
	 */
	public void setEliteFlag(String eliteFlag) {
		this.eliteFlag = eliteFlag;
	}

	/**
	 * @return the segment
	 */
	public String getSegment() {
		return segment;
	}

	/**
	 * @param segment the segment to set
	 */
	public void setSegment(String segment) {
		this.segment = segment;
	}

	/**
	 * @return the creditLimitAmount
	 */
	public String getCreditLimitAmount() {
		return creditLimitAmount;
	}

	/**
	 * @param creditLimitAmount the creditLimitAmount to set
	 */
	public void setCreditLimitAmount(String creditLimitAmount) {
		this.creditLimitAmount = creditLimitAmount;
	}

	/**
	 * @return the cPEDCN
	 */
	public String getCPEDCN() {
		return CPEDCN;
	}

	/**
	 * @param cpedcn the cPEDCN to set
	 */
	public void setCPEDCN(String cpedcn) {
		CPEDCN = cpedcn;
	}

	/**
	 * @return the masterBillingAccountIndicator
	 */
	public String getMasterBillingAccountIndicator() {
		return masterBillingAccountIndicator;
	}

	/**
	 * @param masterBillingAccountIndicator the masterBillingAccountIndicator to set
	 */
	public void setMasterBillingAccountIndicator(
			String masterBillingAccountIndicator) {
		this.masterBillingAccountIndicator = masterBillingAccountIndicator;
	}

	/**
	 * @return the vanityType
	 */
	public String getVanityType() {
		return vanityType;
	}

	/**
	 * @param vanityType the vanityType to set
	 */
	public void setVanityType(String vanityType) {
		this.vanityType = vanityType;
	}

	/**
	 * @return the srviceBillingIndicator
	 */
	public String getSrviceBillingIndicator() {
		return srviceBillingIndicator;
	}

	/**
	 * @param srviceBillingIndicator the srviceBillingIndicator to set
	 */
	public void setSrviceBillingIndicator(String srviceBillingIndicator) {
		this.srviceBillingIndicator = srviceBillingIndicator;
	}

	/**
	 * @return the billCycle
	 */
	public String getBillCycle() {
		return billCycle;
	}

	/**
	 * @param billCycle the billCycle to set
	 */
	public void setBillCycle(String billCycle) {
		this.billCycle = billCycle;
	}

	/**
	 * @return the barringReason
	 */
	public String getBarringReason() {
		return barringReason;
	}

	/**
	 * @param barringReason the barringReason to set
	 */
	public void setBarringReason(String barringReason) {
		this.barringReason = barringReason;
	}

	/**
	 * @return the barringStatus
	 */
	public String getBarringStatus() {
		return barringStatus;
	}

	/**
	 * @param barringStatus the barringStatus to set
	 */
	public void setBarringStatus(String barringStatus) {
		this.barringStatus = barringStatus;
	}

	/**
	 * @return the tEJWALYFlag
	 */
	public String getTEJWALYFlag() {
		return TEJWALYFlag;
	}

	/**
	 * @param flag the tEJWALYFlag to set
	 */
	public void setTEJWALYFlag(String flag) {
		TEJWALYFlag = flag;
	}

	/**
	 * @return the tEJWALYFlagUpdateDate
	 */
	public String getTEJWALYFlagUpdateDate() {
		return TEJWALYFlagUpdateDate;
	}

	/**
	 * @param flagUpdateDate the tEJWALYFlagUpdateDate to set
	 */
	public void setTEJWALYFlagUpdateDate(String flagUpdateDate) {
		TEJWALYFlagUpdateDate = flagUpdateDate;
	}

	/**
	 * @return the contractLanguage
	 */
	public String getContractLanguage() {
		return contractLanguage;
	}

	/**
	 * @param contractLanguage the contractLanguage to set
	 */
	public void setContractLanguage(String contractLanguage) {
		this.contractLanguage = contractLanguage;
	}

	/**
	 * @return the contractId
	 */
	public String getContractId() {
		return contractId;
	}

	/**
	 * @param contractId the contractId to set
	 */
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	/**
	 * @return the lineNumber
	 */
	public String getLineNumber() {
		return lineNumber;
	}

	/**
	 * @param lineNumber the lineNumber to set
	 */
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}

	/**
	 * @return the otherCharacters
	 */
	public Map<String, String> getOtherCharacters() {
		return otherCharacters;
	}

	/**
	 * @param otherCharacters the otherCharacters to set
	 */
	public void setOtherCharacters(Map<String, String> otherCharacters) {
		this.otherCharacters = otherCharacters;
	}

	/**
	 * @return the duration
	 */
	public String getDuration() {
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration(String duration) {
		this.duration = duration;
	}

	/**
	 * @return the dealName
	 */
	public String getDealName() {
		return dealName;
	}

	/**
	 * @param dealName the dealName to set
	 */
	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	public String getProductLine() {
		return productLine;
	}

	public void setProductLine(String productLine) {
		if(PRODUCT_LINE_IPTV_BUNDLE.equalsIgnoreCase(productLine)) {
			this.setPackageServiceType(PACKAGE_SERVICE_TYPE_IPTV);
		}
		else if (PACKAGE_CATEGORY_FV.equalsIgnoreCase(productLine)){//SR22516 - ePortal Fixed Voice
			this.setPackageServiceType(SubscriptionTypeConstant.FV);
		}
		this.productLine = productLine;
	}

	public int getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(int subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public int getLineType() {
		return lineType;
	}

	public void setLineType(int lineType) {
		this.lineType = lineType;
	}

	public String getCustomerSegment()
	{
		return customerSegment;
	}

	public void setCustomerSegment(String customerSegment)
	{
		this.customerSegment = customerSegment;
	}

	public int getFuncId()
	{
		return funcId;
	}

	public void setFuncId(int funcId)
	{
		this.funcId = funcId;
	}

	public String getTelephoneNumber()
	{
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber)
	{
		this.telephoneNumber = telephoneNumber;
	}

	public String getEmailAddress()
	{
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	public String getUsrId()
	{
		return usrId;
	}

	public void setUsrId(String usrId)
	{
		this.usrId = usrId;
	}

	public String getsCId()
	{
		return sCId;
	}

	public void setsCId(String sCId)
	{
		this.sCId = sCId;
	}

	/**
	 * @return the contactNumber
	 */
	public String getContactNumber()
	{
		return contactNumber;
	}

	/**
	 * @param contactNumber the contactNumber to set
	 */
	public void setContactNumber(String contactNumber)
	{
		this.contactNumber = contactNumber;
	}

	/**
	 * @return the givenName
	 */
	public String getGivenName()
	{
		return givenName;
	}

	/**
	 * @param givenName the givenName to set
	 */
	public void setGivenName(String givenName)
	{
		this.givenName = givenName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName()
	{
		return middleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	/**
	 * @return the thirdName
	 */
	public String getThirdName()
	{
		return thirdName;
	}

	/**
	 * @param thirdName the thirdName to set
	 */
	public void setThirdName(String thirdName)
	{
		this.thirdName = thirdName;
	}

	/**
	 * @return the familyName
	 */
	public String getFamilyName()
	{
		return familyName;
	}

	/**
	 * @param familyName the familyName to set
	 */
	public void setFamilyName(String familyName)
	{
		this.familyName = familyName;
	}

	/**
	 * @return the occupation
	 */
	public String getOccupation()
	{
		return occupation;
	}

	/**
	 * @param occupation the occupation to set
	 */
	public void setOccupation(String occupation)
	{
		this.occupation = occupation;
	}

	 
}

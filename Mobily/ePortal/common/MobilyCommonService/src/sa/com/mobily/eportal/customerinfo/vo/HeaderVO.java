package sa.com.mobily.eportal.customerinfo.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import sa.com.mobily.eportal.common.service.vo.BaseVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}FuncId"/>
 *         &lt;element ref="{}MsgVersion"/>
 *         &lt;element ref="{}RequestorChannelId"/>
 *         &lt;element ref="{}SrDate"/>
 *         &lt;element ref="{}RequestorUserId"/>
 *         &lt;element ref="{}ChannelTransId"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "funcId",
    "msgVersion",
    "requestorChannelId",
    "srDate",
    "requestorUserId",
    "channelTransId"
})
@XmlRootElement(name = "SR_HEADER")
public class HeaderVO extends BaseVO{

    @XmlElement(name = "FuncId", required = true)
    protected String funcId;
    @XmlElement(name = "MsgVersion", required = true)
    protected String msgVersion;
    @XmlElement(name = "RequestorChannelId", required = true)
    protected String requestorChannelId;
    @XmlElement(name = "SrDate", required = true)
    protected String srDate;
    @XmlElement(name = "RequestorUserId", required = true)
    protected String requestorUserId;
    @XmlElement(name = "ChannelTransId", required = true)
    protected String channelTransId;

    /**
     * Gets the value of the funcId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuncId() {
        return funcId;
    }

    /**
     * Sets the value of the funcId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuncId(String value) {
        this.funcId = value;
    }

    /**
     * Gets the value of the msgVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgVersion() {
        return msgVersion;
    }

    /**
     * Sets the value of the msgVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgVersion(String value) {
        this.msgVersion = value;
    }

    /**
     * Gets the value of the requestorChannelId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestorChannelId() {
        return requestorChannelId;
    }

    /**
     * Sets the value of the requestorChannelId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestorChannelId(String value) {
        this.requestorChannelId = value;
    }

    /**
     * Gets the value of the srDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrDate() {
        return srDate;
    }

    /**
     * Sets the value of the srDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrDate(String value) {
        this.srDate = value;
    }

    /**
     * Gets the value of the requestorUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestorUserId() {
        return requestorUserId;
    }

    /**
     * Sets the value of the requestorUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestorUserId(String value) {
        this.requestorUserId = value;
    }

    /**
     * Gets the value of the channelTransId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelTransId() {
        return channelTransId;
    }

    /**
     * Sets the value of the channelTransId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelTransId(String value) {
        this.channelTransId = value;
    }

}

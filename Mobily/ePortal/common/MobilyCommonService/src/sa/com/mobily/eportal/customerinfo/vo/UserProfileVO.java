package sa.com.mobily.eportal.customerinfo.vo;

import java.sql.Timestamp;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class UserProfileVO extends BaseVO
{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String san = null;
	private String productType =null;
	private String customerType =null;
	private String accountNumber =null;
	private String packageId = null;
	private String createdDate =null;
	private String updatedDate = null;
	private String idType = null;
	private String idNumber =null;
	private String accountStatus =null;
	private Timestamp createdTimeStamp = null;
	private String productNameEN = null;
	private String productNameAR = null;
	private String primeContactNumber = null;
	private String packageCategory;
	private String customerCategory;
	private String corporatePackage;
	private String planCategory;
	private String packageServiceType;
	private String  email;
	private byte[] customeProfileData;
	private String contactNumber;
	private Timestamp contactUpdateDate;
	public String getSan()
	{
		return san;
	}
	public void setSan(String san)
	{
		this.san = san;
	}
	public String getProductType()
	{
		return productType;
	}
	public void setProductType(String productType)
	{
		this.productType = productType;
	}
	public String getCustomerType()
	{
		return customerType;
	}
	public void setCustomerType(String customerType)
	{
		this.customerType = customerType;
	}
	public String getAccountNumber()
	{
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber)
	{
		this.accountNumber = accountNumber;
	}
	public String getPackageId()
	{
		return packageId;
	}
	public void setPackageId(String packageId)
	{
		this.packageId = packageId;
	}
	public String getCreatedDate()
	{
		return createdDate;
	}
	public void setCreatedDate(String createdDate)
	{
		this.createdDate = createdDate;
	}
	public String getUpdatedDate()
	{
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate)
	{
		this.updatedDate = updatedDate;
	}
	public String getIdType()
	{
		return idType;
	}
	public void setIdType(String idType)
	{
		this.idType = idType;
	}
	public String getIdNumber()
	{
		return idNumber;
	}
	public void setIdNumber(String idNumber)
	{
		this.idNumber = idNumber;
	}
	public String getAccountStatus()
	{
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus)
	{
		this.accountStatus = accountStatus;
	}
	public Timestamp getCreatedTimeStamp()
	{
		return createdTimeStamp;
	}
	public void setCreatedTimeStamp(Timestamp createdTimeStamp)
	{
		this.createdTimeStamp = createdTimeStamp;
	}
	public String getProductNameEN()
	{
		return productNameEN;
	}
	public void setProductNameEN(String productNameEN)
	{
		this.productNameEN = productNameEN;
	}
	public String getProductNameAR()
	{
		return productNameAR;
	}
	public void setProductNameAR(String productNameAR)
	{
		this.productNameAR = productNameAR;
	}
	public String getPrimeContactNumber()
	{
		return primeContactNumber;
	}
	public void setPrimeContactNumber(String primeContactNumber)
	{
		this.primeContactNumber = primeContactNumber;
	}
	public String getPackageCategory()
	{
		return packageCategory;
	}
	public void setPackageCategory(String packageCategory)
	{
		this.packageCategory = packageCategory;
	}
	public String getCustomerCategory()
	{
		return customerCategory;
	}
	public void setCustomerCategory(String customerCategory)
	{
		this.customerCategory = customerCategory;
	}
	public String getCorporatePackage()
	{
		return corporatePackage;
	}
	public void setCorporatePackage(String corporatePackage)
	{
		this.corporatePackage = corporatePackage;
	}
	public String getPlanCategory()
	{
		return planCategory;
	}
	public void setPlanCategory(String planCategory)
	{
		this.planCategory = planCategory;
	}
	public String getPackageServiceType()
	{
		return packageServiceType;
	}
	public void setPackageServiceType(String packageServiceType)
	{
		this.packageServiceType = packageServiceType;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public byte[] getCustomeProfileData()
	{
		return customeProfileData;
	}
	public void setCustomeProfileData(byte[] customeProfileData)
	{
		this.customeProfileData = customeProfileData;
	}
	/**
	 * @return the contactNumber
	 */
	public String getContactNumber()
	{
		return contactNumber;
	}
	/**
	 * @param contactNumber the contactNumber to set
	 */
	public void setContactNumber(String contactNumber)
	{
		this.contactNumber = contactNumber;
	}
	/**
	 * @return the contactUpdateDate
	 */
	public Timestamp getContactUpdateDate()
	{
		return contactUpdateDate;
	}
	/**
	 * @param contactUpdateDate the contactUpdateDate to set
	 */
	public void setContactUpdateDate(Timestamp contactUpdateDate)
	{
		this.contactUpdateDate = contactUpdateDate;
	}
	


}

package sa.com.mobily.eportal.customerinfo.xml;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.jaxb.JAXBUtilities;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.customerinfo.constant.TagIfc;
import sa.com.mobily.eportal.customerinfo.vo.AddressListVO;
import sa.com.mobily.eportal.customerinfo.vo.AddressVO;
import sa.com.mobily.eportal.customerinfo.vo.ContactListVO;
import sa.com.mobily.eportal.customerinfo.vo.ContactVO;
import sa.com.mobily.eportal.customerinfo.vo.CorporateProfileReplyVO;
import sa.com.mobily.eportal.customerinfo.vo.CorporateProfileRequestVO;
import sa.com.mobily.eportal.customerinfo.vo.HeaderVO;

/**
 * 
 * @author r.agarwal.mit
 * 
 */

public class CorporateProfileXMLHandler implements TagIfc
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	private final String className = CorporateProfileXMLHandler.class.getName();

	/**
	 * @date: September 11, 2013
	 * @author: r.agarwal.mit
	 * @description: Responsible to generate the request XML for Corporate
	 *               Profile
	 * @param requestVO
	 * @return xmlRequest
	 */

	public String generateCorporateProfileXML(String billingAccountNumber)
	{
		String method = "generateCorporateProfileXML";
		String xmlRequest = "";

		executionContext.audit(Level.INFO, "Started for billingAccountNumber: " + billingAccountNumber, className, method);

		// final ByteOutputStream outputStream = new ByteOutputStream();
		// XMLStreamWriter xmlStreamWriter = null;
		try
		{
			CorporateProfileRequestVO requestVO = new CorporateProfileRequestVO();
			HeaderVO headerVO = new HeaderVO();
			headerVO.setFuncId(ConstantsIfc.CORP_PROFILE_FUNC_ID);
			headerVO.setMsgVersion(ConstantsIfc.CORP_PROFILE_MSG_VERSION);
			headerVO.setRequestorChannelId(ConstantIfc.REQUESTOR_CHANNEL_ID);
			headerVO.setSrDate(new SimpleDateFormat(ConstantIfc.DATE_FORMAT).format(new Date()));
			headerVO.setRequestorUserId(ConstantsIfc.EPORTAL_USER);
			headerVO.setChannelTransId(ConstantIfc.Requestor_ChannelId + "_" + new SimpleDateFormat(ConstantIfc.DATE_FORMAT).format(new Date()));

			requestVO.setHeaderVO(headerVO);
			requestVO.setKeyType(ConstantsIfc.KEY_TYPE_BA);
			requestVO.setKeyValue(billingAccountNumber);

			// final XMLOutputFactory outputFactory =
			// XMLOutputFactory.newInstance();
			// xmlStreamWriter =
			// outputFactory.createXMLStreamWriter(outputStream, "UTF-8");
			//
			// JAXBContext jaxbContext =
			// JAXBContext.newInstance(CorporateProfileRequestVO.class);
			// Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			// jaxbMarshaller.marshal(requestVO, xmlStreamWriter);
			// outputStream.flush();
			//
			// xmlRequest = new String(outputStream.getBytes(), "UTF-8").trim();

			xmlRequest = JAXBUtilities.getInstance().marshal(requestVO);
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception while generating account poid xml" + e.getMessage(), className, method, e);
			throw new ServiceException(e.getMessage(), e);
		}
		finally
		{
			// if (xmlStreamWriter != null)
			// {
			// try
			// {
			// xmlStreamWriter.close();
			// }
			// catch (XMLStreamException e)
			// {
			// executionContext.log(Level.SEVERE, e.getMessage(), className,
			// method, e);
			// }
			// }
			// if (outputStream != null)
			// {
			// outputStream.close();
			// }
		}
		return xmlRequest;
	}

	/**
	 * @date: September 11, 2013
	 * @author: r.agarwal.mit
	 * @description: Responsible to parse the Corporate profile XML
	 * @param replyMessage
	 * @return CustomerInfoVO
	 */
	public CorporateProfileReplyVO parseCorporateProfileXML(String replyMessage)
	{
		String method = "parseCorporateProfileXML";
		CorporateProfileReplyVO replyVO = null;
		try
		{
			// JAXBContext jc =
			// JAXBContext.newInstance(CorporateProfileReplyVO.class);
			// Unmarshaller unmarshaller = jc.createUnmarshaller();
			// XMLInputFactory xmlif = XMLInputFactory.newInstance();

			// for testing -- START
			/*
			 * FileReader fr = new FileReader(
			 * "G:\\Portal 8\\Projects\\EAI New Interfaces\\Customer Profile\\capProfileReply.xml"
			 * ); XMLStreamReader xmler = xmlif.createXMLStreamReader(fr);
			 */

			// for Testing -- END

			// XMLStreamReader xmler = xmlif.createXMLStreamReader(new
			// StringReader(replyMessage));
			// replyVO = (CorporateProfileReplyVO)
			// unmarshaller.unmarshal(xmler);

			replyVO = (CorporateProfileReplyVO) JAXBUtilities.getInstance().unmarshal(CorporateProfileReplyVO.class, replyMessage);
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception while parsing corporate profile xml" + e.getMessage(), className, method, e);

			throw new ServiceException(e.getMessage(), e);
		}
		return replyVO;
	}

	public String generateCorporateProfileXMLV01(String billingAccountNumber)
	{
		String method = "generateCorporateProfileXMLV01";
		String xmlRequest = "";

		executionContext.audit(Level.INFO, "Started for billingAccountNumber: " + billingAccountNumber, className, method);

		try
		{
			sa.com.mobily.eportal.corporateprofile.jaxb.request.MOBILYBSLSR requestVO = new sa.com.mobily.eportal.corporateprofile.jaxb.request.MOBILYBSLSR();
			sa.com.mobily.eportal.corporateprofile.jaxb.request.SRHEADER headerVO = new sa.com.mobily.eportal.corporateprofile.jaxb.request.SRHEADER();
			headerVO.setFuncId(ConstantsIfc.CORP_PROFILE_FUNC_ID);
			headerVO.setMsgVersion(ConstantsIfc.CORP_PROFILE_MSG_VERSION_0001);
			headerVO.setRequestorChannelId(ConstantIfc.REQUESTOR_CHANNEL_ID);
			headerVO.setSrDate(new SimpleDateFormat(ConstantIfc.DATE_FORMAT).format(new Date()));
			headerVO.setRequestorUserId(ConstantsIfc.EPORTAL_USER);
			requestVO.setSRHEADER(headerVO);
			requestVO.setCustomerAccountNo(billingAccountNumber);
			requestVO.setOperation(ConstantsIfc.CORP_PROFILE_MSG_OPERATION_PROFILE);

			xmlRequest = JAXBUtilities.getInstance().marshal(requestVO);
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception while generating account poid xml" + e.getMessage(), className, method, e);
			throw new ServiceException(e.getMessage(), e);
		}
		finally
		{
		}
		return xmlRequest;
	}

	public sa.com.mobily.eportal.corporateprofile.jaxb.reply.MOBILYSRMESSAGE parseCorporateProfileXMLV01(String replyMessage)
	{
		String method = "parseCorporateProfileXMLV01";
		sa.com.mobily.eportal.corporateprofile.jaxb.reply.MOBILYSRMESSAGE replyVO = null;
		try
		{
			replyVO = (sa.com.mobily.eportal.corporateprofile.jaxb.reply.MOBILYSRMESSAGE) JAXBUtilities.getInstance().unmarshal(
					sa.com.mobily.eportal.corporateprofile.jaxb.reply.MOBILYSRMESSAGE.class, replyMessage);
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception while parsing corporate profile xml" + e.getMessage(), className, method, e);
			throw new ServiceException(e.getMessage(), e);
		}
		return replyVO;
	}

	public static void main(String agrs[])
	{

		CorporateProfileXMLHandler handler = new CorporateProfileXMLHandler();

		String requestXML = handler.generateCorporateProfileXML("101123456789");
//		System.out.println("requestXML=" + requestXML);

		String replyMessage = "";
		try
		{
			/*
			 * String requestXml = handler.generateAccountPOIDXML(requestVO);
			 * System.out.println("requestXml::" + requestXml);
			 */

			CorporateProfileReplyVO replyVO = handler.parseCorporateProfileXML(replyMessage);
			if (replyVO != null)
			{
//				System.out.println("msg format=" + replyVO.getHeaderVO().getFuncId());
//				System.out.println("getCompanyName=" + replyVO.getCompanyName());
//				System.out.println("getCustomertype=" + replyVO.getCustomertype());
//				System.out.println("getCustomercategory=" + replyVO.getCustomercategory());
//
//				System.out.println("getCompanyidnumber=" + replyVO.getCompanyidnumber());
//				System.out.println("getBillingAccountNumber=" + replyVO.getBillingAccountNumber());
//				System.out.println("getCustomerAccountNumber=" + replyVO.getCustomerAccountNumber());

				AddressListVO addressListVO = replyVO.getAddressListVO();
				if (addressListVO != null)
				{
					List<AddressVO> addressVOList = addressListVO.getAddressVOList();
					for (AddressVO addressVO : addressVOList)
					{
//						System.out.println("getCompanyAddress=" + addressVO.getCompanyAddress());
//						System.out.println("getCompanyCity=" + addressVO.getCompanyCity());
//						System.out.println("getCompanyCountry=" + addressVO.getCompanyCountry());
//						System.out.println("getCompanyPOBox=" + addressVO.getCompanyPOBox());
//						System.out.println("getCompanyPostalCode=" + addressVO.getCompanyPostalCode());
					}
				}

				ContactListVO contactListVO = replyVO.getContactListVO();
				if (contactListVO != null)
				{
					List<ContactVO> contactVOList = contactListVO.getContactVOList();
					for (ContactVO contactVO : contactVOList)
					{
//						System.out.println("getAuthorizedPersonFirstName=" + contactVO.getAuthorizedPersonFirstName());
//						System.out.println("getAuthorizedPersonMiddleName=" + contactVO.getAuthorizedPersonMiddleName());
//						System.out.println("getAuthorizedPersonLastName=" + contactVO.getAuthorizedPersonLastName());
//						System.out.println("getAuthorizedPersonGender=" + contactVO.getAuthorizedPersonGender());
//						System.out.println("getAuthorizedPersonEmail=" + contactVO.getAuthorizedPersonEmail());
//
//						System.out.println("getAuthorizedPersonFax=" + contactVO.getAuthorizedPersonFax());
//						System.out.println("getAuthorizedPersonMobile=" + contactVO.getAuthorizedPersonMobile());
//						System.out.println("getAuthorizedPersonNationality=" + contactVO.getAuthorizedPersonNationality());
//						System.out.println("getAuthorizedPersonTelephone=" + contactVO.getAuthorizedPersonTelephone());
					}
				}
			}
		}
		catch (Exception e)
		{
//			System.out.println("here=" + e.getMessage());
//			e.printStackTrace();
		}
	}
}
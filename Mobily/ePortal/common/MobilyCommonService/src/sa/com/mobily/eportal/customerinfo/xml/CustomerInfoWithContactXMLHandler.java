package sa.com.mobily.eportal.customerinfo.xml;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.constant.ExceptionConstantIfc;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.RandomGenerator;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;
import sa.com.mobily.eportal.common.service.util.jaxb.JAXBUtilities;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.customerinfo.constant.TagIfc;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.request.Body;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.request.Characteristic;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.request.CharacteristicValue;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.request.MsgRqHdr;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.request.RetrieveCustomerProductConfiguration;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.request.SourceCriteria;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.request.Specification;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.request.TargetCriteria;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.Agreement;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.AgreementItem;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.ContactMedium;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.Customer;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.CustomerConfiguration;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.Individual;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.Party;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.PartyChoice;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.PartyExtensions;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.PartyIdentification;
import sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.RetrieveCustomerProductConfigurationRs;

import sa.com.mobily.eportal.customerinfo.vo.CustomerInfoVO;

/**
 * 
 * @author r.agarwal.mit
 * 
 */

public class CustomerInfoWithContactXMLHandler implements TagIfc
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.CUSTOMER_INFO_SERVICE_LOGGER_NAME);

	private static final String className = CustomerInfoWithContactXMLHandler.class.getName();

	/**
	 * @date: April 05, 2013
	 * @author: r.agarwal.mit
	 * @description: Responsible to generate the request XML for Customer
	 *               Profile
	 * @param requestVO
	 * @return xmlRequest <retrieveCustomerProductConfiguration> <MsgRqHdr>
	 *         <FuncId>40200200</FuncId> <RqMode>0</RqMode>
	 *         <RqUID>20130312112500101</RqUID> <LangPref>En</LangPref>
	 *         <ClientDt>2010-10-27T16:50:03</ClientDt>
	 *         <CallerService>SubmitProductOrder</CallerService> </MsgRqHdr>
	 *         <Body> <sourceCriteria> <Id>1000112459996085</Id> <Specification>
	 *         <Name>BillingAccountNumber</Name> </Specification>
	 *         </sourceCriteria> </Body> </retrieveCustomerProductConfiguration>
	 */

	public String generateCustomerInfoWithContactXML(CustomerInfoVO requestVO)
	{
		String method = "generateCustomerInfoWithContactXML";
		String xmlRequest = "";

		if (requestVO == null)
		{
			// log.fatal("CustomerProfileXMLHandler > generateCustomerProfileXML : ActiveLinesInquiryVO is null.");
			throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, ConstantsIfc.INVALID_REQUEST);
		}

		/*LogEntryVO logEntryVO = new LogEntryVO(Level.INFO, "CustomerInfoWithContactXMLHandler > generateCustomerInfoWithContactXML : start for account: " + requestVO.getSourceId(), className,
				method, null);
		executionContext.log(logEntryVO);*/

//		final ByteOutputStream outputStream = new ByteOutputStream();
//		XMLStreamWriter xmlStreamWriter = null;
		try
		{

			RetrieveCustomerProductConfiguration objectAsXML = new RetrieveCustomerProductConfiguration();
			MsgRqHdr msgRqHdr = new MsgRqHdr();
			msgRqHdr.setFuncId(ConstantsIfc.RCPC_CONTACTNUBER_FUNC_ID);
			msgRqHdr.setRqMode(""+ConstantsIfc.REQ_MODE_0);
			
			//msgRqHdr.setRqUID(new BigDecimal(new SimpleDateFormat(ConstantsIfc.DATE_FORMAT).format(new Date()))); 
			msgRqHdr.setRqUID(""+new BigDecimal(System.currentTimeMillis()+RandomGenerator.generateNumericRandom(3))); //PBI000000023851
			msgRqHdr.setLangPref(requestVO.getPreferredLanguage());
			msgRqHdr.setClientDt(new SimpleDateFormat(ConstantsIfc.DATE_FORMAT).format(new Date()));
			
			objectAsXML.setMsgRqHdr(msgRqHdr);

			Body body = new Body();

			SourceCriteria sourceCriteria = new SourceCriteria();
			sourceCriteria.setId(requestVO.getSourceId());

			Specification specification = new Specification();
			specification.setName(requestVO.getSourceSpecName());

			sourceCriteria.setSpecification(specification);

			body.setSourceCriteria(sourceCriteria);
			
			TargetCriteria targetCriteria = new TargetCriteria();
				targetCriteria.setName(ConstantsIfc.AllContractsWithProduct);
				List<CharacteristicValue> cvList = new ArrayList<CharacteristicValue>();
				
				CharacteristicValue cv = new CharacteristicValue();
				Characteristic chart = new Characteristic(); 
				chart.setName(ConstantsIfc.InquiryLevel);
				cv.setValue("2");
				cv.setCharacteristic(chart);
				cvList.add(cv);
				cv = new CharacteristicValue();
				chart = new Characteristic(); 
				chart.setName(ConstantsIfc.Status);
				cv.setValue(ConstantsIfc.Active);
				cv.setCharacteristic(chart);
				cvList.add(cv);
				cv = new CharacteristicValue();
				chart = new Characteristic(); 
				chart.setName(ConstantsIfc.PartyInquiryLevel);
				cv.setValue("1");
				cv.setCharacteristic(chart);
				cvList.add(cv);
				targetCriteria.getCharacteristicValue().addAll(cvList);
				//targetCriteria.setCharacteristicValue(cvList);
				body.setTargetCriteria(targetCriteria);
			objectAsXML.setBody(body);

//			final XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
//			xmlStreamWriter = outputFactory.createXMLStreamWriter(outputStream, "UTF-8");
//
//			JAXBContext jaxbContext = JAXBContext.newInstance(RetrieveCustomerProductConfiguration.class);
//			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
//			jaxbMarshaller.marshal(objectAsXML, xmlStreamWriter);
//			outputStream.flush();
//
//			xmlRequest = new String(outputStream.getBytes(), "UTF-8").trim();
			
			
			
			
			xmlRequest = JAXBUtilities.getInstance().marshal(objectAsXML);
			/*logEntryVO = new LogEntryVO(Level.INFO, "CustomerInfoWithContactXMLHandler > generateCustomerInfoWithContactXML > xmlRequest" + xmlRequest, className, method, null);
			executionContext.log(logEntryVO);*/
		}
		catch (Exception e)
		{
			/*logEntryVO = new LogEntryVO(Level.SEVERE, "CustomerInfoWithContactXMLHandler > generateCustomerInfoWithContactXML > Exception while generating customer info xml" + e.getMessage(), className, method, e);
			executionContext.log(logEntryVO);*/
			throw new ServiceException(e.getMessage(), e);
		}
		finally
		{
//			if (xmlStreamWriter != null)
//			{
//				try
//				{
//					xmlStreamWriter.close();
//				}
//				catch (XMLStreamException e)
//				{
//					logEntryVO = new LogEntryVO(Level.SEVERE, e.getMessage(), className, method, e);
//					executionContext.log(logEntryVO);
//				}
//			}
//			if (outputStream != null)
//			{
//				outputStream.close();
//			}
		}
		return xmlRequest;
	}

	/**
	 * @date: April 05 2013
	 * @author: r.agarwal.mit
	 * @description: Responsible to parse the Customer profile XML
	 * @param replyMessage
	 * @return CustomerInfoVO
	 */
	public CustomerInfoVO parseCustomerInfoWithContactXML(String replyMessage)
	{
		executionContext.audit(Level.INFO, "CustomerInfoWithContactXMLHandler > parseCustomerInfoWithContactXML > start", className, "getCustProductProfileByMsisdn");
		String methodName = "parseCustomerInfoXML";
		CustomerInfoVO customerInfoVO = null;
		try
		{
//			JAXBContext jc = JAXBContext.newInstance(RetrieveCustomerProductConfigurationRs.class);
//			Unmarshaller unmarshaller = jc.createUnmarshaller();
//			XMLInputFactory xmlif = XMLInputFactory.newInstance();

			// for testing -- START
			// FileReader fr = new
			// FileReader("G:\\Portal 8\\RCPC response.xml");
			// XMLStreamReader xmler = xmlif.createXMLStreamReader(fr);
			// for Testing -- END

//			XMLStreamReader xmler = xmlif.createXMLStreamReader(new StringReader(replyMessage));
//			RetrieveCustomerProductConfigurationRs obj = (RetrieveCustomerProductConfigurationRs) unmarshaller.unmarshal(xmler);
			
			//for testing...
			JAXBContext jaxbContext = JAXBContext.newInstance(RetrieveCustomerProductConfigurationRs.class);
		    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		     
		    //We had written this file in marshalling example
		   // RetrieveCustomerProductConfigurationRs obj = (RetrieveCustomerProductConfigurationRs) jaxbUnmarshaller.unmarshal(new File("D://xml/RCPCResponse.xml"));
		    //for testing... ends here
			
			
			RetrieveCustomerProductConfigurationRs obj = (RetrieveCustomerProductConfigurationRs) JAXBUtilities.getInstance().unmarshal(RetrieveCustomerProductConfigurationRs.class, replyMessage);

			if (obj != null)
			{
				customerInfoVO = new CustomerInfoVO();
				customerInfoVO.setStatusCode(obj.getMsgRsHdr() != null ? obj.getMsgRsHdr().getStatusCode() : "");
				executionContext.audit(Level.INFO, "CustomerInfoWithContactXMLHandler > parseCustomerInfoWithContactXML > obj.getBody()"+obj.getBody(), className, "getCustProductProfileByMsisdn");
				
				if (obj.getBody() != null)
				{
					executionContext.audit(Level.INFO, "CustomerInfoWithContactXMLHandler > parseCustomerInfoWithContactXML > getBody not null", className, "getCustProductProfileByMsisdn");
					CustomerConfiguration customerConfiguration = obj.getBody().getCustomerConfiguration();
					if (customerConfiguration != null)
					{
						executionContext.audit(Level.INFO, "CustomerInfoWithContactXMLHandler > parseCustomerInfoWithContactXML > customerConfiguration not null", className, "getCustProductProfileByMsisdn");
						
						Customer customer = customerConfiguration.getCustomer();
						Agreement agreement = customerConfiguration.getAgreement();

						// Fetch Items related to customer
						if (customer != null)
						{
							executionContext.audit(Level.INFO, "CustomerInfoWithContactXMLHandler > parseCustomerInfoWithContactXML > customer not null", className, "getCustProductProfileByMsisdn");
							Party party = customer.getParty();
							if (party != null)
							{
								executionContext.audit(Level.INFO, "CustomerInfoWithContactXMLHandler > parseCustomerInfoWithContactXML > party not null", className, "getCustProductProfileByMsisdn");
								sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.Specification specification = party.getSpecification();
								if (specification != null)
								{
									executionContext.audit(Level.INFO, "CustomerInfoWithContactXMLHandler > parseCustomerInfoWithContactXML > specification not null", className, "getCustProductProfileByMsisdn");
									// Customer Category
									customerInfoVO.setCustomerCategory(specification.getCategory());
									// Customer Type
									customerInfoVO.setCustomerType(specification.getType());
								}

								PartyIdentification partyIdentification = party.getPartyIdentification();
								if (partyIdentification != null)
								{
									// Customer ID Number
									customerInfoVO.setCustomerIdNumber(partyIdentification.getId());
									sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.Specification partySpecification = partyIdentification.getSpecification();
									// Customer ID Type
									customerInfoVO.setCustomerIdType(partySpecification.getType());
								}

								PartyExtensions partyExtensions = party.getPartyExtensions();
								if (partyExtensions != null)
								{
									PartyChoice partyChoice = partyExtensions.getPartyChoice();
									if (partyChoice != null)
									{
										Individual individual = partyChoice.getIndividual();
										// Customer Gender
										//customerInfoVO.setCustomerGender(individual != null ? individual.getGender() : "");
										/*if(individual!=null){
											sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.Name name=  individual.getName();
											if(name!=null){
												List<Serializable> content = name.getContent();
												for (Object o : content) {
												    String data="";
												    if (o instanceof String) {
												        //System.out.println(((String)o).trim());
												        data=((String)o).trim();
												        System.out.println("Names>>"+data);
												    } else if (o instanceof JAXBElement) {
												        //System.out.println(((JAXBElement)o).getValue().toString());
												        data=((JAXBElement)o).getValue().toString();
												        System.out.println("Names::"+data);
												   }
												   //do things you need to do with data
												}
											}
											
										}*/
									}
								}
								
								ContactMedium contactMedium = party.getContactMedium();
								if(contactMedium !=null){
									if(contactMedium.getEmailContact()!=null)
										customerInfoVO.setEmailAddress(FormatterUtility.checkNull(contactMedium.getEmailContact().getEmailAddress()));
									if(contactMedium.getTelephoneContact()!=null){
										executionContext.audit(Level.INFO, "CustomerInfoWithContactXMLHandler > parseCustomerInfoWithContactXML > contactMedium.getTelephoneContact() not null", className, "getCustProductProfileByMsisdn");
										customerInfoVO.setTelephoneNumber(FormatterUtility.checkNull(contactMedium.getTelephoneContact().getNumber()));
									}
										
								}

								/*List<sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.CharacteristicValue> list = party.getCharacteristicValue();

								if (list != null && list.size() > 0)
								{
									Method[] methodArray = customerInfoVO.getClass().getMethods();
									Method method = null;
									for (CharacteristicValue characteristicValue : list)
									{
										if (methodArray != null)
										{
											int count = methodArray.length;
											for (int k = 0; k < count; k++)
											{
												method = methodArray[k];
												if (method != null && method.getName().equalsIgnoreCase("set" + characteristicValue.getCharacteristic().getName()))
												{
													method.invoke(customerInfoVO, new Object[] { characteristicValue.getValue() });
													break;
												}
											}
										}
									}
								}*/

							}
						}

						// Fetch Items related to Agreement
						if (agreement != null)
						{
							//System.out.println("agreement not null");
							AgreementItem agreementItem = agreement.getAgreementItem();
							if (agreementItem != null)
							{//System.out.println("agreementItem not null");
								List<sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.CharacteristicValue> list = agreementItem.getCharacteristicValue();

								if (list != null && list.size() > 0)
								{
									//System.out.println("list size is ::"+list.size());
									Method[] methodArray = customerInfoVO.getClass().getMethods();
									Method method = null;
									for (sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.CharacteristicValue characteristicValue : list)
									{
										if (methodArray != null)
										{
											//System.out.println("characteristicValue.getCharacteristic().getName()::"+characteristicValue.getCharacteristic().getName());
											int count = methodArray.length;
											for (int k = 0; k < count; k++)
											{
												method = methodArray[k];
												
												if (method != null && method.getName().equalsIgnoreCase("set" + characteristicValue.getCharacteristic().getName()))
												{
													//System.out.println("characteristicValue.getCharacteristic().getName() in  if");
													method.invoke(customerInfoVO, new Object[] { characteristicValue.getValue() });
													break;
												}
											}
										}
									}
								}
							}
							
							sa.com.mobily.eportal.customerinfo.jaxb.customerinfowithcontactnumber.response.CustomerAccountId  customerAccountId = agreement.getCustomerAccountId();
							 if(customerAccountId != null){
								 customerInfoVO.setAccountNumber(customerAccountId.getId());
							 }
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			executionContext.audit(Level.SEVERE, "CustomerInfoWithContactXMLHandler > parseCustomerInfoWithContactXML > Exception while parsing customer info xml" + e.getMessage(), className, "getCustProductProfileByMsisdn");
			//System.out.println("parseCustomerInfoWithContactXML > Exception"+e.getMessage());
			/*LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, "Exception while parsing customer info xml" + e.getMessage(), className, methodName, e);
			executionContext.log(logEntryVO);*/
			throw new ServiceException(e.getMessage(), e);
		}
		executionContext.audit(Level.INFO, "CustomerInfoWithContactXMLHandler > parseCustomerInfoWithContactXML > end > customerInfoVO::"+customerInfoVO, className, "getCustProductProfileByMsisdn");
		return customerInfoVO;
	}
	/*public static void main(String[] args)
	{
		//System.out.println("RS:"+);
		parseCustomerInfoWithContactXML("test");
	}*/
}

package sa.com.mobily.eportal.customerinfo.xml;

import java.lang.reflect.Method;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.jaxb.JAXBUtilities;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.customerinfo.constant.TagIfc;
import sa.com.mobily.eportal.customerinfo.jaxb.favnumber.request.Body;
import sa.com.mobily.eportal.customerinfo.jaxb.favnumber.request.Characteristic;
import sa.com.mobily.eportal.customerinfo.jaxb.favnumber.request.CharacteristicValue;
import sa.com.mobily.eportal.customerinfo.jaxb.favnumber.request.CustomerOrder;
import sa.com.mobily.eportal.customerinfo.jaxb.favnumber.request.MsgRqHdr;
import sa.com.mobily.eportal.customerinfo.jaxb.favnumber.request.Product;
import sa.com.mobily.eportal.customerinfo.jaxb.favnumber.request.ProductOrderItem;
import sa.com.mobily.eportal.customerinfo.jaxb.favnumber.request.RetrieveProductRulesRq;
import sa.com.mobily.eportal.customerinfo.vo.FavoriteNumberVO;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;

/**
 * 
 * @author r.agarwal.mit
 * 
 */

public class FavoriteNumberXMLHandler implements TagIfc
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	private static final String className = FavoriteNumberXMLHandler.class.getName();

	/**
	 * @date: September 10, 2013
	 * @author: r.agarwal.mit
	 * @description: Responsible to generate the request XML for Favorite Number
	 *               by package id
	 * @param requestVO
	 * @return xmlRequest
	 *         <retrieveProductRulesRq><MsgRqHdr><RqUID>SR_5277072163<
	 *         /RqUID><MsgType
	 *         >REQUEST</MsgType><FuncId>44400000</FuncId><RqMode></RqMode
	 *         ><SCId>Broker</
	 *         SCId><LangPref>E</LangPref><CustId>EAI</CustId><MSISDN
	 *         >966560085059
	 *         </MSISDN><BillingAcctNum>13</BillingAcctNum><ClientDt
	 *         >2013-03-11T00
	 *         :00:47.358635</ClientDt></MsgRqHdr><Body><customerOrder
	 *         ><ProductOrderItem
	 *         ><Action></Action><Product><CharacteristicValue>
	 *         <Characteristic><Name
	 *         >PackageId</Name></Characteristic><Value>1490
	 *         </Value></CharacteristicValue
	 *         ></Product></ProductOrderItem></customerOrder
	 *         ></Body></retrieveProductRulesRq>
	 */

	public String generateFavoriteNumberXML(String packageId)
	{
		String method = "generateFavoriteNumberXML";
		String xmlRequest = "";

		executionContext.audit(Level.INFO, "FavoriteNumberXMLHandler > generateFavoriteNumberXML : start for packageId: " + packageId, className, method);

//		final ByteOutputStream outputStream = new ByteOutputStream();
//		XMLStreamWriter xmlStreamWriter = null;
		try
		{
			RetrieveProductRulesRq objectAsXML = new RetrieveProductRulesRq();
			MsgRqHdr msgRqHdr = new MsgRqHdr();
			msgRqHdr.setRqUID("SR_" + new BigDecimal(new SimpleDateFormat(ConstantsIfc.DATE_FORMAT).format(new Date())));
			msgRqHdr.setMsgType(ConstantsIfc.FAV_NUMBER_MSG_TYPE);
			msgRqHdr.setFuncId(ConstantsIfc.FAV_NUMBER_FUNC_ID);
			msgRqHdr.setSCId(ConstantsIfc.FAV_NUMBER_SC_ID);
			msgRqHdr.setLangPref(ConstantsIfc.LANG_E);
			msgRqHdr.setCustId(ConstantsIfc.FAV_NUMBER_CUST_ID);
			msgRqHdr.setClientDt(new SimpleDateFormat(ConstantsIfc.DATE_FORMAT).format(new Date()));

			objectAsXML.setMsgRqHdr(msgRqHdr);

			Body body = new Body();

			CustomerOrder customerOrder = new CustomerOrder();
			ProductOrderItem productOrderItem = new ProductOrderItem();
			productOrderItem.setAction(ConstantsIfc.ACTION_ADD);

			Product product = new Product();

			CharacteristicValue characteristicValue = new CharacteristicValue();
			characteristicValue.setValue(packageId);

			Characteristic characteristic = new Characteristic();
			characteristic.setName(TAG_PACKAGE_ID);
			characteristicValue.setCharacteristic(characteristic);

			product.setCharacteristicValue(characteristicValue);

			productOrderItem.setProduct(product);
			customerOrder.setProductOrderItem(productOrderItem);
			body.setCustomerOrder(customerOrder);

			objectAsXML.setBody(body);

//			final XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
//			xmlStreamWriter = outputFactory.createXMLStreamWriter(outputStream, "UTF-8");
//
//			JAXBContext jaxbContext = JAXBContext.newInstance(RetrieveProductRulesRq.class);
//			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
//			jaxbMarshaller.marshal(objectAsXML, xmlStreamWriter);
//			outputStream.flush();
//
//			xmlRequest = new String(outputStream.getBytes(), "UTF-8").trim();

			xmlRequest = JAXBUtilities.getInstance().marshal(objectAsXML);

		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception while generating favorite number xml" + e.getMessage(), className, method, e);
			throw new ServiceException(e.getMessage(), e);
		}
		finally
		{
//			if (xmlStreamWriter != null)
//			{
//				try
//				{
//					xmlStreamWriter.close();
//				}
//				catch (XMLStreamException e)
//				{
//					executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
//				}
//			}
//			if (outputStream != null)
//			{
//				outputStream.close();
//			}
		}
		return xmlRequest;
	}

	/**
	 * @date: September 10, 2013
	 * @author: r.agarwal.mit
	 * @description: Responsible to parse the Favorite Number XML
	 * @param replyMessage
	 * @return FavoriteNumberVO
	 */
	public FavoriteNumberVO parseFavoriteNumberXML(String replyMessage)
	{
		String methodName = "parseFavoriteNumberXML";
		FavoriteNumberVO favoriteNumberVO = new FavoriteNumberVO();
		try
		{

			Document doc = XMLUtility.parseXMLString(replyMessage);
			Node rootNode = doc.getDocumentElement();
			NodeList nl = rootNode.getChildNodes();

			for (int j = 0; j < nl.getLength(); j++)
			{
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				String l_szNodeValue = null;
				if (l_Node.getFirstChild() != null)
				{
					l_szNodeValue = l_Node.getFirstChild().getNodeValue();
				}
				if (sa.com.mobily.eportal.common.service.constant.TagIfc.TAG_MsgRsHdr.equalsIgnoreCase(p_szTagName))
				{
					NodeList headerNodeList = l_Node.getChildNodes();
					for (int i = 0; headerNodeList != null && i < headerNodeList.getLength(); i++)
					{
						Element s_Node = (Element) headerNodeList.item(i);
						String s_szNodeTagName = s_Node.getTagName();

						if (sa.com.mobily.eportal.common.service.constant.TagIfc.TAG_StatusCode.equalsIgnoreCase(s_szNodeTagName))
						{
							String statusCodeNodeValue = null;
							if (s_Node.getFirstChild() != null)
							{
								statusCodeNodeValue = s_Node.getFirstChild().getNodeValue();

								favoriteNumberVO.setStatusCode(statusCodeNodeValue);

							}
						}
					}
				}
				else if (TAG_BODY.equalsIgnoreCase(p_szTagName))
				{
					NodeList bodyNodeList = l_Node.getChildNodes();
					for (int i = 0; bodyNodeList != null && i < bodyNodeList.getLength(); i++)
					{

						Element b_Node = (Element) bodyNodeList.item(i);

						NodeList customerAccountNodeList = b_Node.getChildNodes();

						for (int m = 0; customerAccountNodeList != null && m < customerAccountNodeList.getLength(); m++)
						{
							Element p_Node = (Element) customerAccountNodeList.item(m);
							String p_szNodeTagName = p_Node.getTagName();

							if (TAG_PRODUCT_ORDER_ITEM.equalsIgnoreCase(p_szNodeTagName))
							{
								NodeList specificationNodeList = p_Node.getChildNodes();
								for (int n = 0; specificationNodeList != null && n < specificationNodeList.getLength(); n++)
								{
									Element specification_Node = (Element) specificationNodeList.item(n);
									String specification_szNodeTagName = specification_Node.getTagName();

									if (TAG_PRODUCT.equalsIgnoreCase(specification_szNodeTagName))
									{

										NodeList productOrderNodeList = specification_Node.getChildNodes();

										for (int p = 0; productOrderNodeList != null && p < productOrderNodeList.getLength(); p++)
										{
											Element product_Node = (Element) productOrderNodeList.item(p);
											String product_szNodeTagName = product_Node.getTagName();
											if (sa.com.mobily.eportal.common.service.constant.TagIfc.TAG_CharacteristicValue.equalsIgnoreCase(product_szNodeTagName))
											{

												String name = "";

												NodeList characteristicValueNodeList = product_Node.getChildNodes();
												for (int q = 0; characteristicValueNodeList != null && q < characteristicValueNodeList.getLength(); q++)
												{
													Element characteristicValue_Node = (Element) characteristicValueNodeList.item(q);
													String characteristicValue_szNodeTagName = characteristicValue_Node.getTagName();
													if (sa.com.mobily.eportal.common.service.constant.TagIfc.TAG_Characteristic.equalsIgnoreCase(characteristicValue_szNodeTagName))
													{

														NodeList nameNodeList = characteristicValue_Node.getChildNodes();
														for (int s = 0; nameNodeList != null && s < nameNodeList.getLength(); s++)
														{
															Element name_Node = (Element) nameNodeList.item(s);
															String name_szNodeTagName = name_Node.getTagName();
															if (TAG_NAME.equalsIgnoreCase(name_szNodeTagName))
															{

																String nameValue = null;
																if (name_Node.getFirstChild() != null)
																{
																	nameValue = name_Node.getFirstChild().getNodeValue();
																	name = nameValue;
																	// customerProfileReplyVO.setAccountType(typeNodeValue);

																}
															}
															else if (sa.com.mobily.eportal.common.service.constant.TagIfc.TAG_FAVORITE_NUMBER_TYPE
																	.equalsIgnoreCase(name_szNodeTagName))
															{
																String typeValue = null;
																if (name_Node.getFirstChild() != null)
																{
																	typeValue = name_Node.getFirstChild().getNodeValue();
																	// customerProfileReplyVO.setAccountType(typeNodeValue);
																}

															}
														}

													}
													else if (sa.com.mobily.eportal.common.service.constant.TagIfc.TAG_VALUE.equalsIgnoreCase(characteristicValue_szNodeTagName))
													{

														String value = null;
														if (characteristicValue_Node.getFirstChild() != null)
														{
															value = characteristicValue_Node.getFirstChild().getNodeValue();
															// charactersMap.put(name,
															// value);
															// customerProfileReplyVO.setAccountType(typeNodeValue);

															Method[] methodArray = favoriteNumberVO.getClass().getMethods();
															Method method = null;

															if (methodArray != null)
															{
																int count = methodArray.length;
																for (int k = 0; k < count; k++)
																{
																	method = methodArray[k];
																	if (method != null && method.getName().equalsIgnoreCase("set" + name))
																	{
																		method.invoke(favoriteNumberVO, new Object[] { value });
																		break;
																	}
																}
															}

														}
													}
												}

											}
										}
									}
								}
							}
						}
					}
				}
			}

			/*
			 * JAXBContext jc =
			 * JAXBContext.newInstance(RetrieveProductRulesRs.class);
			 * Unmarshaller unmarshaller = jc.createUnmarshaller();
			 * XMLInputFactory xmlif = XMLInputFactory.newInstance();
			 * 
			 * // for testing -- START FileReader fr = new FileReader(
			 * "G:\\Portal 8\\Projects\\EAI New Interfaces\\RetrieveProductRulesRs-FavoriteNumberEligibility.xml"
			 * ); XMLStreamReader xmler = xmlif.createXMLStreamReader(fr); //
			 * for Testing -- END System.out.println("here"); //String str =
			 * "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><NS1:retrieveProductRulesRs xmlns:NS1=\"http://www.ejada.com\"><MsgRsHdr xmlns=\"http://www.ejada.com\"><StatusCode>I000000</StatusCode><RqUID>SR_5277072163</RqUID></MsgRsHdr><Body><NS2:customerOrder xmlns:NS2=\"http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/fulfillment/schema/CustomerOrderMessage\"><ProductOrderItem><Product><CharacteristicValue><Characteristic><Name>PackageId</Name></Characteristic><Value>1490</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>NationalFavNo</Name><Type>Resource</Type></Characteristic><Value>0</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>NationalFavNoCode</Name></Characteristic><Value>1</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>InternationalFavNo</Name><Type>Resource</Type></Characteristic><Value>2</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>InternationalFavNoCode</Name></Characteristic><Value>2</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>FixedLineFavNo</Name><Type>Resource</Type></Characteristic><Value>0</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>FixedLineFavNoCode</Name></Characteristic><Value>3</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>OnNetFavNo</Name><Type>Resource</Type></Characteristic><Value>0</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>OnNetFavNoCode</Name></Characteristic><Value>4</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>CrossNetFavNo</Name><Type>Resource</Type></Characteristic><Value>0</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>CrossNetFavNoCode</Name></Characteristic><Value>5</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>InternationalSMS</Name><Type>Resource</Type></Characteristic><Value>0</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>InternationalSMSCode</Name></Characteristic><Value>6</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>FavCountry</Name><Type>Resource</Type></Characteristic><Value>0</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>FavCountryCode</Name></Characteristic><Value>7</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>MCC</Name><Type>Resource</Type></Characteristic><Value>0</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>MCCCode</Name></Characteristic><Value>8</Value></CharacteristicValue></Product></ProductOrderItem></NS2:customerOrder></Body></NS1:retrieveProductRulesRs>"
			 * ; //String str =
			 * "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><NS1:retrieveProductRulesRs xmlns:NS1=\"http://www.ejada.com\"><MsgRsHdr xmlns=\"http://www.ejada.com\"><StatusCode>I000000</StatusCode><RqUID>SR_5277072163</RqUID></MsgRsHdr><Body xmlns=\"http://www.ejada.com\"><test>asdfasdf</test></Body></NS1:retrieveProductRulesRs>"
			 * ;
			 * 
			 * //XMLStreamReader xmler = xmlif.createXMLStreamReader(new
			 * StringReader(str)); RetrieveProductRulesRs obj =
			 * (RetrieveProductRulesRs) unmarshaller.unmarshal(xmler);
			 * System.out.println("obj="+obj); if (obj != null) {
			 * favoriteNumberVO = new FavoriteNumberVO();
			 * favoriteNumberVO.setStatusCode(obj.getMsgRsHdr() != null ?
			 * obj.getMsgRsHdr().getStatusCode() : "");
			 * System.out.println("getMsgRsHdr()="
			 * +obj.getMsgRsHdr().getRqUID());
			 * System.out.println("obj.getBody()="+obj.getBody()); if
			 * (obj.getBody() != null) { Object bodyObj =
			 * obj.getBody().getCustomerOrder(); if (bodyObj != null) {
			 * System.out.println("body class"+bodyObj.getClass().getName());
			 * Customer customer = bodyObj.getClass()Customer();
			 * 
			 * // Fetch Items related to Agreement if (agreement != null) {
			 * AgreementItem agreementItem = agreement.getAgreementItem(); if
			 * (agreementItem != null) { List<CharacteristicValue> list =
			 * agreementItem.getCharacteristicValue();
			 * 
			 * if (list != null && list.size() > 0) { Method[] methodArray =
			 * customerInfoVO.getClass().getMethods(); Method method = null; for
			 * (CharacteristicValue characteristicValue : list) { if
			 * (methodArray != null) { int count = methodArray.length; for (int
			 * k = 0; k < count; k++) { method = methodArray[k]; if (method !=
			 * null && method.getName().equalsIgnoreCase("set" +
			 * characteristicValue.getCharacteristic().getName())) {
			 * method.invoke(customerInfoVO, new Object[] {
			 * characteristicValue.getValue() }); break; } } } } } } } } } }
			 */
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception while parsing customer info xml" + e.getMessage(), className, methodName, e);
			throw new ServiceException(e.getMessage(), e);
		}
		return favoriteNumberVO;
	}

	public static void main(String agrs[])
	{

		/*
		 * requestVO.setSourceId("12345678912345");
		 * requestVO.setSourceSpecName("BillingAccountNumber");
		 */

		String str = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><NS1:retrieveProductRulesRs xmlns:NS1=\"http://www.ejada.com\"><MsgRsHdr xmlns=\"http://www.ejada.com\"><StatusCode>I000000</StatusCode><RqUID>SR_5277072163</RqUID></MsgRsHdr><Body><NS2:customerOrder xmlns:NS2=\"http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/fulfillment/schema/CustomerOrderMessage\"><ProductOrderItem><Product><CharacteristicValue><Characteristic><Name>PackageId</Name></Characteristic><Value>1490</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>NationalFavNo</Name><Type>Resource</Type></Characteristic><Value>0</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>NationalFavNoCode</Name></Characteristic><Value>1</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>InternationalFavNo</Name><Type>Resource</Type></Characteristic><Value>2</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>InternationalFavNoCode</Name></Characteristic><Value>2</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>FixedLineFavNo</Name><Type>Resource</Type></Characteristic><Value>0</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>FixedLineFavNoCode</Name></Characteristic><Value>3</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>OnNetFavNo</Name><Type>Resource</Type></Characteristic><Value>0</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>OnNetFavNoCode</Name></Characteristic><Value>4</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>CrossNetFavNo</Name><Type>Resource</Type></Characteristic><Value>0</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>CrossNetFavNoCode</Name></Characteristic><Value>5</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>InternationalSMS</Name><Type>Resource</Type></Characteristic><Value>0</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>InternationalSMSCode</Name></Characteristic><Value>6</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>FavCountry</Name><Type>Resource</Type></Characteristic><Value>0</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>FavCountryCode</Name></Characteristic><Value>7</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>MCC</Name><Type>Resource</Type></Characteristic><Value>0</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>MCCCode</Name></Characteristic><Value>8</Value></CharacteristicValue></Product></ProductOrderItem></NS2:customerOrder></Body></NS1:retrieveProductRulesRs>";

		FavoriteNumberXMLHandler handler = new FavoriteNumberXMLHandler();

		FavoriteNumberVO favoriteNumberVO = handler.parseFavoriteNumberXML(str);
		/*System.out.println(favoriteNumberVO.getPackageId());
		System.out.println(favoriteNumberVO.getNationalFavNo());
		System.out.println(favoriteNumberVO.getMCCCode());
		System.out.println(favoriteNumberVO.getCrossNetFavNoCode());*/
		/*
		 * favoriteNumberVO.setPackageId("1234");
		 * 
		 * String xml = handler.generateFavoriteNumberXML(favoriteNumberVO);
		 * System.out.println("xml11="+xml);
		 */

		/*
		 * try { String requestXml = handler.generateCustomerInfoXML(requestVO);
		 * System.out.println("requestXml::" + requestXml);
		 * 
		 * CustomerInfoVO replyVO = handler.parseCustomerInfoXML(""); if
		 * (replyVO != null) { System.out.println("customer id=" +
		 * replyVO.getCustomerIdNumber());
		 * System.out.println("customer id type=" +
		 * replyVO.getCustomerIdType());
		 * 
		 * System.out.println("customer gender=" + replyVO.getCustomerGender());
		 * System.out.println("customer category=" +
		 * replyVO.getCustomerCategory());
		 * 
		 * System.out.println("billing id=" + replyVO.getBillingId());
		 * System.out.println("CPE number=" + replyVO.getCPENumber()); }
		 * 
		 * } catch (Exception e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */
	}
}
package sa.com.mobily.eportal.google.captcha.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Contains the configuration information of application.
 * 
 * @author r.agarwal.mit
 * @version 1.0
 */
public class AppConfig
{
	/**
	 * Property file name
	 */
	protected static final String propertyFileName = "config/google_config.properties";

	/**
	 * Instance of AppConfig
	 */
	protected static AppConfig config = null;

	/**
	 * Contains all <code>properties</code> in a Properties instance
	 */
	protected Properties properties;

	/**
	 * Creates an AppConfig instance.
	 */
	protected AppConfig()
	{
		properties = new Properties();
		ClassLoader sc = Thread.currentThread().getContextClassLoader();
		InputStream stream = sc.getResourceAsStream(propertyFileName);

		if (stream == null)
		{
			throw new IllegalArgumentException("Could not load " + propertyFileName + ".Please make sure that it is in CLASSPATH.");
		}

		try
		{
			properties.load(stream);
		}
		catch (IOException e)
		{
			IllegalStateException ex = new IllegalStateException("An error occurred when reading from the input stream");
			ex.initCause(e);
			throw ex;
		}
		finally
		{
			try
			{
				stream.close();
			}
			catch (IOException e)
			{
				IllegalStateException ex = new IllegalStateException("An I/O error occured while closing the stream");
				ex.initCause(e);
				throw ex;
			}
		}
	}

	/**
	 * Gets the instance of <code>AppConfig</code>.
	 * 
	 * @return instance of AppConfig
	 */
	public static AppConfig getInstance()
	{

		if (config == null)
			config = new AppConfig();

		return config;
	}

	/**
	 * Gets the value of a property based on key provided.
	 * 
	 * @param key
	 *            The key for which value needs to be retrieved from
	 * @return The value for the key in <code>app.properties</code>. Returns
	 *         null if the value does not exist in the property file.
	 * 
	 */
	public String get(String key)
	{

		if (properties == null || key == null)
			return null;

		return (String) properties.get(key);
	}
}
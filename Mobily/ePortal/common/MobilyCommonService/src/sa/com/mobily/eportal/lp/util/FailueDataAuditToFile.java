/**
 * 
 */
package sa.com.mobily.eportal.lp.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

/**
 * @author n.gundluru.mit
 *
 */
public class FailueDataAuditToFile {
	
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	private static final String className = FailueDataAuditToFile.class.getName();
	private static final DateFormat sdf = new SimpleDateFormat("dd:MM:yyyy HH:mm:ss");
	
	
	public static boolean uploadDataToFile(String fileName, FailureDataAuditVO failureDataAuditVoObj) {
		boolean uploadStatus = false;
		File targetFile = null;
		String method = "uploadDataToFile";
		BufferedWriter buffWriter = null;
		FileWriter fileWriter = null;
		StringBuffer content = new StringBuffer();
		Date currentDate = new Date();
        
		executionContext.log(Level.INFO, "Called....", className, method);
		
		try {

			if(FormatterUtility.isNotEmpty(fileName) && failureDataAuditVoObj != null){
				
				
				if(FailueDataAuditConstants.FILE_NOTIFICATION_ACTION.equalsIgnoreCase(fileName)){ //write to LP_NOTIFICATION_ACTION_LP.txt
					
					//Create the content String
						content.append(sdf.format(currentDate));
							content.append(">>");
							content.append("USER_TRANS_ID:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getUserTransId()));
							content.append(",NOTIFICATION_TRANS_ID:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getNotificationTransId()));
							content.append(",CREATED_DATE:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getCreateDate()));
							content.append(",ACTION_STATUS:");
							content.append(failureDataAuditVoObj.getActionStatus());
							content.append(",REASON:");
							content.append(",TYPE:");
							content.append(failureDataAuditVoObj.getType());
							content.append("\n");
					
					targetFile = new File(FailueDataAuditConstants.FAILURE_DATA_AUDIT_FILE_PATH + FailueDataAuditConstants.FILE_NOTIFICATION_ACTION);
					//targetFile = new File("D:\\Nagendra\\LPAuditFailures\\"+fileName);
					
					executionContext.log(Level.INFO, "'"+targetFile +"' exist =["+targetFile.exists()+"]", className, method);
					
					if(targetFile.exists()){ //File already Present
						
						Writer output;
						output = new BufferedWriter(new FileWriter(targetFile, true));  //Open the file in APPEND mode
						output.append(content.toString());
						output.close();
						
						uploadStatus = true;
						
					}else{ //Create new file and add the content
						
						//Set the File permissions
							targetFile.setExecutable(true);
							targetFile.setReadable(true);
							targetFile.setWritable(true);
					      
						fileWriter = new FileWriter(targetFile);
						buffWriter = new BufferedWriter(fileWriter);
						buffWriter.write(content.toString());
						
						uploadStatus = true;
					}
					
				} else if(FailueDataAuditConstants.FILE_SERVICE_TRANSACTION.equalsIgnoreCase(fileName)){ //write to LP_SERVICE_TRANSACTION_TBL.txt
					
					//Create the content String
						content.append(sdf.format(currentDate));
							content.append(">>");
							content.append("SERVICE_TRANS_ID:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getServiceTransId()));
							content.append(",CREATED_DATE:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getCreateDate()));
							content.append(",STATUS:");
							content.append(failureDataAuditVoObj.getActionStatus());
							content.append(",UPDATE_REASON:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getReason()));
							content.append("\n");
					
					targetFile = new File(FailueDataAuditConstants.FAILURE_DATA_AUDIT_FILE_PATH + FailueDataAuditConstants.FILE_SERVICE_TRANSACTION);
					//targetFile = new File("D:\\Nagendra\\LPAuditFailures\\"+fileName);
					
					executionContext.log(Level.INFO, "'"+targetFile +"' exist =["+targetFile.exists()+"]", className, method);
					
					if(targetFile.exists()){ //File already Present
						
						Writer output;
						output = new BufferedWriter(new FileWriter(targetFile, true));  //Open the file in APPEND mode
						output.append(content.toString());
						output.close();
						
						uploadStatus = true;
						
					}else{ //Create new file and add the content
						
						//Set the File permissions
							targetFile.setExecutable(true);
							targetFile.setReadable(true);
							targetFile.setWritable(true);
					      
						fileWriter = new FileWriter(targetFile);
						buffWriter = new BufferedWriter(fileWriter);
						buffWriter.write(content.toString());
						
						uploadStatus = true;
					}
					
				} else if(FailueDataAuditConstants.FILE_USER_TRANSACTION.equalsIgnoreCase(fileName)){ //write to LP_USER_TRANSACTION_TBL.txt
					
					//Create the content String
						content.append(sdf.format(currentDate));
							content.append(">>");
							content.append("USER_TRANS_ID:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getUserTransId()));
							content.append(",PAYMENT_TRANS_ID:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getPaymentTransId()));
							content.append(",SERVICE_TRANS_ID:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getServiceTransId()));
							content.append(",CREATED_DATE:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getCreateDate()));
							content.append(",STATUS:");
							content.append(failureDataAuditVoObj.getActionStatus());
							content.append(",UPDATE_REASON:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getReason()));
							content.append(",ACCOUNT_NUMBER:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getMsisdn()));
							content.append("\n");
					
					targetFile = new File(FailueDataAuditConstants.FAILURE_DATA_AUDIT_FILE_PATH + FailueDataAuditConstants.FILE_USER_TRANSACTION);
					//targetFile = new File("D:\\Nagendra\\LPAuditFailures\\"+fileName);
					
					executionContext.log(Level.INFO, "'"+targetFile +"' exist =["+targetFile.exists()+"]", className, method);
					
					if(targetFile.exists()){ //File already Present
						
						Writer output;
						output = new BufferedWriter(new FileWriter(targetFile, true));  //Open the file in APPEND mode
						output.append(content.toString());
						output.close();
						
						uploadStatus = true;
						
					}else{ //Create new file and add the content
						
						//Set the File permissions
							targetFile.setExecutable(true);
							targetFile.setReadable(true);
							targetFile.setWritable(true);
					      
						fileWriter = new FileWriter(targetFile);
						buffWriter = new BufferedWriter(fileWriter);
						buffWriter.write(content.toString());
						
						uploadStatus = true;
					}
				} else if(FailueDataAuditConstants.FILE_PAYMENT_TRANSACTION.equalsIgnoreCase(fileName)){ //write to LP_PAYMENT_TRANS_TBL.txt
					
					//Create the content String
						content.append(sdf.format(currentDate));
							content.append(">>");
							content.append("PAYMENT_TRANS_ID:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getPaymentTransId()));
							content.append(",CREATED_DATE:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getCreateDate()));
							content.append(",STATUS:");
							content.append(failureDataAuditVoObj.getActionStatus());
							content.append(",UPDATE_REASON:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getReason()));
							content.append(",ACCOUNT_NUMBER:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getMsisdn()));
							content.append(",AMOUNT:");
							content.append(FormatterUtility.checkNull(failureDataAuditVoObj.getPaymentAmount()));
							content.append("\n");
					
					targetFile = new File(FailueDataAuditConstants.FAILURE_DATA_AUDIT_FILE_PATH + FailueDataAuditConstants.FILE_PAYMENT_TRANSACTION);
					//targetFile = new File("D:\\Nagendra\\LPAuditFailures\\"+fileName);
					
					executionContext.log(Level.INFO, "'"+targetFile +"' exist =["+targetFile.exists()+"]", className, method);
					
					if(targetFile.exists()){ //File already Present
						
						Writer output;
						output = new BufferedWriter(new FileWriter(targetFile, true));  //Open the file in APPEND mode
						output.append(content.toString());
						output.close();
						
						uploadStatus = true;
						
					}else{ //Create new file and add the content
						
						//Set the File permissions
							targetFile.setExecutable(true);
							targetFile.setReadable(true);
							targetFile.setWritable(true);
					      
						fileWriter = new FileWriter(targetFile);
						buffWriter = new BufferedWriter(fileWriter);
						buffWriter.write(content.toString());
						
						uploadStatus = true;
					}
				}
				
			}

		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "Exception while writing files to shared location::"	+ e.getMessage(), className, method, e);
			uploadStatus = false;
			
		}finally {
			try {
				if (buffWriter != null)
					buffWriter.close();

				if (fileWriter != null)
					fileWriter.close();

			} catch (Exception ex) {
				executionContext.log(Level.SEVERE, "Exception while closing files ::"	+ ex.getMessage(), className, method, ex);
				uploadStatus = false;
			}

		}
		
		executionContext.log(Level.INFO, "'"+fileName +"' uploadStatus ="+uploadStatus, className, method);
		
		return uploadStatus;
	}
	
	
	
	
	public static void main(String args[]){
		
		FailureDataAuditVO failureDataAuditVoObj = new FailureDataAuditVO();
		
		FailueDataAuditToFile.uploadDataToFile(FailueDataAuditConstants.FILE_NOTIFICATION_ACTION, failureDataAuditVoObj);
		FailueDataAuditToFile.uploadDataToFile(FailueDataAuditConstants.FILE_SERVICE_TRANSACTION, failureDataAuditVoObj);
		FailueDataAuditToFile.uploadDataToFile(FailueDataAuditConstants.FILE_USER_TRANSACTION, failureDataAuditVoObj);
	}
	
	
}

/*
 * Copyright 2007 Soren Davidsen, Tanesha Networks
 *  
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package sa.com.mobily.eportal.recaptcha;

import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;
import sa.com.mobily.eportal.recaptcha.http.HttpLoader;
import sa.com.mobily.eportal.recaptcha.http.SimpleHttpLoader;

public class ReCaptchaImpl implements ReCaptcha
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.RECAPTCHA_SERVICE_LOGGER_NAME);

	private final String className = ReCaptchaImpl.class.getName();

	public static final String PROPERTY_THEME = "theme";

	public static final String PROPERTY_TABINDEX = "tabindex";

	public static final String HTTP_SERVER = "http://api.recaptcha.net";

	public static final String HTTPS_SERVER = "https://api-secure.recaptcha.net";

	public static final String VERIFY_URL = "http://api-verify.recaptcha.net/verify";

	// private String privateKey = "6LcYnN8SAAAAAIBueR8RJ-PjOsu-cUZIH9RAaEC1";
	// // for 10.14.11.233
	// private String publicKey = "6LcYnN8SAAAAAPIV0y-oirVKxIWxo7pd60Ny9J2C";//
	// for 10.14.11.233
	// private String privateKey = "6Lf1QuASAAAAAF5XFWfxXj4EKllqvQANArSIgDuI";
	// // for 234
	// private String publicKey = "6Lf1QuASAAAAAPI-G3HPEHPsUvvLAPAxlhGTonyo";//
	// for 234
	// private String privateKey = "6Le-zuMSAAAAAJu0HE9fnQ3t9s5mGlsLn4hoOCwg";
	// // for 10.14.170.167
	// private String publicKey = "6Le-zuMSAAAAALZr-TzaBKa4ksGiG0cWnfiLeO1L";//
	// for 10.14.170.167

	// for 10.14.170.163
	private String privateKey = "6LcUy-QSAAAAAHEverzRLEKwXfrL0yEN86ZF6DeN";
	
	// for 10.14.170.163
	private String publicKey = "6LcUy-QSAAAAAN7skw2z2ZXfroH0zRlmPz2uDPCx";

	private String recaptchaServer = HTTP_SERVER;

	private boolean includeNoscript = false;

	private HttpLoader httpLoader = new SimpleHttpLoader();

	public void setPrivateKey(String privateKey)
	{
		this.privateKey = privateKey;
	}

	public void setPublicKey(String publicKey)
	{
		this.publicKey = publicKey;
	}

	public void setRecaptchaServer(String recaptchaServer)
	{
		this.recaptchaServer = recaptchaServer;
	}

	public void setIncludeNoscript(boolean includeNoscript)
	{
		this.includeNoscript = includeNoscript;
	}

	public void setHttpLoader(HttpLoader httpLoader)
	{
		this.httpLoader = httpLoader;
	}

	public ReCaptchaResponse checkAnswer(String remoteAddr, String challenge, String response)
	{
		String method = "checkAnswer";
		LogEntryVO logEntryVO = new LogEntryVO(Level.INFO, "ReCaptchaImpl >  checkAnswer > called for remoteAddr[" + remoteAddr + "],challenge[" + challenge + "],response["
				+ response + "]", className, method, null);
		executionContext.log(logEntryVO);
		boolean valid = false;
		String errorMessage = null;

		String postParameters = "privatekey=" + URLEncoder.encode(privateKey) + "&remoteip=" + URLEncoder.encode(remoteAddr) + "&challenge=" + URLEncoder.encode(challenge)
				+ "&response=" + URLEncoder.encode(response);

		logEntryVO = new LogEntryVO(Level.INFO, "ReCaptchaImpl >  checkAnswer > postParameters::" + postParameters, className, method, null);
		executionContext.log(logEntryVO);

		String message = httpLoader.httpPost(VERIFY_URL, postParameters);

		logEntryVO = new LogEntryVO(Level.INFO, "ReCaptchaImpl >  checkAnswer > message::" + message, className, method, null);
		executionContext.log(logEntryVO);

		if (message == null)
		{
			return new ReCaptchaResponse(false, "Null read from server.");
		}

		String[] a = message.split("\r?\n");
		if (a.length < 1)
		{
			return new ReCaptchaResponse(false, "No answer returned from recaptcha: " + message);
		}
		valid = "true".equals(a[0]);
		if (!valid)
		{
			if (a.length > 1)
				errorMessage = a[1];
			else
				errorMessage = "recaptcha4j-missing-error-message";
		}
		logEntryVO = new LogEntryVO(Level.INFO, "ReCaptchaImpl >  checkAnswer > valid::" + valid, className, method, null);
		executionContext.log(logEntryVO);
		return new ReCaptchaResponse(valid, errorMessage);
	}

	public String createRecaptchaHtml(String errorMessage, Properties options)
	{
		String errorPart = (errorMessage == null ? "" : "&amp;error=" + URLEncoder.encode(errorMessage));

		String message = fetchJSOptions(options);

		message += "<script type=\"text/javascript\" src=\"" + recaptchaServer + "/challenge?k=" + publicKey + errorPart + "\"></script>\r\n";

		if (includeNoscript)
		{
			String noscript = "<noscript>\r\n" + "	<iframe src=\"" + recaptchaServer + "/noscript?k=" + publicKey + errorPart
					+ "\" height=\"300\" width=\"500\" frameborder=\"0\"></iframe><br>\r\n"
					+ "	<textarea name=\"recaptcha_challenge_field\" rows=\"3\" cols=\"40\"></textarea>\r\n"
					+ "	<input type=\"hidden\" name=\"recaptcha_response_field\" value=\"manual_challenge\">\r\n" + "</noscript>";
			message += noscript;
		}

		return message;
	}

	public String createRecaptchaHtml(String errorMessage, String theme, Integer tabindex)
	{
		Properties options = new Properties();

		if (theme != null)
		{
			options.setProperty(PROPERTY_THEME, theme);
		}
		if (tabindex != null)
		{
			options.setProperty(PROPERTY_TABINDEX, String.valueOf(tabindex));
		}

		return createRecaptchaHtml(errorMessage, options);
	}

	/**
	 * Produces javascript array with the RecaptchaOptions encoded.
	 * 
	 * @param properties
	 * @return
	 */
	private String fetchJSOptions(Properties properties)
	{
		if (properties == null || properties.size() == 0)
		{
			return "";
		}

		String jsOptions = "<script type=\"text/javascript\">\r\n" + "var RecaptchaOptions = {";

		for (Enumeration e = properties.keys(); e.hasMoreElements();)
		{
			String property = (String) e.nextElement();

			jsOptions += property + ":'" + properties.getProperty(property) + "'";

			if (e.hasMoreElements())
			{
				jsOptions += ",";
			}

		}

		jsOptions += "};\r\n</script>\r\n";

		return jsOptions;
	}
}
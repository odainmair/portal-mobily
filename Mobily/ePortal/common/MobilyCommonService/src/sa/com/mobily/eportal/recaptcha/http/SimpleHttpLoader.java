package sa.com.mobily.eportal.recaptcha.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;

public class SimpleHttpLoader implements HttpLoader
{	
	private final String className = SimpleHttpLoader.class.getName();
	
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	public String httpGet(String urlS)
	{
		String method = "httpGet";
		LogEntryVO logEntryVO = new LogEntryVO(Level.INFO, "SimpleHttpLoader >  httpGet > urlS::" + urlS, className, method, null);
		executionContext.log(logEntryVO);
		InputStream in = null;
		URLConnection connection = null;
		try
		{
			/*
			 * System.setProperty("http.proxyHost", "proxy");
			 * System.setProperty("http.proxyPort", "8080");
			 */

			SocketAddress addr = new InetSocketAddress("proxy", 8080);
			Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);

			URL url = new URL(urlS);
			connection = url.openConnection(proxy);

			logEntryVO = new LogEntryVO(Level.INFO, "SimpleHttpLoader >  httpGet > connection::" + connection, className, method, null);
			executionContext.log(logEntryVO);

			// jdk 1.4 workaround
			setJdk15Timeouts(connection);

			in = connection.getInputStream();

			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			while (true)
			{
				int rc = in.read(buf);
				if (rc <= 0)
					break;
				else
					bout.write(buf, 0, rc);
			}

			// return the generated javascript.
			return bout.toString();
		}
		catch (IOException e)
		{
			logEntryVO = new LogEntryVO(Level.SEVERE, e.getMessage(), className, method, e);
			executionContext.log(logEntryVO);
			throw new ServiceException("Cannot load URL: " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (in != null)
					in.close();
			}
			catch (Exception e)
			{
				// swallow.
			}
		}
	}

	public String httpPost(String urlS, String postdata)
	{
		String method = "httpPost";
		LogEntryVO logEntryVO = new LogEntryVO(Level.INFO, "SimpleHttpLoader >  httpPost > urlS111::" + urlS, className, method, null);
		executionContext.log(logEntryVO);
		InputStream in = null;
		URLConnection connection = null;
		try
		{

			/*
			 * System.setProperty("http.proxyHost", "proxy");
			 * System.setProperty("http.proxyPort", "8080");
			 */

			SocketAddress addr = new InetSocketAddress("proxy", 8080);
			Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);

			URL url = new URL(urlS);
			connection = url.openConnection(proxy);

			logEntryVO = new LogEntryVO(Level.INFO, "SimpleHttpLoader >  httpPost > connection111::" + connection, className, method, null);
			executionContext.log(logEntryVO);

			connection.setDoOutput(true);
			connection.setDoInput(true);

			setJdk15Timeouts(connection);

			OutputStream out = connection.getOutputStream();
			out.write(postdata.getBytes());
			out.flush();

			in = connection.getInputStream();

			logEntryVO = new LogEntryVO(Level.INFO, "SimpleHttpLoader >  httpPost > in::" + in, className, method, null);
			executionContext.log(logEntryVO);

			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			while (true)
			{
				int rc = in.read(buf);
				if (rc <= 0)
					break;
				else
					bout.write(buf, 0, rc);
			}

			out.close();
			in.close();

			// return the generated javascript.
			return bout.toString();
		}
		catch (IOException e)
		{
			logEntryVO = new LogEntryVO(Level.SEVERE, e.getMessage(), className, method, e);
			executionContext.log(logEntryVO);
			throw new ServiceException("Cannot load URL: " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if (in != null)
					in.close();
			}
			catch (Exception e)
			{
				// swallow.
			}
		}
	}

	/**
	 * Timeouts are new from JDK1.5, handle it generic for JDK1.4 compatibility.
	 * 
	 * @param connection
	 */
	private void setJdk15Timeouts(URLConnection connection)
	{
		try
		{
			Method readTimeoutMethod = connection.getClass().getMethod("setReadTimeout", new Class[] { Integer.class });
			Method connectTimeoutMethod = connection.getClass().getMethod("setConnectTimeout", new Class[] { Integer.class });
			if (readTimeoutMethod != null)
			{
				readTimeoutMethod.invoke(connection, new Object[] { new Integer(10000) });
			}
			if (connectTimeoutMethod != null)
			{
				connectTimeoutMethod.invoke(connection, new Object[] { new Integer(10000) });
			}
		}
		catch (Exception e)
		{
			// swallow silently
		}
	}
}
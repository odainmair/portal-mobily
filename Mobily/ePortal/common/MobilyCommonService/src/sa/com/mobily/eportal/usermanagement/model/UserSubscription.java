package sa.com.mobily.eportal.usermanagement.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

//import org.apache.openjpa.persistence.DataCache;


/**
 * The persistent class for the USER_SUBSCRIPTION database table.
 * 
 */
@Entity
@Table(name="USER_SUBSCRIPTION")
//@DataCache(enabled=false)
@NamedQueries({
    @NamedQuery(name=UserSubscription.FIND_SUBSCRIPTION_BY_ACCOUNT, query="SELECT sub FROM UserSubscription sub WHERE LOWER(sub.isDefault) = 'y' AND ( sub.msisdn = :accountNumber OR sub.serviceAcctNumber = :accountNumber)"),
    @NamedQuery(name=UserSubscription.DELETE_SUBSCRIPTION_BY_MSISDN, query="DELETE FROM UserSubscription sub WHERE CONCAT(sub.msisdn,sub.serviceAcctNumber) IN : msisdnList"),
    @NamedQuery(name=UserSubscription.DELETE_SUBSCRIPTION_BY_SERVICEACCOUNTNUMBER, query="DELETE FROM UserSubscription sub WHERE sub.serviceAcctNumber IN : serviceAccountList"),
    @NamedQuery(name=UserSubscription.UPDATE_SUBSCRIPTION_INACTIVE, query="UPDATE UserSubscription sub SET sub.isActive=:activeVal WHERE CONCAT(sub.msisdn,sub.serviceAcctNumber) IN : msisdnList"),
    @NamedQuery(name=UserSubscription.FIND_EXISTING_SUBSCRIPTION, query="SELECT sub FROM UserSubscription sub WHERE sub.msisdn = :accountNumber OR sub.serviceAcctNumber = :accountNumber")
})
public class UserSubscription implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_SUBSCRIPTION_BY_ACCOUNT = "UserSubscription.FindSubscriptionByAccount";
	public static final String DELETE_SUBSCRIPTION_BY_MSISDN = "UserSubscription.DELETE_SUBSCRIPTION_BY_MSISDN";
	public static final String DELETE_SUBSCRIPTION_BY_SERVICEACCOUNTNUMBER = "UserSubscription.DELETE_SUBSCRIPTION_BY_SERVICEACCOUNTNUMBER";
	public static final String UPDATE_SUBSCRIPTION_INACTIVE = "UserSubscription.UPDATE_SUBSCRIPTION_INACTIVE";
	public static final String FIND_EXISTING_SUBSCRIPTION = "UserSubscription.FIND_EXISTING_SUBSCRIPTION";

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USER_SUBSCRIPTION_SEQ")
	@SequenceGenerator(name="USER_SUBSCRIPTION_SEQ", sequenceName="USER_SUBSCRIPTION_SEQ")
	@Column(name="SUBSCRIPTION_ID")
	//@NotNull
	private Long subscriptionId;

	@Column(name="CC_STATUS")
	private Integer ccStatus;

	@Column(name="CREATED_DATE")
	//@NotNull
	private Timestamp createdDate;

	@Column(name="CUSTOMER_TYPE")
	//@Size(min=1, max=20)
	private String customerType;

	@Column(name="IS_ACTIVE")
	//@Size(min=1, max=1)
	private String isActive;

	@Column(name="IS_DEFAULT")
	//@NotNull
	//@Size(min=1, max=1)
	private String isDefault;

	@Column(name="LAST_UPDATED_TIME")
	private Timestamp lastUpdatedTime;
	
	//@Size(min=1, max=20)
	private String msisdn;

	@Column(name="PACKAGE_ID")
	//@Size(min=1, max=30)
	private String packageId;

	@Column(name="SERVICE_ACCT_NUMBER")
	//@Size(min=1, max=50)
	private String serviceAcctNumber;
	
	@Column(name="SUBSCRIPTION_NAME")
	//@Size(min=1, max=100)
	private String subscriptionName;

	@Column(name="SUBSCRIPTION_TYPE")
	//@NotNull
	private Integer subscriptionType;
	
	@Column(name="LINE_TYPE")
	private Integer lineType;

	//bi-directional many-to-one association to UserAccount
    @ManyToOne
	@JoinColumn(name="USER_ACCT_ID")
	private UserAccount userAccount;

    public UserSubscription() {
    }

	public Long getSubscriptionId() {
		return this.subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Integer getCcStatus() {
		return this.ccStatus;
	}

	public void setCcStatus(Integer ccStatus) {
		this.ccStatus = ccStatus;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getCustomerType() {
		return this.customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getIsActive() {
		return this.isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsDefault() {
		return this.isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	public Timestamp getLastUpdatedTime() {
		return this.lastUpdatedTime;
	}

	public void setLastUpdatedTime(Timestamp lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public String getMsisdn() {
		return this.msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getPackageId() {
		return this.packageId;
	}

	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	public String getServiceAcctNumber() {
		return this.serviceAcctNumber;
	}

	public void setServiceAcctNumber(String serviceAcctNumber) {
		this.serviceAcctNumber = serviceAcctNumber;
	}

	public Integer getSubscriptionType() {
		return this.subscriptionType;
	}

	public void setSubscriptionType(Integer subscriptionType) {
		this.subscriptionType = subscriptionType;
	}
	
	public UserAccount getUserAccount() {
		return this.userAccount;
	}

	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}
	
	public String getSubscriptionName() {
		return subscriptionName;
	}

	public void setSubscriptionName(String subscriptionName) {
		this.subscriptionName = subscriptionName;
	}
	
	public Integer getLineType() {
		return lineType;
	}

	public void setLineType(Integer lineType) {
		this.lineType = lineType;
	}
}
package sa.com.mobily.eportal.util;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author Suresh Vathsavai-MIT
 * @Description: 
 * @Date 30-Mar-2017
 */
public class TransactionIdInput extends BaseVO
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String channelName;
	private String transactionType;
	private String accountNumber;
	/**
	 * @return the channelName
	 */
	public String getChannelName()
	{
		return channelName;
	}
	/**
	 * @param channelName the channelName to set
	 */
	public void setChannelName(String channelName)
	{
		this.channelName = channelName;
	}
	/**
	 * @return the transactionType
	 */
	public String getTransactionType()
	{
		return transactionType;
	}
	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType)
	{
		this.transactionType = transactionType;
	}
	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber()
	{
		return accountNumber;
	}
	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber)
	{
		this.accountNumber = accountNumber;
	}
	
	
}

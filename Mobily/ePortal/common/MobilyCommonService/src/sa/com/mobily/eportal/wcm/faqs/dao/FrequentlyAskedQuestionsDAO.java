package sa.com.mobily.eportal.wcm.faqs.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.wcm.faqs.vo.FaqVO;

/**
 * This DAO class is responsible for DB operations on USER_ACCOUNTS table
 * <p>
 * it contains methods for retrieve, add and update.
 * 
 * @author Raef Kandil
 */
public class FrequentlyAskedQuestionsDAO extends AbstractDBDAO<FaqVO>
{

	private static String COL_FAQ_NAME = "FAQ_NAME";

	private static String COL_FAQ_TYPE = "FAQ_TYPE";

	private static String COL_COUNTER = "COUNTER";

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.WCM_SERVICES_LOGGER_NAME);

	private static String clazz = FrequentlyAskedQuestionsDAO.class.getName();

	private static FrequentlyAskedQuestionsDAO instance = new FrequentlyAskedQuestionsDAO();

	private static final String FAQ_ADD_FAQ = "INSERT INTO FAQ_COUUNTER(FAQ_NAME, FAQ_TYPE, COUNTER) " + "VALUES(?, ?, 0) ";

	private static final String FAQ_DEL_FAQ = "DELETE FROM FAQ_COUUNTER where FAQ_NAME= ?";

	private static final String FAQ_UPD_COUNTER = "UPDATE FAQ_COUUNTER SET COUNTER = ? WHERE FAQ_NAME = ? ";

	private static final String FAQ_SLCT_FAQ_CNT = "SELECT * FROM FAQ_COUUNTER WHERE FAQ_NAME = ?";

	private static final String FAQ_SLCT_FAQ_TOP = "select * from " + 
													"(SELECT * FROM FAQ_COUUNTER WHERE FAQ_TYPE = ? ORDER BY COUNTER DESC)" + 
													"WHERE ROWNUM <= 10";

	protected FrequentlyAskedQuestionsDAO()
	{
		super(DataSources.DEFAULT);
	}

	/**
	 * This method returns a singleton instance from the DAO class
	 * 
	 * @return <code>UserAccountDAO</code>
	 * @author Yousef Alkhalaileh
	 */
	public static synchronized FrequentlyAskedQuestionsDAO getInstance()
	{
		return instance;
	}

	public int insertFaqQuestion(String questionName, String questionType)
	{
		return update(FAQ_ADD_FAQ, new Object[] { questionName, questionType });

	}

	public int selectFAQCounter(String qestionName)
	{
		return query(FAQ_SLCT_FAQ_CNT, new Object[] { qestionName }).size() > 0 ? query(FAQ_SLCT_FAQ_CNT, new Object[] { qestionName }).get(0).getCounter() : -1;

	}

	public int deleteFaqQuestion(String qestionName)
	{
		return update(FAQ_DEL_FAQ, qestionName);

	}

	public int updateCounter(String qestionName)
	{
		int counter = selectFAQCounter(qestionName);
		counter++;
		return update(FAQ_UPD_COUNTER, new Object[] { counter, qestionName });
	}

	public List<FaqVO> selectFAQtop(String faqType)
	{
		return query(FAQ_SLCT_FAQ_TOP, new Object[] { faqType });
	}

	public int updateFAQrecords(String qestionName, String faqType)
	{
		if (selectFAQCounter(qestionName) != -1)
		{
			return updateCounter(qestionName);
		}
		else
		{
			return insertFaqQuestion(qestionName, faqType);
		}
	}

	/**
	 * This method returns a UserAccount from DB
	 * 
	 * @param request
	 *            <code>ResultSet</code> from DB
	 * @return <code>UserAccount</code> mapped user account
	 * @author Yousef Alkhalaileh
	 */
	@Override
	protected FaqVO mapDTO(ResultSet rs) throws SQLException
	{

		FaqVO faqVO = new FaqVO();

		faqVO.setCounter(rs.getInt(COL_COUNTER));

		faqVO.setFaqName(rs.getString(COL_FAQ_NAME));

		faqVO.setFaqType(rs.getString(COL_FAQ_TYPE));

		LogEntryVO logEntryVO = new LogEntryVO(Level.INFO, "dto >>>>" + faqVO.toString(), clazz, "mapDTO", null);
		executionContext.audit(logEntryVO);

		return faqVO;
	}

}
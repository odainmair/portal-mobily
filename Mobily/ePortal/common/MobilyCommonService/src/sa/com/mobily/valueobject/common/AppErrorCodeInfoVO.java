package sa.com.mobily.valueobject.common;

import java.util.Date;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class AppErrorCodeInfoVO  extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String errorCode;
	
	private String errorCodeEn;
	
	private String errorCodeAr;
	
	private Date updatedDate;

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorCodeEn() {
		return errorCodeEn;
	}

	public void setErrorCodeEn(String errorCodeEn) {
		this.errorCodeEn = errorCodeEn;
	}

	public String getErrorCodeAr() {
		return errorCodeAr;
	}

	public void setErrorCodeAr(String errorCodeAr) {
		this.errorCodeAr = errorCodeAr;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}

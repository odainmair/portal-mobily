package sa.com.mobily.valueobject.common;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class PinCodeVO extends BaseVO
{
	private static final long serialVersionUID = 9196658972189075544L;

	private String msisdn;

	private String language;

	private String messageIdEn;

	private String messageIdAr;

	private int serviceID;
	
	private String existingPINCode;
	
	private int pinLength = 8;
	
	/**
	 * @return the pinLength
	 */
	public int getPinLength()
	{
		return pinLength;
	}

	/**
	 * @param pinLength the pinLength to set
	 */
	public void setPinLength(int pinLength)
	{
		this.pinLength = pinLength;
	}

	public String getMsisdn()
	{
		return msisdn;
	}

	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}

	public String getLanguage()
	{
		return language;
	}

	public void setLanguage(String language)
	{
		this.language = language;
	}

	public String getMessageIdEn()
	{
		return messageIdEn;
	}

	public void setMessageIdEn(String messageIdEn)
	{
		this.messageIdEn = messageIdEn;
	}

	public String getMessageIdAr()
	{
		return messageIdAr;
	}

	public void setMessageIdAr(String messageIdAr)
	{
		this.messageIdAr = messageIdAr;
	}

	public int getServiceID()
	{
		return serviceID;
	}

	public void setServiceID(int serviceID)
	{
		this.serviceID = serviceID;
	}

	public String getExistingPINCode()
	{
		return existingPINCode;
	}

	public void setExistingPINCode(String existingPINCode)
	{
		this.existingPINCode = existingPINCode;
	}
}